/*! Console History v1.5.1 | (c) 2016-2019 | MIT Licensed | https://git.io/console */
"use strict"
if(!DEBUG_MODE){
	if("undefined"!=typeof console.history)throw new Error("Only one instance of console-history.js can run at a time.")
	console._log=console.log
	console._info=console.info
	console._warn=console.warn
	console._error=console.error
	console._debug=console.debug
	console.history=[]
	console.log=function(){return console._intercept("log",arguments)}
	console.info=function(){return console._intercept("info",arguments)}
	console.warn=function(){return console._intercept("warn",arguments)}
	console.error=function(){return console._intercept("error",arguments)}
	console.debug=function(){return console._intercept("debug",arguments)}
	console._intercept=function(a,b){
		var c=(new Date).toUTCString()
		if(a||(a="log"),b&&0!==b.length){
			console["_"+a].apply(console,b)
			//console._log("intercepted")
			var d=!1
			try{throw Error("")}
				catch(a){
					var e=a.stack.split("\n")
					d=[]
					for(var f=0;f<e.length;f++)
						e[f].indexOf("console-history.js")>-1||e[f].indexOf("console-history.min.js")>-1 || "Error"===e[f]||d.push(e[f].trim())
					}
			console.history.push({type:a,timestamp:c,arguments:b,stack:d})
			//limit 30 elements
			if(console.history.length>30){console.history.shift()}
		}
	}
}
