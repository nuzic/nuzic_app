//data manipulation functions

function DATA_change_voice_data_at_voice_id(voice_data){
	let data = DATA_get_current_state_point(true)
	let voice_number = data.voice_data.findIndex(voice=>{
		return voice.voice_id==voice_data.voice_id
	})
	if(voice_number==-1){
		console.error("Voice not found")
		return
	}
	data.voice_data[voice_number]=voice_data
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}


function DATA_change_PPM(value){
	let data = DATA_get_current_state_point(true)
	let provv=DATA_calculate_inRange(value,30,240)[0]
	data.global_variables.PPM=provv
	PPM=provv
	data.voice_data.forEach(voice=>{
		voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
	})
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_calculate_inRange(x, value1, value2) {
	//ritorna un valore entro i limiti
	return x < value1 ? [value1,false] : (x > value2 ? [value2,false] : [x,true]);
}

function DATA_calculate_minimum_Li(){
	let data = DATA_get_current_state_point(false)
	let neopulse_list = data.voice_data.map(voice=>{
		return voice.neopulse
	})
	let mCM = neopulse_list.reduce((prop,item)=>{
		return Math.abs((prop * item.N) / gcd_two_numbers(prop, item.N))
	},1)
	return mCM
	function gcd_two_numbers(x, y) {
		x = Math.abs(x)
		y = Math.abs(y)
		while(y) {
			var t = y
			y = x % y
			x = t
		}
		return x
	}
}

function DATA_force_Li(new_Li_absolute){
	let current_progress_bar_values=APP_return_progress_bar_values()
	let end_loop_at_Li = false
	if(current_progress_bar_values[2]==Li)end_loop_at_Li=true
	let data = DATA_get_current_state_point(true)
	//verify if last segment blocked voices can be extended
	let extend_blocked=DATA_verify_blocked_voices_extend_last_segment(data)
	data.voice_data.forEach(voice=>{
		DATA_calculate_force_voice_Li(voice,new_Li_absolute,extend_blocked)
	})
	data.global_variables.Li=new_Li_absolute
	//actualize progress bar
	APP_redraw_Progress_bar_limits(false,end_loop_at_Li)
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_add_voice(voice_id){
	let data = DATA_get_current_state_point(true)
	//remove solo
	data.voice_data.forEach(voice=>{
		voice.solo=false
		voice.selected=false
	})
	let voice_position = -1
	let voice_data = JSON.parse(JSON.stringify(data.voice_data.find(item=>{
		voice_position++
		return item.voice_id==voice_id
	})))
	if(voice_data==null)return
	voice_data.data.segment_data.forEach(segment=>{
		let frac = segment.fraction.pop()
		frac.N=1
		frac.D=1
		frac.start=0
		segment.fraction=[]
		segment.fraction[0]=frac
		//reset sound
		segment.sound=[]
		segment.sound[0]={note:-1,diesis:true,A_pos:[],A_neg:[]}
		segment.sound[1]={note:-4,diesis:true,A_pos:[],A_neg:[]}
		//reset time
		segment.time=[]
		segment.time[0]={"P":0,"F":0}
		segment.time[1]={"P":frac.stop,"F":0}
	})
	let voice_id_list = data.voice_data.map(item=>{
		let number = Number(item.voice_id)
		return number
	})
	let new_voice_id = Math.max(...voice_id_list)+1
	voice_data.solo= false
	voice_data.metro= 0
	voice_data.selected=true
	voice_data.voice_id = new_voice_id
	voice_data.A = false
	let string = `${APP_language_return_text_id("voice")} ${new_voice_id}`
	voice_data.name= string.slice(0,8)
	let new_color=DATA_calculate_data_new_color(data)
	if(new_color!=null){
		voice_data.color=new_color
	}
	voice_data.data.midi_data=DATA_segment_data_to_midi_data(voice_data.data.segment_data,voice_data.neopulse)
	data.voice_data.splice(voice_position,0,voice_data)
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_voice_change_midi_channel(voice_id, channel){
	APP_stop()
	//Selected voice
	let data = DATA_get_current_state_point(true)
	let selected_voice_data= data.voice_data.find(item=>{
		return item.voice_id==voice_id
	})
	selected_voice_data.midiCh=channel
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_calculate_data_new_color(data){
	let all_colors=["nuzic_blue","nuzic_green","nuzic_yellow","nuzic_pink","nuzic_red"]
	let used_colors=[]
	data.voice_data.forEach(voice=>{
		used_colors.push(voice.color)
	})
	let used_colors_set=[...new Set(used_colors)]
	let new_color=all_colors.find(color=>{return !_test_color(color)})
	return new_color
	function _test_color(c){
		return used_colors_set.some(item=>{
			return item===c
		})
	}
}

function DATA_clear_voice(voice_id){
	let data = DATA_get_current_state_point(true)
	//duplicate current voice
	let voice_data = data.voice_data.find(item=>{
		return item.voice_id==voice_id
	})
	voice_data.data.segment_data.forEach(segment=>{
		let frac = segment.fraction.pop()
		frac.N=1
		frac.D=1
		frac.start=0
		segment.fraction=[]
		segment.fraction[0]=frac
		//reset sound
		segment.sound=[]
		segment.sound[0]={note:-1,diesis:true,A_pos:[],A_neg:[]}
		segment.sound[1]={note:-4,diesis:true,A_pos:[],A_neg:[]}
		//reset time
		segment.time=[]
		segment.time[0]={"P":0,"F":0}
		segment.time[1]={"P":frac.stop,"F":0}
	})
	voice_data.A=false
	//data.midi_data
	voice_data.data.midi_data=DATA_segment_data_to_midi_data(voice_data.data.segment_data,voice_data.neopulse)
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_delete_voice(voice_id){
	let data = DATA_get_current_state_point(true)
	//duplicate current voice
	if(data.voice_data.length>1){
		//more that 1 voice exist
		//proceed to eliminate voice
		let voice_number = -1
		data.voice_data.find(item=>{
			voice_number++
			return item.voice_id==voice_id
		})
		let was_selected=data.voice_data[voice_number].selected
		data.voice_data.splice(voice_number,1)
		//select previous voice if no other voice is selected
		if(was_selected){
			voice_number--
			if(voice_number<0)voice_number=0
			data.voice_data[voice_number].selected=true
		}
		DATA_insert_new_state_point(data)
		DATA_load_state_point_data(false,true)
	}
}

function DATA_calculate_force_voice_NP(voice_id,N,D){
	//calculating if every segment existing is multiple of N
	let NP_ok=false
	let data = DATA_get_current_state_point(true)
	let segment_Ls_list = []
	data.voice_data.forEach(voice=>{
		voice.data.segment_data.forEach(segment=>{
			segment_Ls_list.push(segment.time.slice(-1)[0].P*voice.neopulse.N/voice.neopulse.D)
		})
	})
	NP_ok=!segment_Ls_list.find(Ls=>{return Ls%N!=0})
	if(!NP_ok){
		return false
	}
	APP_stop()
	let selected_voice_data=null
	if(voice_id==null){
		//Selected voice
		selected_voice_data= data.voice_data.find(item=>{
			return item.selected
		})
	}else{
		selected_voice_data= data.voice_data.find(item=>{
			return item.voice_id==voice_id
		})
	}
	if(selected_voice_data==null)return false
	//changing L segments to new one and clearing them
	let N_old = selected_voice_data.neopulse.N
	let D_old = selected_voice_data.neopulse.D
	selected_voice_data.data.segment_data.forEach(segment=>{
		let Pend = (Math.round(segment.time.slice(-1)[0].P*N_old/D_old))*(D/N)
		segment.time = [{"P":0,"F":0},{"P": Pend,"F":0}]
		segment.fraction = [{"N": 1,"D": 1,"start": 0,"stop":Pend}]
		segment.sound = [{note:-1,diesis:true,A_pos:[],A_neg:[]},{note:-4,diesis:true,A_pos:[],A_neg:[]}]
	})
	selected_voice_data.neopulse={"N":N,"D":D}
	//recalc midi
	selected_voice_data.data.midi_data=DATA_segment_data_to_midi_data(selected_voice_data.data.segment_data,selected_voice_data.neopulse)
	APP_info_msg("enterNP")
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
	return true
}

//input voice_data , new_Li 1/1
function DATA_calculate_force_voice_Li(voice,new_Li_absolute,extend_last_segment_blocked="false"){
	//!!! this function modify voice_data !!!
	//need to be done Before
	//calculate minimum voice length (pulse absolute)
	//DATA_calculate_minimum_Li
	let new_Li = new_Li_absolute * voice.neopulse.D/voice.neopulse.N
	let voice_Li = 0 //att is voice Li EXCLUDING segments that will be cutted
	let index_cut = 0
	let index = 0
	voice.data.segment_data.find(segment=>{
		let Ls = segment.time.slice(-1)[0].P
		voice_Li+=Ls
		if(voice_Li>=new_Li){
			index_cut=index
			return true
		}
		index++
	})
	let d_Li = new_Li - voice_Li
	let n_seg = voice.data.segment_data.length-1
	if(voice_Li<new_Li){
		//modify last segment if void OR create a new one
		let last_segment = voice.data.segment_data[voice.data.segment_data.length-1]
		//if voice is blocked need to add a new segment always
		if(!(voice.blocked && !extend_last_segment_blocked) && last_segment.sound.length==2 && last_segment.sound[0].note==-1 && last_segment.fraction.length==1 && last_segment.fraction[0].N==1 && last_segment.fraction[0].D==1){
			let end_pulse=last_segment.time[1].P+d_Li
			voice.data.segment_data[voice.data.segment_data.length-1].time[1].P=end_pulse
			voice.data.segment_data[voice.data.segment_data.length-1].fraction[0].stop=end_pulse
			//diesis doesnt change
		}else{
			// let fraction=[]
			let new_Ls=d_Li
			// fraction[0] = {"N":1,"D":1,"start":0,"stop":new_Ls}
			// let new_segment_data={"time":[{"P":0,"F":0},{"P":new_Ls,"F":0}],"fraction":fraction,"sound":[{note:-1,diesis:true,A_pos:[],A_neg:[]},{note:-4,diesis:true,A_pos:[],A_neg:[]}],"segment_name":""}
			let new_segment_data=DATA_calculate_new_segment_data(new_Ls,"")
			voice.data.segment_data.splice(voice.data.segment_data.length,0,new_segment_data)
		}
	}
	if(voice_Li>=new_Li){
		//modify or eliminate last segment(s)
		voice.data.segment_data.splice(index_cut+1,n_seg-index_cut+1)
		if(d_Li==0){
			//cut from next segment
			//voice.data.segment_data.splice(index_cut+1,n_seg-index_cut+1)
		}else{
			//shorten index_cut segment
			let new_Ls = voice.data.segment_data[index_cut].time.slice(-1)[0].P+d_Li//d_Li negativo
			//extend_last_element false but doesn'matter, it cut the segment here
			let extend_last_element=false
			DATA_calculate_force_segment_data_Ls(voice.data.segment_data[index_cut],new_Ls,extend_last_element)
		}
	}
	voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
}

function DATA_calculate_new_segment_data(Ls,segment_name=""){
	return new_segment_data={"time":[{"P":0,"F":0},{"P":Ls,"F":0}],"fraction":[{"N":1,"D":1,"start":0,"stop":Ls}],"sound":[{note:-1,diesis:true,A_pos:[],A_neg:[]},{note:-4,diesis:true,A_pos:[],A_neg:[]}],"segment_name":segment_name}
}

function DATA_verify_blocked_voices_extend_last_segment(data){
	let extend_blocked = true
	data.voice_data.forEach(voice=>{
		if(voice.blocked){
			let last_segment = voice.data.segment_data[voice.data.segment_data.length-1]
			if(last_segment.sound.length==2 && last_segment.sound[0].note==-1 && last_segment.fraction.length==1 && last_segment.fraction[0].N==1 && last_segment.fraction[0].D==1){
				//no problem
			}else{
				extend_blocked=false
			}
		}
	})
	return extend_blocked
}

function DATA_rearrange_voices(new_voice_id_order){
	let data = DATA_get_current_state_point(true)
	//verify that new_voices_order contain every voice_id of data voices XXX
	let voice_id_array = data.voice_data.map(voice=>{
		return voice.voice_id
	})
	//console.log(voice_id_array)
	if(new_voice_id_order.length!=voice_id_array.length){
		console.error("New voice order not compatible with current voice list")
		return
	}
	let changed = voice_id_array.some((value,index)=>{
		return value!=new_voice_id_order[index]
	})
	if(!changed){
		//console.error("No cheanges")
		return
	}
	let proceed = !voice_id_array.some(id=>{
		return !new_voice_id_order.some(new_id=>{return new_id==id})
	})
	if(!proceed){
		console.error("New voice order not compatible with current voice list")
		return
	}
	// reorder
	let new_voice_data = new_voice_id_order.map(voice_id=>{
		return data.voice_data.find(voice=>{return voice.voice_id==voice_id})
	})
	data.voice_data=new_voice_data
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_which_voice_instrument(voice_id){
	let data = DATA_get_current_state_point(false)
	let current_voice_data = data.voice_data.find(item=>{
		return item.voice_id==voice_id
	})
	return current_voice_data.instrument
}

function DATA_show_hide_voice_RE(voice_id,show){
	let data = DATA_get_current_state_point(true)
	//Selected voice
	let selected_voice_data= data.voice_data.find(item=>{
		return item.voice_id==voice_id
	})
	selected_voice_data.RE_visible = show
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_list_available_metro_sounds(){
	let all_metro_sounds=[0,1,2,3,4]
	//not used  1,2,3,4,5
	let data = DATA_get_current_state_point(true)
	let used = data.voice_data.map(voice => {return voice.metro})
	used.sort(function(a, b){return a-b})
	//make sure no duplicates (zeros)
	let unique = [...new Set(used)]
	let available_metro_sounds = all_metro_sounds.filter(item=>{
		let found = unique.some(usedItem=>{
			return usedItem == item
		})
		return !found
	})
	//add 0
	available_metro_sounds.unshift(0)
	available_metro_sounds = [...new Set(available_metro_sounds)]
	return available_metro_sounds
}

function DATA_set_pulse_sound(options){
	let data = DATA_get_current_state_point(true)
	let voice_data = data.voice_data.find(item=>{
		return item.voice_id==options.voice_id
	})
	if(voice_data.e_sound==options.e_sound)return
	voice_data.e_sound=options.e_sound
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

//SEGMENT COLUMN
function DATA_clear_column_segment(segment_index){
	let data = DATA_get_current_state_point(true)
	//change segment all blocked voices
	data.voice_data.forEach(voice=>{
		if(voice.blocked){
			let segment_data = voice.data.segment_data[segment_index]
			//reset fractioning
			let frac = segment_data.fraction.pop()
			frac.N=1
			frac.D=1
			frac.start=0
			segment_data.fraction=[]
			segment_data.fraction[0]=frac
			//reset sound
			segment_data.sound=[]
			segment_data.sound[0]={note:-1,diesis:true,A_pos:[],A_neg:[]}
			segment_data.sound[1]={note:-4,diesis:true,A_pos:[],A_neg:[]}
			//reset time
			segment_data.time=[]
			segment_data.time[0]={"P":0,"F":0}
			segment_data.time[1]={"P":frac.stop,"F":0}
			//reset name
			//segment_data.segment_name="" //other blocked segments too??? NO!
			//midi data
			voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
		}
	})
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_delete_column_segment(segment_index){
	//if(!DEBUG_MODE)console.log("DATA delete column segment")
	APP_stop()
	let data = DATA_get_current_state_point(true)
	//change segment all blocked voices
	let first_blocked_voice = data.voice_data.find(item=>{
		return item.blocked
	})
	//verify there is more than 1 segment
	if(first_blocked_voice.data.segment_data.length==1)return
	let Ls_absolute= first_blocked_voice.data.segment_data[segment_index].time.slice(-1)[0].P*first_blocked_voice.neopulse.N/first_blocked_voice.neopulse.D
	//recalculate Li
	let new_Li_absolute = data.global_variables.Li-Ls_absolute
	data.global_variables.Li=new_Li_absolute
	data.voice_data.forEach(item=>{
		if(item.blocked){
			//remove same segment from blocked voices
			item.data.segment_data.splice(segment_index,1)
			//recalculate midi
			item.data.midi_data=DATA_segment_data_to_midi_data(item.data.segment_data,item.neopulse)
		}else{
			//cut other voices
			DATA_calculate_force_voice_Li(item,new_Li_absolute,false)
		}
	})
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_copy_column_segment(segment_index){
	let data = DATA_get_current_state_point(true)
	let segment_data_list = []
	data.voice_data.forEach(voice=>{
		if(voice.blocked){
			let Ps_start=voice.data.segment_data.reduce((prop,item,index)=>{return (index<segment_index)?item.time.slice(-1)[0].P+prop:prop},0)
			segment_data_list.push({voice_id:voice.voice_id,neopulse:voice.neopulse, Psg_segment_start:Ps_start, segment: voice.data.segment_data[segment_index]})
		}
	})
	copied_data = {"id":"column", "data":segment_data_list}
}

function DATA_paste_column_segment_all(segment_index){
	if(copied_data.id!="column"){
		return
	}
	let data = DATA_get_current_state_point(true)
	let success = DATA_calculate_paste_column_segment_all(data,segment_index)
	if(!success)return
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_calculate_paste_column_segment_all(data,segment_index){
	//verify if copied data is compatible (same list of blocked voices and in correct order)
	let blocked_voices_list= data.voice_data.filter(item=>{
		return item.blocked
	})
	let compatible=true
	if (blocked_voices_list.length==copied_data.data.length){
		for (let i = 0; i < blocked_voices_list.length; i++) {
			if(blocked_voices_list[i].voice_id!=copied_data.data[i].voice_id)compatible=false
			if(blocked_voices_list[i].neopulse.N!=copied_data.data[i].neopulse.N)compatible=false
			if(blocked_voices_list[i].neopulse.D!=copied_data.data[i].neopulse.D)compatible=false
		}
	}else{
		compatible=false
	}
	if(!compatible){
		console.error("COPIED DATA NON COMPATIBLE")
		return false
	}
	let new_Li_absolute=0
	data.voice_data.forEach(voice=>{
		if(voice.blocked){
			let segment_copied_data = copied_data.data.find(data=>{return data.voice_id==voice.voice_id}).segment
			//verify if is possible...
			//if segment Ls> and not void
			let current_ls = voice.data.segment_data[segment_index].time.slice(-1)[0].P
			let copied_ls = segment_copied_data.time.slice(-1)[0].P
			if(current_ls==copied_ls){
				//segments are same length, proceed to copy data
				voice.data.segment_data[segment_index]=segment_copied_data
				//recalculate midi time and notes
				DATA_verify_voice_A(voice,false)
				voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
			}else{
				//different
				if(current_ls>copied_ls){
					//segment bigger
					//this function will create a segment that start with the copied one and continue with Fr 1/1 to the end of current segment
					let last_index = segment_copied_data.time.length-1
					//copy data
					voice.data.segment_data[segment_index]=JSON.parse(JSON.stringify(segment_copied_data))
					//add silence last-1 item
					voice.data.segment_data[segment_index].sound.splice(last_index,0,{note:-1,diesis:true,A_pos:[],A_neg:[]})
					//add last pulse
					voice.data.segment_data[segment_index].time.push({"P":current_ls,"F":0})
					//add/change fractioning
					let last_fraction = segment_copied_data.fraction.slice(-1)[0]
					if(last_fraction.N==1 && last_fraction.D==1){
						//modify last fraction interval
						voice.data.segment_data[segment_index].fraction[segment_copied_data.fraction.length-1].stop=current_ls
					}else{
						//add new fraction interval
						voice.data.segment_data[segment_index].fraction.push({"N":1,"D":1,"start":copied_ls,"stop":current_ls})
					}
					//recalculate midi time and notes
					DATA_verify_voice_A(voice,false)
					voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
				}else{
					//short
					//make the segment bigger
					let min_delta_Ls_abs = DATA_calculate_minimum_Li()
					let delta_abs = (copied_ls-current_ls)*voice.neopulse.N/voice.neopulse.D
					let resto_abs = delta_abs%min_delta_Ls_abs
					//check compatibility with NP
					if(resto_abs==0){
						//i can change the Li of the segment with the copied one
					}else{
						//need to add some more pulses
						//in this case it is a BAD THING : theorically impossible
						console.error("ERROR copy segment column, mismatching Ls")
						compatible=false
						return
					}
					//change Li
					let newLi=data.global_variables.Li+delta_abs
					if(new_Li_absolute!=0 && new_Li_absolute!=newLi){
						//maybe overkill
						console.error("ERROR copy segment column, mismatching Li")
						compatible=false
						return
					}
					new_Li_absolute=newLi
					if(newLi>max_Li){
						console.error("ERROR copy segment column, limit Li reached")
						compatible=false
						return
					}
					//copy segment
					voice.data.segment_data[segment_index]=segment_copied_data
					DATA_verify_voice_A(voice,false)
					voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
				}
			}
		}
	})
	if(!compatible){
		console.error("COPIED DATA NON COMPATIBLE: Li reached")
		return false
	}
	//change Li
	if(new_Li_absolute!=0){
		data.global_variables.Li=new_Li_absolute
		//change unblocked voices
		data.voice_data.forEach(voice=>{
				if(!voice.blocked){
					DATA_calculate_force_voice_Li(voice,new_Li_absolute,false)
				}
		})
	}
	return true
}

//SEGMENTS
function DATA_add_segment(voice_id,segment_index){
	let data = DATA_get_current_state_point(true)
	let current_voice_data = data.voice_data.find(item=>{
		return item.voice_id==voice_id
	})
	if(current_voice_data==null){
		console.error("Error adding segment, voice index not found")
		return
	}
	if(current_voice_data.data.segment_data.length<segment_index || segment_index<0){
		console.error("Error adding segment, segment index not found")
		return
	}
	//calculate minimum segment length (pulse absolute)
	let new_Ls_absolute=DATA_calculate_minimum_Li()
	//try to match with previous segment Li
	if(segment_index>0){
		let last_segment_Ls_absolte = current_voice_data.data.segment_data[segment_index-1].time.slice(-1)[0].P *current_voice_data.neopulse.N/current_voice_data.neopulse.D
		if(last_segment_Ls_absolte%new_Ls_absolute==0){
			//it is possible to use this value
			//console.log(last_segment_Ls_absoltue)
			new_Ls_absolute=last_segment_Ls_absolte
		}
	}
	let new_Li_absolute=Li+new_Ls_absolute
	//max length idea
	if(new_Li_absolute>max_Li){
		console.error("ERROR: max Li reached")
		return
	}
	if(current_voice_data.blocked){
		//voice is blocked
		data.voice_data.forEach(voice=>{
			let new_Ls=new_Ls_absolute*voice.neopulse.D/voice.neopulse.N
			let new_segment_data=DATA_calculate_new_segment_data(new_Ls,"")
			if(voice.blocked){
				//add segment in position
				voice.data.segment_data.splice(segment_index,0,new_segment_data)
				//midi
				voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
			}else{
				//add-extend segment in end
				DATA_calculate_force_voice_Li(voice,new_Li_absolute,false)
			}
		})
	}else{
		//verify if last segment blocked voices can be extended
		let extend_blocked=DATA_verify_blocked_voices_extend_last_segment(data)
		data.voice_data.forEach(voice=>{
			let new_Ls=(new_Ls_absolute*voice.neopulse.D/voice.neopulse.N)
			let new_segment_data=DATA_calculate_new_segment_data(new_Ls,"")
			if(voice.voice_id==voice_id){
				//add segment in position current voice
				voice.data.segment_data.splice(segment_index,0,new_segment_data)
				//midi
				voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
			}else{
				//add-extend segment in end
				DATA_calculate_force_voice_Li(voice,new_Li_absolute,extend_blocked)
			}
		})
	}
	//recalculate Li
	data.global_variables.Li=new_Li_absolute
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_delete_segment(voice_id,segment_index){
	APP_stop()
	let data = DATA_get_current_state_point(true)
	let voice = data.voice_data.find(item=>{
		return item.voice_id==voice_id
	})
	//verify there is more than 1 segment
	if(voice.data.segment_data.length==1)return
	let Ls_absolute= voice.data.segment_data[segment_index].time.slice(-1)[0].P*voice.neopulse.N/voice.neopulse.D
	//recalculate Li
	let new_Li_absolute = data.global_variables.Li-Ls_absolute
	data.global_variables.Li=new_Li_absolute
	if(voice.blocked){
		data.voice_data.forEach(item=>{
			if(item.blocked){
				//remove same segment from blocked voices
				item.data.segment_data.splice(segment_index,1)
				//recalculate midi
				item.data.midi_data=DATA_segment_data_to_midi_data(item.data.segment_data,item.neopulse)
			}else{
				//cut other voices
				DATA_calculate_force_voice_Li(item,new_Li_absolute,false)
			}
		})
	}else{
		data.voice_data.forEach(item=>{
			if(item.voice_id==voice_id){
				//remove segment
				item.data.segment_data.splice(segment_index,1)
				//recalculate midi
				item.data.midi_data=DATA_segment_data_to_midi_data(item.data.segment_data,item.neopulse)
			}else{
				//cut other voices
				DATA_calculate_force_voice_Li(item,new_Li_absolute,false)
			}
		})
	}
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_rearrange_segments(voice_id,new_segment_index_order){
	let data = DATA_get_current_state_point(true)
	let current_voice_data = data.voice_data.find(item=>{
		return item.voice_id==voice_id
	})
	let mod_voice_list=[]
	if(current_voice_data.blocked){
		//find list all blocked voices
		mod_voice_list=data.voice_data.filter(item=>{
			return item.blocked
		})
	}else{
		mod_voice_list[0]=current_voice_data
	}
	mod_voice_list.forEach(voice=>{
		let new_segment_data = []
		new_segment_index_order.forEach(segment_index=>{
			new_segment_data.push(voice.data.segment_data[segment_index])
		})
		voice.data.segment_data=new_segment_data
		voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
	})
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_calculate_force_segment_data_Ls(segment,new_Ls,extend_last_element=true){
	let old_Ls = segment.time.slice(-1)[0].P
	//new_Ls is NOT absolute
	if(new_Ls>old_Ls){
		//expand segment
		//verify fractioning
		let last_fraction = segment.fraction.slice(-1)[0]
		let delta = new_Ls-old_Ls
		let resto = delta%last_fraction.N
		if(resto==0){
			//extending fraction
			last_fraction.stop=new_Ls
			//if need to extend last element or last element == silence
			if(extend_last_element || segment.sound[segment.sound.length-2].note==-1){
				//mod last pulse
				segment.time[segment.time.length-1].P=new_Ls
			}else{
				//add a element with desired iT
				segment.time.push({"P":new_Ls,"F":0})
				segment.sound[segment.sound.length-1].note=-1
				segment.sound.push({note:-4,diesis:true,A_pos:[],A_neg:[]})
			}
		}else{
			//creating a new fractioning range 1/1+ "s"
			let inter_position = new_Ls-resto
			segment.fraction[segment.fraction.length-1].stop=inter_position
			let new_fraction = {"N":1,"D":1,"start":inter_position,"stop":new_Ls}
			segment.fraction.push(new_fraction)
			//add pulse(s) and mod last
			if(!extend_last_element &&segment.time[segment.time.length-1].P!=inter_position){
				//need to add a silence in order to complete previous fractioning range
				//mod old Ls pulse
				segment.sound[segment.sound.length-1].note=-1
				//add new pulse start new frac range
				segment.time.push({"P":inter_position,"F":0})
				segment.sound.push({note:-1,diesis:true,A_pos:[],A_neg:[]})
				//add new Ls pulse
				segment.time.push({"P":new_Ls,"F":0})
				segment.sound.push({note:-4,diesis:true,A_pos:[],A_neg:[]})
			}else{
				segment.time[segment.time.length-1].P=inter_position
				segment.time.push({"P":new_Ls,"F":0})
				segment.sound[segment.sound.length-1].note=-1
				segment.sound.push({note:-4,diesis:true,A_pos:[],A_neg:[]})
			}
		}
	}
	if(new_Ls<old_Ls){
		//trim segment
		//determine fractioning roules
		//what is the last fraction involved
		let index_last_fraction_range=0
		//var position=0
		segment.fraction.find(fraction=>{
			if(fraction.stop>=new_Ls)return true
			index_last_fraction_range++
		})
		//eliminate the others
		segment.fraction.splice(index_last_fraction_range+1,segment.fraction.length-index_last_fraction_range-1)
		//eventually modificate/duplicate the last fractioning range
		let last_fraction = segment.fraction[index_last_fraction_range]
		let start_pulse = last_fraction.start
		let stop_pulse = last_fraction.stop
		let new_range = new_Ls-start_pulse
		let resto = new_range%last_fraction.N
		if(resto==0){
			//no need of new fractioning range
			//modify current fractioning range
			segment.fraction[index_last_fraction_range].stop=new_Ls
			//modify notes and pulses
			let index_cut=0 //simple bc resto ==0
			segment.time.find(time=>{
				if( time.P>=new_Ls)return true
				index_cut++
			})
			segment.time.splice(index_cut,segment.time.length-index_cut)
			segment.sound.splice(index_cut,segment.sound.length-index_cut)
			segment.time.push({"P":new_Ls,"F":0})
			segment.sound.push({note:-4,diesis:true,A_pos:[],A_neg:[]})
		}else{
			//modify current fractioning range
			let start_last_fraction_range=0
			if(new_range==resto){
				//old fractioning range is lost
				segment.fraction[index_last_fraction_range].N=1
				segment.fraction[index_last_fraction_range].D=1
				segment.fraction[index_last_fraction_range].stop=new_Ls
				start_last_fraction_range=start_pulse
			}else{
				segment.fraction[index_last_fraction_range].stop=(new_Ls-resto)
				//creating new fractioning range
				segment.fraction.push({"N":1,"D":1,"start":(new_Ls-resto),"stop":new_Ls})
				start_last_fraction_range=new_Ls-resto
			}
			//modify notes and pulses
			//clear all note and pulses of entire LAST fractioning range
			let index_cut=0 //OF STARTING LAST RANGE FORCEd 1/1
			segment.time.find(time=>{
				if(time.P>=start_last_fraction_range)return true
				index_cut++
			})
			segment.time.splice(index_cut,segment.time.length-index_cut)
			segment.sound.splice(index_cut,segment.sound.length-index_cut)
			segment.time.push({"P":start_last_fraction_range,"F":0})
			segment.sound.push({note:-1,diesis:true,A_pos:[],A_neg:[]})
			segment.time.push({"P":new_Ls,"F":0})
			segment.sound.push({note:-4,diesis:true,A_pos:[],A_neg:[]})
		}
	}
}

function DATA_change_Ls_segment(voice_id,segment_index,new_Ls){
	//if(!DEBUG_MODE)console.log("DATA change Ls segment")
	//new_Ls is relative to the voice (no absolute)
	let data = DATA_get_current_state_point(true)
	let extend_last_element=false
	data = DATA_calculate_change_Ls_segment(data,voice_id,segment_index,new_Ls,extend_last_element)
	if(data==false)return false
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
	return true
}

function DATA_calculate_change_Ls_segment(data,voice_id,segment_index,new_Ls,extend_last_element=true){
	let voice_data_mod = data.voice_data.find(voice=>{return voice.voice_id==voice_id})
	//calc min Ls
	let delta_Ls_absolute=DATA_calculate_minimum_Li()
	let delta_Ls = delta_Ls_absolute*voice_data_mod.neopulse.D/voice_data_mod.neopulse.N
	if((new_Ls%delta_Ls)!=0)return false//new Ls not compatible with this voice segment, redraw correct segment(s)
	let new_Ls_absolute = new_Ls*voice_data_mod.neopulse.N/voice_data_mod.neopulse.D
	let new_Li_absolute = Li+(new_Ls-voice_data_mod.data.segment_data[segment_index].time.slice(-1)[0].P)*voice_data_mod.neopulse.N/voice_data_mod.neopulse.D
	if(new_Li_absolute>max_Li){
		console.error("ERROR: max Li reached")
		return data
	}
	data.global_variables.Li = new_Li_absolute
	//recalculate data segment
	if(voice_data_mod.blocked){
		data.voice_data.forEach(voice=>{
			if(voice.blocked){
				//change segment
				if(voice.voice_id==voice_id){
					DATA_calculate_force_segment_data_Ls(voice.data.segment_data[segment_index],new_Ls_absolute*voice.neopulse.D/voice.neopulse.N,extend_last_element)
				}else{
					//never extend last
					DATA_calculate_force_segment_data_Ls(voice.data.segment_data[segment_index],new_Ls_absolute*voice.neopulse.D/voice.neopulse.N,false)
				}
				voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
			}else{
				//change Li
				DATA_calculate_force_voice_Li(voice,new_Li_absolute,false)
			}
		})
	}else{
		let extend_blocked=DATA_verify_blocked_voices_extend_last_segment(data)
		data.voice_data.forEach(voice=>{
			if(voice.voice_id==voice_id){
				//change segment
				DATA_calculate_force_segment_data_Ls(voice.data.segment_data[segment_index],new_Ls_absolute*voice.neopulse.D/voice.neopulse.N,extend_last_element)
				voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
			}else{
				//change Li
				DATA_calculate_force_voice_Li(voice,new_Li_absolute,extend_blocked)
			}
		})
	}
	return data
}

function DATA_merge_segment(voice_id,segment_index){
	//if(!DEBUG_MODE)console.log("DATA merge segment")
	let data = DATA_get_current_state_point(true)
	let current_voice_data = data.voice_data.find(item=>{
		return item.voice_id==voice_id
	})
	// console.log("implement this "+voice_id+" "+segment_index)
	let voice_list=[]
	if(current_voice_data.blocked){
		//merge everything is connected
		data.voice_data.forEach(item=>{
			if(item.blocked)voice_list.push(item)
		})
	}else{
		voice_list.push(current_voice_data)
	}
	voice_list.forEach(voice=>{
		let seg1=voice.data.segment_data[segment_index]
		let seg2=voice.data.segment_data[segment_index+1]
		//merge
		seg1.sound.pop()
		seg1.sound=seg1.sound.concat(seg2.sound)
		//recalculate ranges
		let fraction_index=seg1.fraction.length
		let prev_fraction_end=seg1.time[seg1.time.length-1].P
		seg1.fraction=seg1.fraction.concat(seg2.fraction)
		for(let i=fraction_index; i<seg1.fraction.length;i++){
			let range=seg1.fraction[i].stop-seg1.fraction[i].start
			seg1.fraction[i].start=prev_fraction_end
			prev_fraction_end+=range
			seg1.fraction[i].stop=prev_fraction_end
		}
		//if contact fraction are == merge!
		if(seg1.fraction[fraction_index-1].N==seg1.fraction[fraction_index].N && seg1.fraction[fraction_index-1].D==seg1.fraction[fraction_index].D){
			//merge
			seg1.fraction[fraction_index-1].stop=seg1.fraction[fraction_index].stop
			seg1.fraction.splice(fraction_index,1)
		}
		//time need to recalc
		let time_index=seg1.time.length
		let prev_segment_end=seg1.time[seg1.time.length-1].P
		seg2.time.shift()
		seg1.time=seg1.time.concat(seg2.time)
		for(let i=time_index; i<seg1.time.length;i++){
			seg1.time[i].P+=prev_segment_end
		}
		//eliminate second
		voice.data.segment_data.splice(segment_index+1,1)
		//midi
		voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
	})
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

//function find and clear data inside a segment and redraw
function DATA_clear_segment(voice_id,segment_index){
	let data = DATA_get_current_state_point(true)
	let voice = data.voice_data.find(item=>{
		return item.voice_id==voice_id
	})
	let segment_data = voice.data.segment_data[segment_index]
	//reset fractioning
	let frac = segment_data.fraction.pop()
	frac.N=1
	frac.D=1
	frac.start=0
	segment_data.fraction=[]
	segment_data.fraction[0]=frac
	//reset sound
	segment_data.sound=[]
	segment_data.sound[0]={note:-1,diesis:true,A_pos:[],A_neg:[]}
	segment_data.sound[1]={note:-4,diesis:true,A_pos:[],A_neg:[]}
	//reset time
	segment_data.time=[]
	segment_data.time[0]={"P":0,"F":0}
	segment_data.time[1]={"P":frac.stop,"F":0}
	//reset name
	//midi data
	voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_break_segment(voice_id,segment_index, pulse){
	let data = DATA_get_current_state_point(true)
	let voice_data = data.voice_data.find(item=>{
		return item.voice_id==voice_id
	})
	//calculating absolute pulse
	let abs_pulse= pulse * voice_data.neopulse.N/voice_data.neopulse.D
	let voices_data_list=[]
	if(voice_data.blocked){
		//find all voice datas
		data.voice_data.forEach(item=>{
			if(item.blocked)voices_data_list.push(item)
		})
	}else{
		voices_data_list[0]=voice_data
	}
	voices_data_list.forEach(voice=>{
		//create a copy of degment data and inserting it in next position
		let position = (abs_pulse*voice.neopulse.D/voice.neopulse.N)
		let position_index = voice.data.segment_data[segment_index].time.indexOf(voice.data.segment_data[segment_index].time.find(time=>{return time.P==position && time.F==0}))
		if(position_index==-1){
			//need to add an event in that position
			voice.data.segment_data[segment_index].time.find(time=>{
				let pulse =  time.P
				position_index++
				if(pulse>=position){//>= bc it didnt found it and it can be a fraction up
					voice.data.segment_data[segment_index].time.splice(position_index,0,{"P":position,"F":0})
					voice.data.segment_data[segment_index].sound.splice(position_index,0,{note:-1,diesis:true,A_pos:[],A_neg:[]})
					return true
				}
			})
		}
		let new_segment = JSON.parse(JSON.stringify(voice.data.segment_data[segment_index]))
		//cut segment at position
		//first segment
		let eliminate_s1 = voice.data.segment_data[segment_index].time.length-1-position_index //XXX if posiiton index = -1 ???
		voice.data.segment_data[segment_index].time.splice(position_index+1,eliminate_s1)
		voice.data.segment_data[segment_index].sound.splice(position_index+1,eliminate_s1)
		voice.data.segment_data[segment_index].sound[position_index]={note:-4,diesis:true,A_pos:[],A_neg:[]}
		//fractioning
		let fraction_index=-1
		voice.data.segment_data[segment_index].fraction.find(fraction=>{
			fraction_index++
			return fraction.stop>=position
		})
		voice.data.segment_data[segment_index].fraction.splice(fraction_index+1,voice.data.segment_data[segment_index].fraction.length-1-fraction_index)
		//verify last is == Ls
		voice.data.segment_data[segment_index].fraction[voice.data.segment_data[segment_index].fraction.length-1].stop=position
		//second segment
		new_segment.time.splice(0,position_index)
		new_segment.sound.splice(0,position_index)
		//verify first note is not a L
		if(new_segment.sound[0].note==-3)new_segment.sound[0].note=-1
		//recalculate pulses
		let s2_time=[]
		new_segment.time.forEach(time=>{
			s2_time.push({"P":time.P-position,"F":time.F})
		})
		new_segment.time=s2_time
		//fractioning
		fraction_index=-1
		new_segment.fraction.find(fraction=>{
			fraction_index++
			return fraction.stop>position
		})
		new_segment.fraction.splice(0,fraction_index)
		new_segment.fraction[0].start=position
		//recalculate fraction ranges
		let current_p=0
		for(var i=0; i<new_segment.fraction.length; i++){
			new_segment.fraction[i].start=new_segment.fraction[i].start-position
			new_segment.fraction[i].stop=new_segment.fraction[i].stop-position
		}
		voice.data.segment_data.splice(segment_index+1,0,new_segment)
		voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
	})
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_calculate_segment_break_in_object_index(voice_id,segment_index,abs_position){
	//already checked if it is a non fractioned pulse
	let data = DATA_get_current_state_point(false)
	let voice_data = data.voice_data.find(voice=>{
		return voice.voice_id==voice_id
	})
	let position = abs_position
	let all_segment_data= voice_data.data.segment_data
	if(segment_index==null){
		//ABSOLUTE position
		//need to calculate segment number and relative position
		segment_index=0
		let prev_segments_n_obj = 0
		all_segment_data.find(segment=>{
			let n_obj = segment.time.length -1
			if(prev_segments_n_obj+n_obj>abs_position){
				return true
			}else{
				//next segment
				prev_segments_n_obj+=n_obj //last element of a segment is .
				segment_index++
				return false
			}
		})
		position = abs_position - prev_segments_n_obj
	}
	let current_segment_data=all_segment_data[segment_index]
	if(current_segment_data.time[position].F!=0)return false
	let pulse = current_segment_data.time[position].P
	let pulse_abs=pulse*voice_data.neopulse.N/voice_data.neopulse.D
	//verify if divisible by min Ls
	let min_Ls_abs = DATA_calculate_minimum_Li()
	if(pulse_abs%min_Ls_abs!=0)return false
	if(voice_data.blocked){
		//if voice blocked verify fraccioning
		//find every fraction range
		let not_breakable = data.voice_data.some(voice=>{
			if(voice.blocked){
				//find current segment/fractioning range
				let current_pulse= pulse_abs*voice.neopulse.D/voice.neopulse.N
				let current_fraction=voice.data.segment_data[segment_index].fraction.find(fraction=>{return fraction.start<=current_pulse && fraction.stop>current_pulse})
				//verify if current pulse exist in every fractioning range
				let delta = current_pulse-current_fraction.start
				if(delta%current_fraction.N!=0){
					//not breakable
					//stop the search!!!
					return true
				}
			}
		})
		if(not_breakable){
			return false
		}else{
			return true
		}
	}else{
		//if voice unblocked ok
		return true
	}
	return false
}

function DATA_copy_segment(voice_id,segment_index){
	//verify if voice is blocked
	let data = DATA_get_current_state_point(true)
	let voice = data.voice_data.find(item=>{
		return item.voice_id==voice_id
	})
	//selected segment
	let segment_data=voice.data.segment_data[segment_index]
	let Psg_segment_start=voice.data.segment_data.reduce((prop,item,index)=>{return (index<segment_index)?item.time.slice(-1)[0].P+prop:prop},0)
	copied_data = {id:"segment",data:[{voice_id:voice.voice_id,neopulse:voice.neopulse, Psg_segment_start:Psg_segment_start, segment: segment_data}]}
}

function DATA_paste_segment_all(voice_id,segment_index){
	if(copied_data.id!="segment"){
		return
	}
	let data = DATA_get_current_state_point(true)
	DATA_calculate_paste_segment_all(data,voice_id,segment_index)
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_calculate_paste_segment_all(data,voice_id,segment_index){
	let voice = data.voice_data.find(item=>{
		return item.voice_id==voice_id
	})
	//selected segment
	let current_segment_data=voice.data.segment_data[segment_index]
	//verify if is possible...
	//if segment Ls> and not void
	let current_copied_data=JSON.parse(JSON.stringify(copied_data.data[0]))
	let current_ls = voice.data.segment_data[segment_index].time.slice(-1)[0].P
	let copied_ls = current_copied_data.segment.time.slice(-1)[0].P
	if(current_ls==copied_ls){
		//segments are same length, proceed to copy data
		voice.data.segment_data[segment_index]=current_copied_data.segment
		//recalculate midi time and notes
		DATA_verify_voice_A(voice,false)
		voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
	}else{
		//different
		if(current_ls>copied_ls){
			//segment bigger
			//this function will create a segment that start with the copied one and continue with Fr 1/1 to the end of current segment
			let last_index = current_copied_data.segment.time.length-1
			//copy data
			voice.data.segment_data[segment_index]=JSON.parse(JSON.stringify(current_copied_data.segment))
			//add silence last-1 item
			voice.data.segment_data[segment_index].sound.splice(last_index,0,{note:-1,diesis:true,A_pos:[],A_neg:[]})
			//add last pulse
			voice.data.segment_data[segment_index].time.push({"P":current_ls,"F":0})
			//add/change fractioning
			let last_fraction = current_copied_data.segment.fraction.slice(-1)[0]
			if(last_fraction.N==1 && last_fraction.D==1){
				//modify last fraction interval
				voice.data.segment_data[segment_index].fraction[current_copied_data.segment.fraction.length-1].stop=current_ls
			}else{
				//add new fraction interval
				voice.data.segment_data[segment_index].fraction.push({"N":1,"D":1,"start":copied_ls,"stop":current_ls})
			}
			//recalculate midi time and notes
			DATA_verify_voice_A(voice,false)
			voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
		}else{
			//short
			//make the segment bigger
			let min_delta_Ls_abs = DATA_calculate_minimum_Li()
			let delta_abs = (copied_ls-current_ls)*voice.neopulse.N/voice.neopulse.D
			let resto_abs = delta_abs%min_delta_Ls_abs
			//check compatibility with NP
			let wanted_Ls_abs = 0
			if(resto_abs==0){
				//i can change the Li of the segment with the copied one
				wanted_Ls_abs= copied_ls*voice.neopulse.N/voice.neopulse.D
			}else{
				//need to add some more pulses
				wanted_Ls_abs= copied_ls*voice.neopulse.N/voice.neopulse.D + (min_delta_Ls_abs-resto_abs)
				delta_abs=wanted_Ls_abs-(current_ls*voice.neopulse.N/voice.neopulse.D)
			}
			//change Li
			data.global_variables.Li+=delta_abs
			if(data.global_variables.Li>max_Li){
				console.error("ERROR copy segment, limit Li reached")
				return
			}
			//check segment blocked
			if(voice.blocked){
				data.voice_data.forEach(item=>{
					if(item.blocked){
						//change Ls all blocked voices
						let end_pulse=wanted_Ls_abs*item.neopulse.D/item.neopulse.N
						item.data.segment_data[segment_index].time.push({"P":end_pulse,"F":0})
						item.data.segment_data[segment_index].sound.splice(item.data.segment_data[segment_index].sound.length-1,0,{note:-1,diesis:true,A_pos:[],A_neg:[]})
						//if contact fraction are == merge!
						let fraction_list = item.data.segment_data[segment_index].fraction
						if(fraction_list[fraction_list.length-1].N==1 && fraction_list[fraction_list.length-1].D==1){
							//merge-expand range
							fraction_list[fraction_list.length-1].stop=end_pulse
						}else{
							//add
							fraction_list[fraction_list.length]={"N":1,"D":1,"start":fraction_list[fraction_list.length-1].stop,"stop":end_pulse}
						}
					}else{
						//add-extend segment at the end
						let last_segment = item.data.segment_data[item.data.segment_data.length-1]
						if(last_segment.sound.length==2 && last_segment.sound[0].note==-2 && last_segment.fraction.length==1 && last_segment.fraction[0].N==1 && last_segment.fraction[0].D==1){
							let end_pulse=last_segment.time[1].P+delta_abs*item.neopulse.D/item.neopulse.N
							item.data.segment_data[item.data.segment_data.length-1].time[1].P=end_pulse
							item.data.segment_data[item.data.segment_data.length-1].fraction[0].stop=end_pulse
						}else{
							let end_pulse=delta_abs*item.neopulse.D/item.neopulse.N
							let f = []
							f[0]={"N":1,"D":1,"start":0,"stop":end_pulse}
							let new_segment={"time": [{"P":0,"F":0},{"P":end_pulse,"F":0}], "sound":[{note:-2,diesis:true,A_pos:[],A_neg:[]},{note:-4,diesis:true,A_pos:[],A_neg:[]}],"fraction":f,"segment_name":""}
							item.data.segment_data[item.data.segment_data.length]=new_segment
						}
					}
				})
				//copy segment
				current_ls = voice.data.segment_data[segment_index].time.slice(-1)[0].P
				if(current_ls==copied_ls){
					voice.data.segment_data[segment_index]=current_copied_data.segment
				}else{
					//enter here if segment copied from other voice with a different neopulse not compatible with current ((current_ls=copied Ls+extra_compatibility_pulses)> copied LS)
					let old_last_pulse_index = current_copied_data.segment.time.length-1
					//copy data
					voice.data.segment_data[segment_index]=JSON.parse(JSON.stringify(current_copied_data.segment))
					//add silence last-1 item
					voice.data.segment_data[segment_index].sound.splice(old_last_pulse_index,0,{note:-1,diesis:true,A_pos:[],A_neg:[]})
					//add last pulse
					voice.data.segment_data[segment_index].time.push({"P":current_ls,"F":0})
					//add/change fractioning
					let last_fraction = current_copied_data.segment.fraction.slice(-1)[0]
					if(last_fraction.N==1 && last_fraction.D==1){
						//modify last fraction interval
						voice.data.segment_data[segment_index].fraction[current_copied_data.segment.fraction.length-1].stop=current_ls
					}else{
						//add new fraction interval
						voice.data.segment_data[segment_index].fraction.push({"N":1,"D":1,"start":copied_ls,"stop":copied_ls})
					}
				}
				data.voice_data.forEach(item=>{
					//midi data
					DATA_verify_voice_A(item,false)
					item.data.midi_data=DATA_segment_data_to_midi_data(item.data.segment_data,item.neopulse)
				})
			}else{
				//copy segment after changing its length
				//change Ls
				let end_pulse=wanted_Ls_abs*voice.neopulse.D/voice.neopulse.N
				if(end_pulse==copied_ls){
					//can copy exactly
					voice.data.segment_data[segment_index]=current_copied_data.segment
				}else{
					//need to add a silence and possibly a fraction range
					let old_last_pulse_index = current_copied_data.segment.time.length-1
					//copy data
					voice.data.segment_data[segment_index]=JSON.parse(JSON.stringify(current_copied_data.segment))
					//add silence last-1 item
					voice.data.segment_data[segment_index].sound.splice(old_last_pulse_index,0,{note:-1,diesis:true,A_pos:[],A_neg:[]})
					//add last pulse
					voice.data.segment_data[segment_index].time.push({"P":end_pulse,"F":0})
					//add/change fractioning
					let last_fraction = current_copied_data.segment.fraction.slice(-1)[0]
					if(last_fraction.N==1 && last_fraction.D==1){
						//modify last fraction interval
						voice.data.segment_data[segment_index].fraction[current_copied_data.segment.fraction.length-1].stop=end_pulse
					}else{
						//add new fraction interval
						voice.data.segment_data[segment_index].fraction.push({"N":1,"D":1,"start":copied_ls,"stop":copied_ls})
					}
				}
				//change other voices
				data.voice_data.forEach(item=>{
					if(item.voice_id!=voice_id){
						//XXX USE FUNCTION CALCULATE VOICE LI???
						//DATA_calculate_force_voice_Li(voice,new_Li_absolute,false)
						//add-extend segment at the end
						let last_segment = item.data.segment_data[item.data.segment_data.length-1]
						if(last_segment.sound.length==2 && last_segment.sound[0].note==-2 && last_segment.fraction.length==1 && last_segment.fraction[0].N==1 && last_segment.fraction[0].D==1){
							let end_pulse=last_segment.time[1].P+delta_abs*item.neopulse.D/item.neopulse.N
							item.data.segment_data[item.data.segment_data.length-1].time[1].P=end_pulse
							item.data.segment_data[item.data.segment_data.length-1].fraction[0].stop=end_pulse
						}else{
							let end_pulse=delta_abs*item.neopulse.D/item.neopulse.N
							let f = []
							f[0]={"N":1,"D":1,"start":0,"stop":end_pulse}
							let new_segment={"time": [{"P":0,"F":0},{"P":end_pulse,"F":0}], "sound":[{note:-2,diesis:true,A_pos:[],A_neg:[]},{note:-4,diesis:true,A_pos:[],A_neg:[]}],"fraction":f,"segment_name":""}
							item.data.segment_data[item.data.segment_data.length]=new_segment
						}
					}
					DATA_verify_voice_A(item,false)
					item.data.midi_data=DATA_segment_data_to_midi_data(item.data.segment_data,item.neopulse)
				})
			}
		}
	}
}

function DATA_paste_segment_all_grade(voice_id,segment_index){
	let data = DATA_get_current_state_point(true)
	//verify if voice index !=null
	//let data_changed=false
	let outside_range=false
	let paste_success=true
	let scale_sequence= data.scale.scale_sequence
	if(scale_sequence.length==0){
		DATA_paste_segment_all(voice_id,segment_index)
		return
	}
	if(voice_id!=null){
		//single segment
		if(copied_data.id!="segment"){
			return
		}
		//use instead
		let current_voice_data = data.voice_data.find(item=>{return item.voice_id==voice_id})
		if(current_voice_data==null){
			console.error("Paste error, data not found")
			return false
		}

		// if(note_array_copy.length==1 && note_array_copy[0]==-1)note_array_copy[0]=-2
		DATA_calculate_paste_segment_all(data,voice_id,segment_index)
		//replace with grades
		let current_copied_data=JSON.parse(JSON.stringify(copied_data.data[0]))
		let result=DATA_calculate_paste_segment_grade(data,segment_index,current_voice_data,current_copied_data,scale_sequence)
		if(result.outside_range)outside_range=true
		//if(result.data_changed)data_changed=true //not using this bc first Paste segment all...
		if(!result.paste_success)paste_success=false
		DATA_verify_voice_A(current_voice_data,false)
		current_voice_data.data.midi_data=DATA_segment_data_to_midi_data(current_voice_data.data.segment_data,current_voice_data.neopulse)
	}else{
		//column
		if(copied_data.id!="column"){
			return
		}
		//verify if copied data is compatible (same list of blocked voices and in correct order)
		let blocked_voices_list= data.voice_data.filter(item=>{
			return item.blocked
		})
		let compatible=true
		if (blocked_voices_list.length==copied_data.data.length){
			for (let i = 0; i < blocked_voices_list.length; i++) {
				if(blocked_voices_list[i].voice_id!=copied_data.data[i].voice_id)compatible=false
				if(blocked_voices_list[i].neopulse.N!=copied_data.data[i].neopulse.N)compatible=false
				if(blocked_voices_list[i].neopulse.D!=copied_data.data[i].neopulse.D)compatible=false
			}
		}else{
			compatible=false
		}
		if(!compatible){
			console.error("COPIED DATA NON COMPATIBLE")
			return
		}
		//COPY TIMES
		paste_success=DATA_calculate_paste_column_segment_all(data,segment_index) //XXX XXX XXX XXX
		//replace note with grades
		data.voice_data.forEach(current_voice_data=>{
			if(current_voice_data.blocked){
				let current_copied_data = JSON.parse(JSON.stringify(copied_data.data.find(data=>{return data.voice_id==current_voice_data.voice_id})))
				let result=DATA_calculate_paste_segment_grade(data,segment_index,current_voice_data,current_copied_data,scale_sequence)
				DATA_verify_voice_A(current_voice_data,false)
				if(result.outside_range)outside_range=true
				//if(result.data_changed)data_changed=true
				if(!result.paste_success)paste_success=false
			}
		})
	}
	if(outside_range){
		APP_info_msg("out_N_range")
	}
	if (!paste_success) {
		console.error("Paste error")
		return false
	}
	// if (!data_changed) {
	// 	console.error("Paste: nothing to do")
	// 	return false
	// }
	if (!paste_success) {
		console.error("Paste error")
		return false
	}
	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_paste_segment_note(voice_id,segment_index){
	let data = DATA_get_current_state_point(true)
	//verify if voice index !=null
	let data_changed=false
	let paste_success=true
	if(voice_id!=null){
		//single segment
		if(copied_data.id!="segment"){
			return
		}
		//use instead
		let current_voice_data = data.voice_data.find(item=>{return item.voice_id==voice_id})
		if(current_voice_data==null){
			console.error("Paste error, data not found")
			return false
		}
		let current_copied_data=JSON.parse(JSON.stringify(copied_data.data[0]))
		let results = DATA_calculate_paste_segment_note(data,current_voice_data,segment_index,current_copied_data)
		if(results.data_changed)data_changed=true
		if(!results.paste_success)paste_success=false
	}else{
		//column
		if(copied_data.id!="column"){
			return
		}
		//verify if copied data is compatible (same list of blocked voices and in correct order)
		let blocked_voices_list= data.voice_data.filter(item=>{
			return item.blocked
		})
		let compatible=true
		if (blocked_voices_list.length==copied_data.data.length){
			for (let i = 0; i < blocked_voices_list.length; i++) {
				if(blocked_voices_list[i].voice_id!=copied_data.data[i].voice_id)compatible=false
				//here no need to be so restrictive
				//if(blocked_voices_list[i].neopulse.N!=copied_data.data[i].neopulse.N)compatible=false
				//if(blocked_voices_list[i].neopulse.D!=copied_data.data[i].neopulse.D)compatible=false
			}
		}else{
			compatible=false
		}
		if(!compatible){
			console.error("COPIED DATA NON COMPATIBLE")
			return
		}
		data.voice_data.forEach(current_voice_data=>{
			if(current_voice_data.blocked){
				let current_copied_data = JSON.parse(JSON.stringify(copied_data.data.find(data=>{return data.voice_id==current_voice_data.voice_id})))
				let results = DATA_calculate_paste_segment_note(data,current_voice_data,segment_index,current_copied_data)
				if(results.data_changed)data_changed=true
				if(!results.paste_success)paste_success=false
			}
		})
	}
	if (!paste_success) {
		console.error("Paste error")
		return false
	}
	if (!data_changed) {
		console.error("Paste: nothing to do")
		return false
	}
	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_calculate_paste_segment_note(data,current_voice_data,segment_index,current_copied_data){
	let sound_array=current_copied_data.segment.sound.slice()
	sound_array.pop()
	let sound_array_copy=JSON.stringify(current_voice_data.data.segment_data[segment_index].sound)
	//clear notes
	current_voice_data.data.segment_data[segment_index].sound.forEach(item=>{
		item.note=-2
		item.diesis=true
		item.A_pos=[]
		item.A_neg=[]
	})
	sound_array.forEach(item=>{
		item.diesis=true
	})
	current_voice_data.data.segment_data[segment_index].sound[current_voice_data.data.segment_data[segment_index].sound.length-1]={note:-4,diesis:true,A_pos:[],A_neg:[]}
	let paste_success=DATA_calculate_insert_sound_array_segment_data(data,current_voice_data.voice_id,segment_index,sound_array,true,true,true,true,false)
	let data_changed=(sound_array_copy==JSON.stringify(current_voice_data.data.segment_data[segment_index].sound))?false:true
	DATA_verify_voice_A(current_voice_data,false)
	current_voice_data.data.midi_data=DATA_segment_data_to_midi_data(current_voice_data.data.segment_data,current_voice_data.neopulse)
	let results={paste_success,data_changed}
	return results
}

function DATA_paste_segment_grade(voice_id,segment_index){
	let data = DATA_get_current_state_point(true)
	//verify if voice index !=null
	let data_changed=false
	let outside_range=false
	let paste_success=true
	let scale_sequence= data.scale.scale_sequence
	if(scale_sequence.length==0){
		DATA_paste_segment_note(voice_id,segment_index)
		return
	}
	if(voice_id!=null){
		//single segment
		if(copied_data.id!="segment"){
			return
		}
		let current_voice_data = data.voice_data.find(item=>{return item.voice_id==voice_id})
		if(current_voice_data==null){
			console.error("Paste error, data not found")
			return false
		}
		let current_copied_data=JSON.parse(JSON.stringify(copied_data.data[0]))
		let result=DATA_calculate_paste_segment_grade(data,segment_index,current_voice_data,current_copied_data,scale_sequence)
		DATA_verify_voice_A(current_voice_data,false)
		if(result.outside_range)outside_range=true
		if(result.data_changed)data_changed=true
		if(!result.paste_success)paste_success=false
	}else{
		//column
		if(copied_data.id!="column"){
			return
		}
		//verify if copied data is compatible (same list of blocked voices and in correct order)
		let blocked_voices_list= data.voice_data.filter(item=>{
			return item.blocked
		})
		let compatible=true
		if (blocked_voices_list.length==copied_data.data.length){
			for (let i = 0; i < blocked_voices_list.length; i++) {
				if(blocked_voices_list[i].voice_id!=copied_data.data[i].voice_id)compatible=false
				//here no need to be so restrictive
				//if(blocked_voices_list[i].neopulse.N!=copied_data.data[i].neopulse.N)compatible=false
				//if(blocked_voices_list[i].neopulse.D!=copied_data.data[i].neopulse.D)compatible=false
			}
		}else{
			compatible=false
		}
		if(!compatible){
			console.error("COPIED DATA NON COMPATIBLE")
			return
		}
		data.voice_data.forEach(current_voice_data=>{
			if(current_voice_data.blocked){
				let current_copied_data = JSON.parse(JSON.stringify(copied_data.data.find(data=>{return data.voice_id==current_voice_data.voice_id})))
				let result=DATA_calculate_paste_segment_grade(data,segment_index,current_voice_data,current_copied_data,scale_sequence)
				DATA_verify_voice_A(current_voice_data,false)
				if(result.outside_range)outside_range=true
				if(result.data_changed)data_changed=true
				if(!result.paste_success)paste_success=false
			}
		})
	}
	if(outside_range){
		APP_info_msg("out_N_range")
	}
	if (!paste_success) {
		console.error("Paste error")
		return false
	}
	if (!data_changed) {
		console.error("Paste: nothing to do")
		return false
	}
	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_calculate_paste_segment_grade(data,segment_index,current_voice_data,current_copied_data,scale_sequence){
	//transform to Ng_array original segment
	//if undefined nothing to copy
	let outside_range=false
	let paste_success=true
	let data_changed=false
	let origin_scale_list = DATA_calculate_origin_scale_list(current_copied_data.segment,current_copied_data.Psg_segment_start,current_copied_data.neopulse,scale_sequence)
	let insert_length = origin_scale_list.length
	if(insert_length!=0){
		let sound_array=[]
		let target_scale_list = DATA_calculate_insert_scale_list(data,current_voice_data.voice_id,segment_index,insert_length,true,true,true,true)
		let idea_scale_list=DATA_get_idea_scale_list()
		current_copied_data.segment.sound.forEach((sound,index)=>{
			if(sound.note<0){
				if(sound.note==-4)return
				sound_array.push({note:sound.note,diesis:true,A_pos:[],A_neg:[]})
			}else{
				//conversion to new scale (grade/reg no delta)
				let result=DATA_calculate_sound_translation_scale(sound,origin_scale_list[index],target_scale_list[index],idea_scale_list,true)
				if(result.outside_range)outside_range=true
				sound_array.push(result.sound)
			}
		})
		let sound_array_copy=JSON.stringify(current_voice_data.data.segment_data[segment_index].sound)
		paste_success=DATA_calculate_insert_sound_array_segment_data(data,current_voice_data.voice_id,segment_index,sound_array,true,true,true,true,false)
		data_changed=(sound_array_copy==JSON.stringify(current_voice_data.data.segment_data[segment_index].sound))?false:true
		DATA_verify_voice_A(current_voice_data,false)
		current_voice_data.data.midi_data=DATA_segment_data_to_midi_data(current_voice_data.data.segment_data,current_voice_data.neopulse)
	}
	let result={paste_success,data_changed,outside_range}
	return result
}

function DATA_paste_segment_time(voice_id,segment_index){
	let data = DATA_get_current_state_point(true)
	//verify if voice index !=null
	let paste_success=true
	if(voice_id!=null){
		//single segment
		if(copied_data.id!="segment"){
			return
		}
		//use instead
		let current_voice_data = data.voice_data.find(item=>{return item.voice_id==voice_id})
		if(current_voice_data==null){
			console.error("Paste error, data not found")
			return false
		}
		//copy original notes
		let sound_array_copy=JSON.parse(JSON.stringify(current_voice_data.data.segment_data[segment_index].sound))
		sound_array_copy.pop()
		if(sound_array_copy.length==1 && sound_array_copy[0].note==-1)sound_array_copy[0].note=-2
		DATA_calculate_paste_segment_all(data,voice_id,segment_index)
		//replace with old values
		DATA_calculate_replace_sound_array_segment_data(current_voice_data,segment_index,sound_array_copy)
		DATA_verify_voice_A(current_voice_data,false)
		current_voice_data.data.midi_data=DATA_segment_data_to_midi_data(current_voice_data.data.segment_data,current_voice_data.neopulse)
	}else{
		//column
		if(copied_data.id!="column"){
			return
		}
		//verify if copied data is compatible (same list of blocked voices and in correct order)
		let blocked_voices_list= data.voice_data.filter(item=>{
			return item.blocked
		})
		let compatible=true
		if (blocked_voices_list.length==copied_data.data.length){
			for (let i = 0; i < blocked_voices_list.length; i++) {
				if(blocked_voices_list[i].voice_id!=copied_data.data[i].voice_id)compatible=false
				if(blocked_voices_list[i].neopulse.N!=copied_data.data[i].neopulse.N)compatible=false
				if(blocked_voices_list[i].neopulse.D!=copied_data.data[i].neopulse.D)compatible=false
			}
		}else{
			compatible=false
		}
		if(!compatible){
			console.error("COPIED DATA NON COMPATIBLE")
			return
		}
		//copy original notes
		let sound_array_copy=[]
		data.voice_data.forEach(current_voice_data=>{
			if(current_voice_data.blocked){
				sound_array=JSON.parse(JSON.stringify(current_voice_data.data.segment_data[segment_index].sound))
				sound_array.pop()
				if(sound_array.length==1 && sound_array[0].note==-1)sound_array[0].note=-2
				sound_array_copy.push(sound_array)
			}
		})
		paste_success=DATA_calculate_paste_column_segment_all(data,segment_index)
		//replace with old values
		let index=0
		data.voice_data.forEach(current_voice_data=>{
			if(current_voice_data.blocked){
				DATA_calculate_replace_sound_array_segment_data(current_voice_data,segment_index,sound_array_copy[index])
				index++
				DATA_verify_voice_A(current_voice_data,false)
				current_voice_data.data.midi_data=DATA_segment_data_to_midi_data(current_voice_data.data.segment_data,current_voice_data.neopulse)
			}
		})
	}
	if (!paste_success) {
		console.error("Paste error")
		return false
	}
	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_calculate_replace_sound_array_segment_data(current_voice_data,segment_index,sound_array){
	//clear/restore note
	let sound=current_voice_data.data.segment_data[segment_index].sound
	sound.forEach((s,i)=>{
		//paste original note seq
		if(sound_array[i]!=null){
			sound[i]=sound_array[i]
		}else{
			//clear
			//note[i]=-2;diesis[i]=true
			sound[i]={note:-2,diesis:true,A_pos:[],A_neg:[]}
		}
	})
	sound[sound.length-1]={note:-4,diesis:true,A_pos:[],A_neg:[]}
}

function DATA_paste_segment_fraction(voice_id,segment_index){
	let data = DATA_get_current_state_point(true)
	//verify if voice index !=null
	let paste_success=true
	if(voice_id!=null){
		//single segment
		if(copied_data.id!="segment"){
			return
		}
		//use instead
		let current_voice_data = data.voice_data.find(item=>{return item.voice_id==voice_id})
		if(current_voice_data==null){
			console.error("Paste error, data not found")
			return false
		}
		//copy original notes
		let sound_array_copy=JSON.parse(JSON.stringify(current_voice_data.data.segment_data[segment_index].sound))
		sound_array_copy.pop()
		//adding notes to empty segment
		if(sound_array_copy.length==1 && sound_array_copy[0].note==-1)sound_array_copy[0].note=-2
		//take care of Li and segments connections
		DATA_calculate_paste_segment_all(data,voice_id,segment_index)
		//clean up non fraction pulses
		current_voice_data.data.segment_data[segment_index].sound=[{note:-2,diesis:true,A_pos:[],A_neg:[]}]
		current_voice_data.data.segment_data[segment_index].time=[{P:0,F:0}]
		current_voice_data.data.segment_data[segment_index].fraction.forEach(fr_range=>{
			current_voice_data.data.segment_data[segment_index].sound.push({note:-2,diesis:true,A_pos:[],A_neg:[]})
			current_voice_data.data.segment_data[segment_index].time.push({P:fr_range.stop,F:0})
		})
		//replace with old values
		DATA_calculate_replace_sound_array_segment_data(current_voice_data,segment_index,sound_array_copy)
		DATA_verify_voice_A(current_voice_data,false)
		current_voice_data.data.midi_data=DATA_segment_data_to_midi_data(current_voice_data.data.segment_data,current_voice_data.neopulse)
	}else{
		//column
		if(copied_data.id!="column"){
			return
		}
		//verify if copied data is compatible (same list of blocked voices and in correct order)
		let blocked_voices_list= data.voice_data.filter(item=>{
			return item.blocked
		})
		let compatible=true
		if (blocked_voices_list.length==copied_data.data.length){
			for (let i = 0; i < blocked_voices_list.length; i++) {
				if(blocked_voices_list[i].voice_id!=copied_data.data[i].voice_id)compatible=false
				if(blocked_voices_list[i].neopulse.N!=copied_data.data[i].neopulse.N)compatible=false
				if(blocked_voices_list[i].neopulse.D!=copied_data.data[i].neopulse.D)compatible=false
			}
		}else{
			compatible=false
		}
		if(!compatible){
			console.error("COPIED DATA NON COMPATIBLE")
			return
		}
		//copy original notes
		let sound_array_copy=[]
		data.voice_data.forEach(current_voice_data=>{
			if(current_voice_data.blocked){
				sound_array=JSON.parse(JSON.stringify(current_voice_data.data.segment_data[segment_index].sound))
				sound_array.pop()
				if(sound_array.length==1 && sound_array[0].note==-1)sound_array[0].note=-2
				sound_array_copy.push(sound_array)
			}
		})
		paste_success=DATA_calculate_paste_column_segment_all(data,segment_index)
		//replace with old values
		let index=0
		data.voice_data.forEach(current_voice_data=>{
			if(current_voice_data.blocked){
				//clean up non fraction pulses
				current_voice_data.data.segment_data[segment_index].sound=[{note:-2,diesis:true,A_pos:[],A_neg:[]}]
				current_voice_data.data.segment_data[segment_index].time=[{P:0,F:0}]
				current_voice_data.data.segment_data[segment_index].fraction.forEach(fr_range=>{
					current_voice_data.data.segment_data[segment_index].sound.push({note:-2,diesis:true,A_pos:[],A_neg:[]})
					current_voice_data.data.segment_data[segment_index].time.push({P:fr_range.stop,F:0})
				})
				DATA_calculate_replace_sound_array_segment_data(current_voice_data,segment_index,sound_array_copy[index])
				index++
				DATA_verify_voice_A(current_voice_data,false)
				current_voice_data.data.midi_data=DATA_segment_data_to_midi_data(current_voice_data.data.segment_data,current_voice_data.neopulse)
			}
		})
	}
	if (!paste_success) {
		console.error("Paste error")
		return false
	}
	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

//FRACTIONS
function DATA_modify_fraction_value(voice_id,segment_index,abs_position,fractComplex,fractSimple,fraction_number,voice_data){
	//return false if fraction value is not possible
	//return new voice data if ok
	let data = DATA_get_current_state_point(true)
	let position = abs_position
	let voice_data_mod = data.voice_data.find(item=>{
		return item.voice_id==voice_id
	})
	let all_segment_data= voice_data_mod.data.segment_data
	if(segment_index==null){
		//absolute position
		//need to calculate segment number and relative position
		segment_index=0
		let prev_segments_n_obj = 0
		all_segment_data.find(segment=>{
			let n_obj = segment.fraction.length
			if(prev_segments_n_obj+n_obj>abs_position){
				return true
			}else{
				//next segment
				prev_segments_n_obj+=n_obj //last element of a segment is .
				segment_index++
				return false
			}
		})
		position = abs_position - prev_segments_n_obj
	}
	let current_segment_data=all_segment_data[segment_index]
	let current_fraction = current_segment_data.fraction[position]
	if(isNaN(fractComplex) || isNaN(fractSimple) || fractSimple == 0 || fractComplex == 0)return false
	if(fractSimple>29 || fractComplex>29 || fractComplex/fractSimple<0.1)return false
	let range = current_fraction.stop-current_fraction.start
	let resto =  range%fractComplex
	if(resto==0){
		//all ok rewrite value in database
		//Other Cases
		let old_fractComplex = current_fraction.N
		let old_fractSimple = current_fraction.D
		current_fraction.N=fractComplex
		current_fraction.D=fractSimple
		//modify database pulses/note
		let tot_iT = range*fractSimple/fractComplex
		let start_range_index = current_segment_data.time.indexOf(current_segment_data.time.find(time=>{return time.P==current_fraction.start && time.F==0}))
		let end_range_index = current_segment_data.time.indexOf(current_segment_data.time.find(time=>{return time.P==current_fraction.stop && time.F==0}))
		//case range is empty
		if(start_range_index==end_range_index-1 && (current_segment_data.sound[start_range_index].note==-1 || current_segment_data.sound[start_range_index].note==-2)){
			voice_data_mod.data.midi_data=DATA_segment_data_to_midi_data(all_segment_data,voice_data_mod.neopulse)
			DATA_insert_new_state_point(data)
			DATA_load_state_point_data(false,true)
			return true
		}
		//calculate old iTs
		let iT_list = []
		let sound_insert = []
		let prev_pulse=0
		let prev_fraction=0
		for (let i = start_range_index; i <= end_range_index ; i++) {
			let pulse = current_segment_data.time[i].P
			let fraction = current_segment_data.time[i].F
			//calculate the dT value to be written
			let value = (pulse - prev_pulse)  * old_fractSimple / old_fractComplex + (fraction - prev_fraction)
			iT_list.push(value)
			sound_insert.push(current_segment_data.sound[i])
			prev_pulse=pulse
			prev_fraction=fraction
		}
		//eliminate first iT
		iT_list.shift()
		//last element is "not part" or fractioning range
		//it is part of following range/segment/end of voice
		sound_insert.pop()
		//adjust it list to new fractioning range
		let sum = iT_list.reduce((prop,item)=>{return item+prop},0)
		if(tot_iT>sum){
			//add a last it
			iT_list.push(tot_iT-sum)
			//add a last s
			sound_insert.push({note:-1,diesis:true,A_pos:[],A_neg:[]})
		}else if(tot_iT<sum){
			//take out its
			let position = 0
			let new_iT_list = []
			iT_list.forEach(iT=>{
				if(tot_iT>position){
					let partial_iT=tot_iT-position
					position+=iT
					if(iT<=partial_iT){
						new_iT_list.push(iT)
					}else{
						new_iT_list.push(partial_iT)
					}
				}else{
					//outside
				}
			})
			iT_list = new_iT_list
			//eliminate extra notes
			sound_insert.splice(iT_list.length,(sound_insert.length-iT_list.length))
		}
		//recalculate pulse time with new fractioning and iT_list XXX
		prev_pulse = current_fraction.start
		prev_fraction = 0
		let time_insert = [{"P":current_fraction.start,"F":0}]
		iT_list.forEach(iT=>{
			let d_pulse = (Math.floor((prev_fraction+iT)/fractSimple))*fractComplex
			prev_pulse+=d_pulse
			prev_fraction = (prev_fraction+iT)%fractSimple
			time_insert.push({"P":prev_pulse,"F":prev_fraction})
		})
		//last element is "not part" or fractioning range
		//it is part of following range/segment/end of voice
		time_insert.pop()
		//Change original data
		current_segment_data.time.splice(start_range_index,end_range_index-start_range_index,...time_insert)
		current_segment_data.sound.splice(start_range_index,end_range_index-start_range_index,...sound_insert)
		//recalculate midi_data
		voice_data_mod.data.midi_data=DATA_segment_data_to_midi_data(all_segment_data,voice_data_mod.neopulse)
		DATA_insert_new_state_point(data)
		DATA_load_state_point_data(false,true)
		return true
	}else {
		//show error and restore old value
		return false
	}
}

function DATA_break_fraction_range(voice_id,segment_index,current_fraction_index,new_fraction_list){
	//new_fractions are the substitutes of fraction[current_fraction_index]
	let data = DATA_get_current_state_point(true)
	let index_voice_to_change = -1
	data.voice_data.some(voice=>{
		index_voice_to_change++
		return voice.voice_id==voice_id
	})
	//verify fraction(s)
	let fraction_verificated = true
	let current_fraction = data.voice_data[index_voice_to_change].data.segment_data[segment_index].fraction[current_fraction_index]
	let frac_N = current_fraction.N
	let frac_D = current_fraction.D
	if(current_fraction.start!=new_fraction_list[0].start || current_fraction.stop!=new_fraction_list.slice(-1)[0].stop)fraction_verificated=false
	for (let i = 0; i < new_fraction_list.length-1; i++) {
		if(new_fraction_list[i].stop!=new_fraction_list[i+1].start)fraction_verificated=false
	}
	for (let i = 0; i < new_fraction_list.length; i++) {
		if(new_fraction_list[i].D!=frac_D)fraction_verificated=false
		if(new_fraction_list[i].N!=frac_N)fraction_verificated=false
	}
	if(!fraction_verificated){
		console.error("ERROR BREAKING FRACTIONS")
		console.log(current_fraction)
		console.log(new_fraction_list)
		return
	}
	data.voice_data[index_voice_to_change].data.segment_data[segment_index].fraction.splice(current_fraction_index,1,...new_fraction_list)
	//add pulse(s) in time if doesn t exist
	let recalc_midi = false
	for (let i = 0; i < new_fraction_list.length-1; i++) {
		let new_pulse = new_fraction_list[i].stop
		let found = data.voice_data[index_voice_to_change].data.segment_data[segment_index].time.some(time=>{return time.P==new_pulse && time.F==0})
		if(!found){
			//insert
			let index = data.voice_data[index_voice_to_change].data.segment_data[segment_index].time.indexOf(data.voice_data[index_voice_to_change].data.segment_data[segment_index].time.find(time=>{return time.P>=new_pulse}))
			data.voice_data[index_voice_to_change].data.segment_data[segment_index].time.splice(index,0,{"P":new_pulse,"F":0})
			data.voice_data[index_voice_to_change].data.segment_data[segment_index].sound.splice(index,0,{note:-2,diesis:true,A_pos:[],A_neg:[]})
			recalc_midi=true
		}
	}
	if(recalc_midi){
		data.voice_data[index_voice_to_change].data.midi_data=DATA_segment_data_to_midi_data(data.voice_data[index_voice_to_change].data.segment_data,data.voice_data[index_voice_to_change].neopulse)
	}
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

//verify if is fractioned pulse first and then if is all ok breaking here,
//return true original fraction index, array new fraction ranges
function DATA_calculate_fraction_break_in_object_index(voice_id,segment_index,abs_position,is_A_T){
	//calculate if possible breaking fraction in position
	let data = DATA_get_current_state_point(true)
	let voice_data_mod = data.voice_data.find(item=>{
		return item.voice_id==voice_id
	})
	let position = abs_position
	let all_segment_data= voice_data_mod.data.segment_data
	if(segment_index==null){
		//ABSOLUTE position
		//need to calculate segment number and relative position
		segment_index=0
		let prev_segments_n_obj = 0
		all_segment_data.find(segment=>{
			let n_obj = segment.time.length -1
			if(prev_segments_n_obj+n_obj>abs_position){
				return true
			}else{
				//next segment
				prev_segments_n_obj+=n_obj //last element of a segment is .
				segment_index++
				return false
			}
		})
		position = abs_position - prev_segments_n_obj
	}
	let current_segment_data=all_segment_data[segment_index]
	if(is_A_T){
		//cutting a pulse
		//ok if F=0 and not a fraction range
		let time = current_segment_data.time[position]
		if(time.F!=0)return [false,]
		let fraction_range_index=-1
		let current_fraction_range = current_segment_data.fraction.find(range=>{
			fraction_range_index++
			return range.stop>time.P
		})
		if(current_fraction_range==null)return [false,]
		if(current_fraction_range.start==time.P)return [false,]
		let new_fraction_range_1 = {N:current_fraction_range.N,D:current_fraction_range.D,start:current_fraction_range.start,stop:time.P}
		let new_fraction_range_2 = {N:current_fraction_range.N,D:current_fraction_range.D,start:time.P,stop:current_fraction_range.stop}
		return [true,fraction_range_index,[new_fraction_range_1,new_fraction_range_2]]
	}else{
		//cutting a iT
		let timeA = current_segment_data.time[position]
		let timeB = current_segment_data.time[position+1]
		if(timeA.F!=0 || timeB.F!=0)return [false,]
		let fraction_range_index=-1
		let current_fraction_range = current_segment_data.fraction.find(range=>{
			fraction_range_index++
			return range.stop>timeA.P
		})
		if(current_fraction_range==null)return [false,]
		//if is entire interval
		if(current_fraction_range.start==timeA.P && current_fraction_range.stop==timeB.P)return [false,]
		//3 cases
		if(current_fraction_range.start==timeA.P){
			let new_fraction_range_1 = {N:current_fraction_range.N,D:current_fraction_range.D,start:current_fraction_range.start,stop:timeB.P}
			let new_fraction_range_2 = {N:current_fraction_range.N,D:current_fraction_range.D,start:timeB.P,stop:current_fraction_range.stop}
			return [true,fraction_range_index,[new_fraction_range_1,new_fraction_range_2]]
		}else if(current_fraction_range.stop==timeB.P){
			let new_fraction_range_1 = {N:current_fraction_range.N,D:current_fraction_range.D,start:current_fraction_range.start,stop:timeA.P}
			let new_fraction_range_2 = {N:current_fraction_range.N,D:current_fraction_range.D,start:timeA.P,stop:current_fraction_range.stop}
			return [true,fraction_range_index,[new_fraction_range_1,new_fraction_range_2]]
		}else{
			let new_fraction_range_1 = {N:current_fraction_range.N,D:current_fraction_range.D,start:current_fraction_range.start,stop:timeA.P}
			let new_fraction_range_2 = {N:current_fraction_range.N,D:current_fraction_range.D,start:timeA.P,stop:timeB.P}
			let new_fraction_range_3 = {N:current_fraction_range.N,D:current_fraction_range.D,start:timeB.P,stop:current_fraction_range.stop}
			return [true,fraction_range_index,[new_fraction_range_1,new_fraction_range_2,new_fraction_range_3]]
		}
	}
}

function DATA_join_fraction_ranges(voice_id,segment_index,first_fraction_index){
	let data = DATA_get_current_state_point(true)
	let voice_number = -1
	data.voice_data.some(voice=>{
		voice_number++
		return voice.voice_id==voice_id
	})
	let current_segment_data=data.voice_data[voice_number].data.segment_data[segment_index]
	//control if fract 1 compatible with new range, if not =1/1
	let start1=current_segment_data.fraction[first_fraction_index].start
	let stop1=current_segment_data.fraction[first_fraction_index].stop
	let start2=current_segment_data.fraction[first_fraction_index+1].start
	let stop2=current_segment_data.fraction[first_fraction_index+1].stop
	let N1 = current_segment_data.fraction[first_fraction_index].N
	let D1 = current_segment_data.fraction[first_fraction_index].D
	let N2 = current_segment_data.fraction[first_fraction_index+1].N
	let D2 = current_segment_data.fraction[first_fraction_index+1].D
	let new_start = start1
	let new_stop = stop2
	let new_range = new_stop-new_start
	let new_N = N1
	let new_D = D1
	let recalculate_range1_time = false
	let recalculate_range2_time = false
	if(new_range%new_N==0){
		//we can use the first fractioning
		if(N1==N2 && D1==D2){
			//mantaining all the pulses and notes
			//nothing to do
		}else{
			recalculate_range2_time=true
			//on second range
		}
	}else{
		//reset fractioning
		new_N = 1
		new_D = 1
		recalculate_range1_time=true
		recalculate_range2_time=true
		//on all the range
	}
	let iT_list = []
	let sound_insert_list = []
	let recalculate_start_range_index = null
	let recalculate_stop_range_index = null
	let recalculate_start=null
	let recalculate_stop=null
	if(recalculate_range2_time){
		//RECALCULATE USING IT
		//calculate old iTs
		let prev_pulse=0
		let prev_fraction=0
		//var tot_iT = range*fractSimple/fractComplex
		let start_range2_index = current_segment_data.time.indexOf(current_segment_data.time.find(time=>{return time.P==start2 && time.F==0}))
		let stop_range2_index = current_segment_data.time.indexOf(current_segment_data.time.find(time=>{return time.P==stop2 && time.F==0}))
		recalculate_start_range_index = start_range2_index
		recalculate_stop_range_index = stop_range2_index
		recalculate_start=start2
		recalculate_stop=stop2
		for (let i = start_range2_index; i <= stop_range2_index ; i++) {
			let pulse = current_segment_data.time[i].P
			let fraction = current_segment_data.time[i].F
			//calculate the dT value to be written
			let value = (pulse - prev_pulse) * D2 / N2 + (fraction - prev_fraction)
			iT_list.push(value)
			sound_insert_list.push(current_segment_data.sound[i])
			prev_pulse=pulse
			prev_fraction=fraction
		}
		//eliminate first iT
		iT_list.shift()
		//last element is "not part" or fractioning range
		//it is part of following range/segment/end of voice
		sound_insert_list.pop()
	}
	if(recalculate_range1_time){
		let prev_pulse=0
		let prev_fraction=0
		let start_range1_index = current_segment_data.time.indexOf(current_segment_data.time.find(time=>{return time.P==start1 && time.F==0}))
		let stop_range1_index = current_segment_data.time.indexOf(current_segment_data.time.find(time=>{return time.P==stop1 && time.F==0}))
		recalculate_start_range_index = start_range1_index
		recalculate_start=start1
		let iT_list_1 = []
		let sound_insert_list_1 = []
		for (let i = start_range1_index; i <= stop_range1_index ; i++) {
			let pulse = current_segment_data.time[i].P
			let fraction = current_segment_data.time[i].F
			//calculate the dT value to be written
			let value = (pulse - prev_pulse) * D1 / N1 + (fraction - prev_fraction)
			iT_list_1.push(value)
			sound_insert_list_1.push(current_segment_data.sound[i])
			prev_pulse=pulse
			prev_fraction=fraction
		}
		//eliminate first iT
		iT_list_1.shift()
		//last element is "not part" or fractioning range
		//it is part of following range/segment/end of voice
		sound_insert_list_1.pop()
		//concat 2 lists
		iT_list=iT_list_1.concat(iT_list)
		sound_insert_list=sound_insert_list_1.concat(sound_insert_list)
	}
	if(recalculate_range1_time || recalculate_range2_time){
		//adjust it list to new fractioning range
		let sum = iT_list.reduce((prop,item)=>{return item+prop},0)
		let range = recalculate_stop-recalculate_start
		let tot_iT = range*new_D/new_N
		if(tot_iT>sum){
			//add a last it
			iT_list.push(tot_iT-sum)
			//add a last s
			sound_insert_list.push({note:-1,diesis:true,A_pos:[],A_neg:[]})
		}else if(tot_iT<sum){
			//take out its
			let position = 0
			let new_iT_list = []
			iT_list.forEach(iT=>{
				if(tot_iT>position){
					let partial_iT=tot_iT-position
					position+=iT
					if(iT<=partial_iT){
						new_iT_list.push(iT)
					}else{
						new_iT_list.push(partial_iT)
					}
				}else{
					//outside
				}
			})
			iT_list = new_iT_list
			//eliminate extra notes
			sound_insert_list.splice(iT_list.length,(sound_insert_list.length-iT_list.length))
		}
		//recalculate pulse time with new fractioning and iT_list
		prev_pulse = recalculate_start
		prev_fraction = 0
		let time_insert_list = [{"P":recalculate_start,"F":0}]
		iT_list.forEach(iT=>{
			let d_pulse = (Math.floor((prev_fraction+iT)/new_D))*new_N
			prev_pulse+=d_pulse
			prev_fraction = (prev_fraction+iT)%new_D
			time_insert_list.push({"P":prev_pulse,"F":prev_fraction})
		})
		//last element is "not part" or fractioning range
		//it is part of following range/segment/end of voice
		time_insert_list.pop()
		//Change original data
		current_segment_data.time.splice(recalculate_start_range_index,recalculate_stop_range_index-recalculate_start_range_index,...time_insert_list)
		current_segment_data.sound.splice(recalculate_start_range_index,recalculate_stop_range_index-recalculate_start_range_index,...sound_insert_list)
	}
	//change first range
	current_segment_data.fraction[first_fraction_index].N=new_N
	current_segment_data.fraction[first_fraction_index].D=new_D
	current_segment_data.fraction[first_fraction_index].stop=new_stop
	//delete second
	current_segment_data.fraction.splice(first_fraction_index+1,1)
	//overwrite midi_data
	data.voice_data[voice_number].data.midi_data=DATA_segment_data_to_midi_data(data.voice_data[voice_number].data.segment_data,data.voice_data[voice_number].neopulse)
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

//ELEMENTS
function DATA_get_object_prev_note_index(voice_id,segment_index,abs_position){
	let data = DATA_get_current_state_point(true)
	let voice_data_mod = data.voice_data.find(item=>{
		return item.voice_id==voice_id
	})
	let all_segment_data= voice_data_mod.data.segment_data
	let current_segment_data=all_segment_data[segment_index]
	let position = abs_position
	if(segment_index==null){
		//ABSOLUTE position
		//need to calculate segment number and relative position
		segment_index=0
		let prev_segments_n_obj = 0
		all_segment_data.find(segment=>{
			let n_obj = segment.time.length -1
			if(prev_segments_n_obj+n_obj>abs_position){
				return true
			}else{
				//next segment
				prev_segments_n_obj+=n_obj //last element of a segment is .
				segment_index++
				return false
			}
		})
		position = abs_position - prev_segments_n_obj
	}
	return _find_prev_obj_position(position-1)
	function _find_prev_obj_position(position){
		if(position<0)return null
		let note = current_segment_data.sound[position].note
		if (note<0){
			return _find_prev_obj_position(position-1)
		}else{
			return position
		}
	}
}

//function return time, diesis, noteNa, N,D,N_NP,D_NP,isFrange(optional),compas(optional),scale(optional)
function DATA_get_object_index_info(voice_id,segment_index,abs_position,iT=0,getRange=false,getCompass=false,getScale=false){
	let data = DATA_get_current_state_point(true)
	let voice_data_mod = data.voice_data.find(item=>{
		return item.voice_id==voice_id
	})
	let all_segment_data= voice_data_mod.data.segment_data
	let position = abs_position
	if(segment_index==null){
		//ABSOLUTE position
		//need to calculate segment number and relative position
		segment_index=0
		let prev_segments_n_obj = 0
		all_segment_data.find(segment=>{
			let n_obj = segment.time.length -1
			if(prev_segments_n_obj+n_obj>abs_position){
				return true
			}else{
				//next segment
				prev_segments_n_obj+=n_obj //last element of a segment is .
				segment_index++
				return false
			}
		})
		position = abs_position - prev_segments_n_obj
	}
	let current_segment_data=all_segment_data[segment_index]
	let isRange=null
	let time=current_segment_data.time[position]
	let sound=current_segment_data.sound[position]
	let N_NP=voice_data_mod.neopulse.N
	let D_NP=voice_data_mod.neopulse.D
	let current_compas=null
	let current_scale=null
	let fraction = current_segment_data.fraction.find(fraction=>{return fraction.stop>time.P})
	if (typeof(fraction) == "undefined")fraction = null
	if(getRange){
		if(position==0){
			isRange=true
		}else{
			if(position==current_segment_data.time.length-1){
				isRange=true
				//fraction null bc is the end of segment
			}else{
				isRange = current_segment_data.fraction.some(fraction=>{
					return fraction.start==time.P && time.F==0
				})
			}
		}
	}
	let Psg_segment_start=0
	for (let i = 0; i < segment_index; i++) {
		Psg_segment_start+=all_segment_data[i].time.slice(-1)[0].P
	}
	if(getCompass && N_NP==1 && N_NP==1){
		let compas_data = data.compas
		let last_compas= compas_data.slice(-1)[0].compas_values
		let compas_length_value = last_compas[0]+last_compas[1]*last_compas[2]
		//it int_p inside a compas
		//take into account that pulse is part of a segment un x position
		//No problem using Pa, no compas if neopulse!!! Psg===Pa
		let Pa_element = Psg_segment_start + current_segment_data.time[position].P
		if(Pa_element <compas_length_value && compas_length_value!=0){
			//element inside a module
			let compas_type = compas_data.filter(compas=>{
				return compas.compas_values[0] <= Pa_element
			})
			let current_compas_type = compas_type.pop()
			let current_compas_number = compas_type.reduce((prop,item)=>{
				return item.compas_values[2]+prop
			},0)
			let dP = Pa_element - current_compas_type.compas_values[0]
			let mod = current_compas_type.compas_values[1]
			let dC = Math.floor(dP/mod)
			let resto = dP
			if(dC!=0) resto = dP%mod
			current_compas_number+= dC
			current_compas={"":resto,"c":current_compas_number}
		}else{
			//outside or no compasses
		}
	}
	if(getScale){
		//find correct scale
		//find Pa_equivalent and verify if inside a scale
		let frac_p = 1
		if(fraction==null){
			//probably end segment
			console.error("ERROR: search for scale on end segment??")
		}else{
			frac_p = fraction.N/fraction.D
		}
		let Pa_equivalent = (Psg_segment_start+time.P) * N_NP/D_NP + (time.F+iT)* frac_p * N_NP/D_NP
		let scale_sequence= data.scale.scale_sequence
		current_scale = DATA_get_Pa_eq_scale(Pa_equivalent , scale_sequence)
		//scale_index -1 if outside
		/*
		let all_scale_length_value = scale_sequence.reduce((p,scale)=>{return parseInt(scale.duration)+p},0)
		if(Pa_equivalent <all_scale_length_value && all_scale_length_value!=0){
			//element inside a scale module
			let scale_end_p=0
			let index=-1
			current_scale = scale_sequence.find(scale=>{
				index++
				scale_end_p += parseInt(scale.duration)
				return scale_end_p > Pa_equivalent
			})
			current_scale.scale_index=index
		}else {
			//outside range scale
		}*/
	}
	return {time:time,sound:sound,fraction:fraction,N_NP:N_NP,D_NP:D_NP,isRange:isRange,compas:current_compas,scale:current_scale,Psg_segment_start:Psg_segment_start}
}

//enter single pulse (no duration)
function DATA_enter_new_object(voice_id, segment_index, pulse, fraction , note, diesis,A_pos=[],A_neg=[]){
	if(note==-3 && pulse==0 && fraction==0)return
	let data = DATA_get_current_state_point(true)
	let voice_data_mod = data.voice_data.find(item=>{
		return item.voice_id==voice_id
	})
	let current_segment_data=voice_data_mod.data.segment_data[segment_index]
	if(current_segment_data.time.slice(-1)[0].P<=pulse){
		segment_index++
		if(segment_index>voice_data_mod.data.segment_data.length-1){
			//reached end of voice
			console.log("end of voice")
			return
		}
		current_segment_data=voice_data_mod.data.segment_data[segment_index]
		pulse=0
		fraction=0
	}
	//verify if is a new entry or a modification and in what position insert the thing
	let new_time = {"P":pulse,"F":fraction}
	let index_found=current_segment_data.time.indexOf(current_segment_data.time.find(time=>{return time.P==pulse && time.F==fraction}))
	if(index_found!=-1){
		//modify element if it is not the last one
		if(current_segment_data.sound[index_found].note==-4){
			return
		}
		current_segment_data.sound[index_found]={note,diesis,A_pos,A_neg}
	}else{
		//add element
		let index_next=current_segment_data.time.indexOf(current_segment_data.time.find(time=>{return time.P>pulse || (time.P==pulse && time.F>fraction)}))
		if(index_next==-1){
			console.error("pulse not localized in database: "+pulse+"."+fraction)
			return
		}
		if(!DATA_verify_pulse_position(current_segment_data,pulse,fraction)){
			return
		}
		current_segment_data.time.splice(index_next,0,new_time)
		current_segment_data.sound.splice(index_next,0,{note,diesis,A_pos:A_pos,A_neg:A_neg})
	}
	if(!!A_pos.length,!!A_neg.length)DATA_verify_voice_A(voice_data_mod,false)
	//midi
	voice_data_mod.data.midi_data=DATA_segment_data_to_midi_data(voice_data_mod.data.segment_data,voice_data_mod.neopulse)
	//load
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_verify_pulse_position(current_segment_data,pulse,fraction){//XXX XXX XXX XXX USE MORE THIS PLEASE XXX XXX XXX XXX
	let current_fraction_range=current_segment_data.fraction.find(fr=>{return fr.start<=pulse && fr.stop>pulse})
	if(current_fraction_range==null){
		console.error("Pulse position not compatible with fractioning range: "+pulse+"."+fraction)
		return false
	}
	let resto=(pulse-current_fraction_range.start) % current_fraction_range.N
	if(resto==0 && fraction<current_fraction_range.D){
		return true
	}
	console.error("Pulse position not compatible with fractioning range: "+pulse+"."+fraction)
	return false
}

//enter single pulse (no duration) in position index =   (btw position-1 and position of current data)
//at iT 1 from previous position if P and F == null
//if P anf F /! null control if correct values
function DATA_enter_new_object_index(voice_id, segment_index, abs_position , P=null,F=null,note, diesis,A_pos=[],A_neg=[]){
	let data = DATA_get_current_state_point(true)
	let position = abs_position
	let voice_data_mod = data.voice_data.find(item=>{
		return item.voice_id==voice_id
	})
	let all_segment_data= voice_data_mod.data.segment_data
	if(segment_index==null){
		//absolute position
		//need to calculate segment number and relative position
		segment_index=0
		let prev_segments_n_obj = 0
		all_segment_data.find(segment=>{
			let n_obj = segment.time.length -1
			if(prev_segments_n_obj+n_obj>abs_position){
				return true
			}else{
				//next segment
				prev_segments_n_obj+=n_obj //last element of a segment is .
				segment_index++
				return false
			}
		})
		position = abs_position - prev_segments_n_obj
	}
	//find if there is space after position
	let current_segment_data=all_segment_data[segment_index]
	if(current_segment_data.time.length>position && position!=0){
		if(P!=null &&F!=null){
			//find correct fraction range
			let current_fraction_range = current_segment_data.fraction.find(range=>{
				return range.stop>P //i m creating a new pulse, i m sure it is not a fraction range
			})
			let float_prev= current_segment_data.time[position-1].P+current_segment_data.time[position-1].F/100
			let float_next= current_segment_data.time[position].P+current_segment_data.time[position].F/100
			let float_current= P+F/100
			if(float_prev<float_current && float_current<float_next){
				//respect fractioning
				let rest = (P-current_segment_data.time[position-1].P) % current_fraction_range.N
				if(rest!=0){
					return false
				}
				//control if fractioned part is allowed
				if(F<current_fraction_range.D){
					let new_time = {"P":P,"F":F}
					current_segment_data.time.splice(position,0,new_time)
					current_segment_data.sound.splice(position,0,{note,diesis,A_pos,A_neg})
					//midi
					DATA_verify_voice_A(voice_data_mod,false)
					voice_data_mod.data.midi_data=DATA_segment_data_to_midi_data(all_segment_data,voice_data_mod.neopulse)
					//load
					DATA_insert_new_state_point(data)
					DATA_load_state_point_data(false,true)
					return true
				}else {
					return false
				}
			} else {
				//outside range
				return false
			}
		}else{
			//put at iT=1
			//verify if there is space (it>2)
			//find correct fraction range
			let current_fraction_range = current_segment_data.fraction.find(range=>{
				//fraction_range_index++
				return range.stop>current_segment_data.time[position-1].P
			})
			let iT=DATA_calculate_interval_PA_PB(current_segment_data.time[position-1],current_segment_data.time[position],current_fraction_range)
			if(iT>1){
				let new_time = DATA_calculate_next_position_PA_iT(current_segment_data.time[position-1],1,current_fraction_range)
				current_segment_data.time.splice(position,0,new_time)
				current_segment_data.sound.splice(position,0,{note,diesis,A_pos,A_neg})
				//midi
				DATA_verify_voice_A(voice_data_mod,false)
				voice_data_mod.data.midi_data=DATA_segment_data_to_midi_data(all_segment_data,voice_data_mod.neopulse)
				//load
				DATA_insert_new_state_point(data)
				DATA_load_state_point_data(false,true)
				return true
			}else{
				return false
			}
		}
	}else {
		return false
	}
}

//MODIFY / DELETE objects
//this family of functions modify or delete existing object(s) starting from a given position
//work with voice index , segment index ,position
//POSITION IS GIVEN WITH THE INDEX OF THE OBJECT instead of pulse and fraction
function DATA_delete_object_index(voice_id,segment_index,abs_position){
	let data = DATA_get_current_state_point(true)
	let position = abs_position
	let voice_data_mod = data.voice_data.find(item=>{
		return item.voice_id==voice_id
	})
	let all_segment_data= voice_data_mod.data.segment_data
	if(segment_index==null){
		//absolute position
		//need to calculate segment number and relative position
		segment_index=0
		let prev_segments_n_obj = 0
		all_segment_data.find(segment=>{
			let n_obj = segment.time.length -1
			if(prev_segments_n_obj+n_obj>abs_position){
				return true
			}else{
				//next segment
				prev_segments_n_obj+=n_obj //last element of a segment is .
				segment_index++
				return false
			}
		})
		position = abs_position - prev_segments_n_obj
	}
	let current_segment_data=all_segment_data[segment_index]
	if(position==0 ){
		//e
		//obj_list[abs_position].value=previous_value
		if(current_segment_data.sound[position].note==-2){
			//nothing to do
			return false
		}
		current_segment_data.sound[position]={note:-2,diesis:true,A_pos:[],A_neg:[]}
	}else{
		//check if is part of a fractioning range
		let found = current_segment_data.fraction.some(fraction=>{
			return fraction.start==current_segment_data.time[position].P && current_segment_data.time[position].F==0
		})
		if(found){
			if(current_segment_data.sound[position].note==-2){
				//nothing to do
				return false
			}
			current_segment_data.sound[position]={note:-2,diesis:true,A_pos:[],A_neg:[]}
		}else{
			current_segment_data.sound.splice(position,1)
			current_segment_data.time.splice(position,1)
		}
	}
	voice_data_mod.data.midi_data=DATA_segment_data_to_midi_data(all_segment_data,voice_data_mod.neopulse)
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
	return true
}

//modify starting point of an object
function DATA_modify_object_index_time(voice_id,segment_index, abs_position, new_P,new_F){
	let data = DATA_get_current_state_point(true)
	let position = abs_position
	let voice_data_mod = data.voice_data.find(item=>{
		return item.voice_id==voice_id
	})
	let all_segment_data= voice_data_mod.data.segment_data
	if(segment_index==null){
		//absolute position
		//need to calculate segment number and relative position
		segment_index=0
		let prev_segments_n_obj = 0
		all_segment_data.find(segment=>{
			let n_obj = segment.time.length -1
			if(prev_segments_n_obj+n_obj>abs_position){
				return true
			}else{
				//next segment
				prev_segments_n_obj+=n_obj //last element of a segment is .
				segment_index++
				return false
			}
		})
		position = abs_position - prev_segments_n_obj
	}
	if(position==0){
		return {success:false,error:0,error_info:null}
	}
	//verify if is a fractioning range
	let P=all_segment_data[segment_index].time[position].P
	let F=all_segment_data[segment_index].time[position].F
	result=DATA_calculate_modify_object_position_T_delta_strict(voice_data_mod,segment_index,P,F,new_P,new_F)
	if(result.success){
		//overwrite midi_data
		voice_data_mod.data.midi_data=DATA_segment_data_to_midi_data(all_segment_data,voice_data_mod.neopulse)
		DATA_insert_new_state_point(data)
		DATA_load_state_point_data(false,true)
	}
	return result
}

//modify has success if inside prev and next pulse
function DATA_calculate_modify_object_position_T_delta_strict(voice_data_mod,segment_index,P,F,new_P,new_F){
	//This function CAN modify P=0 F=0 into something else, but is called with those parameters ONLY by PMC (that use _free version)
	let all_segment_data= voice_data_mod.data.segment_data
	let current_segment_data=all_segment_data[segment_index]
	//calculate iNa segment
	let position = current_segment_data.time.findIndex(item=>{return item.P==P && item.F==F})
	let result={success:false,error:0,error_info:null}
	if(position==-1)return result
	//verify if is a fractioning range
	let isRange = false
	let current_fraction_index = null
	let current_fraction = current_segment_data.fraction.find((fraction,index)=>{
		current_fraction_index=index
		return fraction.stop>P
	})
	if(F==0 && current_fraction.start==P){
		isRange = true
	}
	//NEW CASE : P=0 F=0, create silence
	if(P==0 && F==0){//XXX
		if(new_P==0 && new_F==0){
			result.success=true
			result.error=null
			result.error_info=null
			return result
		}
		let next_time_data = current_segment_data.time[position+1]
		let rest = (new_P-current_fraction.start) % current_fraction.N
		if(rest!=0){
			return result
		}
		//control if fractioned part is allowed
		if(new_F>=current_fraction.D){
			return result
		}
		if(new_P>next_time_data.P || (new_P==next_time_data.P && new_F>=next_time_data.F)){
			return result
		}
		current_segment_data.time[position].P=new_P
		current_segment_data.time[position].F=new_F
		//add 0.0
		current_segment_data.time.unshift({P:0,F:0})
		current_segment_data.sound.unshift({note:-1,diesis:true,A_pos:[],A_neg:[]})
		//overwrite midi_data
		voice_data_mod.data.midi_data=DATA_segment_data_to_midi_data(all_segment_data,voice_data_mod.neopulse)
		result.success=true
		result.error=null
		result.error_info=null
		return result
	}
	if(isRange){
		//verify fraction ==0
		if(new_F==0){
			//change fractioning ranges and internal data structure
			//find previous fraction range
			let prev_fraction = current_segment_data.fraction[current_fraction_index-1]
			let resto = (new_P-prev_fraction.start)%prev_fraction.N
			if(new_P<=prev_fraction.start || resto!=0){
				let error_T_indexA=0
				current_segment_data.time.find((time,index)=>{
					if(time.P==prev_fraction.start){
						error_T_indexA=index
						return true
					}
				})
				let error_T_indexB=0
				current_segment_data.time.find((time,index)=>{
					if(time.P==prev_fraction.stop){
						error_T_indexB=index
						return true
					}
				})
				result.error=1
				result.error_info={"error_Tf_index":current_fraction_index-1,"error_T_indexA":error_T_indexA,"error_T_indexB":error_T_indexB}
				return result
			}
			resto = (current_fraction.stop-new_P)%current_fraction.N
			if(new_P>=current_fraction.stop || resto!=0){
				let error_T_indexA=0
				current_segment_data.time.find((time,index)=>{
					if(time.P==current_fraction.start){
						error_T_indexA=index
						return true
					}
				})
				let error_T_indexB=0
				current_segment_data.time.find((time,index)=>{
					if(time.P==current_fraction.stop){
						error_T_indexB=index
						return true
					}
				})
				result.error=2
				result.error_info={"error_Tf_index":current_fraction_index,"error_T_indexA":error_T_indexA,"error_T_indexB":error_T_indexB}
				return result
			}
			//trim ranges data
			current_segment_data.fraction[current_fraction_index-1].stop=new_P
			current_segment_data.fraction[current_fraction_index].start=new_P
			//find index A and B
			let range1_start_index = -1
			current_segment_data.time.find((time,index)=>{
				if(time.P==current_segment_data.fraction[current_fraction_index-1].start && time.F==0){
					range1_start_index=index
					return true
				}
			})
			let range2_stop_index = -1
			current_segment_data.time.find((time,index)=>{
				if(time.P==current_segment_data.fraction[current_fraction_index].stop && time.F==0){
					range2_stop_index=index
					return true
				}
			})
			let cutFROM=-1
			let cutTO=-1
			for (let i = range1_start_index+1; i < position; i++) {
				if(current_segment_data.time[i].P>=new_P){
					cutFROM=i
					cutTO=position-1
					i=position
				}
			}
			if(cutFROM==-1){
				cutFROM=position+1
				for (let i = position+1; i <= range2_stop_index; i++) {
					let time_aprox= current_segment_data.time[i].P+current_segment_data.time[i].F/100
					if(time_aprox>new_P){
						cutTO=i-1
						i=range2_stop_index
					}
				}
			}
			current_segment_data.time[position].P=new_P
			current_segment_data.time[position].F=0
			current_segment_data.time.splice(cutFROM,(cutTO-cutFROM+1))
			current_segment_data.sound.splice(cutFROM,(cutTO-cutFROM+1))
			//XXX maybe control L
		}else{
			return result
		}
	}else{
		//verify if P and F value is legit
		let rest = (new_P-current_fraction.start) % current_fraction.N
		if(rest!=0){
			return result
		}
		//control if fractioned part is allowed
		if(new_F>=current_fraction.D){
			return result
		}
		let prev_time_data = current_segment_data.time[position-1]
		let next_time_data = current_segment_data.time[position+1]
		//pulses like float numbers
		let new_value_float = parseFloat(new_P+"."+new_F)
		let previous_value_float = parseFloat(prev_time_data.P+"."+prev_time_data.F)
		let next_value_float = parseFloat(next_time_data.P+"."+next_time_data.F)
		if(previous_value_float<new_value_float && new_value_float<next_value_float){
			current_segment_data.time[position].P=new_P
			current_segment_data.time[position].F=new_F
		}else{
			return result
		}
	}
	//overwrite midi_data
	voice_data_mod.data.midi_data=DATA_segment_data_to_midi_data(all_segment_data,voice_data_mod.neopulse)
	result.success=true
	result.error=null
	result.error_info=null
	return result
}

//modify has success if inside range Fr colled by PMC only
function DATA_calculate_modify_object_position_T_delta_free(voice_data_mod,segment_index,P,F,new_P,new_F){
	//This functio CAN modify P=0 F=0 into something else, ONLY by PMC
	let all_segment_data= voice_data_mod.data.segment_data
	let current_segment_data=all_segment_data[segment_index]
	//calculate iNa segment
	let position = current_segment_data.time.findIndex(item=>{return item.P==P && item.F==F})
	let result={success:false,error:0,error_info:null}
	if(position==-1)return result
	if(new_P==P && new_F==F)return {success:true,error:null,error_info:null}
	//verify if is a fractioning range
	let isRange = false
	let current_fraction_index = null
	let current_fraction = current_segment_data.fraction.find((fraction,index)=>{
		current_fraction_index=index
		return fraction.stop>P
	})
	if(F==0 && current_fraction.start==P){
		isRange = true
	}
	//NEW CASE : P=0 F=0, create silence
	if(P==0 && F==0){//XXX limited, nonononononono XXX XXX XXX XXX XXX XXX
		if(new_P==0 && new_F==0){
			result.success=true
			result.error=null
			result.error_info=null
			return result
		}
		let position_end_range = current_segment_data.time.findIndex(item=>{return item.P==current_fraction.stop && item.F==0})
		let new_value_float = parseFloat(new_P+"."+new_F)
		for(let index=0;index<=position_end_range;index++){
			let current_value_float = parseFloat(current_segment_data.time[index].P+"."+current_segment_data.time[index].F)
			if(current_value_float<=new_value_float){
				cut_position=index
			}
		}
		if(cut_position==0){
			current_segment_data.time[0].P=new_P
			current_segment_data.time[0].F=new_F
		}else{
			for(let index=0;index<cut_position;index++){
				current_segment_data.time[index]=null
				current_segment_data.sound[index]=null
			}
			if(cut_position!=position_end_range){
				current_segment_data.time[cut_position].P=new_P
				current_segment_data.time[cut_position].F=new_F
			}
		}
		//add 0.0
		current_segment_data.time.unshift({P:0,F:0})
		current_segment_data.sound.unshift({note:-1,diesis:true,A_pos:[],A_neg:[]})
		current_segment_data.time=current_segment_data.time.filter(element => element !== null);
		current_segment_data.sound=current_segment_data.sound.filter(element => element !== null);
		//overwrite midi_data
		voice_data_mod.data.midi_data=DATA_segment_data_to_midi_data(all_segment_data,voice_data_mod.neopulse)
		result.success=true
		result.error=null
		result.error_info=null
		return result
	}
	if(isRange){
		//verify fraction ==0
		if(new_F==0){
			//change fractioning ranges and internal data structure
			if(new_P<current_fraction.start){
				let from_fraction_index = null
				let from_fraction = current_segment_data.fraction.find((fraction,index)=>{
					from_fraction_index=index
					return fraction.stop>new_P
				})
				//verify dP is ok for both current fraction and from fraction (can be erased)
				let dP_current_fraction=current_fraction.start-new_P
				if(dP_current_fraction%current_fraction.N!=0)return result
				if((new_P-from_fraction.start)%from_fraction.N!=0)return result
				//eventually delete obsolete fractions
				for(let index=from_fraction_index+1;index<current_fraction_index;index++){
					current_segment_data.fraction[index]=null
				}
				let position_start_range = current_segment_data.time.findIndex(item=>{return item.P==current_fraction.start && item.F==0})
				current_segment_data.fraction[from_fraction_index].stop=new_P
				current_segment_data.fraction[current_fraction_index].start=new_P
				if(from_fraction.start==new_P)current_segment_data.fraction[from_fraction_index]=null
				//eventually delete obsolete events
				for(let index=0;index<position_start_range;index++){
					if(current_segment_data.time[index].P>=new_P){
						current_segment_data.time[index]=null
						current_segment_data.sound[index]=null
					}
				}
				current_segment_data.time[position_start_range].P=new_P
			}else{
				current_fraction_index--;
				current_fraction = current_segment_data.fraction[current_fraction_index]
				let to_fraction_index = null
				let to_fraction = current_segment_data.fraction.find((fraction,index)=>{
					to_fraction_index=index
					return fraction.stop>=new_P
				})
				//verify dP is ok for both current fraction and to fraction (can be erased)
				let dP_current_fraction=new_P-current_fraction.start
				if(dP_current_fraction%current_fraction.N!=0)return result
				if((new_P-to_fraction.start)%to_fraction.N!=0)return result
				//eventually delete obsolete fractions
				for(let index=current_fraction_index+1;index<to_fraction_index;index++){
					current_segment_data.fraction[index]=null
				}
				current_segment_data.fraction[current_fraction_index].stop=new_P
				current_segment_data.fraction[to_fraction_index].start=new_P
				if(to_fraction.stop==new_P)current_segment_data.fraction[to_fraction_index]=null
				//eventually delete obsolete events
				let position_end_range = current_segment_data.time.findIndex(item=>{return item.P==P && item.F==0})
				let to_position = current_segment_data.time.findIndex(item=>{return item.P>=new_P})
				if (current_segment_data.time[to_position].P==new_P && current_segment_data.time[to_position].F==0)to_position++;
				let last_sound=null
				for(let index=position_end_range+1;index<to_position;index++){
					//if(current_segment_data.time[index].P>=P && current_segment_data.time[index].P<new_P){
						current_segment_data.time[index]=null
						last_sound=JSON.parse(JSON.stringify(current_segment_data.sound[index]))
						current_segment_data.sound[index]=null
					//}
				}
				if(last_sound==null)return result
				current_segment_data.time[position_end_range].P=new_P
				current_segment_data.sound[position_end_range]=last_sound
			}
			current_segment_data.fraction=current_segment_data.fraction.filter(element => element !== null);
			current_segment_data.time=current_segment_data.time.filter(element => element !== null);
			current_segment_data.sound=current_segment_data.sound.filter(element => element !== null);
			//check L
			let new_position = current_segment_data.time.findIndex(item=>{return item.P==new_P && item.F==0})
			if(new_position==-1)return result
			if(current_segment_data.sound[new_position].note==-3 && new_position==0){
				current_segment_data.sound[new_position].note=-2
			}
		}else{
			return result
		}
	}else{
		//verify if P and F value is legit, inside current range including range start, end
		let rest = (new_P-current_fraction.start) % current_fraction.N
		if(rest!=0){
			return result
		}
		//control if fractioned part is allowed
		if(new_F>=current_fraction.D){
			return result
		}
		//control no L start
		if(new_F==0 && new_P==0 && current_segment_data.sound[position].note==-3){
			return result
		}
		let position_start_range = current_segment_data.time.findIndex(item=>{return item.P==current_fraction.start && item.F==0})
		let position_end_range= current_segment_data.time.findIndex(item=>{return item.P==current_fraction.stop && item.F==0})
		let new_value_float = parseFloat(new_P+"."+new_F)
		let old_value_float = parseFloat(P+"."+F)
		if(new_value_float<old_value_float){
			//eventually eliminate previous elements and expand this event to start range
			for(let index=position_start_range;index<position;index++){
				if(current_segment_data.time[index].P>new_P){
					current_segment_data.time[index]=null
					current_segment_data.sound[index]=null
				}else{
					if(current_segment_data.time[index].P==new_P && current_segment_data.time[index].F>=new_F){
						current_segment_data.time[index]=null
						current_segment_data.sound[index]=null
					}
				}
			}
			if(new_P<current_fraction.start){
				current_segment_data.time[position].P=current_fraction.start
				current_segment_data.time[position].F=0
			}else{
				current_segment_data.time[position].P=new_P
				current_segment_data.time[position].F=new_F
			}
		}else{
			//eventually eliminate following elements including this event to the end range
			let cut_position=position
			let new_value_float = parseFloat(new_P+"."+new_F)
			for(let index=position;index<=position_end_range;index++){
				let current_value_float = parseFloat(current_segment_data.time[index].P+"."+current_segment_data.time[index].F)
				if(current_value_float<=new_value_float){
					cut_position=index
				}
			}
			if(cut_position==position){
				current_segment_data.time[position].P=new_P
				current_segment_data.time[position].F=new_F
			}else{
				for(let index=position;index<cut_position;index++){
					current_segment_data.time[index]=null
					current_segment_data.sound[index]=null
				}
				if(cut_position!=position_end_range){
					current_segment_data.time[cut_position].P=new_P
					current_segment_data.time[cut_position].F=new_F
				}
			}
		}
		current_segment_data.time=current_segment_data.time.filter(element => element !== null);
		current_segment_data.sound=current_segment_data.sound.filter(element => element !== null);
	}
	//overwrite midi_data
	voice_data_mod.data.midi_data=DATA_segment_data_to_midi_data(all_segment_data,voice_data_mod.neopulse)
	result.success=true
	result.error=null
	result.error_info=null
	return result
}

//mod if segment index==null //position is absolute position
function DATA_modify_object_index_sound(voice_id,segment_index, abs_position, note_Na=null,diesis=null,A_pos=null,A_neg=null){
	let data = DATA_get_current_state_point(true)
	let position = abs_position
	let voice_data_mod = data.voice_data.find(item=>{
		return item.voice_id==voice_id
	})
	let all_segment_data= voice_data_mod.data.segment_data
	let outside_range=false
	if(segment_index==null){
		//absolute position
		//need to calculate segment number and relative position
		segment_index=0
		let prev_segments_n_obj = 0
		all_segment_data.find(segment=>{
			let n_obj = segment.time.length -1
			if(prev_segments_n_obj+n_obj>abs_position){
				return true
			}else{
				//next segment
				prev_segments_n_obj+=n_obj //last element of a segment is .
				segment_index++
				return false
			}
		})
		position = abs_position - prev_segments_n_obj
	}
	let segment=all_segment_data[segment_index]
	let sound=segment.sound[position]
	//traduction already done
	let old_sound=JSON.parse(JSON.stringify(sound))
	if(note_Na!=null){
		//change note
		if(position==0 && note_Na==-3){
			return false
		}
		let max_note_number = TET * N_reg-1
		if(note_Na>max_note_number){
			outside_range=true
			note_Na=max_note_number
		}
		sound.note=note_Na
	}
	if(diesis!=null){
		//change diesis
		sound.diesis=diesis
	}
	if(note_Na>=0){
		if(A_pos!=null){
			//change it
			sound.A_pos=A_pos
			voice_data_mod.A=true
		}
		if(A_neg!=null){
			//change it
			sound.A_neg=A_neg
			voice_data_mod.A=true
		}
		if(A_neg==null && A_pos==null && A_global_variables.type=="Ag"){
			//verify condition and translate A grade
			let Psg_segment_start=0
			for (let i = 0; i < segment_index; i++) {
				Psg_segment_start+=voice_data_mod.data.segment_data[i].time.slice(-1)[0].P
			}
			let time=segment.time[position]
			let fraction = segment.fraction.find(fraction=>{return fraction.stop>time.P})
			if (typeof(fraction) == "undefined"){
				return
			}
			//calculating position
			let frac_p = fraction.N/fraction.D
			let N_NP=voice_data_mod.neopulse.N
			let D_NP=voice_data_mod.neopulse.D
			let Pa_equivalent = (Psg_segment_start+time.P) * N_NP/D_NP + time.F* frac_p * N_NP/D_NP
			//element inside a scale module
			let current_scale = DATA_get_Pa_eq_scale(Pa_equivalent)
			//let current_scale = DATA_get_Pa_eq_scale(Pa_equivalent,scale_sequence)
			if(current_scale!=null){
				let idea_scale_list=DATA_get_idea_scale_list()
				let new_A = DATA_calculate_A_from_sound_grade_change(old_sound,note_Na,diesis,current_scale,null,idea_scale_list)
				sound.A_pos=new_A.A_pos
				sound.A_neg=new_A.A_neg
			}
			//else use modular, no need to recalculate
		}
		let o_r=DATA_verify_sound_A(sound)
		DATA_verify_voice_A(voice_data_mod,false)
		if(o_r)outside_range=true
	}else{
		sound.A_pos=[]
		sound.A_neg=[]
	}
	if(outside_range){
		APP_info_msg("out_N_range")
	}
	//overwrite midi_data
	voice_data_mod.data.midi_data=DATA_segment_data_to_midi_data(all_segment_data,voice_data_mod.neopulse)
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
	return true
}

//for PMC N modification
function DATA_calculate_modify_object_position_N_delta(voice_data_mod,segment_index,P,F,delta){
	let all_segment_data= voice_data_mod.data.segment_data
	let segment=all_segment_data[segment_index]
	//calculate iNa segment
	let position = segment.time.findIndex(item=>{return item.P==P && item.F==F})
	if(position==-1)return {success:false}
	let sound=segment.sound[position]
	let current_note = sound.note
	let new_note=current_note+delta
	let outside_range=false
	if(current_note<0){
		return {success:false}
	}else{
		if(new_note<0 || new_note>TET*N_reg-1)outside_range=true
		if(new_note<0)new_note=0
		if(new_note>TET*N_reg-1)new_note=TET*N_reg-1
	}
	//traduction already done
	let old_sound=JSON.parse(JSON.stringify(sound))
	sound.note=new_note
	if(A_global_variables.type=="Ag"){
		//verify condition and translate A grade
		let Psg_segment_start=0
		for (let i = 0; i < segment_index; i++) {
			Psg_segment_start+=all_segment_data[i].time.slice(-1)[0].P
		}
		let time=segment.time[position]
		let fraction = segment.fraction.find(fraction=>{return fraction.stop>time.P})
		if (typeof(fraction) == "undefined"){
			return
		}
		//calculating position
		let frac_p = fraction.N/fraction.D
		let N_NP=voice_data_mod.neopulse.N
		let D_NP=voice_data_mod.neopulse.D
		let Pa_equivalent = (Psg_segment_start+time.P) * N_NP/D_NP + time.F* frac_p * N_NP/D_NP
		//element inside a scale module
		let current_scale = DATA_get_Pa_eq_scale(Pa_equivalent)
		//let current_scale = DATA_get_Pa_eq_scale(Pa_equivalent,scale_sequence)
		let idea_scale_list=DATA_get_idea_scale_list()
		let new_A = DATA_calculate_A_from_sound_grade_change(old_sound,new_note,null,current_scale,null,idea_scale_list)
		sound.A_pos=new_A.A_pos
		sound.A_neg=new_A.A_neg
	}
	let result = DATA_verify_sound_A(sound)
	if(result) outside_range=true
	voice_data_mod.data.midi_data=DATA_segment_data_to_midi_data(all_segment_data,voice_data_mod.neopulse)
	return {success:true,outside_range}
}

//mod it = change iT of a object (if 0 delete it)
function DATA_modify_object_index_iT(voice_id,segment_index,abs_position,duration_iT){
	let data = DATA_get_current_state_point(true)
	let voice_data_mod = data.voice_data.find(item=>{
		return item.voice_id==voice_id
	})
	let position = abs_position
	let all_segment_data= voice_data_mod.data.segment_data
	if(segment_index==null){
		//ABSOLUTE position
		//need to calculate segment number and relative position
		segment_index=0
		let prev_segments_n_obj = 0
		all_segment_data.find(segment=>{
			let n_obj = segment.time.length -1
			if(prev_segments_n_obj+n_obj>abs_position){
				return true
			}else{
				//next segment
				prev_segments_n_obj+=n_obj //last element of a segment is .
				segment_index++
				return false
			}
		})
		position = abs_position - prev_segments_n_obj
	}
	let current_segment_data=all_segment_data[segment_index]
	//find pulse and fraction start
	let pulse_start = current_segment_data.time[position].P
	let fraction_start = current_segment_data.time[position].F
	//find correct fraction range
	let current_fraction_range_index=0
	let current_fraction_range = current_segment_data.fraction.find((range,index)=>{
		if(range.stop>pulse_start){
			current_fraction_range_index=index
			return true
		}
		return false
	})
	//find index start and stop change modification
	let index_start_mod = position
	let index_stop_mod = current_segment_data.time.indexOf(current_segment_data.time.find(time=>{return time.P==current_fraction_range.stop && time.F==0}))-1
	let old_iT_value = DATA_calculate_interval_PA_PB(current_segment_data.time[index_start_mod],current_segment_data.time[index_start_mod+1],current_fraction_range)
	let delta_iT = duration_iT-old_iT_value
	if(duration_iT==0){
		//delete range
		//if only element in the range or the last...
		if(index_stop_mod==index_start_mod){
			//it is the last element
			if(fraction_start==0 && pulse_start==current_fraction_range.start){
				//it is the only element
				return [false,0,]
			}else{
				//delete object
				current_segment_data.time.splice(index_start_mod,1)
				current_segment_data.sound.splice(index_start_mod,1)
			}
		}else{
			//delete element and slide other objects
			for (let i = index_start_mod+1; i<=index_stop_mod; i++){
				current_segment_data.time[i] = DATA_calculate_next_position_PA_iT(current_segment_data.time[i],delta_iT,current_fraction_range)
			}
			//delete object
			current_segment_data.time.splice(index_start_mod,1)
			current_segment_data.sound.splice(index_start_mod,1)
		}
	}else{
		//modify element
		//check if I am editing the last value OF THE RANGE
		if(index_stop_mod==index_start_mod){
			//modify last element
			if(duration_iT>old_iT_value){
				//there is no space
				//calculating if the delta_iT make integer pulses AND if this pulses are a multiple of min_Li
				let resto_Fr = delta_iT%current_fraction_range.D
				if(resto_Fr==0){
					let delta_Ps = delta_iT*current_fraction_range.N/current_fraction_range.D
					let min_Li = DATA_calculate_minimum_Li()
					let min_Ls = min_Li * (voice_data_mod.neopulse.D/voice_data_mod.neopulse.N)
					let resto = delta_Ps%min_Ls
					if(resto==0){
						//1 : enlarge the segment (and other voices!)
						let new_Ls = current_segment_data.time.slice(-1)[0].P+delta_Ps
						let extend_last_element=true
						data = DATA_calculate_change_Ls_segment(data,voice_id,segment_index,new_Ls,extend_last_element)
						if(data==false){//not necess
							return [false,3,]
						}
						//2 : change pulse sequence in current segment
						for (let i = index_start_mod+1; i < current_segment_data.time.length-1; i++) {
							current_segment_data.time[i].P+=delta_Ps
						}
						//change fraction range of segment
						current_segment_data.fraction.forEach((fraction,index,array)=>{
							if(fraction.start>pulse_start){
								array[index].start+=delta_Ps
							}
							if(fraction.stop>pulse_start){
								array[index].stop+=delta_Ps
							}
						})
						//last one was already corrected
						current_segment_data.fraction[current_segment_data.fraction.length-1].stop-=delta_Ps
					}else{
						return [false,3,]
					}
				}else{
					return [false,2,{"error_Tf_index":current_fraction_range_index}]
				}
			}else{
				//create new element at the end of the range
				let new_time = DATA_calculate_next_position_PA_iT(current_segment_data.time[index_start_mod],duration_iT,current_fraction_range)
				current_segment_data.time.splice(index_start_mod+1,0,new_time)
				current_segment_data.sound.splice(index_start_mod+1,0,{note:-2,diesis:true,A_pos:[],A_neg:[]})
			}
		}else{
			//find if there is space to place new object
			let provv_next_time = DATA_calculate_next_position_PA_iT(current_segment_data.time[index_start_mod+1],delta_iT,current_fraction_range)
			if(provv_next_time.P<current_fraction_range.stop || (provv_next_time.P==current_fraction_range.stop && provv_next_time.F==0)){
				//recalculating new P values inside the range (not the last one)
				for (let i = index_start_mod+1; i<=index_stop_mod; i++){
					current_segment_data.time[i] = DATA_calculate_next_position_PA_iT(current_segment_data.time[i],delta_iT,current_fraction_range)
					if(current_segment_data.time[i].P>=current_fraction_range.stop){
						current_segment_data.time[i]=null
						current_segment_data.sound[i]=null
					}
				}
				//eliminate items ==null
				current_segment_data.time=current_segment_data.time.filter(item=>{return item!=null})
				current_segment_data.sound=current_segment_data.sound.filter(item=>{return item!=null})
			}else{
				return [false,1,{"error_iT_index":index_start_mod}]
			}
		}
	}
	voice_data_mod.data.midi_data=DATA_segment_data_to_midi_data(all_segment_data,voice_data_mod.neopulse)
	//load
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
	return [true,,]
}

//for PMC iT modification
function DATA_calculate_modify_object_position_iT_delta(voice_data_mod,segment_index,P,F,delta_iT,force=false){
	//force=true if it<=0 iT=1, if iT>max_value iT=max_value
	//force is not necessary anymore, mouseup feed last success d_iT value
	//test delta is the new iT
	let all_segment_data= voice_data_mod.data.segment_data
	let current_segment_data=all_segment_data[segment_index]
	let position = current_segment_data.time.findIndex(item=>{return item.P==P && item.F==F})
	if(position==-1)return {success:false}
	let current_fraction_range=current_segment_data.fraction.find(fraction=>{return fraction.stop>P})
	if(current_fraction_range==null)return {success:false}
	//find index start and stop change modification
	let index_start_mod = position
	let index_stop_mod = current_segment_data.time.indexOf(current_segment_data.time.find(time=>{return time.P==current_fraction_range.stop && time.F==0}))-1
	let old_iT_value = DATA_calculate_interval_PA_PB(current_segment_data.time[index_start_mod],current_segment_data.time[index_start_mod+1],current_fraction_range)
	let new_iT_value = old_iT_value+delta_iT
	if(new_iT_value<=0){
		if(force){
			new_iT_value=1
			delta_iT=new_iT_value-old_iT_value
			if(old_iT_value==new_iT_value)return {success:false}
		}else{
			return {success:false}
		}
	}
	//modify element
	//check if I am editing the last value OF THE RANGE
	if(index_stop_mod==index_start_mod){
		//modify last element
		if(new_iT_value>old_iT_value){
			//there is no space
			//calculating if the delta_iT make integer pulses AND if this pulses are a multiple of min_Li
			let resto_Fr = delta_iT%current_fraction_range.D
			if(resto_Fr==0){
				let delta_Ps = delta_iT*current_fraction_range.N/current_fraction_range.D
				let min_Li = DATA_calculate_minimum_Li()
				let min_Ls = min_Li * (voice_data_mod.neopulse.D/voice_data_mod.neopulse.N)
				let resto = delta_Ps%min_Ls
				if(resto==0){
					//1 : enlarge the segment (and other voices!)
					let new_Ls = current_segment_data.time.slice(-1)[0].P+delta_Ps
					let extend_last_element=true
					let new_data = DATA_get_current_state_point(true)
					new_data = DATA_calculate_change_Ls_segment(new_data,voice_data_mod.voice_id,segment_index,new_Ls,extend_last_element)
					//2 : change pulse sequence in current segment of new data
					let new_voice_data_mod=new_data.voice_data.find(voice=>{return voice.voice_id==voice_data_mod.voice_id})
					let new_all_segment_data= new_voice_data_mod.data.segment_data
					let new_current_segment_data=new_all_segment_data[segment_index]
					for (let i = index_start_mod+1; i < new_current_segment_data.time.length-1; i++) {
						new_current_segment_data.time[i].P+=delta_Ps
					}
					//change fraction range of segment
					new_current_segment_data.fraction.forEach((fraction,index,array)=>{
						if(fraction.start>P){
							array[index].start+=delta_Ps
						}
						if(fraction.stop>P){
							array[index].stop+=delta_Ps
						}
					})
					//last one was already corrected
					new_current_segment_data.fraction[current_segment_data.fraction.length-1].stop-=delta_Ps
					new_voice_data_mod.data.midi_data=DATA_segment_data_to_midi_data(new_all_segment_data,new_voice_data_mod.neopulse)
					return {success:true,data:new_data}
				}else{
					return {success:false}
				}
			}else{
				return {success:false}
			}
		}else{
			//create new element at the end of the range
			let new_time = DATA_calculate_next_position_PA_iT(current_segment_data.time[index_start_mod],new_iT_value,current_fraction_range)
			current_segment_data.time.splice(index_start_mod+1,0,new_time)
			current_segment_data.sound.splice(index_start_mod+1,0,{note:-2,diesis:true,A_pos:[],A_neg:[]})
		}
	}else{
		//new iT rule, no need to use force bool
		//find if there is space to place new object
		let provv_next_time = DATA_calculate_next_position_PA_iT(current_segment_data.time[index_start_mod+1],delta_iT,current_fraction_range)
		if(provv_next_time.P<current_fraction_range.stop || (provv_next_time.P==current_fraction_range.stop && provv_next_time.F==0)){
			//recalculating new P values inside the range (not the last one)
			for (var i = index_start_mod+1; i<=index_stop_mod; i++){
				//current_segment_data.time[i] = DATA_calculate_next_position_PA_iT(current_segment_data.time[i],delta_iT,current_fraction_range)
				current_segment_data.time[i] = DATA_calculate_next_position_PA_iT(current_segment_data.time[i],delta_iT,current_fraction_range)
				if(current_segment_data.time[i].P>=current_fraction_range.stop){
					current_segment_data.time[i]=null
					current_segment_data.sound[i]=null
				}
			}
			//eliminate items ==null
			current_segment_data.time=current_segment_data.time.filter(item=>{return item!=null})
			current_segment_data.sound=current_segment_data.sound.filter(item=>{return item!=null})
		}else{
			return {success:false, "error_D_iT_index":0}//PMC
		}
	}
	voice_data_mod.data.midi_data=DATA_segment_data_to_midi_data(all_segment_data,voice_data_mod.neopulse)
	//return voice_data_mod
	return {success:true}
}

function DATA_modify_object_index_iN(voice_id,segment_index,abs_position,value,isanote=true,diesis=null){
	let data = DATA_get_current_state_point(true)
	let voice_data_mod = data.voice_data.find(item=>{
		return item.voice_id==voice_id
	})
	let position = abs_position
	let all_segment_data= voice_data_mod.data.segment_data
	if(segment_index==null){
		//ABSOLUTE position
		//need to calculate segment number and relative position
		segment_index=0
		let prev_segments_n_obj = 0
		all_segment_data.find(segment=>{
			let n_obj = segment.time.length -1
			if(prev_segments_n_obj+n_obj>abs_position){
				return true
			}else{
				//next segment
				prev_segments_n_obj+=n_obj //last element of a segment is .
				segment_index++
				return false
			}
		})
		position = abs_position - prev_segments_n_obj
	}
	let current_segment_data=all_segment_data[segment_index]
	let iNa
	if(isanote){
		//is a iNa
		iNa=value
	}else{
		//is e , L or endrange
		if(value==-1)iNa="s"
		if(value==-2)iNa="e"
		if(value==-3){
			iNa=tie_intervals
			if(position==0)return false
		}
		if(value==-4)iNa=end_range
	}
	//calculate iNa segment
	let iNa_list=DATA_calculate_iNa_from_Na_list(current_segment_data.sound.map(s=>{return s.note}))
	iNa_list[position]=iNa
	let [new_Na_list,outside_range]= DATA_calculate_Na_from_iNa_list(iNa_list)
	let Psg_segment_start=0
	for (let i = 0; i < segment_index; i++) {
		Psg_segment_start+=all_segment_data[i].time.slice(-1)[0].P
	}
	let N_NP=voice_data_mod.neopulse.N
	let D_NP=voice_data_mod.neopulse.D
	let A_outside_range=DATA_calculate_segment_change_Na_line(new_Na_list,current_segment_data,Psg_segment_start,N_NP,D_NP)
	if(A_outside_range)outside_range=true
	if(outside_range){
		APP_info_msg("out_N_range")
	}
	//if diesis != null change diesis
	if(diesis==true || diesis==false)current_segment_data.sound[position].diesis=diesis
	voice_data_mod.data.midi_data=DATA_segment_data_to_midi_data(all_segment_data,voice_data_mod.neopulse)
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
	return true
}

//for PMC iN modification
function DATA_calculate_modify_object_position_iN_delta(voice_data_mod,segment_index,P,F,delta){
	let all_segment_data= voice_data_mod.data.segment_data
	let current_segment_data=all_segment_data[segment_index]
	//calculate iNa segment
	let iNa_list=DATA_calculate_iNa_from_Na_list(current_segment_data.sound.map(s=>{return s.note}))
	let position = current_segment_data.time.findIndex(item=>{return item.P==P && item.F==F})
	if(position==-1)return {success:false}
	if(isNaN(iNa_list[position])){
		//find position next note
		position = iNa_list.findIndex((item,i)=>{return i>position && !isNaN(item)})
		if(position==-1)return {success:false}
	}
	iNa_list[position]=iNa_list[position]+delta
	let [new_Na_list,outside_range]= DATA_calculate_Na_from_iNa_list(iNa_list)
	let Psg_segment_start=0
	for (let i = 0; i < segment_index; i++) {
		Psg_segment_start+=all_segment_data[i].time.slice(-1)[0].P
	}
	let N_NP=voice_data_mod.neopulse.N
	let D_NP=voice_data_mod.neopulse.D
	let A_outside_range=DATA_calculate_segment_change_Na_line(new_Na_list,current_segment_data,Psg_segment_start,N_NP,D_NP)
	if(A_outside_range)outside_range=true
	voice_data_mod.data.midi_data=DATA_segment_data_to_midi_data(all_segment_data,voice_data_mod.neopulse)
	return {success:true,outside_range}
}

function DATA_enter_new_object_index_iN(voice_id,segment_index,abs_position,value,isanote=true,diesis=true,A_pos=[],A_neg=[]){
	let data = DATA_get_current_state_point(true)
	let voice_data_mod = data.voice_data.find(item=>{
		return item.voice_id==voice_id
	})
	let position = abs_position
	let all_segment_data= voice_data_mod.data.segment_data
	if(segment_index==null){
		//ABSOLUTE position
		//need to calculate segment number and relative position
		segment_index=0
		let prev_segments_n_obj = 0
		all_segment_data.find(segment=>{
			let n_obj = segment.time.length -1
			if(prev_segments_n_obj+n_obj>abs_position){
				return true
			}else{
				//next segment
				prev_segments_n_obj+=n_obj //last element of a segment is .
				segment_index++
				return false
			}
		})
		position = abs_position - prev_segments_n_obj
	}
	let current_segment_data=all_segment_data[segment_index]
	//find if there is space after position
	if(current_segment_data.time.length>position && position!=0){
		//verify if there is space (it>2)
		//find correct fraction range
		let current_fraction_range = current_segment_data.fraction.find(range=>{
			//fraction_range_index++
			return range.stop>current_segment_data.time[position-1].P
		})
		let iT=DATA_calculate_interval_PA_PB(current_segment_data.time[position-1],current_segment_data.time[position],current_fraction_range)
		if(iT>1){
			let new_time = DATA_calculate_next_position_PA_iT(current_segment_data.time[position-1],1,current_fraction_range)
			current_segment_data.time.splice(position,0,new_time)
			let iNa_list=DATA_calculate_iNa_from_Na_list(current_segment_data.sound.map(s=>{return s.note}))
			let iNa
			if(isanote){
				//is a iNa
				iNa=value
			}else{
				//is e , L or endrange
				if(value==-1)iNa="s"
				if(value==-2)iNa="e"
				if(value==-3)iNa=tie_intervals
				if(value==-4)iNa=end_range
			}
			iNa_list.splice(position,0,iNa)
			//add empty
			current_segment_data.sound.splice(position,0,{note:-1,diesis,A_pos,A_neg})
			let [new_Na_list,outside_range]= DATA_calculate_Na_from_iNa_list(iNa_list)
			let Psg_segment_start=0
			for (let i = 0; i < segment_index; i++) {
				Psg_segment_start+=all_segment_data[i].time.slice(-1)[0].P
			}
			let N_NP=voice_data_mod.neopulse.N
			let D_NP=voice_data_mod.neopulse.D
			let A_outside_range=DATA_calculate_segment_change_Na_line(new_Na_list,current_segment_data,Psg_segment_start,N_NP,D_NP)
			if(A_outside_range)outside_range=true
			if(outside_range){
				APP_info_msg("out_N_range")
			}
			//midi
			voice_data_mod.data.midi_data=DATA_segment_data_to_midi_data(all_segment_data,voice_data_mod.neopulse)
			//load
			DATA_insert_new_state_point(data)
			DATA_load_state_point_data(false,true)
			return true
		}else{
			return false
		}
	}else {
		return false
	}
}

function DATA_delete_object_index_iN(voice_id,segment_index,abs_position){
	let data = DATA_get_current_state_point(true)
	let voice_data_mod = data.voice_data.find(item=>{
		return item.voice_id==voice_id
	})
	let position = abs_position
	let all_segment_data= voice_data_mod.data.segment_data
	if(segment_index==null){
		//ABSOLUTE position
		//need to calculate segment number and relative position
		segment_index=0
		let prev_segments_n_obj = 0
		all_segment_data.find(segment=>{
			let n_obj = segment.time.length -1
			if(prev_segments_n_obj+n_obj>abs_position){
				return true
			}else{
				//next segment
				prev_segments_n_obj+=n_obj //last element of a segment is .
				segment_index++
				return false
			}
		})
		position = abs_position - prev_segments_n_obj
	}
	let current_segment_data=all_segment_data[segment_index]
	let iNa_list=DATA_calculate_iNa_from_Na_list(current_segment_data.sound.map(s=>{return s.note}))
	if(position==0 ){
		if(iNa_list[position]=="e")return false //nothing to do
		iNa_list[position]="e"
	}else{
		//check if is part of a fractioning range
		let found = current_segment_data.fraction.some(fraction=>{
			return fraction.start==current_segment_data.time[position].P && current_segment_data.time[position].F==0
		})
		if(found){
			if(iNa_list[position]=="e")return false //nothing to do
			iNa_list[position]="e"
		}else{
			iNa_list.splice(position,1)
			current_segment_data.time.splice(position,1)
			current_segment_data.sound.splice(position,1)
		}
	}
	let [new_Na_list,outside_range]= DATA_calculate_Na_from_iNa_list(iNa_list)
	let Psg_segment_start=0
	for (let i = 0; i < segment_index; i++) {
		Psg_segment_start+=all_segment_data[i].time.slice(-1)[0].P
	}
	let N_NP=voice_data_mod.neopulse.N
	let D_NP=voice_data_mod.neopulse.D
	let A_outside_range=DATA_calculate_segment_change_Na_line(new_Na_list,current_segment_data,Psg_segment_start,N_NP,D_NP)
	if(A_outside_range)outside_range=true
	if(outside_range){
		APP_info_msg("out_N_range")
	}

	voice_data_mod.data.midi_data=DATA_segment_data_to_midi_data(all_segment_data,voice_data_mod.neopulse)
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
	return true
}

function DATA_calculate_iT_list_from_segment_data(segment){
	let iT_list = []
	let time_array_copy=JSON.parse(JSON.stringify(segment.time))
	let fraction_array_copy=JSON.parse(JSON.stringify(segment.fraction))
	for (let i = 0; i < time_array_copy.length-1 ; i++) {
		let pulse = time_array_copy[i].P
		let fraction = time_array_copy[i].F
		let next_pulse = time_array_copy[i+1].P
		let next_fraction = time_array_copy[i+1].F
		let current_fraction=fraction_array_copy.find(fraction=>{return fraction.stop>pulse})
		//calculate the dT value to be written
		let value = (next_pulse - pulse)  * current_fraction.D / current_fraction.N + (next_fraction - fraction)
		//or DATA_calculate_interval_PA_PB(time1,time2,current_fraction)
		iT_list.push(value)
	}
	return iT_list
}

function DATA_calculate_iT_from_time(pulse,fraction,next_pulse,next_fraction,segment_data){
	let fraction_array_copy=JSON.parse(JSON.stringify(segment_data.fraction))
	let current_fraction=fraction_array_copy.find(fraction=>{return fraction.stop>pulse})
	if(current_fraction==null)return null
	if((current_fraction.stop>next_pulse || (current_fraction.stop==next_pulse && next_fraction==0)) &&
		//control fraction values are allowed
		(fraction<current_fraction.D  && next_fraction<current_fraction.D)){
		//calculate the dT value
		let value = (next_pulse - pulse)  * current_fraction.D / current_fraction.N + (next_fraction - fraction)
		//or DATA_calculate_interval_PA_PB(time1,time2,current_fraction)
		if(value>0) return value
		return null
	}else {
		return null
	}
}

function DATA_calculate_time_from_iT(iT_list,fraction){
	//return void array if error (iT not compatible with Fr)
	let time_list=[]
	let current_Fr_index=0
	time_list.push({P:0,F:0})
	let P=0
	let F=0
	let error=false
	iT_list.forEach(iT=>{
		if(fraction.length>current_Fr_index){
			F+=iT
			let dP = (Math.floor(F/fraction[current_Fr_index].D))*fraction[current_Fr_index].N
			let resto = F%fraction[current_Fr_index].D
			F=resto
			P+=dP
			if(P<fraction[current_Fr_index].stop){
				time_list.push({"P":P,"F":F})
			}else if(P==fraction[current_Fr_index].stop){
				if(F==0){
					//ok change fraction range
					time_list.push({"P":P,"F":F})
					current_Fr_index++
				}else{
					error=true
				}
			}else if(P>fraction[current_Fr_index].stop){
				error=true
			}
		}else{
			error=true
		}
	})
	if(error){
		return []
	}else{
		//verify last pulse is last fraction range end
		let last_time = time_list.slice(-1)[0]
		let last_fraction = fraction.slice(-1)[0]
		if(last_time.P==last_fraction.stop && last_time.F==0){
			return time_list
		}else{
			return []
		}
	}
}

function DATA_calculate_iNa_from_Na_list(Na_list){/////////////SOUND_LIST/////////////?????
	let first_note=true
	let prev_note=null
	let iNa_list=[]
	Na_list.forEach((current_note,index)=>{
		if(current_note==-1){
			iNa_list.push("s")
		}else if(current_note==-2){
			iNa_list.push("e")
		}else if(current_note==-3){
			iNa_list.push((index==0)?"e":tie_intervals)//last resource, normally cast an error
		}else if(current_note==-4){
			iNa_list.push((index==Na_list.length-1)?end_range:"e")
		}else{
			if(first_note){
				iNa_list.push(current_note)
				first_note=false
				prev_note=current_note
			}else{
				iNa_list.push(current_note-prev_note)
				prev_note=current_note
			}
		}
	})
	return iNa_list
}

//ATT!!! iNa list use letters  "e" "L" end_range
//return Na list and if outside note range
function DATA_calculate_Na_from_iNa_list(iNa_list){
	let Na_list = []
	let index=0
	let first_element= true
	let current_note = 0
	let max_note_number = TET * N_reg-1
	let outside_range=false
	iNa_list.forEach((current_iNa,index)=>{
		if(current_iNa=="s"){
			Na_list.push(-1)
		}else if(current_iNa=="e"){
			Na_list.push(-2)
		}else if(current_iNa==tie_intervals){
			Na_list.push((index==0)?-2:-3)
		}else if(current_iNa==end_range){
			Na_list.push((index==iNa_list.length-1)?-4:-3)
		}else{
			if(first_element){
				//first element is a note
				current_note= DATA_calculate_inRange(current_iNa,0,max_note_number)[0]
				//the first note
				first_element=false
			} else {
				let inRange;
				[current_note,inRange] = DATA_calculate_inRange(current_note+current_iNa,0,max_note_number)
				if(!inRange)outside_range=true
			}
			Na_list.push(current_note)
		}
	})
	return [Na_list,outside_range]
}

function DATA_calculate_segment_change_Na_line(Na_list,segment,Psg_segment_start,N_NP,D_NP){
	let outside_range=false
	if(A_global_variables.type=="Ag"){
		let scale_sequence=DATA_get_scale_sequence()
		let idea_scale_list=DATA_get_idea_scale_list()
		segment.sound.forEach((sound,index)=>{
			let old_sound=JSON.parse(JSON.stringify(sound))
			let new_note=Na_list[index]
			sound.note=new_note
			//verify condition and translate A grade
			let time=segment.time[index]
			let fraction = segment.fraction.find(fraction=>{return fraction.stop>time.P})
			if (typeof(fraction) == "undefined"){
				return
			}
			//calculating position
			let frac_p = fraction.N/fraction.D
			let Pa_equivalent = (Psg_segment_start+time.P) * N_NP/D_NP + time.F* frac_p * N_NP/D_NP
			//element inside a scale module
			let current_scale = DATA_get_Pa_eq_scale(Pa_equivalent,scale_sequence)
			let new_A = DATA_calculate_A_from_sound_grade_change(old_sound,new_note,null,current_scale,null,idea_scale_list)
			sound.A_pos=new_A.A_pos
			sound.A_neg=new_A.A_neg
			let result_A = DATA_verify_sound_A(sound)
			if(result_A) outside_range=true
		})
	}else{
		segment.sound.forEach((sound,index)=>{
			sound.note=Na_list[index]
			let result_A = DATA_verify_sound_A(sound)
			if(result_A) outside_range=true
		})
	}
	return outside_range
}

//operations with grades
function DATA_calculate_iNgm_from_Ngm_list(Ngm_list,scale_list){
	//Ngm_list == {note,grade,delta,reg,diesis,A_pos,A_neg}
	let first_note=true
	let prev_note_grade=null
	let iNgm_list=[]
	let scale_module_list=[]
	scale_list.forEach(item=>{
		let module = scale_module_list.find(module=>{
			return module.acronym=item.acronym
		})
		if(module==null){
			module = BNAV_get_scale_from_acronym(item.acronym).module
			scale_module_list.push({acronym:item.acronym,module:module})
		}else{
			scale_module_list.push(module)
		}
	})
	Ngm_list.forEach((current_sound_grade,index)=>{
		let current_Na=current_sound_grade.note
		if(current_Na<0){
			//not a note
			iNgm_list.push({note:current_Na,diesis:current_sound_grade.diesis,A_pos:[],A_neg:[]})
		}else{
			if(first_note){
				iNgm_list.push(current_sound_grade)
				first_note=false
			}else{
				//IN CASE OF CHANGE OF SCALE XXX XXX XXX XXX
				//USE SCALE MODULE PREV ELEMENT OR CURRENT ELEMENT????
				//iNgm evaluated current scale, current module
				let scale_module = scale_module_list[index].module
				let d_grad = current_sound_grade.grade-prev_note_grade.grade+(current_sound_grade.reg-prev_note_grade.reg)*scale_module
				iNgm_list.push({note:current_Na,iNgm:d_grad,diesis:current_sound_grade.diesis,A_pos:current_sound_grade.A_pos,A_neg:current_sound_grade.A_neg})
			}
			prev_note_grade=current_sound_grade
		}
	})
	return iNgm_list
}

function DATA_calculate_Na_from_iNgm_list(iNgm_list,scale_list,idea_scale_list){
	//iNgm_list
	//not note: {note:Na,diesis:diesis})
	//first note: {note:Na,grade:grade,delta:delta,reg:reg,diesis:diesis}
	//d_grad : {note:Na,iNgm:d_grad,diesis:diesis}
	//Ngm_list == {note,grade,delta,reg,diesis}
	//Na_list == {note,diesis}
	let first_note=true
	let prev_note_grade=null
	let Na_list=[]
	let outside_range=false
	iNgm_list.forEach((current_i_note_grade,index)=>{
		let current_Na=current_i_note_grade.note
		if(current_Na<0){
			//not a note
			Na_list.push({note:current_Na,diesis:current_i_note_grade.diesis})
		}else{
			let scale_data=scale_list[index]
			if(first_note){
				let result = DATA_calculate_scale_note_to_absolute(current_i_note_grade.grade,current_i_note_grade.delta,current_i_note_grade.reg,scale_data,idea_scale_list)
				if(result.outside_range)outside_range=true
				let new_Na = result.note
				Na_list.push({note:new_Na,diesis:current_i_note_grade.diesis})
				first_note=false
				prev_note_grade=current_i_note_grade
			}else{
				let new_grade=prev_note_grade.grade+current_i_note_grade.iNgm
				let new_delta=0//snap to grade
				let new_reg=prev_note_grade.reg
				let result = DATA_calculate_scale_note_to_absolute(new_grade,new_delta,new_reg,scale_data,idea_scale_list)
				if(result.outside_range)outside_range=true
				let new_Na = result.note
				Na_list.push({note:new_Na,diesis:current_i_note_grade.diesis})
				prev_note_grade={grade:new_grade,delta:new_delta,reg:new_reg}
			}
		}
	})
	return [Na_list,outside_range]
}

function DATA_calculate_Ng_import_from_data_index(voice_id,segment_index=null,reg=null){
	let data = DATA_calculate_Ngm_from_data_index(voice_id,segment_index)
	//({sound_grade:sound_grade,scale:null})
	//traduction to grade absolute with base reg
	let Ng_array=[]
	if(data==null){
		return []
	}
	let idea_scale_list=DATA_get_idea_scale_list()
	data.forEach((d,index)=>{
		if(d.sound_grade.note<0)return
		let grade=d.sound_grade.grade
		if(d.scale==null){
			//Nm
			if(reg!=null){
				let d_reg=d.sound_grade.reg-reg
				if(d_reg!=0){
					grade+=d_reg*TET
				}
			}
		}else{
			if(reg!=null){
				let d_reg=d.sound_grade.reg-reg
				if(d_reg!=0){
					//find scale module
					let scale = idea_scale_list.find(item=>{
						return item.acronym==d.scale.acronym
					})
					if(scale==null){
						return
					}
					let module = scale.module
					grade+=d_reg*module
				}
			}
		}
		Ng_array.push(grade)
	})
	return Ng_array
}

function DATA_calculate_iNg_import_from_data_index(voice_id,segment_index=null,reg=null){
	let data = DATA_calculate_Ngm_from_data_index(voice_id,segment_index)
	if(data==null){
		return []
	}
	let iNg_array=[]
	if(data[0].scale==null){
		//purge from no sound elements
		let provv = data.map(d=>{return d.sound_grade.note})
		let Na_list=provv.filter(p=>{return p>=0})
		let iNa_list=DATA_calculate_iNa_from_Na_list(Na_list)
		if(iNa_list.length==0)return []
		let first_reg=Math.floor(iNa_list[0]/TET)
		let resto=iNa_list[0]%TET
		if(reg!=null){
			let d_reg=first_reg-reg
			if(d_reg!=0){
				resto+=d_reg*TET
			}
		}
		iNa_list[0]=resto
		iNg_array=iNa_list
	}else{
		//calc iNgm
		let sound_grade_list=[]
		let scale_list=[]
		data.forEach(d=>{
			sound_grade_list.push(d.sound_grade)
			scale_list.push(d.scale)
		})
		let i_sound_grade_list=DATA_calculate_iNgm_from_Ngm_list(sound_grade_list,scale_list)
		//traduction to grade absolute first is a note
		let starting_note=true
		let idea_scale_list=DATA_get_idea_scale_list()
		i_sound_grade_list.forEach((i_sound_data,index)=>{
			if(i_sound_data.note<0)return
			if(starting_note){
				let grade=i_sound_data.grade
				if(reg!=null){
					let d_reg=i_sound_data.reg-reg
					if(d_reg!=0){
						//find scale module
						let scale = idea_scale_list.find(item=>{
							return item.acronym==scale_list[index].acronym
						})
						if(scale==null){
							return
						}
						let module = scale.module
						grade+=d_reg*module
					}
				}
				iNg_array.push(grade)
				starting_note=false
			}else{
				iNg_array.push(i_sound_data.iNgm)
			}
		})
	}
	return iNg_array
}

function DATA_calculate_Ngm_from_data_index(voice_id,segment_index=null){
	//find data
	let data = DATA_get_current_state_point(false)
	let current_voice_data = data.voice_data.find(item=>{return item.voice_id==voice_id})
	if(current_voice_data==null){
		console.error("Operation error, data not found")
		return []
	}
	let segment=null
	let Psg_segment_start=0
	if(segment_index==null){
		segment= DATA_concat_segments(current_voice_data)
	}else{
		if(current_voice_data.data.segment_data.length<=segment_index){
			console.error("Operation error, data not found")
			return []
		}
		segment=current_voice_data.data.segment_data[segment_index]
		for (let i = 0; i < segment_index; i++) {
			Psg_segment_start+=current_voice_data.data.segment_data[i].time.slice(-1)[0].P
		}
	}
	let scale_data= data.scale
	return DATA_calculate_Ngm_from_segment(segment,Psg_segment_start,current_voice_data.neopulse,scale_data)
}

function DATA_calculate_origin_scale_list(segment,Psg_segment_start,neopulse,scale_sequence){
	//result array of scales
	let [N_NP,D_NP]=[neopulse.N,neopulse.D]
	let result=[] //{note_grade,A_pos_grade,A_neg_grade,scale}
	segment.time.find((time,index)=>{
		//traduction
		let fraction = segment.fraction.find(fraction=>{return fraction.stop>time.P})
		if (typeof(fraction) == "undefined"){
			//end segment
			return true
		}
		//calculating position
		let frac_p = fraction.N/fraction.D
		let Pa_equivalent = (Psg_segment_start+time.P) * N_NP/D_NP + time.F* frac_p * N_NP/D_NP
		//inside a scale module
		let current_scale = DATA_get_Pa_eq_scale(Pa_equivalent , scale_sequence)
		//handy store the calculated scale for later calculations
		result.push(current_scale)
	})
	return result
}

function DATA_calculate_Ngm_from_segment(segment,Psg_segment_start,neopulse,scale_data){
	//result array of note_grade
	let [N_NP,D_NP]=[neopulse.N,neopulse.D]
	let result=[] //note_grade
	if(scale_data.scale_sequence.length!=0){
		segment.sound.find((sound,index)=>{
			//traduction
			let time = segment.time[index]
			let fraction = segment.fraction.find(fraction=>{return fraction.stop>time.P})
			if (typeof(fraction) == "undefined"){
				//end segment
				return true
			}
			//calculating position
			let frac_p = fraction.N/fraction.D
			let Pa_equivalent = (Psg_segment_start+time.P) * N_NP/D_NP + time.F* frac_p * N_NP/D_NP
			//inside a scale module
			let current_scale = DATA_get_Pa_eq_scale(Pa_equivalent , scale_data.scale_sequence)
			let sound_grade
			if(sound.note<0){
				sound_grade={note:sound.note}
			}else{
				let [grade,delta,reg]=DATA_calculate_absolute_note_to_scale(sound.note,sound.diesis,current_scale,scale_data.idea_scale_list)
				sound_grade={note:sound.note,grade:grade,delta:delta,reg:reg,diesis:sound.diesis}
			}
			//handy store the calculated scale for later calculations
			result.push({sound_grade:sound_grade,scale:current_scale})
		})
	}else{
		//Nm
		segment.sound.find((sound,index)=>{
			let sound_grade
			if(sound.note<0){
				sound_grade={note:sound.note}
			}else{
				let reg = Math.floor(sound.note/TET)
				let resto = sound.note%TET
				sound_grade={note:sound.note,grade:resto,delta:0,reg:reg,diesis:sound.diesis}
			}
			result.push({sound_grade:sound_grade,scale:null})
		})
	}
	return result
}

function DATA_modify_object_index_iNgrade(voice_id,segment_index,abs_position,value,isanote=true,diesis=null){
	let data = DATA_get_current_state_point(true)
	let voice_data_mod = data.voice_data.find(item=>{
		return item.voice_id==voice_id
	})
	let position = abs_position
	let all_segment_data= voice_data_mod.data.segment_data
	let idea_scale_list=data.scale.idea_scale_list
	if(segment_index==null){
		//ABSOLUTE position
		//need to calculate segment number and relative position
		segment_index=0
		let prev_segments_n_obj = 0
		all_segment_data.find(segment=>{
			let n_obj = segment.time.length -1
			if(prev_segments_n_obj+n_obj>abs_position){
				return true
			}else{
				//next segment
				prev_segments_n_obj+=n_obj //last element of a segment is .
				segment_index++
				return false
			}
		})
		position = abs_position - prev_segments_n_obj
	}
	let Psg_segment_start = 0
	for (let i = 0; i < segment_index; i++) {
		Psg_segment_start+=all_segment_data[i].time.slice(-1)[0].P
	}
	let N_NP = voice_data_mod.neopulse.N
	let D_NP = voice_data_mod.neopulse.D
	let current_segment_data=all_segment_data[segment_index]
	let current_Na = current_segment_data.sound[position].note
	let current_diesis = current_segment_data.sound[position].diesis
	let current_scale = _current_voice_segment_position_scale(position)
	if(current_scale==null)return false
	//find previous note and modified delta grade
	let prev_position=position-1
	let prev_scale = "NaN"
	if(prev_position<0){
		//simply mod object
	}else{
		while (prev_position>=0 && current_segment_data.sound[prev_position].note<0){
			prev_position--
		}
		if(prev_position<0){
			//simply mod object
			//console.error("iN grade outside segment")
			//return false //XXX XXX XXX why?? this prevent a  S - starting note to change
		}else{
			prev_scale=_current_voice_segment_position_scale(prev_position)
		}
	}
	if(isanote){
		if(prev_scale.scale_index!= current_scale.scale_index){
			//is start of a new scale
			if(current_Na>=0){
				//changing a note, i want to mantain serie iNgm
				let [current_grade,current_delta,current_reg]=DATA_calculate_absolute_note_to_scale(current_Na,current_diesis,current_scale,idea_scale_list)
				let [target_grade,target_delta,target_reg]=DATA_calculate_absolute_note_to_scale(value,current_diesis,current_scale,idea_scale_list)
				let scale_module = BNAV_get_scale_from_acronym(current_scale.acronym).module
				let d_grade = target_grade-current_grade+(target_reg-current_reg)*scale_module
				let outside_range = DATA_calculate_D_iNgm_from_object_index(d_grade,position,current_scale,current_segment_data,Psg_segment_start,N_NP,D_NP,A_global_variables.type).outside_range
				if(outside_range)APP_info_msg("out_N_range")
			}else{
				current_segment_data.sound[position].note=value
			}
		}else{
			//is a iNg
			let d_grade = value
			let prev_Na = current_segment_data.sound[prev_position].note
			if(current_Na>=0){
				//need to recalculate all intervals
				let prev_diesis = current_segment_data.sound[prev_position].diesis
				let [grade_A,delta_A,reg_A]=DATA_calculate_absolute_note_to_scale(prev_Na,prev_diesis,prev_scale,idea_scale_list)
				let [current_grade,current_delta,current_reg]=DATA_calculate_absolute_note_to_scale(current_Na,current_diesis,current_scale,idea_scale_list)
				let scale_module = BNAV_get_scale_from_acronym(current_scale.acronym).module
				let old_d_grade = current_grade-grade_A+(current_reg-reg_A)*scale_module
				d_grade-=old_d_grade
			}else{
				current_segment_data.sound[position].note=prev_Na
			}
			let outside_range = DATA_calculate_D_iNgm_from_object_index(d_grade,position,current_scale,current_segment_data,Psg_segment_start,N_NP,D_NP,A_global_variables.type).outside_range
			if(outside_range)APP_info_msg("out_N_range")
		}
	}else{
		//verify if is the position 0 no L
		if(position==0 && value==-3){
			return false
		}
		//find scale element and scale prev element
		if(prev_scale.scale_index!= current_scale.scale_index){
			//first object scale , change no problem
			current_segment_data.sound[position].note=value
		}else{
			if(current_Na<0){
				//not a note , change no problem
				current_segment_data.sound[position].note=value
			}else{
				//need to recalculate all intervals
				let prev_Na = current_segment_data.sound[prev_position].note
				let prev_diesis = current_segment_data.sound[prev_position].diesis
				let [grade_A,delta_A,reg_A]=DATA_calculate_absolute_note_to_scale(prev_Na,prev_diesis,prev_scale,idea_scale_list)
				let [current_grade,current_delta,current_reg]=DATA_calculate_absolute_note_to_scale(current_Na,current_diesis,current_scale,idea_scale_list)
				let scale_module = BNAV_get_scale_from_acronym(current_scale.acronym).module
				let d_grade = current_grade-grade_A+(current_reg-reg_A)*scale_module
				current_segment_data.sound[position].note=value //XXX
				let outside_range = DATA_calculate_D_iNgm_from_object_index(-d_grade,position,current_scale,current_segment_data,Psg_segment_start,N_NP,D_NP,A_global_variables.type).outside_range
				if(outside_range)APP_info_msg("out_N_range")
			}
		}
	}
	//if diesis != null change diesis XXX
	if(diesis==true || diesis==false)current_segment_data.sound[position].diesis=diesis
	voice_data_mod.data.midi_data=DATA_segment_data_to_midi_data(all_segment_data,voice_data_mod.neopulse)
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
	return true

	function _current_voice_segment_position_scale(pos){
		let _P = current_segment_data.time[pos].P
		let _F = current_segment_data.time[pos].F
		//find correct scale
		//find Pa_equivalent and verify if inside a scale
		let _fraction = current_segment_data.fraction.find(fraction=>{return fraction.stop>_P})
		let _frac_p = _fraction.N/_fraction.D
		let _Pa_equivalent = (Psg_segment_start+_P) * N_NP/D_NP + _F* _frac_p * N_NP/D_NP
		return DATA_get_Pa_eq_scale(_Pa_equivalent)
	}
}

//for PMC iNgm modification
function DATA_calculate_modify_object_position_iNgrade_delta(voice_data_mod,segment_index,P,F,delta){
	let all_segment_data= voice_data_mod.data.segment_data
	let current_segment_data=all_segment_data[segment_index]
	let N_NP = voice_data_mod.neopulse.N
	let D_NP = voice_data_mod.neopulse.D
	let position = current_segment_data.time.findIndex(item=>{return item.P==P && item.F==F})
	if(position==-1)return {success:false,data_change:false,outside_range:false}
	let Psg_segment_start = 0
	for (let i = 0; i < segment_index; i++) {
		Psg_segment_start+=all_segment_data[i].time.slice(-1)[0].P
	}
	let current_scale = _current_voice_segment_position_scale(position)
	if(current_scale==null)return {success:false,data_change:false,outside_range:false}
	if(current_segment_data.sound[position].note<0){
		//find position next note
		position = current_segment_data.sound.findIndex((item,i)=>{return i>position && item.note>0})
		if(position==-1)return {success:false,data_change:false,outside_range:false}
		//if different scale
		let new_pos_scale=_current_voice_segment_position_scale(position)
		if(current_scale.scale_index!=new_pos_scale.scale_index)return {success:false,data_change:false}
	}
	let note_array_copy=JSON.stringify(current_segment_data.sound)
	let outside_range= DATA_calculate_D_iNgm_from_object_index(delta,position,current_scale,current_segment_data,Psg_segment_start,N_NP,D_NP,A_global_variables.type).outside_range
	voice_data_mod.data.midi_data=DATA_segment_data_to_midi_data(all_segment_data,voice_data_mod.neopulse)
	let data_change//doesnt work in one line
	data_change=(note_array_copy==JSON.stringify(current_segment_data.sound))?false:true
	return {success:true,data_change:data_change,outside_range:outside_range}

	function _current_voice_segment_position_scale(pos){
		let _P = current_segment_data.time[pos].P
		let _F = current_segment_data.time[pos].F
		//find correct scale
		//find Pa_equivalent and verify if inside a scale
		let _fraction = current_segment_data.fraction.find(fraction=>{return fraction.stop>_P})
		let _frac_p = _fraction.N/_fraction.D
		let _Pa_equivalent = (Psg_segment_start+_P) * N_NP/D_NP + _F* _frac_p * N_NP/D_NP
		//console.log(Pa_equivalent)
		return DATA_get_Pa_eq_scale(_Pa_equivalent)
	}
}

function DATA_enter_new_object_index_iNgrade(voice_id,segment_index,abs_position,value,isanote=true,diesis=true,A_pos=[],A_neg=[]){
	let data = DATA_get_current_state_point(true)
	let voice_data_mod = data.voice_data.find(item=>{
		return item.voice_id==voice_id
	})
	let position = abs_position
	let all_segment_data= voice_data_mod.data.segment_data
	if(segment_index==null){
		//ABSOLUTE position
		//need to calculate segment number and relative position
		segment_index=0
		let prev_segments_n_obj = 0
		all_segment_data.find(segment=>{
			let n_obj = segment.time.length -1
			if(prev_segments_n_obj+n_obj>abs_position){
				return true
			}else{
				//next segment
				prev_segments_n_obj+=n_obj //last element of a segment is .
				segment_index++
				return false
			}
		})
		position = abs_position - prev_segments_n_obj
	}
	let Psg_segment_start = 0
	for (let i = 0; i < segment_index; i++) {
		Psg_segment_start+=all_segment_data[i].time.slice(-1)[0].P
	}
	let N_NP = voice_data_mod.neopulse.N
	let D_NP = voice_data_mod.neopulse.D
	let current_segment_data=all_segment_data[segment_index]
	//find if there is space after position
	if(current_segment_data.time.length>position && position!=0){
		//verify if there is space (it>2)
		//find correct fraction range
		let current_fraction_range = current_segment_data.fraction.find(range=>{
			//fraction_range_index++
			return range.stop>current_segment_data.time[position-1].P
		})
		let iT=DATA_calculate_interval_PA_PB(current_segment_data.time[position-1],current_segment_data.time[position],current_fraction_range)
		if(iT>1){
			let new_time = DATA_calculate_next_position_PA_iT(current_segment_data.time[position-1],1,current_fraction_range)
			current_segment_data.time.splice(position,0,new_time)
			//add prev note and apply D iNgm
			let prev_position=position-1
			let prev_scale = "NaN"
			if(prev_position<0){
				console.error("Start of a segment: operation not allowed")
				return false
			}else{
				while (prev_position>=0 && current_segment_data.sound[prev_position].note<0){
					prev_position--
				}
				if(prev_position<0){
					//console.error("prev position / note not found")
					// return false
				}else{
					prev_scale=_current_voice_segment_position_scale(prev_position)
				}
			}
			//provv a silence
			current_segment_data.sound.splice(position,0,{note:-1,diesis:diesis,A_pos:A_pos,A_neg:A_neg})
			let current_scale = _current_voice_segment_position_scale(position)
			if(current_scale==null)return false
			if(isanote){
				//if(JSON.stringify(prev_scale) != JSON.stringify(current_scale)){
				if(prev_scale.scale_index!= current_scale.scale_index){
					//is start of a new scale not a iNgm
					//inserting a note, i want to mantain serie iNgm
					current_segment_data.sound[position].note=value
				}else{
					let prev_Na = current_segment_data.sound[prev_position].note
					current_segment_data.sound[position].note=prev_Na
					let outside_range = DATA_calculate_D_iNgm_from_object_index(value,position,current_scale,current_segment_data,Psg_segment_start,N_NP,D_NP,A_global_variables.type).outside_range
					if(outside_range)APP_info_msg("out_N_range")
				}
			}else{
				//verify if is the position 0 no L
				if(position==0 && value==-3){
					//impossible
					return false
				}
				//insertion of a no note will not change the sequence
				current_segment_data.sound[position].note=value
				current_segment_data.sound[position].A_pos=[]
				current_segment_data.sound[position].A_neg=[]
			}
		}else{
			return false
		}
	}else {
		return false
	}
	//midi
	voice_data_mod.data.midi_data=DATA_segment_data_to_midi_data(all_segment_data,voice_data_mod.neopulse)
	//load
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
	return true

	function _current_voice_segment_position_scale(pos){
		let _P = current_segment_data.time[pos].P
		let _F = current_segment_data.time[pos].F
		//find correct scale
		//find Pa_equivalent and verify if inside a scale
		let _fraction = current_segment_data.fraction.find(fraction=>{return fraction.stop>_P})
		let _frac_p = _fraction.N/_fraction.D
		let _Pa_equivalent = (Psg_segment_start+_P) * N_NP/D_NP + _F* _frac_p * N_NP/D_NP
		//console.log(Pa_equivalent)
		return DATA_get_Pa_eq_scale(_Pa_equivalent)
	}
}

function DATA_delete_object_index_iNgrade(voice_id,segment_index,abs_position){
	//if(!DEBUG_MODE)console.log("DATA delete onject index iNgrade")
	let data = DATA_get_current_state_point(true)
	let voice_data_mod = data.voice_data.find(item=>{
		return item.voice_id==voice_id
	})
	let position = abs_position
	let all_segment_data= voice_data_mod.data.segment_data
	let idea_scale_list=data.scale.idea_scale_list
	if(segment_index==null){
		//ABSOLUTE position
		//need to calculate segment number and relative position
		segment_index=0
		let prev_segments_n_obj = 0
		all_segment_data.find(segment=>{
			let n_obj = segment.time.length -1
			if(prev_segments_n_obj+n_obj>abs_position){
				return true
			}else{
				//next segment
				prev_segments_n_obj+=n_obj //last element of a segment is .
				segment_index++
				return false
			}
		})
		position = abs_position - prev_segments_n_obj
	}
	let Psg_segment_start = 0
	for (let i = 0; i < segment_index; i++) {
		Psg_segment_start+=all_segment_data[i].time.slice(-1)[0].P
	}
	let N_NP = voice_data_mod.neopulse.N
	let D_NP = voice_data_mod.neopulse.D
	let current_segment_data=all_segment_data[segment_index]
	let found = current_segment_data.fraction.some(fraction=>{
		return fraction.start==current_segment_data.time[position].P && current_segment_data.time[position].F==0
	})
	let current_sound = current_segment_data.sound[position]
	let current_Na = current_sound.note
	let current_diesis = current_sound.diesis
	let current_scale = _current_voice_segment_position_scale(position)
	if(current_scale==null)return false
	let d_grade = 0
	if(current_Na>=0){
		//find current interval and use it in order to modify other intervals
		let [current_grade,current_delta,current_reg]=DATA_calculate_absolute_note_to_scale(current_Na,current_diesis,current_scale,idea_scale_list)
		let scale_module = BNAV_get_scale_from_acronym(current_scale.acronym).module
		//find previous note and deleted delta grade
		let prev_position=position-1
		if(prev_position<0){
			//simply delete object (not necess, control already done? in RE!)
			console.error("simply try delete it")
		}else{
			while (prev_position>=0 && current_segment_data.sound[prev_position].note<0){
				prev_position--
			}
			if(prev_position<0){
				//simply delete object (not necess, control already done? in RE!)
				console.error("simply try delete it")
			}else{
				let prev_scale=_current_voice_segment_position_scale(prev_position)
				//if(JSON.stringify(prev_scale) != JSON.stringify(current_scale)){
				if(prev_scale.scale_index!= current_scale.scale_index){
					//delete bc first note of the scale , second note will be new first note
					return DATA_delete_object_index(voice_id,segment_index,abs_position)
				}
				let prev_Na = current_segment_data.sound[prev_position].note
				let prev_diesis = current_segment_data.sound[prev_position].diesis
				let [grade_A,delta_A,reg_A]=DATA_calculate_absolute_note_to_scale(prev_Na,prev_diesis,prev_scale,idea_scale_list)
				let d_grade = current_grade-grade_A+(current_reg-reg_A)*scale_module
			}
		}
	}
	if(found){
		if(current_segment_data.sound[position].note==-2){
			//nothing to do
			return false
		}
		let outside_range = DATA_calculate_D_iNgm_from_object_index(-d_grade,position,current_scale,current_segment_data,Psg_segment_start,N_NP,D_NP,A_global_variables.type).outside_range
		if(outside_range)APP_info_msg("out_N_range")
		current_segment_data.sound[position].note=-2
		current_segment_data.sound[position].A_pos=[]
		current_segment_data.sound[position].A_neg=[]
	}else{
		let outside_range = DATA_calculate_D_iNgm_from_object_index(-d_grade,position,current_scale,current_segment_data,Psg_segment_start,N_NP,D_NP,A_global_variables.type).outside_range
		if(outside_range)APP_info_msg("out_N_range")
		current_segment_data.time.splice(position,1)
		current_segment_data.sound.splice(position,1)
	}
	//save
	voice_data_mod.data.midi_data=DATA_segment_data_to_midi_data(all_segment_data,voice_data_mod.neopulse)
	//var mom = (JSON.parse(JSON.stringify(current_position_PMCRE)))
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
	//data load current_position
	//current_position_PMCRE=mom
	return true

	function _current_voice_segment_position_scale(pos){
		let _P = current_segment_data.time[pos].P
		let _F = current_segment_data.time[pos].F
		//find correct scale
		//find Pa_equivalent and verify if inside a scale
		let _fraction = current_segment_data.fraction.find(fraction=>{return fraction.stop>_P})
		let _frac_p = _fraction.N/_fraction.D
		let _Pa_equivalent = (Psg_segment_start+_P) * N_NP/D_NP + _F* _frac_p * N_NP/D_NP
		//console.log(Pa_equivalent)
		return DATA_get_Pa_eq_scale(_Pa_equivalent)
	}
}

function DATA_calculate_D_iNgm_from_object_index(d_grade,position,current_scale,current_segment_data,Psg_segment_start,N_NP,D_NP,A_type){
	//target_position = position+1 change FROM position
	let target_position = position
	let target_scale = _current_voice_segment_position_scale(target_position)
	let max_pos=current_segment_data.sound.length-1
	//target scale can be == null in this case
	let outside_range=false
	let idea_scale_list=DATA_get_idea_scale_list()
	while (JSON.stringify(target_scale) == JSON.stringify(current_scale)) {
		//calculating iNg
		let target_sound = current_segment_data.sound[target_position]
		let target_Na = target_sound.note
		if(target_Na>=0){
			//need to recalc note
			let target_diesis = target_sound.diesis//no change???
			let [target_grade,target_delta,target_reg]=DATA_calculate_absolute_note_to_scale(target_Na,target_diesis,target_scale,idea_scale_list)
			target_grade+=d_grade
			let result = DATA_calculate_scale_note_to_absolute(target_grade,0,target_reg,target_scale,idea_scale_list)
			let new_target_Na = result.note
			if(result.outside_range)outside_range=true
			//diesis no change??
			//A_pos A_neg no change??
			if(A_type=="Ag"){
				let old_sound=JSON.parse(JSON.stringify(current_segment_data.sound[target_position]))
				current_segment_data.sound[target_position].note=new_target_Na
				//verify condition and translate A grade
				//element inside a scale module
				let new_A = DATA_calculate_A_from_sound_grade_change(old_sound,new_target_Na,null,current_scale,null,idea_scale_list)
				current_segment_data.sound[target_position].A_pos=new_A.A_pos
				current_segment_data.sound[target_position].A_neg=new_A.A_neg
				let result_A = DATA_verify_sound_A(current_segment_data.sound[target_position])
				if(result_A) outside_range=true
			}else{
				current_segment_data.sound[target_position].note=new_target_Na
			}
			let result_A = DATA_verify_sound_A(current_segment_data.sound[target_position])
			if(result_A) outside_range=true
		}
		//calculating new target note
		//find next target scale
		target_position++
		if(target_position>=max_pos){
			target_scale="EXITNOW"
		}else{
			target_scale= _current_voice_segment_position_scale(target_position)
		}
	}
	return {outside_range}
	function _current_voice_segment_position_scale(pos){
		let _P = current_segment_data.time[pos].P
		let _F = current_segment_data.time[pos].F
		//find correct scale
		//find Pa_equivalent and verify if inside a scale
		let _fraction = current_segment_data.fraction.find(fraction=>{return fraction.stop>_P})
		let _frac_p = _fraction.N/_fraction.D
		let _Pa_equivalent = (Psg_segment_start+_P) * N_NP/D_NP + _F* _frac_p * N_NP/D_NP
		//console.log(Pa_equivalent)
		return DATA_get_Pa_eq_scale(_Pa_equivalent)
	}
}

//mod iA XXX




//OVERWRITE
//this family of functions write over existing object(s) starting from a given position
//work with voice index , segment index , pulse position (s) and or it, notes

//overwrite note from -> to cutting whatever exist
function DATA_calculate_overwrite_new_object_PA_PB(voice_id,segment_index,pulse_start,fraction_start,pulse_end,fraction_end,note,diesis,A_pos,A_neg){
	//verify it is not L on 0.0
	if(note==-3 && pulse_start==0 && fraction_start==0)return
	let data = DATA_get_current_state_point(true)
	let voice_data_mod = data.voice_data.find(item=>{
		return item.voice_id==voice_id
	})
	let current_segment_data=voice_data_mod.data.segment_data[segment_index]
	//find how many fraction ranges are involved
	let fraction_changes = 0
	let starting_fraction_range_index=0
	current_segment_data.fraction.find((fraction_range,fraction_range_index)=>{
			if(fraction_range.start <= pulse_start)starting_fraction_range_index=fraction_range_index
			fraction_changes = fraction_range_index - starting_fraction_range_index
			return fraction_range.stop >= pulse_end+fraction_end/100//valid aproximation
		}
	)
	let sound_insert = []
	let time_insert = []
	time_insert.push({P:pulse_start,F:fraction_start})
	sound_insert.push({note,diesis,A_pos,A_neg})
	for (let i = 0; i < fraction_changes; i++) {
		let fraction_range = current_segment_data.fraction[starting_fraction_range_index+i+1]
		time_insert.push({P:fraction_range.start,F:0})
		sound_insert.push({note:-3,diesis,A_pos:[],A_neg:[]})
	}
	//find index position start
	let index_start = 0
	let index_insert = 0
	current_segment_data.time.find(time=>{
		if(time.P==pulse_start && time.F==fraction_start){
			//exact
			index_insert=index_start
			return true
		}
		if(time.P>pulse_start || (time.P==pulse_start && time.F>fraction_start)){
			//need to create a new object silence
			index_insert=index_start
			index_start--
			return true
		}
		index_start++
	})
	//find index position end: if exist nothing , if not add pulse + silence
	let index_end = 0
	current_segment_data.time.find(time=>{
		if(time.P==pulse_end && time.F==fraction_end){
			//exact
			//index_end--
			return true
		}
		if(time.P>pulse_end || (time.P==pulse_end && time.F>fraction_end)){
			//need to create a new object silence
			time_insert.push({P:pulse_end,F:fraction_end})
			sound_insert.push({note:-1,diesis,A_pos:[],A_neg:[]})
			return true
		}
		index_end++
	})
	let array_cut =(index_end-index_insert)
	if(array_cut<0)array_cut=0
	current_segment_data.time.splice(index_insert,array_cut,...time_insert)
	current_segment_data.sound.splice(index_insert,array_cut,...sound_insert)
	voice_data_mod.data.midi_data=DATA_segment_data_to_midi_data(voice_data_mod.data.segment_data,voice_data_mod.neopulse)
	//load
	DATA_insert_new_state_point(data)
	//DATA_load_state_point_data(false,true)
}

////overwrite note from pulse to iT cutting whatever exist (for piano key)
function DATA_overwrite_new_object_PA_iT(voice_id,segment_index,pulse_start,fraction_start,duration_iT,note,diesis,A_pos,A_neg){
	let data = DATA_get_current_state_point(true)
	let voice_data_mod = data.voice_data.find(item=>{
		return item.voice_id==voice_id
	})
	let current_segment_data=voice_data_mod.data.segment_data[segment_index]
	//control if positioned in the last pulse of a segment
	if(current_segment_data.time.slice(-1)[0].P<=pulse_start){
		segment_index++
		if(segment_index>voice_data_mod.data.segment_data.length-1){
			//reached end of voice
			return
		}
		current_segment_data=voice_data_mod.data.segment_data[segment_index]
		pulse_start=0
		fraction_start=0
	}
	//verify if pulse start is correct
	if(!DATA_verify_pulse_position(current_segment_data,pulse_start,fraction_start)){
		return
	}
	let pulse_end= pulse_start
	let fraction_end = fraction_start
	if(duration_iT==0){
		DATA_enter_new_object(voice_id, segment_index, pulse_start, fraction_start, note,diesis,A_pos,A_neg)
		//position accordingly
		current_segment_data.time.find(position=>{
			if(position.P>pulse_start || (position.P==pulse_start && position.F>fraction_start)){
				pulse_end = position.P
				fraction_end = position.F
				return true
			}
		})
	}else{
		//calculate pulse_end,fraction_end
		let fraction_range_index= -1
		let current_fraction_range = current_segment_data.fraction.find(range=>{
			fraction_range_index++
			return range.stop>pulse_start
		})
		//look how many iT are left from current position
		let iT_available = calc_iT_available_fract(current_segment_data.fraction[fraction_range_index],pulse_start,fraction_start)//function
		let end_of_segment=false
		let fraction_start_provv = fraction_start
		while (iT_available<duration_iT) {
			pulse_end=current_fraction_range.stop
			fraction_end=0
			fraction_start_provv=0
			//recalc iT_available and duration_iT
			duration_iT-=iT_available
			if(fraction_range_index<current_segment_data.fraction.length-1){
				fraction_range_index++
			}else{
				end_of_segment=true
				break
			}
			current_fraction_range=current_segment_data.fraction[fraction_range_index]
			iT_available=calc_iT_available_fract(current_fraction_range,pulse_end,fraction_end)
		}
		function calc_iT_available_fract(f_range,P,F){
			return (f_range.stop-P)*f_range.D/f_range.N - F
		}
		if(!end_of_segment){
			fraction_end=(fraction_start_provv+duration_iT)%current_fraction_range.D
			pulse_end+= (fraction_start_provv+duration_iT-fraction_end)*current_fraction_range.N/current_fraction_range.D
		}
		DATA_calculate_overwrite_new_object_PA_PB(voice_id,segment_index,pulse_start,fraction_start,pulse_end,fraction_end, note,diesis,A_pos,A_neg)
		DATA_load_state_point_data(false,true)
	}
	let segment_end_P_abs=0
	let segment_start_P_abs=0
	let Psg_segment_end = 0
	let Psg_segment_start = 0
	let N_NP=voice_data_mod.neopulse.N
	let D_NP=voice_data_mod.neopulse.D
	let index=-1
	voice_data_mod.data.segment_data.find(segment=>{
		segment_start_P_abs=segment_end_P_abs
		//NEOPULSE correction
		segment_end_P_abs +=segment.time.slice(-1)[0].P * N_NP / D_NP
		Psg_segment_start=Psg_segment_end
		Psg_segment_end +=segment.time.slice(-1)[0].P
		index++
		return index==segment_index
	})
	let fraction_range_index= -1
	let current_fraction_range = current_segment_data.fraction.find(range=>{
		fraction_range_index++
		return range.stop>pulse_end
	})
	if(current_fraction_range==null){
		//end segment
		current_fraction_range= {N:1,D:1}
	}
	let pulse_abs=segment_start_P_abs + pulse_end*N_NP/D_NP
	let time = DATA_calculate_exact_time(Psg_segment_start,pulse_end,fraction_end,current_fraction_range.N,current_fraction_range.D,N_NP,D_NP)
	APP_player_to_time(time, pulse_abs,pulse_end,fraction_end,segment_index,voice_id)
}

//INSERT
//this family of functions create a new object in a given position PUSHING existing object
//work with voice index , segment index , pulse position (s) and or it, notes
function DATA_insert_new_object_index_iT(voice_id,segment_index,abs_position,duration_iT,note,diesis,A_pos,A_neg){
	let data = DATA_get_current_state_point(true)
	let voice_data_mod = data.voice_data.find(item=>{
		return item.voice_id==voice_id
	})
	let position = abs_position
	let all_segment_data= voice_data_mod.data.segment_data
	if(segment_index==null){
		//ABSOLUTE position
		//need to calculate segment number and relative position
		segment_index=0
		let prev_segments_n_obj = 0
		all_segment_data.find(segment=>{
			let n_obj = segment.time.length -1
			if(prev_segments_n_obj+n_obj>abs_position){
				return true
			}else{
				//next segment
				prev_segments_n_obj+=n_obj //last element of a segment is .
				segment_index++
				return false
			}
		})
		position = abs_position - prev_segments_n_obj
	}
	let current_segment_data=all_segment_data[segment_index]
	//verify position is not end of segment
	if(current_segment_data.time.length-1<=position){
		return [false,0,]
	}
	//find position time
	//a copy of the data
	let starting_time= {"P":current_segment_data.time[position].P,"F":current_segment_data.time[position].F}
	//find fractioning range
	let current_fraction_range = current_segment_data.fraction.find(range=>{
		return range.stop>starting_time.P
	})
	//find if there is space to place new object
	let provv_next_time = DATA_calculate_next_position_PA_iT(starting_time,duration_iT,current_fraction_range)
	//better solution, works even without checking, it simply cut iT without generating errors
	if(provv_next_time.P<current_fraction_range.stop || (provv_next_time.P==current_fraction_range.stop && provv_next_time.F==0)){
		//add new value and change others
		let position_time_aprox = starting_time.P+ starting_time.F/100
		current_segment_data.time.find((time,index,array)=>{
			if(time.P ==current_fraction_range.stop){
				return true
			}
			let time_aprox = time.P+ time.F/100
			if(time_aprox>=position_time_aprox){
				let current_iT_value = DATA_calculate_interval_PA_PB(time,array[index+1],current_fraction_range)
				if(provv_next_time.P>=current_fraction_range.stop){
					//eliminate element
					array[index]=null
					current_segment_data.sound[index]=null
				}else{
					//move element
					array[index].P=provv_next_time.P
					array[index].F=provv_next_time.F
					//recalculate new next P and F
					provv_next_time = DATA_calculate_next_position_PA_iT(array[index],current_iT_value,current_fraction_range)
				}
			}
		})
		//insert element
		current_segment_data.time.splice(position,0,starting_time)
		current_segment_data.sound.splice(position,0,{note,diesis,A_pos,A_neg})
		//eliminate items ==null
		current_segment_data.time=current_segment_data.time.filter(item=>{return item!=null})
		current_segment_data.sound=current_segment_data.sound.filter(item=>{return item!=null})
		voice_data_mod.data.midi_data=DATA_segment_data_to_midi_data(voice_data_mod.data.segment_data,voice_data_mod.neopulse)
		//load
		DATA_insert_new_state_point(data)
		DATA_load_state_point_data(false,true)
		return [true,,]
	}else{
		return [false,1,{"error_iT_index":position}]
	}
}

function DATA_calculate_next_position_PA_iT(timeA,iT,fraction){
	let next_F=timeA.F+iT
	let rest = next_F%fraction.D
	let delta_P = Math.floor(next_F/fraction.D)*fraction.N
	let next_P=timeA.P+delta_P
	next_F= (rest>=0)? rest : (fraction.D+rest)//this way work for negative iT
	//sometime next_F= -0 !!!
	next_F = Math.abs(next_F)
	//return [next_P,next_F]
	return {"P":next_P,"F":next_F}
}

//COPY IN WD
function DATA_calculate_interval_PA_PB(timeA,timeB,fraction){
	return (timeB.P - timeA.P)  * fraction.D / fraction.N + (timeB.F - timeA.F)
}

function DATA_get_compas_sequence(){
	let data = DATA_get_current_state_point(false)
	//read new compas sequence
	return JSON.parse(JSON.stringify(data.compas))
}

function DATA_get_scale_data(){
	let data = DATA_get_current_state_point(false)
	//read new compas sequence
	return JSON.parse(JSON.stringify(data.scale))
}

function DATA_get_scale_sequence(){
	let data = DATA_get_current_state_point(false)
	//read new compas sequence
	return JSON.parse(JSON.stringify(data.scale.scale_sequence))
}

function DATA_get_idea_scale_list(){
	let data = DATA_get_current_state_point(false)
	return JSON.parse(JSON.stringify(data.scale.idea_scale_list))
}

function DATA_insert_scale_at_pulse(pulse){
	//read database
	let data = DATA_get_current_state_point(true)
	let scale_sequence=data.scale.scale_sequence
	let scale_start=0
	let insert_index=null
	let insert_scale=BNAV_insert_scale_use_current_selected_scale_data()
	let last_compas= data.compas[data.compas.length-1]
	let end_last_compas_abs_pulse = last_compas.compas_values[0]+last_compas.compas_values[1]*last_compas.compas_values[2]
	let max_abs_pulse=(Li>end_last_compas_abs_pulse)?Li:end_last_compas_abs_pulse
	let found=scale_sequence.find((scale,index)=>{
		let scale_end=scale_start+scale.duration
		if(scale_start<pulse && scale_end>pulse){
			if(insert_scale==null)insert_scale=JSON.parse(JSON.stringify(scale))
			insert_scale.duration=scale_end-pulse
			scale.duration=pulse-scale_start
			insert_index=index+1
			//scale_sequence.splice(insert_index,0,insert_scale)
			//verify scale ghost
			//save new database
			// DATA_insert_new_state_point(data)
			//redraw/re align S lines
			// DATA_load_state_point_data(false,true)
			// BNAV_refresh_S_composition(DATA_get_scale_data())
			// BNAV_find_and_select_scale_composition(insert_scale.acronym,insert_scale.rotation,insert_scale.starting_note)
			return true
		}
		scale_start=scale_end
	})
	if(found!=null){
		return {success:false,insert_scale,original_scale:found,insert_index}
	}
	if(found==null && insert_scale!=null){
		//adding inside a ghost scale: add 2 scales to scale sequence
		if(scale_sequence.length==0){
			insert_scale.duration=pulse
			scale_sequence.push(JSON.parse(JSON.stringify(insert_scale)))
			insert_scale.duration=max_abs_pulse-pulse
			scale_sequence.push(JSON.parse(JSON.stringify(insert_scale)))
		}else{
			//expand last scale
			scale_sequence[scale_sequence.length-1].duration+=(pulse-scale_start)
			//add new scale
			insert_scale.duration=max_abs_pulse-pulse
			//old without return
			//scale_sequence.push(JSON.parse(JSON.stringify(insert_scale)))
			return {success:false,insert_scale:JSON.parse(JSON.stringify(insert_scale)),original_scale:scale_sequence[scale_sequence.length-1],insert_index:scale_sequence.length}
		}
		//verify scale ghost no need, ghost deleted
		//BNAV_check_ghost(data.scale.scale_sequence,max_abs_pulse)
		//save new database
		DATA_insert_new_state_point(data)
		//redraw/re align S lines
		DATA_load_state_point_data(false,true)
		BNAV_refresh_S_composition(DATA_get_scale_data())
		BNAV_find_and_select_scale_composition(insert_scale.acronym,insert_scale.rotation,insert_scale.starting_note)
	}
	return {success:true}
}

function DATA_insert_scale_at_pulse_extra(insert_data){
	let data = DATA_get_current_state_point(true)
	let scale_sequence=data.scale.scale_sequence
	if(insert_data.translate_grade){
		//translate grade
		scale_sequence[insert_data.insert_index-1].duration=insert_data.original_scale.duration
		scale_sequence.splice(insert_data.insert_index,0,insert_data.original_scale)
		scale_sequence[insert_data.insert_index].duration=insert_data.insert_scale.duration
		DATA_translate_scale_sequence_at_index(insert_data.insert_index,insert_data.insert_scale,data)
	}else{
		//mantain note
		scale_sequence[insert_data.insert_index-1].duration=insert_data.original_scale.duration
		scale_sequence.splice(insert_data.insert_index,0,insert_data.insert_scale)
		//save new database
		DATA_insert_new_state_point(data)
		//redraw/re align S lines
		DATA_load_state_point_data(false,true)
		BNAV_refresh_S_composition(DATA_get_scale_data())
		BNAV_find_and_select_scale_composition(insert_data.insert_scale.acronym,insert_data.insert_scale.rotation,insert_data.insert_scale.starting_note)
	}
}

function DATA_modify_scale_sequence_at_index(scale_index,scale_data){
	//verify data
	let result = DATA_verify_scale_circle_data(scale_index,scale_data)
	if (result==false)return
	scale_data=result
	if(!DATA_verify_scale_circle_data_changed(scale_index,scale_data))return
	let data = DATA_get_current_state_point(true)
	data.scale.scale_sequence[scale_index]=scale_data
	DATA_insert_new_state_point(data)
	//redraw/re align S lines
	DATA_load_state_point_data(true,true)
}

function DATA_translate_scale_sequence_at_index(scale_index,scale_data,data=null){
	//verify data
	let result = DATA_verify_scale_circle_data(scale_index,scale_data)
	if (result==false)return
	scale_data=result
	if(data==null){
		//MODIFICATION/TRANSLATION existing scale: verify if data changed
		if(!DATA_verify_scale_circle_data_changed(scale_index,scale_data))return
	}//else is add new scale
	data = (data==null)?DATA_get_current_state_point(true):data
	data.scale.scale_sequence[scale_index]=scale_data
	//copy
	let new_scale_sequence=JSON.parse(JSON.stringify(data.scale.scale_sequence))
	let outside_range=false
	data.voice_data.forEach(voice=>{
		if(!voice.blocked)return
		let N_NP=voice.neopulse.N
		let D_NP=voice.neopulse.D
		let Psg_segment_start=0
		voice.data.segment_data.forEach(segment=>{
			segment.sound.forEach((sound,index)=>{
				if(sound.note<0)return
				//determine Pa
				let time = segment.time[index]
				let fraction = segment.fraction.find(fraction=>{return fraction.stop>time.P})
				if (typeof(fraction) == "undefined"){
					//end segment
					return true
				}
				//calculating position
				frac_p = fraction.N/fraction.D
				let Pa_equivalent = (Psg_segment_start+time.P) * N_NP/D_NP + time.F* frac_p * N_NP/D_NP
				//determine old scale
				let old_scale=DATA_get_Pa_eq_scale(Pa_equivalent)//no null case
				//determine new scale
				let new_scale=DATA_get_Pa_eq_scale(Pa_equivalent,new_scale_sequence)
				//determine new note
				let result=DATA_calculate_sound_translation_scale(sound,old_scale,new_scale,data.scale.idea_scale_list,true)
				if(result.outside_range)outside_range=true
				segment.sound[index]=result.sound
			})
			Psg_segment_start+=segment.time.slice(-1)[0].P
		})
		//midi
		voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
	})
	if(outside_range)APP_info_msg("out_N_range")
	//save new database
	DATA_insert_new_state_point(data)
	//redraw/re align S lines
	DATA_load_state_point_data(false,true)
}

function DATA_verify_scale_circle_data(scale_index,scale_data){
	//verify data
	//letter
	let idea_scale_list=DATA_get_idea_scale_list()
	let already_used = idea_scale_list.some(item=>{
		return item.acronym===scale_data.acronym
	})
	if(!already_used)return false
	//starting note
	scale_data.starting_note=scale_data.starting_note%TET
	//rotation
	let module=idea_scale_list.find(item=>{
		return item.acronym===scale_data.acronym
	}).module
	scale_data.rotation=scale_data.rotation%module
	//duration
	if(scale_data.duration<=0){
		return false
	}
	return scale_data
}

function DATA_verify_scale_circle_data_changed(scale_index,scale_data){
	let current_scale_sequence=DATA_get_scale_sequence()
	if(scale_index>=current_scale_sequence.length || scale_index<0)return false
	let old_scale=current_scale_sequence[scale_index]
	if(old_scale.acronym==scale_data.acronym && old_scale.duration==scale_data.duration && old_scale.starting_note==scale_data.starting_note && old_scale.rotation==scale_data.rotation)return false
	return true

}

function DATA_list_voice_data_segment_change(voice_data){
	let time_segment_change= []
	let data = (JSON.parse(JSON.stringify(voice_data.data.segment_data)))
	let pre = 0
	time_segment_change = data.map(segment=>{
		if(segment==null)return
		let P = segment.time.pop().P
		let value = DATA_calculate_exact_time(pre,P,0,1,1,voice_data.neopulse.N,voice_data.neopulse.D)
		pre += P
		return value
	})
	time_segment_change.unshift(0)
	time_segment_change.pop()
	return time_segment_change
}

function DATA_list_voice_data_metro_fractioning(voice_data){//copy in WEB
	//var fractioning = [{"time": "", "freq":"", "rep":""}]
	let fractioning = []
	let data = (JSON.parse(JSON.stringify(voice_data.data.segment_data)))
	let fractioning_data_raw = []
	data.forEach(segment=>{
		segment.fraction.forEach(F_range_raw=>{
			fractioning_data_raw.push(F_range_raw)
		})
	})
	let D_NP = voice_data.neopulse.D
	let N_NP = voice_data.neopulse.N
	let prev_time_range_end = 0
	fractioning = fractioning_data_raw.map(raw=>{
		//calcolate starting time
		let time = DATA_calculate_exact_time(prev_time_range_end,0,0,1,1,N_NP,D_NP)
		//actualize range and next fractioning
		let range = raw.stop-raw.start
		prev_time_range_end += range
		let time2 = DATA_calculate_exact_time(prev_time_range_end,0,0,1,1,N_NP,D_NP)
		let rep = range* raw.D/raw.N
		let freq = rep/(time2-time)
		return {time, freq, rep}
	})
	return fractioning
}

//Harmonic Voices
function DATA_change_type_A(type=null){
	//use A_global_variables
	if(type==null){
		//rotation....obsolete
		let type_list=["Aa","Am","Ag"]
		let index= type_list.indexOf(A_global_variables.type)
		let next_index=(index==type_list.length-1)?0:(index+1)
		A_global_variables.type=type_list[next_index]
	}else{
		if(A_global_variables.type==type)return //nothing to do
		A_global_variables.type=type
	}
	flag_DATA_updated.RE=false
	flag_DATA_updated.PMC=false
	DATA_smart_redraw()
	/*
	let data = DATA_get_current_state_point(true)
	if(type==null){
		//rotation....obsolete
		let type_list=["Aa","Am","Ag"]
		let index= type_list.indexOf(data.global_variables.A.type)
		let next_index=(index==type_list.length-1)?0:(index+1)
		data.global_variables.A.type=type_list[next_index]
	}else{
		if(data.global_variables.A.type==type)return //nothing to do
		data.global_variables.A.type=type
	}
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)*/
}

function DATA_change_mute_A(){
	let data = DATA_get_current_state_point(true)
	data.global_variables.A.mute=!data.global_variables.A.mute
	mute_A=data.global_variables.A.mute
	data.voice_data.forEach(voice=>{
			voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
	})
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
	//A_global_variables changed by loading
}

function DATA_change_show_A(){
	//use A_global_variables
	A_global_variables.show=!A_global_variables.show
	//if(A_global_variables.show)A_global_variables.show_note=true
	flag_DATA_updated.RE=false
	flag_DATA_updated.PMC=false
	console.log(A_global_variables)
	DATA_smart_redraw()
}

function DATA_change_show_A_note(){
	//use A_global_variables
	A_global_variables.show_note=!A_global_variables.show_note
	flag_DATA_updated.RE=false
	flag_DATA_updated.PMC=false
	DATA_smart_redraw()
}

function DATA_voice_add_A(voice_id){
	APP_stop()
	let data = DATA_get_current_state_point(true)
	//Selected voice
	let voice= data.voice_data.find(item=>{
		return item.voice_id==voice_id
	})
	if(voice==null)return
	if(voice.A)return
	voice.A=true
	data.global_variables.A.show=true
	data.global_variables.A.show_note=true
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_voice_delete_A(voice_id){
	APP_stop()
	let data = DATA_get_current_state_point(true)
	//Selected voice
	let voice= data.voice_data.find(item=>{
		return item.voice_id==voice_id
	})
	if(voice==null)return
	voice.A=false
	voice.data.segment_data.forEach(segment=>{
		segment.sound.forEach(item=>{
			item.A_pos=[]
			item.A_neg=[]
		})
	})
	voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_voice_explode_A(voice_id){
	APP_stop()
	let data = DATA_get_current_state_point(true)
	//Selected voice
	let voice= data.voice_data.find(item=>{
		return item.voice_id==voice_id
	})
	if(voice==null)return
	voice.A=false
	//duplicate voice and add harmony
	let pos_A_voices_note_list=[]
	let neg_A_voices_note_list=[]
	voice.data.segment_data.forEach(segment=>{
		segment.sound.forEach(item=>{
			if(item.note==-4)return
			let current_note=item.note
			let note_list=[]
			item.A_pos.forEach(A_pos=>{
				current_note+=A_pos
				note_list.push(current_note)
			})
			pos_A_voices_note_list.push(note_list)
			current_note=item.note
			note_list=[]
			item.A_neg.forEach(A_neg=>{
				current_note-=A_neg
				note_list.push(current_note)
			})
			neg_A_voices_note_list.push(note_list)
			//pos_A_voices_list.push(item.A_pos)
			//neg_A_voices_list.push(item.A_neg)
		})
	})
	let n_voices_pos = pos_A_voices_note_list.reduce((max,item)=>{return Math.max(max,item.length)},0)
	let n_voices_neg = neg_A_voices_note_list.reduce((max,item)=>{return Math.max(max,item.length)},0)
	//create voices needed
	//remove solo
	data.voice_data.forEach(voice=>{
		voice.solo=false
	})
	//duplicate current voice
	let voice_position = -1
	let voice_copy = JSON.parse(JSON.stringify(data.voice_data.find(item=>{
		voice_position++
		return item.voice_id==voice_id
	})))
	//clear voice
	voice_copy.solo= false
	voice_copy.selected=false
	voice_copy.data.segment_data.forEach(segment=>{
		segment.sound.forEach(sound=>{
			sound.A_pos=[]
			sound.A_neg=[]
			sound.note=(sound.note==-4)?-4:-1
		})
	})
	let voice_id_list = data.voice_data.map(item=>{
		let number = Number(item.voice_id)
		return number
	})
	let new_voice_id = Math.max(...voice_id_list)+1
	console.log(pos_A_voices_note_list)
	for (let i = 0; i<n_voices_pos; i++) {
		let voice_data_copy=JSON.parse(JSON.stringify(voice_copy))
		voice_data_copy.voice_id = new_voice_id
		new_voice_id++
		let string = voice_data_copy.name+"#"
		voice_data_copy.name= string.slice(0,8)
		//same color
		// let new_color = DATA_calculate_data_new_color(data)
		// if(new_color!=null){
		// 	voice_data_copy.color=new_color
		// }
		//add current notes
		let index=0
		voice_data_copy.data.segment_data.forEach(segment=>{
			segment.sound.forEach(sound=>{
				if(sound.note!=-4){
					let target_note=pos_A_voices_note_list[index][i]
					if(target_note!=null)sound.note=target_note
					index++
				}
			})
		})
		_clear_voice_silences(voice_data_copy)
		voice_data_copy.data.midi_data=DATA_segment_data_to_midi_data(voice_data_copy.data.segment_data,voice_data_copy.neopulse)
		data.voice_data.splice(voice_position,0,voice_data_copy)
	}
	for (let i = 0; i<n_voices_neg; i++) {
		let voice_data_copy=JSON.parse(JSON.stringify(voice_copy))
		voice_data_copy.voice_id = new_voice_id
		new_voice_id++
		let string = voice_data_copy.name+"#"
		voice_data_copy.name= string.slice(0,8)
		//same color
		// let new_color = DATA_calculate_data_new_color(data)
		// if(new_color!=null){
		// 	voice_data_copy.color=new_color
		// }
		//add current notes
		let index=0
		voice_data_copy.data.segment_data.forEach(segment=>{
			segment.sound.forEach(sound=>{
				if(sound.note!=-4){
					let target_note=neg_A_voices_note_list[index][i]
					if(target_note!=null)sound.note=target_note
					index++
				}
			})
		})
		_clear_voice_silences(voice_data_copy)
		voice_data_copy.data.midi_data=DATA_segment_data_to_midi_data(voice_data_copy.data.segment_data,voice_data_copy.neopulse)
		data.voice_data.splice(voice_position+n_voices_pos+i+1,0,voice_data_copy)
	}
	//delete harmony
	voice.data.segment_data.forEach(segment=>{
		segment.sound.forEach(item=>{
			item.A_pos=[]
			item.A_neg=[]
		})
	})
	voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
	function _clear_voice_silences(voice){
		voice.data.segment_data.forEach(segment=>{
			let clear_seg=true
			segment.sound.forEach(sound=>{
				if(sound.note>=0){
					clear_seg=false
				}
			})
			if(clear_seg){
				//reset fractioning
				let frac = segment.fraction.pop()
				frac.N=1
				frac.D=1
				frac.start=0
				segment.fraction=[]
				segment.fraction.push(frac)
				//reset sound
				segment.sound=[]
				segment.sound.push({note:-1,diesis:true,A_pos:[],A_neg:[]})
				segment.sound.push({note:-4,diesis:true,A_pos:[],A_neg:[]})
				//reset time
				segment.time=[]
				segment.time.push({"P":0,"F":0})
				segment.time.push({"P":frac.stop,"F":0})
			}
		})
	}
}

//A manipolation
function DATA_verify_sound_A(sound){
	let provv=sound.note
	if(provv<0){
		sound.A_pos=[]
		sound.A_neg=[]
		return false
	}
	let new_A_pos=[]
	let new_A_neg=[]
	let o_1=sound.A_pos.some(A=>{
		provv+=A
		if(provv>=TET*N_reg)return true
		new_A_pos.push(A)
	})
	provv=sound.note
	let o_2=sound.A_neg.some(A=>{
		provv-=A
		if(provv<0)return true
		new_A_neg.push(A)
	})
	sound.A_pos=new_A_pos
	sound.A_neg=new_A_neg
	return o_1 || o_2
}

function DATA_verify_voice_A(voice,force_A_off=false){
	if(!voice.A || (voice.A && force_A_off)){
		voice.A=voice.data.segment_data.some(segment=>{
			return segment.sound.some(sound=>{
				if(!!sound.A_pos.length || !!sound.A_neg.length){
					//voice has A pos or neg
					return true
				}
			})
		})
	}
}

//for PMC A modification
function DATA_calculate_modify_object_position_A_delta(voice_data_mod,segment_index,P,F,delta,old_A_info){
	let A_pos=old_A_info[0]
	let A_index=old_A_info[1]
	let all_segment_data= voice_data_mod.data.segment_data
	let current_segment_data=all_segment_data[segment_index]
	let position = current_segment_data.time.findIndex(item=>{return item.P==P && item.F==F})
	if(position==-1)return {success:false}
	let outside_range=false
	let current_note = current_segment_data.sound[position].note
	let old_A_value=0
	let new_value=0
	if(A_pos){
		old_A_value=current_segment_data.sound[position].A_pos[A_index]
		new_value=old_A_value+delta
	}else{
		old_A_value=current_segment_data.sound[position].A_neg[A_index]
		new_value=old_A_value-delta
	}
	if(new_value<1)new_value=1
	if(A_pos){
		current_segment_data.sound[position].A_pos[A_index]=new_value
	}else{
		current_segment_data.sound[position].A_neg[A_index]=new_value
	}
	let o_r = DATA_verify_sound_A(current_segment_data.sound[position])
	voice_data_mod.data.midi_data=DATA_segment_data_to_midi_data(all_segment_data,voice_data_mod.neopulse)
	return {success:true,outside_range:o_r}
}

function DATA_calculate_modify_object_position_A_note_delta(voice_data_mod,segment_index,P,F,delta,old_A_note=-1){
	let all_segment_data= voice_data_mod.data.segment_data
	let current_segment_data=all_segment_data[segment_index]
	//calculate iNa segment
	//let iNa_list=DATA_calculate_iNa_from_Na_list(current_segment_data.sound.map(s=>{return s.note}))
	let position = current_segment_data.time.findIndex(item=>{return item.P==P && item.F==F})
	if(position==-1)return {success:false}
	let outside_range=false
	let current_note = current_segment_data.sound[position].note

	if(old_A_note!=-1){
		//modify Harmonic voice
		let A_list=[]
		if(old_A_note<current_note){
			//mod A_neg
			A_list=current_segment_data.sound[position].A_neg
		}else{
			//mod A_pos
			A_list=current_segment_data.sound[position].A_pos
		}
		let note_list=[]
		let provv=current_note
		if(old_A_note<current_note){
			A_list.forEach(val=>{
				provv-=val
				note_list.push(provv)
			})
		}else{
			A_list.forEach(val=>{
				provv+=val
				note_list.push(provv)
			})
		}
		let index=note_list.indexOf(old_A_note)
		if(index!=-1){
			//take out old note
			note_list.splice(index,1)
			let provv=current_note
			let new_A_list=[]
			if(old_A_note<current_note){
				note_list.forEach(item=>{
					new_A_list.push(provv-item)
					provv=item
				})
				current_segment_data.sound[position].A_neg=new_A_list
				//modify delta to reflect real position
				delta+=old_A_note-current_note
			}else{
				note_list.forEach(item=>{
					new_A_list.push(item-provv)
					provv=item
				})
				current_segment_data.sound[position].A_pos=new_A_list
				//modify delta to reflect real position
				delta+=old_A_note-current_note
			}
		}else{
			return {success:false}
		}
	}

	let new_note=current_note+delta
	if(new_note<0 || new_note>TET*N_reg-1)outside_range=true
	if(new_note<0){
		new_note=0
		delta=0-current_note
	}
	if(new_note>TET*N_reg-1){
		new_note=TET*N_reg-1
		delta=TET*N_reg-1-current_note
	}

	if(delta==0){
		if(old_A_note!=-1){
			voice_data_mod.data.midi_data=DATA_segment_data_to_midi_data(all_segment_data,voice_data_mod.neopulse)
			return {success:true,data_change:true}
		}
		//nothing to do
		return {success:true,data_change:false}
	}
	//insert new Harmonic note
	if(delta>0){
		let A_pos=current_segment_data.sound[position].A_pos
		let pos_note_list=[]
		let provv=current_note
		A_pos.forEach(val=>{
			provv+=val
			pos_note_list.push(provv)
		})
		let index=pos_note_list.indexOf(new_note)
		if(index==-1){
			pos_note_list.push(new_note)
			let sorted_list=pos_note_list.sort()
			let provv=current_note
			let new_A_pos=[]
			sorted_list.forEach(item=>{
				new_A_pos.push(item-provv)
				provv=item
			})
			current_segment_data.sound[position].A_pos=new_A_pos
			voice_data_mod.A=true
		}else{
			//nothing to do
			return {success:true,data_change:false}
		}
	}
	if(delta<0){
		let A_neg=current_segment_data.sound[position].A_neg
		let neg_note_list=[]
		let provv=current_note
		A_neg.forEach(val=>{
			provv-=val
			neg_note_list.push(provv)
		})
		let index=neg_note_list.indexOf(new_note)
		if(index==-1){
			neg_note_list.push(new_note)
			let sorted_list=neg_note_list.sort().reverse()
			let provv=current_note
			let new_A_neg=[]
			sorted_list.forEach(item=>{
				new_A_neg.push(provv-item)
				provv=item
			})
			current_segment_data.sound[position].A_neg=new_A_neg
			//voice_data_mod.A_neg=true
			voice_data_mod.A=true
		}else{
			//nothing to do
			return {success:true,data_change:false}
		}
	}
	voice_data_mod.data.midi_data=DATA_segment_data_to_midi_data(all_segment_data,voice_data_mod.neopulse)
	return {success:true,outside_range,data_change:true}
}

// function DATA_calculate_modify_object_position_A_delta_grade(voice_data_mod,segment_index,P,F,delta,old_A_note=-1){//XXX XXX XXX XXX XXX
	//implement if snap PMC MOUSE needed
// }

function DATA_calculate_object_index_Ag_list_to_A_list(voice_id,segment_index,abs_position,positive,Ag_list){
	let data = DATA_get_current_state_point(true)
	let position = abs_position
	let voice_data_mod = data.voice_data.find(item=>{
		return item.voice_id==voice_id
	})
	let all_segment_data= voice_data_mod.data.segment_data
	if(segment_index==null){
		//absolute position
		//need to calculate segment number and relative position
		segment_index=0
		let prev_segments_n_obj = 0
		all_segment_data.find(segment=>{
			let n_obj = segment.time.length -1
			if(prev_segments_n_obj+n_obj>abs_position){
				return true
			}else{
				//next segment
				prev_segments_n_obj+=n_obj //last element of a segment is .
				segment_index++
				return false
			}
		})
		position = abs_position - prev_segments_n_obj
	}
	//traduction already done
	let note_Na=all_segment_data[segment_index].sound[position].note
	if(note_Na<0){
		return []
	}

	let current_scale = _current_voice_segment_position_scale(position)
	if(current_scale==null)return null//it doesn't modify current scale already done by APP_text_to_A_list function
	let A_list=[]
	let current_note=note_Na
	let idea_scale_list=data.scale.idea_scale_list
	let [current_grade,current_delta,current_reg]=DATA_calculate_absolute_note_to_scale(note_Na,all_segment_data[segment_index].sound[position].diesis,current_scale,idea_scale_list)
	Ag_list.forEach(item=>{
		//doesn't matter if reg doesnt change
		current_grade=positive?(current_grade+item):(current_grade-item)
		let result = DATA_calculate_scale_note_to_absolute(current_grade,0,current_reg,current_scale,idea_scale_list)
		let delta=Math.abs(current_note-result.note)
		if(delta!=0)A_list.push(delta)
		current_note=result.note
	})
	return A_list
	function _current_voice_segment_position_scale(pos){
		let N_NP = voice_data_mod.neopulse.N
		let D_NP = voice_data_mod.neopulse.D
		let current_segment_data=all_segment_data[segment_index]
		let Psg_segment_start = 0
		for (let i = 0; i < segment_index; i++) {
			Psg_segment_start+=all_segment_data[i].time.slice(-1)[0].P
		}
		let _P = current_segment_data.time[pos].P
		let _F = current_segment_data.time[pos].F
		//find correct scale
		//find Pa_equivalent and verify if inside a scale
		let _fraction = current_segment_data.fraction.find(fraction=>{return fraction.stop>_P})
		let _frac_p = _fraction.N/_fraction.D
		let _Pa_equivalent = (Psg_segment_start+_P) * N_NP/D_NP + _F* _frac_p * N_NP/D_NP
		//console.log(Pa_equivalent)
		return DATA_get_Pa_eq_scale(_Pa_equivalent)
	}
}

function DATA_delete_object_index_A(voice_id,segment_index,abs_position,A_pos=true,A_neg=true){
	let data = DATA_get_current_state_point(true)
	let position = abs_position
	let voice_data_mod = data.voice_data.find(item=>{
		return item.voice_id==voice_id
	})
	let all_segment_data= voice_data_mod.data.segment_data
	if(segment_index==null){
		//absolute position
		//need to calculate segment number and relative position
		segment_index=0
		let prev_segments_n_obj = 0
		all_segment_data.find(segment=>{
			let n_obj = segment.time.length -1
			if(prev_segments_n_obj+n_obj>abs_position){
				return true
			}else{
				//next segment
				prev_segments_n_obj+=n_obj //last element of a segment is .
				segment_index++
				return false
			}
		})
		position = abs_position - prev_segments_n_obj
	}
	let current_segment_data=all_segment_data[segment_index]
	let success=false

	if(A_neg && !(current_segment_data.sound[position].A_neg==[])){current_segment_data.sound[position].A_neg=[];success=true}
	if(A_pos && !(current_segment_data.sound[position].A_pos==[])){current_segment_data.sound[position].A_pos=[];success=true}
	if(!success)return false
	voice_data_mod.data.midi_data=DATA_segment_data_to_midi_data(all_segment_data,voice_data_mod.neopulse)
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
	return true
}
