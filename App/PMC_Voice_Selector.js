function PMC_press_vsb_N(button){
	if(button.classList.contains("pressed")){
		button.classList.remove("pressed")
	} else {
		button.classList.add("pressed")
	}

	if(APP_read_PMC_extra_info()){
		PMC_redraw_all_voices_svg()
	}
	PMC_redraw_selected_voice()
}

function PMC_press_vsb_Nabc(button){
	if(button.classList.contains("pressed")){
		button.classList.remove("pressed")
	} else {
		button.classList.add("pressed")
	}
	PMC_redraw_Nabc()
}

function PMC_set_vsb_N(value){
	//change in side panel
	let container = document.querySelector(".App_PMC_vsb_N_type")
	let buttons = [...container.querySelectorAll(".App_PMC_vsb_N_type_button")]
	let button_Nabc = document.querySelector("#App_PMC_vsb_Nabc")
	buttons.forEach(el=>{
		if(el.classList.contains("pressed")){
			el.classList.remove("pressed")
		}
	})
	buttons.find(bttn=>{
		if(bttn.value==value.type){
			bttn.classList.add("pressed")
			return true
		}
	})
	//change in voice selector
	let button = document.querySelector("#App_PMC_vsb_N")
	if(value.type=="Na")button.innerHTML=value.type
	if(value.type=="Nm")button.innerHTML="Nm"
	if(value.type=="Ngm")button.innerHTML="N°"
	if(value.show && !button.classList.contains("pressed"))button.classList.add("pressed")
	if(!value.show && button.classList.contains("pressed"))button.classList.remove("pressed")
}

function PMC_set_vsb_Nabc(value_abc){
	//change in side panel
	let button = document.querySelector("#App_PMC_vsb_Nabc")
	if(value_abc){
		button.classList.add("pressed")
	} else {
		button.classList.remove("pressed")
	}
}

function PMC_read_vsb_N(){
	let container = document.querySelector(".App_PMC_vsb_N_type")
	let button = container.querySelector(".App_PMC_vsb_N_type_button.pressed")
	let type = button.value
	let show = document.querySelector("#App_PMC_vsb_N").classList.contains("pressed")
	return {show,type}
}

function PMC_read_vsb_Nabc(){
	let button = document.querySelector("#App_PMC_vsb_Nabc")
	let type = false
	if(button.classList.contains("pressed"))type=true
	return type
}

function PMC_press_vsb_iN(button){
	if(button.classList.contains("pressed")){
		button.classList.remove("pressed")
	} else {
		button.classList.add("pressed")
	}
	PMC_redraw_selected_voice()
}

function PMC_set_vsb_iN(value){
	//change in side panel too
	let container = document.querySelector(".App_PMC_vsb_iN_type")
	let buttons = [...container.querySelectorAll(".App_PMC_vsb_iN_type_button")]
	buttons.forEach(el=>{
		if(el.classList.contains("pressed")){
			el.classList.remove("pressed")
		}
	})
	buttons.find(bttn=>{
		if(bttn.value==value.type){
			bttn.classList.add("pressed")
			return true
		}
	})
	//change in voice selector
	let button = document.querySelector("#App_PMC_vsb_iN")
	if(value.type=="iNa")button.innerHTML="iSa"
	if(value.type=="iNm")button.innerHTML="iSm"
	if(value.type=="iNgm")button.innerHTML="iS°"
	if(value.show && !button.classList.contains("pressed"))button.classList.add("pressed")
	if(!value.show && button.classList.contains("pressed"))button.classList.remove("pressed")
}

function PMC_read_vsb_iN(){
	let container = document.querySelector(".App_PMC_vsb_iN_type")
	let button = container.querySelector(".App_PMC_vsb_iN_type_button.pressed")
	let type = button.value
	let show = document.querySelector("#App_PMC_vsb_iN").classList.contains("pressed")
	return {show,type}
}

function PMC_press_vsb_T(button){
	if(button.classList.contains("pressed")){
		button.classList.remove("pressed")
	} else {
		button.classList.add("pressed")
	}
	PMC_redraw_selected_voice()
}

function PMC_set_vsb_T(value){
	//change in side panel
	let container = document.querySelector(".App_PMC_vsb_T_type")
	let buttons = [...container.querySelectorAll(".App_PMC_vsb_T_type_button")]
	buttons.forEach(el=>{
		if(el.classList.contains("pressed")){
			el.classList.remove("pressed")
		}
	})
	buttons.find(bttn=>{
		if(bttn.value==value.type){
			bttn.classList.add("pressed")
			return true
		}
	})
	//change in voice selector
	let button = document.querySelector("#App_PMC_vsb_T")
	if(value.type=="Ta")button.innerHTML="Pa"
	if(value.type=="Tm")button.innerHTML="Pm"
	if(value.type=="Ts")button.innerHTML="Psg"
	if(value.show && !button.classList.contains("pressed"))button.classList.add("pressed")
	if(!value.show && button.classList.contains("pressed"))button.classList.remove("pressed")
}

function PMC_read_vsb_T(){
	let container = document.querySelector(".App_PMC_vsb_T_type")
	let button = container.querySelector(".App_PMC_vsb_T_type_button.pressed")
	let type = button.value
	let show = document.querySelector("#App_PMC_vsb_T").classList.contains("pressed")
	return {show,type}
}

function PMC_press_vsb_iT(button){
	if(button.classList.contains("pressed")){
		button.classList.remove("pressed")
	} else {
		button.classList.add("pressed")
	}
	if(APP_read_PMC_extra_info()){
		PMC_redraw_all_voices_svg()
	}
	PMC_redraw_selected_voice()
}

function PMC_set_vsb_iT(value){
	let button = document.querySelector("#App_PMC_vsb_iT")
	//ONLY OPTION
	if(value.show && !button.classList.contains("pressed"))button.classList.add("pressed")
	if(!value.show && button.classList.contains("pressed"))button.classList.remove("pressed")
}

function PMC_read_vsb_iT(){
	let type = "iT" //only option
	let show = document.querySelector("#App_PMC_vsb_iT").classList.contains("pressed")
	return {show,type}
}

function PMC_select_type_N_button(bttn){
	let button = document.querySelector("#App_PMC_vsb_N")
	let value={"show":button.classList.contains("pressed"),"type":bttn.value}
	PMC_set_vsb_N(value)
	if(APP_read_PMC_extra_info()){
		PMC_redraw_all_voices_svg()
	}
	PMC_write_sound_line_values()
	PMC_redraw_selected_voice()
	PMC_redraw_selected_voice_sequence()
}

function PMC_select_type_iN_button(bttn){
	let button = document.querySelector("#App_PMC_vsb_N")
	let value={"show":button.classList.contains("pressed"),"type":bttn.value}
	PMC_set_vsb_iN(value)
	PMC_redraw_selected_voice()
}

function PMC_select_type_T_button(bttn){
	let button = document.querySelector("#App_PMC_vsb_T")
	let value={"show":button.classList.contains("pressed"),"type":bttn.value}
	PMC_set_vsb_T(value)
	PMC_redraw_time_line()
	PMC_redraw_selected_voice()
}

function PMC_select_type_iT_button(button){
	if(button.classList.contains("pressed")){
		//remove
		//button.classList.remove("pressed")
	}else{
		//add
		button.classList.add("pressed")
	}
	//nothing to do , only 1 option
}

function PMC_populate_voice_list(){
	let data = DATA_get_current_state_point(false)
	let voice_list = document.querySelector(".App_PMC_voice_selector_voice_list")
	voice_list.innerHTML= ""
	let voice_list_string = ""
	data.voice_data.forEach((voice, index) =>{
		let checked=""
		let mutesolo=""
		let eye_inner='<img src="./Icons/side_eye_hover.svg">'
		let eye_class="App_PMC_voice_selector_voice_item_eyeIcon"
		let controller=""
		if(voice.selected){
			checked="checked"
			let inst_name= instrument_name_list[voice.instrument]
			let voice_blocked_class= voice.blocked?"pressed":""
			let voice_blocked_img0= voice.blocked?"flex":"none"
			let voice_blocked_img1= voice.blocked?"none":"flex"
			let np_value=voice.neopulse.N+"/"+voice.neopulse.D
			let metro_class=(voice.metro==0)?'App_PMC_text_strikethrough':""
			let metro_text=(voice.metro==0)?"M":("M"+voice.metro)
			let e_sound_text=(voice.e_sound==22)?"e":"e"+voice.e_sound
			let class_mute=(voice.mute)?"App_PMC_selected_voice_mute pressed":"App_PMC_selected_voice_mute"
			let class_solo=(voice.solo)?"App_PMC_selected_voice_solo pressed":"App_PMC_selected_voice_solo"
			let A_disabled=(voice.A)?"disabled":""
			let A_provv_extra_buttons="" //XXX provv , recolocate them
			if(voice.A){
				A_provv_extra_buttons=`	<button class="App_PMC_selected_voice_A_explode" onclick="PMC_selected_voice_explode_A()">
						<img src="./Icons/bomb.svg"/>
					</button>
					<button class="App_PMC_selected_voice_delete_A" onclick="PMC_selected_voice_delete_A()">
						<img class="App_RE_icon" src="./Icons/delete.svg"/>
					</button>
				`
			}
			let instrument_disabled=(soundFontLoaded)?"":'disabled="true"'
			controller=`<div class="App_PMC_selected_voice_controller_sound_container">
							<button class="App_PMC_selected_voice_instrument" onclick="PMC_change_selected_voice_instrument()" onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)" data-value="InstrumentHover" ${instrument_disabled}>${inst_name}</button>
							<div class="App_PMC_selected_voice_controller_A_container">
								<button class="App_PMC_selected_voice_add_A" onclick="PMC_selected_voice_add_A(true)" ${A_disabled}>A</button>
								${A_provv_extra_buttons}
							</div>
							<div>
								<button class="App_PMC_selected_voice_block ${voice_blocked_class}" onclick="PMC_block_unblock_selected_voice(this)">
									<img onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)" data-value="SyncVoiceHover" src="./Icons/changes_prevent.svg" style="display: ${voice_blocked_img0};">
									<img onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)" data-value="AsyncVoiceHover" src="./Icons/changes_allow.svg" style="display: ${voice_blocked_img1};">
								</button>
								<input class="App_PMC_selected_voice_neopulse_inp_box" onkeypress="APP_inpTest_NP(event,this)" onfocusin="APP_set_previous_value(this)" onfocusout="PMC_selected_voice_enter_new_NP(this)" maxlength="3" value="${np_value}" onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)" data-value="FrGeneralHover"/>
								<button class="App_PMC_selected_voice_metro_sound ${metro_class}" onclick="PMC_change_metro_sound_selected_voice(this)" value="${voice.metro}" onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)" data-value="MetronomeVoiceHover">${metro_text}</button>
							</div>
						</div>
						<div class="App_PMC_selected_voice_controller_time_container">
							<button class="${class_mute}" onClick="PMC_change_selected_voice_mute()" onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)" data-value="muteHover">M</button>
							<button class="${class_solo}" onClick="PMC_change_selected_voice_solo()" onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)" data-value="SoloHover">S</button>
							<button class="App_PMC_selected_voice_pulse_sound" onclick="PMC_show_pulse_sound_selector(${voice.voice_id},${voice.e_sound})">${e_sound_text}</button>
						</div>
						<div class="App_PMC_selected_voice_right_spacer">
						</div>`
		}
		let mute_value=0
		let mute_inner=""
		let mute_class="App_PMC_voice_selector_voice_item_voiceMuteSoloIcon"
		if(!voice.solo && !voice.mute){
			mute_value=0
			mute_inner='<img src="./Icons/audio_volume_small_w.svg">'
		} else if(!voice.solo && voice.mute){
			mute_value=1
			mute_class+=" App_PMC_vsb_mute"
			mute_inner='M'
		} else if(voice.solo){
			mute_value=2
			mute_class+=" App_PMC_vsb_solo"
			mute_inner='S'
		}
		//no need to disable bc all menu is opened...
		mutesolo=`<button class="${mute_class}" onclick="PMC_voice_volMuteSolo_Loop(this)" value="${mute_value}">${mute_inner}</button>`
		// }
		if(voice.PMC_visible){
			eye_inner= '<img src="./Icons/side_eye.svg">'
			eye_class+=" checked"
		}
		voice_list_string+=`<div class="App_PMC_voice_selector_voice_item ${checked}" value="${voice.voice_id}" draggable="true"
			onclick="PMC_select_voice_button(this)">
			<div>
				<label class="App_PMC_voice_selector_voice_number">${data.voice_data.length-index-1}</label>
				<input class="App_PMC_voice_selector_voice_item_name" value="${voice.name}" maxlength="8" onchange="PMC_change_voice_name(this)"></input>
				<div class="App_PMC_voice_selector_voice_item_content">
					<button class="APP_PMC_voice_selector_color_picker" value="${voice.color}" style="background-color:${eval(voice.color)}" onclick="PMC_color_picker_toggle(this)"></button>
					<div class="App_PMC_dropdown_colorPicker">
						<button onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)" onclick="PMC_color_picker_selectColor(this)"
							value="nuzic_blue"></button>
						<button onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)" onclick="PMC_color_picker_selectColor(this)"
							value="nuzic_green"></button>
						<button onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)" onclick="PMC_color_picker_selectColor(this)"
							value="nuzic_yellow"></button>
						<button onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)" onclick="PMC_color_picker_selectColor(this)"
							value="nuzic_pink"></button>
						<button onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)" onclick="PMC_color_picker_selectColor(this)"
							value="nuzic_red"></button>
					</div>
					${mutesolo}
					<button class="${eye_class}" onclick="PMC_switch_voice_visibility(this)">${eye_inner}</button>
					<button class="App_PMC_voice_selector_voice_item_menuIcon"><img src="./Icons/segment_menu.svg"></button>
					<div class="App_PMC_dropdown_voiceMenu">
						<a onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)"
						onclick="DATA_add_voice(${voice.voice_id})"><img class="App_RE_icon" src="./Icons/op_menu_add.svg"></a>
						<a onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)"
						onclick="DATA_clear_voice(${voice.voice_id})"><img class="App_RE_icon" src="./Icons/op_menu_clear.svg"></a>
						<a onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)"
						onclick="APP_stop;DATA_delete_voice(${voice.voice_id})"><img class="App_RE_icon" src="./Icons/op_menu_delete.svg"></a>
						<a onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)"
						onclick="PMC_repeat_selected_voice_button()"><img class="App_RE_icon" src="./Icons/op_menu_repeat_up.svg"></a>
						<a onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)"
						onclick="PMC_operations_selected_voice_button()"><img class="App_RE_icon" src="./Icons/op_menu_operations.svg"></a>
						<a ></a>
					</div>
				</div>
			</div>
			<div>
				${controller}
			</div>
		</div>`
	})
	voice_list.innerHTML= voice_list_string
	PMC_refresh_draggable_voices()
}

function PMC_voice_volMuteSolo_Loop(button){
	APP_stop()
	//find voice_id
	let voice_id= parseInt(button.closest(".App_PMC_voice_selector_voice_item").getAttribute("value"))
	let data = DATA_get_current_state_point(true)
	let voice_data= data.voice_data.find(voice=>{
		return voice.voice_id==voice_id
	})
	if(button.value==0){
		voice_data.mute=true
		voice_data.solo=false
	} else if(button.value==1){
		voice_data.mute=false
		voice_data.solo=true
	} else if(button.value==2){
		voice_data.mute=false
		voice_data.solo=false
	}
	DATA_insert_new_state_point(data)
	//DATA_load_state_point_data(false,true)
	PMC_populate_voice_list()
	BNAV_update_V(data)
	//no need to load
	flag_DATA_updated.PMC=true
}

function PMC_switch_voice_visibility(button){
	let voice_id= parseInt(button.closest(".App_PMC_voice_selector_voice_item").getAttribute("value"))
	let data = DATA_get_current_state_point(true)
	let voice_data= data.voice_data.find(voice=>{
		return voice.voice_id==voice_id
	})
	voice_data.PMC_visible= !voice_data.PMC_visible
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
	return
}

function PMC_change_voice_name(element){
	element.blur()
	let data = DATA_get_current_state_point(true)
	let selected_voice_data= data.voice_data.find(item=>{
		return item.selected==true
	})
	selected_voice_data.name = element.value
	DATA_insert_new_state_point(data)
	BNAV_update_V(data)
}

function PMC_toggle_line_selector_menu(button){
	let parent = button.parentNode
	if(parent.querySelector(".App_PMC_voice_selector_button_visibility_dropdown").classList.contains("show")){
		parent.querySelector(".App_PMC_voice_selector_button_visibility_dropdown").classList.remove("show")
	}else{
		parent.querySelector(".App_PMC_voice_selector_button_visibility_dropdown").classList.add("show")
	}
}

function PMC_color_picker_toggle(button){
	let parent = button.closest(".App_PMC_voice_selector_voice_item")
	let menu=parent.querySelector(".App_PMC_dropdown_colorPicker")
	menu.classList.toggle("show")
	//position the menu vertically
	let list_container=parent.closest(".App_PMC_voice_selector_voice_list")
	let [index,]=RE_element_index(parent,".App_PMC_voice_selector_voice_item")
	let top_position=100+nuzic_block * index - list_container.scrollTop
	menu.style="top:"+top_position+"px;"
}

function PMC_color_picker_selectColor(button){
	let parent = button.closest(".App_PMC_voice_selector_voice_item_content")
	let colorSelected = parent.querySelector(".APP_PMC_voice_selector_color_picker")
	if(colorSelected.value != button.value){
		//color has changed
		colorSelected.value= button.value
		colorSelected.style.backgroundColor = eval(button.value+"_light")
		let data = DATA_get_current_state_point(true)
		let selected_voice_data= data.voice_data.find(item=>{
			return item.selected==true
		})
		selected_voice_data.color = colorSelected.value
		DATA_insert_new_state_point(data)
		DATA_load_state_point_data(false,true)
	}
}

function PMC_voice_is_selected(voice_id){
	let voice_list = PMC_list_voices()
	let voice = voice_list.find(voice=>{
		let id = Number(voice.getAttribute("value"))
		return voice_id==id
	})
	let selected = false
	if(voice!=null){
		selected = voice.classList.contains("checked")
	}
	return selected
}

function PMC_voice_is_visible(voice_id){
	let voice_list = PMC_list_voices()
	let voice = voice_list.find(voice=>{
		let id = Number(voice.getAttribute("value"))
		return voice_id==id
	})
	let PMC_visible = true
	if(voice!=null){
		//color = voice.querySelector(".APP_PMC_voice_selector_color_picker").value
		var button=voice.querySelector(".App_PMC_voice_selector_voice_item_eyeIcon")
		PMC_visible=button.classList.contains("checked")
	}
	return PMC_visible
}

function PMC_read_voice_color(voice_id){
	let voice_list = PMC_list_voices()
	let voice = voice_list.find(voice=>{
		let id = Number(voice.getAttribute("value"))
		return voice_id==id
	})
	let color = "nuzic_blue"
	if(voice!=null){
		color = voice.querySelector(".APP_PMC_voice_selector_color_picker").value
	}
	return color
}

function PMC_list_voices(){
	let container = document.querySelector(".App_PMC_voice_selector_voice_list")
	return [...container.querySelectorAll(".App_PMC_voice_selector_voice_item")]
}

//Drag and drop
function PMC_refresh_draggable_voices(){
	let voice_container = document.querySelector(".App_PMC_voice_selector_voice_list")
	let draggables_voices = [...voice_container.querySelectorAll(".App_PMC_voice_selector_voice_item")]
	draggables_voices.forEach(draggable => {
		draggable.addEventListener("dragstart", (e) => {
			draggable.classList.add("dragging_voice")
			draggable.style.border="2px solid var(--Nuzic_light)"
			let canvas = document.createElement("canvas")
			let context = canvas. getContext("2d")
			context. clearRect(0, 0, canvas. width, canvas. height)
			e.dataTransfer.setDragImage(canvas, 10, 10)
		})
		draggable.addEventListener("dragend", () => {
			draggable.classList.remove("dragging_voice")
			let voice_container = document.querySelector(".App_PMC_voice_selector_voice_list")
			let draggables_voices = [...voice_container.querySelectorAll(".App_PMC_voice_selector_voice_item")]
			let new_voice_id_order = draggables_voices.map(div=>{return parseInt(div.getAttribute("value"))})
			DATA_rearrange_voices(new_voice_id_order)
		})
		draggable.addEventListener("dragover", e => {
			e.preventDefault()
			let after_element = PMC_get_drag_after_voice(voice_container, e.clientY)
			let draggable = voice_container.querySelector(".dragging_voice")
			if (after_element == null){
				voice_container.appendChild(draggable)
			} else {
				voice_container.insertBefore(draggable, after_element)
			}
		})
	})
}

//Voices find drag
function PMC_get_drag_after_voice(container, y) {
	let draggableElements = [...container.querySelectorAll(".App_PMC_voice_selector_voice_item:not(.dragging_voice)")]
	return draggableElements.reduce((closest, child) => {
		let box = child.getBoundingClientRect()
		let offset = y - box.top - box.height / 2
		if (offset < 0 && offset > closest.offset) {
			return { offset: offset, element: child }
		} else {
			return closest
		}
	}, { offset: Number.NEGATIVE_INFINITY }).element
}
