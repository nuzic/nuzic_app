let _listeners = []

EventTarget.prototype.addEventListenerBase = EventTarget.prototype.addEventListener;
EventTarget.prototype.addEventListener = function(type, listener){
	_listeners.push({target: this, type: type, listener: listener})
	this.addEventListenerBase(type, listener)
}

EventTarget.prototype.removeEventListeners = function(targetType){
	for(let index = 0; index != _listeners.length; index++){
		let item = _listeners[index]
		let target = item.target
		let type = item.type
		let listener = item.listener
		if(target == this && type == targetType){
			this.removeEventListener(type, listener)
		}
	}
}

function RE_draggable_segment_dragstart(draggable,e){
	draggable.classList.add('dragging_segment')
	let canvas = document.createElement('canvas')
	let context = canvas.getContext('2d')
	context.clearRect(0, 0, canvas.width, canvas.height)
	e.dataTransfer.setDragImage(canvas, 10, 10)
	e.stopPropagation()
}

function RE_draggable_segment_dragend(draggable,e){
	e.stopPropagation()
	draggable.classList.remove('dragging_segment')
	let segment_container = draggable.closest(".App_RE_voice_data")
	let draggables_segments = [...segment_container.querySelectorAll(".App_RE_segment")]
	let new_segment_index_order = draggables_segments.map(segment=>{
		let segment_index= parseInt(segment.querySelector(".App_RE_segment_number").innerHTML)
		return segment_index
	})
	if(new_segment_index_order.some((value,index)=>{return value!=index})){
		let voice_id = RE_read_voice_id(segment_container.closest(".App_RE_voice"))[1]
		DATA_rearrange_segments(voice_id,new_segment_index_order)
	}
	//in case no change
	RE_show_iA_lines()
}

function RE_segment_container_dragover(container,e){
	e.preventDefault()
	e.stopPropagation()
	let afterElement = RE_get_drag_after_segment(container, e.clientX)
	let draggable = document.querySelector('.dragging_segment')
	if(draggable==null)return
	let draggable_voice = draggable.closest(".App_RE_voice")
	let [index,seg_list ] = RE_element_index(draggable,'.App_RE_segment')
	let spacer_list = [...draggable.parentNode.querySelectorAll('.App_RE_segment_spacer')]
	let indexAE=-1
	//PREVENT DRAG TO OTHER VOICE
	if(container===draggable.parentNode){
		//detect if no move
		if (afterElement == null) {
			//Last element
			if(index==seg_list.length-1){
				//not moved
				return
			}
			//not here , there is segment spacer end
			let end = container.querySelector(".App_RE_segment_spacer_end")
			container.insertBefore(draggable,end)
			container.insertBefore(spacer_list[index],end)
		} else {
			[indexAE, ] = RE_element_index(afterElement,'.App_RE_segment')
			if(index==indexAE-1){
				//not moved
				return
			}
			container.insertBefore(draggable, afterElement)
			container.insertBefore(spacer_list[index],afterElement)
		}
		if(RE_voice_is_blocked(draggable_voice)){
			//find all voices with same polipulse fractioning and blocked and move the corresponding segment too
			let voice_matrix = document.querySelector('.App_RE_voice_matrix')
			let voice_list = [...voice_matrix.querySelectorAll(".App_RE_voice")]
			let sync_voice_list = voice_list.filter(voice => {
				//drag if polipulse blocked!
				return RE_voice_is_blocked(voice) && voice!=draggable_voice
			})
			//move synched segments [index ]in place
			sync_voice_list.forEach((voice)=>{
				let sync_segment_list=[...voice.querySelectorAll(".App_RE_segment")]
				//voice minimized
				if(sync_segment_list.length==0)return
				let sync_container = sync_segment_list[index].parentNode
				let sync_spacer_list=[...sync_container.querySelectorAll(".App_RE_segment_spacer")]
				//moving sync_segment_list[index] in position indexAE
				if(indexAE!=-1){
					sync_container.insertBefore(sync_segment_list[index], sync_segment_list[indexAE])
					sync_container.insertBefore(sync_spacer_list[index],sync_segment_list[indexAE])
				}else{
					//append at the end
					//not here , there is segment spacer end
					let end = sync_container.querySelector(".App_RE_segment_spacer_end")
					sync_container.insertBefore(sync_segment_list[index],end)
					sync_container.insertBefore(sync_spacer_list[index],end)
				}
			})
		}
		APP_stop()
	}
	RE_hide_iA_lines()
}

function RE_draggable_voice_dragstart(draggable,e){
	draggable.classList.add('dragging_voice')
	//console.log(draggable)
	let canvas = document.createElement('canvas')
	let context = canvas. getContext('2d')
	context. clearRect(0, 0, canvas. width, canvas. height)
	e.dataTransfer.setDragImage(canvas, 10, 10)
}

function RE_draggable_voice_dragend(draggable){
	draggable.classList.remove('dragging_voice')
	let voice_container = document.querySelector('.App_RE_voice_matrix')
	let draggables_voices = [...voice_container.querySelectorAll('.App_RE_voice')]
	let new_voice_id_order = draggables_voices.map(voice_obj=>{return RE_read_voice_id(voice_obj)[1]})
	let no_change=DATA_rearrange_voices(new_voice_id_order)
	//in case no change
	RE_show_iA_lines()
}

function RE_voice_matrix_dragover(voice_matrix,e){
	e.preventDefault()
	//var afterElement = RE_get_drag_after_voice(container, e.clientY)
	let afterElement = RE_get_drag_after_voice(voice_matrix, e.clientY)
	let draggable = document.querySelector('.dragging_voice')
	if(draggable==null)return
	let [index, voice_list] = RE_element_index(draggable,'.App_RE_voice')
	let iA_list = [...voice_matrix.querySelectorAll('.App_RE_voices_between')]
	if (afterElement == null) {
		if(index==voice_list.length-1){
			//not moved
			return
		}
		//if compasses is not in the container
		//append before Compasses etc
		let final_child = voice_matrix.querySelector(".RE_modules")
		voice_matrix.insertBefore(iA_list[index],final_child)
		voice_matrix.insertBefore(draggable, final_child)
	} else {
		[indexAE, ] = RE_element_index(afterElement,'.App_RE_voice')
		if(index==indexAE-1){
			//not moved
			return
		}
		voice_matrix.insertBefore(iA_list[index],iA_list[indexAE])
		voice_matrix.insertBefore(draggable, iA_list[indexAE])

		//voice_matrix.insertBefore(draggable, afterElement.previousSibling)
		// voice_matrix.insertBefore(draggable, afterElement)
		// voice_matrix.insertBefore(iA_list[index],afterElement)
	}
	//REFRESH iA visibility
	RE_hide_iA_lines()
}

function RE_hide_iA_lines(){
	let voice_matrix = document.querySelector(".App_RE_voice_matrix")
	let line_between_voices_list = [...voice_matrix.querySelectorAll(".App_RE_voices_between")]
	let voice_list = [...voice_matrix.querySelectorAll(".App_RE_voice")]
	let first_line = line_between_voices_list.shift()
	first_line.querySelector(".App_RE_label").classList.add('hidden')
	first_line.querySelector(".App_RE_iA").classList.add('hidden')
	let first_iA_shown = true
	if(RE_voice_is_visible(voice_list[0]))first_iA_shown=false
	let index=1
	line_between_voices_list .forEach((item) =>{
		item.querySelector(".App_RE_iA").classList.add('hidden')
		if(RE_voice_is_visible(voice_list[index])){
			if(first_iA_shown){
				//first voice shown has iA still hidden
				item.querySelector(".App_RE_label").classList.add('hidden')
				first_iA_shown=false
			}else{
				//
				item.querySelector(".App_RE_label").classList.remove('hidden')
			}
		}else{
			//hidden
			item.querySelector(".App_RE_label").classList.add('hidden')
		}
		index++
	})
}

function RE_show_iA_lines(){
	let voice_matrix = document.querySelector(".App_RE_voice_matrix")
	let line_between_voices_list = [...voice_matrix.querySelectorAll(".App_RE_voices_between")]
	let voice_list = [...voice_matrix.querySelectorAll(".App_RE_voice")]
	let first_line = line_between_voices_list.shift()
	first_line.querySelector(".App_RE_label").classList.add('hidden')
	first_line.querySelector(".App_RE_iA").classList.add('hidden')
	let first_iA_shown = true
	if(RE_voice_is_visible(voice_list[0]))first_iA_shown=false
	let index=1
	line_between_voices_list .forEach((item) =>{
		if(RE_voice_is_visible(voice_list[index])){
			if(first_iA_shown){
				//first voice shown has iA still hidden
				item.querySelector(".App_RE_label").classList.add('hidden')
				item.querySelector(".App_RE_iA").classList.add('hidden')
				first_iA_shown=false
			}else{
				//
				item.querySelector(".App_RE_label").classList.remove('hidden')
				item.querySelector(".App_RE_iA").classList.remove('hidden')
			}
		}else{
			//hidden
			item.querySelector(".App_RE_label").classList.add('hidden')
			item.querySelector(".App_RE_iA").classList.add('hidden')
		}
		index++
	})
}

function RE_get_drag_after_segment(container, x){
	//list of draggable segments - segment that i'm currently dragging
	let draggableElements = [...container.querySelectorAll('.App_RE_segment:not(.dragging_segment)')]
	return draggableElements.reduce((closest, child) => {
		let box = child.getBoundingClientRect()
		let offset = x - box.left - box.width / 2
		if (offset < 0 && offset > closest.offset) {
			return { offset: offset, element: child }
		} else {
			return closest
		}
	}, { offset: Number.NEGATIVE_INFINITY }).element
}

//Voices

function RE_get_drag_after_voice(container, y) {
	let draggableElements = [...container.querySelectorAll('.App_RE_voice:not(.dragging_voice)')]
	return draggableElements.reduce((closest, child) => {
		let box = child.getBoundingClientRect()
		let offset = y - box.top - box.height / 2
		if (offset < 0 && offset > closest.offset) {
			return { offset: offset, element: child }
		} else {
			return closest
		}
	}, { offset: Number.NEGATIVE_INFINITY }).element
}
