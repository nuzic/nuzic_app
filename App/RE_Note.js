//Implementation of N_line roules
function RE_enter_new_value_S(element,type){
	let string = element.value
	if(string==previous_value) return
	if(type=="Ngm" && string==RE_superscript_to_string(previous_value)){
		//need to rewrite
		element.value=previous_value
		return
	}
	APP_stop()
	//control value
	let position = RE_input_to_position(element)
	let voice_id = position.voice_id
	let segment_index = position.segment_index
	let sound_index = position.sound_index
	let next=0
	let focus_index=sound_index
	if(string==""){
		if(previous_value!=""){
			//delete values if not fraction range
			let success = DATA_delete_object_index(voice_id,segment_index,sound_index) //if fraction range note = "e"
			focus_index=sound_index
			if(!success){
				element.value=previous_value
				return
			}
			next=0
		}else{
			return
		}
	}else{
		//control correctness new value of the note
		if(previous_value==""){
			//added new item
			//the note define a new iT
			let [note_number,isanote,diesis]= RE_read_Sound_Element(element,type)
			if(note_number==null){
				//no need of continuing calculations.... is an error
				element.value=previous_value
				return
			}
			if(note_number>=TET*N_reg){
				element.value=previous_value
				APP_blink_error(element)
				return
			}
			diesis= (diesis==null)?true:diesis
			let success = DATA_enter_new_object_index(voice_id,segment_index,sound_index+1,null,null,note_number,diesis,[],[])
			if(!success){
				element.value=previous_value
				APP_blink_error(element)
				let segment_obj = element.closest(".App_RE_segment")
				let RE_iT = segment_obj.querySelector(".App_RE_iT")
				if(RE_iT!=null){
					let input_iT_list = [...RE_iT.querySelectorAll(".App_RE_inp_box:not(:placeholder-shown)")]
					APP_blink_error(input_iT_list[sound_index])
				}
				return
			}
			//play note
			if(SNAV_read_play_new_note() && success){
				let instrument_index = DATA_which_voice_instrument(voice_id)
				APP_play_this_note_number(note_number,instrument_index,voice_id)
			}
			next=1
			focus_index=sound_index+1
		}else {
			//changed item
			let [note_number,isanote,diesis]= RE_read_Sound_Element(element,type)
			if(note_number==null){
				//no need of continuing calculations.... is an error
				element.value=previous_value
				return
			}
			//if ligadura and first element
			//if(note_number==-3 && position.column_index==0){
			if(note_number==-3 && position.sound_index==0){
				element.value=previous_value
				APP_blink_error(element)
				return
			} else {
				if(note_number>=TET*N_reg){
					element.value=previous_value
					APP_blink_error(element)
					return
				}
				let success = DATA_modify_object_index_sound(voice_id,segment_index,sound_index, note_number,diesis,null,null)
				if(!success){
					element.value=previous_value
					APP_blink_error(element)
					return
				}
				//play note
				if(SNAV_read_play_new_note() && success){
					let instrument_index = DATA_which_voice_instrument(voice_id)
					APP_play_this_note_number(note_number,instrument_index,voice_id)
				}
				next=1
			}
		}
	}
	RE_focus_on_object_index(voice_id,segment_index,focus_index,type,next)
}

function RE_enter_new_value_iN(element,type){
	//if P is part of fraction range
	let string = element.value
	if(string==previous_value) return
	if(type=="iNgm" && string==RE_superscript_to_string(previous_value)){
		//need to rewrite
		element.value=previous_value
		return
	}
	if(previous_value==end_range){
		element.value=previous_value
		return
	}
	if(string=="-") {
		//no need of continuing calculations.... is an error???
		element.value=previous_value
		return
	}
	//control value ok
	// if (string==end_range){
		//structural element
	// 	element.value=previous_value
	// 	return
	// }
	APP_stop()
	if (string==tie_intervals){
		//tie 2 intervals
	}
	if (string==tie_intervals_m){
		//tie 2 intervals
		element.value=tie_intervals
	}
	let position=RE_input_to_position(element)
	let voice_id = position.voice_id
	let segment_index = position.segment_index
	let sound_index = position.sound_index
	let next = 0
	let focus_index=sound_index
	if(previous_value==""){
		//the iN define a new iT
		let [note_number,isanote,diesis]= RE_read_Sound_Element(element,type)
		//console.log(note_number+"  "+isanote+"  "+diesis)
		if(note_number==null){
			element.value=""
			APP_blink_error(element)
			return
		}
		diesis= (diesis==null)?true:diesis
		let success = false
		if(type!="iNgm"){
			success = DATA_enter_new_object_index_iN(voice_id,segment_index,sound_index+1,note_number,isanote,diesis,[],[])
		}else {
			success = DATA_enter_new_object_index_iNgrade(voice_id,segment_index,sound_index+1,note_number,isanote,diesis,[],[])
		}
		if(!success){
			//no space for new note
			element.value=""
			APP_blink_error(element)
			let segment_obj = element.closest(".App_RE_segment")
			let RE_iT = segment_obj.querySelector(".App_RE_iT")
			if(RE_iT!=null){
				let input_iT_list = [...RE_iT.querySelectorAll(".App_RE_inp_box:not(:placeholder-shown)")]
				APP_blink_error(input_iT_list[sound_index-1])
			}
			return
		}
		if(SNAV_read_play_new_note() && success){
			APP_stop()
			//play new note!!! -element pos + 1
			APP_play_this_note_RE_element(element,1)
		}
		next=1
		focus_index++
	}else {
		//modify existing Note sequence
		if(element.value==""){
			//delete pulse value but if it is a fraction range put a "e"
			let success=false
			//if(!element.classList.contains("App_RE_note_bordered_cell") && type=="iNgm"){//control not necess.... inside next function
			if(type=="iNgm"){
				success = DATA_delete_object_index_iNgrade(voice_id,segment_index,sound_index)
			}else{
				success = DATA_delete_object_index_iN(voice_id,segment_index,sound_index)
			}
			if(!success){
				element.value=previous_value
				//APP_blink_error(element)
				return
			}
			next=0
		}else{
			//control correctness new value (range and type)
			let [note_number,isanote,diesis]= RE_read_Sound_Element(element,type)
			if(note_number==null){
				//no need of continuing calculations.... is an error
				element.value=previous_value
				APP_blink_error(element)
				return
			}
			//if diesis == null no change
			let success=false
			if(type!="iNgm"){
				success = DATA_modify_object_index_iN(voice_id,segment_index,sound_index,note_number,isanote,diesis)
			}else{
				success = DATA_modify_object_index_iNgrade(voice_id,segment_index,sound_index,note_number,isanote,diesis)
			}
			if(!success){
				element.value=previous_value
				APP_blink_error(element)
				return
			}
			if(SNAV_read_play_new_note() && success){
				APP_stop()
				APP_play_this_note_RE_element(element)
			}
			next=1
		}
	}
	RE_focus_on_object_index(voice_id,segment_index,focus_index,type,next)
}

function RE_enter_new_value_A(element,type,positive){
	APP_stop()
	element.placeholder=""
	if(element.value==previous_value)return
	let line_container = element.parentNode
	if(line_container==null)return //case double fire last element refocus
	let position = RE_input_to_position(element)
	//modify database
	let A_list=APP_text_to_A_list(element.value,type)
	if(A_list==null){
		//error
		element.value=previous_value
		return
	}
	let next=(element.getAttribute("focus")=="left")?-1:1
	if(A_list.length==0){
		if(previous_value!=""){
			//delete
			let success = DATA_delete_object_index_A(position.voice_id,position.segment_index,position.sound_index,positive,!positive)
			if(!success){
				element.value=previous_value
			}
		}else{
			element.value=previous_value
		}
		//focus
		RE_focus_on_object_index(position.voice_id,position.segment_index,position.input_index,(positive)?"A_pos":"A_neg",next)
		return
	}
	if(type=="Ag"){
		A_list=DATA_calculate_object_index_Ag_list_to_A_list(position.voice_id,position.segment_index,position.sound_index,positive,A_list)
	}
	let A_pos=positive?A_list:null
	let A_neg=positive?null:A_list
	let success = DATA_modify_object_index_sound(position.voice_id,position.segment_index,position.sound_index, null,null,A_pos,A_neg)
	if(!success){
		element.value=previous_value
		return
	}
	//let true_index=[...line_container.querySelectorAll("input")].indexOf(element)
	RE_focus_on_object_index(position.voice_id,position.segment_index,position.input_index,(positive)?"A_pos":"A_neg",next)
}

function RE_read_Sound_Element(element, line_id=null){
	//translate element.value to N element absolute
	if(line_id==null){
		console.error("read_sound no line_id")
		return
	}
	let string = element.value
	if(string==="") return [null,false]
	let note_number = null
	let diesis = null
	//case not a note
	if (string=="s"){
		//silence
		note_number=-1
		return [note_number,false]
	}
	if (string=="e"){
		//un-determined element
		note_number=-2
		return [note_number,false]
	}
	if (string==tie_intervals){
		note_number=-3
		return [note_number,false]
	}
	if (string==tie_intervals_m){
		//console.log("ligadura l min")
		note_number=-3
		element.value=tie_intervals
		return [note_number,false]
	}
	if (string==end_range){
		//structural element
		note_number=-4
		return [note_number,false]
	}
	if (string=="."){
		//structural element
		note_number=-4
		element.value=end_range
		return [note_number,false]
	}
	//using database in order to find prev element note/scale/etc...
	let position=RE_input_to_position(element)
	let voice_id = position.voice_id
	let segment_index = position.segment_index
	let sound_index = position.sound_index
	//case it is a note
	switch(line_id) {
	case "Na":
		// Abs
		note_number = Math.floor(string)
		if(isNaN(note_number)){
			note_number = null
			break
		}
		var max_note_number = TET * N_reg-1
		if(note_number>max_note_number){
			//if OUT OF NOTE RANGE
			note_number=max_note_number
		}
		break;
	case "Nm":
		// Mod
		//verify if has module, if not read previous value
		var split = element.value.split('r')
		var resto = Math.floor(split[0])
		var reg=3
		if(split[1]>=0 && split[1]!=""){
			reg=Math.floor(split[1])
		}else{
			let from_index=(previous_value=="")?sound_index+1:sound_index
			let previous_element_sound_index = DATA_get_object_prev_note_index(voice_id,segment_index,from_index)
			if(previous_element_sound_index!= null){
				reg= Math.floor(DATA_get_object_index_info(voice_id,segment_index,previous_element_sound_index).sound.note/TET)
			}
		}
		if(resto>=TET){
			note_number = null
		}else{
			note_number = resto + reg*TET
			if(isNaN(note_number)){
				note_number = null
			}
		}
		break;
	case "Ngm":
		// grade mod
		var iT=0
		if(previous_value==""){
			iT=1
		}
		var current_scale = DATA_get_object_index_info(voice_id,segment_index,sound_index,iT,false,false,true).scale
		var idea_scale_list=DATA_get_idea_scale_list()
		if (current_scale==null){
			//no scale in this pulse
			console.error("Error reading RE input Ngm, scale not entered")
			return [null,null,null]
			break;
		}
		//ATT! reg can be -1
		var reg = 3
		var grade = 0
		var delta = 0
		if(string.includes("r")){
			var split = string.split("r")
			if(split[1]!=""){
				reg=Math.floor(split[1])
			}else{
				//no scale in this pulse
				console.error("no reg spec")
				note_number = null
				break;
			}
			var str_grade = split[0]
			if(split[0].includes("+")){
				var split2= split[0].split("+")
				str_grade=split2[0]
				delta=parseInt(split2[1])
				if(isNaN(delta))delta=1
				diesis=true
			}
			if(split[0].includes("-")){
				var split2= split[0].split("-")
				str_grade=split2[0]
				delta=parseInt(split2[1])*(-1)
				if(isNaN(delta))delta=-1
				diesis=false
			}
			var grade = parseInt(str_grade)
		}else{
			//find reg in previous OR reg==3
			let from_index=(previous_value=="")?sound_index+1:sound_index
			var previous_element_sound_index = DATA_get_object_prev_note_index(voice_id,segment_index,from_index)
			if(previous_element_sound_index!= null){
				var prev_element_info = DATA_get_object_index_info(voice_id,segment_index,previous_element_sound_index,0,false,false,true)
				var [,,c]=DATA_calculate_absolute_note_to_scale(prev_element_info.sound.note,true,prev_element_info.scale,idea_scale_list)
				reg= c
			}
			var str_grade = string
			if(string.includes("+")){
				var split2= string.split("+")
				str_grade=split2[0]
				delta=parseInt(split2[1])
				if(isNaN(delta))delta=1
				diesis=true
			}
			if(string.includes("-")){
				var split2= string.split("-")
				str_grade=split2[0]
				delta=parseInt(split2[1])*(-1)
				if(isNaN(delta))delta=-1
				diesis=false
			}
			var grade = parseInt(str_grade)
		}
		let scaleLength = idea_scale_list.find(item=>{
			return item.acronym==current_scale.acronym
		})
		if(grade>=scaleLength.iS.length){
			//console.log(scale)
			element.value = previous_value
			console.log("ERROR : grade exceed scale length")
		} else {
			note_number = DATA_calculate_scale_note_to_absolute(grade,delta,reg,current_scale,idea_scale_list).note//and outside_range XXX???
			if(note_number==null){
				element.value = previous_value
				console.error("error calculating Na from Ngm")
				note_number = null
			}
		}
		if(isNaN(note_number)){
			note_number = null
		}
		break;
	case "iNa":
		note_number = Math.floor(string)
		//control if first element is a note
		if(isNaN(note_number)){
			note_number = null
		}
		var max_note_number = TET * N_reg-1
		if(note_number>max_note_number){
			//un-determined element
			note_number=max_note_number
		}
		if(note_number<-max_note_number){
			//un-determined element
			note_number=-max_note_number
		}
		break;
	case "iNm":
		// distance modular
		//verify if is negative
		var i= 1
		if (string[0]=="-"){
			i=-1
		}
		//verify if has module, if not read previous value
		var split = element.value.split('r')
		var resto = Math.floor(split[0])
		var reg=0
		if(split[1]>=0 && split[1]!=""){
			reg=Math.floor(split[1])
		}
		if(!isNaN(resto)){
			note_number = resto + reg*TET*i
		}else{
			note_number = null
		}
		break;
	case "iNgm":
		//distance grade mod
		//find correct scale
		var iT=0
		if(previous_value==""){
			iT=1
		}
		var current_scale = DATA_get_object_index_info(voice_id,segment_index,sound_index,iT,false,false,true).scale
		var idea_scale_list=DATA_get_idea_scale_list()
		if(current_scale==null){
			note_number = null
			break
		}
		//find previous note
		let from_index=(previous_value=="")?sound_index+1:sound_index
		var previous_element_sound_index = DATA_get_object_prev_note_index(voice_id,segment_index,from_index)
		var previous_scale = null
		if(previous_element_sound_index!= null){
			previous_scale = DATA_get_object_index_info(voice_id,segment_index,previous_element_sound_index,0,false,false,true).scale
		}
		//verify if is negative
		var i= 1
		if (string[0]=="-"){
			i=-1
			string = string.substring(1)
		}
		var split = string.split('r')
		var reg=0
		if(JSON.stringify(current_scale)!=JSON.stringify(previous_scale))reg=3
		if(string.includes("r")){
			if(split[1]!=""){
				reg=parseInt(split[1])
			}
		}
		//calculating grade and dieresis
		//part with diesis
		var g = 0
		var delta = 0
		if(split[0].includes("+")){
			//positive dieresis
			var split2 = split[0].split('+')
			g= Math.floor(split2[0])
			if(split2[1]==""){
				delta=1
			}else{
				delta=Math.floor(split2[1])
			}
		}else if(split[0].includes("-")){
			//negatives
			var split2 = split[0].split('-')
			g= Math.floor(split2[0])
			if(split2[1]==""){
				delta=-1
			}else{
				delta=Math.floor(split2[1])*(-1)
			}
		}else{
			g= Math.floor(split[0])
		}
		//changing this values in note_number
		if(!isNaN(i) && !isNaN(g) && !isNaN(delta) && !isNaN(reg)){
			//grade , delta , register
			var grade = g*i
			if(JSON.stringify(current_scale)==JSON.stringify(previous_scale)){
				//same scale
				//calculations made in DATA_function
				note_number=grade
				break
			}else{
				//scale changed INPUT INSERTED IS NEW PULSE
				if(grade<0 || reg<0){ //XXX non é detto   RICORDA LE ROTAZIONIIIII XXX
					note_number = null
					console.log("going grade "+grade+" and diesis "+delta+" and reg "+reg)
					console.error("first number of a new scale cant be neg")
					break
				}
				var current_Na = DATA_calculate_scale_note_to_absolute(grade,delta,reg,current_scale,idea_scale_list).note//and outside_range XXX???
				if(current_Na==null){
					console.error("error calculating Na from Ngm")
					note_number = null
					break
				}
				if(current_Na<0){
					console.log("going grade "+grade+" and diesis "+delta+" and reg "+reg)
					console.error("first element of a new scale but negative : Na "+current_Na)
					note_number = null
					break
				}
				//note_number= current_Na-previous_Na//nono,scale changed
				note_number= current_Na
				if(delta>0)diesis=true
				if(delta<0)diesis=false
				//no change if delta = 0
				break
			}
		}else{
			note_number = null
		}
		break;
	default:
		console.error("Error reading case N writing roules")
		note_number=null
	}
	if(note_number=="X" || note_number==null)console.error("i don't understand what you wrote")
	return [note_number,true,diesis]
}


