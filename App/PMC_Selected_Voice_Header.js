function PMC_load_selected_voice_header(voice_data){
	let central_header_container = document.getElementById("App_PMC_selected_voice_note_header")
	//<label class="App_PMC_voice_selector_voice_number">${voice_data.length-index-1}</label>
	let data=DATA_get_current_state_point()
	let v_index=""
	data.voice_data.forEach((item, index)=>{
		//<label class="App_PMC_voice_selector_voice_number">${voice_data.length-index-1}</label>
		if(item.voice_id==voice_data.voice_id){
			v_index=data.voice_data.length-index-1
		}
	})
	let central_header_string=`<label class="App_PMC_voice_selector_voice_number">${v_index}</label>
				<label class="App_PMC_voice_selector_voice_item_name">${voice_data.name}</label>
				<button class="App_PMC_voice_selector_voice_item_menuIcon"><img src="./Icons/segment_menu.svg"></button>
				<div class="App_PMC_dropdown_voiceMenu">
					<a onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)"
					onclick="DATA_add_voice(${voice_data.voice_id})"><img class="App_RE_icon" src="./Icons/op_menu_add.svg"></a>
					<a onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)"
					onclick="DATA_clear_voice(${voice_data.voice_id})"><img class="App_RE_icon" src="./Icons/op_menu_clear.svg"></a>
					<a onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)"
					onclick="APP_stop;DATA_delete_voice(${voice_data.voice_id})"><img class="App_RE_icon" src="./Icons/op_menu_delete.svg"></a>
					<a onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)"
					onclick="PMC_repeat_selected_voice_button()"><img class="App_RE_icon" src="./Icons/op_menu_repeat_up.svg"></a>
					<a onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)"
					onclick="PMC_operations_selected_voice_button()"><img class="App_RE_icon" src="./Icons/op_menu_operations.svg"></a>
					<a ></a>
				</div>`
	central_header_container.innerHTML=central_header_string

	let A_pos_header_container = document.getElementById("App_PMC_selected_voice_A_pos_sequence")
	let A_neg_header_container = document.getElementById("App_PMC_selected_voice_A_neg_sequence")
	if(voice_data.A){
		A_pos_header_container.style=""
		A_neg_header_container.style=""
		let A_pos_label = A_pos_header_container.querySelector("label")
		let A_neg_label = A_neg_header_container.querySelector("label")
		let A_string=(A_global_variables.type=="Ag")?"iAº":("i"+A_global_variables.type)
		A_pos_label.innerHTML=A_string+"+"
		A_neg_label.innerHTML=A_string+"-"
	}else{
		A_pos_header_container.style="display:none"
		A_neg_header_container.style="display:none"
	}
}

function PMC_change_selected_voice_instrument(){
	APP_stop()
	let data = DATA_get_current_state_point(true)
	//Selected voice
	let selected_voice_data= data.voice_data.find(item=>{
		return item.selected
	})
	let options = {"voice_id":selected_voice_data.voice_id,"instrument":selected_voice_data.instrument}
	APP_show_instrument_selector(options)
}


function PMC_block_unblock_selected_voice(button){
	let data = DATA_get_current_state_point(true)
	//Selected voice
	let selected_voice_data= data.voice_data.find(item=>{
		return item.selected
	})
	if(selected_voice_data.blocked){
		selected_voice_data.blocked = false
		DATA_insert_new_state_point(data)
		//PMC_load_selected_voice_header(selected_voice_data)
		PMC_populate_voice_list()
		//no need to load
		flag_DATA_updated.PMC=true
	}else{
		//control if blockable
		let blocked_voice_list = data.voice_data.filter(item=>{
			return item.blocked==true
		})
		//verify if voice is blockable
		let is_blockable = false
		if(blocked_voice_list.length==0 ) {
			is_blockable=true
		}else{
			//verify long and number every segment is the same
			let N_NP_this_voice= selected_voice_data.neopulse.N
			let D_NP_this_voice= selected_voice_data.neopulse.D
			let segment_data_list = selected_voice_data.data.segment_data
			let segment_L_base_list = segment_data_list.map(item=>{
				let last_time = item.time.slice(-1)[0]
				return Math.round(last_time.P*N_NP_this_voice/D_NP_this_voice)
			})
			//create give a list of segment Longitud of a blocked voices and translate to 1/1 //XXX  TS
			let first_blocked_voice_data= blocked_voice_list[0]
			let N_NP= first_blocked_voice_data.neopulse.N
			let D_NP= first_blocked_voice_data.neopulse.D
			let first_blocked_segment_data_list = first_blocked_voice_data.data.segment_data
			let first_blocked_segment_L_base_list = first_blocked_segment_data_list.map(item=>{
				let last_time = item.time.slice(-1)[0]
				return Math.round(last_time.P*N_NP/D_NP)
			})
			if(segment_L_base_list.length==first_blocked_segment_L_base_list.length){
				let not_equal = segment_L_base_list.some((segment_L_base,index)=>{
					return segment_L_base!=first_blocked_segment_L_base_list[index]
				})
				if(!not_equal)is_blockable=true
			}
		}
		if(is_blockable){
			selected_voice_data.blocked = true
			//verify if segment names are the same
			let segment_name_changed=false
			if(blocked_voice_list.length!=0 ) {
				let first_blocked_voice_data= blocked_voice_list[0]
				first_blocked_voice_data.data.segment_data.forEach((segment,index)=>{
					if(segment.segment_name!=selected_voice_data.data.segment_data[index].segment_name){
						selected_voice_data.data.segment_data[index].segment_name=segment.segment_name
						segment_name_changed=true
					}
				})
			}
			//warning segment name changed
			if(segment_name_changed){
				APP_info_msg("bk_v_change_s_name")
				DATA_insert_new_state_point(data)
				DATA_load_state_point_data(false,true)
			}else{
				//var PMC_reload = flag_DATA_updated.PMC
				DATA_insert_new_state_point(data)
				//no need to load
				//PMC_load_selected_voice_header(selected_voice_data)
				PMC_populate_voice_list()
				//no need to load
				flag_DATA_updated.PMC=true
			}
		}else{
			return
		}
	}
}

function PMC_show_pulse_sound_selector(voice_id,e_sound){
	APP_stop()
	let options = {"voice_id":voice_id,"e_sound":e_sound}
	APP_show_pulse_sound_selector(options)
}

function PMC_change_metro_sound_selected_voice(button){
	APP_stop()
	let current_value = parseInt(button.value)
	let available_metro_sounds = DATA_list_available_metro_sounds()
	//var max_value = 5
	let max_value = available_metro_sounds-length-1
	current_value = available_metro_sounds.find(item=>{return item>current_value})
	//null if current = 5 (last one)
	if(current_value>max_value || current_value==null)current_value=0
	let data = DATA_get_current_state_point(true)
	//Selected voice
	let selected_voice_data= data.voice_data.find(item=>{
		return item.selected
	})
	selected_voice_data.metro=current_value
	DATA_insert_new_state_point(data)
	PMC_populate_voice_list()
	BNAV_update_V(data)
	//no need to load
	flag_DATA_updated.PMC=true
}

function PMC_selected_voice_enter_new_NP(element){
	if(element.value == previous_value) return
	let NP_voice = element.value
	let split = NP_voice.split('/')
	let N = Math.floor(split[0])
	let D = Math.floor(split[1])
	if(N==0 || D==0 || isNaN(D)){
		APP_info_msg("enterNP_fail")
		APP_blink_error(element)
		element.value = previous_value
		return
	}
	//Selected voice
	let voice_id=null
	let success = DATA_calculate_force_voice_NP(voice_id,N,D)
	if(!success){
		APP_info_msg("enterNP_fail")
		APP_blink_error(element)
		element.value = previous_value
	}
	//in case focus out with PMC click
	PMC_reset_action_pmc()
}

function PMC_change_selected_voice_mute(){
	APP_stop()
	let data = DATA_get_current_state_point(true)
	//Selected voice
	let selected_voice_data= data.voice_data.find(item=>{
		return item.selected
	})
	// if(selected_voice_data.volume==0){
	// 	selected_voice_data.volume = 3
	// }else{
	// 	selected_voice_data.volume = 0
	// }
	selected_voice_data.mute=!selected_voice_data.mute
	DATA_insert_new_state_point(data)
	PMC_populate_voice_list()
	BNAV_update_V(data)
	//no need to load
	flag_DATA_updated.PMC=true
}

function PMC_change_selected_voice_solo(){
	APP_stop()
	let data = DATA_get_current_state_point(true)
	//Selected voice
	let selected_voice_data= data.voice_data.find(item=>{
		return item.selected
	})
	selected_voice_data.solo=!selected_voice_data.solo
	//var PMC_reload = flag_DATA_updated.PMC
	DATA_insert_new_state_point(data)
	PMC_populate_voice_list()
	BNAV_update_V(data)
	//no need to load
	flag_DATA_updated.PMC=true
}

function PMC_selected_voice_delete_A(){
	let data = DATA_get_current_state_point(true)
	//Selected voice
	let selected_voice_data= data.voice_data.find(item=>{
		return item.selected
	})
	DATA_voice_delete_A(selected_voice_data.voice_id)
}

function PMC_selected_voice_add_A(){
	let data = DATA_get_current_state_point(true)
	//Selected voice
	let selected_voice_data= data.voice_data.find(item=>{
		return item.selected
	})
	DATA_voice_add_A(selected_voice_data.voice_id)
}

function PMC_selected_voice_explode_A(){
	let data = DATA_get_current_state_point(true)
	//Selected voice
	let selected_voice_data= data.voice_data.find(item=>{
		return item.selected
	})
	DATA_voice_explode_A(selected_voice_data.voice_id)
}


