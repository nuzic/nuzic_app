//init
initvalues()//trigger initvalues

function initvalues(){
	//with database
	document.querySelector("title").innerHTML="Nuzic App"
	document.getElementById("App_version").innerHTML=version
	console.log("Current App version: "+version+" commit "+ hash)
	APP_loader_zero()
	//no wait for language to load
	DATA_load_languages()
	//control App_body height and presence of nuzic web bar //LOCAL
	TNAV_maximize(document.getElementById("App_Nav_maximize"))
	TNAV_maximize(document.getElementById("App_Nav_maximize"))
	DATA_update_midi_devices(true)

	DATA_load_preferences_file()
	var data = DATA_new_composition_data()
	DATA_initialize_state_point(data)

	//load scale list from general database prima
	DATA_load_global_scale_list()
	//load user scale list
	DATA_load_user_scale_list()

	//PMC
	//ZOOM
	PMC_set_zoom_time(1)
	PMC_set_zoom_sound(1)

	DATA_load_state_point_data(true)

	TNAV_reset_composition_name()
	current_composition_id=""
	if(typeof Reset_current_composition_id === "function"){
		Reset_current_composition_id()
	}

	//Virtual Keyboard
	BNAV_set_keyboard_starting_note(0,3)
	//BNAV_draw_keyboard() //done by DATA_load_state_point TRUE

	if(DEBUG_MODE){
		let data_test = APP_data_test("v11")
		//let data_test = APP_data_test("v10opt")
		//let data_test = APP_data_test("v9.1")
		//let data_test = APP_data_test("v8")
		//let data_test = APP_data_test("v2")
		DATA_insert_new_state_point(data_test)
		DATA_load_state_point_data(true)
		//APP_selection_options={"target": 'segment',"working_space": 'RE',"voice_number": 0,"voice_id": 0,"segment_index": 1}
		//APP_selection_options.target='column'
		//APP_selection_options.target='voice'
		//APP_selection_options.working_space='RE'
		//APP_show_operation_dialog_box('operations')
		//copied_data={id:"segment",data:[{voice_id:1,neopulse:{N:1,D:1},Psg_segment_start:0,segment:data_test.voice_data[0].data.segment_data[0]}]}
		//APP_show_operation_dialog_box('paste')

		// CALC_add_line()
		//some calc linea
		// let new_line = CALC_generate_generic_line_data([0,1,2,3,4,5,6],1,'calc_generate_random',[0,6,7])
		// let data_lines=CALC_get_current_state_point()
		// data_lines.push(new_line)
		//data[line_index].children.push(new_name)
		//select first line bf saving
		// var left_line = document.querySelector('.App_BNav_calc_lines_container').children[line_index]
		// CALC_select_line(left_line)
		// CALC_insert_new_state_point(data_lines)
		// CALC_write_line_from_data(new_line)
		// CALC_refresh_line_counter()
		//CALC_display_menu_export()
		//CALC_display_menu_import()
		//CALC_display_menu_translate()//gen error!!
		//CALC_display_menu_force()//gen error!!
	}else{
		//POPUP before leaving
		window.addEventListener('beforeunload', function (e) {
			e.preventDefault()
			e.returnValue = ''
		})
	}
	APP_disable_voice_instrument_buttons()
}
