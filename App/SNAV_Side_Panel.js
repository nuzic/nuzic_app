function SNAV_set_default_icons(){
	let sideNavParent = document.querySelector('.App_SNav_button_container')
	let sideNavButtonsArr = [...sideNavParent.querySelectorAll('button')]
	sideNavButtonsArr.forEach(function(el){
		let img= el.getElementsByTagName('img')
		img[2].style.display='none'
		img[1].style.display='none'
		img[0].style.display='flex'
		let input = el.getElementsByTagName('input')
		if(input[0].checked){
			input[0].checked = false
		}
	})
}

function SNAV_display_menu_content(button){
	if(button.querySelector('input').checked){
		SNAV_hide_menu_content()
		SNAV_set_default_icons()
		let img= button.getElementsByTagName('img')
		//console.log(img)
		//img[0].src=iconsArr[el.value]
		img[2].style.display='none'
		img[0].style.display='none'
		img[1].style.display='flex'
		return
	}
	let sideBugButton = document.getElementById('bug_button')
	if(sideBugButton.value == 1){
		SNAV_show_bug_report(sideBugButton)
	}
	SNAV_set_default_icons()
	let menu_content = document.querySelector(".App_SNav_content")
	let voice_line_selector = document.querySelector('.App_SNav_line_selector')
	let parameters = document.querySelector('.App_SNav_parameters')
	let notebook = document.querySelector('.App_SNav_notebook')
	let share = document.querySelector('.App_SNav_share')
	let settings = document.querySelector('.App_SNav_settings')
	let app_box = document.querySelector('.App_Box')
	let tab1 = document.getElementById('Parameter_input')
	let tab2 =  document.getElementById('Notes_input')
	let tab3 =  document.getElementById('Share_input')
	let tab4 = document.getElementById('UserOptions')
	let buttonIcon = button.getElementsByTagName('img')
	buttonIcon[0].style.display='none'
	buttonIcon[1].style.display='none'
	buttonIcon[2].style.display='flex'
	menu_content.style["display"] = "flex"
	tab1.checked = false
	tab2.checked = false
	tab3.checked = false
	tab4.checked = false
	if (button.value == 1){
		tab1.checked = true
		parameters.style["display"] = "flex"
		notebook.style["display"] = "none"
		share.style["display"] = "none"
		settings.style["display"] = "none"
	}else if(button.value == 2){
		tab2.checked = true
		parameters.style["display"] = "none"
		notebook.style["display"] = "block"
		share.style["display"] = "none"
		settings.style["display"] = "none"
	}else if(button.value == 3){
		tab3.checked = true
		parameters.style["display"] = "none"
		notebook.style["display"] = "none"
		share.style["display"] = "block"
		settings.style["display"] = "none"
	}else if(button.value == 4){
		tab4.checked = true
		parameters.style["display"] = "none"
		notebook.style["display"] = "none"
		share.style["display"] = "none"
		settings.style["display"] = "block"
	}
	app_box.style.width = 'calc(100% - var(--RE_block5) * 2)'
}

function SNAV_hide_menu_content(){
	SNAV_set_default_icons()
	let menu_content = document.querySelector(".App_SNav_content")
	let app_box = document.querySelector('.App_Box')
	menu_content.style["display"] = 'none'
	app_box.style.width = 'calc(100% - var(--RE_block2))'
}

function SNAV_display_menu_share(){
	let sideNavParent = document.querySelector('.App_SNav_button_container')
	let sideNavButtonsArr = [...sideNavParent.querySelectorAll('button')]
	SNAV_display_menu_content(sideNavButtonsArr[2])
}

function SNAV_buttonHoverEnter(button){
	let buttonInput= button.getElementsByTagName('input')
	let buttonIcon = button.getElementsByTagName('img')
	if(buttonInput[0].checked==true){
		return
	}else{
		buttonIcon[0].style.display="none"
		buttonIcon[1].style.display="block"
		buttonIcon[2].style.display="none"
	}
}

function SNAV_buttonHoverExit(button){
	let buttonInput= button.getElementsByTagName('input')
	let buttonIcon = button.getElementsByTagName('img')
	if(buttonInput[0].checked==true){
		return
	}else{
		buttonIcon[0].style.display="block"
		buttonIcon[1].style.display="none"
		buttonIcon[2].style.display="none"
	}
}

function SNAV_switch_play_new_note(){
	//reversed because it is not yet switched
	let pressed = SNAV_read_play_new_note()
	if(pressed){
		SNAV_set_play_new_note(false)
	}else{
		SNAV_set_play_new_note(true)
	}
	//Save state preferences
	DATA_save_preferences_file()
}

function SNAV_read_play_new_note() {
	let button = document.querySelector("#activate_play_new_note_option button")
	return (button.value === "true")
}

function SNAV_set_play_new_note(value) {
	let button = document.querySelector("#activate_play_new_note_option button")
	button.value=value
	if(value){
		button.innerHTML='On'
	}else{
		button.innerHTML='Off'
	}
}

function SNAV_switch_note_on_click(){
	//reversed because it is not yet switched
	let pressed = SNAV_read_note_on_click()
	if(pressed){
		SNAV_set_note_on_click(false)
	}else{
		SNAV_set_note_on_click(true)
	}
	//Save state preferences
	DATA_save_preferences_file()
}

function SNAV_read_note_on_click() {
	let button = document.querySelector("#activate_play_click_note_option button")
	return (button.value === "true")
}

function SNAV_set_note_on_click(value) {
	let button = document.querySelector("#activate_play_click_note_option button")
	button.value=value
	if(value){
		button.innerHTML='On'
	}else{
		button.innerHTML='Off'
	}
}

function SNAV_switch_note_over_grade(){
	//reversed because it is not yet switched
	let pressed = SNAV_read_note_over_grade()
	if(pressed){
		SNAV_set_note_over_grade(false)
	}else{
		SNAV_set_note_over_grade(true)
	}
	//Save state preferences
	DATA_save_preferences_file()
}

function SNAV_read_note_over_grade() {
	let button = document.querySelector("#activate_note_over_grade_option button")
	return (button.value === "true")
}

function SNAV_set_note_over_grade(value) {
	let button = document.querySelector("#activate_note_over_grade_option button")
	button.value=value
	if(value){
		button.innerHTML='On'
	}else{
		button.innerHTML='Off'
	}
}

function SNAV_switch_alert_overwrite_scale(){
	//reversed because it is not yet switched
	let pressed = SNAV_read_alert_overwrite_scale()
	if(pressed){
		SNAV_set_alert_overwrite_scale(false)
	}else{
		SNAV_set_alert_overwrite_scale(true)
	}
	//Save state preferences
	DATA_save_preferences_file()
}

function SNAV_read_alert_overwrite_scale() {
	let button = document.querySelector("#show_alert_overwrite_scale_option button")
	return (button.value === "true")
}

function SNAV_set_alert_overwrite_scale(value) {
	let button = document.querySelector("#show_alert_overwrite_scale_option button")
	button.value=value
	if(value){
		button.innerHTML='On'
	}else{
		button.innerHTML='Off'
	}
}

function SNAV_switch_export_audio_file_format() {
	let button = document.querySelector("#export_audio_file_type_option button")
	let value=button.value
	if(value=="mp3"){
		button.innerHTML='wav'
		button.value='wav'
	}else{
		button.innerHTML='mp3'
		button.value='mp3'
	}
}

function SNAV_read_export_audio_file_format() {
	let button = document.querySelector("#export_audio_file_type_option button")
	return value=button.value
}

async function SNAV_show_bug_report(button){
	let sideNavContent = document.querySelector('.App_SNav_content')
	let bugsDivContent = document.querySelector('.App_SNav_bug_content')
	let app_box = document.querySelector('.App_Box')
	if(sideNavContent.style.display != 'none'){
		SNAV_hide_menu_content()
	}
	if(button.value == 0){
		bugsDivContent.style.display="flex"
		app_box.style.width = 'calc(100% - var(--RE_block5) * 2)'
		button.classList.add('pressed')
		button.value ++;

		//show bug loader
		let bug_loader=document.querySelector('.App_SNav_bug_loader')
		if( window.nuzic_web_current_user==5 || DEBUG_MODE){
			bug_loader.style.display='flex'
		}else{
			bug_loader.style.display='none'
		}

		if(typeof wp_Get_User_Permissions!='function' || DEBUG_MODE){
			let user = 0
			if(DEBUG_MODE){
				user=10
			}else{
				return
			}
			if(user==0){
				let bugSentTitle = document.querySelector('.App_SNav_bug_noUserTitle')
				let bugSentText = document.querySelector('.App_SNav_bug_noUserText')
				let sendBugButton = document.querySelector('.App_SNav_bug_sendButton')
				bugSentText.style.display='block'
				bugSentTitle.style.display='block'
				sendBugButton.classList.add('noUser')
				sendBugButton.disabled = true
			}
		}else{
			let user = await wp_Get_User_Permissions('','levels')
			if(user==0){
				let bugSentTitle = document.querySelector('.App_SNav_bug_noUserTitle')
				let bugSentText = document.querySelector('.App_SNav_bug_noUserText')
				let sendBugButton = document.querySelector('.App_SNav_bug_sendButton')
				bugSentText.style.display='block'
				bugSentTitle.style.display='block'
				sendBugButton.classList.add('noUser')
				sendBugButton.disabled = true
			}
		}
	}else{
		let bugSentTitle = document.querySelector('.App_SNav_bug_sendTitle')
		let bugParent= document.querySelector('.App_SNav_bug_content_fields')
		let bugSentText = document.querySelector('.App_SNav_bug_sendText')
		bugSentTitle.style.display='none'
		bugSentText.style.display='none'
		app_box.style.width = 'calc(100% - var(--RE_block2))'
		bugsDivContent.style.display="none"
		button.classList.remove('pressed')
		button.value = 0
	}
}

function SNAV_toggle_sendConsolePrint(bttn){
	if(bttn.value==1){
		bttn.value=0
	}else{
		bttn.value=1
	}
}

function SNAV_send_bug(){
	let bugTitle = document.getElementById('App_SNav_bug_title')
	let bugDescription = document.getElementById('App_SNav_bug_description')
	if(bugTitle.innerHTML!='' && bugDescription.innerHTML!=''){
		let bugSend_animation = document.querySelector('.App_SNav_bug_sending')
		bugSend_animation.style.display="block"
		if(typeof Save_server_BUG!='function' || DEBUG_MODE){
			setTimeout(function () {
				SNAV_user_feedback_on_send_bug(true)
			}, 2000)
		}else {
			Save_server_BUG()
		}
		bugTitle.innerHTML=''
		bugDescription.innerHTML=''
		let bugButton = document.querySelector('.App_SNav_bug_sendButton')
		bugButton.style.backgroundColor='var(--Nuzic_red_light)'
		bugButton.style.color='var(--Nuzic_light)'
		bugButton.disabled=true
		//close menu
		//SNAV_show_bug_report(document.getElementById("bug_button"))
	}else{
		bugButton.style.backgroundColor='var(--Nuzic_red)'
	}
}

function SNAV_user_feedback_on_send_bug(success,error_text){
	let bugSentTitle = document.querySelector('.App_SNav_bug_sendTitle')
	let bugSentText = document.querySelector('.App_SNav_bug_sendText')
	document.querySelector('.App_SNav_bug_sending').style.display="none"
	if(success){
		bugSentTitle.style.display='block'
		bugSentText.style.display='block'
	}else{
		console.error("BUG not sent")
		bugSentTitle.style.display='none'
		bugSentText.style.display='none'
	}
	//no need to console.log(error_text) already done by web
}

function SNAV_test_send_bug(){
	let bugTitle = document.getElementById('App_SNav_bug_title')
	let bugDescription = document.getElementById('App_SNav_bug_description')
	let bugButton = document.querySelector('.App_SNav_bug_sendButton')
	//disable animation
	document.querySelector('.App_SNav_bug_sending').style.display="none"
	//disable result senging
	document.querySelector('.App_SNav_bug_sendTitle').style.display="none"
	document.querySelector('.App_SNav_bug_sendText').style.display="none"
	if(bugTitle.innerHTML=='' || bugTitle.innerHTML=='<br>'  || bugDescription.innerHTML=='' || bugDescription.innerHTML=='<br>'){
		bugButton.style.backgroundColor='var(--Nuzic_red_light)'
		bugButton.style.color='var(--Nuzic_light)'
		bugButton.disabled=true
	//}else if(bugTitle.innerHTML!='' && bugDescription.innerHTML!=''){
	}else{
		bugButton.style.backgroundColor='var(--Nuzic_green)'
		bugButton.style.color='var(--Nuzic_dark)'
		bugButton.disabled=false
	}
}

function SNAV_select_zoom(selector) {
	let zoom = Number(selector.value)
	let App_RE_data = document.querySelector(".App_RE")
	let App_PMC_data = document.querySelector(".App_PMC")
	let transformOrigin = [0,0]
	let p = ["webkit", "moz", "ms", "o"],
		s = "scale(" + zoom + ")",
		oString = (transformOrigin[0] * 100) + "% " + (transformOrigin[1] * 100) + "%"
	for (let i = 0; i < p.length; i++) {
		App_RE_data.style[p[i] + "Transform"] = s
		App_RE_data.style[p[i] + "TransformOrigin"] = oString
		App_PMC_data.style[p[i] + "Transform"] = s
		App_PMC_data.style[p[i] + "TransformOrigin"] = oString
	}
	App_RE_data.style["transform"] = s
	App_RE_data.style["transformOrigin"] = oString
	App_PMC_data.style["transform"] = s
	App_PMC_data.style["transformOrigin"] = oString
	let percent_string = 100/zoom+"%"
	App_RE_data.style.maxHeight = percent_string
	App_RE_data.style.minHeight = percent_string
	App_RE_data.style.maxWidth = percent_string
	App_RE_data.style.minWidth = percent_string
	App_RE_data.style["width"]= percent_string
	App_RE_data.style["height"]= percent_string
	App_PMC_data.style.maxHeight = percent_string
	App_PMC_data.style.minHeight = percent_string
	App_PMC_data.style.maxWidth = percent_string
	App_PMC_data.style.minWidth = percent_string
	App_PMC_data.style["width"]= percent_string
	App_PMC_data.style["height"]= percent_string
}

async function SNAV_load_console_data_bug(){
	let id = document.querySelector(".App_SNav_bug_loader input").value
	console.log("Printing BUG id "+id+" DATA:")
	if(typeof WebApp_get_bug_console=='function'){
			let bug_data_text = await WebApp_get_bug_console(id)
			bug_data=eval(bug_data_text)
			console.log(bug_data)
			console.log("-----------------")
			console.log("COMMANDS:")
			console.log("SNAV_show_BUG()")
			console.log("SNAV_load_BUG_data(line)")
	}
}

function SNAV_show_BUG(){
	console.log(bug_data)
}

function SNAV_load_BUG_data(line){
	console.error("XXX do things")
	console.log(bug_data[line])
				//XXX do things

}
