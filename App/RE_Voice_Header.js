function RE_minimized_voice_header_button(button){
	let voice_obj = button.closest(".App_RE_voice")
	//maximize
	//make sure voice is visible
	let visible = RE_voice_is_visible(voice_obj)
	if(!visible){
		let [,voice_id] = RE_read_voice_id(voice_obj)
		DATA_show_hide_voice_RE(voice_id,true)
	}
}

function RE_show_hide_voice_button(button){
	let voice_obj = button.closest(".App_RE_voice")
	let [,voice_id] = RE_read_voice_id(voice_obj)
	let show
	if(button.classList.contains("show")){
		show=false
	}else{
		show=true
	}
	DATA_show_hide_voice_RE(voice_id,show)
}

function RE_voice_is_visible(voice){
	let toggle = voice.querySelector(".App_RE_voice_visible")
	let class_string= toggle.className
	if (class_string.includes("show")){
		return true
	} else {
		return false
	}
}

function RE_block_unblock_voice(voice_id,button){
	let class_string= button.className
	let RE_voice_block_icons = [...button.querySelectorAll(".App_RE_icon")]
	if (class_string.includes("pressed")){
		button.classList.remove("pressed")
		RE_voice_block_icons[0].style["display"] = 'none'
		RE_voice_block_icons[1].style["display"] = 'flex'
		let data = DATA_get_current_state_point(true)
		//Selected voice
		let voice_obj = button.closest(".App_RE_voice")
		voice_obj.classList.remove("blocked")
		let selected_voice_data= data.voice_data.find(item=>{
			return item.voice_id==voice_id
		})
		selected_voice_data.blocked = false
		//var PMC_reload = flag_DATA_updated.PMC
		DATA_insert_new_state_point(data)
		//no need to load
		//flag_DATA_updated={PMC:PMC_reload,RE:true}
		flag_DATA_updated.RE=true
		RE_redraw_column_selection(data)
	} else {
		//control if voice is blockable
		//find all blocked voices
		let data = DATA_get_current_state_point(true)
		//Selected voice
		let voice_obj = button.closest(".App_RE_voice")
		let current_voice_data= data.voice_data.find(item=>{
			return item.voice_id==voice_id
		})
		let blocked_voice_list = data.voice_data.filter(item=>{
			return item.blocked==true
		})
		//verify if voice is blockable
		let is_blockable = false
		if(blocked_voice_list.length==0 ) {
			is_blockable=true
		}else{
			//verify long and number every segment is the same
			let N_NP_this_voice= current_voice_data.neopulse.N
			let D_NP_this_voice= current_voice_data.neopulse.D
			let segment_data_list = current_voice_data.data.segment_data
			let segment_L_base_list = segment_data_list.map(item=>{
				let last_time = item.time.slice(-1)[0]
				return Math.round(last_time.P*N_NP_this_voice/D_NP_this_voice)
			})

			//create give a list of segment Longitud of a blocked voices and translate to 1/1 //XXX  TS
			let first_blocked_voice_data= blocked_voice_list[0]
			let N_NP= first_blocked_voice_data.neopulse.N
			let D_NP= first_blocked_voice_data.neopulse.D
			let first_blocked_segment_data_list = first_blocked_voice_data.data.segment_data
			let first_blocked_segment_L_base_list = first_blocked_segment_data_list.map(item=>{
				let last_time = item.time.slice(-1)[0]
				return Math.round(last_time.P*N_NP/D_NP)
			})

			if(segment_L_base_list.length==first_blocked_segment_L_base_list.length){
				let not_equal = segment_L_base_list.some((segment_L_base,index)=>{
					return segment_L_base!=first_blocked_segment_L_base_list[index]
				})
				if(!not_equal)is_blockable=true
			}
		}
		if(is_blockable){
			button.classList.add("pressed")
			voice_obj.classList.add("blocked")
			RE_voice_block_icons[0].style["display"] = 'flex'
			RE_voice_block_icons[1].style["display"] = 'none'
			current_voice_data.blocked = true
			//verify if segment names are the same
			let segment_name_changed=false
			if(blocked_voice_list.length!=0 ) {
				var first_blocked_voice_data= blocked_voice_list[0]
				first_blocked_voice_data.data.segment_data.forEach((segment,index)=>{
					if(segment.segment_name!=current_voice_data.data.segment_data[index].segment_name){
						current_voice_data.data.segment_data[index].segment_name=segment.segment_name
						segment_name_changed=true
					}
				})
			}
			//warning segment name changed
			if(segment_name_changed){
				APP_info_msg("bk_v_change_s_name")
				DATA_insert_new_state_point(data)
				DATA_load_state_point_data(false,true)
			}else{
				//var PMC_reload = flag_DATA_updated.PMC
				DATA_insert_new_state_point(data)
				//no need to load
				//flag_DATA_updated={PMC:PMC_reload,RE:true}
				flag_DATA_updated.RE=true
				RE_redraw_column_selection(data)
			}
		}
	}
}

function RE_voice_is_blocked(voice){
	if(voice==null)return false
	let button = voice.querySelector(".App_RE_voice_block")
	let class_string= button.className
	if (class_string.includes("pressed")){
		return true
	}else{
		return false
	}
}

function RE_change_voice_instrument(voice_id,button){
	APP_stop()
	//better using array
	let options = {"voice_id":voice_id,"instrument":Number(button.value)}
	APP_show_instrument_selector(options)
}

function RE_which_voice_instrument(voice){  //XXX
	//console.error("OBSOLETE FUNCTION, use database DATA_which_voice_instrument(voice_id)")
	let voice_button = voice.querySelector(".App_RE_voice_instrument")
	return parseInt(voice_button.value)
}

function RE_change_metro_sound(voice_id,current_value,button){
	APP_stop()
	let available_metro_sounds = DATA_list_available_metro_sounds()
	//var max_value = 5
	let max_value = available_metro_sounds-length-1
	current_value = available_metro_sounds.find(item=>{return item>current_value})
	//null if current = 5 (last one)
	if(current_value>max_value || current_value==null)current_value=0
	if(current_value==0){
		button.innerHTML="M"
		button.classList.add('App_RE_text_strikethrough')
	}else{
		button.classList.remove('App_RE_text_strikethrough')
		button.innerHTML="M"+current_value
	}
	let data = DATA_get_current_state_point(true)
	//Selected voice
	let selected_voice_data= data.voice_data.find(item=>{
		return item.voice_id==voice_id
	})
	selected_voice_data.metro=current_value
	DATA_insert_new_state_point(data)
	BNAV_update_V(data)
	button.setAttribute('onclick', `RE_change_metro_sound(${voice_id},${current_value},this)`);
	//no need to load
	flag_DATA_updated.RE=true
}

function RE_which_metro_sound(voice){
	let metro_button = voice.querySelector(".App_RE_metro_sound")
	return parseInt(metro_button.value)
}

function RE_change_voice_name(voice_id,element){
	element.blur()
	let data = DATA_get_current_state_point(true)
	//Selected voice
	let selected_voice_data= data.voice_data.find(item=>{
		return item.voice_id==voice_id
	})
	selected_voice_data.name = element.value
	let PMC_reload = flag_DATA_updated.PMC
	DATA_insert_new_state_point(data)
	if(PMC_reload)PMC_populate_voice_list()
	//no need to load
	flag_DATA_updated={PMC:PMC_reload,RE:true}
	BNAV_update_V(data)
}

function RE_read_voice_volume(voice){
	//find voice index array
	let voice_id=parseInt(voice.getAttribute("value"))
	// V_global_variable index volume
	return V_global_variables.volumes[voice_id].volume
}

//return true if voice is muted
function RE_mute_unmute_voice(voice_id,button) {
	APP_stop()
	let data = DATA_get_current_state_point(true)
	//Selected voice
	let voice_obj = button.closest(".App_RE_voice")
	let selected_voice_data= data.voice_data.find(item=>{
		return item.voice_id==voice_id
	})
	selected_voice_data.mute=!selected_voice_data.mute
	if(button.classList.contains("pressed")){
		button.classList.remove("pressed")
	}else{
		button.classList.add("pressed")
	}
	DATA_insert_new_state_point(data)
	BNAV_update_V(data)
	//no need to load
	flag_DATA_updated.RE=true
}

function RE_change_solo_voice(voice_id,button){
	APP_stop()
	let data = DATA_get_current_state_point(true)
	//Selected voice
	let selected_voice_data= data.voice_data.find(item=>{
		return item.voice_id==voice_id
	})
	selected_voice_data.solo=!selected_voice_data.solo
	if(selected_voice_data.solo){
		button.classList.add("pressed")
	}else{
		button.classList.remove("pressed")
	}
	DATA_insert_new_state_point(data)
	BNAV_update_V(data)
	flag_DATA_updated.RE=true
}

function RE_enter_new_NP(element){
	if(element.value == previous_value) return
	let voice_obj = element.closest(".App_RE_voice")
	let [N,D]= RE_calculate_NP(voice_obj)
	if(N==0 || D==0 || isNaN(D)){
		APP_info_msg("enterNP_fail")
		APP_blink_error(element)
		element.value = previous_value
		return
	}
	let [,voice_id] = RE_read_voice_id(voice_obj)
	let success = DATA_calculate_force_voice_NP(voice_id,N,D)
	if(!success){
		APP_info_msg("enterNP_fail")
		APP_blink_error(element)
		element.value = previous_value
	}

	/*/calculating if every segment existing is multiple of N
	var NP_ok=false
	var data = DATA_get_current_state_point(true)
	var segment_Ls_list = []
	data.voice_data.forEach(voice=>{
		voice.data.segment_data.forEach(segment=>{
			segment_Ls_list.push(segment.time.slice(-1)[0].P*voice.neopulse.N/voice.neopulse.D)
		})
	})
	//console.log(segment_Ls_list)
	NP_ok=!segment_Ls_list.find(Ls=>{return Ls%N!=0})
	if(!NP_ok){
		element.value = previous_value
		APP_blink_error(element)
		console.error("at least one segment Ls in the composition not divisible by N neopulse")
		APP_info_msg("enterNP_fail")
		return
	}

	APP_stop()
	//Selected voice
	var [,voice_id] = RE_read_voice_id(voice_obj)
	var selected_voice_data= data.voice_data.find(item=>{
		return item.voice_id==voice_id
	})

	//changing L segments to new one and clearing them
	var N_old = selected_voice_data.neopulse.N
	var D_old = selected_voice_data.neopulse.D

	selected_voice_data.data.segment_data.forEach(segment=>{
		let Pend = (Math.round(segment.time.slice(-1)[0].P*N_old/D_old))*(D/N)
		segment.time = [{"P":0,"F":0},{"P": Pend,"F":0}]
		segment.fraction = [{"N": 1,"D": 1,"start": 0,"stop":Pend}]
		segment.sound=[{note:-1,diesis:true,A_pos:[],A_neg:[]},{note:-4,diesis:true,A_pos:[],A_neg:[]}]
	})
	selected_voice_data.neopulse={"N":N,"D":D}
	//recalc midi
	selected_voice_data.data.midi_data=DATA_segment_data_to_midi_data(selected_voice_data.data.segment_data,selected_voice_data.neopulse)
	APP_info_msg("enterNP")
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)*/
}

function RE_show_pulse_sound_selector(voice_id,e_sound){
	APP_stop()
	let options = {"voice_id":voice_id,"e_sound":e_sound}
	APP_show_pulse_sound_selector(options)
}
