function APP_generate_midi_blob(current_state_point){
	// current_state_point.voice_data[voice_number].data.midi_data
	// contain
	// .time
	// .note (freq)
	// .duration
	//with jsmidgen.js
	if(current_state_point.global_variables.TET!=12){
		alert("Can't convert to MIDI a non TET 12 sound (YET!) contact admin to ask info")  // display string message
		return null
	}else{
		let file = new Midi.File()
		let tiks_N = 32767
		file.ticks = tiks_N
		let tick_L = (60/PPM)/tiks_N
		//create 1 track for every voice or chord list (NOTE) + 1 track for PULSE (if present)
		let channel=0
		current_state_point.voice_data.forEach(voice=>{
			let track_velocity=voice.vel
			let track_volume=voice.volume//XXX
			let track=new Midi.Track()
			let track_pulse=new Midi.Track()
			track.setTempo(current_state_point.global_variables.PPM) //NEOPULSE not inserted
			track_pulse.setTempo(current_state_point.global_variables.PPM)
			//voice.name
			track.addEvent(new Midi.MetaEvent({type: Midi.MetaEvent.TRACK_NAME, data: voice.name }))
			track_pulse.addEvent(new Midi.MetaEvent({type: Midi.MetaEvent.TRACK_NAME, data: voice.name+" Pulse" }))
			let number = (voice.instrument<128)?voice.instrument: (voice.instrument==128)?0:1
			let number_pulse = 0
			let e_sound=voice.e_sound
			//let track_channel = channel
			let track_channel = (voice.instrument<128)?0:9
			//channel=(channel<9)?channel++:channel
			let track_pulse_channel = 9
			let hexStr = "0x"+ number.toString(16)
			let hexStr_pulse = "0x"+ number_pulse.toString(16)
			track.setInstrument(track_channel, hexStr)
			track_pulse.setInstrument(track_pulse_channel, hexStr_pulse)
			//adding notes
			// voice.data.midi_data
			// .duration
			// .note
			// .time
			//add midi objects
			// i==0?track.addNoteOn(track_channel, note_Midi, time_list[index]):track.addNoteOn(track_channel, note_Midi)
			// i==0?track.addNoteOff(track_channel, note_Midi, time_list[index+1]):track.addNoteOff(track_channel, note_Midi)
			//alternatively, a chord may be created with the addChord function
			// .addChord(0, ['c4', 'e4', 'g4'], 64)
			let current_d_time=0
			let current_d_time_pulse=0
			//for L
			let last_duration_converted=0
			let last_duration_converted_pulse=0
			let last_note=-1
			let last_pulse=-1
			let pulse_track=false
			let note_track=false
			voice.data.midi_data.note_freq.forEach((note_str,index_event) =>{
				let note = +(note_str[0])
				let duration_freq = voice.data.midi_data.duration[index_event]
				//cange duration frequency in seconds
				let duration = 1/duration_freq
				if(note<0){ //XXX XXX XXX XXX
					if(note==-1){
						//silence
						_end_last_events_here()
						let duration_converted = Math.round(duration/tick_L)
						current_d_time_pulse+= duration_converted
						current_d_time+= duration_converted
						//end last note pulse in this position
					}else if(note==-2){
						pulse_track=true
						_end_last_events_here()
						//nuzic_note 22 --> MIDI note 34 //XXX XXX XXX XXX depends on e_sound voice
						let note_Midi = e_sound+12
						let duration_converted = Math.round(duration/tick_L)
						track_pulse.addNoteOn(track_pulse_channel, note_Midi, current_d_time_pulse, track_velocity)
						//track_pulse.addNoteOff(track_pulse_channel, note_Midi, duration_converted)
						last_duration_converted_pulse=duration_converted
						last_pulse=note_Midi
						//current_d_time_pulse=0
						current_d_time+= duration_converted
					}else if(note==-3){
						//ligadura
						let duration_converted = Math.round(duration/tick_L)
						current_d_time_pulse+= duration_converted
						current_d_time+= duration_converted
						last_duration_converted+=duration_converted
						last_duration_converted_pulse+=duration_converted
					}
				}else{
					//note
					note_track=true
					_end_last_events_here()
					let duration_converted = Math.round(duration/tick_L)
					//for tone there are 1000 ticks for pulse
					last_note = []
					let first=true
					note_str.forEach(note=>{
						let nM=DATA_Hz_to_midi(note,12).midi_note
						//console.log(note+"hz ->"+nM)
						if(first){
							track.addNoteOn(track_channel, nM, current_d_time,track_velocity)
							first=false
						}else{
							track.addNoteOn(track_channel, nM, null , track_velocity)//null -> 0 , velocity??? XXX XXX XXX
						}
						last_note.push(nM)
					})
					last_duration_converted=duration_converted
					current_d_time_pulse+= duration_converted
				}
			})
			_end_last_events_here()
			//ADD A TRACKS
			if (pulse_track){
				file.addTrack(track_pulse)
			}
			if (note_track || !pulse_track){
				//add note track even if is empty if there is no pulse track
				file.addTrack(track)
			}
			function _end_last_events_here(){
				if(last_note!=-1 && last_duration_converted!=0){
					let first=true
					last_note.forEach(note=>{
						if(first){
							//track.addNoteOff(track_channel, note, last_duration_converted,track_velocity)//XXX???
							track.addNoteOff(track_channel, note, last_duration_converted)
							first=false
						}else{
							track.addNoteOff(track_channel, note)//velocity??? XXX XXX XXX
						}
					})
					current_d_time=0
					last_note=-1
				}
				if(last_pulse!=-1 && last_duration_converted_pulse!=0){
					track_pulse.addNoteOff(track_pulse_channel, last_pulse, last_duration_converted_pulse)
					current_d_time_pulse=0
					last_pulse=-1
				}
			}
		})
		return file
	}
}

