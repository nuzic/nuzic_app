function CALC_initialize_state_point(){
	current_state_point_index_calc = 0
	min_state_point_index_calc=current_state_point_index_calc
	max_state_point_index_calc=current_state_point_index_calc
	state_point_array_calc[current_state_point_index_calc]=[]
}
CALC_initialize_state_point()

function CALC_insert_new_state_point(state_point){
	//save current selected state
	CALC_save_current_state_point_selection()
	//control version and save
	var data = JSON.parse(JSON.stringify(state_point))
	data.forEach(line=>{line.selected=false})
	current_state_point_index_calc++
	if(min_state_point_index_calc!=0)min_state_point_index_calc=current_state_point_index_calc+1
	if(min_state_point_index_calc==state_point_array_calc.length)min_state_point_index_calc=0
	if(current_state_point_index_calc==state_point_array_calc.length) {
		current_state_point_index_calc=0
		min_state_point_index_calc=1
	}
	max_state_point_index_calc=current_state_point_index_calc
	state_point_array_calc[current_state_point_index_calc]=data
}

function CALC_point_to_previous_state_point(){
	if(current_state_point_index_calc==min_state_point_index_calc){
		console.log("Ended number of saved sates")
		return false
	} else {
		var previous_state_point_index_calc = current_state_point_index_calc -1
		if (previous_state_point_index_calc<0)previous_state_point_index_calc=state_point_array_calc.length-1
		current_state_point_index_calc=previous_state_point_index_calc
		return true
	}
}

function CALC_get_current_state_point(){
	//return A COPY of current index state point
	var data_copy=JSON.parse(JSON.stringify(state_point_array_calc[current_state_point_index_calc]))
	return data_copy
}

function CALC_save_current_state_point_selection(){
	state_point_array_calc[current_state_point_index_calc].forEach(line=>{line.selected=false})
	var line_selected_L = document.querySelector('.App_BNav_calc_line_content.selected')
	if(line_selected_L!=null){
		var index_line = [...line_selected_L.parentNode.children].indexOf(line_selected_L)
		state_point_array_calc[current_state_point_index_calc][index_line].selected=true
	}
}


function CALC_point_to_next_state_point(){
	if(current_state_point_index_calc==max_state_point_index_calc){
		console.log("No more next saved sates")
		return false
	} else {
		var next_state_point_index_calc = current_state_point_index_calc +1
		if (next_state_point_index_calc==state_point_array_calc.length)next_state_point_index_calc=0
		current_state_point_index_calc=next_state_point_index_calc
		return true
	}
}

function CALC_undo(){
	//save current selection
	CALC_save_current_state_point_selection()
	var success = CALC_point_to_previous_state_point()
	if(!success){
		return
	} else {
		CALC_load_state_point_data()
	}
}

function CALC_redo(){
	//save current selection
	CALC_save_current_state_point_selection()
	var success = CALC_point_to_next_state_point()
	//calc_arr= Array.from(state_point_array_calc[current_state_point_index_calc])
	if(!success){
		return
	} else {
		CALC_load_state_point_data()
	}
}

function CALC_load_state_point_data(){
	var state_point = CALC_get_current_state_point()
	CALC_delete_all_lines()
	CALC_clear_selection()
	state_point.forEach(line => {
		//focus = false
		CALC_write_line_from_data(line)
	})
	CALC_refresh_line_counter()
}

//modify database
function CALC_data_add_new_line(data){
	/*var line = {
		content:[],//		content:['2','5','6'],
		name:CALC_new_line_name()//		name:0, // number
		origin:{operation:'calc_add',values:[],active:true}//calc_add always active:true
		type:'n',
		parent:[],
		children:[],
		partingNote:'',
		selected:false,
	}*/
	var line=CALC_generate_generic_line_data([],CALC_new_line_name(),'calc_add')
	data.push(line)
	return line
}

function CALC_generate_generic_line_data(content,name,operation,value_array=[],parent_array=[]){
	var line = {
		content:content,
		name:name,
		origin:{operation:operation,values:value_array,active:true},
		type:'n',
		parent:parent_array,
		children:[],
		//partingNote:'',//XXX???
		selected:true,
	}
	return line
}

function CALC_data_remove_line(index){
	var state_point = CALC_get_current_state_point()
	var children_array=state_point[index].children
	var L_parent = document.querySelector('.App_BNav_calc_lines_container')
	var L_lines_array=[...L_parent.querySelectorAll(".App_BNav_calc_line_box")]
	children_array.forEach(child=>{
		var child_index=CALC_line_name_to_index(state_point,child)
		if(child_index!=null){
			state_point[child_index].origin.active=false
			CALC_write_line_from_data(state_point[child_index],child_index)
		}
	})
	//take out from parents
	state_point.forEach(line=>{
		//filter bc can be 2 times child (sum same line)
		var target = line.children.filter((child,i)=>{
			return child==state_point[index].name
		})
		if(target.length!=0){
			target.forEach(t=>{
				line.children.splice(line.children.indexOf(t),1)
			})
		}
	})
	state_point.splice(index,1)
	return state_point
}

//function CALC_new_line_name(fromData,undoRedo,line_index){
function CALC_new_line_name(){
	let data = CALC_get_current_state_point()
	if(data.length!=0){
		let prevName = data[data.length-1].name
		var indexName=prevName+1
		//  if(undoRedo==1){ //XXX ???
		// 	indexName=line_index
		// }else if(fromData==1){ //XXX ???
		// 	indexName--
		// }
		// if(indexName<10){
		// 	indexName=`[0${indexName}]`
		// }else{
		// 	indexName=`[${indexName}]`
		// }
		return indexName
	}else{
		return 0
	}
}

