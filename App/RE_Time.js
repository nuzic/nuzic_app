//Implementation of T_line roules

//SEGMENT

function RE_break_segment_calculate_position(element,event){
	let bttn = element.querySelector("button")
	let segment_obj = element.closest(".App_RE_segment")
	let random_line_obj= segment_obj.querySelector(".App_RE_segment_line:not(.App_RE_A_pos):not(.App_RE_A_neg)")
	if(random_line_obj==null)return
	let input_list= [...random_line_obj.querySelectorAll(".App_RE_inp_box")]
	let rect = element.getBoundingClientRect()
	//console.log(bttn)
	let x = event.clientX;     // Get the horizontal coordinate
	let y = event.clientY;     // Get the vertical coordinate
	let position = x-rect.left
	let global_zoom = Number(document.querySelector('.App_SNav_select_zoom').value)
	let RE_block = nuzic_block*global_zoom
	let index = Math.floor(position/RE_block)
	let finalPosition = index*nuzic_block
	//if iT index+1
	let d_index=0
	if(random_line_obj.classList.contains("App_RE_iT"))d_index=1
	if(index == 0 || index == input_list.length-1 || input_list[index+d_index].value==''){
		bttn.style ='display:none'
	}else {
		let elementIndex = index+d_index
		let position = 0
		input_list.find((inp,index)=>{
		if (index==elementIndex)return true
			if(inp.value!="")position++
		})
		let voice_obj = segment_obj.closest(".App_RE_voice")
		let voice_id = RE_read_voice_id(voice_obj)[1]
		let segment_list= [...voice_obj.querySelectorAll(".App_RE_segment")]
		let segment_index = segment_list.indexOf(segment_obj)
		//it is a non fractioned pulse and not first or last value of a segment, now....
		//control if segment is breakable in this point
		if(bttn.style.display=='flex'){
			//console.log("already shown, stop calculations")
			return
		}
		//verify if is fractioned pulse first and then if is all ok breaking here
		if(DATA_calculate_segment_break_in_object_index(voice_id,segment_index,position)){
			bttn.style='display:flex'
		}else{
			bttn.style ='display:none'
		}
	}
	bttn.style.left = `${finalPosition}px`
	bttn.value=index
}

function RE_exit_break_segment(element){
	let bttn = element.querySelector('button')
	bttn.style ='display:none'
}

//FRACTIONS

function RE_enter_new_fraction(element){
	APP_stop()
	let segment_obj = element.closest(".App_RE_segment")
	let voice_obj = segment_obj.closest(".App_RE_voice")
	let voice_id = RE_read_voice_id(voice_obj)[1]
	let segment_list= [...voice_obj.querySelectorAll(".App_RE_segment")]
	let segment_index = segment_list.indexOf(segment_obj)
	if(element.value == previous_value){
		return
	}
	let split = element.value.split('/');
	let fractComplex = Math.floor(split[0])
	let fractSimple = Math.floor(split[1])
	let fract_list= [...segment_obj.querySelectorAll(".App_RE_Tf>.App_RE_inp_box")]
	let sound_index = fract_list.indexOf(element)
	let result = DATA_modify_fraction_value(voice_id,segment_index,sound_index,fractComplex,fractSimple)
	if(result==false){
		element.value = previous_value
		APP_blink_error(element)
		APP_info_msg("enterFr")
		return
	}
	//RE_focus_on_object_index(voice_id,segment_index,sound_index,"Tf",1)
	RE_focus_on_object_index(voice_id,segment_index,sound_index,"Tf",1)
}

function RE_break_fraction_range(button){
	APP_stop()
	let img = button.querySelector("img")
	if(img.style.display=='block'){
		//valid index
		let segment_obj = button.closest('.App_RE_segment')
		let voice_id = RE_read_voice_id(segment_obj.closest(".App_RE_voice"))[1]
		let segment_index = [...segment_obj.closest(".App_RE_voice_data").querySelectorAll(".App_RE_segment")].indexOf(segment_obj)
		let visible_line_list = [...segment_obj.querySelectorAll(".App_RE_segment_line:not(.App_RE_A_pos):not(.App_RE_A_neg)")]
		let first_input_list= [...visible_line_list[0].querySelectorAll(".App_RE_inp_box")]
		//all the calculation if breakable is done by the spawning of the button
		let target_elementIndex = Number(button.value)
		let sound_index = 0
		first_input_list.find((inp,index)=>{
			if (index==target_elementIndex)return true
			if(inp.value!="")sound_index++
		})
		let success=[false]
		if(isOdd(target_elementIndex)){
			//cutting a iT
			//if shown only iT no need to -1 sound_index
			if(!visible_line_list[0].classList.contains("App_RE_iT"))sound_index--
			success = DATA_calculate_fraction_break_in_object_index(voice_id,segment_index,sound_index,false)
		}else{
			success = DATA_calculate_fraction_break_in_object_index(voice_id,segment_index,sound_index,true)
		}
		if(success[0])DATA_break_fraction_range(voice_id,segment_index,success[1],success[2])
	}
}

function RE_break_fraction_range_calculate_position(element,event){
	let img = element.querySelector("img")
	let segment_obj = element.closest(".App_RE_segment")
	let visible_line_list = [...segment_obj.querySelectorAll(".App_RE_segment_line:not(.App_RE_A_pos):not(.App_RE_A_neg)")]
	let show_iT_frag=visible_line_list.some(line=>{return line.classList.contains("App_RE_iT")})
	let show_T_frag=visible_line_list.some(line=>{return !line.classList.contains("App_RE_iT") && !line.classList.contains("App_RE_Tf")})
	let first_input_list= [...visible_line_list[0].querySelectorAll(".App_RE_inp_box")]
	let rect_segment = segment_obj.getBoundingClientRect()
	let rect_element = element.getBoundingClientRect()
	let x = event.clientX;     // Get the horizontal coordinate
	let y = event.clientY;     // Get the vertical coordinate
	let position_px = x-rect_segment.left
	let global_zoom = Number(document.querySelector('.App_SNav_select_zoom').value)
	let RE_block = nuzic_block*global_zoom
	let elementIndex = Math.floor(position_px/RE_block)
	let finalPosition = Math.floor((x-rect_element.left)/RE_block)*nuzic_block
	//already shown, stop calculations
	let old_index= parseInt(element.value)
	if(img.style.display=='block' && old_index==elementIndex)return
	//calculate the position
	let is_A_iT = false
	let is_A_T = false
	if(show_iT_frag && !show_T_frag){
		//only it is shown
		is_A_iT=first_input_list[elementIndex].value!=""
	}else{
		if(show_iT_frag){
			is_A_iT= first_input_list[elementIndex-1].value!=""
		}
		is_A_T= first_input_list[elementIndex].value!=""
	}
	if(!is_A_iT && !is_A_T){
		img.style ='display:none'
		return
	}
	let voice_obj = segment_obj.closest(".App_RE_voice")
	let voice_id = RE_read_voice_id(voice_obj)[1]
	let segment_list= [...voice_obj.querySelectorAll(".App_RE_segment")]
	let segment_index = segment_list.indexOf(segment_obj)
	let sound_index = 0
	let index_last
	first_input_list.find((inp,index)=>{
		if (index==elementIndex)return true
		if(inp.value!="")sound_index++
	})

	//if is a iT position is the next one, need to be lowered
	// if(is_A_iT)sound_index--
	if(is_A_iT && show_T_frag)sound_index--
	let success = DATA_calculate_fraction_break_in_object_index(voice_id,segment_index,sound_index,is_A_T)
	if(success[0]){
		img.style ='display:block'
		img.style.left = `${finalPosition}px`
		element.value=elementIndex
	}else{
		img.style ='display:none'
	}
}

function RE_exit_break_fraction_range(element){
	let img = element.querySelector("img")
	img.style ='display:none'
}

function RE_join_fraction_ranges(element){
console.error("asda")
	let segment = element.closest(".App_RE_segment")
	let segment_list = [...segment.closest(".App_RE_voice_data").querySelectorAll(".App_RE_segment")]
	let segment_index = segment_list.indexOf(segment)
	let RE_Tf = segment.querySelector(".App_RE_Tf")
	let join_F_list = [...RE_Tf.querySelectorAll(".App_RE_frac_join_button")]
	let first_fraction_index=join_F_list.indexOf(element)//mo need -1 bc first fraction doesnt have a join button
	let voice_id= Number(segment.closest(".App_RE_voice").getAttribute("value"))
	//with new data function
	DATA_join_fraction_ranges(voice_id,segment_index,first_fraction_index)
}

//TIME
function RE_enter_new_value_T(element,type){
	let string = element.value
	if(string==previous_value)return
	if(type=="Tm" && string==RE_superscript_to_Tm_string(previous_value)){
		//need to rewrite
		element.value=previous_value
		return
	}
	if(type=="Tm"){
		//need to rewrite
		let input_list = [...element.parentNode.querySelectorAll(".App_RE_inp_box")]
		if(input_list[0].getAttribute("tm_triade")==""){
			element.value=previous_value
			return
		}
	}
	APP_stop()
	let position=RE_input_to_position(element)
	let voice_id = position.voice_id
	let segment_index = position.segment_index
	let sound_index = position.sound_index
	let focus_index=sound_index
	let next=0
	//verify if is last cell
	if(element.classList.contains("App_RE_last_cell_editable_input")){
		if (string!="") {
			//modify segment longitude
			let [P,F]=RE_read_Time_Element(element,type)
			if(F!=0 || P==null){
				element.value=previous_value
				APP_blink_error(element)
				APP_info_msg("enterLs")
				return
			}
			let success = DATA_change_Ls_segment(voice_id,segment_index,P)
			if(!success){
				element.value=previous_value
				APP_blink_error(element)
				APP_info_msg("enterLs")
				return
			}
			focus_index=-1//stay in the same segment
			next=0
		}else {
			element.value=previous_value
			APP_blink_error(element)
			APP_info_msg("enterLs")
			return
		}
	}else{
		if(string==""){
			if(previous_value!=""){
				//delete values
				let success = DATA_delete_object_index(voice_id,segment_index,sound_index) //if fraction range note = "e"
				if(!success){
					element.value=previous_value
					APP_info_msg("deleteT")
				}
				next=0
				focus_index++
			}else{
				return
			}
		}else{
			//control correctness new value (range and fraction)
			let [P,F]=RE_read_Time_Element(element,type)
			if(P==null){
				element.value=previous_value
				APP_blink_error(element)
				APP_info_msg("addT")
				return
			}
			if(previous_value==""){
				//added new item
				let success = DATA_enter_new_object_index(voice_id,segment_index,sound_index+1,P,F,-2,true,[],[])
				if(!success){
					element.value=previous_value
					APP_blink_error(element)
					APP_info_msg("addT")
					return
				}
				next=1
				focus_index++
			}else {
				//changed item
				let result = DATA_modify_object_index_time(voice_id,segment_index, sound_index, P,F)
				let success=result.success
				let error_type=result.error
				let error_data=result.error_info
				next=1
				if(!success){
					if(error_type==0){
						//simple
						element.value=previous_value
						APP_blink_error(element)
						APP_info_msg("modT1")
						return
					}
					if(error_type==1){
						//error on position, blink on prev fractioning and start prev frac
						element.value=previous_value
						APP_blink_error(element)
						APP_info_msg("modTrange")
						let line_inp_list = [...element.closest(".App_RE_segment_line").querySelectorAll(".App_RE_inp_box")]
						let A_PF1_index=null
						let current_index =-1
						line_inp_list.some((inp_box,index)=>{
							if(inp_box.value!=""){
								current_index++
							}
							if(current_index==error_data.error_T_indexA){
								APP_blink_error(inp_box)
								return true
							}
							return false
						})
						let segment_obj = element.closest(".App_RE_segment")
						let RE_Tf = segment_obj.querySelector(".App_RE_Tf")
						if(RE_Tf!=null){
							let input_F_list = [...RE_Tf.querySelectorAll(".App_RE_inp_box")]
							APP_blink_error(input_F_list[error_data.error_Tf_index])
						}
						return
					}
					if(error_type==2){
						//error on sound_index, blink on fractioning and stop frac
						element.value=previous_value
						APP_blink_error(element)
						APP_info_msg("modT2")
						let line_inp_list = [...element.closest(".App_RE_segment_line").querySelectorAll(".App_RE_inp_box")]
						let current_index =-1
						line_inp_list.some((inp_box,index)=>{
							if(inp_box.value!=""){
								current_index++
							}
							if(current_index==error_data.error_T_indexB){
								APP_blink_error(inp_box)
								return true
							}
							return false
						})
						let segment_obj = element.closest(".App_RE_segment")
						let RE_Tf = segment_obj.querySelector(".App_RE_Tf")
						if(RE_Tf!=null){
							let input_F_list = [...RE_Tf.querySelectorAll(".App_RE_inp_box")]
							APP_blink_error(input_F_list[error_data.error_Tf_index])
						}
						return
					}
				}
			}
		}
	}
	RE_focus_on_object_index(voice_id,segment_index,focus_index,type,next)
}

//INTERVAL

function RE_enter_new_value_dT(element,type){
	if(element.value==previous_value) return
	APP_stop()
	let position=RE_input_to_position(element)
	let voice_id = position.voice_id
	let segment_index = position.segment_index
	let insertion_index= position.insertion_index
	let next=0
	let focus_index=insertion_index
	let [duration_iT,]=RE_read_Time_Element(element,type)
	if(duration_iT==null)duration_iT=0
	//determine if
	if(previous_value==""){
		//creating new interval
		if(duration_iT==0){
			element.value=previous_value
			return
		}
		//using insertion_index
		let note =-2
		if(insertion_index==0)note=-1
		let [success,error_type,error_data] = DATA_insert_new_object_index_iT(voice_id,segment_index,insertion_index,duration_iT,note,true,[],[])
		if(!success){
			//no space for interval
			if(error_type==1){
				let current_index =-1
				let [,input_iT_list]=RE_element_index(element,".App_RE_inp_box")
				input_iT_list.some((inp_box,index)=>{
					if(inp_box.value!="" && inp_box!=element)current_index++
					if(current_index==error_data.error_iT_index){
						//found last_iT_range
						APP_blink_error(inp_box)
						return true
					}
					return false
				})
			}
			element.value=previous_value
			APP_blink_error(element)
			//stay on position doesnt work really
			return
		}
		next=1
	}else{
		//modify or deleting existing iT
		let [success,error_type,error_data] = DATA_modify_object_index_iT(voice_id,segment_index,insertion_index,duration_iT)
		//various case of insuccess
		if(!success){
			//cant deleting iT
			element.value=previous_value
			APP_blink_error(element)
			if(error_type==1){
				//no space for interval
				let current_index =-1
				let [,input_iT_list]=RE_element_index(element,".App_RE_inp_box")
				input_iT_list.some((inp_box,index)=>{
					if(inp_box.value!="")current_index++
					if(current_index==error_data.error_iT_index){
						//found last_iT_range
						APP_blink_error(inp_box)
						return true
					}
					return false
				})
			}
			if(error_type==2){
				//fraction range incompatible with new iT
				let segment_obj = element.closest(".App_RE_segment")
				let RE_Tf = segment_obj.querySelector(".App_RE_Tf")
				if(RE_Tf!=null){
					let input_F_list = [...RE_Tf.querySelectorAll(".App_RE_inp_box")]
					APP_blink_error(input_F_list[error_data.error_Tf_index])
				}
			}
			if(error_type==3){
				//voices neopulse doesn't permit change of the range
				//blink all the RE_neopulse_inp_box
				let neopulse_elements_list = [...document.querySelectorAll(".App_RE_neopulse_inp_box")]
				neopulse_elements_list.forEach(item=>{
					APP_blink_error(item)
				})
			}
			//stay on position ?? not work really
			RE_focus_on_object_index(voice_id,segment_index,insertion_index,type,-1)
			return
		}
		next=0
		if(duration_iT!=0)next=1
	}
	RE_focus_on_object_index(voice_id,segment_index,focus_index,type,next)
}

function RE_element_is_fraction_range(element){
	//return true if element is part of a range of fractioning
	let position=RE_input_to_position(element)
	let voice_id = position.voice_id
	let segment_index = position.segment_index
	let sound_index = position.sound_index
	let info = DATA_get_object_index_info(voice_id,segment_index,sound_index,0,true)
	return info.isRange
}

function RE_read_Time_Element(element, line_id=null){
	//translate element.value to P element absolute
	if(line_id==null){
		console.error("read_time without line_id")
		return
	}
	let position=RE_input_to_position(element)
	let voice_id = position.voice_id
	let segment_index = position.segment_index
	let sound_index = position.sound_index
	let pulse = -1
	let fraction = -1
	let string = element.value
	switch(line_id) {
	case "Ts":
		if(string==="") return [null,null]
		// Abs
		//code verify if prev element has same int_p and eventually omit info
		var split = element.value.split('.')
		if(split[0].length==0){
			//let info_previous_element = (sound_index-1>=0)?DATA_get_object_index_info(voice_id,segment_index,sound_index-1):null //XXX future implementation
			let previous_element = _find_prev_element(element)
			let [prev_P,]=RE_read_Time_Element(previous_element, "Ts")
			split[0]=prev_P
		}
		pulse = Math.floor(split[0])
		fraction = Math.floor(split[1]) //give 0
		if (split[1]==null) fraction=0 //never enter here , never null , is length ==0
		break;
	case "Tm":
		// mod
		if(string=="") {
			return [null,null]
		}else{
			//translate
			string = RE_superscript_to_Tm_string(string)
			let splitted = string.split('c')
			let compas_number = parseInt(splitted[1])
			let [pulse_m_string , fraction_m_string] = splitted[0].split('.')
			let pulse_m = parseInt(pulse_m_string)
			let fraction_m = parseInt(fraction_m_string)
			//case fraction undetermined = 0
			if(isNaN(fraction_m))fraction_m=0
			//case compas_number is undetermined == last one
			if(isNaN(compas_number)){
				//using last compas
				let previous_element = _find_prev_element(element)
				if (previous_element==null){
					pulse=0
					fraction = 0
					break
				}
				//special function
				compas_number=_find_previous_compas_number(element)
				function _find_previous_compas_number(element){
					let previous_element = _find_prev_element(element)
					//if previous_element is null...it never enter here
					if (previous_element==null){
						//console.error("Not found prev element compas")
						return null
					}
					let [previous_pulse_m_string,previous_fraction_m_string,previous_compas_number_string] = previous_element.getAttribute("tm_triade").split(',')
					previous_pulse_m = parseInt(previous_pulse_m_string)
					previous_fraction_m = parseInt(previous_fraction_m_string)
					previous_compas_number = parseInt(previous_compas_number_string)
					if(isNaN(previous_compas_number)){
						previous_compas_number=_find_previous_compas_number(previous_element)
					}
					return previous_compas_number
				}
				//if neither pulse was specified == last modular pulse
				if(isNaN(pulse_m)){
					//using last modular pulse
					pulse_m = _find_previous_compas_pulse_number(element)
					function _find_previous_compas_pulse_number(element){
						let previous_element = _find_prev_element(element)
						//if previous_element is null...it never enter here
						if (previous_element==null){
							console.error("Not found prev element pulse")
							return 0
						}
						let [previous_pulse_m_string,previous_fraction_m_string,previous_compas_number_string] = previous_element.getAttribute("tm_triade").split(',')
						previous_pulse_m = parseInt(previous_pulse_m_string)
						if(isNaN(previous_pulse_m)){
							previous_pulse_m=_find_previous_compas_pulse_number(previous_element)
						}
						return previous_pulse_m
					}
				}
			}else{
				//compas typed but not pulse, i suppose you wanted a 0 pulse
				if(isNaN(pulse_m))pulse_m=0
			}
			let arr = RE_calcolate_segment_time_from_compas_values(pulse_m,fraction_m,compas_number,segment_index,voice_id)
			pulse = arr[0]
			fraction = arr[1]
		}
		break;
	case "Ta":
		// not implemented
		pulse = null
		fraction = null
		break;
	case "iT":
		// distances abs
		if(string==="") return [null,null]
		pulse = Math.floor(element.value)
		fraction = null
		break;
	default:
		console.error("Error reading case Time writing roules")
	}
	return [pulse, fraction]

	function _find_prev_element(element, string="input"){
		let parent = element.parentNode
		var input_list = [...parent.querySelectorAll(string)]
		var elementIndex = input_list.indexOf(element)
		var index_previous = -1

		for (var i=elementIndex-1; i>=0;i--){
			var value = input_list[i].value
			if(value!=""){
				//element not void
				index_previous=i
				i=-1
			}
		}

		if (index_previous>=0){
			return input_list[index_previous]
		}else{
			return null
		}
	}
}

//function that calculate the current segment absolute pulse and fraction given a compas time coordinate
function RE_calcolate_segment_time_from_compas_values(pulse_m,fraction_m,compas_number,segment_index,voice_id){
	//find starting time of the segment
	if(compas_number==null){
		console.log("cannot determinate previous compas number")
		return [null,null]
	}
	let data = DATA_get_current_state_point(true)
	let voice_data_mod = data.voice_data.find(item=>{
		return item.voice_id==voice_id
	})
	let all_segment_data= voice_data_mod.data.segment_data
	let segment_starting_pulse = 0
	all_segment_data.find((segment_data,index)=>{
		if(index==segment_index){
			return true
		}else{
			segment_starting_pulse+=segment_data.time.slice(-1)[0].P
		}
	})
	//calculating pulse position
	//getting current compas Lc
	let [compas_start_at, current_Lc] = BNAV_compas_number_to_Properties(compas_number)
	if(compas_start_at==null){
		//compas doesnt exist
		return [null,null]
	}
	//verify if current_Lc!=-1 and pulse_m tyed is < Lc
	if(current_Lc<2 || pulse_m>=current_Lc){
		return [null,null]
	} else {
		if(compas_start_at+pulse_m-segment_starting_pulse>0 && fraction_m>=0){
			return [compas_start_at+pulse_m-segment_starting_pulse,fraction_m]
		}else{
			return [null,null]
		}
	}
}


