function PMC_write_C_line_values() {
	let C_line = document.querySelector(".App_PMC_C")
	let C_line_string=""
	let compasArr= DATA_get_current_state_point(false).compas
	let compasArrWithReps = []
	let counterCompasReps = 0
	for(let i=0;i<compasArr.length;i++){
		compasArr[i].compas_values[0] = counterCompasReps
		compasArrWithReps.push(compasArr[i])
		counterCompasReps+=compasArr[i].compas_values[1]
		if(compasArr[i].compas_values[2]!=1){
			for(j=1;j<compasArr[i].compas_values[2];j++){
				let tempCompas = {compas_values:[counterCompasReps,compasArr[i].compas_values[1],compasArr[i].compas_values[2],compasArr[i].compas_values[3],compasArr[i].compas_values[4],true]}
				compasArrWithReps.push(tempCompas)
				counterCompasReps+=tempCompas.compas_values[1]
			}
		}
	}
	compasArrWithReps.forEach((element, index) => {
		let C_button_left = PMC_zoom_x*nuzic_block*element.compas_values[0]+nuzic_block/2
		let inner_string=""
		let display_C_range=""
		if(PMC_zoom_x<1)display_C_range='style="display:none;"'
		if(element.compas_values.length==5){
			inner_string = `<div class="App_PMC_C_inp_box">
								<div class="App_PMC_C_value">
									<p class="App_PMC_sub_C_value">${index}</p>
								</div>
								<div class="App_PMC_C_range" ${display_C_range}>
									<p class="App_PMC_sub_C_range">${element.compas_values[1]}</p>
								</div>
							</div>`
		}else {
			inner_string = `<div class="App_PMC_C_inp_box">
								<div class="App_PMC_C_value">
									<p class="App_PMC_sub_C_value">${index}</p>
								</div>
							</div>`
		}
		C_line_string+=`<button class="App_PMC_C_button" style="left: ${C_button_left}px;" position="${element.compas_values[0]}" number="${index}" onclick="BNAV_show_compas_input(this)">
							${inner_string}
						</button>`
	})
	let voice_data = DATA_get_selected_voice_data()
	let maxLength = PMC_x_length(voice_data)-0.5
	C_line_string+=`<div class="PMC_scaleCompas_lastEl" style="left: 0px; width: calc(var(--PMC_x_block) * ${maxLength} + var(--RE_block))"></div>`
	C_line.innerHTML = C_line_string
}

function PMC_write_S_line_values() {
	let S_line = document.querySelector(".App_PMC_S")
	let S_line_string=""
	let data= DATA_get_current_state_point(false)
	let scale_sequence = data.scale.scale_sequence
	let scale_pulse = 0
	let display_S_range=""
	if(PMC_zoom_x<1)display_S_range='style="display:none;"'
	scale_sequence.forEach((element, index) => {
		let S_obj_left= PMC_zoom_x*nuzic_block* scale_pulse +nuzic_block/2
		let S_obj_width= PMC_zoom_x*nuzic_block* element.duration
		S_line_string+=`<div class="App_PMC_S_obj" style="left:${S_obj_left}px;width:${S_obj_width}px" onmousemove="PMC_break_scale_range_calculate_position(this,event)">
							<div class="App_PMC_S_scale" style="display: inline;" onmouseenter="PMC_exit_break_scale_range(null,this.parentNode)" onmousemove="event.stopPropagation()">
								<div class="App_PMC_S_value" value='${JSON.stringify(element)}' onclick="PMC_show_scale_circle(this);event.stopPropagation()">
									<p class="App_PMC_sub_S_value">${element.acronym}${element.rotation}.${element.starting_note}</p>
								</div>
								<div class="App_PMC_S_duration" onclick="PMC_show_scale_input(this)">
									<p class="App_PMC_sub_S_duration" ${display_S_range}>${element.duration}</p>
								</div>
							</div>
							<div class="App_PMC_break_scale_range hidden" onclick="PMC_break_scale_range(this)" onmouseleave="PMC_exit_break_scale_range(this)">
								<p>+</p>
								<p class="App_PMC_break_scale_info"></p>
							</div>
						</div>`
		scale_pulse += element.duration
	})
	//ghost scale
	let last_compas= data.compas[data.compas.length-1]
	let end_last_compas_abs_pulse = last_compas.compas_values[0]+last_compas.compas_values[1]*last_compas.compas_values[2]
	let max_abs_pulse=(Li>end_last_compas_abs_pulse)?Li:end_last_compas_abs_pulse
	if(scale_sequence.length!=0 && scale_pulse<max_abs_pulse){
		let duration=max_abs_pulse-scale_pulse
		let element=scale_sequence.slice(-1)[0]
		let S_obj_left= PMC_zoom_x*nuzic_block* scale_pulse +nuzic_block/2
		let S_obj_width= PMC_zoom_x*nuzic_block* duration
		let value={"acronym":element.acronym,"rotation":element.rotation,"starting_note":element.starting_note,"duration":duration}
		S_line_string+=`<div class="App_PMC_S_obj ghost" style="left:${S_obj_left}px;width:${S_obj_width}px;opacity: 50%;" onmousemove="PMC_break_scale_range_calculate_position(this,event);event.stopPropagation()">
							<div class="App_PMC_S_scale" style="display: inline;" onmouseenter="PMC_exit_break_scale_range(null,this.parentNode)" onmousemove="event.stopPropagation()">
								<div class="App_PMC_S_value" value='${JSON.stringify(value)}' onclick="BNAV_add_last_scale_sequence()">
									<p class="App_PMC_sub_S_value">${element.acronym}${element.rotation}.${element.starting_note}</p>
								</div>
								<div class="App_PMC_S_duration">
									<p class="App_PMC_sub_S_duration" ${display_S_range}>${duration}</p>
								</div>
							</div>
							<div class="App_PMC_break_scale_range hidden" onclick="PMC_break_scale_range(this)" onmouseleave="PMC_exit_break_scale_range(this)">
								<p>+</p>
								<p class="App_PMC_break_scale_info"></p>
							</div>
						</div>`
	}else if(scale_sequence.length==0){
		let S_obj_left = nuzic_block/2
		let S_obj_width = PMC_zoom_x*nuzic_block* max_abs_pulse
		let value={"acronym":null,"rotation":null,"starting_note":null,"duration":max_abs_pulse}
		S_line_string+=`<div class="App_PMC_S_obj ghost" style="left:${S_obj_left}px;width:${S_obj_width}px;opacity: 50%;" onmousemove="PMC_break_scale_range_calculate_position(this,event)">
							<div class="App_PMC_S_scale" style="display: inline;" onmouseenter="PMC_exit_break_scale_range(null,this.parentNode)" onmousemove="event.stopPropagation()">
								<div class="App_PMC_S_value" onclick="BNAV_add_first_scale_sequence()" value='${JSON.stringify(value)}'>
									<img class="App_PMC_sub_S_value" src="./Icons/add.svg">
								</div>
								<div class="App_PMC_S_duration">
									<p class="App_PMC_sub_S_duration">${max_abs_pulse}</p>
								</div>
							</div>
							<div class="App_PMC_break_scale_range hidden" onclick="PMC_break_scale_range(this)" onmouseleave="PMC_exit_break_scale_range(this)">
								<p>+</p>
								<p class="App_PMC_break_scale_info"></p>
							</div>
						</div>`
	}
	let voice_data = DATA_get_selected_voice_data()
	let maxLength = PMC_x_length(voice_data)-0.5
	S_line_string+=`<div class="PMC_scaleCompas_lastEl" style="left: 0px; width: calc(var(--PMC_x_block) * ${maxLength} + var(--RE_block))"></div>`
	S_line.innerHTML = S_line_string
}

function PMC_show_scale_circle(S_value){
	APP_hide_scale_circle_elements()
	//check scrollbar position (sticky)
	let scale_data = JSON.parse(S_value.getAttribute("value"))
	let parent = S_value.closest(".App_PMC_S_obj")
	let [scale_index,]=RE_element_index(parent,".App_PMC_S_obj")
	let pink_line=document.querySelector(".App_PMC_pink_scale_range_background")
	//add circle container //trigger mouse leave
	pink_line.innerHTML+=BNAV_calculate_S_circle_container(scale_data,scale_index)
	//make sure start is visible
	let App_PMC=document.querySelector(".App_PMC")
	let left_position=parent.getBoundingClientRect().left-App_PMC.getBoundingClientRect().left
	if(left_position<(nuzic_block*7)){
		//move scrollbar
		PMC_show_scale_number_S_line(scale_index)
		//setTimeout(function(){PMC_show_pink_scale_line_background(parent,true)},10)
		setTimeout(function(){PMC_show_scale_circle(S_value)},10)
		return
	}
	//add pink line / break scale range
	PMC_show_pink_scale_line_background(parent,true)
	BNAV_find_and_select_scale_composition(scale_data.acronym,scale_data.rotation,scale_data.starting_note)
	BNAV_select_scale_number_sequence_list(scale_index,false)
}

function PMC_hide_scale_circle_elements(){
	let pink_line=document.querySelector(".App_PMC_pink_scale_range_background")
	pink_line.classList.add("hidden")
	PMC_delete_S_circle_container()
}

function PMC_delete_S_circle_container(){
	let pink_line=document.querySelector(".App_PMC_pink_scale_range_background")
	let circle_container= [...pink_line.querySelectorAll(".App_BNav_S_circle_container")]
	if(circle_container!=null){
		circle_container.forEach(item=>{item.remove()})
	}
}

function PMC_show_scale_input(S_button=null){
	//use scale != null == button pressed to focus on correct scale
	//APP_stop()
	let selected = -1
	let parent = S_button.closest(".App_PMC_S_obj")
	if(S_button==null){
		//select first scale
		selected=0
	}else{
		let [elementIndex,list]=RE_element_index(parent,".App_PMC_S_obj")
		selected=elementIndex
	}
	//verify if found something
	let scale_sequence= DATA_get_scale_sequence()
	if(selected<0 && selected>=scale_sequence.length)selected=0
	let ES_button = document.getElementById("App_BNav_tab_ES")
	ES_button.click()
	BNAV_select_scale_number_sequence_list(selected)
}

function PMC_show_scale_number_S_line(index){
	let scale_line = document.querySelector('.App_PMC_S')
	let scale_obj_list = [...scale_line.querySelectorAll(".App_PMC_S_obj")]
	//find correct div and move horizontal bar there!
	let scale_obj_target = scale_obj_list[index]
	let str = scale_obj_target.style.left
	let n_pixels_left=parseInt(str.substring(0, str.length - 2))
	//var tot_buttons = scale_obj_list.length
	let value = n_pixels_left-nuzic_block
	PMC_checkScroll_H(value)
}

function PMC_break_scale_range(object){
	let pulse=parseInt(object.getAttribute("value"))
	let result=DATA_insert_scale_at_pulse(pulse)
	if(!result.success){
		//open scale circle for extra input
		//avoid pink line disappearing
		event.stopPropagation()
		object.classList.add("insert_input")
		let pink_line=document.querySelector(".App_PMC_pink_scale_range_background")
		pink_line.style.zIndex=100
		//add circle container //trigger mouse leave
		pink_line.innerHTML+=BNAV_calculate_S_circle_container_insert(result)
		setTimeout(function(){object.classList.remove("insert_input");object.classList.add("hidden")},10)
	}
}

function PMC_exit_break_scale_range(break_scale_range,object){
	if(break_scale_range!=null){
		if(break_scale_range.classList.contains("insert_input")){
			return
		}
	}
	if(break_scale_range==null)break_scale_range=object.querySelector(".App_PMC_break_scale_range")
	if(!break_scale_range.classList.contains("hidden")){
		break_scale_range.classList.add("hidden")
		PMC_hide_scale_circle_elements()
	}
}

function PMC_break_scale_range_calculate_position(object,event){
	//PMC_zoom_x*nuzic_block* scale_pulse +nuzic_block/2
	if(PMC_scale_circle_active())return
	let box_position=Math.round(event.layerX/(PMC_zoom_x*nuzic_block))
	let break_obj=object.querySelector(".App_PMC_break_scale_range")
	//App_RE_pink_scale_range_background
	if(box_position==0)return
	if(!event.target.classList.contains("App_PMC_S_obj"))return
	let scale_position=Math.round((object.offsetLeft-nuzic_block/2)/(PMC_zoom_x*nuzic_block))
	let pulse=box_position+scale_position
	let parent_duration=JSON.parse(object.querySelector(".App_PMC_S_value").getAttribute("value")).duration
	let duration=parent_duration-box_position
	if(duration<=0)return
	break_obj.classList.remove("hidden")
	break_obj.style.left=(box_position*(PMC_zoom_x*nuzic_block)-(2*nuzic_block))+"px"
	break_obj.setAttribute("value",pulse)
	break_obj.querySelector(".App_PMC_break_scale_info").innerHTML=duration
	PMC_show_pink_scale_line_background(break_obj)
}

function PMC_scale_circle_active(){
	let pink_line=document.querySelector(".App_PMC_pink_scale_range_background")
	let circle_container= pink_line.querySelector(".App_BNav_S_circle_container")
	return circle_container!=null
}

function PMC_show_pink_scale_line_background(obj,onTop=false){
	let App_PMC=document.querySelector(".App_PMC")
	let pink_line=App_PMC.querySelector(".App_PMC_pink_scale_range_background")
	let left_position=obj.getBoundingClientRect().left-App_PMC.getBoundingClientRect().left
	pink_line.style=""
	if(left_position<(nuzic_block*7))return false
	let global_zoom = Number(document.querySelector('.App_SNav_select_zoom').value)
	pink_line.style.left=(left_position/global_zoom)+"px"
	//pink_line.style.zIndex=10
	if(onTop)pink_line.style.zIndex=100
	pink_line.classList.remove("hidden")
}




