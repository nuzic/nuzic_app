const database_version = 12.0
const preferences_version = 1.0

function DATA_initialize_state_point(data){
	current_state_point_index = 0
	min_state_point_index=current_state_point_index
	max_state_point_index=current_state_point_index
	state_point_array[current_state_point_index]=data
}

//API web
function DATA_web_load_composition(data,filename){
	state_point_array=[]
	DATA_initialize_state_point(data)
	//DATA_insert_new_state_point(datatest)
	DATA_load_state_point_data()
	//DATA_load_state_point_data(true)//same
	// Set current composition name
	APP_set_composition_name(filename)
}

function DATA_insert_new_state_point(data_in){
	//control version and save
	let data = DATA_control_database_version(data_in)
	current_state_point_index++
	if(min_state_point_index!=0)min_state_point_index=current_state_point_index+1
	if(min_state_point_index==state_point_array.length)min_state_point_index=0
	if(current_state_point_index==state_point_array.length) {
		current_state_point_index=0
		min_state_point_index=1
	}
	max_state_point_index=current_state_point_index
	state_point_array[current_state_point_index]=data
	flag_DATA_updated.PMC=false
	flag_DATA_updated.RE=false
}

function DATA_point_to_previous_state_point(){
	if(current_state_point_index==min_state_point_index){
		console.log("Ended number of saved sates")
		return false
	} else {
		let previous_state_point_index = current_state_point_index -1
		if (previous_state_point_index<0)previous_state_point_index=state_point_array.length-1
		current_state_point_index=previous_state_point_index
		return true
	}
}

function DATA_get_current_state_point(integrate_current_preferences=true){
	//return A COPY of current index state point
	let data_copy=JSON.parse(JSON.stringify(state_point_array[current_state_point_index]))
	if(integrate_current_preferences && is_App){
		//PMC POSITION , ZOOM, AND VISIBILITY
		data_copy.global_variables.app_containers=DATA_read_app_containers_button_states()
		data_copy.global_variables.PMC=DATA_read_PMC_global_variables()
		if(document.querySelector('.App_RE_main_scrollbar_H')!=null)RE_global_variables.scroll_x=document.querySelector('.App_RE_main_scrollbar_H').scrollLeft
		if(document.querySelector('.App_RE_voice_matrix')!=null)RE_global_variables.scroll_y=document.querySelector('.App_RE_voice_matrix').scrollTop
		data_copy.global_variables.RE=RE_global_variables
		data_copy.global_variables.A=A_global_variables
		//NOTEBOOK
		data_copy.notebook = document.getElementById('App_notebook').innerHTML
		//count in
		data_copy.global_variables.count_in=TNAV_read_countIn()
	}
	return data_copy
}

function DATA_point_to_next_state_point(){
	if(current_state_point_index==max_state_point_index){
		console.log("No more next saved sates")
		return false
	} else {
		let next_state_point_index = current_state_point_index +1
		if (next_state_point_index==state_point_array.length)next_state_point_index=0
		current_state_point_index=next_state_point_index
		return true
	}
}

function DATA_get_selected_voice_data(){
	let data = DATA_get_current_state_point(false)
	// find current voice database v6
	let selected_voice_data= data.voice_data.find(item=>{
		return item.selected
	})
	return selected_voice_data
}

function DATA_get_PMC_visible_voice_data(data=null){
	if(data==null) data = DATA_get_current_state_point(false)
	// find current voice database v6
	let visible_voices_data = data.voice_data.filter(item=>{
		//return !item.selected and visible
		return item.PMC_visible && !item.selected
	})
	//return JSON.parse(JSON.stringify(data.voice_data[0]))
	return visible_voices_data
}

function DATA_control_database_version(data_in){
	//crerate a copy of the data
	let data = JSON.parse(JSON.stringify(data_in))
	//from lab
	if(data.notebook=="LAB_melody_simple" ||
		data.notebook=="LAB_rhythm_simple"
	)	data.notebook=""
	if (data.database_version==database_version){
		let error=false
		data.voice_data.forEach((voice,v_index)=>{
			voice.data.segment_data.forEach((segment,s_index)=>{
				if(segment.time.length!=segment.sound.length){
					error=true
					console.error(`voice ${voice.name} index ${v_index},segment ${s_index} sound length error`)
				}
			})
		})
		if(error){
			alert("SORRY, something went wrong!\n\nDATABASE ERROR, please report BUG describing the operation done just before this operation")
			console.log("DATABASE ERROR")
			console.log(data)
			//return data
			if(typeof Save_server_BUG=='function'){
				let bugTitle = document.getElementById('App_SNav_bug_title')
				let bugDescription = document.getElementById('App_SNav_bug_description')
				bugTitle.innerHTML!='DATA ERROR'
				bugDescription.innerHTML!='DATA_control_database_version segment.time.lenght != segment.sound.length'
				Save_server_BUG()
			}
			return DATA_get_current_state_point(true)
		}else{
			//important
			return data
		}
	}
	if (data.database_version==null){
		console.error("no database_version found")
		data.database_version=4
	}
	//change of version
	if (data.database_version<3){
		console.info("Old Database , changing compas data")
		//changed .compas
		//from array of array[3]
		//to  array of .compas_values	{int , int, int , [int ,int ],int}
		//								 start, Lc , rep , accents
		let mom = data.compas
		//transform into new version
		data.compas = 0
		let index=0
		mom.forEach(item => {
			//var data = {global_variables:[], compas:[], sound_quantization:[], voice_data:[]}
			let compas_values = {compas_values:[parseInt(item[0]),parseInt(item[1]),parseInt(item[2]),[0],0]}//XXX no rotation???
			mom[index]=compas_values
			//compas_value=item
			//datatest.compas[index].compas_value[3]=[0]
			index++
		})
		data.compas = mom
	}

	//change of version
	if (data.database_version<4){
		console.info("Old Database , changing scale list data")
		//added scale.scale_list and scale.scale_sequence
		let scale = JSON.parse(JSON.stringify({"idea_scale_list": {"scale_list":[],"TET_list":[12]},"scale_sequence":[]}))
		//old version "scale":{"idea_scale_list":{"scale_list":[],"TET_list":[12]},"scale_sequence":[]},
		data.scale = scale
	}

	//change of version
	//voice number / voice name, Minimap/PMC/RE shown
	//change of version
	if (data.database_version<5){
		console.info("Old Database, changing data")
		data.global_variables.app_containers= {"RE":true, "PMC":false,"MMap":true, "ES_tab": false, "ET_tab":false, "Key_tab":true, "tabs_opened":false}
		let voice_index= 0
		data.voice_data.forEach(item=>{
			item.voice_index=voice_index
			voice_index++
		})
		//verify midi_data note
		//bug midi_data.note to note_freq
		data.voice_data.forEach(item=>{
			if(item.data.midi_data.note!=null){
				item.data.midi_data.note_freq = item.data.midi_data.note
				delete item.data.midi_data.note
			}
		})
	}
	if (data.database_version<6){
		//count in
		//PMC
		//zoom(s)
		//scrollbar position
		//axis type (Na,Pa...)
		//RE_preview
		//elements shown (Na iS O iT in PMC and type)
		//PMC module button states
		//VOICES
		//voice selected
		//voice PMC visible
		//voice color
		//segment_name
		//string to int
		//volume
		//instrument
		//metro
		//fraction
		//time
		//note s-1  e-2  L-3!!!  ·-4!!!
		//scale duration
		console.info("Old Database, changing data")
		//count in
		data.global_variables.count_in = 0
		//PMC
		data.global_variables.PMC={}
		//zoom(s)
		data.global_variables.PMC.zoom_x=1
		data.global_variables.PMC.zoom_y=1
		//scrollbar position
		data.global_variables.PMC.scroll_x=0//pixel
		data.global_variables.PMC.scroll_y=50//percent
		//RE_preview
		data.global_variables.PMC.vsb_RE=false
		//elements shown (Na iS O iT in PMC and type)
		data.global_variables.PMC.vsb_N={"show":true, "type":"Na"}
		data.global_variables.PMC.vsb_iN={"show":true, "type":"iNa"}
		data.global_variables.PMC.vsb_T={"show":true, "type":"Ta"}
		data.global_variables.PMC.vsb_iTa={"show":true, "type":"iTa"}
		//PMC module button states NEW
		//console.log(data.global_variables)
		data.global_variables.module_button_states={"ES_PMC":true,"CET_PMC":true,"ES_RE":true,"CET_RE":true}
		//VOICES
		let index=0
		data.voice_data.forEach(voice=>{
			//voice selected
			if(index==0){
				voice.selected=true
			}else{
				voice.selected=false
			}
			//voice PMC visible
			voice.PMC_visible=true
			//voice color
			voice.color="nuzic_green"
			//str to int
			voice.volume=parseInt(voice.volume)
			voice.instrument=parseInt(voice.instrument)
			voice.metro=parseInt(voice.metro)
			index++
			voice.data.segment_data.forEach(segment=>{
				segment.segment_name=""
				//str to int
				var index_element=0
				segment.note.forEach(note=>{
					if(note=="s"){
						segment.note[index_element]=-1
					}else if(note=="e"){
						segment.note[index_element]=-2
					}else if(note=="L"){
						segment.note[index_element]=-3
					}else if(note==end_range){
						segment.note[index_element]=-4
					}else{
						segment.note[index_element]=parseInt(note)
					}
					index_element++
				})
				index_element=0
				let current_pulse=0
				segment.time.forEach(time=>{
					let split = time.split('.')
					let P=Math.floor(split[0])
					let F=Math.floor(split[1])
					if(split[1]==null)F=0
					if(P==0)P=current_pulse
					current_pulse=P
					segment.time[index_element]={P,F}
					index_element++
				})
				index_element=0
				segment.fraction.forEach(fraction=>{
					let split = fraction[0].split('/')
					let N=Math.floor(split[0])
					let D=Math.floor(split[1])
					segment.fraction[index_element]={"N":N,"D":D,"start":parseInt(fraction[1]),"stop":parseInt(fraction[2])}
					index_element++
				})
			})
			//data.midi_data correcting bug truncate NO NEED , done in next check
			/*
			var old_PPM = PPM
			PPM=data.global_variables.PPM
			voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
			PPM=old_PPM
			*/
		})
		//SCALE duration is a number
		data.scale.scale_sequence.forEach(sequence=>{
			sequence.duration=parseInt(sequence.duration)
		})
	}
	//add some other validations
	if (data.database_version<7){
		//ADDING//RENAMING LINES
		// PA
		// PS
		// ALFABET NOTATION
		//VOICE VISIBLE== VOICE RE_visible
		//no possible change on C and S line visibility
		console.info("Old Database version " +data.database_version+ " , changing data")
		data.voice_data.forEach(voice=>{
			voice.RE_visible=voice.visible
			delete voice.visible
		})
		//data.global_variables.line_button_states -> data.global_variables.RE
		//add new key
		data.global_variables.RE = {}
		data.global_variables.RE.scroll_x=0
		data.global_variables.RE.scroll_y=0
		data.global_variables.RE.Na = data.global_variables.line_button_states.Na
		data.global_variables.RE.Nm = data.global_variables.line_button_states.Nm
		data.global_variables.RE.Ngm = data.global_variables.line_button_states.Ngm
		data.global_variables.RE.Nabc = false
		data.global_variables.RE.iNa = data.global_variables.line_button_states.iNa
		data.global_variables.RE.iNm = data.global_variables.line_button_states.iNm
		data.global_variables.RE.iNgm = data.global_variables.line_button_states.iNgm
		data.global_variables.RE.Ta = false
		data.global_variables.RE.Tf = data.global_variables.line_button_states.Tf
		data.global_variables.RE.Ts = data.global_variables.line_button_states.Ta
		data.global_variables.RE.Tm = data.global_variables.line_button_states.Tm
		data.global_variables.RE.iT = data.global_variables.line_button_states.iTa
		data.global_variables.PMC.vsb_Nabc = true
		data.global_variables.PMC.vsb_iT=data.global_variables.line_button_states.iTa
		delete data.global_variables.line_button_states.iTa
		delete data.global_variables.line_button_states
		//no module button states anymore
		//detete data.global_variables.module_button_states
		data.global_variables.module_button_states.ES_RE=true
		data.global_variables.module_button_states.CET_RE=true
		data.global_variables.module_button_states.ES_PMC=true
		data.global_variables.module_button_states.CET_PMC=true
		//delete data.global_variables.module_button_states
		if(data.global_variables.hasOwnProperty("iA_button_states")){
			data.global_variables.RE.iAa=data.global_variables.iA_button_states.iAa
			data.global_variables.RE.iAgm=data.global_variables.iA_button_states.iAgm
			data.global_variables.RE.iAm=data.global_variables.iA_button_states.iAm
			delete data.global_variables.iA_button_states
		}else{
			data.global_variables.RE.iAa=false
			data.global_variables.RE.iAgm=false
			data.global_variables.RE.iAm=true
		}
		//BUG, control no null values in data compas
		data.compas.forEach((compas,index,array)=>{
			if (compas.compas_values[4]==null){
				array[index].compas_values[4]=0
				return
			}
		})
		data.voice_data.forEach(voice=>{
			voice.data.segment_data.forEach(segment=>{
				//every note is # or +
				segment.diesis = segment.note.map(index=>{
					return true
				})
			})
			voice.neopulse={N:voice.neopulse[0],D:voice.neopulse[1]}
			//new truncate
			//data.midi_data correcting bug truncate
			//data.midi_data correcting bug truncate NO NEED , done in next check
			// let old_PPM = PPM
			// PPM=data.global_variables.PPM
			// voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
			// PPM=old_PPM
		})
	}

	if (data.database_version<8){
		//control y CORRECTION DIESIS //for database ==7
		console.info("Old Database version " +data.database_version+ " , changing data")
		let diesis_correction=false
		data.voice_data.forEach(voice=>{
			voice.data.segment_data.forEach(segment=>{
				//every note is # or +
				if(segment.diesis==undefined){
					segment.diesis=[true,true]
					diesis_correction=true
				}
				let d_length = segment.diesis.length
				let t_length = segment.time.length
				if(d_length>t_length){
					for (let i = 0; i < d_length-t_length; i++) {
						segment.diesis.pop()
					}
					diesis_correction=true
				}
				if(d_length<t_length){
					for (let i = 0; i < t_length-d_length; i++) {
						segment.diesis.push(true)
					}
					diesis_correction=true
				}
			})
		})
		if(diesis_correction){
			console.error("DIESIS NOT SAME LENGTH OF TIME - NOTE - DIESIS")
			APP_error_popup("ERROR GENERATING DATABASE: note diesis array\n\nError patched")
		}
		//control BUG scale sequence not strings
		data.scale.scale_sequence.forEach(item=>{
			item.duration=parseInt(item.duration)
			item.starting_note=parseInt(item.starting_note)
			item.rotation=parseInt(item.rotation)
		})
		//add CALC tab
		data.global_variables.app_containers.Calc_tab=false
		//add sound definition for pulse
		//delete key polipulse
		data.voice_data.forEach(voice=>{
			voice.e_sound=22
			delete voice.polipulse
		})
		//BUG delete voice from RE create multiple selection
		//verify single voice selected
		let selection= data.voice_data.filter(voice=>{return voice.selected})
		if(selection.length!=1){
			//clear selection and select first voice
			data.voice_data.forEach(voice=>{voice.selected=false})
			data.voice_data[0].selected=true
		}
	}

	if (data.database_version<9){
		//control diesis note and time segments are ok!!
		console.info("Old Database, changing data")
		let error=false
		data.voice_data.forEach((voice,v_index)=>{
			voice.data.segment_data.forEach((segment,s_index)=>{
				if(segment.time.length!=segment.diesis.length){
					error=true
					console.error(`voice ${voice.name} index ${v_index},segment ${s_index} diesis length error`)
				}
				if(segment.time.length!=segment.note.length){
					error=true
					console.error(`voice ${voice.name} index ${v_index},segment ${s_index} note length error`)
				}
			})
		})

		if(error){
			alert("SORRY, something went wrong!\n\nDATABASE ERROR, please report BUG describing the operation done just before this operation")
			console.log("DATABASE ERROR")
			console.log(data)
			//return data
			if(typeof Save_server_BUG=='function'){
				let bugTitle = document.getElementById('App_SNav_bug_title')
				let bugDescription = document.getElementById('App_SNav_bug_description')
				bugTitle.innerHTML!='DATA ERROR'
				bugDescription.innerHTML!='DATA_control_database_version v9 conversion segment.time.lenght != segment.sound.length'
				Save_server_BUG()
			}
			return DATA_get_current_state_point(true)
		}else{
			//return data
		}
		//introduction polyphonic voices
		//segment{time,sound:[{note,diesis,A_pos,A_neg}...]}
		//global_variables visualization Aa Am Ag, mute_A
		data.global_variables.A={show_note:true,show:false,type:"Am",mute:false}
		mute_A=data.global_variables.A.mute
		//delete old iA entry
		delete data.global_variables.RE.iAa
		delete data.global_variables.RE.iAgm
		delete data.global_variables.RE.iAm
		data.voice_data.forEach((voice,v_index)=>{
			//change when add A line
			voice.A=false
			voice.data.segment_data.forEach((segment,s_index)=>{
				segment.sound=[]
				segment.note.forEach((note,index)=>{segment.sound.push({note,diesis:segment.diesis[index],A_pos:[],A_neg:[]})})
				delete segment.note
				delete segment.diesis
			})
			// let old_PPM = PPM
			// let old_mute_A = mute_A
			// PPM=data.global_variables.PPM
			voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse,data.global_variables.PPM,data.global_variables.TET,mute_A)
			// PPM=old_PPM
			// mute_A=old_mute_A
		})
	}

	if (data.database_version<10){
		console.info("Old Database, changing data")
		let idea_scale_list=JSON.parse(JSON.stringify(data.scale.idea_scale_list.scale_list))
		//change of version
		// name,iS,acronym,module,family,global
		// family {rot , name}
		// letter,start,rotation,duration
		// purge idea scale list from TET != composition TET
		// NO tags, NO info, no TET
		// family {rot , name}
		let new_idea_scale_list = idea_scale_list.filter(item=>{
			//scale names - old
			item.name=item.name+"_OLD"
			item.acronym=item.letter
			item.global=false
			delete item.letter
			delete item.info
			delete item.tags
			item.family=[]
			return item.TET == data.global_variables.TET
		})
		new_idea_scale_list.forEach(item=>{
			delete item.TET
		})

		//data.scale.idea_scale_list=new_idea_scale_list
		//try to merge into families
		let merged_idea_scale_list=[]
		let scale_sequence=JSON.parse(JSON.stringify(data.scale.scale_sequence))
		let merged_scale_sequence=[]
		let current_TET
		if(is_App){
			//no need
			new_idea_scale_list.forEach(item=>{
				let target=DATA_find_global_scale_from_iS_list(item.iS)
				if(target!=null){
					//this scale is a rotation/translation of a global scale
					//substitution
					let already_sub=_find_used_scale_from_iS_list(target.iS,merged_idea_scale_list)
					if(already_sub!=null){
						//already_sub ==> target
						let d_rot = _determine_iS_seq_rot_sn(item.iS,already_sub.iS)
						_substitution_scale_sequence(scale_sequence,item.acronym,already_sub.acronym,d_rot,already_sub.module)
					}else{
						//add
						let new_acronym=BNAV_generate_new_scale_acronym(merged_idea_scale_list)
						let family=[]
						target.family.forEach(fam=>{
							family.push({"name":fam.name[current_language],"rotation":fam.rotation})
						})
						let new_scale={"module":target.module,"acronym":new_acronym,"iS":target.iS,"name":target.name[current_language],"family":family,"global":true}
						merged_idea_scale_list.push(new_scale)
						let d_rot = _determine_iS_seq_rot_sn(item.iS,target.iS)
						_substitution_scale_sequence(scale_sequence,item.acronym,new_acronym,d_rot,target.module)
					}
					//merged_idea_scale_list.
				}else{
					//new scale
					let new_acronym=BNAV_generate_new_scale_acronym(merged_idea_scale_list)
					let new_scale={"module":item.module,"acronym":new_acronym,"iS":item.iS,"name":item.name,"family":item.family,"global":false}
					merged_idea_scale_list.push(new_scale)
					//d_rot==0
					_substitution_scale_sequence(scale_sequence,item.acronym,new_acronym,0,new_scale.module)
				}
			})
			_clear_scale_sequence()
			//data.scale.idea_scale_list=new_idea_scale_list
			data.scale.idea_scale_list=merged_idea_scale_list
			data.scale.scale_sequence=scale_sequence
		}
		function _find_used_scale_from_iS_list(iS_list,idea_scale_list){
			let module=iS_list.length
			let iS_list_string=JSON.stringify(iS_list)
			//exactly similar
			let target=idea_scale_list.find(item=>{
				if(item.module==module){
					return JSON.stringify(item.iS)==iS_list_string
				}
			})
			if(target==null)return null
			return JSON.parse(JSON.stringify(target))
		}
		function _determine_iS_seq_rot_sn(iS,iS_base){
			let iS_list_string=JSON.stringify(iS_base)
			//verify rotations too
			let iS_copy=JSON.parse(JSON.stringify(iS))
			let module=iS.length
			for(let i=0;i<module;i++){
				if(JSON.stringify(iS_copy)==iS_list_string){
					let rot=(module-i)%module
					return rot
				}
				iS_copy.push(iS_copy.shift())
			}
			/*
			let rot=0
			let sn=0
			for(let i=0;i<module;i++){
				if(JSON.stringify(iS_copy)==iS_list_string){
					rot=(module-i)%module
				}
				iS_copy.push(iS_copy.shift())
			}
			return rot
			*/
		}
		function _substitution_scale_sequence(scale_sequence,original_acronym,new_acronym,d_rot,module){
			scale_sequence.forEach(item=>{
				if(item.acronym==original_acronym){
					item.acronym=new_acronym+"_provv"
					item.rotation=(item.rotation+d_rot)%module
				}
			})
		}
		function _clear_scale_sequence(){
			scale_sequence.forEach(item=>{
				item.acronym=item.acronym[0]
			})
		}
	}

	if (data.database_version<11){
		//instruments volumes and XY position
		data.voice_data.forEach(voice=>{
			//change when add A line
			let new_instrument_index=0//grand piano
			switch (voice.instrument) {
			case 1:
				//drums
				new_instrument_index=128
			break;
			case 2:
				//synth
				new_instrument_index=89//38
			break;
			case 3:
				//synth
				new_instrument_index=90//50
			break;
			case 4:
				//synth
				new_instrument_index=93//62
			break;
			case 5:
				//electric piano
				new_instrument_index=2
			break;
			}
			voice.instrument=new_instrument_index
			voice.voice_id=voice.voice_index
			voice.midiCh=null
			delete voice.voice_index
			//volume
			if(voice.volume==0){
				//mute
				voice.mute=true
			}else{
				//mute
				voice.mute=false
				voice.volume=voice.volume*30
			}
			//voice.volume=80 //default value
			voice.pan=0.5
			voice.fx=1
			voice.lpf=0.5
			voice.vel=V_velocity_default
			//change when add A line
			if(voice.A_neg!=undefined){
				if(voice.A_neg)voice.A=true
				delete voice.A_neg
			}
		})
		//volume
		data.global_variables.effects={}
		data.global_variables.effects.reverb=0
		data.global_variables.app_containers.V_tab=true //XXX false
		data.global_variables.app_containers.tabs_opened=true //XXX TO BE eliminated
		data.global_variables.app_containers.ES_tab=false //XXX TO BE eliminated
		//xy position XXX XXX XXX
	}

	if (data.database_version<12){
		//percentage
		data.global_variables.PMC.scroll_y=50
	}

	if (data.database_version>database_version){
		console.error("incompatible database")
		data="error"
	}
	data.database_version=database_version
	return data
}

function DATA_segment_data_to_midi_data(segment_data_in,neopulse,current_PPM=PPM,current_TET=TET,current_mute_A=mute_A,count_in=0){
	let D_NP = neopulse.D
	let N_NP = neopulse.N
	let segment_data = JSON.parse(JSON.stringify(segment_data_in))//FK COPY!!!
	let time = []
	let frequency = []
	let note = []
	let nzc_note_list=[]
	let A_pos_list=[]
	let A_neg_list=[]
	let nzc_time_list = []
	let nzc_diesis_list = []
	let nzc_frac_list = []
	let error=false
	segment_data.forEach(segment=>{
		let e=false
		if(segment.note!=null){console.error("segment.note!=null");e=true}
		if(segment.diesis!=null){console.error("segment.diesis!=null");e=true}
		if(segment.time.length!=segment.sound.length){console.error("segment.sound.length not ok");console.log(segment);e=true}
		if(e){error=true}
		segment.sound.pop()
		segment.sound.forEach(item=>{
			nzc_note_list.push(item.note)
			A_pos_list.push(item.A_pos)
			A_neg_list.push(item.A_neg)
		})
		//create a list of times
		nzc_time_list = nzc_time_list.concat(segment.time)
		//create a list of fractioning ranges
		nzc_frac_list = nzc_frac_list.concat(segment.fraction)
		nzc_diesis_list = nzc_diesis_list.concat(segment.diesis)
	})
	if(error){
		alert("SORRY, something went wrong!\n\nDATABASE ERROR, please report BUG describing the operation done just before this operation")
		console.log("DATABASE ERROR on midi calc")
		console.log(segment_data_in)
		//return data
		if(typeof Save_server_BUG=='function'){
			let bugTitle = document.getElementById('App_SNav_bug_title')
			let bugDescription = document.getElementById('App_SNav_bug_description')
			bugTitle.innerHTML!='DATA ERROR'
			bugDescription.innerHTML!='DATA_midi sound error'
			Save_server_BUG()
		}
	}
	//convert array time in pulse/fraction data
	let last_pulse = 0
	let segment_start = 0
	let voice_time_PF_list = [[0,0]]
	nzc_time_list.forEach(value=> {
		if(value==="") {
			console.error("error database, pulse string conversion")
			alert("error database, pulse string conversion")
			voice_time_PF_list.push( [null,null])
		}
		let pulse = value.P
		let fraction = value.F
		if(pulse == 0 && fraction==0) {
			//start of a segment
			segment_start=last_pulse
			pulse=last_pulse
		}else{
			pulse +=segment_start
			voice_time_PF_list.push([pulse,fraction])
		}
		last_pulse = pulse
	})
	//convert array fraction ranges
	last_pulse = 0
	let voice_fraction_range_list = []
	nzc_frac_list.forEach(fraction=>{
		let delta = fraction.stop-fraction.start
		let start = last_pulse
		let end = start+delta
		last_pulse = end
		voice_fraction_range_list.push([[fraction.N,fraction.D],start,end,delta])
	})
	//calculate time
	voice_time_PF_list.forEach(item=>{
		//better truncate
		if (item[1]!=0){
			let fraction_data = voice_fraction_range_list.find((fraction)=>{
				return fraction[2]>item[0]
			})
			let time_sec = DATA_calculate_exact_time(count_in,item[0],item[1],fraction_data[0][0],fraction_data[0][1],N_NP,D_NP,current_PPM)
			time.push(time_sec)
		}else{
			let time_sec = DATA_calculate_exact_time(count_in,item[0],0,1,1,N_NP,D_NP,current_PPM)
			time.push(time_sec)
		}
	})
	//recalculate the duration or iTs
	let index=1
	let max_index = time.length
	let ranges_event_list = time.map(item=>{
		if(index<=max_index){
			let range= time[index] - item
			index++
			return range
		}
	})
	ranges_event_list.pop()
	//Define duration frequency : better way
	frequency = ranges_event_list.map(item=>{
		return DATA_truncate_number(1/item)
	})
	//NOTE
	nzc_note_list.forEach((nzc_note,index)=>{
		if (nzc_note <0){
			//silence
			note.push([nzc_note])
		} else {
			if(current_mute_A){
				note.push([DATA_note_to_Hz(nzc_note,current_TET)])
			}else{
				let nzc_note_A=[DATA_note_to_Hz(nzc_note,current_TET)]
				let provv=nzc_note
				A_pos_list[index].forEach(item=>{
					provv+=item
					nzc_note_A.push(DATA_note_to_Hz(provv,current_TET))
				})
				provv=nzc_note
				A_neg_list[index].forEach(item=>{
					provv-=item
					nzc_note_A.push(DATA_note_to_Hz(provv,current_TET))
				})
				note.push(nzc_note_A)
			}
		}
	})
	time.pop()//no end in midi_data only start and duration
	return {"time":time, "duration":frequency, "note_freq":note}
}

function DATA_calculate_exact_time(Psg_segment_start,Ps,Fs,N,D,N_NP,D_NP,current_PPM=PPM){
	let A = 60*(Psg_segment_start+Ps)*N_NP*D
	let B = 60*Fs*N_NP*N
	let value = (A+B)/(D_NP*D*current_PPM)
	return DATA_truncate_number(value)
}

function DATA_truncate_number(number){
	//var Number((number).toFixed(5))
	//XXX BETTER TRUNCATE FUNCTION
	return Math.trunc(number*Math.pow(10, 5))/Math.pow(10, 5) //66.66666 give error
}

function DATA_note_to_Hz(note_number,note_TET=null){
	//C4 do reg 4, nzc 0r4= 48 , midi note 60
	let f_princ = 261.62
	let f_princ_reg = 4
	//LA is the 0
	//var f_princ = 440
	//register TET
	if(note_TET==null){
		console.error("specify TET")
		return
	}
	let freq = Math.pow(2, (note_number-(f_princ_reg*note_TET))/note_TET) * f_princ
	return freq
}

function DATA_Hz_to_note(freq,note_TET=null){
	let f_princ = 261.62
	let f_princ_reg = 4
	//LA is the 0
	//var f_princ = 440
	//register TET
	if(note_TET==null){
		console.error("specify TET")
		return
	}
	let note_number = -1 //silence
	if(freq<0){
		//is a Nuzic special object
		note_number = freq
	}else{
		note_number = Math.round((Math.log2(freq/f_princ)+f_princ_reg)*note_TET)
	}
	return note_number
}

function DATA_Hz_to_midi(freq){
	let midiNote_float = 69+12*Math.log2(freq/440)
	midiNote=Math.round(midiNote_float)
	let midiNote_freq=Math.pow(2, (midiNote-69)/12) * 440
	cents=Math.round(1200*Math.log2(freq/midiNote_freq))
	return {midi_note:midiNote,cents:cents}
}

function DATA_load_state_point_data(new_file=true,reload_extra=true){
	if(!is_App)return
	//new_file reload values meant to be loaded only with new files and not in undo redo (values change doesn't trigger save new state)
	if(new_file)reload_extra=true
	let old_current_position_PMCRE = (JSON.parse(JSON.stringify(current_position_PMCRE)))
	//select and clean current objects
	let use_current_visualization_options = !reload_extra
	let data = DATA_get_current_state_point(use_current_visualization_options)
	if(data=="error" || data==null){
		console.error("error in loading: database not found")
		return
	}
	//input_active=false
	let t1, t2,t1glob,t2glob
	startTimer = function() {
		t1 = new Date().getTime();
		return t1;
	},
	stopTimer = function() {
		t2 = new Date().getTime();
		return t2 - t1;
	},startTimerglob = function() {
		t1glob = new Date().getTime();
		return t1glob;
	},
	stopTimerglob = function() {
		t2glob = new Date().getTime();
		return t2glob - t1glob;
	}
	if(debug_RE){startTimer();startTimerglob();}
	//GLOBAL VARIABLES
	let global = data.global_variables
	//[Li,PPM,TET,line_button_states]
	old_Li = Li
	Li = global.Li
	PPM = global.PPM
	mute_A=data.global_variables.A.mute
	//show_A=data.global_variables.A.show
	// if(TET != global.TET){
	TET = global.TET
	//BNAV_draw_keyboard()
	if(new_file){
		//verify note reg
		let [note,reg]=BNAV_read_keyboard_starting_note()
		if(note>=TET){
			BNAV_set_keyboard_starting_note(TET-1,reg)
		}else{
			BNAV_draw_keyboard()
		}
		setTimeout(function(){
			BNAV_keyboard_check_scroll_position()//XXX doesnt work bc scrollleftmax ==0
		}, 20)
	}
	if(debug_RE){
		console.log('DATA load keyboard ' + stopTimer() + 'ms')
		startTimer()
	}
	APP_populate_global_inputs_PMC(data)//Li TET etc...no RE, in draw RE
	TNAV_convertLiToTime()
	if(reload_extra){
		TNAV_set_countIn(global.count_in)
	}
	//modules lines
	//minimap PMC - RE - tabs opened/closed
	DATA_set_app_containers_button_states(global.app_containers)
	if(debug_RE){
		console.log('DATA load button states ' + stopTimer() + 'ms')
		startTimer()
	}
	//COMPAS
	let compas_values = data.compas
	let current_compas_values = BNAV_read_current_compas_sequence()
	//compare arrays to detect if there is a change
	if(JSON.stringify(compas_values)===JSON.stringify(current_compas_values)){
		//nothing to do
	} else {
		BNAV_write_compas_sequence(compas_values)
	}
	if(debug_RE){
		console.log('DATA compass sequence ' + stopTimer() + 'ms')
		startTimer()
	}
	//progress bar
	let end_loop_at_Li=false
	if(APP_return_progress_bar_values()[2]>=old_Li)end_loop_at_Li=true
	APP_redraw_Progress_bar_limits(new_file,end_loop_at_Li)
	if(debug_RE){
		console.log('DATA progress bar ' + stopTimer() + 'ms')
		startTimer()
	}
	//SOUND LINE
	let scale_values =JSON.parse(JSON.stringify(data.scale))
	let current_scale_values = BNAV_read_current_scale_data()
	//made in automatic control opening of tab
	//BNAV_update_S_selected()
	//compare arrays to detect if there is a change
	if(JSON.stringify(scale_values)===JSON.stringify(current_scale_values)){
		//nothing to do
	} else {
		BNAV_refresh_scale_list()
		BNAV_refresh_S_composition(scale_values)
		BNAV_write_scale_sequence(scale_values.scale_sequence)
		BNAV_refresh_PsLi()
	}
	if(debug_RE){
		console.log('DATA scale ' + stopTimer() + 'ms')
		startTimer()
	}
	//OPTIMIZED
	if(new_file){
		current_position_PMCRE={voice_data:DATA_get_selected_voice_data(),PMC:{X_start:-1,Y_start:null},RE:{scrollbar:true,column:null,X_start:global.RE.scroll_x,Y_start:global.RE.scroll_y},segment_index : null,pulse :null,fraction :null,note : null}
		//use saved global variables
		DATA_set_PMC_global_variables(global.PMC)
		RE_global_variables=JSON.parse(JSON.stringify(global.RE))
		A_global_variables=JSON.parse(JSON.stringify(global.A))
	}else{
		//scrollbars
		let scrollbarH=document.querySelector('.App_RE_main_scrollbar_H')
		let scrollbarH_left = (scrollbarH!=null)?scrollbarH.scrollLeft:global.RE.scroll_x//scrollbarH.scrollLeftMax
		let scrollbarV = document.querySelector('.App_RE_voice_matrix')
		let scrollbarV_Top = (scrollbarV!=null)?scrollbarV.scrollTop:global.RE.scroll_y//scrollbarV.scrollTopMax
		let current_voice_data= DATA_get_selected_voice_data()
		if(old_current_position_PMCRE.voice_data!=null){
			//try to find old selected voice
			let try_voice_data = data.voice_data.find(item=>{
				return item.voice_id==old_current_position_PMCRE.voice_data.voice_id && item.name==old_current_position_PMCRE.voice_data.name
			})
			if(try_voice_data!=null)current_voice_data=try_voice_data
		}
		current_position_PMCRE={voice_data:current_voice_data,PMC:{X_start:-1,Y_start:null},RE:{scrollbar:true,column:null,X_start:scrollbarH_left,Y_start:scrollbarV_Top},segment_index : 0,pulse :0,fraction :0,note : null}
	}
	if(debug_RE){
		console.log('DATA setting scrollbars ' + stopTimer() + 'ms')
		startTimer()
	}
	DATA_smart_redraw(reload_extra,global)
	if(debug_RE){
		console.log('DATA smart redraw ' + stopTimer() + 'ms')
		startTimer()
	}
	//Notebook
	let notebook = document.getElementById('App_notebook')
	if(data.notebook!=null){
		notebook.innerHTML = data.notebook
	}else{
		notebook.innerHTML = ""
	}
	if(debug_RE){
		console.log('DATA notebook ' + stopTimer() + 'ms')
		startTimer()
	}
	//current_file_name = file_name
	APP_load_minimap()
	APP_refresh_minimap()
	if(debug_RE){
		console.log('DATA minimap ' + stopTimer() + 'ms')
		//startTimer()
	}
	if(debug_RE){
		console.error('DATA load in ' + stopTimerglob() + 'ms')
		//startTimer()
	}
	//input_active=true
	//volume levels
	BNAV_update_V(data)
	DATA_set_global_V(data)
}

function DATA_smart_redraw(reload_extra=false,global=null){
	//reset global variable selection
	APP_reset_selection_global_variable()
	//redraw eventually play line OR current position IF i'm about to change something
	let provv = JSON.parse(JSON.stringify(current_position_PMCRE))
	//FLAGS
	if (tabPMCbutton.checked){
		if(flag_DATA_updated.PMC)return
		//2 times (1 for data to reload)
		if(reload_extra){
			DATA_set_PMC_global_variables(global.PMC)//XXX 2 times??? //maybe not needed (new file already does this)
		}
		//Draw PMC
		//separated form global redraw PMC
		//A (mute show show_note type)
		if(global==null){
			//switch from RE, read database
			global = DATA_get_current_state_point(false).global_variables
		}
		document.querySelector("#App_PMC_show_extra_A").checked=A_global_variables.show_note?true:false
		let show_A_button=document.querySelector("#App_PMC_vsb_A")
		//if(global.A.show && global.A.show_note){
		if(A_global_variables.show){
			show_A_button.classList.add("pressed")
		}else{
			show_A_button.classList.remove("pressed")
		}
		show_A_button.innerHTML=(A_global_variables.type=="Ag")?"iAº":("i"+A_global_variables.type)
		let A_type_selectors=[...document.querySelectorAll(".App_PMC_vsb_A_type_button")]
		A_type_selectors.forEach(button=>{
			(button.value==A_global_variables.type)?button.classList.add("pressed"):button.classList.remove("pressed")
		})
		if(A_global_variables.mute){
			document.querySelector("#App_PMC_mute_A").classList.add("pressed")
		}else{
			document.querySelector("#App_PMC_mute_A").classList.remove("pressed")
		}
		PMC_set_V_scrollbar_step_N()
		PMC_redraw_sound_line()
		PMC_populate_voice_list()
		//true redraw of PMC
		PMC_redraw()
		if(debug_PMC)console.log('Regular rendering of ALL PMC in ' + stopTimer() + 'ms')
		current_position_PMCRE=provv//XXX needed???
		//if voice.selected != current_position_PMCRE.voice_data
		if(current_position_PMCRE!=null && !current_position_PMCRE.voice_data.selected){
			PMC_reset_action_pmc()
			//clear eventually selected columns animate_play_green
			APP_reset_blink_play()
		}else{
			//draw
			if(current_position_PMCRE!=null)PMC_draw_progress_line_position(current_position_PMCRE.PMC.X_start)
		}
		flag_DATA_updated.PMC=true
		//2 times (1 for x y position)
		if(reload_extra){
			DATA_set_PMC_global_variables(global.PMC)//XXX 2 times???
		}
	}else{
		if(flag_DATA_updated.RE)return
		//Draw RE
		let data = DATA_get_current_state_point(!reload_extra)
		RE_redraw(data,debug_RE)
		flag_DATA_updated.RE=true
	}
}

async function DATA_save_local(){
	// let user = 0
	// if(typeof wp_Get_User_Permissions!='function' || DEBUG_MODE){
	// 	if(DEBUG_MODE){
	// 		user=100
	// 	}else{
	// 		return
	// 	}
	// } else{
	// 	user = await wp_Get_User_Permissions('','levels')
	// }
	// if(user==0){return}
	APP_stop()
	TNAV_read_composition_name()
	let current_state_point = DATA_get_current_state_point(true)
	const a = document.createElement('a')
	const file = new Blob([JSON.stringify(current_state_point)], {type: 'text/cvs'}) //text/plain
	a.href= URL.createObjectURL(file)
	a.download = current_file_name+extension
	a.click()
	URL.revokeObjectURL(a.href)
	TNAV_hide_main_menu()
}

async function DATA_load_local(event){
	let user = 0
	if(typeof wp_Get_User_Permissions!='function' || DEBUG_MODE){
		if(DEBUG_MODE){
			user=100
		}else{
			return
		}
	} else{
		user = await wp_Get_User_Permissions('','levels')
	}
	if(user==0){return}
	APP_stop()
	//load data file with FileIO
	let files = await FileIO.openDialog({
		contentTypes: '.nzc, .pmc, text/csv', //, image/*', // Optional. The accepted file types
		allowMultiple: false,    // Optional. Allows multiple file selection. Default is false
	})
	DATA_show_loader()
	//Do something with the selected files
	let file_name = generic_file_name
	files.forEach((file, index) => {
		console.log(`File #${index + 1}: ${file.name}`)
		file_name = file.name.split(".")[0]
	})
	let contents = await FileIO.read({
		data: files[0],                 // File/Blob
		encoding: 'UTF-8',     // ISO-8859-1   Optional. If reading as text, set the encoding. Default is UTF-8
		readAsTypedArray: false,    // Optional. Specifies to read the file as a Uint8Array. Default is false
		onProgressChange: (progressEvent) => { // Optional. Fires periodically as the file is read
			let percentLoaded = Number(((progressEvent.loaded / progressEvent.total) * 100).toFixed(2))
			console.log(`Percent Loaded: ${percentLoaded}`)
		}
	})
	//Do something with the file contents
	//verify the data version and load / convert to actual one
	let data = JSON.parse(contents)
	setTimeout(function(){
		//maybe faster???
		DATA_insert_new_state_point(data)
		DATA_load_state_point_data(true)
		//repositionate the scrollbar
		if(document.querySelector('.App_RE_voice_matrix')!=null)RE_checkScroll_H(0)
		APP_set_composition_name(file_name)
		TNAV_hide_main_menu()
		DATA_hide_loader(contents)
	}, 20)
}

async function DATA_load_server(){
	//contact server and get a list of user compositions
	let user = 0
	if(typeof wp_Get_User_Permissions!='function' || DEBUG_MODE){
		if(DEBUG_MODE){
			user=100
		}else{
			return
		}
	} else{
		user = await wp_Get_User_Permissions('','levels')
	}
	if(user==0){return}
	WEB_Open_APP_Sidebar('mis-compos',false)
	TNAV_hide_main_menu()
}

async function DATA_export_midi(){
	let user = 0
	if(typeof wp_Get_User_Permissions!='function' || DEBUG_MODE){
		if(DEBUG_MODE){
			user=100
		}else{
			return
		}
	} else{
		user = await wp_Get_User_Permissions('','levels')
	}
	if(user==0){return}
	APP_stop()
	let current_state_point = DATA_get_current_state_point(true)
	//save
	let file_data = APP_generate_midi_blob(current_state_point)
	if(file_data==null)return
	const a = document.createElement('a')
	let bytesU16 = file_data.toBytes() //returns a UTF-16 encoded string
	//!!!! UTF-8 is a variable length encoding scheme (Uint8Array and a Uint16Array) leading to data corruption
	//MIDI only use 8 bit encoding
	// Convert data to UTF-8 encoding
	let bytesU8 = new Uint8Array(bytesU16.length)
	for (let i=0; i<bytesU16.length; i++) {
		//only first part of data
		bytesU8[i] = bytesU16[i].charCodeAt(0)
	}
	let blob = new Blob([bytesU8], {type: "audio/midi"})
	a.href= URL.createObjectURL(blob)
	a.download = current_file_name+".mid"
	a.click()
	URL.revokeObjectURL(a.href)
	TNAV_hide_main_menu()
}

function DATA_new_composition_data(){
	//v6
	//var data_in = {"global_variables":{"Li":12,"PPM":120,"TET":12,"count_in":0,"PMC":{"zoom_x":1,"zoom_y":1,"scroll_x":0,"scroll_y":0,"vsb_RE":false,"vsb_N":{"show":true,"type":"Nm"},"vsb_iN":{"show":true,"type":"iNa"},"vsb_T":{"show":true,"type":"Ta"},"vsb_iT":{"show":true,"type":"iTa"}},"line_button_states":{"Na":true,"Nm":false,"Ngm":false,"iNa":true,"iNm":false,"iNgm":false,"Ta":true,"Tm":false,"iTa":true,"Tf":true},"iA_button_states":{"iAa":false,"iAm":true,"iAgm":false},"module_button_states":{"ES_RE":true,"CET_RE":true,"ES_PMC":true,"CET_PMC":true},"app_containers":{"RE":true,"PMC":false,"MMap":false,"ES_tab":true,"ET_tab":false,"Key_tab":false,"tabs_opened":false}},"compas":[{"compas_values":[0,0,0,[0],0]}],"scale":{"idea_scale_list":{"scale_list":[],"TET_list":[12]},"scale_sequence":[]},"voice_data":[{"name":"Voice 1","voice_index":0,"blocked":true,"visible":true,"volume":3,"solo":false,"instrument":0,"metro":0,"polipulse":"???","neopulse":[1,1],"selected":true,"PMC_visible":true,"color":"nuzic_blue","data":{"segment_data":[{"time":[{"P":0,"F":0},{"P":12,"F":0}],"fraction":[{"N":1,"D":1,"start":0,"stop":12}],"note":[-1,-4],"segment_name":""}],"midi_data":{"time":[0],"duration":[0.16666],"note_freq":[-1]}}}],"notebook":"","database_version":6}
	//v7
	//var data_in = {"global_variables":{"Li":12,"PPM":120,"TET":12,"count_in":0,"PMC":{"zoom_x":1,"zoom_y":1,"scroll_x":0,"scroll_y":0,"vsb_RE":true,"vsb_N":{"show":true,"type":"Nm"},"vsb_Nabc":true,"vsb_iN":{"show":true,"type":"iNm"},"vsb_T":{"show":true,"type":"Ts"},"vsb_iT":{"show":true,"type":"iT"}},"module_button_states":{"ES_RE":true,"CET_RE":true,"ES_PMC":true,"CET_PMC":true},"app_containers":{"RE":true,"PMC":false,"MMap":false,"ES_tab":true,"ET_tab":false,"Key_tab":false,"tabs_opened":false},"RE":{"Na":false,"Nm":true,"Ngm":false,"Nabc":false,"iNa":false,"iNm":true,"iNgm":false,"iAa":false,"iAm":false,"iAgm":false,"Ta":false,"Tm":false,"Ts":true,"iT":true,"Tf":true,"scroll_x":0,"scroll_y":0}},"compas":[{"compas_values":[0,0,0,[0],0]}],"scale":{"idea_scale_list":{"scale_list":[],"TET_list":[12]},"scale_sequence":[]},"voice_data":[{"name":"Voz 1","voice_index":0,"blocked":true,"volume":3,"solo":false,"instrument":0,"metro":0,"polipulse":"???","neopulse":{"N":1,"D":1},"selected":true,"PMC_visible":true,"color":"nuzic_blue","data":{"segment_data":[{"time":[{"P":0,"F":0},{"P":12,"F":0}],"fraction":[{"N":1,"D":1,"start":0,"stop":12}],"note":[-1,-4],"segment_name":"","diesis":[true,true]}],"midi_data":{"time":[0],"duration":[0.16666],"note_freq":[-1]}},"RE_visible":true}],"notebook":"","database_version":7}
	//v8
	//var data_in = {"global_variables":{"Li":12,"PPM":120,"TET":12,"count_in":0,"PMC":{"zoom_x":1,"zoom_y":1,"scroll_x":0,"scroll_y":0,"vsb_RE":true,"vsb_N":{"show":true,"type":"Nm"},"vsb_Nabc":true,"vsb_iN":{"show":true,"type":"iNm"},"vsb_T":{"show":true,"type":"Ts"},"vsb_iT":{"show":true,"type":"iT"}},"module_button_states":{"ES_RE":true,"CET_RE":true,"ES_PMC":true,"CET_PMC":true},"app_containers":{"RE":true,"PMC":false,"MMap":false,"ES_tab":true,"ET_tab":false,"Key_tab":false,"Calc_tab":false,"tabs_opened":false},"RE":{"Na":false,"Nm":true,"Ngm":false,"Nabc":false,"iNa":false,"iNm":true,"iNgm":false,"iAa":false,"iAm":false,"iAgm":false,"Ta":false,"Tm":false,"Ts":true,"iT":true,"Tf":true,"scroll_x":0,"scroll_y":0}},"compas":[{"compas_values":[0,0,0,[0],0]}],"scale":{"idea_scale_list":{"scale_list":[],"TET_list":[12]},"scale_sequence":[]},"voice_data":[{"name":voice_name,"voice_index":0,"blocked":true,"volume":3,"solo":false,"instrument":0,"metro":0,"e_sound":22,"neopulse":{"N":1,"D":1},"selected":true,"PMC_visible":true,"color":"nuzic_blue","data":{"segment_data":[{"time":[{"P":0,"F":0},{"P":12,"F":0}],"fraction":[{"N":1,"D":1,"start":0,"stop":12}],"note":[-1,-4],"segment_name":"","diesis":[true,true]}],"midi_data":{"time":[0],"duration":[0.16666],"note_freq":[-1]}},"RE_visible":true}],"notebook":"","database_version":8}
	let voice_name = `${APP_language_return_text_id("voice")} 0`
	function APP_language_return_text_id(id){
		if(lang_localization_list.length==0)return "Voice"
		let app_text_list = lang_localization_list.find(lang=>{return lang.current}).text
		let found = app_text_list.find(item=>{
			return item.id==id
		})
		if(found==null){
			//console.log(id)
			return id
		}
		return found.text
	}
	//v10
	// let data_in = {"global_variables":{"Li":12,"PPM":120,"TET":12,"count_in":0,
	// 									"PMC":{"zoom_x":1,"zoom_y":1,"scroll_x":0,"scroll_y":0,"vsb_RE":true,"vsb_N":{"show":true,"type":"Nm"},"vsb_Nabc":true,"vsb_iN":{"show":true,"type":"iNm"},"vsb_T":{"show":true,"type":"Ts"},"vsb_iT":{"show":true,"type":"iT"}},
	// 									"module_button_states":{"ES_RE":true,"CET_RE":true,"ES_PMC":true,"CET_PMC":true},
	// 									"app_containers":{"RE":true,"PMC":false,"MMap":false,"ES_tab":true,"ET_tab":false,"Calc_tab":false,"Key_tab":false,"tabs_opened":false},
	// 									"RE":{"Na":false,"Nm":true,"Ngm":false,"Nabc":false,"iNa":false,"iNm":true,"iNgm":false,"Ta":false,"Tm":false,"Ts":true,"iT":true,"Tf":true,"scroll_x":0,"scroll_y":0},
	// 									"A":{"show":true,"show_note":true,"type":"Am","mute":false}},"compas":[{"compas_values":[0,0,0,[0],0]}],"scale":{"idea_scale_list":[],"scale_sequence":[]},
	// 				"voice_data":[{"name":voice_name,"voice_index":0,"blocked":true,"volume":3,"solo":false,"instrument":0,"metro":0,"e_sound":22,"neopulse":{"N":1,"D":1},"selected":true,"PMC_visible":true,"color":"nuzic_blue","data":{"segment_data":[{"time":[{"P":0,"F":0},{"P":12,"F":0}],"fraction":[{"N":1,"D":1,"start":0,"stop":12}],"segment_name":"","sound":[{"note":-1,"diesis":true,"A_pos":[],"A_neg":[]},{"note":-4,"diesis":true,"A_pos":[],"A_neg":[]}]}],"midi_data":{"time":[0],"duration":[0.16666],"note_freq":[[-1]]}},"RE_visible":true,"A":false}],
	// 				"notebook":"","database_version":10}
	//v11
	let data_in = {"global_variables":{"Li":12,"PPM":120,"TET":12,"count_in":0,
										"PMC":{"zoom_x":1,"zoom_y":1,"scroll_x":0,"scroll_y":50,"vsb_RE":false,"vsb_N":{"show":true,"type":"Nm"},"vsb_Nabc":true,"vsb_iN":{"show":true,"type":"iNm"},"vsb_T":{"show":true,"type":"Ts"},"vsb_iT":{"show":true,"type":"iT"}},
										"module_button_states":{"ES_RE":true,"CET_RE":true,"ES_PMC":true,"CET_PMC":true},
										"app_containers":{"RE":false,"PMC":true,"MMap":false,"V_tab":true,"ES_tab":false,"ET_tab":false,"Calc_tab":false,"Key_tab":false,"tabs_opened":false},
										"RE":{"Na":false,"Nm":true,"Ngm":false,"Nabc":false,"iNa":false,"iNm":true,"iNgm":false,"Ta":false,"Tm":false,"Ts":true,"iT":true,"Tf":true,"scroll_x":0,"scroll_y":0},
										"A":{"show":true,"show_note":true,"type":"Am","mute":false},"effects":{"reverb":0}},"compas":[{"compas_values":[0,0,0,[0],0]}],"scale":{"idea_scale_list":[],"scale_sequence":[]},
					"voice_data":[{"name":"Voz 0","blocked":true,"volume":90,"solo":false,"instrument":0,"metro":0,"e_sound":22,"neopulse":{"N":1,"D":1},"selected":true,"PMC_visible":true,"color":"nuzic_blue","data":{"segment_data":[{"time":[{"P":0,"F":0},{"P":12,"F":0}],"fraction":[{"N":1,"D":1,"start":0,"stop":12}],"segment_name":"","sound":[{"note":-1,"diesis":true,"A_pos":[],"A_neg":[]},{"note":-4,"diesis":true,"A_pos":[],"A_neg":[]}]}],"midi_data":{"time":[0],"duration":[0.16666],"note_freq":[[-1]]}},"RE_visible":true,"A":false,"voice_id":0,"midiCh":null,"mute":false,"pan":0.5,"fx":1,"lpf":0.5,"vel":80}],
					"notebook":"","database_version":11}
	let data = DATA_control_database_version(data_in)
	return data
}

async function DATA_new(){
	APP_stop()
	DATA_show_loader()
	setTimeout(function(){
		//make with pure DATA ??
		let data = DATA_new_composition_data()
		DATA_insert_new_state_point(data)
		DATA_load_state_point_data(true)
		TNAV_reset_composition_name()
		current_composition_id=""
		if(typeof Reset_current_composition_id === "function"){
			Reset_current_composition_id()
		}
		DATA_hide_loader()
		TNAV_hide_main_menu()
	}, 20)
}

//local HTML5 storage for preferences
function DATA_save_preferences_file(){
	let data = DATA_read_current_state_preferences()
	localStorage.setItem('nuzic_preferences', JSON.stringify(data))
}

function DATA_load_preferences_file(){
	let contents = localStorage.getItem('nuzic_preferences')
	if(contents!=null){
		let pref_data = JSON.parse(contents)
		if(pref_data.version==preferences_version){
			//GLOBAL OPTIONS
			//show or not hover labels
			APP_set_hover_labels(pref_data.showHoverLabels)
			//Play naw note pressing enter or arrows
			SNAV_set_play_new_note(pref_data.playNewNote)
			SNAV_set_note_on_click(pref_data.playNoteOnClick)
			//XXX DISABLED
			//SNAV_set_note_over_grade(pref_data.noteOverGrade)
			SNAV_set_note_over_grade(true)
			//XXX DISABLED
			//SNAV_set_alert_overwrite_scale(pref_data.showAlertOverwriteScale)
			SNAV_set_alert_overwrite_scale(true)
			//main volume
			BNAV_set_master_volume(pref_data.mainVolume)
			//main pan
			if(pref_data.mainPan==null){
				pref_data.mainPan=0.5
			}
			BNAV_set_master_pan(pref_data.mainPan)
			//main metronome
			TNAV_set_main_metronome_OnOff(pref_data.mainMetronome)
			//old version of pref_data
			if(pref_data.mainMetronomeVolume==null)pref_data.mainMetronomeVolume=90
			//console.log(pref_data.mainMetronomeVolume)
			BNAV_set_main_metronome_volume(pref_data.mainMetronomeVolume)
			//main loop
			TNAV_set_loop_OnOff(pref_data.mainLoop)
			//language
			if(pref_data.language!=null){
				//console.log("language data found")
				lang_localization_list.push(pref_data.language)
				current_language = pref_data.language.lang
				APP_verify_language_availability(true)
				APP_change_texts_language()
			}else{
				current_language = "ES"
			}
			//MIDI DEVICES
			DATA_load_preferences_file_onlyMIDI(pref_data)

			/*/COMPOSITION_DATA EXCLUDED
			data.global_variables.PMC=pref_data.PMC
			data.global_variables.RE=pref_data.RE

			//modules
			data.global_variables.module_button_states = pref_data.module_button_states
			//app containers
			if(pref_data.app_containers!=null){
				data.global_variables.app_containers = pref_data.app_containers
			}
			*/
			APP_set_PMC_extra_info(pref_data.showPMCextra)
			//scale lists
			BNAV_set_show_scale_list(pref_data.showScaleList)
			BNAV_set_show_global_scale(pref_data.showGLOBALscale)
			BNAV_set_show_user_scale(pref_data.showUSERscale)
			BNAV_set_show_editor_selected_scale(pref_data.showEDITORscale)
		}else{
			console.error("App stored preferences version obsolete")
			//detect language
			let userLang = navigator.language || navigator.userLanguage
			console.log("The language is: " + userLang)
			let language="ES"
			if(userLang=="en-GB" || userLang=="en-US" || userLang=="en")language="EN"
			if(userLang=="ca")language="CAT"
			if(userLang=="it-IT" || userLang=="it")language="IT"
			current_language=language
			//wait download language and reload preferences file
		}
	}else{
		//first time open app
		//detect language
		let userLang = navigator.language || navigator.userLanguage
		let language="ES"
		if(userLang=="en-GB" || userLang=="en-US" || userLang=="en")language="EN"
		if(userLang=="ca")language="CAT"
		if(userLang=="it-IT" || userLang=="it")language="IT"
		current_language=language
		//wait download language and reload preferences file
	}
}

function DATA_load_preferences_file_onlyMIDI(pref_data=null){
	if(pref_data==null){
		let contents = localStorage.getItem('nuzic_preferences')
		if(contents!=null){
			pref_data = JSON.parse(contents)
		}
	}
	if(pref_data==null)return
	if(pref_data.version==preferences_version){
		//MIDI DEVICES
		if(pref_data.speaker==null){
			//true if speaker
			pref_data.speaker=false
			pref_data.midiIn_ID=null
			pref_data.midiOut_ID=null
		}
		BNAV_set_output_preferences(pref_data.speaker,pref_data.midiOut_ID)
		BNAV_set_input_preferences(pref_data.midiIn_ID)
	}
}

function DATA_read_current_state_preferences(){
	let data = {}
	data.version=preferences_version
	//show or not hover labels
	data.showHoverLabels = APP_read_hover_labels()
	//Play new note pressing enter or arrows
	data.playNewNote = SNAV_read_play_new_note()
	//play note on click
	data.playNoteOnClick = SNAV_read_note_on_click()
	data.noteOverGrade = true
	data.showAlertOverwriteScale = true
	//main volume
	data.mainVolume = BNAV_read_master_volume()
	//main pan
	data.mainPan = BNAV_read_master_pan()
	//main metronome
	data.mainMetronome = (outputMainMetronome.value === 'true')
	data.mainMetronomeVolume = BNAV_read_main_metronome_volume()
	//main loop
	data.mainLoop = (outputLoop.value === 'true')
	//language
	data.language=lang_localization_list.find(lang=>{return lang.current})
	data.showPMCextra = APP_read_PMC_extra_info()
	data.showScaleList = BNAV_read_show_scale_list()
	data.showGLOBALscale = BNAV_read_show_global_scale()
	data.showUSERscale = BNAV_read_show_user_scale()
	data.showEDITORscale = BNAV_read_show_editor_selected_scale()
	//midi in out
	let [speaker,midi_output_ID]=BNAV_read_output_preferences()
	data.speaker=speaker
	data.midiIn_ID=BNAV_read_input_preferences()
	data.midiOut_ID=midi_output_ID
	return data
}

function DATA_switch_sound_preferences_mod_grade(){
	//RE_global_variables
	let RE_mod_state={"Na": false,"Nm": true,"Ngm": false,"Nabc": false,"iNa": false,"iNm": true,"iNgm": false,"Ta": RE_global_variables.Ta,"Tm": RE_global_variables.Tm,"Ts": RE_global_variables.Ts,"iT": RE_global_variables.iT,"Tf": RE_global_variables.Tf,"scroll_x": RE_global_variables.scroll_x,"scroll_y": RE_global_variables.scroll_y}
	let RE_grade_state={"Na": false,"Nm": false,"Ngm": true,"Nabc": false,"iNa": false,"iNm": false,"iNgm": true,"Ta": RE_global_variables.Ta,"Tm": RE_global_variables.Tm,"Ts": RE_global_variables.Ts,"iT": RE_global_variables.iT,"Tf": RE_global_variables.Tf,"scroll_x": RE_global_variables.scroll_x,"scroll_y": RE_global_variables.scroll_y}
	let RE_grade_state_string=JSON.stringify(RE_grade_state)
	let RE_global_variables_string=JSON.stringify(RE_global_variables)
	if(RE_global_variables_string!=RE_grade_state_string){
		//to grade
		RE_global_variables=RE_grade_state
	}else{
		//to modular
		RE_global_variables=RE_mod_state
	}
	//PMC
	let PMC_global_variables=DATA_read_PMC_global_variables()
	let PMC_mod_state={"zoom_x": PMC_global_variables.zoom_x,"zoom_y": PMC_global_variables.zoom_y,"scroll_x": PMC_global_variables.scroll_x,"scroll_y": PMC_global_variables.scroll_y,"vsb_RE": false,"vsb_N": {"show": true,"type": "Nm"},"vsb_Nabc": PMC_global_variables.vsb_Nabc,"vsb_iN": {"show": true,"type": "iNm"},"vsb_T": PMC_global_variables.vsb_T,"vsb_iT": PMC_global_variables.vsb_iT}
	let PMC_grade_state={"zoom_x": PMC_global_variables.zoom_x,"zoom_y": PMC_global_variables.zoom_y,"scroll_x": PMC_global_variables.scroll_x,"scroll_y": PMC_global_variables.scroll_y,"vsb_RE": false,"vsb_N": {"show": true,"type": "Ngm"},"vsb_Nabc": PMC_global_variables.vsb_Nabc,"vsb_iN": {"show": true,"type": "iNgm"},"vsb_T": PMC_global_variables.vsb_T,"vsb_iT": PMC_global_variables.vsb_iT}
	let PMC_grade_state_string=JSON.stringify(PMC_grade_state)
	let PMC_global_variables_string=JSON.stringify(PMC_global_variables)
	if(PMC_global_variables_string!=PMC_grade_state_string){
		//to grade
		DATA_set_PMC_global_variables(PMC_grade_state)
	}else{
		//to modular
		DATA_set_PMC_global_variables(PMC_mod_state)
	}
	//A_global_variables
	let A_mod_state={"show": A_global_variables.show,"show_note": A_global_variables.show_note,"type": "Am","mute": A_global_variables.mute}
	let A_grade_state={"show": A_global_variables.show,"show_note": A_global_variables.show_note,"type": "Ag","mute": A_global_variables.mute}
	let A_grade_state_string=JSON.stringify(A_grade_state)
	let A_global_variables_string=JSON.stringify(A_global_variables)
	if(A_global_variables_string!=A_grade_state_string){
		//to grade
		A_global_variables=A_grade_state
	}else{
		//to modular
		A_global_variables=A_mod_state
	}
	flag_DATA_updated.RE=false
	flag_DATA_updated.PMC=false
	DATA_smart_redraw()
}

//load scale list from general database
function DATA_load_global_scale_list(){
	let contents = localStorage.getItem('nuzic_global_scales_family')
	if(contents!=null){
		//start loading this one
		console.log("Loading local file Global scales")
		global_scale_list=JSON.parse(contents)
		BNAV_generate_TET_list_from_scale_list(global_scale_list)
		BNAV_refresh_scale_list()
	}else{
		//create a VERY basic database
		//TET 6
		//TET 12
		//TET 24
		global_scale_list={scale_list:[
			{
				TET:6,
				module:3,
				name:"Aldo",
				iS:[2, 2, 2],
				tags:[],
				info:""
			},
			{
				TET:12,
				module:3,
				name:"Giovanni",
				iS:[4, 4 ,4],
				tags:[],
				info:""
			},
				{
				TET:24,
				module:2,
				name:"Giacomo",
				iS:[8, 8, 8],
				tags:[],
				info:""
			}
		]}
		//save this database in nuzic_global_scales
		localStorage.setItem('nuzic_global_scales_family', JSON.stringify(global_scale_list))
		//refresh tab global scale
		BNAV_generate_TET_list_from_scale_list(global_scale_list)
		BNAV_refresh_scale_list()
	}
	//try to retrive from server AND eventually use them in the global scale list
	let url = 'https://cdn.nuzic.org/Arch/Scale_global.dat?3'
	//let url = 'https://cdn.nuzic.org/Arch/Scale_global.dat?123'//cache
	// read text from URL location
	let request = new XMLHttpRequest()
	request.open('GET', url, true)
	//request.setRequestHeader("Cache-Control", "no-cache, no-store, max-age=0")
	request.send(null)
	request.onreadystatechange = function () {
		if (request.readyState === 4 && request.status === 200) {
			let type = request.getResponseHeader('Content-Type')
			if (type.indexOf("text") !== 1) {
				let returnValue = request.responseText
				let datatest = JSON.parse(returnValue)
				let current_datatest = {scale_list: global_scale_list.scale_list}
				//refresh tab global scale if there is a new database
				if(JSON.stringify(datatest)!=JSON.stringify(current_datatest)){
					global_scale_list=datatest
					//save this database in nuzic_global_scales
					console.log("Loading NEW external file Global scales")
					//attention at ===scale BUT different tags!!!!!!!!!!!
					localStorage.setItem('nuzic_global_scales_family', JSON.stringify(global_scale_list))
					BNAV_generate_TET_list_from_scale_list(global_scale_list)
					BNAV_refresh_scale_list()
				}else{
					//nothing to do
				}
				return returnValue
			}
		}
	}
}

function DATA_save_user_scale_list(){
	BNAV_generate_TET_list_from_scale_list(user_scale_list)
	let data_to_save= {scale_list: user_scale_list.scale_list}
	//save locally
	localStorage.setItem('nuzic_user_scales', JSON.stringify(data_to_save))
}

//load user scale list
function DATA_load_user_scale_list(){
	let contents = localStorage.getItem('nuzic_user_scales')
	if(contents!=null){
		//start loading this one
		console.log("Loading local file User scales")
		user_scale_list=JSON.parse(contents)
		if(user_scale_list.scale_list.length!=0){
			if(user_scale_list.scale_list[0].family==undefined){
				console.log("Old user scale data, cleaning")
				user_scale_list={scale_list:[],TET_list:[]}
				//save this database in nuzic_user_scales
				DATA_save_user_scale_list()
			}
		}
		BNAV_generate_TET_list_from_scale_list(user_scale_list)
	}else{
		//create a VERY basic database
		user_scale_list={scale_list:[],TET_list:[]}
		//save this database in nuzic_user_scales
		DATA_save_user_scale_list()
	}
}

function DATA_show_loader(){
	let background = document.getElementById('App_loading_background')
	let bouncer = document.getElementById('App_loading_bouncer')
	//show background loader
	background.style.display = 'flex'
	bouncer.style.display = 'flex'
}

function DATA_hide_loader(){
	let background = document.getElementById('App_loading_background')
	let bouncer = document.getElementById('App_loading_bouncer')
	//hude background loader
	background.style.display = 'none'
	bouncer.style.display = 'none'
}

function DATA_export_audio(){
	//control if all instruments loaded
	let type=SNAV_read_export_audio_file_format()
	if(!APP_verify_sound_ready()){
		return
	}
	DATA_show_loader()
	APP_stop()
	//real time recording OGG
	//no error, doesnt record
	SF_manager.SF_source.disconnect()
	const audioContext = new AudioContext();
	const mixedOutput = audioContext.createMediaStreamDestination();
	const audioStream1 = audioContext.createMediaStreamSource(SF_manager.SF_destination.stream);
	audioStream1.connect(mixedOutput);
	let [now,start,end,max]=APP_return_progress_bar_values()
	//XXX count in not
	var countin = 0
	var time_ms = (Number(end) - Number(start) - countin) * 1000 * 60 / PPM + 50
	//force play bar
	var progressBar = document.querySelector('#progress_bar')
	progressBar.value=start
	//APP_check_minimap_scroll()
	const chunks = []//XXX
	//API recorder WORKING!!!
	//let web_recorder = new MediaRecorder(SF_manager.SF_destination.stream)
	//with multiple streams
	let web_recorder = new MediaRecorder(mixedOutput.stream);
	// web_recorder.ondataavailable = evt => chunks.push(evt.data);
	/*OGG
	web_recorder.onstop = evt => {
		let blob = new Blob(chunks, { type: 'audio/ogg; codecs=opus' });
		const a = document.createElement('a')
		a.href= URL.createObjectURL(blob)
		a.download = "recording.ogg"
		a.click()
		URL.revokeObjectURL(a.href)
		TNAV_hide_main_menu()
		//web_recorder.dispose()
		setTimeout(async () => {
			// the recorded audio is returned as a blob
			console.log("end recording")
			SF_manager.SF_source.connect(SF_manager.context.destination)
		}, 200)
	};*/
	//TEST MP3
	web_recorder.onstop = evt => {
		//let blob = new Blob(chunks, { type: 'audio/ogg; codecs=opus' })
		let blob = new Blob(chunks, { type: 'audio/webm' })
		function blobToArrayBuffer(blob) {
			return new Promise((resolve, reject) => {
			const reader = new FileReader()
			reader.onloadend = () => resolve(reader.result)
			reader.onerror = reject
			reader.readAsArrayBuffer(blob)
			});
		}
		blobToArrayBuffer(blob).then(arrayBuffer => {
			const audioContext = new AudioContext()
			audioContext.decodeAudioData(arrayBuffer, (audioBuffer) => {
				TNAV_hide_main_menu()
				// Process Audio
				let offlineAudioCtx = new (window.OfflineAudioContext || window.webkitOfflineAudioContext)({
					numberOfChannels: audioBuffer.numberOfChannels,
					length: audioBuffer.length,
					sampleRate: audioBuffer.sampleRate,
				})
				// Audio Buffer Source
				let soundSource = offlineAudioCtx.createBufferSource()
				soundSource.buffer = audioBuffer
				soundSource.connect(offlineAudioCtx.destination)
				soundSource.start(0)
				const worker = new Worker("WORKER_Export_Audio.js")
				const a = document.createElement('a')
				offlineAudioCtx.startRendering().then(function(renderedBuffer) {
					let sampleRate = renderedBuffer.sampleRate
					let numOfChan = renderedBuffer.numberOfChannels
					let channels = []
					for(let i = 0; i < numOfChan; i++)channels.push(renderedBuffer.getChannelData(i))
					worker.postMessage({
						command: 'exportAudio',
						data: {
							type:type,
							//buffer: renderedBuffer,//cannot pass renderedBuffer
							sampleRate:sampleRate,
							numOfChan:numOfChan,
							channels:channels,
							len: offlineAudioCtx.length,
						}
					})
				}).catch(function(err) {
					// Handle error
					APP_warning_popup("Error export audio file")
				})
				worker.onmessage = (event) => {
					if(type=="mp3"){
						let mp3_blob = event.data
						let new_file = URL.createObjectURL(mp3_blob)
						a.href= new_file
						a.download = "recording.mp3"
					}else{
						let wave_blob = event.data
						let new_file = URL.createObjectURL(wave_blob)
						a.href= new_file
						a.download = "recording.wav"
					}
					a.click()
					URL.revokeObjectURL(a.href)
					worker.terminate()
					DATA_hide_loader()
				}
			})
		})
		setTimeout(async () => {
			// the recorded audio is returned as a blob
			console.log("end recording")
			SF_manager.SF_source.connect(SF_manager.context.destination)
		}, 200)
	};

	setTimeout(async () => {
		console.log("start recording")
		// start recording
		web_recorder.start()
		//play note
		APP_play_recorder()
		// wait for the notes to end and stop the recording
		//with chunks
		web_recorder.ondataavailable = evt => chunks.push(evt.data)
		setTimeout(async () => {
			// the recorded audio is returned as a blob
			APP_stop()
			DATA_hide_loader()
			web_recorder.stop()
		}, time_ms)
	}, 100)
}

function DATA_read_app_containers_button_states(){
	//get buttons states
	let PMC_button = tabPMCbutton.checked
	let RE_button = tabREbutton.checked
	let show_minimap = (document.querySelector("#App_show_minimap").value == 1)
	let V_button = document.getElementById("App_BNav_tab_V").checked
	let ES_button = document.getElementById("App_BNav_tab_ES").checked
	let ET_button = document.getElementById("App_BNav_tab_ET").checked
	let Calc_button = document.getElementById("App_BNav_tab_calc").checked
	let Key_button = document.getElementById("App_BNav_tab_keyboard").checked
	let tab_open = document.querySelector('.App_BNav_tab_button').classList.contains('App_BNav_tab_button_rotated')
	return {
		RE:RE_button,
		PMC:PMC_button,
		MMap: show_minimap,
		V_tab: V_button,
		ES_tab: ES_button,
		ET_tab: ET_button,
		Calc_tab: Calc_button,
		Key_tab: Key_button,
		tabs_opened: tab_open
		}
}

function DATA_set_app_containers_button_states(app_containers_button_states){
	if(app_containers_button_states==null)return
	let V_button = document.getElementById("App_BNav_tab_V")
	let ES_button = document.getElementById("App_BNav_tab_ES")
	let ET_button = document.getElementById("App_BNav_tab_ET")
	let calc_button = document.getElementById("App_BNav_tab_calc")
	let Key_button = document.getElementById("App_BNav_tab_keyboard")
	let tab_open_button = document.querySelector('.App_BNav_tab_button')
	APP_set_show_hide_minimap(app_containers_button_states.MMap)
	tabPMCbutton.checked = app_containers_button_states.PMC
	tabREbutton.checked = app_containers_button_states.RE
	BNAV_show_RE_PMC()
	V_button.checked = app_containers_button_states.V_tab
	ES_button.checked = app_containers_button_states.ES_tab
	ET_button.checked = app_containers_button_states.ET_tab
	calc_button.checked = app_containers_button_states.Calc_tab
	Key_button.checked = app_containers_button_states.Key_tab
	if(app_containers_button_states.tabs_opened!=tab_open_button.classList.contains('App_BNav_tab_button_rotated')){
		tab_open_button.click()
	}
	if(app_containers_button_states.tabs_opened){
		//verify if correct tab is shown
		BNAV_open_bottom_nav_content()
	}
}

function DATA_read_PMC_global_variables(){
	let container = document.querySelector(".App_PMC")
	let scrollbar_V = container.querySelector('.App_PMC_main_scrollbar_V')
	let y_val = Number(document.querySelector("#App_PMC_select_zoom_sound").value)
	return {
		zoom_x:PMC_zoom_x,
		//zoom_y:PMC_zoom_y,  //0 is all
		zoom_y:y_val,
		scroll_x:PMC_main_scrollbar_H.scrollLeft,
		scroll_y:DATA_calculate_vertical_srollbar_percentage(scrollbar_V),
		//vsb_RE:PMC_read_vsb_RE(),
		vsb_RE:false,
		vsb_N:PMC_read_vsb_N(),
		vsb_Nabc:PMC_read_vsb_Nabc(),
		vsb_iN:PMC_read_vsb_iN(),
		vsb_T:PMC_read_vsb_T(),
		vsb_iT:PMC_read_vsb_iT(),
		}
}

function DATA_calculate_vertical_srollbar_percentage(scrollbar){
	var p = (scrollbar.scrollTop) / (scrollbar.scrollHeight - scrollbar.clientHeight ) * 100
	if(p<0)p=0
	if(p>100)p=100
	return p
}

function DATA_set_PMC_global_variables(PMC){
	//zoom(s)
	PMC_set_zoom_sound(PMC.zoom_y)
	PMC_set_zoom_time(PMC.zoom_x)
	//scrollbar position //XXX HERE??? maybe later (when the alignment has drawn everything
	PMC_checkScroll_V(PMC.scroll_y,true)
	PMC_checkScroll_H(PMC.scroll_x,true)
	PMC_set_vsb_N(PMC.vsb_N)
	PMC_set_vsb_Nabc(PMC.vsb_Nabc)
	PMC_set_vsb_iN(PMC.vsb_iN)
	PMC_set_vsb_T(PMC.vsb_T)
	PMC_set_vsb_iT(PMC.vsb_iT)
}

let spread1=false
let spread2=false
let spread3=false
function DATA_load_languages(){
	//https://docs.google.com/spreadsheets/d/1gfpguRCpplCSn2TAsRWSm_npz043u0gbrzlyrT3WuBs/edit#gid=0
	//https://docs.google.com/spreadsheets/d/{key}/gviz/tq?tqx=out:csv&sheet={sheet_name}
	//https://docs.google.com/spreadsheets/d/{key}/gviz/tq?tqx=out:json&sheet={sheet_name}&range={A:B}
	//try to retrive spreadsheet from server AND eventually ise it
	//HOVER TEXTS
	let spreasdheet_ID="1gfpguRCpplCSn2TAsRWSm_npz043u0gbrzlyrT3WuBs"
	let url = 'https://docs.google.com/spreadsheets/d/'+spreasdheet_ID+'/gviz/tq?tqx=out:json&sheet=APP_HOVER'//&sheet=
	// read text from URL location
	let request = new XMLHttpRequest()
	request.open('GET', url, true)
	request.send(null)
	request.onreadystatechange = function () {
		if (request.readyState === 4 && request.status === 200) {
			let type = request.getResponseHeader('Content-Type')
			if (type.indexOf("text") !== 1) {
				let returnValue = request.responseText
				let lines=returnValue.split("setResponse(")
				let datatest = JSON.parse(lines[1].substring(0, lines[1].length-2))
				let table = datatest.table.rows
				let language_array = []
				datatest.table.rows[0].c.forEach(cell=>{
					if(cell!=null && cell.v!=null)language_array.push(cell.v)
				})
				//find languages and add to database
				APP_add_language_array(language_array)
				language_array.forEach((language,index)=>{
					//add specific columns to language data
					let index_column_title=index*2+1
					let index_column_text=index*2+2
					//look if is complete
					if(table[1].c[index_column_title].v=="ok"){
						//console.log(language+ "to be added")
						let lang_index=-1
						let found = lang_localization_list.find((item,item_index)=>{
							lang_index=item_index
							return item.lang==language
						})
						if (found!=null){
							//clear
							lang_localization_list[lang_index].hover=[]
							for(let i=3;i<table.length;i++){
								let optional_text =(table[i].c[index_column_text]!=null && table[i].c[index_column_text].v!=null)?table[i].c[index_column_text].v:""
								lang_localization_list[lang_index].hover.push({id:table[i].c[0].v,title:table[i].c[index_column_title].v,text:optional_text})
							}
						}
					}
				})
				spread1=true
				APP_verify_language_availability()
				return
			}
		}
	}
	//TEXTS
	url = 'https://docs.google.com/spreadsheets/d/'+spreasdheet_ID+'/gviz/tq?tqx=out:json&sheet=APP_TEXT'
	// read text from URL location
	let request2 = new XMLHttpRequest()
	request2.open('GET', url, true)
	request2.send(null)
	request2.onreadystatechange = function () {
		if (request2.readyState === 4 && request2.status === 200) {
			let type = request2.getResponseHeader('Content-Type')
			if (type.indexOf("text") !== 1) {
				let returnValue = request2.responseText
				let lines=returnValue.split("setResponse(")
				let datatest = JSON.parse(lines[1].substring(0, lines[1].length-2))
				let table = datatest.table.rows
				let language_array = []
				table[0].c.forEach(cell=>{
					if(cell!=null && cell.v!=null)language_array.push(cell.v)
				})
				//find languages and add to database
				APP_add_language_array(language_array)
				language_array.forEach((language,index)=>{
					//add specific columns to language data
					//look if is complete
					let index_column_text=index+1
					if(table[1].c[index_column_text].v=="ok"){
						let lang_index=-1
						let found = lang_localization_list.find((item,item_index)=>{
							lang_index=item_index
							return item.lang==language
						})
						if (found!=null){
							lang_localization_list[lang_index].text=[]
							for(let i=3;i<table.length;i++){
								lang_localization_list[lang_index].text.push({id:table[i].c[0].v,text:table[i].c[index_column_text].v})
							}
						}
					}
				})
				spread2=true
				APP_verify_language_availability()
				return
			}
		}
	}
	//MESSAGES
	//TEXTS
	url = 'https://docs.google.com/spreadsheets/d/'+spreasdheet_ID+'/gviz/tq?tqx=out:json&sheet=APP_MSG'//&sheet=
	// read text from URL location
	let request3 = new XMLHttpRequest()
	request3.open('GET', url, true)
	request3.send(null)
	request3.onreadystatechange = function () {
		if (request3.readyState === 4 && request3.status === 200) {
			let type = request3.getResponseHeader('Content-Type')
			if (type.indexOf("text") !== 1) {
				let returnValue = request3.responseText
				let lines=returnValue.split("setResponse(")
				let datatest = JSON.parse(lines[1].substring(0, lines[1].length-2))
				let table = datatest.table.rows
				let language_array = []
				datatest.table.rows[0].c.forEach(cell=>{
					if(cell!=null && cell.v!=null)language_array.push(cell.v)
				})
				//find languages and add to database
				APP_add_language_array(language_array)
				language_array.forEach((language,index)=>{
					//add specific columns to language data
					let index_column_text=index+2
					//look if is complete
					if(table[1].c[index_column_text].v=="ok"){
						//console.log(language+ "to be added")
						let lang_index=-1
						let found = lang_localization_list.find((item,item_index)=>{
							lang_index=item_index
							return item.lang==language
						})
						if (found!=null){
							lang_localization_list[lang_index].msg=[]
							for(let i=3;i<table.length;i++){
								lang_localization_list[lang_index].msg.push({id:table[i].c[0].v,type:table[i].c[1].v,text:table[i].c[index_column_text].v})
							}
						}
					}
				})
				spread3=true
				APP_verify_language_availability()
				return
			}
		}
	}
}

//global variable

function DATA_set_global_V(data){
	V_global_variables.effects={reverb:data.global_variables.effects.reverb}
	let max_voice_id=data.voice_data.reduce((prop,item)=>{return (item.voice_id>prop)?item.voice_id:prop},0)+1
	V_global_variables.volumes=new Array(max_voice_id)
	data.voice_data.forEach(voice=>{
		let item={volume: voice.volume, fx: voice.fx, lpf: voice.lpf, vel:voice.vel, pan:voice.pan}
		V_global_variables.volumes[voice.voice_id]=item
	})
}
