/////////*****REPETITIONS*****/////////

function DATA_repeat_voice(voice_id,repetitions,insert){
	let data = DATA_get_current_state_point(true)
	//remove solo
	data.voice_data.forEach(voice=>{
		voice.solo=false
	})
	//duplicate current voice
	let voice_position = -1
	let voice_data = JSON.parse(JSON.stringify(data.voice_data.find(item=>{
		voice_position++
		return item.voice_id==voice_id
	})))
	if(voice_data==null)return
	if(insert){
		let voice_id_list = data.voice_data.map(item=>{
			let number = Number(item.voice_id)
			return number
		})
		let new_voice_id = Math.max(...voice_id_list)+1
		voice_data.solo= false
		voice_data.metro= 0
		voice_data.selected=false
		for (let i = 0; i < repetitions; i++) {
			let voice_data_copy=JSON.parse(JSON.stringify(voice_data))
			voice_data_copy.voice_id = new_voice_id
			new_voice_id++
			let string = voice_data_copy.name+"#"
			voice_data_copy.name= string.slice(0,8)
			let new_color = DATA_calculate_data_new_color(data)
			if(new_color!=null){
				voice_data_copy.color=new_color
			}
			data.voice_data.splice(voice_position,0,voice_data_copy)
		}
		DATA_insert_new_state_point(data)
		DATA_load_state_point_data(false,true)
	}else{
		//overwrite
		let voice_id_list = data.voice_data.map(item=>{
			let number = Number(item.voice_id)
			return number
		})
		let new_voice_id = Math.max(...voice_id_list)+1
		voice_data.solo= false
		voice_data.metro= 0
		voice_data.selected=false
		for (let i = 0; i < repetitions; i++) {
			let voice_data_copy=JSON.parse(JSON.stringify(voice_data))
			voice_data_copy.voice_id = new_voice_id
			new_voice_id++
			let string = voice_data_copy.name+"#"
			voice_data_copy.name= string.slice(0,8)
			voice_position--
			if(voice_position>=0){
				//change voice
				data.voice_data.splice(voice_position,1,voice_data_copy)
			}else{
				//add voice
				let new_color = DATA_calculate_data_new_color(data)
				if(new_color!=null){
					voice_data_copy.color=new_color
				}
				data.voice_data.splice(0,0,voice_data_copy)
			}
		}
		//verify at least oneselected exist
		let something_selected = data.voice_data.some(voice=>{
			return voice.selected
		})
		if(!something_selected){
			//select the copied voice
			data.voice_data.find(item=>{
				if(item.voice_id==voice_id)item.selected=true
				return item.voice_id==voice_id
			})
		}
		DATA_insert_new_state_point(data)
		DATA_load_state_point_data(false,true)
	}
}

function DATA_repeat_segment(voice_id,segment_index,repetitions,insert){
	let data = DATA_get_current_state_point(true)
	let current_voice_data = data.voice_data.find(item=>{
		return item.voice_id==voice_id
	})
	if(current_voice_data==null){
		console.error("Error repeating segment, voice index not found")
		return
	}
	if(current_voice_data.data.segment_data.length<segment_index || segment_index<0){
		console.error("Error repeating segment, segment index not found")
		return
	}
	let segment_Ls_absolte = current_voice_data.data.segment_data[segment_index].time.slice(-1)[0].P *current_voice_data.neopulse.N/current_voice_data.neopulse.D
	let segment_data=JSON.parse(JSON.stringify(current_voice_data.data.segment_data[segment_index]))
	if(insert){
		let new_Li_absolute=(segment_Ls_absolte*repetitions+data.global_variables.Li)
		//verify if compatible with max Li
		if(new_Li_absolute>max_Li){
			console.error("ERROR: max Li reached")
			return
		}
		if(current_voice_data.blocked){
			//voice is blocked
			data.voice_data.forEach(voice=>{
				let new_Ls=segment_Ls_absolte*voice.neopulse.D/voice.neopulse.N
				let empty_segment_data=DATA_calculate_new_segment_data(new_Ls,segment_data.segment_name)
				if(voice.blocked){
					if(voice.voice_id==voice_id){
						//add copy of segment in position
						for (let i = 0; i < repetitions; i++) {
							let segment_data_copy=JSON.parse(JSON.stringify(segment_data))
							voice.data.segment_data.splice(segment_index,0,segment_data_copy)
						}
					}else{
						//add empty segments in position
						for (let i = 0; i < repetitions; i++) {
							let empty_segment_data_copy=JSON.parse(JSON.stringify(empty_segment_data))
							voice.data.segment_data.splice(segment_index+1,0,empty_segment_data_copy)
						}
					}
					//midi
					voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
				}else{
					//add-extend segment in end
					DATA_calculate_force_voice_Li(voice,new_Li_absolute,false)
				}
			})
		}else{
			//voice is unblocked
			//verify if blocked voices can be extended
			let extend_blocked=DATA_verify_blocked_voices_extend_last_segment(data)
			data.voice_data.forEach(voice=>{
				let new_Ls=(segment_Ls_absolte*voice.neopulse.D/voice.neopulse.N)
				let new_segment_data=DATA_calculate_new_segment_data(new_Ls,"")
				if(voice.voice_id==voice_id){
					//add segment in position current voice
					for (let i = 0; i < repetitions; i++) {
						let segment_data_copy=JSON.parse(JSON.stringify(segment_data))
						voice.data.segment_data.splice(segment_index,0,segment_data_copy)
					}
					//midi
					//console.log(voice.data.segment_data)
					voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
				}else{
					//add-extend segment in end
					DATA_calculate_force_voice_Li(voice,new_Li_absolute,extend_blocked)
				}
			})
		}
		//recalculate Li
		data.global_variables.Li=new_Li_absolute
		DATA_insert_new_state_point(data)
		DATA_load_state_point_data(false,true)
	}else{
		//overwrite
		//verify if compatible with max Li
		let sum_Ls_to_segment_rep = 0
		let sum_Ls_from_segment_rep = 0
		current_voice_data.data.segment_data.forEach((segment,index)=>{
			if(index<=segment_index)sum_Ls_to_segment_rep+=segment.time.slice(-1)[0].P *current_voice_data.neopulse.N/current_voice_data.neopulse.D
			if(index>segment_index+repetitions)sum_Ls_from_segment_rep+=segment.time.slice(-1)[0].P *current_voice_data.neopulse.N/current_voice_data.neopulse.D
		})
		let new_Li_absolute=sum_Ls_to_segment_rep+(segment_Ls_absolte*repetitions)+sum_Ls_from_segment_rep
		if(new_Li_absolute>max_Li){
			console.error("ERROR: max Li reached")
			return
		}
		if(current_voice_data.blocked){
			//voice is blocked
			data.voice_data.forEach(voice=>{
				if(voice.voice_id==voice_id){
					//replace/create necessary structure in current voice
					let segment_position=segment_index
					let segment_length = voice.data.segment_data.length
					for (let i = 0; i < repetitions; i++) {
						let segment_data_copy=JSON.parse(JSON.stringify(segment_data))
						segment_position++
						if(segment_position<=segment_length){
							//change segment
							voice.data.segment_data.splice(segment_position,1,segment_data_copy)
						}else{
							//add segment
							voice.data.segment_data.splice(segment_length-1,0,segment_data_copy)
						}
					}
					//midi
					//console.log(voice.data.segment_data)
					voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
				}else{
					if(voice.blocked){
						//cut/expand/create segments un other blocked voices
						let segment_position=segment_index
						let segment_length = voice.data.segment_data.length
						let new_Ls = voice.data.segment_data[segment_position].time.slice(-1)[0].P
						let new_name = voice.data.segment_data[segment_position].segment_name
						for (let i = 0; i < repetitions; i++) {
							segment_position++
							if(segment_position<segment_length){
								//change segment Ls (expand last element if necessary
								var extend_last_element=false
								DATA_calculate_force_segment_data_Ls(voice.data.segment_data[segment_position],new_Ls,extend_last_element)
								//change segment name
								voice.data.segment_data[segment_position].segment_name=new_name
							}else{
								//add empty segment
								let new_segment_data=DATA_calculate_new_segment_data(new_Ls,new_name)
								voice.data.segment_data.splice(segment_length,0,new_segment_data)
							}
						}
						//midi
						voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
					}else{
						//cut/expand unnblocked voices
						DATA_calculate_force_voice_Li(voice,new_Li_absolute,false)
					}
				}
			})
		}else{
			//voice is unblocked
			//verify if last segment blocked voices can be extended
			let extend_blocked=DATA_verify_blocked_voices_extend_last_segment(data)
			data.voice_data.forEach(voice=>{
				if(voice.voice_id==voice_id){
					//replace/create necessary structure in current voice
					let segment_position=segment_index
					let segment_length = voice.data.segment_data.length
					for (let i = 0; i < repetitions; i++) {
						let segment_data_copy=JSON.parse(JSON.stringify(segment_data))
						segment_position++
						if(segment_position<=segment_length){
							//change segment
							voice.data.segment_data.splice(segment_position,1,segment_data_copy)
						}else{
							//add segment
							voice.data.segment_data.splice(segment_length-1,0,segment_data_copy)
						}
					}
					//midi
					//console.log(voice.data.segment_data)
					voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
				}else{
					//cut/expand other voices
					DATA_calculate_force_voice_Li(voice,new_Li_absolute,extend_blocked)
				}
			})
		}
		//recalculate Li
		data.global_variables.Li=new_Li_absolute
		DATA_insert_new_state_point(data)
		DATA_load_state_point_data(false,true)
	}
}

function DATA_repeat_segment_column(segment_index,repetitions,insert){
	let data = DATA_get_current_state_point(true)
	let current_voice_data_list = data.voice_data.filter(item=>{
		return item.blocked
	})
	if(current_voice_data_list.length==0){
		console.error("Error repeating segment, voice index not found")
		return
	}
	if(current_voice_data_list[0].data.segment_data.length<segment_index || segment_index<0){
		console.error("Error repeating segment, segment index not found")
		return
	}
	let segment_Ls_absolte = current_voice_data_list[0].data.segment_data[segment_index].time.slice(-1)[0].P *current_voice_data_list[0].neopulse.N/current_voice_data_list[0].neopulse.D
	if(insert){
		let new_Li_absolute=(segment_Ls_absolte*repetitions+data.global_variables.Li)
		//verify if compatible with max Li
		if(new_Li_absolute>max_Li){
			console.error("ERROR: max Li reached")
			return
		}
		//clone if voice is blocked, add segment end if not
		data.voice_data.forEach(voice=>{
			if(voice.blocked){
				let segment_data=JSON.parse(JSON.stringify(voice.data.segment_data[segment_index]))
				//add copy of segment in position
				for (let i = 0; i < repetitions; i++) {
					let segment_data_copy=JSON.parse(JSON.stringify(segment_data))
					voice.data.segment_data.splice(segment_index,0,segment_data_copy)
				}
				//midi
				voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
			}else{
				//add-extend segment in end
				DATA_calculate_force_voice_Li(voice,new_Li_absolute,false)
			}
		})
		//recalculate Li
		data.global_variables.Li=new_Li_absolute
		DATA_insert_new_state_point(data)
		DATA_load_state_point_data(false,true)
	}else{
		//overwrite
		//verify if compatible with max Li
		let sum_Ls_to_segment_rep = 0
		let sum_Ls_from_segment_rep = 0
		current_voice_data_list[0].data.segment_data.forEach((segment,index)=>{
			if(index<=segment_index)sum_Ls_to_segment_rep+=segment.time.slice(-1)[0].P *current_voice_data_list[0].neopulse.N/current_voice_data_list[0].neopulse.D
			if(index>segment_index+repetitions)sum_Ls_from_segment_rep+=segment.time.slice(-1)[0].P *current_voice_data_list[0].neopulse.N/current_voice_data_list[0].neopulse.D
		})
		let new_Li_absolute=sum_Ls_to_segment_rep+(segment_Ls_absolte*repetitions)+sum_Ls_from_segment_rep
		if(new_Li_absolute>max_Li){
			console.error("ERROR: max Li reached")
			return
		}
		data.voice_data.forEach(voice=>{
			if(voice.blocked){
				//replace/create necessary structure in current voice
				let segment_data=JSON.parse(JSON.stringify(voice.data.segment_data[segment_index]))
				let segment_position=segment_index
				let segment_length = voice.data.segment_data.length
				for (let i = 0; i < repetitions; i++) {
					let segment_data_copy=JSON.parse(JSON.stringify(segment_data))
					segment_position++
					if(segment_position<=segment_length){
						//change segment
						voice.data.segment_data.splice(segment_position,1,segment_data_copy)
					}else{
						//add segment
						voice.data.segment_data.splice(segment_length-1,0,segment_data_copy)
					}
				}
				//midi
				voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
			}else{
				//cut/expand unnblocked voices
				DATA_calculate_force_voice_Li(voice,new_Li_absolute,false)
			}
		})
		//recalculate Li
		data.global_variables.Li=new_Li_absolute
		DATA_insert_new_state_point(data)
		DATA_load_state_point_data(false,true)
	}
}

/////////*****SUMS*****/////////

//iT
function DATA_calculate_sum_iT_segment(segment,value){
	let iT_list=DATA_calculate_iT_list_from_segment_data(segment)
	//mantain Ls, expand contract Fr and elements
	let new_iT_list_raw=iT_list.map(iT=>{return (iT+value>0)?iT+value:0})
	let new_time_list=[]
	let new_sound_list=[]
	let new_fraction=[]
	let current_new_Fr_iT_sum=0
	let current_new_Fr_start=0
	let current_Fr_index=0
	let last_deleted_sound=null
	let new_iT_list=[]
	new_iT_list_raw.forEach((new_iT,index)=>{
		let current_Fr=segment.fraction[current_Fr_index]
		if(segment.time[index].P==current_Fr.stop){
			//add previous fraction stop and eventually a silence if needed
			let current_new_Fr=new_fraction.pop()
			let delta_P = (Math.ceil(current_new_Fr_iT_sum/current_new_Fr.D))*current_new_Fr.N
			current_new_Fr.stop=current_new_Fr.start+delta_P
			new_fraction.push(current_new_Fr)
			let resto = current_new_Fr_iT_sum%current_new_Fr.D
			if(resto!=0){
				//need to add a silence
				new_sound_list.push({note:-1,diesis:true,A_pos:[],A_neg:[]})
				new_iT_list.push(current_new_Fr.D-resto)
			}
			current_Fr_index++
			current_Fr=segment.fraction[current_Fr_index]
			current_new_Fr_iT_sum=0
			current_new_Fr_start=current_new_Fr.stop
		}
		if(new_iT==0){
			//delete element
			//register note and index in case deleting a future L!!!
			if(segment.sound[index].note!=-3)last_deleted_sound=JSON.parse(JSON.stringify(segment.sound[index]))
		}else{
			//add note and diesis
			if(segment.sound[index].note==-3 && last_deleted_sound!=null){
				new_sound_list.push(last_deleted_sound)
			}else{
				new_sound_list.push(segment.sound[index])
			}
			last_deleted_sound=null
			//add iT
			new_iT_list.push(new_iT)
			current_new_Fr_iT_sum+=new_iT
			if(new_fraction.length<=current_Fr_index){
				//add fraction (no stop)
				new_fraction.push({ "N": current_Fr.N, "D": current_Fr.D, "start": current_new_Fr_start, "stop": null })
			}
		}
	})
	//fill stop last fraction
	let current_new_Fr=new_fraction.pop()
	let delta_P = (Math.ceil(current_new_Fr_iT_sum/current_new_Fr.D))*current_new_Fr.N
	current_new_Fr.stop=current_new_Fr.start+delta_P
	new_fraction.push(current_new_Fr)
	let resto = current_new_Fr_iT_sum%current_new_Fr.D
	if(resto!=0){
		//need to add a silence
		new_sound_list.push({note:-1,diesis:true,A_pos:[],A_neg:[]})
		new_iT_list.push(current_new_Fr.D-resto)
	}
	new_time_list = DATA_calculate_time_from_iT(new_iT_list,new_fraction)
	if(new_time_list.length==0){
		console.error("SUM iT error, Fr not compatible")
		success=false
		return null
	}
	let new_segment=JSON.parse(JSON.stringify(segment))
	//end segment
	new_sound_list.push({note:-4,diesis:true,A_pos:[],A_neg:[]})
	new_segment.time=new_time_list
	new_segment.sound=new_sound_list
	new_segment.fraction=new_fraction
	return new_segment
}

function DATA_sum_iT_segment(voice_id,segment_index,value,block_Ls){
	if(voice_id==-1)return
	let data = DATA_get_current_state_point(true)
	let current_voice_data = data.voice_data.find(item=>{return item.voice_id==voice_id})
	if(current_voice_data==null){
		console.error("Operation error, data not found")
		return
	}
	let sum_success=false
	let new_segment=null
	if(current_voice_data.data.segment_data.length>segment_index){
		new_segment=DATA_calculate_sum_iT_segment(current_voice_data.data.segment_data[segment_index],value)
		if(new_segment==null){
			sum_success=false
		}else{
			sum_success=true
		}
	}
	if(!sum_success){
		APP_info_msg("op_not_succes")
		return
	}
	let old_Ls=current_voice_data.data.segment_data[segment_index].time.slice(-1)[0].P
	let new_Ls=new_segment.time.slice(-1)[0].P
	if(block_Ls){
		//mantain Ls of the segment
		let extend_last_element=false
		DATA_calculate_force_segment_data_Ls(new_segment,old_Ls,extend_last_element)
		current_voice_data.data.segment_data[segment_index]=new_segment
		//calculate midi
		current_voice_data.data.midi_data=DATA_segment_data_to_midi_data(current_voice_data.data.segment_data,current_voice_data.neopulse)
	}else{
		//unblock voice if blocked and there are others voices, force Li to every voice
		if(data.voice_data.length>1)current_voice_data.blocked=false
		//verify Li compatibility
		let delta_Ls_min_absolute=DATA_calculate_minimum_Li()
		let N_N=current_voice_data.neopulse.N
		let N_D=current_voice_data.neopulse.D
		let delta_Ls_min=delta_Ls_min_absolute*N_D/N_N
		let compatible=new_Ls%delta_Ls_min
		let delta_Ls=new_Ls-old_Ls
		if(compatible!=0){
			//new Ls not compatible
			if(delta_Ls>0)new_Ls=Math.ceil(new_Ls/(delta_Ls_min))*delta_Ls_min
			if(delta_Ls<0)new_Ls=Math.floor(new_Ls/(delta_Ls_min))*delta_Ls_min
			let extend_last_element=false
			DATA_calculate_force_segment_data_Ls(new_segment,new_Ls,extend_last_element)
			delta_Ls=new_Ls-old_Ls
		}
		current_voice_data.data.segment_data[segment_index]=new_segment
		//calculate midi
		current_voice_data.data.midi_data=DATA_segment_data_to_midi_data(current_voice_data.data.segment_data,current_voice_data.neopulse)
		let new_Li_absolute=data.global_variables.Li+(delta_Ls*N_N/N_D)
		if(delta_Ls<0){
			if(data.voice_data.length>1){
				//add a segment at the end of current voice and complete Li
				//extend_blocked is irrelevant
				DATA_calculate_force_voice_Li(current_voice_data,data.global_variables.Li,false)//make its own midi calc
			}else{
				data.global_variables.Li=new_Li_absolute
			}
		}
		if(delta_Ls>0){
			if(new_Li_absolute>max_Li){
				console.error("ERROR: max Li reached")
				sum_success=false
				return
			}
			//add a segment to the end of every other voice
			//verify if last segment blocked voices can be extended
			let extend_blocked=DATA_verify_blocked_voices_extend_last_segment(data)
			data.voice_data.forEach(voice=>{
				if(voice.voice_id!=voice_id){
					DATA_calculate_force_voice_Li(voice,new_Li_absolute,extend_blocked)//make its own midi calc
				}
			})
			data.global_variables.Li=new_Li_absolute
		}
	}
	if(!sum_success){
		APP_info_msg("op_not_succes")
		return
	}
	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_sum_iT_segment_column(segment_index,value,block_Ls){
	if(segment_index==-1)return
	let data = DATA_get_current_state_point(true)
	let find_data=false
	let sum_success=false
	let success=[]
	let new_segment_column_list=[]
	let neopulse_column_list=[]
	data.voice_data.forEach(voice=>{
		if(voice.blocked){
			find_data=true
			if(voice.data.segment_data.length<=segment_index){
				find_data=false
				return
			}
			let new_segment=DATA_calculate_sum_iT_segment(voice.data.segment_data[segment_index],value)
			if(new_segment==null){
				success.push(false)
				return
			}else{
				success.push(true)
				new_segment_column_list.push(new_segment)
				neopulse_column_list.push(voice.neopulse)
			}
		}
	})
	if(!find_data){
		console.error("Operation error, data not found")
		return
	}
	sum_success=!success.some(item=>{return !item})
	if(!sum_success){
		APP_info_msg("op_not_succes")
		return
	}
	if(block_Ls){
		//mantain Ls of the segments
		let index=0
		data.voice_data.forEach(voice=>{
			if(voice.blocked){
				let old_Ls=voice.data.segment_data[segment_index].time.slice(-1)[0].P
				let extend_last_element=false
				DATA_calculate_force_segment_data_Ls(new_segment_column_list[index],old_Ls,extend_last_element)
				voice.data.segment_data[segment_index]=new_segment_column_list[index]
				//calculate midi
				voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
				index++;
			}
		})
	}else{
		//look at the bigger segment length, adjust every segment of the column to the bigger one, add empty segment at the end of voices (eventually)
		let absolute_Ls_column=[0]
		let delta_Ls_min_absolute=DATA_calculate_minimum_Li()
		new_segment_column_list.forEach((new_segment,index)=>{
			//verify Li compatibility
			let new_Ls=new_segment.time.slice(-1)[0].P
			let N_N=neopulse_column_list[index].N
			let N_D=neopulse_column_list[index].D
			let delta_Ls_min=delta_Ls_min_absolute*N_D/N_N
			let compatible=new_Ls%delta_Ls_min
			if(compatible!=0){
				//new Ls not compatible
				if(value>0)new_Ls=Math.ceil(new_Ls/(delta_Ls_min))*delta_Ls_min
				if(value<0)new_Ls=Math.floor(new_Ls/(delta_Ls_min))*delta_Ls_min
			}
			absolute_Ls_column.push(new_Ls*N_N/N_D)
		})
		let max_Ls_absolute=Math.max(...absolute_Ls_column)
		if(max_Ls_absolute==0){
			APP_info_msg("op_not_succes")
			return
		}
		new_segment_column_list.forEach((new_segment,index)=>{
			let N_N=neopulse_column_list[index].N
			let N_D=neopulse_column_list[index].D
			let new_Ls = max_Ls_absolute*N_D/N_N
			let extend_last_element=false
			DATA_calculate_force_segment_data_Ls(new_segment,new_Ls,extend_last_element)
		})
		let old_Ls_absolute=0
		data.voice_data.find(voice=>{
			if(voice.blocked){
				let old_Ls=voice.data.segment_data[segment_index].time.slice(-1)[0].P
				old_Ls_absolute=old_Ls*voice.neopulse.N/voice.neopulse.D
				return true
			}
		})
		if(old_Ls_absolute==0)return
		let delta_Ls_absolute=max_Ls_absolute-old_Ls_absolute
		let new_Li_absolute=data.global_variables.Li+delta_Ls_absolute
		//console.log(max_Ls_absolute+"-"+old_Ls_absolute+"="+delta_Ls_absolute)
		if(delta_Ls_absolute<0){
			//if there are only blocked voices do nothing
			let unblocked=false
			let index=0
			data.voice_data.forEach(voice=>{
				if(voice.blocked){
					//substituite segment
					voice.data.segment_data[segment_index]=new_segment_column_list[index]
					//calculate midi
					voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
					index++;
				}
			})
			if(unblocked){
				//add a segment at the end of blocked voices and complete Li
				//extend_blocked is relevant BC add at the end of blocked voices
				let extend_blocked=DATA_verify_blocked_voices_extend_last_segment(data)
				data.voice_data.forEach(voice=>{
					if(voice.blocked){
						DATA_calculate_force_voice_Li(voice,data.global_variables.Li,extend_blocked)//make its own midi calc
					}
				})
			}else{
				//only blocked voices, change Li
				data.global_variables.Li=new_Li_absolute
			}
		}
		if(delta_Ls_absolute>0){
			if(new_Li_absolute>max_Li){
				console.error("ERROR: max Li reached")
				sum_success=false
				return
			}
			//extend_blocked is irrelevant
			// var extend_blocked=DATA_verify_blocked_voices_extend_last_segment(data)
			let index=0
			data.voice_data.forEach(voice=>{
				if(voice.blocked){
					//substituite segment
					voice.data.segment_data[segment_index]=new_segment_column_list[index]
					//calculate midi
					voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
					index++;
				}else{
					//add a segment to the end of every other voice
					DATA_calculate_force_voice_Li(voice,new_Li_absolute,false)//make its own midi calc
					//DATA_calculate_force_voice_Li(voice,new_Li_absolute,extend_blocked)
				}
			})
			data.global_variables.Li=new_Li_absolute
		}
	}
	if(!sum_success){
		APP_info_msg("op_not_succes")
		return
	}
	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_sum_iT_voice(voice_id,value,block_Ls){
	if(voice_id==-1)return
	let data = DATA_get_current_state_point(true)
	let current_voice_data = data.voice_data.find(item=>{return item.voice_id==voice_id})
	if(current_voice_data==null){
		console.error("Operation error, data not found")
		return
	}
	let sum_success=true
	let new_segment_list=[]
	current_voice_data.data.segment_data.forEach((segment)=>{
		new_segment=DATA_calculate_sum_iT_segment(segment,value)
		if(new_segment==null){
			sum_success=false
		}
		//if at least one is ok
		new_segment_list.push(new_segment)
	})
	if(!sum_success){
		APP_info_msg("op_not_succes")
		return
	}
	//
	if(block_Ls){
		//mantain Ls of the segments
		current_voice_data.data.segment_data.forEach((segment,segment_index)=>{
			let old_Ls=segment.time.slice(-1)[0].P
			let new_Ls=new_segment_list[segment_index].time.slice(-1)[0].P
			let extend_last_element=false
			DATA_calculate_force_segment_data_Ls(new_segment_list[segment_index],old_Ls,extend_last_element)
			current_voice_data.data.segment_data[segment_index]=new_segment_list[segment_index]
		})
		//calculate midi
		current_voice_data.data.midi_data=DATA_segment_data_to_midi_data(current_voice_data.data.segment_data,current_voice_data.neopulse)
	}else{
		//unblock voice if blocked and there are others voices, force Li to every voice
		if(data.voice_data.length>1)current_voice_data.blocked=false
		//verify Li compatibility for every segment
		let delta_Ls_min_absolute=DATA_calculate_minimum_Li()
		let N_N=current_voice_data.neopulse.N
		let N_D=current_voice_data.neopulse.D
		let delta_Ls_min=delta_Ls_min_absolute*N_D/N_N
		new_segment_list.forEach((new_segment,segment_index)=>{
			let old_Ls=current_voice_data.data.segment_data[segment_index].time.slice(-1)[0].P
			let new_Ls=new_segment.time.slice(-1)[0].P
			let compatible=new_Ls%delta_Ls_min
			let delta_Ls=new_Ls-old_Ls
			if(compatible!=0){
				//new Ls not compatible
				if(delta_Ls>0)new_Ls=Math.ceil(new_Ls/(delta_Ls_min))*delta_Ls_min
				if(delta_Ls<0)new_Ls=Math.floor(new_Ls/(delta_Ls_min))*delta_Ls_min
				var extend_last_element=false
				DATA_calculate_force_segment_data_Ls(new_segment,new_Ls,extend_last_element)
				delta_Ls=new_Ls-old_Ls
			}
			current_voice_data.data.segment_data[segment_index]=new_segment
		})
		//calculate new Li
		let new_Li_absolute=(current_voice_data.data.segment_data.reduce((prop,segment)=>{return segment.time.slice(-1)[0].P+prop},0))*N_N/N_D
		if(new_Li_absolute<data.global_variables.Li){
			if(data.voice_data.length>1){
				//add a segment at the end of current voice and complete Li
				//extend_blocked is irrelevant
				DATA_calculate_force_voice_Li(current_voice_data,data.global_variables.Li,false)//make its own midi calc
			}else{
				//calculate midi
				current_voice_data.data.midi_data=DATA_segment_data_to_midi_data(current_voice_data.data.segment_data,current_voice_data.neopulse)
				data.global_variables.Li=new_Li_absolute
			}
		}
		if(new_Li_absolute>data.global_variables.Li){
			//verify if it is > max Li
			if(new_Li_absolute>max_Li){
				console.error("ERROR: max Li reached")
				sum_success=false
				return
			}
			//calculate midi
			current_voice_data.data.midi_data=DATA_segment_data_to_midi_data(current_voice_data.data.segment_data,current_voice_data.neopulse)
			//add a segment to the end of every other voice
			//verify if last segment blocked voices can be extended
			let extend_blocked=DATA_verify_blocked_voices_extend_last_segment(data)
			data.voice_data.forEach(voice=>{
				if(voice.voice_id!=voice_id){
					DATA_calculate_force_voice_Li(voice,new_Li_absolute,extend_blocked)//make its own midi calc
				}
			})
			data.global_variables.Li=new_Li_absolute
		}
	}
	if(!sum_success){
		APP_info_msg("op_not_succes")
		return
	}
	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

//N
//calc Na
function DATA_calculate_sum_Na_segment(segment,value){
	let max_note = TET*N_reg -1
	let outside_range=false
	let sum_success=false
	segment.sound.forEach((sound,sound_index)=>{
		if(sound.note>=0){
			sum_success=true
			sound.note+=value
			if(sound.note>max_note){
				sound.note=max_note
				outside_range=true
			}
			if(sound.note<0){
				sound.note=0
				outside_range=true
			}
			let result_A = DATA_verify_sound_A(sound)
			if(result_A) outside_range=true
			//segment.note[note_index]=note
		}
	})
	return [sum_success,outside_range]
}

//calc Ngm
function DATA_calculate_sum_Ngm_segment(segment,value,Psg_segment_start,neopulse,scale_sequence){
	let [N_NP,D_NP]=[neopulse.N,neopulse.D]
	let outside_range=false
	let sum_success=false
	let idea_scale_list=DATA_get_idea_scale_list()
	segment.time.forEach((time,index)=>{
		let current_note=segment.sound[index].note
		if(current_note>=0){
			//calculating current fraction
			let fraction = segment.fraction.find(fraction=>{return fraction.stop>time.P})
			if (typeof(fraction) == "undefined"){
				return
			}
			//calculating position
			let frac_p = fraction.N/fraction.D
			let Pa_equivalent = (Psg_segment_start+time.P) * N_NP/D_NP + time.F* frac_p * N_NP/D_NP
			//element inside a scale module
			let current_scale = DATA_get_Pa_eq_scale(Pa_equivalent,scale_sequence)
			let [grade,delta,reg]=DATA_calculate_absolute_note_to_scale(current_note,segment.sound[index].diesis,current_scale,idea_scale_list)
			//do the sum
			grade+=(value.grade*value.positive)
			//delta not in use
			reg+=(value.reg*value.positive)
			let result = DATA_calculate_scale_note_to_absolute(grade,delta,reg,current_scale,idea_scale_list)
			if(result.outside_range)outside_range=true
			//A
			let new_A = DATA_calculate_A_from_sound_grade_change(segment.sound[index],result.note,null,current_scale,null,idea_scale_list)
			sum_success=true
			segment.sound[index].note=result.note
			segment.sound[index].A_pos=new_A.A_pos
			segment.sound[index].A_neg=new_A.A_neg
			let result_A = DATA_verify_sound_A(segment.sound[index])
			if(result_A) outside_range=true
		}
	})
	return [sum_success,outside_range]
}

function DATA_sum_N_segment(voice_id,segment_index,value,type_N){
	if(voice_id==-1 || segment_index==-1)return
	let data = DATA_get_current_state_point(true)
	let current_voice_data = data.voice_data.find(item=>{return item.voice_id==voice_id})
	if(current_voice_data==null){
		console.error("Operation error, data not found")
		return
	}
	if(current_voice_data.data.segment_data.length<=segment_index){
		console.error("Operation error, data not found")
		return
	}
	let sum_success=false
	let outside_range=false
	if(type_N=="Na")[sum_success,outside_range]=DATA_calculate_sum_Na_segment(current_voice_data.data.segment_data[segment_index],value)
	if(type_N=="Ngm"){
		let Psg_segment_start=0
		for (let i = 0; i < segment_index; i++) {
			Psg_segment_start+=current_voice_data.data.segment_data[i].time.slice(-1)[0].P
		}
		let scale_sequence= data.scale.scale_sequence;
		[sum_success,outside_range]=DATA_calculate_sum_Ngm_segment(current_voice_data.data.segment_data[segment_index],value,Psg_segment_start,current_voice_data.neopulse,scale_sequence)
	}
	if(!sum_success){
		APP_info_msg("op_not_succes")
		return
	}
	if(outside_range){
		APP_info_msg("out_N_range")
	}
	current_voice_data.data.midi_data= DATA_segment_data_to_midi_data(current_voice_data.data.segment_data,current_voice_data.neopulse)
	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_sum_N_segment_column(segment_index,value,type_N){
	if(segment_index==-1)return
	let data = DATA_get_current_state_point(true)
	let find_data=false
	let outside_range=false
	let sum_success=false
	data.voice_data.forEach(voice=>{
		if(voice.blocked){
			find_data=true
			if(voice.data.segment_data.length<=segment_index){
				find_data=false
				return
			}
			let success=false
			let out_r=false
			if(type_N=="Na")[success,out_r]=DATA_calculate_sum_Na_segment(voice.data.segment_data[segment_index],value)
			if(type_N=="Ngm"){
				if(value!=null){
					let Psg_segment_start=0
					for (let i = 0; i < segment_index; i++) {
						Psg_segment_start+=voice.data.segment_data[i].time.slice(-1)[0].P
					}
					let scale_sequence= data.scale.scale_sequence;
					[success,out_r]=DATA_calculate_sum_Ngm_segment(voice.data.segment_data[segment_index],value,Psg_segment_start,voice.neopulse,scale_sequence)
				}
			}
			if(!success){
				return
			}
			if(out_r)outside_range=true
			sum_success=true
			//calculate midi
			voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
		}
	})
	if(!find_data){
		console.error("Operation error, data not found")
		return
	}
	if(!sum_success){
		APP_info_msg("op_not_succes")
		return
	}
	if(outside_range){
		APP_info_msg("out_N_range")
	}
	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_sum_N_voice(voice_id,value,type_N){
	if(voice_id==-1)return
	let data = DATA_get_current_state_point(true)
	let current_voice_data = data.voice_data.find(item=>{return item.voice_id==voice_id})
	if(current_voice_data==null){
		console.error("Operation error, data not found")
		return
	}
	let max_note = TET*N_reg -1
	let outside_range=false
	let sum_success=false
	let Psg_segment_start=0
	let scale_sequence= data.scale.scale_sequence
	current_voice_data.data.segment_data.forEach(segment=>{
		let success=false
		let out_r=false
		if(type_N=="Na")[success,out_r]=DATA_calculate_sum_Na_segment(segment,value)
		if(type_N=="Ngm"){
			if(value!=null){
				[success,out_r]=DATA_calculate_sum_Ngm_segment(segment,value,Psg_segment_start,current_voice_data.neopulse,scale_sequence)
			}
		}
		Psg_segment_start+=segment.time.slice(-1)[0].P
		if(!success){
			return
		}
		if(out_r)outside_range=true
		sum_success=true
	})
	if(!sum_success){
		APP_info_msg("op_not_succes")
		return
	}
	if(outside_range){
		APP_info_msg("out_N_range")
	}
	current_voice_data.data.midi_data= DATA_segment_data_to_midi_data(current_voice_data.data.segment_data,current_voice_data.neopulse)
	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

//iNa
function DATA_calculate_sum_iNa_segment(segment,value,type_sum){
	let max_note = TET*N_reg -1
	let outside_range=false
	let sum_success=false
	let note_array_copy = JSON.parse(JSON.stringify(segment.sound.map(s=>{return s.note})))
	let iNa_list=DATA_calculate_iNa_from_Na_list(note_array_copy)
	note_array_copy = []
	let current_note=0
	//process iNa_list
	let is_starting_note=true
	iNa_list.forEach((current_iNa,current_iNa_index)=>{
		if(!isNaN(current_iNa)){
			if(is_starting_note){
				is_starting_note=false
				current_note= current_iNa
				note_array_copy.push(current_iNa)
			}else{
				//new iNa
				sum_success=true
				let provv = current_note
				switch (type_sum) {
					case "sum":
						//pure sum of current value
						//iNa_list[current_iNa_index]=current_iNa+value
						provv += current_iNa+value
					break;
					case "abs":
						//mantain original iNa sign and sum absolute value (expansion or contraction)
						provv += Math.sign(current_iNa)*(Math.abs(current_iNa)+value)
					break;
				}
				current_note = DATA_calculate_inRange(provv,0,max_note)[0]
				if(provv!=current_note)outside_range=true
				note_array_copy.push(current_note)
			}
		}else{
			if(current_iNa=="s"){
				note_array_copy.push(-1)
			}else if(current_iNa=="e"){
				note_array_copy.push(-2)
			}else if(current_iNa==tie_intervals){
				note_array_copy.push((current_iNa_index==0)?-2:-3)
			}else if(current_iNa==end_range){
				note_array_copy.push((current_iNa_index==iNa_list.length-1)?-4:-3)
			}
		}
	})
	if(sum_success){
		note_array_copy.forEach((note,i)=>{
			segment.sound[i].note=note
			let result_A = DATA_verify_sound_A(segment.sound[i])
			if(result_A) outside_range=true
		})
	}
	return [sum_success,outside_range]
}

//iNgm
function DATA_calculate_sum_iNgm_segment(segment,value,type_sum,Psg_segment_start,neopulse,scale_sequence){
	let [N_NP,D_NP]=[neopulse.N,neopulse.D]
	let all_scale_length_value = scale_sequence.reduce((p,scale)=>{return parseInt(scale.duration)+p},0)
	if(all_scale_length_value==0){
		return false
	}
	let outside_range=false
	let sum_success=false
	let sound_grade_list=[]
	let scale_list=[]
	let max_index=0
	let idea_scale_list=DATA_get_idea_scale_list()
	segment.sound.forEach((sound,index)=>{
		//traduction
		let time = segment.time[index]
		let fraction = segment.fraction.find(fraction=>{return fraction.stop>time.P})
		if (typeof(fraction) == "undefined"){
			//end segment
			return true
		}
		//calculating position
		let frac_p = fraction.N/fraction.D
		let Pa_equivalent = (Psg_segment_start+time.P) * N_NP/D_NP + time.F* frac_p * N_NP/D_NP
		let current_scale = DATA_get_Pa_eq_scale(Pa_equivalent,scale_sequence)
		if(sound.note<0){
			sound_grade_list.push({note:sound.note,diesis:sound.diesis,A_pos:[],A_neg:[]})
		}else{
			let [grade,delta,reg]=DATA_calculate_absolute_note_to_scale(sound.note,sound.diesis,current_scale,idea_scale_list)
			sound_grade_list.push({note:sound.note,grade:grade,delta:delta,reg:reg,diesis:sound.diesis,A_pos:sound.A_pos,A_neg:sound.A_neg})
		}
		//handy store the calculated scale for later calculations
		scale_list.push(current_scale)
		max_index++
	})
	//calc iN
	let i_sound_grade_list=DATA_calculate_iNgm_from_Ngm_list(sound_grade_list,scale_list)
	//not note: {note:Na,diesis:diesis})//A included
	//first note: {note:Na,grade:grade,delta:delta,reg:reg,diesis:diesis}
	//d_grad : {note:Na,iNgm:d_grad,diesis:diesis}
	let new_note_array = []
	let current_sound_grade=0
	//process iNa_list
	let is_starting_note=true
	i_sound_grade_list.forEach((i_sound_grade,i_sound_grade_index)=>{
		if(i_sound_grade.note>-1){
			if(is_starting_note){
				is_starting_note=false
				current_sound_grade= i_sound_grade
				new_note_array.push({note:i_sound_grade.note,scale_index:i_sound_grade_index})
			}else{
				//new iNa
				sum_success=true
				let grade = current_sound_grade.grade
				let delta = 0//current_sound_grade.diesis
				let reg = current_sound_grade.reg
				switch (type_sum) {
					case "sum":
						//pure sum of current value
						grade+=i_sound_grade.iNgm+(value.grade*value.positive)
						reg+=(value.reg*value.positive)
					break;
					case "abs":
						//mantain original iNa sign and sum absolute value (expansion or contraction)
						//provv += Math.sign(current_iNa)*(Math.abs(current_iNa)+value)
						grade+=Math.sign(i_sound_grade.iNgm)*(Math.abs(i_sound_grade.iNgm)+value.grade*value.positive)
						reg+=Math.sign(i_sound_grade.iNgm)*(value.reg*value.positive)
					break;
				}
				//traduction back
				let result = DATA_calculate_scale_note_to_absolute(grade,0,reg,scale_list[i_sound_grade_index],idea_scale_list)
				let new_Na=result.note
				//control max value!!
				if(result.outside_range){
					//recalculate current note grade
					outside_range=true;
					[grade,delta,reg]=DATA_calculate_absolute_note_to_scale(new_Na,i_sound_grade.diesis,scale_list[i_sound_grade_index],idea_scale_list)
					current_sound_grade.grade=grade
					//current_sound_grade.delta=0
					reg = current_sound_grade.reg
				}else{
					current_sound_grade.grade=grade
					//current_sound_grade.diesis = 0
					reg = current_sound_grade.reg
				}
				new_note_array.push({note:new_Na,scale_index:i_sound_grade_index})
			}
		}else{
			new_note_array.push({note:i_sound_grade.note,scale_index:i_sound_grade_index})
		}
	})
	if(sum_success){
		//write inside segment
		new_note_array.forEach((new_note,index)=>{
			//A
			let new_A = DATA_calculate_A_from_sound_grade_change(segment.sound[index],new_note.note,null,scale_list[new_note.scale_index],null,idea_scale_list)
			segment.sound[index].A_pos=new_A.A_pos
			segment.sound[index].A_neg=new_A.A_neg
			segment.sound[index].note=new_note.note
			//segment.diesis[index] doesn't change
			let result_A = DATA_verify_sound_A(segment.sound[index])
			if(result_A) outside_range=true
		})
	}
	return [sum_success,outside_range]
}

function DATA_sum_iN_segment(voice_id,segment_index,value,type_sum,type_N){
	if(voice_id==-1 || segment_index==-1)return
	let data = DATA_get_current_state_point(true)
	let current_voice_data = data.voice_data.find(item=>{return item.voice_id==voice_id})
	if(current_voice_data==null){
		console.error("Operation error, data not found")
		return
	}
	if(current_voice_data.data.segment_data.length<=segment_index){
		console.error("Operation error, data not found")
		return
	}
	let sum_success=false
	let outside_range=false
	if(type_N=="Na")[sum_success,outside_range]=DATA_calculate_sum_iNa_segment(current_voice_data.data.segment_data[segment_index],value,type_sum)
	if(type_N=="Ngm"){
		let Psg_segment_start=0
		for (let i = 0; i < segment_index; i++) {
			Psg_segment_start+=current_voice_data.data.segment_data[i].time.slice(-1)[0].P
		}
		let scale_sequence= data.scale.scale_sequence;
		[sum_success,outside_range]=DATA_calculate_sum_iNgm_segment(current_voice_data.data.segment_data[segment_index],value,type_sum,Psg_segment_start,current_voice_data.neopulse,scale_sequence)
	}
	if(!sum_success){
		APP_info_msg("op_not_succes")
		return
	}
	if(outside_range){
		APP_info_msg("out_N_range")
	}
	current_voice_data.data.midi_data=DATA_segment_data_to_midi_data(current_voice_data.data.segment_data,current_voice_data.neopulse)
	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_sum_iN_segment_column(segment_index,value,type_sum,type_N){
	if(segment_index==-1)return
	let data = DATA_get_current_state_point(true)
	let find_data=false
	let outside_range=false
	let sum_success=false
	let max_note = TET*N_reg -1
	data.voice_data.forEach(voice=>{
		if(voice.blocked){
			find_data=true
			if(voice.data.segment_data.length<=segment_index){
				find_data=false
				return
			}
			let success=false
			let out_r=false
			if(type_N=="Na")[success,out_r]=DATA_calculate_sum_iNa_segment(voice.data.segment_data[segment_index],value,type_sum)
			if(type_N=="Ngm"){
				let Psg_segment_start=0
				for (let i = 0; i < segment_index; i++) {
					Psg_segment_start+=voice.data.segment_data[i].time.slice(-1)[0].P
				}
				let scale_sequence= data.scale.scale_sequence;
				[success,out_r]=DATA_calculate_sum_iNgm_segment(voice.data.segment_data[segment_index],value,type_sum,Psg_segment_start,voice.neopulse,scale_sequence)
			}
			if(!success){
				return
			}
			if(out_r)outside_range=true
			sum_success=true
			//calculate midi
			voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
		}
	})
	if(!find_data){
		console.error("Operation error, data not found")
		return
	}
	if(!sum_success){
		APP_info_msg("op_not_succes")
		return
	}
	if(outside_range){
		APP_info_msg("out_N_range")
	}
	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_sum_iN_voice(voice_id,value,type_sum,type_N){
	if(voice_id==-1)return
	let data = DATA_get_current_state_point(true)
	let current_voice_data = data.voice_data.find(item=>{return item.voice_id==voice_id})
	if(current_voice_data==null){
		console.error("Operation error, data not found")
		return
	}
	let max_note = TET*N_reg -1
	let outside_range=false
	let sum_success=false
	let scale_sequence= data.scale.scale_sequence
	let Psg_segment_start=0
	current_voice_data.data.segment_data.forEach(segment=>{
		let success=false
		let out_r=false
		if(type_N=="Na")[success,out_r]=DATA_calculate_sum_iNa_segment(segment,value,type_sum)
		if(type_N=="Ngm"){
			[success,out_r]=DATA_calculate_sum_iNgm_segment(segment,value,type_sum,Psg_segment_start,current_voice_data.neopulse,scale_sequence)
		}
		Psg_segment_start+=segment.time.slice(-1)[0].P
		if(!success){
			return
		}
		if(out_r)outside_range=true
		sum_success=true
	})
	if(!sum_success){
		APP_info_msg("op_not_succes")
		return
	}
	if(outside_range){
		APP_info_msg("out_N_range")
	}
	current_voice_data.data.midi_data=DATA_segment_data_to_midi_data(current_voice_data.data.segment_data,current_voice_data.neopulse)
	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

/////////*****ROTATIONS*****/////////

//iT and N
function DATA_calculate_rotate_element_segment(segment,value){
	let success=false
	let error=false
	//rotate L, is bounded to iT, always rotate
	let iT_list=DATA_calculate_iT_list_from_segment_data(segment)
	let new_time_list=[]
	let new_iT_list=[]
	let new_sound_list=[]
	segment.fraction.forEach(range=>{
		//determine how many elements are in the range
		let index_start=0
		let index_stop=0
		let found_start=false
		let found_stop=segment.time.some((time,index)=>{
			if(time.P==range.start && time.F==0){
				index_start=index
				found_start=true
			}
			if(time.P==range.stop && time.F==0){
				index_stop=index
				return true
			}
		})
		//rotate iTs
		if(found_start&&found_stop){
			let iT_range_list = iT_list.slice(index_start,index_stop)
			let sound_range_list = segment.sound.slice(index_start,index_stop)
			//ROTATING to the exact value and eventually L IF start == 0 (start of a segment)
			let n_iT_range=index_stop-index_start
			let true_rotation_value=value%n_iT_range
			if(index_start==0){
				//verify new first element is not a L
				sound_range_list.some((sound,index)=>{
					if(index==true_rotation_value){
						if(sound.note==-3){
							true_rotation_value++
						}else{
							return true
						}
					}
				})
				true_rotation_value=true_rotation_value%n_iT_range
			}
			if(true_rotation_value==0){
				//nothing to rotate
				new_iT_list.push(...iT_range_list)
				new_sound_list.push(...sound_range_list)
			}else{
				new_iT_list.push(...iT_range_list.slice(true_rotation_value))
				new_iT_list.push(...iT_range_list.slice(0,true_rotation_value))
				new_sound_list.push(...sound_range_list.slice(true_rotation_value))
				new_sound_list.push(...sound_range_list.slice(0,true_rotation_value))
				success=true
			}
		}else{
			error=true
		}
	})
	if(error)success=false
	if(success){
		//test errors
		new_time_list = DATA_calculate_time_from_iT(new_iT_list,segment.fraction)
		if(new_time_list.length==0){
			console.error("ROTATING iT error, Fr not compatible")
			success=false
		}
	}
	if(success){
		segment.sound=new_sound_list
		segment.sound.push({note:-4,diesis:true,A_pos:[],A_neg:[]})
		segment.time=new_time_list
	}
	return success
}

function DATA_rotate_element_segment(voice_id,segment_index,value){
	if(voice_id==-1)return
	let data = DATA_get_current_state_point(true)
	let current_voice_data = data.voice_data.find(item=>{return item.voice_id==voice_id})
	if(current_voice_data==null){
		console.error("Operation error, data not found")
		return
	}
	let rot_success=false
	current_voice_data.data.segment_data.find((segment,s_index)=>{
		if(segment_index==s_index){
			rot_success=DATA_calculate_rotate_element_segment(segment,value)
			return true
		}else{
			return false
		}
	})
	if(!rot_success){
		APP_info_msg("op_not_succes")
		return
	}
	//calculate midi
	current_voice_data.data.midi_data=DATA_segment_data_to_midi_data(current_voice_data.data.segment_data,current_voice_data.neopulse)
	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_rotate_element_segment_column(segment_index,value){
	if(segment_index==-1)return
	let data = DATA_get_current_state_point(true)
	let find_data=false
	let rot_success=false
	data.voice_data.forEach(voice=>{
		if(voice.blocked){
			find_data=true
			if(voice.data.segment_data.length<=segment_index){
				find_data=false
				return
			}
			let success = DATA_calculate_rotate_element_segment(voice.data.segment_data[segment_index],value)
			if(!success){
				return
			}
			rot_success=true
			//calculate midi
			voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
		}
	})
	if(!find_data){
		console.error("Operation error, data not found")
		return
	}
	if(!rot_success){
		APP_info_msg("op_not_succes")
		return
	}
	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_rotate_element_voice(voice_id,value){
	if(voice_id==-1)return
	let data = DATA_get_current_state_point(true)
	let current_voice_data = data.voice_data.find(item=>{return item.voice_id==voice_id})
	if(current_voice_data==null){
		console.error("Operation error, data not found")
		return
	}
	let rot_success=false
	current_voice_data.data.segment_data.forEach((segment,segment_index)=>{
		let success=DATA_calculate_rotate_element_segment(segment,value)
		//if at least one is ok
		if(success)rot_success=true
	})
	if(!rot_success){
		APP_info_msg("op_not_succes")
		return
	}
	//calculate midi
	current_voice_data.data.midi_data=DATA_segment_data_to_midi_data(current_voice_data.data.segment_data,current_voice_data.neopulse)
	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

//iT
function DATA_calculate_rotate_iT_segment(segment,value){
	let success=false
	let error=false
	//rotate L, is bounded to iT, always rotate
	let iT_list=DATA_calculate_iT_list_from_segment_data(segment)
	let new_time_list=[]
	let new_iT_list=[]
	let new_sound_list=[]
	segment.fraction.forEach(range=>{
		//determine how many elements are in the range
		let index_start=0
		let index_stop=0
		let found_start=false
		let found_stop=segment.time.some((time,index)=>{
			if(time.P==range.start && time.F==0){
				index_start=index
				found_start=true
			}
			if(time.P==range.stop && time.F==0){
				index_stop=index
				return true
			}
		})
		//rotate iTs
		if(found_start&&found_stop){
			let iT_range_list = iT_list.slice(index_start,index_stop)
			let sound_range_list = segment.sound.slice(index_start,index_stop)
			//ROTATING to the exact value and eventually L IF start == 0 (start of a segment)
			let n_iT_range=index_stop-index_start
			let true_rotation_value=value%n_iT_range
			if(index_start==0){
				//verify new first element is not a L
				sound_range_list.some((sound,index)=>{
					if(index==true_rotation_value){
						if(sound.note==-3){
							true_rotation_value++
						}else{
							return true
						}
					}
				})
				true_rotation_value=true_rotation_value%n_iT_range
			}
			if(true_rotation_value==0){
				//nothing to rotate
				new_iT_list.push(...iT_range_list)
				new_sound_list.push(...sound_range_list)
			}else{
				new_iT_list.push(...iT_range_list.slice(true_rotation_value))
				new_iT_list.push(...iT_range_list.slice(0,true_rotation_value))
				//rotate only L, not note
				let new_sound_range_list=[]
				new_sound_range_list.push(...sound_range_list.slice(true_rotation_value))
				new_sound_range_list.push(...sound_range_list.slice(0,true_rotation_value))
				//overwrite with old note/diesis sequence
				let old_sound_sequence=[]
				sound_range_list.forEach(sound=>{
					if(sound.note!=-3){
						old_sound_sequence.push(sound)
					}
				})
				new_sound_range_list.forEach((sound,index)=>{
					if(sound.note!=-3){
						//substitute with old value
						new_sound_range_list[index]=old_sound_sequence.shift()
					}
				})
				new_sound_list.push(...new_sound_range_list)
				success=true
			}
		}else{
			error=true
		}
	})
	if(error)success=false
	if(success){
		//test errors
		new_time_list = DATA_calculate_time_from_iT(new_iT_list,segment.fraction)
		if(new_time_list.length==0){
			console.error("ROTATING iT error, Fr not compatible")
			success=false
		}
	}
	if(success){
		segment.sound=new_sound_list
		segment.sound.push({note:-4,diesis:true,A_pos:[],A_neg:[]})
		segment.time=new_time_list
	}
	return success
}

function DATA_rotate_iT_segment(voice_id,segment_index,value){
	if(voice_id==-1)return
	let data = DATA_get_current_state_point(true)
	let current_voice_data = data.voice_data.find(item=>{return item.voice_id==voice_id})
	if(current_voice_data==null){
		console.error("Operation error, data not found")
		return
	}
	let rot_success=false
	current_voice_data.data.segment_data.find((segment,s_index)=>{
		if(segment_index==s_index){
			rot_success=DATA_calculate_rotate_iT_segment(segment,value)
			return true
		}else{
			return false
		}
	})
	if(!rot_success){
		APP_info_msg("op_not_succes")
		return
	}
	//calculate midi
	current_voice_data.data.midi_data=DATA_segment_data_to_midi_data(current_voice_data.data.segment_data,current_voice_data.neopulse)
	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_rotate_iT_segment_column(segment_index,value){
	if(segment_index==-1)return
	let data = DATA_get_current_state_point(true)
	let find_data=false
	let rot_success=false
	data.voice_data.forEach(voice=>{
		if(voice.blocked){
			find_data=true
			if(voice.data.segment_data.length<=segment_index){
				find_data=false
				return
			}
			let success = DATA_calculate_rotate_iT_segment(voice.data.segment_data[segment_index],value)
			if(!success){
				return
			}
			rot_success=true
			//calculate midi
			voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
		}
	})
	if(!find_data){
		console.error("Operation error, data not found")
		return
	}
	if(!rot_success){
		APP_info_msg("op_not_succes")
		return
	}
	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_rotate_iT_voice(voice_id,value){
	if(voice_id==-1)return
	let data = DATA_get_current_state_point(true)
	let current_voice_data = data.voice_data.find(item=>{return item.voice_id==voice_id})
	if(current_voice_data==null){
		console.error("Operation error, data not found")
		return
	}
	let rot_success=false
	current_voice_data.data.segment_data.forEach((segment,segment_index)=>{
		let success=DATA_calculate_rotate_iT_segment(segment,value)
		//if at least one is ok
		if(success)rot_success=true
	})
	if(!rot_success){
		APP_info_msg("op_not_succes")
		return
	}
	//calculate midi
	current_voice_data.data.midi_data=DATA_segment_data_to_midi_data(current_voice_data.data.segment_data,current_voice_data.neopulse)
	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

//N
//Na
function DATA_calculate_rotate_Na_segment(segment,value,rot_se){
	let rot_success=false
	//rotate notes
	let sound_array_copy = JSON.parse(JSON.stringify(segment.sound))
	//NO ROTATION OF L
	let sound_array_rot=[]
	//rotate only note, no s,e,L,end_note
	let from_n=-1
	if(rot_se){
		//rotate only note,s,e, no L,end_note
		from_n=-3
	}
	sound_array_copy.forEach(sound=>{
		if(sound.note>from_n){
			sound_array_rot.push(sound)
		}
	})
	if(sound_array_rot.length>1){
		rot_success=true
		for (let i = 0; i < value; i++) {
			sound_array_rot.push(sound_array_rot.shift())
		}
		//reinsert
		let position=0
		sound_array_copy.forEach((sound,index_sound)=>{
			if(sound.note>from_n){
				sound_array_copy[index_sound]=sound_array_rot[position]
				position++
			}
		})
	}
	if(rot_success){
		segment.sound=sound_array_copy
	}
	return rot_success
}

//Ngm
function DATA_calculate_rotate_Ngm_segment(segment,value,rot_se,Psg_segment_start,neopulse,scale_sequence){
	let [N_NP,D_NP]=[neopulse.N,neopulse.D]
	let rot_success=false
	let outside_range=false
	let sound_grade_list=[]
	let scale_list=[]
	let idea_scale_list=DATA_get_idea_scale_list()
	segment.sound.find((sound,index)=>{
		if (sound.note == -4){
			//end segment
			return true
		}
		//traduction
		let time = segment.time[index]
		let fraction = segment.fraction.find(fraction=>{return fraction.stop>time.P})
		if (typeof(fraction) == "undefined"){
			//end segment control 2
			return true
		}
		//calculating position
		let frac_p = fraction.N/fraction.D
		let Pa_equivalent = (Psg_segment_start+time.P) * N_NP/D_NP + time.F* frac_p * N_NP/D_NP
		let current_scale = DATA_get_Pa_eq_scale(Pa_equivalent,scale_sequence)
		//inside a scale module
		if(sound.note<0){
			sound_grade_list.push({note:sound.note,diesis:sound.diesis,A_pos:[],A_neg:[]})
		}else{
			let [grade,delta,reg]=DATA_calculate_absolute_note_to_scale(sound.note,sound.diesis,current_scale,idea_scale_list)
			sound_grade_list.push({note:sound.note,grade:grade,delta:delta,reg:reg,diesis:sound.diesis,A_pos:sound.A_pos,A_neg:sound.A_neg})
		}
		//handy store the calculated scale for later calculations
		scale_list.push(current_scale)
	})
	//NO ROTATION OF L
	//rotate only note, no s,e,L,end_note
	let from_n=-1
	if(rot_se){
		//rotate only note,s,e, no L,end_note
		from_n=-3
	}
	let sound_grade_list_rot=[]
	sound_grade_list.forEach((sound_grade,index_sound)=>{
		if(sound_grade.note>from_n){
			sound_grade.original_index_sound=index_sound
			sound_grade_list_rot.push(sound_grade)
		}
	})
	if(sound_grade_list_rot.length>1){
		rot_success=true
		for (let i = 0; i < value; i++) {
			sound_grade_list_rot.push(sound_grade_list_rot.shift())
		}
		//reinsert
		let position=0
		segment.sound.forEach((sound,index_sound)=>{
			//max_index is outside scale range
			if(sound.note>from_n){
				let note_number=sound_grade_list_rot[position].note
				//recalculate if is a note
				if(note_number>=0){
					let current_scale=scale_list[index_sound]
					let old_scale=scale_list[sound_grade_list_rot[position].original_index_sound]
					let result = DATA_calculate_scale_note_to_absolute(sound_grade_list_rot[position].grade,sound_grade_list_rot[position].delta,sound_grade_list_rot[position].reg,current_scale,idea_scale_list)
					if(result.outside_range) outside_range=true
					let new_note_number=result.note
					//A
					let provv_sound={note:note_number,diesis:sound_grade_list_rot[position].diesis,A_pos:sound_grade_list_rot[position].A_pos,A_neg:sound_grade_list_rot[position].A_neg}
					let new_A = DATA_calculate_A_from_sound_grade_change(provv_sound,new_note_number,null,old_scale,current_scale,idea_scale_list)
					segment.sound[index_sound].note=new_note_number
					segment.sound[index_sound].diesis=sound_grade_list_rot[position].diesis
					segment.sound[index_sound].A_pos=new_A.A_pos
					segment.sound[index_sound].A_neg=new_A.A_neg
					let result_A = DATA_verify_sound_A(segment.sound[index_sound])
					if(result_A) outside_range=true
				}else{
					segment.sound[index_sound].note=note_number
					segment.sound[index_sound].diesis=sound_grade_list_rot[position].diesis
					segment.sound[index_sound].A_pos=[]
					segment.sound[index_sound].A_neg=[]
				}
				position++
			}
		})
	}
	return [rot_success,outside_range]
}

function DATA_rotate_N_segment(voice_id,segment_index,value,rot_se,type_N){
	if(voice_id==-1 || segment_index==-1)return
	let data = DATA_get_current_state_point(true)
	let current_voice_data = data.voice_data.find(item=>{return item.voice_id==voice_id})
	if(current_voice_data==null){
		console.error("Operation error, data not found")
		return
	}
	if(current_voice_data.data.segment_data.length<=segment_index){
		console.error("Operation error, data not found")
		return
	}
	let rot_success = false
	let outside_range=false
	if(type_N=="Na")rot_success=DATA_calculate_rotate_Na_segment(current_voice_data.data.segment_data[segment_index],value,rot_se)
	if(type_N=="Ngm"){
		let Psg_segment_start=0
		for (let i = 0; i < segment_index; i++) {
			Psg_segment_start+=current_voice_data.data.segment_data[i].time.slice(-1)[0].P
		}
		let scale_sequence= data.scale.scale_sequence;
		[rot_success,outside_range]=DATA_calculate_rotate_Ngm_segment(current_voice_data.data.segment_data[segment_index],value,rot_se,Psg_segment_start,current_voice_data.neopulse,scale_sequence)
	}
	if(!rot_success){
		APP_info_msg("op_not_succes")
		return
	}
	if(outside_range){
		APP_info_msg("out_N_range")
	}
	//calculate midi
	current_voice_data.data.midi_data=DATA_segment_data_to_midi_data(current_voice_data.data.segment_data,current_voice_data.neopulse)
	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_rotate_N_segment_column(segment_index,value,rot_se,type_N){
	if(segment_index==-1)return
	let data = DATA_get_current_state_point(true)
	let find_data=false
	let rot_success=false
	let outside_range=false
	data.voice_data.forEach(voice=>{
		if(voice.blocked){
			find_data=true
			if(voice.data.segment_data.length<=segment_index){
				find_data=false
				return
			}
			let success = false
			let o_r=false
			if(type_N=="Na")success=DATA_calculate_rotate_Na_segment(voice.data.segment_data[segment_index],value,rot_se)
			if(type_N=="Ngm"){
				let Psg_segment_start=0
				for (let i = 0; i < segment_index; i++) {
					Psg_segment_start+=voice.data.segment_data[i].time.slice(-1)[0].P
				}
				let scale_sequence= data.scale.scale_sequence;
				[success,o_r]=DATA_calculate_rotate_Ngm_segment(voice.data.segment_data[segment_index],value,rot_se,Psg_segment_start,voice.neopulse,scale_sequence)
				if(o_r)outside_range=true
			}
			if(!success){
				return
			}
			rot_success=true
			//calculate midi
			voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
		}
	})
	if(!find_data){
		console.error("Operation error, data not found")
		return
	}
	if(!rot_success){
		APP_info_msg("op_not_succes")
		return
	}
	if(outside_range){
		APP_info_msg("out_N_range")
	}
	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_rotate_N_voice(voice_id,value,rot_se,by_segment,type_N){
	if(voice_id==-1)return
	let data = DATA_get_current_state_point(true)
	let current_voice_data = data.voice_data.find(item=>{return item.voice_id==voice_id})
	if(current_voice_data==null){
		console.error("Operation error, data not found")
		return
	}
	let rot_success=false
	let outside_range=false
	let scale_sequence= data.scale.scale_sequence
	if(by_segment){
		let Psg_segment_start=0
		current_voice_data.data.segment_data.forEach((segment,segment_index)=>{
			let success = false
			if(type_N=="Na")success=DATA_calculate_rotate_Na_segment(segment,value,rot_se)
			if(type_N=="Ngm"){
				let o_r=false;
				[success,o_r]=DATA_calculate_rotate_Ngm_segment(segment,value,rot_se,Psg_segment_start,current_voice_data.neopulse,scale_sequence)
				if(o_r)outside_range=true
			}
			Psg_segment_start+=segment.time.slice(-1)[0].P
			if(!success){
				return
			}
			rot_success=true
		})
	}else{
		//copy Note lines
		let provv_segment= DATA_concat_segments(current_voice_data)
		let success = false
		if(type_N=="Na")success=DATA_calculate_rotate_Na_segment(provv_segment,value,rot_se)
		if(type_N=="Ngm"){
			let o_r=false;
			[success,o_r]==DATA_calculate_rotate_Ngm_segment(provv_segment,value,rot_se,0,current_voice_data.neopulse,scale_sequence)
			if(o_r)outside_range=true
		}
		if(success){
			rot_success=true
			//overwrite new note/diesis list inside old structure
			//rotate only note, no s,e,L,end_note
			let from_n=-1
			if(rot_se){
				//rotate only note,s,e, no L,end_note
				from_n=-3
			}
			let index=0
			current_voice_data.data.segment_data.forEach((segment,segment_index)=>{
				segment.sound.forEach((sound,sound_index)=>{
					if(sound.note>from_n){
						//sub with old one
						segment.sound[sound_index]=provv_segment.sound[index]
					}
					if(sound.note!=-4)index++
				})
			})
		}
	}
	if(!rot_success){
		APP_info_msg("op_not_succes")
		return
	}
	if(outside_range){
		APP_info_msg("out_N_range")
	}
	//calculate midi
	current_voice_data.data.midi_data=DATA_segment_data_to_midi_data(current_voice_data.data.segment_data,current_voice_data.neopulse)
	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

//iN
//iNa
function DATA_calculate_rotate_iNa_segment(segment,value,rot_se){
	let rot_success=false
	let outside_range=false
	//rotate notes
	//cannot use
	let sound_array_copy = JSON.parse(JSON.stringify(segment.sound))
	//let note_array_copy = JSON.parse(JSON.stringify(segment.sound.map(s=>{return s.note})))
	//var diesis_array_copy = JSON.parse(JSON.stringify(segment.diesis))
	let iNa_list=DATA_calculate_iNa_from_Na_list(JSON.parse(JSON.stringify(sound_array_copy.map(s=>{return s.note}))))
	//NO ROTATION OF L
	let iNa_list_rot=[]
	let sound_array_rot=[]
	if(rot_se){
		//rotate only note,s,e, no L,end_note
		let from_n=-3
		sound_array_copy.forEach((sound,index_sound)=>{
			if(sound.note>from_n){
				iNa_list_rot.push(iNa_list[index_sound])
				sound_array_rot.push(sound)
			}
		})
		//verify if there is something to rotate
		if(iNa_list_rot.length>1){
			rot_success=true
			//rotate iNa
			for (let i = 0; i < value; i++) {
				let current_iNa=iNa_list_rot.shift()
				let current_sound=sound_array_rot.shift()
				if(isNaN(current_iNa)){
					iNa_list_rot.push(current_iNa)
					sound_array_rot.push(current_sound)
				}else{
					//it is starting note
					//find next iNa
					let next_index= _jump_to_next_note_index(iNa_list_rot)
					if(next_index==-1){
						//no other notes, put it last
						iNa_list_rot.push(current_iNa)
						sound_array_rot.push(current_sound)
					}else{
						let next_iNa=iNa_list_rot[next_index]
						let next_sound=sound_array_rot[next_index]
						//take iNa to last ans sunstitute with starting note
						iNa_list_rot.push(next_iNa)
						sound_array_rot.push(next_sound)
						iNa_list_rot[next_index]=current_iNa
						sound_array_rot[next_index]=current_sound
					}
				}
			}
			//reinsert
			let position=0
			sound_array_copy.forEach((sound,index_sound)=>{
				if(sound.note>from_n){
					iNa_list[index_sound]=iNa_list_rot[position]
					sound_array_copy[index_sound]=sound_array_rot[position]
					position++
				}
			})
			let [n_a_c,o_r]=DATA_calculate_Na_from_iNa_list(iNa_list)
			n_a_c.forEach((note,index)=>{sound_array_copy[index].note=note})
			outside_range=o_r
		}
	}else{
		//rotate only note, no s,e,L,end_note
		let from_n=-1
		sound_array_copy.forEach((sound,index_sound)=>{
			if(sound.note>from_n){
				iNa_list_rot.push(iNa_list[index_sound])
				sound_array_rot.push(sound)
			}
		})
		let starting_note=iNa_list_rot.shift()
		let starting_sound=sound_array_rot.shift()
		if(iNa_list_rot.length>1){
			rot_success=true
			//rotate iNa
			for (let i = 0; i < value; i++) {
				iNa_list_rot.push(iNa_list_rot.shift())
				sound_array_rot.push(sound_array_rot.shift())
			}
			iNa_list_rot.unshift(starting_note)
			sound_array_rot.unshift(starting_sound)
			//reinsert
			let position=0
			sound_array_copy.forEach((sound,index_sound)=>{
				if(sound.note>from_n){
					iNa_list[index_sound]=iNa_list_rot[position]
					sound_array_copy[index_sound]=sound_array_rot[position]
					position++
				}
			})
			let [n_a_c,o_r]=DATA_calculate_Na_from_iNa_list(iNa_list)
			n_a_c.forEach((note,index)=>{sound_array_copy[index].note=note})
			outside_range=o_r
		}
	}
	if(rot_success){
		segment.sound=sound_array_copy
		segment.sound.forEach(sound=>{
			let result_A = DATA_verify_sound_A(sound)
			if(result_A) outside_range=true
		})
	}
	return [rot_success,outside_range]

	function _jump_to_next_note_index(new_note_order){
		if(new_note_order.length==0){
			console.error("NOTE NOT FOUND")
			return -1
		}
		var next_index=-1
		new_note_order.find((iNa,index)=>{
			if(!isNaN(iNa)){
				next_index=index
				return true
			}
		})
		return next_index
	}
}

//iNgm
function DATA_calculate_rotate_iNgm_segment(segment,value,rot_se,Psg_segment_start,neopulse,scale_sequence){
	let [N_NP,D_NP]=[neopulse.N,neopulse.D]
	let rot_success=false
	let sound_grade_list=[]
	let scale_list=[]
	let idea_scale_list=DATA_get_idea_scale_list()
	segment.sound.find((sound,index)=>{
		//traduction
		let time = segment.time[index]
		let fraction = segment.fraction.find(fraction=>{return fraction.stop>time.P})
		if (typeof(fraction) == "undefined"){
			//end segment
			return true
		}
		//calculating position
		let frac_p = fraction.N/fraction.D
		let Pa_equivalent = (Psg_segment_start+time.P) * N_NP/D_NP + time.F* frac_p * N_NP/D_NP
		//inside a scale module
		let current_scale = DATA_get_Pa_eq_scale(Pa_equivalent,scale_sequence)
		if(sound.note<0){
			sound_grade_list.push({note:sound.note,diesis:sound.diesis,A_pos:[],A_neg:[]})
		}else{
			let [grade,delta,reg]=DATA_calculate_absolute_note_to_scale(sound.note,sound.diesis,current_scale,idea_scale_list)
			sound_grade_list.push({note:sound.note,grade:grade,delta:delta,reg:reg,diesis:sound.diesis,A_pos:sound.A_pos,A_neg:sound.A_neg})
		}
		//handy store the calculated scale for later calculations
		scale_list.push(current_scale)
	})
	//calc iN
	let i_sound_grade_list=DATA_calculate_iNgm_from_Ngm_list(sound_grade_list,scale_list)
	//not note: {note:Na,diesis:diesis}) //A too
	//first note: {note:Na,grade:grade,delta:delta,reg:reg,diesis:diesis}
	//d_grad : {note:Na,iNgm:d_grad,diesis:diesis}
	//NO ROTATION OF L
	let i_sound_grade_list_rot=[]
	//rotate only note, no s,e,L,end_note
	let from_n=-1
	if(rot_se){
		//rotate only note,s,e, no L,end_note
		from_n=-3
	}
	i_sound_grade_list.forEach((i_sound_grade,index_sound)=>{
		if(i_sound_grade.note>from_n){
			i_sound_grade.original_index_sound=index_sound
			i_sound_grade_list_rot.push(i_sound_grade)
		}
	})
	//verify if there is something to rotate
	if(i_sound_grade_list_rot.length<2){
		return [false,false]
	}
	rot_success=true
	if(rot_se){
		//rotate iNgm
		for (let i = 0; i < value; i++) {
			let current_i_sound_grade=i_sound_grade_list_rot.shift()
			if(current_i_sound_grade.note<0){
				i_sound_grade_list_rot.push(current_i_sound_grade)
			}else{
				//it is starting note
				//find next iNgm
				let next_index= _jump_to_next_note_index(i_sound_grade_list_rot)
				if(next_index==-1){
					//no other notes, put it last
					i_sound_grade_list_rot.push(current_i_sound_grade)
				}else{
					let next_i_sound_grade=i_sound_grade_list_rot[next_index]
					//take iNgm to last and substitute with starting note
					i_sound_grade_list_rot.push(next_i_sound_grade)
					i_sound_grade_list_rot[next_index]=current_i_sound_grade
				}
			}
		}
	}else{
		//rotate i_sound_grade_list_rot
		let starting_note=i_sound_grade_list_rot.shift()
		//rotate iN
		for (let i = 0; i < value; i++) {
			i_sound_grade_list_rot.push(i_sound_grade_list_rot.shift())
		}
		i_sound_grade_list_rot.unshift(starting_note)
	}
	//reinsert
	let i_sound_grade_list_copy=JSON.parse(JSON.stringify(i_sound_grade_list))
	let position=0
	i_sound_grade_list_copy.forEach((i_sound_grade,index_sound)=>{
		if(i_sound_grade.note>from_n){
			i_sound_grade_list_copy[index_sound]=i_sound_grade_list_rot[position]
			position++
		}
	})
	let [Na_list,outside_range]=DATA_calculate_Na_from_iNgm_list(i_sound_grade_list_copy,scale_list,idea_scale_list)
	Na_list.forEach((note_item,index_sound)=>{
		let new_note_number=note_item.note
		//recalculate if is a note
		if(new_note_number>=0){
			let current_scale=scale_list[index_sound]
			let old_scale=scale_list[i_sound_grade_list_copy[index_sound].original_index_sound]
			//A
			let provv_sound={note:i_sound_grade_list_copy[index_sound].note,diesis:note_item.diesis,A_pos:i_sound_grade_list_copy[index_sound].A_pos,A_neg:i_sound_grade_list_copy[index_sound].A_neg}
			let new_A = DATA_calculate_A_from_sound_grade_change(provv_sound,new_note_number,null,old_scale,current_scale,idea_scale_list)
			segment.sound[index_sound].note=new_note_number
			segment.sound[index_sound].diesis=note_item.diesis
			segment.sound[index_sound].A_pos=new_A.A_pos
			segment.sound[index_sound].A_neg=new_A.A_neg
			let result_A = DATA_verify_sound_A(segment.sound[index_sound])
			if(result_A) outside_range=true
		}else{
			segment.sound[index_sound].note=new_note_number
			segment.sound[index_sound].diesis=note_item.diesis
			segment.sound[index_sound].A_pos=[]
			segment.sound[index_sound].A_neg=[]
		}
	})
	return [rot_success,outside_range]
	//
	function _jump_to_next_note_index(new_iNgm_order){
		if(new_iNgm_order.length==0){
			console.error("NOTE NOT FOUND")
			return -1
		}
		let next_index=-1
		new_iNgm_order.find((iNgm,index)=>{
			if(iNgm.note>-1){
				next_index=index
				return true
			}
		})
		return next_index
	}
}

function DATA_rotate_iN_segment(voice_id,segment_index,value,rot_se,type_N){
	if(voice_id==-1 || segment_index==-1)return
	let data = DATA_get_current_state_point(true)
	let current_voice_data = data.voice_data.find(item=>{return item.voice_id==voice_id})
	if(current_voice_data==null){
		console.error("Operation error, data not found")
		return
	}
	if(current_voice_data.data.segment_data.length<=segment_index){
		console.error("Operation error, data not found")
		return
	}
	let rot_success=false
	let outside_range=false
	if(type_N=="Na")[rot_success,outside_range] = DATA_calculate_rotate_iNa_segment(current_voice_data.data.segment_data[segment_index],value,rot_se)
	if(type_N=="Ngm"){
		let Psg_segment_start=0
		for (let i = 0; i < segment_index; i++) {
			Psg_segment_start+=current_voice_data.data.segment_data[i].time.slice(-1)[0].P
		}
		let scale_sequence= data.scale.scale_sequence;
		[rot_success,outside_range]=DATA_calculate_rotate_iNgm_segment(current_voice_data.data.segment_data[segment_index],value,rot_se,Psg_segment_start,current_voice_data.neopulse,scale_sequence)
	}
	if(!rot_success){
		APP_info_msg("op_not_succes")
		return
	}
	if(outside_range){
		APP_info_msg("out_N_range")
	}
	//calculate midi
	current_voice_data.data.midi_data=DATA_segment_data_to_midi_data(current_voice_data.data.segment_data,current_voice_data.neopulse)
	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_rotate_iN_segment_column(segment_index,value,rot_se,type_N){
	if(segment_index==-1)return
	let data = DATA_get_current_state_point(true)
	let find_data=false
	let rot_success=false
	let outside_range=false
	data.voice_data.forEach(voice=>{
		if(voice.blocked){
			find_data=true
			if(voice.data.segment_data.length<=segment_index){
				find_data=false
				return
			}
			let success = false
			let out_r=false
			if(type_N=="Na")[success,out_r]=DATA_calculate_rotate_iNa_segment(voice.data.segment_data[segment_index],value,rot_se)
			if(type_N=="Ngm"){
				let Psg_segment_start=0
				for (let i = 0; i < segment_index; i++) {
					Psg_segment_start+=voice.data.segment_data[i].time.slice(-1)[0].P
				}
				let scale_sequence= data.scale.scale_sequence;
				[success,out_r]=DATA_calculate_rotate_iNgm_segment(voice.data.segment_data[segment_index],value,rot_se,Psg_segment_start,voice.neopulse,scale_sequence)
			}
			if(!success){
				return
			}
			if(out_r)outside_range=true
			rot_success=true
			//calculate midi
			voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
		}
	})
	if(!find_data){
		console.error("Operation error, data not found")
		return
	}
	if(!rot_success){
		APP_info_msg("op_not_succes")
		return
	}
	if(outside_range){
		APP_info_msg("out_N_range")
	}
	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_rotate_iN_voice(voice_id,value,rot_se,by_segment,type_N){
	if(voice_id==-1)return
	let data = DATA_get_current_state_point(true)
	let current_voice_data = data.voice_data.find(item=>{return item.voice_id==voice_id})
	if(current_voice_data==null){
		console.error("Operation error, data not found")
		return
	}
	let rot_success=false
	let outside_range=false
	let scale_sequence= data.scale.scale_sequence
	if(by_segment){
		let Psg_segment_start=0
		current_voice_data.data.segment_data.forEach((segment,segment_index)=>{
			//rotate notes
			let success = false
			let out_r = false
			if(type_N=="Na") [success,out_r] = DATA_calculate_rotate_iNa_segment(segment,value,rot_se)
			if(type_N=="Ngm"){
				[success,out_r]=DATA_calculate_rotate_iNgm_segment(segment,value,rot_se,Psg_segment_start,current_voice_data.neopulse,scale_sequence)
			}
			Psg_segment_start+=segment.time.slice(-1)[0].P
			if(!success){
				return
			}
			if(out_r)outside_range=true
			rot_success=true
		})
	}else{
		//copy Note lines
		let provv_segment= DATA_concat_segments(current_voice_data)
		let success = false
		let out_r = false
		if(type_N=="Na")success=DATA_calculate_rotate_iNa_segment(provv_segment,value,rot_se)
		if(type_N=="Ngm"){
			[success,out_r]=DATA_calculate_rotate_iNgm_segment(provv_segment,value,rot_se,0,current_voice_data.neopulse,scale_sequence)
		}
		if(success){
			rot_success=true
			if(out_r)outside_range=true
			//overwrite new note/diesis list inside old structure
			//rotate only note, no s,e,L,end_note
			let from_n=-1
			if(rot_se){
				//rotate only note,s,e, no L,end_note
				from_n=-3
			}
			let index=0
			current_voice_data.data.segment_data.forEach((segment,segment_index)=>{
				segment.sound.forEach((sound,sound_index)=>{
					if(sound.note>from_n){
						//sub with old one
						segment.sound[sound_index]=provv_segment.sound[index]
					}
					if(note!=-4)index++
				})
			})
		}
	}
	if(!rot_success){
		APP_info_msg("op_not_succes")
		return
	}
	if(outside_range){
		APP_info_msg("out_N_range")
	}
	//calculate midi
	current_voice_data.data.midi_data=DATA_segment_data_to_midi_data(current_voice_data.data.segment_data,current_voice_data.neopulse)
	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

/////////*****VERTICAL MIRROR (horizontal axis)*****/////////

//N
//Na
function DATA_calculate_v_mirror_Na_segment(segment,values){
	let max_note = TET*N_reg -1
	let outside_range=false
	let v_mirror_success=false
	let axis_defined=false
	let axis_note=null
	let provv
	switch (values.axis_type) {
		case "user_note":
			axis_note=values.value
		break;
		case "starting_note":
			axis_note = segment.sound.find(sound=>{
				return sound.note>=0
			}).note
		break;
		case "lowest_note":
			provv=max_note+1
			segment.sound.forEach(sound=>{
				if(sound.note>=0 && sound.note<provv){
					provv=sound.note
					axis_note=sound.note
				}
			})
		break;
		case "highest_note":
			provv=-1
			segment.sound.forEach(sound=>{
				if(sound.note>=0 && sound.note>provv){
					provv=sound.note
					axis_note=sound.note
				}
			})
		break;
	}
	if(axis_note!=null){
		axis_defined=true
		segment.sound.forEach(sound=>{
			//let note=sound.note
			if(sound.note>=0){
				v_mirror_success=true
				//calculating delta Na
				let dNa=(sound.note-axis_note)*2
				sound.note-=dNa
				if(sound.note>max_note){
					sound.note=max_note
					outside_range=true
				}
				if(sound.note<0){
					sound.note=0
					outside_range=true
				}
				let result_A = DATA_verify_sound_A(sound)
				if(result_A) outside_range=true
			}
		})
	}
	return [axis_defined,v_mirror_success,outside_range]
}

//Ngm
function DATA_calculate_v_mirror_Ngm_segment(segment,values,Psg_segment_start,neopulse,scale_sequence){
	let [N_NP,D_NP]=[neopulse.N,neopulse.D]
	let max_note = TET*N_reg -1
	let outside_range=false
	let v_mirror_success=false
	let axis_defined=false
	let axis_note_grade=null
	let snap_to_grade=false
	let provv=null
	let axis_note=null
	let axis_note_index=null
	let idea_scale_list=DATA_get_idea_scale_list()
	if(values.axis_type=="starting_scale_note"){
		snap_to_grade=true
		values.axis_type="starting_note"
	}
	switch (values.axis_type) {
		case "user_note":
			axis_note_grade=values.value
		break;
		case "starting_note":
			segment.sound.find((sound,index)=>{
				if(sound.note>=0){
					//traduction
					let time = segment.time[index]
					let fraction = segment.fraction.find(fraction=>{return fraction.stop>time.P})
					if (typeof(fraction) == "undefined"){
						return true
					}
					//calculating position
					let frac_p = fraction.N/fraction.D
					let Pa_equivalent = (Psg_segment_start+time.P) * N_NP/D_NP + time.F* frac_p * N_NP/D_NP
					let current_scale=DATA_get_Pa_eq_scale(Pa_equivalent,scale_sequence)
					let [grade,delta,reg]=DATA_calculate_absolute_note_to_scale(sound.note,sound.diesis,current_scale,idea_scale_list)
					axis_note_grade={grade:grade,delta:delta,reg:reg,diesis:sound.diesis}
					return true
				}
			})
		break;
		case "lowest_note":
			provv=max_note+1
			axis_note=null
			axis_note_index=null
			segment.sound.forEach((sound,index)=>{
				if(sound.note>=0 && sound.note<provv){
					provv=sound.note
					axis_note=sound.note
					axis_note_index=index
				}
			})
			if(axis_note_index!=null){
				//traduction
				let time = segment.time[axis_note_index]
				let fraction = segment.fraction.find(fraction=>{return fraction.stop>time.P})
				if (typeof(fraction) == "undefined"){
					return [false,false,false]
				}
				//calculating position
				let frac_p = fraction.N/fraction.D
				let Pa_equivalent = (Psg_segment_start+time.P) * N_NP/D_NP + time.F* frac_p * N_NP/D_NP
				let current_scale=DATA_get_Pa_eq_scale(Pa_equivalent,scale_sequence)
				//first element inside a scale module
				let [grade,delta,reg]=DATA_calculate_absolute_note_to_scale(axis_note,segment.sound[axis_note_index].diesis,current_scale,idea_scale_list)
				axis_note_grade={grade:grade,delta:delta,reg:reg,diesis:segment.sound[axis_note_index].diesis}
			}
		break;
		case "highest_note":
			provv=-1
			axis_note=null
			axis_note_index=null
			segment.sound.forEach((sound,index)=>{
				if(sound.note>=0 && sound.note>provv){
					provv=sound.note
					axis_note=sound.note
					axis_note_index=index
				}
			})
			if(axis_note_index!=null){
				//traduction
				let time = segment.time[axis_note_index]
				let fraction = segment.fraction.find(fraction=>{return fraction.stop>time.P})
				if (typeof(fraction) == "undefined"){
					return [false,false,false]
				}
				//calculating position
				let frac_p = fraction.N/fraction.D
				let Pa_equivalent = (Psg_segment_start+time.P) * N_NP/D_NP + time.F* frac_p * N_NP/D_NP
				let current_scale=DATA_get_Pa_eq_scale(Pa_equivalent,scale_sequence)
				let [grade,delta,reg]=DATA_calculate_absolute_note_to_scale(axis_note,segment.sound[axis_note_index].diesis,current_scale,idea_scale_list)
				axis_note_grade={grade:grade,delta:delta,reg:reg,diesis:segment.sound[axis_note_index].diesis}
			}
		break;
	}
	if(axis_note_grade!=null){
		axis_defined=true
		segment.sound.forEach((sound,sound_index)=>{
			if(sound.note>=0){
				v_mirror_success=true
				//calculating delta iNgm
				let time = segment.time[sound_index]
				let fraction = segment.fraction.find(fraction=>{return fraction.stop>time.P})
				if (typeof(fraction) == "undefined"){
					v_mirror_success=false
					return
				}
				//calculating position
				let frac_p = fraction.N/fraction.D
				let Pa_equivalent = (Psg_segment_start+time.P) * N_NP/D_NP + time.F* frac_p * N_NP/D_NP
				let current_scale=DATA_get_Pa_eq_scale(Pa_equivalent,scale_sequence)
				let [grade,delta,reg]=DATA_calculate_absolute_note_to_scale(sound.note,sound.diesis,current_scale,idea_scale_list)
				//consider axis_note_delta=0, axis on the PURE grade no diesis???
				// var dNa=(note-axis_note)*2
				// note-=dNa
				let new_grade=2*axis_note_grade.grade-grade
				let new_delta=delta*(-1)
				if(snap_to_grade)new_delta=0//for iNgm
				if(new_delta!=0){
					//change positivity of diesis
					sound.diesis=!sound.diesis
				}
				let new_reg=2*axis_note_grade.reg-reg
				let result = DATA_calculate_scale_note_to_absolute(new_grade,new_delta,new_reg,current_scale,idea_scale_list)
				if(result.outside_range)outside_range=true
				//A
				let new_A = DATA_calculate_A_from_sound_grade_change(sound,result.note,null,current_scale,null,idea_scale_list)
				sound.note=result.note
				sound.A_pos=new_A.A_pos
				sound.A_neg=new_A.A_neg
				let result_A = DATA_verify_sound_A(sound)
				if(result_A) outside_range=true
			}
		})
	}
	return [axis_defined,v_mirror_success,outside_range]
}

function DATA_v_mirror_N_segment(voice_id,segment_index,values,type_N){
	if(voice_id==-1 || segment_index==-1)return
	let data = DATA_get_current_state_point(true)
	let current_voice_data = data.voice_data.find(item=>{return item.voice_id==voice_id})
	if(current_voice_data==null){
		console.error("Operation error, data not found")
		return
	}
	if(current_voice_data.data.segment_data.length<=segment_index){
		console.error("Operation error, data not found")
		return
	}
	let axis_defined=false
	let success=false
	let outside_range=false
	if(type_N=="Na")[axis_defined,success,outside_range]=DATA_calculate_v_mirror_Na_segment(current_voice_data.data.segment_data[segment_index],values)
	if(type_N=="Ngm"){
		let Psg_segment_start=0
		for (let i = 0; i < segment_index; i++) {
			Psg_segment_start+=current_voice_data.data.segment_data[i].time.slice(-1)[0].P
		}
		let scale_sequence= data.scale.scale_sequence;
		[axis_defined,success,outside_range]=DATA_calculate_v_mirror_Ngm_segment(current_voice_data.data.segment_data[segment_index],values,Psg_segment_start,current_voice_data.neopulse,scale_sequence)
	}
	if(!axis_defined){
		console.error("Operation error, axis not well defined")
		return
	}
	if(!success){
		APP_info_msg("op_not_succes")
		return
	}
	if(outside_range){
		APP_info_msg("out_N_range")
	}
	current_voice_data.data.midi_data=DATA_segment_data_to_midi_data(current_voice_data.data.segment_data,current_voice_data.neopulse)
	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_v_mirror_N_segment_column(segment_index,values,type_N){
	if(segment_index==-1)return
	let data = DATA_get_current_state_point(true)
	let find_data=false
	let outside_range=false
	let v_mirror_success=false
	let max_note = TET*N_reg -1
	data.voice_data.forEach(voice=>{
		if(voice.blocked){
			find_data=true
			if(voice.data.segment_data.length<=segment_index){
				find_data=false
				return
			}
			let axis_defined=false
			let success=false
			let out_r=false
			if(type_N=="Na")[axis_defined,success,out_r] = DATA_calculate_v_mirror_Na_segment(voice.data.segment_data[segment_index],values)
			if(type_N=="Ngm"){
				let Psg_segment_start=0
				for (let i = 0; i < segment_index; i++) {
					Psg_segment_start+=voice.data.segment_data[i].time.slice(-1)[0].P
				}
				let scale_sequence= data.scale.scale_sequence;
				[axis_defined,success,out_r]=DATA_calculate_v_mirror_Ngm_segment(voice.data.segment_data[segment_index],values,Psg_segment_start,voice.neopulse,scale_sequence)
			}
			//axis_defined ??? not used, broke other segments that are mirroed if a segment is empty
			if(!success){
				return
			}
			if(out_r)outside_range=true
			v_mirror_success=true
			//calculate midi
			voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
		}
	})
	if(!find_data){
		console.error("Operation error, data not found")
		return
	}
	if(!v_mirror_success){
		APP_info_msg("op_not_succes")
		return
	}
	if(outside_range){
		APP_info_msg("out_N_range")
	}
	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_v_mirror_N_voice(voice_id,values,type_N){
	if(voice_id==-1)return
	let data = DATA_get_current_state_point(true)
	let current_voice_data = data.voice_data.find(item=>{return item.voice_id==voice_id})
	if(current_voice_data==null){
		console.error("Operation error, data not found")
		return
	}
	//copy Note lines
	let provv_segment= DATA_concat_segments(current_voice_data)
	let v_mirror_success=false
	let axis_defined=false
	let success=false
	let outside_range=false
	if(type_N=="Na")[axis_defined,success,outside_range] = DATA_calculate_v_mirror_Na_segment(provv_segment,values)
	if(type_N=="Ngm"){
		let scale_sequence= data.scale.scale_sequence;
		[axis_defined,success,outside_range] = DATA_calculate_v_mirror_Ngm_segment(provv_segment,values,0,current_voice_data.neopulse,scale_sequence)
	}
	if(!axis_defined){
		console.error("Operation error, axis not well defined")
		return
	}
	if(success){
		v_mirror_success=true
		//overwrite new note/diesis list inside old structure
		let index=0
		current_voice_data.data.segment_data.forEach((segment,segment_index)=>{
			segment.sound.forEach((sound,sound_index)=>{
				//not touching L,e,s,end_range
				if(sound.note>-1){
					//sub with old one
					segment.sound[sound_index]=provv_segment.sound[index]
				}
				if(sound.note!=-4)index++
			})
		})
	}
	if(!v_mirror_success){
		APP_info_msg("op_not_succes")
		return
	}
	if(outside_range){
		APP_info_msg("out_N_range")
	}
	current_voice_data.data.midi_data=DATA_segment_data_to_midi_data(current_voice_data.data.segment_data,current_voice_data.neopulse)
	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

//iN
function DATA_v_mirror_iN_segment(voice_id,segment_index,type_N){
	//same as v_mirror iN with option start note
	let option ={"axis_type": "starting_note"}
	if(type_N=="Ngm")option.axis_type="starting_scale_note"
	DATA_v_mirror_N_segment(voice_id,segment_index,option,type_N)
}

function DATA_v_mirror_iN_segment_column(segment_index,type_N){
	//same as v_mirror iN with option start note
	let option ={"axis_type": "starting_note"}
	if(type_N=="Ngm")option.axis_type="starting_scale_note"
	DATA_v_mirror_N_segment_column(segment_index,option,type_N)
}

function DATA_v_mirror_iN_voice(voice_id,type_N){
	//same as v_mirror iN with option start note
	let option ={"axis_type": "starting_note"}
	if(type_N=="Ngm")option.axis_type="starting_scale_note"
	DATA_v_mirror_N_voice(voice_id,option,type_N)
}

/////////*****HORIZONTAL MIRROR (vertical axis)*****/////////

//iT and N
function DATA_calculate_h_mirror_element_segment(segment){
	//reflect notes
	let sound_array_copy = JSON.parse(JSON.stringify(segment.sound))
	sound_array_copy.pop()
	//check L
	sound_array_copy.forEach((sound,index)=>{
		if(sound.note==-3){
			let provv_sound_L=JSON.parse(JSON.stringify(sound))
			sound_array_copy[index]=sound_array_copy[index-1]
			sound_array_copy[index-1]=provv_sound_L
		}
	})
	if(sound_array_copy.length<2){
		return false
	}
	sound_array_copy.reverse()
	sound_array_copy.push({note:-4,diesis:true,A_pos:[],A_neg:[]})
	segment.sound=sound_array_copy
	//reflect iT and Fr
	//calculate iTs
	let iT_list = []
	let time_array_copy=JSON.parse(JSON.stringify(segment.time))
	let fraction_array_copy=JSON.parse(JSON.stringify(segment.fraction))
	for (let i = 0; i < time_array_copy.length-1 ; i++) {
		let pulse = time_array_copy[i].P
		let fraction = time_array_copy[i].F
		let next_pulse = time_array_copy[i+1].P
		let next_fraction = time_array_copy[i+1].F
		let current_fraction=fraction_array_copy.find(fraction=>{return fraction.stop>pulse})
		//calculate the dT value to be written
		let value = (next_pulse - pulse)  * current_fraction.D / current_fraction.N + (next_fraction - fraction)
		iT_list.push(value)
	}
	iT_list.reverse()
	fraction_array_copy.reverse()
	let start_pulse=0
	fraction_array_copy.forEach((fraction,index)=>{
		let Fr_range=fraction.stop-fraction.start
		fraction_array_copy[index].start=start_pulse
		fraction_array_copy[index].stop=start_pulse+Fr_range
		start_pulse+=Fr_range
	})
	time_array_copy=[]
	//prev_fraction = 0
	time_array_copy.push({"P":0,"F":0})
	iT_list.forEach((iT,index)=>{
		let current_pulse = time_array_copy[index].P
		let current_fraction = time_array_copy[index].F
		let current_Fr=fraction_array_copy.find(fraction=>{return fraction.stop>current_pulse})
		let d_pulse = (Math.floor((current_fraction+iT)/current_Fr.D))*current_Fr.N
		let next_pulse=current_pulse+d_pulse
		let next_fraction = (current_fraction+iT)%current_Fr.D
		time_array_copy.push({"P":next_pulse,"F":next_fraction})
	})
	segment.time=time_array_copy
	segment.fraction=fraction_array_copy
	return true
}

function DATA_h_mirror_element_segment(voice_id,segment_index){
	if(voice_id==-1 || segment_index==-1)return
	let data = DATA_get_current_state_point(true)
	let current_voice_data = data.voice_data.find(item=>{return item.voice_id==voice_id})
	if(current_voice_data==null){
		console.error("Operation error, data not found")
		return
	}
	if(current_voice_data.data.segment_data.length<=segment_index){
		console.error("Operation error, data not found")
		return
	}
	let segment=current_voice_data.data.segment_data[segment_index]
	let success = DATA_calculate_h_mirror_element_segment(segment)
	if(!success){
		APP_info_msg("op_not_succes")
		return
	}
	//calculate midi
	current_voice_data.data.midi_data=DATA_segment_data_to_midi_data(current_voice_data.data.segment_data,current_voice_data.neopulse)
	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_h_mirror_element_segment_column(segment_index){
	if(segment_index==-1)return
	let data = DATA_get_current_state_point(true)
	let find_data=false
	let h_mirror_success=false
	data.voice_data.forEach(voice=>{
		if(voice.blocked){
			find_data=true
			if(voice.data.segment_data.length<=segment_index){
				find_data=false
				return
			}
			let segment=voice.data.segment_data[segment_index]
			let success = DATA_calculate_h_mirror_element_segment(segment)
			if(!success){
				return
			}
			h_mirror_success=true
			//calculate midi
			voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
		}
	})
	if(!find_data){
		console.error("Operation error, data not found")
		return
	}
	if(!h_mirror_success){
		APP_info_msg("op_not_succes")
		return
	}
	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_h_mirror_element_voice(voice_id,by_segment){
	if(voice_id==-1)return
	let data = DATA_get_current_state_point(true)
	let current_voice_data = data.voice_data.find(item=>{return item.voice_id==voice_id})
	if(current_voice_data==null){
		console.error("Operation error, data not found")
		return
	}
	let h_mirror_success=false
	//mirror x segment
	current_voice_data.data.segment_data.forEach((segment,segment_index)=>{
		let success = DATA_calculate_h_mirror_element_segment(segment)
		if(!success){
			return
		}
		h_mirror_success=true
	})
	if(by_segment){
		//nothing else to do
	}else{
		//disconnect voice
		current_voice_data.blocked=false
		//mirror segments order
		if(current_voice_data.data.segment_data.length>1){
			current_voice_data.data.segment_data.reverse()
			h_mirror_success=true
		}
	}
	if(!h_mirror_success){
		APP_info_msg("op_not_succes")
		return
	}
	//calculate midi
	current_voice_data.data.midi_data=DATA_segment_data_to_midi_data(current_voice_data.data.segment_data,current_voice_data.neopulse)
	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

//iT
function DATA_calculate_h_mirror_iT_segment(segment){
	//reflect iT and Fr
	//calculate iTs
	let iT_list = []
	let time_array_copy=JSON.parse(JSON.stringify(segment.time))
	if(time_array_copy.length<3){
		return false
	}
	let fraction_array_copy=JSON.parse(JSON.stringify(segment.fraction))
	for (let i = 0; i < time_array_copy.length-1 ; i++) {
		let pulse = time_array_copy[i].P
		let fraction = time_array_copy[i].F
		let next_pulse = time_array_copy[i+1].P
		let next_fraction = time_array_copy[i+1].F
		let current_fraction=fraction_array_copy.find(fraction=>{return fraction.stop>pulse})
		//calculate the dT value to be written
		let value = (next_pulse - pulse)  * current_fraction.D / current_fraction.N + (next_fraction - fraction)
		iT_list.push(value)
	}
	iT_list.reverse()
	fraction_array_copy.reverse()
	let start_pulse=0
	fraction_array_copy.forEach((fraction,index)=>{
		let Fr_range=fraction.stop-fraction.start
		fraction_array_copy[index].start=start_pulse
		fraction_array_copy[index].stop=start_pulse+Fr_range
		start_pulse+=Fr_range
	})
	time_array_copy=[]
	//prev_fraction = 0
	time_array_copy.push({"P":0,"F":0})
	iT_list.forEach((iT,index)=>{
		let current_pulse = time_array_copy[index].P
		let current_fraction = time_array_copy[index].F
		let current_Fr=fraction_array_copy.find(fraction=>{return fraction.stop>current_pulse})
		let d_pulse = (Math.floor((current_fraction+iT)/current_Fr.D))*current_Fr.N
		let next_pulse=current_pulse+d_pulse
		let next_fraction = (current_fraction+iT)%current_Fr.D
		time_array_copy.push({"P":next_pulse,"F":next_fraction})
	})
	segment.time=time_array_copy
	segment.fraction=fraction_array_copy
	//mirror L structure too
	let sound_array_copy=JSON.parse(JSON.stringify(segment.sound))
	let max_index = sound_array_copy.length-1
	//indexing L
	let L_index_list = []
	sound_array_copy.forEach((sound,index_sound)=>{
		if(sound.note==-3){
			L_index_list.push(index_sound)
		}
	})
	//deleting L
	let copy_L_index_list=JSON.parse(JSON.stringify(L_index_list))
	copy_L_index_list.reverse()
	var copy_L_index_sound=[]
	copy_L_index_list.forEach(index_sound=>{
		let removed= sound_array_copy.splice(index_sound,1)
		copy_L_index_sound.push(removed[0])
	})
	//mirroring index L
	let new_L_index_list = L_index_list.map(item=>{return max_index-item})
	new_L_index_list.reverse()
	copy_L_index_sound.reverse()
	new_L_index_list.forEach((L_index,i)=>{
		sound_array_copy.splice(L_index,0,copy_L_index_sound[i])
	})
	segment.sound=sound_array_copy
	return true
}

function DATA_h_mirror_iT_segment(voice_id,segment_index){
	if(voice_id==-1 || segment_index==-1)return
	let data = DATA_get_current_state_point(true)
	let current_voice_data = data.voice_data.find(item=>{return item.voice_id==voice_id})
	if(current_voice_data==null){
		console.error("Operation error, data not found")
		return
	}
	if(current_voice_data.data.segment_data.length<=segment_index){
		console.error("Operation error, data not found")
		return
	}
	let success = DATA_calculate_h_mirror_iT_segment(current_voice_data.data.segment_data[segment_index])
	if(!success){
		APP_info_msg("op_not_succes")
		return
	}
	//calculate midi
	current_voice_data.data.midi_data=DATA_segment_data_to_midi_data(current_voice_data.data.segment_data,current_voice_data.neopulse)
	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_h_mirror_iT_segment_column(segment_index){
	if(segment_index==-1)return
	let data = DATA_get_current_state_point(true)
	let find_data=false
	let h_mirror_success=false
	data.voice_data.forEach(voice=>{
		if(voice.blocked){
			find_data=true
			if(voice.data.segment_data.length<=segment_index){
				find_data=false
				return
			}
			let segment=voice.data.segment_data[segment_index]
			let success = DATA_calculate_h_mirror_iT_segment(segment)
			if(!success){
				return
			}
			h_mirror_success=true
			//calculate midi
			voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
		}
	})
	if(!find_data){
		console.error("Operation error, data not found")
		return
	}
	if(!h_mirror_success){
		APP_info_msg("op_not_succes")
		return
	}
	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_h_mirror_iT_voice(voice_id,by_segment){
	if(voice_id==-1)return
	let data = DATA_get_current_state_point(true)
	let current_voice_data = data.voice_data.find(item=>{return item.voice_id==voice_id})
	if(current_voice_data==null){
		console.error("Operation error, data not found")
		return
	}
	let h_mirror_success=false
	//mirror x segment
	current_voice_data.data.segment_data.forEach((segment,segment_index)=>{
		let success = DATA_calculate_h_mirror_iT_segment(segment)
		if(!success){
			return
		}
		h_mirror_success=true
	})
	if(by_segment){
		//nothing else to do
	}else{
		//disconnect voice
		current_voice_data.blocked=false
		//copy Note lines
		let original_sound_list = []
		current_voice_data.data.segment_data.forEach((segment,segment_index)=>{
			original_sound_list=original_sound_list.concat(JSON.parse(JSON.stringify(segment.sound)))
		})
		//mirror segments order
		if(current_voice_data.data.segment_data.length>1){
			current_voice_data.data.segment_data.reverse()
			h_mirror_success=true
		}
		//overwrite note order with old one
		current_voice_data.data.segment_data.forEach((segment,segment_index)=>{
			segment.sound.forEach((sound,sound_index)=>{
				//L follow iT movement
				if(sound.note>-3){
					//sub with old one
					let current_sound=original_sound_list.shift()
					while (current_sound.note<=-3) {
						current_sound=original_sound_list.shift()
					}
					segment.sound[sound_index]=current_sound
				}
			})
		})
	}
	if(!h_mirror_success){
		APP_info_msg("op_not_succes")
		return
	}
	//calculate midi
	current_voice_data.data.midi_data=DATA_segment_data_to_midi_data(current_voice_data.data.segment_data,current_voice_data.neopulse)
	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

//N
//Na
function DATA_calculate_h_mirror_Na_segment(segment,mirror_se){
	//reflect notes
	let sound_array_copy = JSON.parse(JSON.stringify(segment.sound))
	let sound_array_mir=[]
	//mirror only note, no s,e,L,end_note
	let from_n=-1
	if(mirror_se){
		//no mirror L
		from_n=-3
	}
	sound_array_copy.forEach(sound=>{
		if(sound.note>from_n){
			sound_array_mir.push(sound)
		}
	})
	if(sound_array_mir.length<2){
		return false
	}
	//mirror
	sound_array_mir.reverse()
	//reinsert
	let position=0
	sound_array_copy.forEach((sound,index_sound)=>{
		if(sound.note>from_n){
			sound_array_copy[index_sound]=sound_array_mir[position]
			position++
		}
	})
	segment.sound=sound_array_copy
	return true
}

//Ngm
function DATA_calculate_h_mirror_Ngm_segment(segment,mirror_se,Psg_segment_start,neopulse,scale_sequence){
	let [N_NP,D_NP]=[neopulse.N,neopulse.D]
	let h_mirror_success=false
	let sound_grade_list=[]
	let scale_list=[]
	let idea_scale_list=DATA_get_idea_scale_list()
	segment.sound.find((sound,index)=>{
		//traduction
		let time = segment.time[index]
		let fraction = segment.fraction.find(fraction=>{return fraction.stop>time.P})
		if (typeof(fraction) == "undefined"){
			//end segment
			return true
		}
		//calculating position
		let frac_p = fraction.N/fraction.D
		let Pa_equivalent = (Psg_segment_start+time.P) * N_NP/D_NP + time.F* frac_p * N_NP/D_NP
		let current_scale=DATA_get_Pa_eq_scale(Pa_equivalent,scale_sequence)
		//inside a scale module
		if(sound.note<0){
			sound_grade_list.push({note:sound.note,diesis:sound.diesis,A_pos:[],A_neg:[]})
		}else{
			let [grade,delta,reg]=DATA_calculate_absolute_note_to_scale(sound.note,sound.diesis,current_scale,idea_scale_list)
			sound_grade_list.push({note:sound.note,grade:grade,delta:delta,reg:reg,diesis:sound.diesis,A_pos:sound.A_pos,A_neg:sound.A_neg,original_index_sound:index})
		}
		//handy store the calculated scale for later calculations
		scale_list.push(current_scale)
	})
	let sound_grade_list_mir=[]
	//mirror only note, no s,e,L,end_note
	let from_n=-1
	if(mirror_se){
		//no mirror L
		from_n=-3
	}
	sound_grade_list.forEach(sound_grade=>{
		if(sound_grade.note>from_n){
			sound_grade_list_mir.push(sound_grade)
		}
	})
	if(sound_grade_list_mir.length<2){
		return [false,false]
	}
	//mirror
	sound_grade_list_mir.reverse()
	let position=0
	let outside_range=false
	segment.sound.forEach((sound,index_sound)=>{
		//max_index is outside scale range
		if(sound.note>from_n){
			let new_note_number=sound_grade_list_mir[position].note
			//recalculate if is a note
			if(new_note_number>=0){
				let current_scale=scale_list[index_sound]
				let old_scale=scale_list[sound_grade_list_mir[position].original_index_sound]
				let result = DATA_calculate_scale_note_to_absolute(sound_grade_list_mir[position].grade,sound_grade_list_mir[position].delta,sound_grade_list_mir[position].reg,current_scale,idea_scale_list)
				if(result.outside_range) outside_range=true
				//A
				let provv_sound={note:new_note_number,diesis:sound_grade_list_mir[position].diesis,A_pos:sound_grade_list_mir[position].A_pos,A_neg:sound_grade_list_mir[position].A_neg}
				let new_A = DATA_calculate_A_from_sound_grade_change(provv_sound,result.note,null,old_scale,current_scale,idea_scale_list)
				sound.note=result.note
				sound.diesis=sound_grade_list_mir[position].diesis
				sound.A_pos=new_A.A_pos
				sound.A_neg=new_A.A_neg
				let result_A = DATA_verify_sound_A(sound)
				if(result_A) outside_range=true
			}else{
				segment.sound[index_sound].note=new_note_number
				segment.sound[index_sound].diesis=sound_grade_list_mir[position].diesis
				segment.sound[index_sound].A_pos=[]
				segment.sound[index_sound].A_neg=[]
			}
			position++
		}
	})
	return [true,outside_range]
}

function DATA_h_mirror_N_segment(voice_id,segment_index,mirror_se,type_N){
	if(voice_id==-1 || segment_index==-1)return
	let data = DATA_get_current_state_point(true)
	let current_voice_data = data.voice_data.find(item=>{return item.voice_id==voice_id})
	if(current_voice_data==null){
		console.error("Operation error, data not found")
		return
	}
	if(current_voice_data.data.segment_data.length<=segment_index){
		console.error("Operation error, data not found")
		return
	}
	let success = false
	let outside_range = false
	if(type_N=="Na")success=DATA_calculate_h_mirror_Na_segment(current_voice_data.data.segment_data[segment_index],mirror_se)
	if(type_N=="Ngm"){
		let Psg_segment_start=0
		for (let i = 0; i < segment_index; i++) {
			Psg_segment_start+=current_voice_data.data.segment_data[i].time.slice(-1)[0].P
		}
		let scale_sequence= data.scale.scale_sequence;
		[success,outside_range]=DATA_calculate_h_mirror_Ngm_segment(current_voice_data.data.segment_data[segment_index],mirror_se,Psg_segment_start,current_voice_data.neopulse,scale_sequence)
	}
	if(!success){
		APP_info_msg("op_not_succes")
		return
	}
	if(outside_range){
		APP_info_msg("out_N_range")
	}
	//calculate midi
	current_voice_data.data.midi_data=DATA_segment_data_to_midi_data(current_voice_data.data.segment_data,current_voice_data.neopulse)
	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_h_mirror_N_segment_column(segment_index,mirror_se,type_N){
	if(segment_index==-1)return
	let data = DATA_get_current_state_point(true)
	let find_data=false
	let h_mirror_success=false
	let outside_range = false
	data.voice_data.forEach(voice=>{
		if(voice.blocked){
			find_data=true
			if(voice.data.segment_data.length<=segment_index){
				find_data=false
				return
			}
			let success = false
			let o_r=false
			if(type_N=="Na")success=DATA_calculate_h_mirror_Na_segment(voice.data.segment_data[segment_index],mirror_se)
			if(type_N=="Ngm"){
				let Psg_segment_start=0
				for (let i = 0; i < segment_index; i++) {
					Psg_segment_start+=voice.data.segment_data[i].time.slice(-1)[0].P
				}
				let scale_sequence= data.scale.scale_sequence;
				[success,o_r]=DATA_calculate_h_mirror_Ngm_segment(voice.data.segment_data[segment_index],mirror_se,Psg_segment_start,voice.neopulse,scale_sequence)
				if(o_r)outside_range=true
			}
			if(!success){
				return
			}
			h_mirror_success=true
			//calculate midi
			voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
		}
	})
	if(!find_data){
		console.error("Operation error, data not found")
		return
	}
	if(!h_mirror_success){
		APP_info_msg("op_not_succes")
		return
	}
	if(outside_range){
		APP_info_msg("out_N_range")
	}
	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_h_mirror_N_voice(voice_id,mirror_se,by_segment,type_N){
	if(voice_id==-1)return
	let data = DATA_get_current_state_point(true)
	let current_voice_data = data.voice_data.find(item=>{return item.voice_id==voice_id})
	if(current_voice_data==null){
		console.error("Operation error, data not found")
		return
	}
	let h_mirror_success=false
	let outside_range = false
	let scale_sequence= data.scale.scale_sequence
	if(by_segment){
		let Psg_segment_start=0
		current_voice_data.data.segment_data.forEach((segment,segment_index)=>{
			let success = false
			let o_r=false
			if(type_N=="Na")success=DATA_calculate_h_mirror_Na_segment(segment,mirror_se)
			if(type_N=="Ngm"){
				[success,o_r] = DATA_calculate_h_mirror_Ngm_segment(segment,mirror_se,Psg_segment_start,current_voice_data.neopulse,scale_sequence)
				if(o_r)outside_range=true
			}
			Psg_segment_start+=segment.time.slice(-1)[0].P
			if(!success){
				return
			}
			h_mirror_success=true
		})
	}else{
		//copy Note lines
		let provv_segment= DATA_concat_segments(current_voice_data)
		let success=false
		let o_r=false
		if(type_N=="Na")success = DATA_calculate_h_mirror_Na_segment(provv_segment,mirror_se)
		if(type_N=="Ngm"){
			[success,o_r] = DATA_calculate_h_mirror_Ngm_segment(provv_segment,mirror_se,0,current_voice_data.neopulse,scale_sequence)
			if(o_r)outside_range=true
		}
		if(success){
			h_mirror_success=true
			//overwrite new note/diesis list inside old structure
			let index=0
			let from_n=-1
			if(mirror_se){
				//no mirror L
				from_n=-3
			}
			current_voice_data.data.segment_data.forEach((segment,segment_index)=>{
				segment.sound.forEach((sound,sound_index)=>{
					//not touching L,e,s,end_range
					if(sound.note>from_n){
						//sub with old one
						segment.sound[sound_index]=provv_segment.sound[index]
					}
					if(sound.note!=-4)index++
				})
			})
		}
	}
	if(!h_mirror_success){
		APP_info_msg("op_not_succes")
		return
	}
	if(outside_range){
		APP_info_msg("out_N_range")
	}
	//calculate midi
	current_voice_data.data.midi_data=DATA_segment_data_to_midi_data(current_voice_data.data.segment_data,current_voice_data.neopulse)
	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

//iN
//iNa
function DATA_calculate_h_mirror_iNa_segment(segment,mirror_se){
	//reflect i notes
	let sound_array_copy = JSON.parse(JSON.stringify(segment.sound))
	//let note_array_copy = JSON.parse(JSON.stringify(segment.sound.map(s=>{return s.note})))
	let outside_range=false
	//rotate only note, no s,e,L,end_note
	let from_n=-1
	if(mirror_se){
		//no mirror L
		from_n=-3
	}
	let iNa_list=DATA_calculate_iNa_from_Na_list(JSON.parse(JSON.stringify(segment.sound.map(s=>{return s.note}))))
	let iNa_list_mir=[]
	let sound_array_mir=[]
	sound_array_copy.forEach((sound,index_sound)=>{
		if(sound.note>from_n){
			iNa_list_mir.push(iNa_list[index_sound])
			sound_array_mir.push(sound_array_copy[index_sound])
		}
	})
	if(iNa_list_mir.length<2){
		return [false,false]
	}
	//mirror
	iNa_list_mir.reverse()
	sound_array_mir.reverse()
	if(!mirror_se){
		//last element is starting note
		let starting_note=iNa_list_mir.pop()
		let starting_sound=sound_array_mir.pop()
		iNa_list_mir.unshift(starting_note)
		sound_array_mir.unshift(starting_sound)
	}else{
		//find starting note
		let last_sound=true
		let last_iNa=null
		sound_array_copy.find((sound,index_sound)=>{
			if(sound.note>-1){
				last_iNa=sound.note
				last_sound=sound_array_copy[index_sound]
				return true
			}
		})
		if(last_iNa!=null){
			//note to mirror, not only s,e
			iNa_list_mir.forEach((iNa,index_iNa)=>{
				if(iNa!="e" && iNa!="s"){
					let provv_iNa=iNa
					let provv_sound = sound_array_mir[index_iNa]
					iNa_list_mir[index_iNa]=last_iNa
					sound_array_mir[index_iNa]=last_sound
					last_iNa=provv_iNa
					last_sound=provv_sound
				}
			})
		}
	}
	//reinsert
	let position=0
	sound_array_copy.forEach((sound,index_sound)=>{
		if(sound.note>from_n){
			iNa_list[index_sound]=iNa_list_mir[position]
			sound_array_copy[index_sound]=sound_array_mir[position]
			position++
		}
	})
	let [n_a_c,o_r]=DATA_calculate_Na_from_iNa_list(iNa_list)
	n_a_c.forEach((note,index)=>{sound_array_copy[index].note=note})
	outside_range=o_r
	segment.sound=sound_array_copy
	segment.sound.forEach(sound=>{
		let result_A = DATA_verify_sound_A(sound)
		if(result_A) outside_range=true
	})
	return [true,outside_range]
}

//iNgm
function DATA_calculate_h_mirror_iNgm_segment(segment,mirror_se,Psg_segment_start,neopulse,scale_sequence){
	let [N_NP,D_NP]=[neopulse.N,neopulse.D]
	let h_mirror_success=false
	let sound_grade_list=[]
	let scale_list=[]
	let idea_scale_list=DATA_get_idea_scale_list()
	segment.sound.find((sound,index)=>{
		//traduction
		let time = segment.time[index]
		let fraction = segment.fraction.find(fraction=>{return fraction.stop>time.P})
		if (typeof(fraction) == "undefined"){
			//end segment
			return true
		}
		//calculating position
		let frac_p = fraction.N/fraction.D
		let Pa_equivalent = (Psg_segment_start+time.P) * N_NP/D_NP + time.F* frac_p * N_NP/D_NP
		let current_scale=DATA_get_Pa_eq_scale(Pa_equivalent,scale_sequence)
		if(sound.note<0){
			sound_grade_list.push({note:sound.note,diesis:sound.diesis,A_pos:[],A_neg:[]})
		}else{
			let [grade,delta,reg]=DATA_calculate_absolute_note_to_scale(sound.note,sound.diesis,current_scale,idea_scale_list)
			sound_grade_list.push({note:sound.note,grade:grade,delta:delta,reg:reg,diesis:sound.diesis,A_pos:sound.A_pos,A_neg:sound.A_neg})
		}
		//handy store the calculated scale for later calculations
		scale_list.push(current_scale)
	})
	//calc iN
	let i_sound_grade_list=DATA_calculate_iNgm_from_Ngm_list(sound_grade_list,scale_list)
	//not note: {note:Na,diesis:diesis}) and A_pos neg
	//first note: {note:Na,grade:grade,delta:delta,reg:reg,diesis:diesis}
	//d_grad : {note:Na,iNgm:d_grad,diesis:diesis}
	let from_n=-1
	if(mirror_se){
		//no mirror L
		from_n=-3
	}
	let i_sound_grade_list_mir=[]
	i_sound_grade_list.forEach((i_sound_grade,index_sound)=>{
		if(i_sound_grade.note>from_n){
			i_sound_grade.original_index_sound=index_sound
			i_sound_grade_list_mir.push(i_sound_grade)
		}
	})
	if(i_sound_grade_list_mir.length<2){
		return [false,false]
	}
	//mirror
	i_sound_grade_list_mir.reverse()
	if(!mirror_se){
		//last element is starting note
		let starting_sound=i_sound_grade_list_mir.pop()
		i_sound_grade_list_mir.unshift(starting_sound)
	}else{
		//find starting note
		let last_i_sound_grade=null
		sound_grade_list.find(sound_grade=>{
			if(sound_grade.note>-1){
				last_i_sound_grade=sound_grade//first is a sound grade
				return true
			}
		})
		if(last_i_sound_grade!=null){
			//note to mirror, not only s,e
			//first element is starting note (last_sound_grade) and every other note shift
			i_sound_grade_list_mir.forEach((i_sound_grade,index)=>{
				if(i_sound_grade.note>-1){
					let provv_i_sound_grade=JSON.parse(JSON.stringify(i_sound_grade))
					i_sound_grade_list_mir[index]=last_i_sound_grade
					last_i_sound_grade=provv_i_sound_grade
				}
			})
		}
	}
	//reinsert
	let position=0
	i_sound_grade_list.forEach((i_sound_grade,index)=>{
		if(i_sound_grade.note>from_n){
			i_sound_grade_list[index]=i_sound_grade_list_mir[position]
			position++
		}
	})
	//calculate new note
	let [Na_list,outside_range]=DATA_calculate_Na_from_iNgm_list(i_sound_grade_list,scale_list,idea_scale_list)
	Na_list.forEach((note_item,index_sound)=>{
		let new_note_number=note_item.note
		//recalculate if is a note
		if(new_note_number>=0){
			let current_scale=scale_list[index_sound]
			let old_scale=scale_list[i_sound_grade_list[index_sound].original_index_sound]
			//A
			let provv_sound={note:i_sound_grade_list[index_sound].note,diesis:note_item.diesis,A_pos:i_sound_grade_list[index_sound].A_pos,A_neg:i_sound_grade_list[index_sound].A_neg}
			let new_A = DATA_calculate_A_from_sound_grade_change(provv_sound,new_note_number,null,old_scale,current_scale,idea_scale_list)
			segment.sound[index_sound].note=new_note_number
			segment.sound[index_sound].diesis=note_item.diesis
			segment.sound[index_sound].A_pos=new_A.A_pos
			segment.sound[index_sound].A_neg=new_A.A_neg
			let result_A = DATA_verify_sound_A(segment.sound[index_sound])
			if(result_A) outside_range=true
		}else{
			segment.sound[index_sound].note=new_note_number
			segment.sound[index_sound].diesis=note_item.diesis
			segment.sound[index_sound].A_pos=[]
			segment.sound[index_sound].A_neg=[]
		}
	})
	return [true,outside_range]
}

function DATA_h_mirror_iN_segment(voice_id,segment_index,mirror_se,type_N){
	if(voice_id==-1 || segment_index==-1)return
	let data = DATA_get_current_state_point(true)
	let current_voice_data = data.voice_data.find(item=>{return item.voice_id==voice_id})
	if(current_voice_data==null){
		console.error("Operation error, data not found")
		return
	}
	if(current_voice_data.data.segment_data.length<=segment_index){
		console.error("Operation error, data not found")
		return
	}
	let success = false
	let outside_range=false
	if(type_N=="Na")[success,outside_range]=DATA_calculate_h_mirror_iNa_segment(current_voice_data.data.segment_data[segment_index],mirror_se)
	if(type_N=="Ngm"){
		let Psg_segment_start=0
		for (let i = 0; i < segment_index; i++) {
			Psg_segment_start+=current_voice_data.data.segment_data[i].time.slice(-1)[0].P
		}
		let scale_sequence= data.scale.scale_sequence;
		[success,outside_range]=DATA_calculate_h_mirror_iNgm_segment(current_voice_data.data.segment_data[segment_index],mirror_se,Psg_segment_start,current_voice_data.neopulse,scale_sequence)
	}
	if(!success){
		APP_info_msg("op_not_succes")
		return
	}
	if(outside_range){
		APP_info_msg("out_N_range")
	}
	//calculate midi
	current_voice_data.data.midi_data=DATA_segment_data_to_midi_data(current_voice_data.data.segment_data,current_voice_data.neopulse)
	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_h_mirror_iN_segment_column(segment_index,mirror_se,type_N){
	if(segment_index==-1)return
	let data = DATA_get_current_state_point(true)
	let find_data=false
	let outside_range=false
	let h_mirror_success=false
	data.voice_data.forEach(voice=>{
		if(voice.blocked){
			find_data=true
			if(voice.data.segment_data.length<=segment_index){
				find_data=false
				return
			}
			let success=false
			let out_r=false
			if(type_N=="Na")[success,out_r] = DATA_calculate_h_mirror_iNa_segment(voice.data.segment_data[segment_index],mirror_se)
			if(type_N=="Ngm"){
				let Psg_segment_start=0
				for (let i = 0; i < segment_index; i++) {
					Psg_segment_start+=voice.data.segment_data[i].time.slice(-1)[0].P
				}
				let scale_sequence= data.scale.scale_sequence;
				[success,out_r]=DATA_calculate_h_mirror_iNgm_segment(voice.data.segment_data[segment_index],mirror_se,Psg_segment_start,voice.neopulse,scale_sequence)
			}
			if(!success){
				return
			}
			if(out_r)outside_range=true
			h_mirror_success=true
			//calculate midi
			voice.data.midi_data=DATA_segment_data_to_midi_data(voice.data.segment_data,voice.neopulse)
		}
	})
	if(!find_data){
		console.error("Operation error, data not found")
		return
	}
	if(!h_mirror_success){
		APP_info_msg("op_not_succes")
		return
	}
	if(outside_range){
		APP_info_msg("out_N_range")
	}
	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_h_mirror_iN_voice(voice_id,mirror_se,by_segment,type_N){
	if(voice_id==-1)return
	let data = DATA_get_current_state_point(true)
	let current_voice_data = data.voice_data.find(item=>{return item.voice_id==voice_id})
	if(current_voice_data==null){
		console.error("Operation error, data not found")
		return
	}
	let h_mirror_success=false
	let outside_range=false
	let scale_sequence= data.scale.scale_sequence
	if(by_segment){
		let Psg_segment_start=0
		current_voice_data.data.segment_data.forEach((segment,segment_index)=>{
			let success = false
			let out_r=false
			if(type_N=="Na")[success,out_r]=DATA_calculate_h_mirror_iNa_segment(segment,mirror_se)
			if(type_N=="Ngm"){
				[success,out_r] = DATA_calculate_h_mirror_iNgm_segment(segment,mirror_se,Psg_segment_start,current_voice_data.neopulse,scale_sequence)
			}
			Psg_segment_start+=segment.time.slice(-1)[0].P
			if(!success){
				return
			}
			if(out_r)outside_range=true
			h_mirror_success=true
		})
	}else{
		//copy Note lines
		let provv_segment= DATA_concat_segments(current_voice_data)
		let success=false
		let out_r=false
		if(type_N=="Na")[success,out_r] = DATA_calculate_h_mirror_iNa_segment(provv_segment,mirror_se)
		if(type_N=="Ngm"){
			[success,out_r] = DATA_calculate_h_mirror_iNgm_segment(provv_segment,mirror_se,0,current_voice_data.neopulse,scale_sequence)
		}
		if(success){
			if(out_r)outside_range=true
			h_mirror_success=true
			//overwrite new note/diesis list inside old structure
			let index=0
			let from_n=-1
			if(mirror_se){
				//no mirror L
				from_n=-3
			}
			current_voice_data.data.segment_data.forEach((segment,segment_index)=>{
				segment.sound.forEach((sound,sound_index)=>{
					//not touching L
					if(sound.note>from_n){
						//sub with old one
						segment.sound[sound_index]=provv_segment.sound[index]
					}
					if(sound.note!=-4)index++
				})
			})
		}
	}
	if(!h_mirror_success){
		APP_info_msg("op_not_succes")
		return
	}
	if(outside_range){
		APP_info_msg("out_N_range")
	}
	//calculate midi
	current_voice_data.data.midi_data=DATA_segment_data_to_midi_data(current_voice_data.data.segment_data,current_voice_data.neopulse)
	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

//function collapse all segments of one voice into a single one ONLY for operation purposes
function DATA_concat_segments(voice){
	let provv_voice=JSON.parse(JSON.stringify(voice))
	//copy Note lines
	let provv_sound = []
	let provv_time=[]
	let provv_fraction=[]
	let Ps_start_segment=0
	//no need to control end segment
	provv_voice.data.segment_data.forEach((segment,segment_index)=>{
		segment.sound.pop()
		provv_sound = provv_sound.concat(segment.sound)
		segment.fraction.forEach(fraction=>{
			fraction.start+=Ps_start_segment
			fraction.stop+=Ps_start_segment
		})
		provv_fraction= provv_fraction.concat(segment.fraction)
		let Ls_segment=segment.time.pop().P
		segment.time.forEach(time=>{
			time.P+=Ps_start_segment
		})
		provv_time = provv_time.concat(segment.time)
		Ps_start_segment+=Ls_segment
	})
	let provv_segment= {sound:provv_sound,time:provv_time,fraction:provv_fraction}
	return provv_segment
}
