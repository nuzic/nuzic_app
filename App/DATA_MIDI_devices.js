function DATA_on_MIDI_Success(midiAccess) {
	midi_input_devices = Array.from(midiAccess.inputs.values())
	midi_output_devices = Array.from(midiAccess.outputs.values())
	//DATA_load_preferences_file_onlyMIDI()
	console.log("MIDI ready")
}

function DATA_update_midi_devices(load_preferences=false){
	DATA_clear_midi_inputs()
	if (navigator.requestMIDIAccess) {
		navigator.requestMIDIAccess().then( DATA_on_MIDI_Success, DATA_on_MIDI_Failure)
		if(load_preferences)DATA_load_preferences_file_onlyMIDI()
	}
}

function DATA_on_MIDI_Failure(msg) {
	console.log("Failed to get MIDI access - " + msg)
	let MIDI_out=document.getElementById("App_BNav_V_midi_output")
	let MIDI_in=document.getElementById("App_BNav_keyboard_midi_input")
	let MIDI_out_label = MIDI_out.querySelector("label")
	let MIDI_in_label = MIDI_in.querySelector("label")
	MIDI_out_label.innerHTML = "no MIDI access"
	MIDI_in_label.innerHTML = "no MIDI access"
	MIDI_in.setAttribute('onclick',"")
	MIDI_out.setAttribute('onclick',"")
}

function DATA_clear_midi_inputs(){
	midi_input_devices.forEach(input => {
		input.onmidimessage = null
	})
}

function DATA_select_midi_input(midi_input_ID){
	let found=midi_input_devices.find(input=>{
		return input.id==midi_input_ID
	})
	if(found){
		found.onmidimessage = (msg) => {
			//console.log("command: ",msg.data[0]," ,note :",msg.data[1],"velocity :",msg.data[2])
			//var instrument_index=0
			//var nzc_note = msg.data[1]-TET
			var nzc_note = msg.data[1]-12
			//var freq_note = DATA_note_to_Hz(nzc_note,TET)

			//button play pause stop
			// if(msg.data[0]==191 && msg.data[1]>106){
				//play NOPE
			// 	var playbutton = document.getElementById("play_button")
				//playbutton.click()
			// 	return
			// }
			// if(msg.data[0]==191 && msg.data[1]>105){
				//Stop NOPE
			// 	APP_stop()
			// 	return
			// }
			//verify is a note
			if((msg.data[0]<128 || msg.data[0]>159) && msg.data[0]!=128)return //16 channels //144 vel 0 is note off
			var focused_elem = document.activeElement
			if(msg.data[2]==0 || (msg.data[0]>=128 && msg.data[0]<=143)){
				//128 ch0 ---> 143 ch15
				//key up
				Add_MIDI_note_to_RE(focused_elem,nzc_note,false)
			}
			if(msg.data[2]!=0 && (msg.data[0]>=144 && msg.data[0]<=159)){
				//144 ch0 ---> 159 ch15
				//keydown
				//console.log("key down" + msg.data[2])
				//console.log("midi note "+msg.data[1]+"  nuzic note "+nzc_note)
				//add a note to the current voice segment
				Add_MIDI_note_to_RE(focused_elem,nzc_note,true,1,msg.data[2]/128)
			}
			//160 ch0 ---> 175 ch15 Polyphonic Aftertouch
			//console.log(msg)
			//Play_this_note(iA_elements.down_Na,"Na")
		}
	}
}

function Add_MIDI_note_to_RE(focused_elem, nzc_note , down , iT, volume) {
	//play or stop
	if(down){
		BNAV_play_note(nzc_note,true)
		BNAV_virtual_keyboard_key_down(nzc_note)
		BNAV_try_insert_note_duration(nzc_note)
	}else{
		BNAV_play_note(nzc_note,false)
		BNAV_virtual_keyboard_key_up(nzc_note)
	}
}





