//SEGMENT LINES
let tab_pressed=false
let shift_pressed=false
function RE_move_focus(evt,element){
	tab_pressed=false
	shift_pressed=false
	//use XXX
	//if(element.value!=previous_value)element.setAttribute("focus","left")
	//to avoid double focus problems
	switch(evt.keyCode){
	case 37:
		// Key left.
		RE_focus_next_index(element,-1)
		evt.preventDefault()
		break
	case 38:
		// Key up.
		RE_focus_up(element)
		evt.preventDefault();
		break
	case 39:
		// Key right.
		RE_focus_next_index(element,1)
		evt.preventDefault();
		break
	case 40:
		// Key down.
		RE_focus_down(element)
		evt.preventDefault();
		break
	case 9:
		//tab
		evt.preventDefault()
		tab_pressed=true
		if(evt.shiftKey) {
			//shift was down when tab was pressed
			shift_pressed=true
			let [previous_element,,]=_find_prev_filled_element(element)
			if(previous_element!=null){
				RE_focus_next_index(previous_element,0)
			}else{
				let point_at_element = element
				RE_focus_next_index(point_at_element,-1)
			}
		}else{
			//tab
			//find next filled element
			//if undefined, maybe is last one
			let elements_list = [...element.parentNode.querySelectorAll("input")]
			let element_index=elements_list.indexOf(element)
			let next_element= elements_list.find((item,index)=>{
				return item.value!="" && index>element_index
			})
			if(next_element!=null){
				RE_focus_next_index(next_element,0)
			}else{
				let elements_list = [... element.parentNode.querySelectorAll("input")]
				let last_segment_element = elements_list.pop()
				let point_at_element = last_segment_element
				if(last_segment_element==element){
					//jump next segment
					RE_focus_next_index(point_at_element,1)
				}else{
					if(element.classList.contains("RE_note_cell")){
						//if sound line jump next segment
						RE_focus_next_index(point_at_element,1)
					}else{
						if(element.parentElement.classList.contains("App_RE_iT")){
							RE_focus_next_index(last_segment_element,2)
						}else{
							//jump end line
							RE_focus_next_index(point_at_element,0)
						}
					}
				}
			}
		}
		break
	case 13:
		//intro
		//if element == empty -> focus next index
		//else focus +1
		if(!element.value){
			let elements_list = [...element.parentNode.querySelectorAll("input")]
			let element_index=elements_list.indexOf(element)
			let next_element= elements_list.find((item,index)=>{
				return item.value!="" && index>element_index
			})
			if(next_element!=null){
				RE_focus_next_index(next_element,0)
			}else{
				let last_segment_element = elements_list.pop()
				if(last_segment_element==element){
					RE_focus_next_index(last_segment_element,1)
				}else{
					RE_focus_next_index(last_segment_element,0)
				}
			}
		}else{
			RE_focus_next_index(element,1)
		}
		break
	}
	//preventDefault to avoid scrollbar to move automatically
	function _find_prev_filled_element(element){
		let parent = element.parentNode
		let input_list = [...parent.querySelectorAll("input")]
		let elementIndex = input_list.indexOf(element)
		let index_previous = -1
		for (let i=elementIndex-1; i>=0;i--){
			let value = input_list[i].value
			if(value!="" && value!=end_range){
				//element not void
				index_previous=i
				i=-1
			}
		}
		if (index_previous>=0){
			let distance = elementIndex-index_previous
			return [input_list[index_previous], index_previous, distance]
		}else{
			return [null, -1,-1]
		}
	}
}

//jump focus to next element
function RE_focus_next_index(element,dindex=1){
	//in case element will be deleted...
	let parent_line= element.parentNode
	let input_list = [...parent_line.querySelectorAll("input")]
	//let position_old = RE_input_to_position(element)
	let index_old=input_list.indexOf(element)
	//let info_old = DATA_get_object_index_info(position_old.voice_id,position_old.segment_index,position_old.sound_index)
	let new_column_index=index_old+dindex
	if(new_column_index<0){
		//out of boundary
		//jump previous segment
		let current_segment_obj = element.closest(".App_RE_segment")
		let [index_current_segment,list_segment]=RE_element_index(current_segment_obj,".App_RE_segment")
		let current_line_object = element.closest(".App_RE_segment_line")
		let [index_current_line,]= RE_element_index(current_line_object,".App_RE_segment_line")
		let success=false
		while (!success) {
			if(index_current_segment>0){
				let next_segment_obj = list_segment[index_current_segment-1]
				if(next_segment_obj.querySelector(".App_RE_segment_loader").classList.contains("loading")){
					//move scrollbar in position
					document.querySelector('.App_RE_main_scrollbar_H').scrollLeft-=nuzic_block*2
					RE_checkScroll_H()
				}
				//better
				let next_line_object_list= [...next_segment_obj.querySelectorAll(".App_RE_segment_line")]
				if(next_line_object_list.length==0){
					console.error("Error: Position focus not found try later")
					success=true
					return
				}
				let next_line_object = next_line_object_list[index_current_line]
				let next_focused_list = [...next_line_object.querySelectorAll("input")]
				let next_focused_element = next_focused_list.pop()
				if(next_focused_element==null){
					index_current_segment--
				}else{
					next_focused_element.focus()
					next_focused_element.select()
					success=true
				}
			}else{
				//very first element
				element.blur()
				element.focus()
				document.querySelector('.App_RE_main_scrollbar_H').scrollLeft-=nuzic_block*2
				success=true
			}
		}
		return
	}
	if(new_column_index>=input_list.length){
		//out of boundary
		//jump next segment
		let current_segment_obj = element.closest(".App_RE_segment")
		let [index_current_segment,list_segment]=RE_element_index(current_segment_obj,".App_RE_segment")
		let current_line_object = element.closest(".App_RE_segment_line")
		let [index_current_line,]= RE_element_index(current_line_object,".App_RE_segment_line")
		let success=false
		while (!success) {
			if(index_current_segment<list_segment.length-1){
				let next_segment_obj = list_segment[index_current_segment+1]
				if(next_segment_obj.querySelector(".App_RE_segment_loader").classList.contains("loading")){
					//move scrollbar in position
					document.querySelector('.App_RE_main_scrollbar_H').scrollLeft+=nuzic_block*2
					RE_checkScroll_H()
				}
				//better BUT not perfect
				let next_line_object_list= [...next_segment_obj.querySelectorAll(".App_RE_segment_line")]
				if(next_line_object_list.length==0){
					console.error("Error: Position focus not found try later")
					success=true
					return
				}
				let next_line_object = next_line_object_list[index_current_line]
				let next_focused_element = next_line_object.querySelector("input")
				if(next_focused_element==null){
					index_current_segment++
				}else{
					if(dindex==2)next_focused_element=next_line_object.querySelectorAll("input")[1]
					next_focused_element.focus()
					next_focused_element.select()
					success=true
				}
			}else{
				//very last element
				element.blur()
				element.focus()
				document.querySelector('.App_RE_main_scrollbar_H').scrollLeft+=nuzic_block*2
				success=true
			}
		}
		return
	}
	element.blur()
	if(new_column_index<input_list.length && new_column_index>=0) {
		input_list[new_column_index].focus()
		input_list[new_column_index].select()
	}
}

function RE_focus_down(element,to_A_line=false){
	let list_container = element.parentNode
	let position = RE_input_to_position(element)
	let [index_line,line_list]=RE_element_index(list_container,".App_RE_segment_line:not(.App_RE_A_neg):not(.App_RE_A_pos):not(.App_RE_Tf)")
	//find wich is the next visible label
	if(index_line+1>=line_list.length || list_container.classList.contains("App_RE_Tf")){
		if(to_A_line){
			//maybe iA or other voice or A !! XXX shift??? // XXX XXX XXX XXX XXX XXX
			//let success = RE_focus_iA_element_from_voice_element(element,false)
		}else{
			RE_focus_voice_to_voice(element,position.segment_index,position.column_index,"down")
		}
	}else{
		let next_visible_line= line_list[index_line+1]
		// if(next_visible_line.classList.contains("App_RE_Tf")){
			//calculate position_px
			//var global_zoom = Number(document.querySelector('.App_SNav_select_zoom').value)
		// 	var RE_block = nuzic_block	//*global_zoom
		// 	var position_px = RE_block*position.column_index

		// 	var Fr_input_list = [...next_visible_line.querySelectorAll(".App_RE_inp_box")]
		// 	var Fr_element_index
		// 	Fr_input_list.find((item,index)=>{
		// 		if(item.offsetLeft>position_px)return true
		// 		Fr_element_index=index
		// 		if(item.offsetLeft==position_px)return true
		// 	})
		// 	var Fr_element = Fr_input_list[Fr_element_index]
		// 	Fr_element.focus()
		// 	Fr_element.select()
		// }else{
		RE_focus_on_element_index(position.column_index,next_visible_line)
	}
}

function RE_focus_up(element,to_A_line=false){
	let list_container = element.parentNode
	let position = RE_input_to_position(element)
	let [index_line,line_list]=RE_element_index(list_container,".App_RE_segment_line:not(.App_RE_A_neg):not(.App_RE_A_pos):not(.App_RE_Tf)")
	//find wich is the next visible label
	if(index_line==0){
		if(to_A_line){
			//maybe iA or other voice or A !! XXX shift??? // XXX XXX XXX XXX XXX XXX
			//let success = RE_focus_iA_element_from_voice_element(element,true)
		}else{
			RE_focus_voice_to_voice(element,position.segment_index,position.column_index,"up")
		}
	}else{
		let previous_visible_line= line_list[index_line-1]
		if(!list_container.classList.contains("App_RE_Tf")){
			//maybe to A line here XXX XXX XXX XXX depends
			RE_focus_on_element_index(position.column_index,previous_visible_line)
		}else{
			//going up from fractioning
			let indexPF1=Math.round(element.offsetLeft/nuzic_block)
			RE_focus_on_element_index(indexPF1,previous_visible_line)
		}
	}
}

function RE_focus_voice_to_voice(element,segment_index,column_index,direction){
	let voice_list = [...document.querySelectorAll(".App_RE_voice")]
	let visible_voice_list = voice_list.filter((item)=>{return RE_voice_is_visible(item)})
	let current_voice = element.closest(".App_RE_voice")
	//var current_segment = element.closest(".App_RE_segment")
	let current_voice_number = visible_voice_list.indexOf(current_voice)
	let target_voice = null
	if(direction=="up"){
		//skip Tf line
		target_voice=visible_voice_list[current_voice_number-1]
	}else if(direction=="down"){
		target_voice=visible_voice_list[current_voice_number+1]
	}
	if(target_voice==null)return
	let current_voice_segment_list = [...current_voice.querySelectorAll(".App_RE_segment")]
	let current_segment_position = 0
	for (let i = 0; i < segment_index; i++) {
		//current_segment_position+= [...current_voice_segment_list[i].querySelector(".App_RE_segment_line").querySelectorAll(".App_RE_inp_box")].length+1
		//current_segment_position+= Math.round(current_voice_segment_list[i].clientWidth/nuzic_block)+1
		current_segment_position+= parseInt(current_voice_segment_list[i].getAttribute("n_column"))+1
	}
	//find index column
	let index_total= column_index + current_segment_position
	//find first line next voice
	let target_voice_segment_list = [...target_voice.querySelectorAll(".App_RE_segment")]
	let target_segment_end = 0
	let target_segment_columns = 0
	let target_segment = target_voice_segment_list.find(segment=>{
		//target_segment_columns = [...segment.querySelector(".App_RE_segment_line").querySelectorAll(".App_RE_inp_box")].length
		target_segment_columns=parseInt(segment.getAttribute("n_column"))
		target_segment_end+=target_segment_columns+1
		return target_segment_end-1>index_total
	})
	let new_index = target_segment_columns - (target_segment_end - 1 - index_total)
	if(new_index<0)new_index=0
	//focus in last visible line
	let line_list = [...target_segment.querySelectorAll(".App_RE_segment_line:not(.App_RE_A_neg):not(.App_RE_A_pos):not(.App_RE_Tf)")]
	let target_line=null
	if(direction=="up"){
		target_line = line_list.pop()
	}else if(direction=="down"){
		target_line = line_list.shift()
	}
	let next_list = [...target_line.querySelectorAll("input")]
	if(new_index>next_list.length-1)new_index=next_list.length-1
	//if(new_index<0)new_index=0
	next_list[new_index].focus()
	next_list[new_index].select()
}

function RE_move_focus_A(event,element){
	//like RE_move_focus
	// let position = RE_input_to_position(element)
	// let type=(element.parentNode.classList.contains("App_RE_A_pos"))?"A_pos":"A_neg"
	//let info = DATA_get_object_index_info(position_old.voice_id,position_old.segment_index,position_old.sound_index)
	switch(event.keyCode){
	case 38://UP
		APP_scan_A_suggestions(element,true)
		APP_inpTest_A_line_size(element)
		break
	case 40://DOWN
		APP_scan_A_suggestions(element,false)
		APP_inpTest_A_line_size(element)
		break
	case 37:
		// Key left.
		//
		//focus doesn't work fi value changed, focus called on RE_enter_new_value_A
		if(element.value!=previous_value)element.setAttribute("focus","left")
		RE_focus_next_index(element,-1)
		//RE_focus_on_object_index(position.voice_id,position.segment_index,position.input_index,type,next=-1)
		event.preventDefault()
		break
	case 39:
		// Key right.
		//focus doesn't work fi value changed, focus called on RE_enter_new_value_A
		if(element.value!=previous_value)element.setAttribute("focus","right")
		RE_focus_next_index(element,1)
		// if(element.value!=previous_value)element.dispatchEvent(new Event("change"))
		// RE_focus_on_object_index(position.voice_id,position.segment_index,position.input_index,type,next=1)
		event.preventDefault();
		break
	case 9:
		//tab
		event.preventDefault()
		//tab_pressed=true
		if(event.shiftKey) {
			//shift was down when tab was pressed
			//shift_pressed=true
			if(element.value!=previous_value)element.setAttribute("focus","right")
			RE_focus_next_index(element,-1)
		}else{
			//tab
			if(element.value!=previous_value)element.setAttribute("focus","right")
			RE_focus_next_index(element,1)
		}
		// tab_pressed=false
		// shift_pressed=false
		break
	case 13:
		//intro
		RE_focus_next_index(element,1)
		event.preventDefault()
	default:
	/* USING ORIGINAL FUNCTIONS XXX
		let changed=false
		if(element.value!=previous_value)changed=true
		// // //if(A_list_a==A_list_b)changed=true

		if(event.keyCode==37 || event.keyCode==39 || event.keyCode==9 || event.keyCode==13){
			RE_move_focus(event,element)//prevent default...
			console.log("hello")
		}
		// Key left.
		// Key right.
		// tab
		// intro
		//manually trigger event
		//if((event.keyCode==37 || event.keyCode==39 || event.keyCode==9 || event.keyCode==13) && changed){element.dispatchEvent(new Event("change"));console.log("hello")}
		*/
		break
	}
}

function RE_focus_on_element_index(index,next_list_container){
	let next_list = [...next_list_container.querySelectorAll("input")]
	if(index<0 || index>next_list.length)return
	next_list[index].focus()
	next_list[index].select()
}

//ATT!!! POSITION IS NOT ABSOLUTE
function RE_focus_on_object_index(voice_id,segment_index,sound_index,type,next=0){
	let voice_obj = RE_find_voice_obj_by_id(voice_id)
	let segment_obj_list = [...voice_obj.querySelectorAll(".App_RE_segment")]
	let lineClass = ".App_RE_"+type
	if(segment_index>segment_obj_list.length-1){
		//focus last element
		if(type=="A_pos" || type=="A_neg"){
			//find last element
			let last_s_i=null
			let last_i_i=null
			segment_obj_list.forEach((segment,s_i)=>{
				let inp_line = segment.querySelector(lineClass)
				let inp_list = [...inp_line.querySelectorAll("input")]
				if(inp_list.length!=0){
					last_s_i=s_i
					last_i_i=inp_list.length-1
				}
			})
			RE_focus_on_object_index(voice_id,last_s_i,last_i_i,type,0)
		}else{
			RE_focus_on_object_index(voice_id,segment_obj_list.length-1,type,-1,0)
		}
		return
	}
	let segment_obj = segment_obj_list[segment_index]
	if(segment_obj.querySelector(".App_RE_segment_loader").classList.contains("loading")){
		//move scrollbar in sound_index
		document.querySelector('.App_RE_main_scrollbar_H').scrollLeft=segment_obj.offsetLeft
		RE_checkScroll_H()
	}
	let inp_line = segment_obj.querySelector(lineClass)
	let inp_list = [...inp_line.querySelectorAll("input")]
	let index_focus = null
	if(sound_index==-1){
		index_focus=inp_list.length-1
	}else{
		if(type=="A_pos" || type=="A_neg"){
			index_focus=sound_index+next
		}else{
			let index_no_empty = -1
			inp_list.find((input,index)=>{
				if(input.value!="")index_no_empty++
				if(index_no_empty==sound_index){
					index_focus=index
					return true
				}
				return false
			})
			if(index_focus==null){
				//may be last element of a segment beeing deleted
				let next_segment_index=segment_index+1
				if(segment_obj_list.length>next_segment_index){
					//focus on first column of next segment
					RE_focus_on_object_index(voice_id,next_segment_index,0,type,0)
				}else{
					//focus on last column
					RE_focus_on_object_index(voice_id,segment_index,-1,type,0)
				}
				//console.error("Error: Position focus not found")
				return
			}
			index_focus+=next
		}
	}
	if(index_focus>inp_list.length-1){
		//try next segment
		RE_focus_on_object_index(voice_id,segment_index+1,0,type,0)
		return
	}
	//change index_focus depending on tab or shift+tab
	if(tab_pressed){
		if(shift_pressed){
			let element_index = index_focus-1
			let prev_filled_element_index = -1
			for (let i=element_index-1; i>=0;i--){
				let value = inp_list[i].value
				if(value!=""){
					//element not void
					prev_filled_element_index=i
					i=-1
				}
			}
			index_focus=prev_filled_element_index
			if(index_focus<0){
				RE_focus_next_index(inp_list[0],-1)
				return
			}
		}else{
			let next_filled_element_index =-1
			if(type=="A_pos" || type=="A_neg"){
				next_filled_element_index=index_focus-1
			}else{
				inp_list.find((item,index)=>{
					if(item.value!="" && index>index_focus){
						next_filled_element_index=index
						return true
					}
				})
			}
			index_focus=next_filled_element_index
			if(index_focus<0)index_focus=inp_list.length-1
		}
	}
	tab_pressed=false
	shift_pressed=false
	inp_list[index_focus].focus()
	inp_list[index_focus].select()
	//calculate sound_index
	//7 is voice header
	let input_position= (index_focus-7)*nuzic_block+segment_obj.offsetLeft
	let scrollbarH_left = current_position_PMCRE.RE.X_start
	let container_width=document.querySelector(".App_Working_Space").clientWidth-(nuzic_block*7)
	// let scrollbarH_left = document.querySelector('.App_RE_main_scrollbar_H').scrollLeft
	if(scrollbarH_left+container_width<input_position){
		//current_position_PMCRE.RE.scrollbar=true
		setTimeout(() => {RE_checkScroll_H(input_position+nuzic_block*3-container_width) },1)
	}
	if(scrollbarH_left>input_position){
		//current_position_PMCRE.RE.scrollbar=true
		setTimeout(() => {RE_checkScroll_H(input_position-nuzic_block*3) },1)
	}
}

function RE_move_focus_iA(evt,element){
	switch(evt.keyCode){
	case 37:
		// Key left.
		//focus previous not disabled element
		if(element.value!=previous_value)element.setAttribute("focus","left")
		RE_focus_next_index_iA(element,-1)
		evt.preventDefault()
		break
	case 38:
		// Key up.//DISABLED
		//focus prev voice
		//RE_focus_voice_element_from_iA_element(element,true)
		evt.preventDefault()
		break
	case 39:
		// Key right.
		//focus next not disabled element
		if(element.value!=previous_value)element.setAttribute("focus","right")
		RE_focus_next_index_iA(element,1)
		evt.preventDefault()
		break
	case 40:
		// Key down.//DISABLED
		//focus next voice
		//RE_focus_voice_element_from_iA_element(element,false)
		evt.preventDefault()
		break
	case 9:
		//tab
		//focus next not disabled element
		evt.preventDefault()
		if(evt.shiftKey) {
			//shift was down when tab was pressed
			if(element.value!=previous_value)element.setAttribute("focus","left")
			RE_focus_next_index_iA(element,-1)
		}else{
			//tab
			//find next filled element
			if(element.value!=previous_value)element.setAttribute("focus","right")
			RE_focus_next_index_iA(element,1)
		}
		break
	case 13:
		//intro
		evt.preventDefault()
		//focus next not disabled element
		if(element.value!=previous_value)element.setAttribute("focus","right")
		RE_focus_next_index_iA(element,1)
		break
	}
}

function RE_focus_next_index_iA(element,direction){
	let input_list=[...element.parentNode.querySelectorAll("input")]
	let element_index=input_list.indexOf(element)
	if(direction==1){
		//right
		let next_element = input_list[element_index+1]
		if(next_element!=null){
			next_element.focus()
			next_element.select()
			return
		}else{
			//very last element
			let info_list=RE_find_voice_elements_info_from_iA_element(element)
			if(element.value!=previous_value)element.dispatchEvent(new Event("focusout"))
			RE_focus_on_object_index_iA(info_list.down.voice_id,element_index)
		}
	}
	if(direction==-1){
		//left
		var previous_element =  input_list[element_index-1]
		if(previous_element!=null){
			previous_element.focus()
			previous_element.select()
			return
		}else{
			//very first element
			let info_list=RE_find_voice_elements_info_from_iA_element(element)
			if(element.value!=previous_value)element.dispatchEvent(new Event("focusout"))
			RE_focus_on_object_index_iA(info_list.down.voice_id,0)
		}
	}
	//stay
	// element.focus()
	// element.select()
}

//ATT!!! PORISTION IS NOT ABSOLUTE
function RE_focus_on_object_index_iA(voice_id_down,position,next=0){
	let voice_obj_down = RE_find_voice_obj_by_id(voice_id_down)
	let iA_container_obj = voice_obj_down.previousElementSibling.querySelector(".App_RE_iA")
	if(iA_container_obj!=null){
		let iA_element_list = [...iA_container_obj.querySelectorAll("input")]
		let iA_element = iA_element_list[position+next]
		if(iA_element!=null){
			iA_element.focus()
			iA_element.select()
			return true
		}else{
			return false
		}
	}
	//calculate sound_index
	//7 is voice header
	let input_position= (index_focus-7)*nuzic_block+segment_obj.offsetLeft
	let scrollbarH_left = current_position_PMCRE.RE.X_start
	let container_width=document.querySelector(".App_Working_Space").clientWidth-(nuzic_block*7)
	// let scrollbarH_left = document.querySelector('.App_RE_main_scrollbar_H').scrollLeft
	if(scrollbarH_left+container_width<input_position){
		//current_position_PMCRE.RE.scrollbar=true
		setTimeout(() => {RE_checkScroll_H(input_position+nuzic_block*3-container_width) },1)
	}
	if(scrollbarH_left>input_position){
		//current_position_PMCRE.RE.scrollbar=true
		setTimeout(() => {RE_checkScroll_H(input_position-nuzic_block*3) },1)
	}
}



/*
function RE_focus_iA_element_from_voice_element(element,up){ //XXX XXX XXX
	//return true if no more action is necessary
	//return false if necessary to jump to a voice

	//used by move focus
	//finding voices
	var voice_matrix = element.closest(".App_RE_voice_matrix")
	var current_voice = element.closest(".App_RE_voice")
	var voice_list = [...document.querySelectorAll(".App_RE_voice")]
	var visible_voice_list = voice_list.filter((item)=>{return RE_voice_is_visible(item)})
	var voice_number = visible_voice_list.indexOf(current_voice)

	//find index
	var visible_iA_list = [...voice_matrix.querySelectorAll(".App_RE_iA:not(.hidden)")]
	//var iA_index = visible_iA_list.indexOf(iA_list)

	var iA_index = null

	//find voice index and voice index+1
	//var voice_up = visible_voice_list[iA_index]
	//var voice_down = visible_voice_list[iA_index+1]
	if(up){
		iA_index=voice_number-1
	}else{
		iA_index=voice_number
	}

	//no more voices
	if(iA_index<0 || iA_index>=visible_iA_list.length)return true
	//need to jump directly to a voice
	if(visible_iA_list[iA_index].style.display=="none")return false
	//need to jump to a iA (visible)
	var iA_element_list = [...visible_iA_list[iA_index].querySelectorAll(".App_RE_inp_box")]

	//determining index iA
	var current_line = element.closest(".App_RE_segment_line")
	var iA_index = Math.round(element.offsetLeft/nuzic_block)

	var segment_list= [...current_voice.querySelectorAll(".App_RE_segment")]
	var current_segment = element.closest(".App_RE_segment")
	var segment_index = segment_list.indexOf(current_segment)

	for (var i =0 ; i<segment_index; i++){
		//iA_index+=[...segment_list[i].querySelector(".App_RE_segment_line").querySelectorAll(".App_RE_inp_box")].length+1
		iA_index+=Math.round(segment_list[i].clientWidth/nuzic_block)+1
		//offsetWidth clientWidth
	}
	var iA_element = iA_element_list[iA_index]
	if(iA_element.disabled == true){
		for (var i = iA_index; i>=0; i--){
			if(iA_element_list[i].disabled != true){
				iA_element=iA_element_list[i]
				i=-1
			}
		}
	}

	if(iA_element!=null){
		iA_element.focus()
		iA_element.select()
		return true
	}else{
		return false
	}
}

function RE_focus_voice_element_from_iA_element(element,up){ //XXX XXX XXX
	//used by move focus
	var iA_elementIndex = Math.round(element.offsetLeft/nuzic_block) //ATT! start at position 8 for some reason !!!
	var iA_line = element.closest(".App_RE_iA")
	var voice_obj=RE_find_voice_obj_from_iA_line(iA_line,up)
	var segment_list= [...voice_obj.querySelectorAll(".App_RE_segment")]

	var segment_index = 0
	var current_segment=segment_list.find(segment=>{
		var seg_end_position=+Math.round((segment.clientWidth+segment.offsetLeft)/nuzic_block)
		return seg_end_position>iA_elementIndex
	})

	var seg_start_position = Math.round(current_segment.offsetLeft/nuzic_block)
	var target_elementIndex = iA_elementIndex-seg_start_position
	var line_list = [...current_segment.querySelectorAll(".App_RE_segment_line")]

	if(up){
		//find the lower line element ( fr excluded )
		var bottom_visible_line = line_list.pop()
		if(bottom_visible_line.classList.contains("App_RE_Tf")){
			bottom_visible_line = line_list.pop()
		}
		var next_list = [...bottom_visible_line.querySelectorAll(".App_RE_inp_box")]
		var new_element = next_list[target_elementIndex]
		new_element.focus()
		new_element.select()
	}else{
		//find the higher line element
		var upper_visible_line = line_list[0]
		var next_list = [...upper_visible_line.querySelectorAll(".App_RE_inp_box")]
		var new_element = next_list[target_elementIndex]
		new_element.focus()
		new_element.select()
	}
}
*/
