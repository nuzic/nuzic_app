function PMC_reset_selection(){
	//clrear time line green selection
	let time_line = document.querySelector(".App_PMC_time_line")
	let pulse_container = time_line.querySelector(".App_PMC_time_line_pulse_container")
	let segment_header_list = [...pulse_container.querySelectorAll(".App_PMC_segment_header")]
	let segment_name_list = [...pulse_container.querySelectorAll(".App_PMC_segment_name")]
	segment_header_list.forEach(header=>{header.classList.remove("selected")})
	segment_name_list.forEach(name=>{name.classList.remove("selected")})
	PMC_selection_time_svg.innerHTML=""
	PMC_svg_selection_background.innerHTML=""
	PMC_svg_selection_elements.innerHTML=""
	//clear pmc selection (elements)
	let object_list = [...document.getElementById("App_PMC_selected_voice").querySelectorAll(".selected")]
	object_list.forEach(object=>{object.classList.remove("selected")})
	//clear voice list green selection
	let voice_object_list_green = document.querySelectorAll(".App_PMC_voice_selector_voice_item.selected_green")
	let voice_object_list_green_light = document.querySelectorAll(".App_PMC_voice_selector_voice_item.selected_green_light")
	voice_object_list_green.forEach(voice_object=>{voice_object.classList.remove("selected_green")})
	voice_object_list_green_light.forEach(voice_object=>{voice_object.classList.remove("selected_green_light")})
	APP_reset_selection_global_variable()
}

function PMC_select_segment(segment_number){
	PMC_reset_selection()
	//time line
	//segment name
	let time_line = document.querySelector(".App_PMC_time_line")
	let pulse_container = time_line.querySelector(".App_PMC_time_line_pulse_container")
	let segment_header_list = [...pulse_container.querySelectorAll(".App_PMC_segment_header")]
	let segment_name_list = [...pulse_container.querySelectorAll(".App_PMC_segment_name")]
	segment_header_list[segment_number].classList.add("selected")
	segment_name_list[segment_number].classList.add("selected")
	//light green overlay
	let width = segment_header_list[segment_number+1].parentNode.clientWidth
	let from_px = segment_header_list[segment_number].parentNode.offsetLeft+nuzic_block_half//+half bc pulselist is half right respect svg
	let to_px = segment_header_list[segment_number+1].parentNode.offsetLeft+width+nuzic_block_half
	if(width!=28){
		//adjust
		from_px+=(width-nuzic_block)/2
		to_px-=(width-nuzic_block)/2
	}
	PMC_selection_time_svg.innerHTML=""
	//background
	let svg_back_string=`<line x1="${from_px}" y1="50%" x2="${to_px}" y2="50%" stroke="var(--Nuzic_green_light)" stroke-width="${(nuzic_block *1.5)}"></line>`
	PMC_selection_time_svg.innerHTML=svg_back_string
	//select voice elements in PMC
	let object_list = [...document.getElementById("App_PMC_selected_voice").querySelectorAll(".App_PMC_selected_voice_object")]
	let from_px_PMC = from_px+nuzic_block_half
	let to_px_PMC = to_px-nuzic_block_half
	object_list.forEach(object=>{
		if(object.offsetLeft>=from_px_PMC && object.offsetLeft+object.clientWidth<=to_px_PMC)object.classList.add("selected")
	})
	//Selected voice
	let data = DATA_get_current_state_point(true)
	//Selected voice
	let voice_number=0
	let voice_id = data.voice_data[data.voice_data.length-1].voice_id
	data.voice_data.find((voice,index_array)=>{
		if(voice.selected){
			voice_number=data.voice_data.length-(1+index_array)
			voice_id=voice.voice_id
			return true
		}
	})
	//find the segment index
	let segment_index= segment_number
	//change global variable
	APP_selection_options={"target": 'segment',"working_space": 'PMC',"voice_number": voice_number,"voice_id": voice_id,"segment_index": segment_index}
}

function PMC_select_segment_column(column_number){
	PMC_reset_selection()
	//time line
	//segment name
	let time_line = document.querySelector(".App_PMC_time_line")
	let pulse_container = time_line.querySelector(".App_PMC_time_line_pulse_container")
	let segment_header_list = [...pulse_container.querySelectorAll(".App_PMC_segment_header")]
	let segment_name_list = [...pulse_container.querySelectorAll(".App_PMC_segment_name")]
	segment_header_list[column_number].classList.add("selected")
	segment_name_list[column_number].classList.add("selected")
	let width = segment_header_list[column_number+1].parentNode.clientWidth
	let from_px = segment_header_list[column_number].parentNode.offsetLeft+nuzic_block_half//+half bc pulselist is half right respect svg
	let to_px = segment_header_list[column_number+1].parentNode.offsetLeft+width+nuzic_block_half
	if(width!=28){
		//adjust
		from_px+=(width-nuzic_block)/2
		to_px-=(width-nuzic_block)/2
	}
	let from_px_PMC = from_px+nuzic_block_half
	let to_px_PMC = to_px-nuzic_block_half
	//highlight area
	let x_L=from_px_PMC//-nuzic_block_half/2
	let x_C=(from_px_PMC+to_px_PMC) *0.5
	let x_R=to_px_PMC//+nuzic_block_half/2
	var svg_back_string=`
			<line x1="${x_C}" y1="0" x2="${x_C}" y2="100%" stroke="var(--Nuzic_green_light)" stroke-width="${(to_px_PMC-from_px_PMC)}"></line>
			<line x1="${x_L}" y1="0" x2="${x_L}" y2="100%" stroke="var(--Nuzic_green)" stroke-width="${nuzic_block_half}"></line>
			<line x1="${x_R}" y1="0" x2="${x_R}" y2="100%" stroke="var(--Nuzic_green)" stroke-width="${nuzic_block_half}"></line>
		`
	PMC_svg_selection_background.innerHTML=svg_back_string
	//select voice elements in PMC
	let object_list = [...document.getElementById("App_PMC_selected_voice").querySelectorAll(".App_PMC_selected_voice_object")]
	object_list.forEach(object=>{
		if(object.offsetLeft>=from_px_PMC && object.offsetLeft+object.clientWidth<=to_px_PMC)object.classList.add("selected")
	})
	//PMC hightlight elements unselected voice (only green borders)
	let voice_data = DATA_get_PMC_visible_voice_data() //every voice minus selected one
	let PMC_global_variables = DATA_read_PMC_global_variables()
	let note_number = TET*N_reg
	//generic width of IDEA
	// var height_string = 'calc('+TET+' * N_reg * var(--PMC_y_block) )'
	// var width_string = "calc(var(--PMC_x_block) * "+Li+" + var(--RE_block))"
	let offset = nuzic_block
	let c_height = nuzic_block * PMC_zoom_y * TET * N_reg + nuzic_block/2
	let delta_y = nuzic_block * PMC_zoom_y
	// using SVG
	// PMC_svg_selection_elements.style.height=height_string
	// PMC_svg_selection_elements.style.width=width_string
	// PMC_svg_selection_elements.innerHTML= ""
	let svg_string=""
	let PMC_grades_data=DATA_calculate_grade_data(Li)
	voice_data.forEach(voice=>{
		//voice widths
		if(voice.blocked){
			let D_NP = voice.neopulse.D
			let N_NP = voice.neopulse.N
			let neopulse_moltiplicator =  N_NP/D_NP
			let delta = nuzic_block * PMC_zoom_x * neopulse_moltiplicator
			let voice_color_light = eval(voice.color+"_light")
			let array_voice_positions = PMC_segment_data_to_obj_data(voice.data.segment_data,voice.neopulse)
			//translations
			let string_N_list = PMC_calculate_object_N_text(array_voice_positions,PMC_global_variables,voice.neopulse,false,PMC_grades_data)[0]
			//positioning and resizing main block
			let first_note = array_voice_positions.find(object=>{
					return object.nzc_note>-1
			})
			//draw objects
			let starting_pos = voice.e_sound
			if(first_note!=null){
				starting_pos = first_note.nzc_note
			}
			let last_nzc_note=starting_pos
			let last_pos=starting_pos
			array_voice_positions.forEach((object,index)=>{
				let nzc_note = object.nzc_note
				if (nzc_note ==-1){
					//silence == nothing
					last_nzc_note=nzc_note
				}else if(nzc_note ==-2){
					//element box positioned in line last note
					let note = last_pos
					let x1 = offset+object.pulse_float*delta
					let x2 = x1 + object.duration *delta
					let y = delta_y*(note_number-note)-delta_y
					if(x2>2)x2-=1//little space
					if(x1>from_px_PMC && x2<to_px_PMC){
						//no border left
						//position text
						let t_x = x1+(x2-x1)/2-(4)/2
						let t_y = y+4+delta_y/2
						svg_string+=`
							<rect x="${x1}" y="${y}" width="${x2-x1}" height="${delta_y}" fill="var(--Nuzic_green_light)" stroke="var(--Nuzic_green)" stroke-width="1"></rect>
							<text x="${t_x}" y="${t_y}" class="App_PMC_selected_element_svg_text">${string_N_list[index]}</text>
						`
					}
					last_nzc_note=nzc_note
				} else if(nzc_note ==-3 ){
					//ligadura, need to print something
					if(last_nzc_note!=-1 && last_nzc_note!=-4){
						let note = last_pos
						let x1 = offset+object.pulse_float*delta
						let x2 = x1 + object.duration *delta
						let y = delta_y*(note_number-note)-delta_y/2
						if(x2>2)x2-=1//little space
						x1-=1 //connect with previous
						if(x1>from_px_PMC && x2<to_px_PMC){
							//no border left
							//position text
							let t_x = x1+(x2-x1)/2-(4)/2
							let t_y = y+4
							let fill_color=voice_color_light
							if(last_nzc_note==-2){
								//modify last
								fill_color="var(--Nuzic_green_light)"
							}
							svg_string+=`
								<rect x="${x1}" y="${y-(delta_y/2)}" width="${x2-x1}" height="${delta_y}" fill="${fill_color}" stroke="var(--Nuzic_green)" stroke-width="1"></rect>
								<line x1="${x1}" y1="${y-(delta_y/2)+1}" x2="${x1}" y2="${y+(delta_y/2)-1}" stroke="${fill_color}" stroke-width="2"></line>
								<text x="${t_x}" y="${t_y}" class="App_PMC_selected_element_svg_text">${string_N_list[index]}</text>
							`
						}
					}
				} else {
					let note = nzc_note
					let x1 = offset+object.pulse_float*delta
					let x2 = x1 + object.duration *delta
					let y = delta_y*(note_number-note)-delta_y/2
					if(x2>2)x2-=1//little space
					if(x1>=from_px_PMC && x2<=to_px_PMC){
						//search text
						let text = string_N_list[index]+""
						let car_x=text.length*5
						let t_x = x1+(x2-x1)/2-(car_x)/2
						let t_y = y+4
						svg_string+=`
							<rect x="${x1}" y="${y-(delta_y/2)}" width="${x2-x1}" height="${delta_y}" fill="transparent" stroke="var(--Nuzic_green)" stroke-width="1"></rect>
							<text x="${t_x}" y="${t_y}" class="App_PMC_selected_element_svg_text">${text}</text>
						`
					}
					last_nzc_note=nzc_note
					last_pos=nzc_note
				}
			})
		}
	})
	PMC_svg_selection_elements.innerHTML=svg_string
	//selected voice list green and light green
	let selector_voice_list_item = document.querySelectorAll(".App_PMC_voice_selector_voice_item")
	let all_voice_data = DATA_get_current_state_point(false).voice_data
	selector_voice_list_item.forEach(list_item=>{
		if(list_item.classList.contains("checked")){
			list_item.classList.add("selected_green")
		}else{
			//verify if voice blocked
			let voice_id= parseInt(list_item.getAttribute("value"))
			let find=all_voice_data.find(voice=>{
				if(voice.voice_id==voice_id && voice.blocked)return true
			})
			if (find)list_item.classList.add("selected_green_light")
		}
	})
	//find the segment index
	let segment_index= column_number
	//change global variable
	APP_selection_options={"target": 'column',"working_space": 'PMC',"voice_number": null,"voice_id": null,"segment_index": segment_index}
}

function PMC_select_voice(){
	PMC_reset_selection()
	//selected voice

	//time line
	//segment names
	let time_line = document.querySelector(".App_PMC_time_line")
	let pulse_container = time_line.querySelector(".App_PMC_time_line_pulse_container")
	let segment_header_list = [...pulse_container.querySelectorAll(".App_PMC_segment_header")]
	let segment_name_list = [...pulse_container.querySelectorAll(".App_PMC_segment_name")]
	segment_header_list.forEach(segment_header=>{segment_header.classList.add("selected")})
	segment_name_list.forEach(segment_name=>{segment_name.classList.add("selected")})
	//light green overlay
	let width = segment_header_list[segment_header_list.length-1].parentNode.clientWidth
	let from_px = segment_header_list[0].parentNode.offsetLeft+nuzic_block_half
	let to_px = segment_header_list[segment_header_list.length-1].parentNode.offsetLeft+width+nuzic_block_half
	if(width!=28){
		//adjust
		from_px+=(width-nuzic_block)/2
		to_px-=(width-nuzic_block)/2
	}
	PMC_selection_time_svg.innerHTML=""
	//background
	let svg_back_string=`<line x1="${from_px}" y1="50%" x2="${to_px}" y2="50%" stroke="var(--Nuzic_green_light)" stroke-width="${(nuzic_block *1.5)}"></line>`
	PMC_selection_time_svg.innerHTML=svg_back_string
	//PMC hightlight all elements voice (elements light green and dark green border)
	let object_list = [...document.getElementById("App_PMC_selected_voice").querySelectorAll(".App_PMC_selected_voice_object")]
	object_list.forEach(object=>{object.classList.add("selected")})
	//voice list
	let voice_object = document.querySelector(".App_PMC_voice_selector_voice_item.checked")
	voice_object.classList.add("selected_green")
	let voice_id = parseInt(voice_object.getAttribute("value"))
	let voice_obj_list = [...voice_object.parentNode.querySelectorAll(".App_PMC_voice_selector_voice_item")]
	let voice_number= voice_obj_list.length - 1 -voice_obj_list.indexOf(voice_object)
	//change global variable
	APP_selection_options={"target": 'voice',"working_space": 'PMC',"voice_number": voice_number,"voice_id": voice_id,"segment_index": null}
}

/////*SEGMENT COLUMN*////////

function PMC_toggle_selection_segment_column_button(button,event){
	let menu_div = document.querySelector("#App_PMC_dropdown_segment_content")
	let toggle_button = menu_div.querySelector(".App_PMC_dropdown_segment_toggle_selection")
	let segment_index= parseInt(menu_div.getAttribute('value'))
	if(!toggle_button.classList.contains("disabled")){
		let toggle_img = toggle_button.querySelector("img")
		if(toggle_button.classList.contains("segment")){
			toggle_button.classList.remove("segment")
			toggle_button.classList.add("column")
			toggle_img.src="./Icons/op_menu_selector_col.svg"
			PMC_select_segment_column(segment_index)
		}else{
			toggle_button.classList.remove("column")
			toggle_button.classList.add("segment")
			toggle_img.src="./Icons/op_menu_selector_seg.svg"
			PMC_select_segment(segment_index)
		}
	}
}

function PMC_toggle_dropdown_menu_segment(button,segment_index){
	let position = button.getBoundingClientRect()
	let menu_div = document.querySelector("#App_PMC_dropdown_segment_content")
	let toggle_button = menu_div.querySelector(".App_PMC_dropdown_segment_toggle_selection")
	let toggle_img = toggle_button.querySelector("img")
	toggle_button.classList.remove("segment","column","disabled")
	toggle_img.src="./Icons/op_menu_selector_seg.svg"
	if(menu_div.style.display == "flex"){
		PMC_close_dropdown_menu_segment()
	}else{
		menu_div.style.display = "flex"
		menu_div.setAttribute('value',segment_index)
		let current_voice_data=DATA_get_selected_voice_data()
		if(current_voice_data.blocked){
			toggle_button.classList.add("segment")
		}else{
			toggle_button.classList.add("disabled")
		}
		PMC_select_segment(segment_index)
		if(segment_index==0){
			menu_div.querySelector(".App_PMC_dropdown_segment_merge").classList.add("disabled")
		}else{
			menu_div.querySelector(".App_PMC_dropdown_segment_merge").classList.remove("disabled")
		}
	}
	//position label hover
	let posx = position.x-23//+position.width
	let posy = position.y-menu_div.clientHeight//-3
	let navTNAV_maximizeBttn = document.getElementById('App_Nav_maximize')
	let header = document.getElementById("mainHeaderNav")
	if(header!=null && navTNAV_maximizeBttn.value == "0"){
		posy-=56
	}
	menu_div.style.left = `${posx}px`
	menu_div.style.top = `${posy}px`
}

function PMC_close_dropdown_menu_segment(){
	let menu_div = document.querySelector("#App_PMC_dropdown_segment_content")
	menu_div.style.display = "none"
	PMC_reset_selection()
}

function PMC_change_segment_name_button(element){
	//focus on segment name
	let menu_div = element.closest("#App_PMC_dropdown_segment_content")
	menu_div.style.display = "none"
	let segment_index= parseInt(menu_div.getAttribute('value'))
	let container = document.querySelector(".App_PMC_time_line")
	let segment_div_list = [...container.querySelectorAll(".App_PMC_segment_header")]
	let current_header = segment_div_list[segment_index]
	var segment_name = current_header.parentNode.querySelector(".App_PMC_segment_name")
	if(segment_name!=null){
		//if not too short
		if(!segment_name.classList.contains("short")){
			segment_name.classList.remove("hidden")//if hidden
			segment_name.disabled=false
			segment_name.focus()
			segment_name.select()
		}
	}
}

function PMC_add_segment_button(element){
	APP_stop()
	let menu_div = document.querySelector("#App_PMC_dropdown_segment_content")
	menu_div.style.display = "none"
	let voice_data = DATA_get_selected_voice_data()
	let voice_id=voice_data.voice_id
	let segment_index=0
	let segment_div = element.closest(".App_PMC_segment_header")
	if(segment_div!=null){
		let container = segment_div.closest(".App_PMC_time_line")
		let segment_div_list = [...container.querySelectorAll(".App_PMC_segment_header")]
		segment_index = segment_div_list.indexOf(segment_div)
	}else{
		//segment index from menu value
		segment_index= parseInt(menu_div.getAttribute('value'))
	}
	DATA_add_segment(voice_id,segment_index)
}

function PMC_merge_segment_button(element){
	APP_stop()
	let menu_div = element.closest("#App_PMC_dropdown_segment_content")
	menu_div.style.display = "none"
	let voice_data = DATA_get_selected_voice_data()
	let voice_id=voice_data.voice_id
	let segment_index = parseInt(menu_div.getAttribute('value'))
	if(segment_index==0)return
	DATA_merge_segment(voice_id,segment_index-1)
}

function PMC_clear_segment_button(element){
	APP_stop()
	let menu_div = element.closest("#App_PMC_dropdown_segment_content")
	menu_div.style.display = "none"
	let voice_data = DATA_get_selected_voice_data()
	let voice_id=voice_data.voice_id
	let segment_index = parseInt(menu_div.getAttribute('value'))
	//determine if clear column or segment
	let toggle_button = menu_div.querySelector(".App_PMC_dropdown_segment_toggle_selection")
	if(toggle_button.classList.contains("disabled") || toggle_button.classList.contains("segment")){
		DATA_clear_segment(voice_id,segment_index)
	}else{
		DATA_clear_column_segment(segment_index)
	}
}

function PMC_copy_segment_button(element){
	APP_stop()
	let menu_div = element.closest("#App_PMC_dropdown_segment_content")
	menu_div.style.display = "none"
	let voice_data = DATA_get_selected_voice_data()
	let voice_id=voice_data.voice_id
	let segment_index = parseInt(menu_div.getAttribute('value'))
	//determine if copy column or segment
	let toggle_button = menu_div.querySelector(".App_PMC_dropdown_segment_toggle_selection")
	if(toggle_button.classList.contains("disabled") || toggle_button.classList.contains("segment")){
		DATA_copy_segment(voice_id,segment_index)
	}else{
		DATA_copy_column_segment(segment_index)
	}
}

function PMC_paste_segment_button(element){
	APP_stop()
	//determine if paste column or segment
	let menu_div = element.closest("#App_PMC_dropdown_segment_content")
	let toggle_button = menu_div.querySelector(".App_PMC_dropdown_segment_toggle_selection")
	if(toggle_button.classList.contains("disabled") || toggle_button.classList.contains("segment")){
		if(copied_data.id!="segment")return
		APP_show_operation_dialog_box('paste')
	}else{
		//column
		if(copied_data.id!="column")return
		APP_show_operation_dialog_box('paste')
	}
}

function PMC_delete_segment_button(element){
	APP_stop()
	let menu_div = element.closest("#App_PMC_dropdown_segment_content")
	menu_div.style.display = "none"
	let voice_data = DATA_get_selected_voice_data()
	let voice_id=voice_data.voice_id
	let segment_index = parseInt(menu_div.getAttribute('value'))
	//determine if paste column or segment
	let toggle_button = menu_div.querySelector(".App_PMC_dropdown_segment_toggle_selection")
	if(toggle_button.classList.contains("disabled") || toggle_button.classList.contains("segment")){
		DATA_delete_segment(voice_id,segment_index)
	}else{
		//column
		DATA_delete_column_segment(segment_index)
	}
}

function PMC_repeat_segment_button(button,e){
	e.stopPropagation()
	APP_close_all_dropdown_segment_contents()
	//with global variable
	APP_show_operation_dialog_box('repetitions')
}

function PMC_operations_segment_button(button,e){
	e.stopPropagation()
	APP_close_all_dropdown_segment_contents()
	APP_show_operation_dialog_box('operations')
}

/////*VOICE*////////

//not a toggle in this case, always one is selected
function PMC_select_voice_button(button){
	//if already selected.... pass
	if(button.classList.contains("checked")){
		//console.log("nothing to do ")
		return
	}
	let container = button.closest(".App_PMC_voice_selector_voice_list")
	if(container==null)return
	let checked_button_list = [...container.querySelectorAll(".App_PMC_voice_selector_voice_item.checked")]
	let data = DATA_get_current_state_point(true)
	let checked = checked_button_list.some(item=>{return item==button})
	checked_button_list.forEach(item=>{
		item.classList.remove("checked")
	})
	button.classList.add("checked")
	data.voice_data.forEach(voice=>{
		voice.selected=PMC_voice_is_selected(voice.voice_id)
	})
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function PMC_toggle_dropdown_voice(button){
	//need to verify selection (if not wait for show)
	let parent = button.closest(".App_PMC_voice_selector_voice_item")
	if(parent==null){
		//second menu on App_PMC_selected_voice_note_header
		parent=document.getElementById("App_PMC_selected_voice_note_header")
	}
	let menu_obj= parent.querySelector(".App_PMC_dropdown_voiceMenu")
	menu_obj.classList.toggle("show")
	if(menu_obj.classList.contains("show")){
		//verify it is NOT a change of selection voice
		//verify it contain show
		let menu_selected=[...document.querySelectorAll(".App_PMC_dropdown_voiceMenu")].some(item=>{
			return item.classList.contains("show")
		})
		if(!menu_selected){
			// var value = parent.getAttribute("value")
			// var parent_list = [...document.querySelectorAll(".App_PMC_voice_selector_voice_item")]
			//find correct parent
			// var new_parent= parent_list.find(papino=>{
			// 	return papino.getAttribute("value")==value
			// })
			let new_parent = document.querySelector(".App_PMC_voice_selector_voice_item.checked")
			//force opening menu??
			new_parent.querySelector(".App_PMC_dropdown_voiceMenu").classList.add("show")
			// scroll_top=new_parent.closest(".App_PMC_voice_selector_voice_list").scrollTop
			// console.log(new_parent)
			// console.log(new_parent.getBoundingClientRect())
			// console.log(scroll_top)
			// top_position=new_parent.getBoundingClientRect().top-scroll_top
			if(new_parent.classList.contains("App_PMC_voice_selector_voice_item")){
				//position the menu vertically
				let list_container=new_parent.closest(".App_PMC_voice_selector_voice_list")
				let [index,]=RE_element_index(new_parent,".App_PMC_voice_selector_voice_item")
				let top_position=100+nuzic_block * index - list_container.scrollTop
				new_parent.querySelector(".App_PMC_dropdown_voiceMenu").style="top:"+top_position+"px;"
			}
		}else{
			if(parent.classList.contains("App_PMC_voice_selector_voice_item")){
				//position the menu vertically
				//let top_position=parent.getBoundingClientRect().top-App_RE.getBoundingClientRect().top
				//let top_position=parent.getBoundingClientRect().top-scroll_top
				let list_container=parent.closest(".App_PMC_voice_selector_voice_list")
				let [index,]=RE_element_index(parent,".App_PMC_voice_selector_voice_item")
				let top_position=100+nuzic_block * index - list_container.scrollTop
				menu_obj.style="top:"+top_position+"px;"
			}
		}
		PMC_select_voice()
	}else{
		PMC_reset_selection()
	}
}

function PMC_repeat_selected_voice_button(){
	//e.stopPropagation()
	APP_close_all_dropdown_segment_contents()
	APP_show_operation_dialog_box('repetitions')
}

function PMC_operations_selected_voice_button(){
	//e.stopPropagation()
	APP_close_all_dropdown_segment_contents()
	APP_show_operation_dialog_box('operations')
}






