function SNAV_enter_on_segment_parameters(element=null,voice_name=null,voice_id=null,segment_index=null){
	let parameters = document.querySelector('.App_SNav_parameters')
	if(parameters.style.display == 'flex'){
		if(element!=null){
			let voice_obj = element.closest('.App_RE_voice');
			[voice_name,voice_id] = RE_read_voice_id(voice_obj)
			//find segment
			let segment_obj = element.closest(".App_RE_segment")
			let voice_data_obj = segment_obj.closest(".App_RE_voice_data")
			//find segment index
			let segment_obj_list = [...voice_data_obj.querySelectorAll(".App_RE_segment")]
			segment_index= segment_obj_list.indexOf(segment_obj)
		}
		//copy of database voice
		let current_voice_data_selected=DATA_get_current_state_point(false).voice_data.find(voice=>{return voice.voice_id==voice_id})
		if(current_voice_data_selected==null)return
		let time_array=current_voice_data_selected.data.midi_data.time
		//add end idea
		time_array.push(Li*60/PPM)
		let time_segment_change = DATA_list_voice_data_segment_change(current_voice_data_selected)
		let voiceInp = document.getElementById('parVoice')
		let segmentInp = document.getElementById('parSegment')
		let data = current_voice_data_selected.data.segment_data[segment_index]
		voiceInp.value= voice_name
		segmentInp.value = `s.${segment_index}`
		let segmentTimeArr
		if(segment_index == time_segment_change.length-1){
			//last segment
			segmentTimeArr = time_array.slice(time_array.indexOf(time_segment_change[segment_index]),time_array.length)
		} else {
			segmentTimeArr = time_array.slice(time_array.indexOf(time_segment_change[segment_index]),time_array.indexOf(time_segment_change[segment_index+1])+1)
		}
		SNAV_parameters_length(data)
		SNAV_parameters_time_quantity(data)
		SNAV_parameters_diferentiated_quantity(data, segmentTimeArr)
		SNAV_parameters_diferentiated_quantity_fr(data)
		SNAV_parameters_temporal_variety(data,segmentTimeArr, segmentTimeArr)
		SNAV_parameters_temporal_density(data)
		SNAV_parameters_temporal_stdDeviation(data, segmentTimeArr)
		SNAV_parameters_sound_range(data)
		SNAV_parameters_sound_quantity(data)
		SNAV_parameters_sound_diferentiated_quantity(data)
		SNAV_parameters_sound_diferentiated_quantity_iS(data)
		SNAV_parameters_sound_resulting_interval(data)
		SNAV_parameters_sound_variety(data)
		SNAV_parameters_sound_variety_iS(data)
		SNAV_parameters_sound_density(data)
	}
}

function SNAV_parameters_length(array){
	document.getElementById('parTLengthInput').value= array.time[array.time.length-1].P
	return array.time[array.time.length-1].P
}

function SNAV_parameters_time_quantity(array){
	document.getElementById('parTQuantityInput').value=array.time.length-1
	return array.time.length-1
}

function SNAV_parameters_diferentiated_quantity(array,timesArr){
	if(array.fraction.length == 1){
		let iTArray =[]
		for(let i=array.time.length-1;i>0;i--){
			iTArray.push(array.time[i].P-array.time[i-1].P)
		}
		iTArray.sort((a, b) => a - b);
		counter = 1;
		for (let i = 1; i < iTArray.length; i++) {
			if (iTArray[i] != iTArray[i - 1]) {
				counter++;
			}
		}
		document.getElementById('parTDiferentiatedQInput').value=counter
		return counter
	} else {
		//Read voice values y quieres el primer y quinto parametro
		let iTArray =[]
		for(let i=timesArr.length-1;i>0;i--){
			let num =timesArr[i]-timesArr[i-1]
			iTArray.push(num.toFixed(4))
		}
		iTArray.sort((a, b) => a - b);
		counter = 1;
	 	for (let i = 1; i < iTArray.length; i++) {
			if (iTArray[i] != iTArray[i - 1]) {
				counter++;
			}
		}
		document.getElementById('parTDiferentiatedQInput').value=counter
		return counter
	}
}

function SNAV_parameters_diferentiated_quantity_fr(array){
	let frArray=[]
	for(let i=array.fraction.length-1;i>=0;i--){
		frArray.push(eval(array.fraction[i][0]))
	}
	//Separar en diferentes arrays en funcion del fraccionamiento y hacer por esparado
	frArray.sort((a, b) => a - b);
	counter = 1;
	for (let i = 1; i < frArray.length; i++) {
		if (frArray[i] != frArray[i - 1]) {
			counter++;
		}
	}
	document.getElementById('parTFrQuantityInput').value=counter
}

function SNAV_parameters_temporal_variety(array, timeArr){
	let differentiatedQuantity = SNAV_parameters_diferentiated_quantity(array,timeArr)
	let timeQuantity = SNAV_parameters_time_quantity(array)
	document.getElementById('parTVarietyInput').value=(differentiatedQuantity/timeQuantity).toFixed(3)
}

function SNAV_parameters_temporal_density(array){
	let length = SNAV_parameters_length(array)
	let timeQuantity = SNAV_parameters_time_quantity(array)
	document.getElementById('parTDensityInput').value=(timeQuantity/length).toFixed(3)
}

function SNAV_parameters_temporal_stdDeviation(array, timesArr){
	let iTArray =[]
	let median = 0
	let stdDev = 0
	for(let i=timesArr.length-1;i>0;i--){
		let num =timesArr[i]-timesArr[i-1]
		iTArray.push(num.toFixed(4))
		median += parseFloat(num.toFixed(4))
	}
	median = median/iTArray.length
	for(let i=0;i<iTArray.length;i++){
		stdDev += ((parseFloat(iTArray[i])-median)**2)
	}
	stdDev = Math.sqrt(stdDev/median)
	document.getElementById('parTDeviationInput').value= stdDev.toFixed(3)
}

function SNAV_parameters_sound_range(array){
	let numberArr = array.sound.map(el => el.note)
	numberArr = numberArr.filter(note => note>0)
	numberArr.sort((a, b) => a - b);
	let input = document.getElementById('parSRangeInput')
	if(isNaN(numberArr[numberArr.length-1]-numberArr[0]) == false){
		input.value=numberArr[numberArr.length-1]-numberArr[0]
		return numberArr[numberArr.length-1]-numberArr[0]
	} else {
		input.value = 0
		return 0
	}
}

function SNAV_parameters_sound_quantity(array){
	let numberArr = array.sound.map(el => el.note)
	numberArr = numberArr.filter(note => note>0)
	document.getElementById('parSQuantityInput').value = numberArr.length
	return numberArr.length
}

function SNAV_parameters_sound_diferentiated_quantity(array){
	let numberArr = array.sound.map(el => el.note)
	numberArr = numberArr.filter(note => note>0)
	let input = document.getElementById('parSDiferentiatedQInput')
	let counter = 1;
	if(numberArr.length ==0){
		input.value = 0
		return 0
	}else{
		numberArr.sort((a, b) => a - b);
		for (let i = 1; i < numberArr.length; i++) {
			if (numberArr[i] != numberArr[i - 1]) {
				counter++;
			}
		}
		input.value=counter
		return counter
	}
}

function SNAV_parameters_sound_diferentiated_quantity_iS(array){
	let numberArr = array.sound.map(el => el.note)
	numberArr = numberArr.filter(note => note>0)
	let input = document.getElementById('parSDiferentiatedQISInput')
	if(numberArr.length ==0){
		input.value = 0
		return 0
	} else{
		let isArr = []
		for(let i=1; i<numberArr.length;i++){
			isArr.push(Math.abs(numberArr[i]-numberArr[i-1]))
		}
		isArr.sort((a, b) => a - b);
		counter = 1;
		for (let i = 1; i < isArr.length; i++) {
			if (isArr[i] != isArr[i - 1]) {
				counter++;
			}
		}
		input.value=counter
		return counter
	}
}

function SNAV_parameters_sound_resulting_interval(array){
	let numberArr = array.sound.map(el => el.note)
	numberArr = numberArr.filter(note => note>0)
	let input = document.getElementById('parSResultingIntervalInput')
	if(isNaN(numberArr[numberArr.length-1]-numberArr[0]) == false){
		input.value=numberArr[numberArr.length-1]-numberArr[0]
	} else {
		input.value = 0
		return
	}
}

function SNAV_parameters_sound_variety(array){
	let differentiatedQuantity = SNAV_parameters_sound_diferentiated_quantity(array)
	let soundQuantity = SNAV_parameters_sound_quantity(array)
	let input = document.getElementById('parSVarietyInput')
	if(soundQuantity != 0){
		input.value=(differentiatedQuantity/soundQuantity).toFixed(3)
	} else {
		input.value=0
		return
	}
}

function SNAV_parameters_sound_variety_iS(array){
	let differentiatedQuantity = SNAV_parameters_sound_diferentiated_quantity_iS(array)
	let soundQuantity = SNAV_parameters_sound_quantity(array)
	let input = document.getElementById('parSVarietyISInput')
	if(soundQuantity != 0){
		input.value=(differentiatedQuantity/soundQuantity).toFixed(3)
	} else {
		input.value = 0
		return
	}
}

function SNAV_parameters_sound_density(array){
	let range = SNAV_parameters_sound_range(array)
	let input = document.getElementById('parSDensityInput')
	if(range==0){
		input.value= 0
		return
	}else{
		let soundQuantity = SNAV_parameters_sound_quantity(array)
		let result=soundQuantity/range
		if(isNaN(result) == false){
			input.value= result.toFixed(3)
		} else {
			input.value= 0
			return
		}
	}
}
