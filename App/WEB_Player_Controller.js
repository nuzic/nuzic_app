//spessasynth
//is_App=false

function APP_prepare_SF_instruments_for_WEB(voice_list,reverb){
	if(SF_manager==null){
		return
	}
	let voices_ready=SF_manager.synth.channelsAmount
	voice_list.forEach((voice,index)=>{
		if(index>=voices_ready){
			//add a channel
			SF_manager.synth.addNewChannel()
			voices_ready++
		}
		//change channel instrument proprieties
		//let V_channel=V_global_variables.volumes[voice.voice_id]
		let V_channel={volume:voice.volume,pan:voice.pan,fx:voice.fx,lpf:voice.lpf,vel:voice.vel}
		App_set_instrument_for_web(index,voice.instrument,V_channel,reverb)
	 })
	//extra voice for "e"
	e_channel=voice_list.length
	if(e_channel>=voices_ready){
		//add a channel
		SF_manager.synth.addNewChannel()
		voices_ready++
	}
	App_set_instrument_for_web(e_channel,128,undefined,reverb)
	//extra voice for pulses and metronome
	p_channel=e_channel+1
	if(p_channel>=voices_ready){
		//add a channel
		SF_manager.synth.addNewChannel()
	}
	App_set_instrument_for_web(p_channel,130,undefined,reverb)
}

function App_set_instrument_for_web(channel,instrument=0,V_channel={volume:90,pan:0.5,fx:1,lpf:0.5,vel:V_velocity_default},reverb=0){
	if (instrument>=128){
		let i_number=instrument-128
		SF_manager.synth.setDrums(channel,true)
		SF_manager.synth.programChange(channel,i_number)
	}else{
		SF_manager.synth.setDrums(channel,false)
		SF_manager.synth.programChange(channel,instrument)
	}
	let reverb_total_value=Math.round((reverb*V_channel.fx)*128)-1 //(or 91??)
	SF_manager.synth.controllerReverb(channel,reverb_total_value)
	//pan (10)
	let pan=Math.round(V_channel.pan*127)//0,64,127
	SF_manager.synth.controllerChange(channel,10,pan)
	//channel volume (7) 0-127
	SF_manager.synth.controllerChange(channel,7,V_channel.volume)
	//lpf (74 brightness)
	let lpf=Math.round(V_channel.lpf*127)
	SF_manager.synth.controllerChange(channel,74,lpf)
}

function APP_sequencer_for_WEB(current_data){
	//in freq
	hz=current_data.global_variables.PPM/60
	pulsationFrequency=hz+"hz"
	//change to database
	//a copy of database
	let voice_solo = current_data.voice_data.filter(voice=>{
		return voice.solo==true
	})
	let voice_list_unmuted_time_data = []
	if(voice_solo!=null&&voice_solo.length!=0){
		//there is a solo
		voice_solo.forEach(element=>{
			if(element.volume!="0" && !element.mute){
				voice_list_unmuted_time_data.push(element)
			}
		})
	}else{
		//find other not muted voices
		voice_list_unmuted_time_data = current_data.voice_data.filter(voice=>{
			return voice.volume!="0" && !voice.mute
		})
	}
	//Soundfont set channels for every voice + one for "e"
	APP_prepare_SF_instruments_for_WEB(voice_list_unmuted_time_data,current_data.global_variables.effects.reverb)

	let compas_values_array = current_data.compas
	let Ti = 60/current_data.global_variables.PPM
	let compas_sequence = [[0,"Accent"]]
	if(compas_values_array.length==1 && compas_values_array[0].compas_values[1]==0){
		//no compases
		//play an accent at the start and pulsacion next
		for (let i=1; i<Li ; i++){
			compas_sequence[i]=[i*Ti,"Tic"]
		}
	}else{
		//compasses
		let index_P = 0
		compas_values_array.forEach(compas =>{
			let accent_sequence=compas.compas_values[3]
			if(compas.compas_values[4]!=0){
				//recalculate rotated sequence
				accent_sequence.push(compas.compas_values[1])
				let iT_sequence = []
				for (let i = 0; i < accent_sequence.length-1; i++) {
					iT_sequence.push(accent_sequence[i+1]-accent_sequence[i])
				}
				//rotate iT sequence
				let rotation_N = compas.compas_values[4]
				while (rotation_N!=0) {
					iT_sequence.unshift(iT_sequence.pop())
					rotation_N--
				}
				accent_sequence=[0]
				let i=0
				iT_sequence.forEach(iT=>{
					accent_sequence.push(accent_sequence[i]+iT)
					i++
				})
				accent_sequence.pop()
			}
			for (let rep = 0; rep<compas.compas_values[2]; rep++){
				for(let pulse =0;pulse<compas.compas_values[1];pulse++){
					if(pulse==0){
						//first accent
						compas_sequence[index_P]=[index_P*Ti,"Accent"]
					}else{
						//if(compas.compas_values[3].includes(pulse)){
						if(accent_sequence.includes(pulse)){
							compas_sequence[index_P]=[index_P*Ti,"Accent_Weak"]
						}else{
							compas_sequence[index_P]=[index_P*Ti,"Tic"]
						}
					}
					index_P++
				}
			}
		})
	}
	let now =0
	let start =0
	let end = current_data.global_variables.Li
	//ADDING SEQUENCES OF SOUNDS LOOP BETTER IN TRANSPORT
	let now_seconds = now*(60/current_data.global_variables.PPM)
	let start_seconds = start*(60/current_data.global_variables.PPM)
	let end_seconds = end*(60/current_data.global_variables.PPM)
	//let composition_end = 1 * end * (60/current_data.global_variables.PPM) -0.01
	let sequencer_instructions=[]
	//CLASSIC METRONOME NEED LOVE
	//dB
	let MainMetronome=false
	if(MainMetronome){
		//see if Li bigger than main metronome
		compas_sequence.forEach(item=>{
			let pulseN = Math.round(item[0]*(current_data.global_variables.PPM/60))
			if( item[1]=="Accent"){
				sequencer_instructions.push([item[0],{action: "SF_metroOn", channel:p_channel,note:{midiNote:p_channel_midiNote.Accent,cents:0}}])
				return
			}
			if( item[1]=="Accent_Weak"){
				//note 24 -> 65.405
				sequencer_instructions.push([item[0],{action: "SF_metroOn", channel:p_channel,note:{midiNote:p_channel_midiNote.Accent_weak,cents:0}}])
				return
			}
			if( item[1]=="Tic"){
				//note 25 -> 61.73409942094616
				sequencer_instructions.push([item[0],{action: "SF_metroOn", channel:p_channel,note:{midiNote:p_channel_midiNote.Tic,cents:0}}])
				return
			}
		})
	}

	//METRONOME VOICE (FRACTIONING)
	voice_list_unmuted_time_data.forEach(voice_data=>{
		//check of i want to hear a metronome for this voice
		//from database
		let metro_sound = voice_data.metro
		if(metro_sound!=0){
			let midiNote= metro_sound+25+12
			//Metro sounds
			let metro_fractioning= DATA_list_voice_data_metro_fractioning_for_web(voice_data)
			metro_fractioning.forEach(change_fraction_data=>{
				for (let i =0; i<change_fraction_data.rep; i++){
					let time = change_fraction_data.time + i/change_fraction_data.freq
					sequencer_instructions.push([time,{action: "SF_metroOn", channel:p_channel,note:{midiNote:midiNote,cents:0}}])
				}
			})
		}
	})
	//NOTES VOICE
	voice_list_unmuted_time_data.forEach((voice_data,channel_index)=>{
		let midi_data = (JSON.parse(JSON.stringify(voice_data.data.midi_data)))
		let index_position = 0
		//changing instruments
		let voice_instrument = voice_data.instrument
		let channel_elements=(voice_data.e_sound==22)?p_channel:e_channel
		let nzc_note = midi_data.note_freq.map(freq=>{return freq.map(f=>{return DATA_Hz_to_note(f,current_data.global_variables.TET)})})
		let freq_note = midi_data.note_freq
		let note_sequence = []//array nuzic_note and frequence
		for (let i = 0 ; i<nzc_note.length;i++){
			note_sequence.push({"nzc" : nzc_note[i], "freq" : freq_note[i]})
		}
		let time_list = midi_data.time
		//duration iT_freq
		let duration_list = midi_data.duration
		let Dduration = 0
		note_sequence.forEach((note,index)=>{
			if(note.nzc<0){
				if(note.nzc==-3){
					//ligadura
					Dduration+=(1/duration_list[index])
				}else{
					//silence or element
					Dduration=0
					//animation silence and element
					let duration = 1/duration_list[index]
					//element
					if(note.nzc==-2){
						let midiNote = voice_data.e_sound+12
						sequencer_instructions.push([time_list[index],{action: "SF_noteOn", channel:channel_elements,voice_id: voice_data.voice_id, note:[{midiNote:midiNote,cents:0}]}])
					}
				}
			}else{
				//it is a note
				let duration = 1/duration_list[index]
				if(Dduration!=0){
					duration += Dduration
					//reset
					Dduration=0
				}
				let start_note_time = time_list[index]
				let end_note_time = start_note_time+duration
				//control loop start/end
				//if commented (and sequence everything) this will prevent cutting a note in a loop if that end after the loop
				if(end_note_time>end_seconds && start_note_time<end_seconds){
					duration= end_seconds-start_note_time
				}
				//calculate midiNotes
				let midiNotes=[]
				note.freq.forEach(frequence=>{
					let midiNote_float = 69+12*Math.log2(frequence/440)
					let midiNote=Math.round(midiNote_float)
					let midiNote_freq=Math.pow(2, (midiNote-69)/12) * 440
					let cents=Math.round(1200*Math.log2(frequence/midiNote_freq))
					cents=(cents==0)?0:cents//-0 == 0
					midiNotes.push({midiNote:midiNote,cents:cents})
				})
				sequencer_instructions.push([start_note_time,{action: "SF_noteOn", channel:channel_index,voice_id: voice_data.voice_id, note:midiNotes}])
				//avoid lag same note
				let end_time=start_note_time+duration-((duration>0.01)?0.01:0)
				sequencer_instructions.push([end_time,{action: "SF_noteOff", channel:channel_index, note:midiNotes}])
			}
		})
	})
	SF_manager.synth.nuzic_sequencer.new_sequence(sequencer_instructions,false,start_seconds,end_seconds,"APP_stop_for_WEB")
}

function APP_play_for_WEB(data_in){
	let current_data = DATA_control_database_version(data_in)
	//var current_data = data_in
	APP_stop_for_WEB()
	let now =0
	let start =0
	let end = current_data.global_variables.Li
	let buffer = 0.3
	//not using bar to define transport position
	let now_seconds = 0
	let start_seconds = 0
	let end_seconds = end*(60/current_data.global_variables.PPM)
	//chrome and safari need action to start context
	if(SF_manager==null)return
	if(SF_manager.synth.nuzic_sequencer ==null)return
	if(!SF_manager.synth.nuzic_sequencer.isPlaying){
		//from the start
		APP_sequencer_for_WEB(current_data)
		//countin
		let j = current_data.global_variables.count_in
		for (let i=0; i<j; i++){
			let countIn_time = i *1000 * 60 / current_data.global_variables.PPM
			setTimeout(function () {
				SF_manager.synth.noteOn(p_channel,p_channel_midiNote.Accent_weak,0 ,90)
			}, countIn_time)
		}
		let countIn_end = j * 1000 * 60 / current_data.global_variables.PPM - buffer * 1000//buffering
		setTimeout(function () {
			let now_seconds_buffer_SF=now_seconds-0.05
			if(now_seconds==0){
				now_seconds_buffer_SF=now_seconds
			}
			SF_manager.synth.nuzic_sequencer.start(now_seconds_buffer_SF)
		}, countIn_end)
	}else {
		//NOTHING TO DO
	}
}

function APP_stop_for_WEB(){
	if(SF_manager==null)return
	if(SF_manager.synth.nuzic_sequencer ==null)return
	if(SF_manager.synth.nuzic_sequencer.isPlaying){
		//need to stop tone event beeng played
		Play_Compo_from_widget_END()
		SF_manager.synth.nuzic_sequencer.stop()
		SF_manager.synth.stopAll()
	}
}

function DATA_Hz_to_note_WEB(freq,note_TET=null){
	let f_princ = 261.62
	let f_princ_reg = 4
	//LA is the 0
	//var f_princ = 440
	//register TET
	if(note_TET==null){
		console.error("specify TET")
		return
	}
	let note_number = -1 //silence
	if(freq<0){
		//is a Nuzic special object
		note_number = freq
	}else{
		note_number = Math.round((Math.log2(freq/f_princ)+f_princ_reg)*note_TET)
	}
	return note_number
}

function WEB_volume_value_to_dB(value){
	//transform volume value 0-1 to decibel value -20 a 0
	let dB = -20 + value * 20
	return dB
}

// function WEB_frequency_to_percentage(frequency){
// 	let f_princ = 261.62
// 	let f_princ_reg = 4
// 	let f_first_note = f_princ/Math.pow(2,f_princ_reg)
// 	let f_last_note = Math.pow(2, (83-(f_princ_reg*12))/12) * f_princ
// 	let value_0 = Math.log(f_first_note)/Math.log(f_last_note)
// 	let value = Math.log(frequency)/Math.log(f_last_note)
// 	let linear = (value- value_0)/(1-value_0)
// 	return linear
// }

function WEB_minimap_get_jpg_for_web_component(datatest=null){
	if (datatest == null) datatest = DATA_get_current_state_point(false)
	let canvas = document.createElement('canvas')
	let timeData = []
	let freqData = []
	let colorData = []
	let timeSig = 0
	let f_princ = 261.62
	let f_princ_reg = 4
	let f_first_note = f_princ/Math.pow(2,f_princ_reg)
	let f_last_note = Math.pow(2, (83-(f_princ_reg*12))/12) * f_princ
	let value_0 = Math.log(f_first_note)/Math.log(f_last_note)
	let percentual_freq_array = []
	timeSig=datatest.global_variables.Li/datatest.global_variables.PPM*60*18
	//refresh tab global scale if there is a new database
	datatest.voice_data.forEach(voice => {
		// voice.data.midi_data
		voice.data.midi_data.time.push(60*datatest.global_variables.Li/datatest.global_variables.PPM)
		timeData.push(voice.data.midi_data.time)
		freqData.push(voice.data.midi_data.note_freq.map(f_list=>{
			let value_list=[]
			f_list.forEach(f=>{
				value_list.push(_frequency_to_percentage(f))
			})
			return value_list
			//return WEB_frequency_to_percentage(f)
		}))
		colorData.push(eval(voice.color))
	})
	canvas.height= 84
	canvas.width = timeSig
	let ctx = canvas.getContext('2d')
	for(let j=0; j<freqData.length;j++){
		let color=colorData[j]
		for(let i=0; i<freqData[j].length;i++){
			freqData[j][i].forEach(pf=>{
				if(isNaN(pf))return
				ctx.beginPath()
				ctx.moveTo(Math.ceil(timeData[j][i]*18),Math.round((1-pf)*canvas.height))
				ctx.lineTo(Math.ceil(timeData[j][i+1]*18),Math.round((1-pf)*canvas.height))
				//ctx.strokeStyle = nuzic_dark
				ctx.strokeStyle = color
				ctx.stroke()
			})
		}
	}
	let img = canvas.toDataURL('image/png')
	return img
	function _frequency_to_percentage(frequency){
		let value = Math.log(frequency)/Math.log(f_last_note)
		let linear = (value- value_0)/(1-value_0)
		return linear
	}
}

function DATA_list_voice_data_metro_fractioning_for_web(voice_data){
	//var fractioning = [{"time": "", "freq":"", "rep":""}]
	let fractioning = []
	let data = (JSON.parse(JSON.stringify(voice_data.data.segment_data)))
	let fractioning_data_raw = []
	data.forEach(segment=>{
		segment.fraction.forEach(F_range_raw=>{
			fractioning_data_raw.push(F_range_raw)
		})
	})
	let D_NP = voice_data.neopulse.D
	let N_NP = voice_data.neopulse.N
	let prev_time_range_end = 0
	fractioning = fractioning_data_raw.map(raw=>{
		//calcolate starting time
		let time = DATA_calculate_exact_time(prev_time_range_end,0,0,1,1,N_NP,D_NP)
		//actualize range and next fractioning
		let range = raw.stop-raw.start
		prev_time_range_end += range
		let time2 = DATA_calculate_exact_time(prev_time_range_end,0,0,1,1,N_NP,D_NP)
		let rep = range* raw.D/raw.N
		let freq = rep/(time2-time)
		return {time, freq, rep}
	})
	return fractioning
}
