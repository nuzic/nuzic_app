//general functions for RE
var debug_RE=false
if(DEBUG_MODE && DEBUG_MODE_EXTRA)debug_RE=true

function RE_redraw(data,debug_RE){
	//monolithic build of RE structures
	//OLD CODE
	let  t1, t2,
	startTimer = function() {
		t1 = new Date().getTime();
		return t1;
	},
	stopTimer = function() {
		t2 = new Date().getTime();
		return t2 - t1;
	}

	if(debug_RE)startTimer()
	//calculate time alignments
	let [global_note_voices_changes,global_time_voices_changes,global_time_segment_change,global_time_all_changes]=RE_calculate_alignment_global_parameters(data)
	//changing global variables
	RE_global_note_voices_changes=global_note_voices_changes
	RE_global_time_voices_changes=global_time_voices_changes
	RE_global_time_segment_change = global_time_segment_change
	RE_global_time_all_changes = global_time_all_changes
	//let RE_grades_data=DATA_calculate_grade_data(data.global_variables.Li)
	let RE_grades_data=DATA_calculate_grade_data(Li)
	//sorta global var
	//let idea_scale_list=DATA_get_idea_scale_list()
	if(debug_RE){
		console.log('---RE time calculations ' + stopTimer() + 'ms')
		startTimer()
	}
	current_position_PMCRE.RE.scrollbar=false //needed for scrollbar pre position
	//VOICES DATA
	let voice_data = data.voice_data
	let voices_container = document.querySelector(".App_RE_voice_matrix")
	let RE_container = document.querySelector(".App_RE")
	//copy error data
	let info_obj=document.getElementById("App_RE_info")
	let info_class=(info_obj!=null)?((info_obj.classList.contains("App_RE_info_preview"))?'class="App_RE_info_preview"':''):''
	let info=(info_obj!=null)?info_obj.querySelector(".App_RE_info_container").innerHTML:""
	let App_RE_S_show_content_obj = document.querySelector(".App_RE_S_show_visualization_options_box")
	let App_RE_C_show_content_obj = document.querySelector(".App_RE_C_show_visualization_options_box")
	let App_RE_S_show_content = (App_RE_S_show_content_obj!=null)?(App_RE_S_show_content_obj.style.display=='none'?"none":"block"):"none"
	let App_RE_C_show_content = (App_RE_C_show_content_obj!=null)?(App_RE_C_show_content_obj.style.display=='none'?"none":"block"):"none"
	let Na_pressed=data.global_variables.RE.Na?"pressed":""
	let Nm_pressed=data.global_variables.RE.Nm?"pressed":""
	let Ngm_pressed=data.global_variables.RE.Ngm?"pressed":""
	let Nabc_pressed=data.global_variables.RE.Nabc?"pressed":""
	let iNa_pressed=data.global_variables.RE.iNa?"pressed":""
	let iNm_pressed=data.global_variables.RE.iNm?"pressed":""
	let iNgm_pressed=data.global_variables.RE.iNgm?"pressed":""
	let Ta_pressed=data.global_variables.RE.Ta?"pressed":""
	let Tm_pressed=data.global_variables.RE.Tm?"pressed":""
	let Ts_pressed=data.global_variables.RE.Ts?"pressed":""
	let iT_pressed=data.global_variables.RE.iT?"pressed":""
	let Tf_pressed=data.global_variables.RE.Tf?"pressed":""
	let Aa_pressed=data.global_variables.A.type=="Aa"?"pressed":""
	let Am_pressed=data.global_variables.A.type=="Am"?"pressed":""
	let Ag_pressed=data.global_variables.A.type=="Ag"?"pressed":""
	let mute_A_pressed = data.global_variables.A.mute?"pressed":""
	let show_A=data.global_variables.A.show
	let A_pressed = show_A?"pressed":""
	let type_A=data.global_variables.A.type
	let A_label_text=""
	//in bw lines
	if(data.global_variables.A.type=="Aa"){
		A_label_text="iAa"
	}else if(data.global_variables.A.type=="Am"){
		A_label_text="iAm"
	}else if(data.global_variables.A.type=="Ag"){
		A_label_text="iA°"
	}
	A_global_list=[]
	if(data.global_variables.A.type=="Ag"){
		let type=data.global_variables.A.type
		voice_data.forEach(voice=>{
			if(voice.RE_visible){
				let Psg_=0
				let N_NP = voice.neopulse.N
				let D_NP = voice.neopulse.D
				voice.data.segment_data.forEach(segment=>{
					let Psg_segment_start = Psg_
					let Psg_segment_end = Psg_segment_start + segment.time.slice(-1)[0].P
					Psg_=Psg_segment_end
					segment.sound.forEach((sound,index)=>{
						if(sound.note<0)return
						let current_P = segment.time[index].P
						let current_F = segment.time[index].F
						//find correct scale
						//find Pa_equivalent and verify if inside a scale
						let current_fraction = segment.fraction.find(fraction=>{return fraction.stop>current_P})
						let frac_p = current_fraction.N/current_fraction.D
						let Pa_equivalent = (Psg_segment_start+current_P) * N_NP/D_NP + current_F* frac_p * N_NP/D_NP
						let current_grades_data= RE_grades_data[DATA_get_Pa_eq_grade_data_index(Pa_equivalent,RE_grades_data)]
						//if(current_scale!=null){
							APP_add_A_text_to_A_global_list(APP_A_list_to_text(sound.A_pos,type,{current_grades_data,base_note:sound.note,base_diesis:sound.diesis,pos:true}))
							APP_add_A_text_to_A_global_list(APP_A_list_to_text(sound.A_neg,type,{current_grades_data,base_note:sound.note,base_diesis:sound.diesis,pos:false}))
						// }else{
						// 	APP_add_A_text_to_A_global_list(APP_A_list_to_text(sound.A_pos,"Am"))
						// 	APP_add_A_text_to_A_global_list(APP_A_list_to_text(sound.A_neg,"Am"))
						// }
					})
				})
			}
		})
	}else{
		let type=data.global_variables.A.type
		voice_data.forEach(voice=>{
			if(voice.RE_visible){
				voice.data.segment_data.forEach(segment=>{
					segment.sound.forEach(sound=>{
						if(sound.note<0)return
						APP_add_A_text_to_A_global_list(APP_A_list_to_text(sound.A_pos,type))
						APP_add_A_text_to_A_global_list(APP_A_list_to_text(sound.A_neg,type))
					})
				})
			}
		})
	}
	let Rg=data.global_variables.TET*N_reg
	let final_seg_time = DATA_calculate_exact_time(Li,0,0,1,1,1,1)
	let final_time = RE_global_time_all_changes[RE_global_time_all_changes.length-1]
	let extra = 0
	if(final_seg_time!=final_time)extra=1
	//scrollbar
	let n_boxes = 1+(RE_global_time_all_changes.length-1) * 2 + (RE_global_time_segment_change.length-1)*2 +extra
	let n_end_space = (RE_global_time_all_changes.length - 1 -RE_global_time_all_changes.indexOf(RE_global_time_segment_change.slice(-1)[0]))*2
	if(n_end_space<0) n_end_space=0
	//give 1 more bc S and T line end with a div that cover the boxes
	n_end_space = extra==1? n_end_space+1:n_end_space
	let final_space = n_end_space+1
	let iA_n_boxes=n_boxes-n_end_space-2
	//S line calculation
	let scale_sequence = data.scale.scale_sequence
	let scale_wantedPositions = []
	let scale_wantedWidth = []
	//add final time
	let scale_time = 0
	let scale_pulse = 0
	//using  RE_find_time_column_index(time_scale_end,RE_global_time_all_changes,RE_global_time_segment_change)
	//let scale_current_time = 0
	scale_sequence.forEach((item) => {
		let duration = item.duration
		let time_scale_end = DATA_calculate_exact_time(scale_pulse,duration,0,1,1,1,1)
		let wanted_position = RE_find_time_column_index(scale_time,RE_global_time_all_changes,RE_global_time_segment_change)+1
		scale_wantedPositions.push(wanted_position)
		//scale_current_time = scale_time
		scale_time = time_scale_end
		scale_pulse += duration
	})
	scale_wantedPositions.forEach((position,index)=>{
		//with ghost
		let next_position=0
		if(scale_wantedPositions[index+1]!=null){
			next_position=scale_wantedPositions[index+1]
		}else{
			//all the times already calc
			next_position= RE_find_time_column_index(scale_time,RE_global_time_all_changes,RE_global_time_segment_change)+1
		}
		scale_wantedWidth.push(next_position-position)
	})
	if(debug_RE){
		console.log('---RE time generate time arrays ' + stopTimer() + 'ms')
		startTimer()
	}

	let S_line_string = ""
	scale_sequence.forEach((element, index) => {
		let S_obj_left = nuzic_block * (scale_wantedPositions[index])
		let S_obj_width = nuzic_block * (scale_wantedWidth[index])
		S_line_string+=`<div class="App_RE_S_obj" style="left:${S_obj_left}px;width:${S_obj_width}px"
							onmousemove="RE_break_scale_range_calculate_position(this,event)">
							<div class="App_RE_S_scale" style="display: inline;" onmouseenter="RE_exit_break_scale_range(null,this.parentNode)" onmousemove="event.stopPropagation()">
								<div class="App_RE_S_value" value='${JSON.stringify(element)}' onclick="RE_show_scale_circle(this);event.stopPropagation()">
									<p class="App_RE_sub_S_value">${element.acronym}${element.rotation}.${element.starting_note}</p>
								</div>
								<div class="App_RE_S_duration" onclick="RE_show_scale_input(this)">
									<p class="App_RE_sub_S_duration">${element.duration}</p>
								</div>
							</div>
							<div class="App_RE_break_scale_range hidden" onclick="RE_break_scale_range(this)" onmouseleave="RE_exit_break_scale_range(this)" >
								<p>+</p>
								<p class="App_RE_break_scale_info"></p>
							</div>
						</div>`
	})
	//ghost scale
	let last_compas= data.compas[data.compas.length-1]
	let end_last_compas_abs_pulse = last_compas.compas_values[0]+last_compas.compas_values[1]*last_compas.compas_values[2]
	let max_abs_pulse=(Li>end_last_compas_abs_pulse)?Li:end_last_compas_abs_pulse
	if(scale_sequence.length!=0 && scale_pulse<max_abs_pulse){
		let duration=max_abs_pulse-scale_pulse
		let element=scale_sequence.slice(-1)[0]
		let position=scale_wantedPositions.slice(-1)[0]+scale_wantedWidth.slice(-1)[0]
		let S_obj_left = nuzic_block * position
		let S_obj_width = nuzic_block * (n_boxes-position)
		let value={"acronym":element.acronym,"rotation":element.rotation,"starting_note":element.starting_note,"duration":duration}
		S_line_string+=`<div class="App_RE_S_obj ghost" style="left:${S_obj_left}px;width:${S_obj_width}px;opacity: 50%;" onmousemove="RE_break_scale_range_calculate_position(this,event)" onmousemove="event.stopPropagation()">
							<div class="App_RE_S_scale" style="display: inline;">
								<div class="App_RE_S_value" onclick="BNAV_add_last_scale_sequence()" value='${JSON.stringify(value)}'>
									<p class="App_RE_sub_S_value">${element.acronym}${element.rotation}.${element.starting_note}</p>
								</div>
								<div class="App_RE_S_duration">
									<p class="App_RE_sub_S_duration">${duration}</p>
								</div>
							</div>
							<div class="App_RE_break_scale_range hidden" onclick="RE_break_scale_range(this)" onmouseleave="RE_exit_break_scale_range(this)">
								<p>+</p>
								<p class="App_RE_break_scale_info"></p>
							</div>
						</div>`
	}else if(scale_sequence.length==0){
		//single ghost
		let S_obj_left = nuzic_block
		let S_obj_width = nuzic_block * (n_boxes -1)
		let value={"acronym":null,"rotation":null,"starting_note":null,"duration":max_abs_pulse}
		S_line_string+=`<div class="App_RE_S_obj ghost" style="left:${S_obj_left}px;width:${S_obj_width}px;opacity: 50%;" onmousemove="RE_break_scale_range_calculate_position(this,event)" onmousemove="event.stopPropagation()">
							<div class="App_RE_S_scale" style="display: inline;">
								<div class="App_RE_S_value" onclick="BNAV_add_first_scale_sequence()" value='${JSON.stringify(value)}'>
									<img class="App_RE_sub_S_value" src="./Icons/add.svg">
								</div>
								<div class="App_RE_S_duration">
									<p class="App_RE_sub_S_duration">${max_abs_pulse}</p>
								</div>
							</div>
							<div class="App_RE_break_scale_range hidden" onclick="RE_break_scale_range(this)" onmouseleave="RE_exit_break_scale_range(this)">
								<p>+</p>
								<p class="App_RE_break_scale_info"></p>
							</div>
						</div>`
	}
	if(debug_RE){
		console.log('---RE scale sequence ' + stopTimer() + 'ms')
		startTimer()
	}

	//C line calculation
	let C_line_string = ""
	let C_line = document.querySelector(".App_RE_C")
	let compas_values = data.compas
	let compas_valuesWithReps = []
	let counterCompasReps = 0
	for(i=0;i<compas_values.length;i++){
		compas_values[i].compas_values[0] = counterCompasReps
		compas_valuesWithReps.push(compas_values[i])
		counterCompasReps+=compas_values[i].compas_values[1]
		if(compas_values[i].compas_values[2]!=1){
			for(j=1;j<compas_values[i].compas_values[2];j++){
				let tempCompas = {compas_values:[counterCompasReps,compas_values[i].compas_values[1],compas_values[i].compas_values[2],compas_values[i].compas_values[3],compas_values[i].compas_values[4],true]}
				compas_valuesWithReps.push(tempCompas)
				counterCompasReps+=tempCompas.compas_values[1]
			}
		}
	}
	let compas_wantedPositions = []
	//new compas wanted position
	compas_valuesWithReps.forEach((item) => {
		let pulse = parseInt(item.compas_values[0])
		let scale_time = DATA_calculate_exact_time(pulse,0,0,1,1,1,1)
		let wanted_position = RE_find_time_column_index(scale_time,RE_global_time_all_changes,RE_global_time_segment_change)+1
		compas_wantedPositions.push(wanted_position)
	})
	compas_valuesWithReps.forEach((element, index) => {
		let C_button_left = nuzic_block*(compas_wantedPositions[index])
		let C_button_position = `${element.compas_values[0]}`
		let C_button_number = `${index}`
		let button_extra_string=""
		if(element.compas_values.length==5){
			button_extra_string = `	<div class="App_RE_C_range">
										<p class="App_RE_sub_C_range">${element.compas_values[1]}</p>
									</div>`
		}
		C_line_string+=`
						<button class="App_RE_C_button" style="left:${C_button_left}px" position="${C_button_position}" number="${C_button_number}" onclick="BNAV_show_compas_input(this)">
							<div class="App_RE_C_inp_box">
								<div class="App_RE_C_value">
									<p class="App_RE_sub_C_value">${index}</p>
								</div>
								${button_extra_string}
							</div>
						</button>`
	})
	let c_string=APP_language_return_text_id("re_dt")
	let s_string=APP_language_return_text_id("re_ds")
	function APP_language_return_text_id(id){
		if(lang_localization_list.length==0)return ""
		let app_text_list = lang_localization_list.find(lang=>{return lang.current}).text
		let found = app_text_list.find(item=>{
			return item.id==id
		})
		if(found==null){
			return id
		}
		return found.text
	}
	if(debug_RE){
		console.log('---RE compas sequence ' + stopTimer() + 'ms')
		startTimer()
	}
	let monolithic_string=`<div class="App_RE_pink_scale_range_background hidden"></div>
					<div class="App_RE_scale_modules">
						<label class="App_RE_S_l" onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)"
							data-value="RgHover">Rg</label>
						<label class="App_RE_S_i" id="RE_Rg">${Rg}</label>
						<label class="App_RE_S_l" onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)"
							data-value="TETHover">TET</label>
						<input class="App_RE_S_i" id="RE_TET" onkeypress="APP_inpTest_intPos(event,this)"
							onfocusout="APP_enter_new_value_TET(this)" maxlength="2" value="${data.global_variables.TET}"></input>
						<button class="App_S_swich_mod_grade" onclick="DATA_switch_sound_preferences_mod_grade()"><img src="./Icons/switch_grade_note.svg"/></button>
						<div class="App_RE_S_arrow">
							<button class="App_RE_S_show_visualization_options_button" value="0"><img src="./Icons/arrow_open_rotated_dark.svg"/></button>
							<div class="App_RE_S_show_visualization_options_box" style="display:${App_RE_S_show_content}">
								<p class="App_RE_S_show_visualization_options_title">${s_string}</p>
								<div class="App_RE_S_show_visualization_options_content" >
									<div class="App_RE_S_show_visualization_options_contentL">
										<p>N</p>
										<p>iS</p>
										<button data-value="showAHover" onmouseenter="APP_hover_area_enter(this);" onmouseleave="APP_hover_area_exit(this);"
												class="hoverLabel App_button_abs App_button_sound_A_show ${A_pressed}"
												onclick="DATA_change_show_A()">A</button>
									</div>
									<div class="App_RE_S_show_visualization_options_contentC">
										<button data-value="NaHover" onmouseenter="APP_hover_area_enter(this);" onmouseleave="APP_hover_area_exit(this);"
											class="hoverLabel App_button_abs App_button_sound ${Na_pressed}"
											onclick="RE_select_line_type_button('Na')">a</button>
										<button data-value="NHover" onmouseenter="APP_hover_area_enter(this);" onmouseleave="APP_hover_area_exit(this);"
											class="hoverLabel App_button_mod App_button_sound ${Nm_pressed}"
											onclick="RE_select_line_type_button('Nm')">m</button>
											<button data-value="iSaHover" onmouseenter="APP_hover_area_enter(this);" onmouseleave="APP_hover_area_exit(this);"
											class="hoverLabel App_button_abs App_button_sound ${iNa_pressed}"
											onclick="RE_select_line_type_button('iNa')">a</button>
										<button data-value="iSHover" onmouseenter="APP_hover_area_enter(this);" onmouseleave="APP_hover_area_exit(this);"
											class="hoverLabel App_button_mod App_button_sound ${iNm_pressed}"
											onclick="RE_select_line_type_button('iNm')">m</button>
										<button data-value="iAaHover" onmouseenter="APP_hover_area_enter(this);" onmouseleave="APP_hover_area_exit(this);"
											class="hoverLabel App_button_abs App_button_sound_A ${Aa_pressed}"
											onclick="DATA_change_type_A('Aa')">a</button>
										<button data-value="iAmHover" onmouseenter="APP_hover_area_enter(this);" onmouseleave="APP_hover_area_exit(this);"
											class="hoverLabel App_button_mod App_button_sound_A ${Am_pressed}"
											onclick="DATA_change_type_A('Am')">m</button>
									</div>
									<div class="App_RE_S_show_visualization_options_contentR">
										<button data-value="NºHover" onmouseenter="APP_hover_area_enter(this);" onmouseleave="APP_hover_area_exit(this);"
											class="hoverLabel App_button_mod App_button_sound ${Ngm_pressed}"
											onclick="RE_select_line_type_button('Ngm')">Nº</button>
										<button data-value="NabcHover" onmouseenter="APP_hover_area_enter(this);" onmouseleave="APP_hover_area_exit(this);"
											class="App_RE_Nabc_button hoverLabel App_button_mod ${Nabc_pressed}"
											onclick="RE_select_line_type_button('Nabc')" type="0->1->2">#/♭</button>
										<button data-value="iSºHover" onmouseenter="APP_hover_area_enter(this);" onmouseleave="APP_hover_area_exit(this);"
											class="hoverLabel App_button_mod App_button_sound ${iNgm_pressed}"
											onclick="RE_select_line_type_button('iNgm')">iSº</button>
										<button data-value="iA°Hover" onmouseenter="APP_hover_area_enter(this);" onmouseleave="APP_hover_area_exit(this);"
											class="hoverLabel App_button_mod App_button_sound_A ${Ag_pressed}"
											onclick="DATA_change_type_A('Ag')">iAº</button>
										<button id="App_RE_mute_A" class="${mute_A_pressed}"
											onclick="DATA_change_mute_A()"><img src="./Icons/audio_volume_small_w.svg"></button>
									</div>
								</div>
							</div>
						</div>
						<div class="App_RE_S">
							${S_line_string}
							<div class="App_RE_SC_scrollDiv" style="width:calc(var(--RE_block) * ${n_boxes});min-width:calc(var(--RE_block) * ${n_boxes})"></div>
						</div>
						<label class="App_RE_S_l_title" onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)"
							data-value="">S</label>
					</div>
					<div class="App_RE_compas_modules">
						<label class="App_RE_C_l" onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)"
							data-value="LiHover">Li</label>
						<input id="RE_Li" class="App_RE_C_i" onkeypress="APP_inpTest_intPos(event,this)"
							onfocusin="APP_set_previous_value(this)" onfocusout="APP_enter_new_value_Li(this)"
							maxlength="4" value="${data.global_variables.Li}"></input>
						<label class="App_RE_C_l" onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)"
							data-value="PPMHover">PPM</label>
						<input id="RE_PPM" class="App_RE_C_i" onkeypress="APP_inpTest_intPos(event,this)"
							onfocusin="APP_set_previous_value(this)" onfocusout="APP_enter_new_value_PPM(this)"
							maxlength="3" value="${data.global_variables.PPM}"></input>
						<div class="App_RE_C_arrow">
							<button class="App_RE_C_show_visualization_options_button" value="0"><img src="./Icons/arrow_open_rotated_dark.svg"/></button>
							<div class="App_RE_C_show_visualization_options_box" style="display:${App_RE_C_show_content}">
								<p class="App_RE_C_show_visualization_options_title">${c_string}</p>
								<div class="App_RE_C_show_visualization_options_content" >
									<div class="App_RE_C_show_visualization_options_contentL">
										<p>P</p>
										<p>iT</p>
									</div>
									<div class="App_RE_C_show_visualization_options_contentC">
										<button data-value="PaHover" onmouseenter="APP_hover_area_enter(this);" onmouseleave="APP_hover_area_exit(this);"
											class="hoverLabel App_button_abs App_RE_line_button_time ${Ta_pressed}"
											onclick="RE_select_line_type_button('Ta')">a</button>
										<button data-value="PmHover" onmouseenter="APP_hover_area_enter(this);" onmouseleave="APP_hover_area_exit(this);"
											class="hoverLabel App_button_mod App_RE_line_button_time ${Tm_pressed}"
											onclick="RE_select_line_type_button('Tm')">m</button>
										<button data-value="iTHover" onmouseenter="APP_hover_area_enter(this);" onmouseleave="APP_hover_area_exit(this);"
											class="hoverLabel App_button_abs App_RE_line_button_time  ${iT_pressed}"
											onclick="RE_select_line_type_button('iT')">a</button>
									</div>
									<div class="App_RE_C_show_visualization_options_contentR">
										<button data-value="PsHover" onmouseenter="APP_hover_area_enter(this);" onmouseleave="APP_hover_area_exit(this);"
											class="hoverLabel App_button_mod App_RE_line_button_time ${Ts_pressed}"
											onclick="RE_select_line_type_button('Ts')">sg</button>
									</div>
								</div>
								<div class="App_RE_C_show_visualization_options_bottom">
									<button data-value="frTHover" onmouseenter="APP_hover_area_enter(this);"
									onmouseleave="APP_hover_area_exit(this);"
									class="hoverLabel App_button_abs App_RE_line_button_time ${Tf_pressed}"
									onclick="RE_select_line_type_button('Tf')">Fr</button>
								</div>
							</div>
						</div>
						<div class="App_RE_C">
							${C_line_string}
							<div class="App_RE_SC_scrollDiv" style="width:calc(var(--RE_block) * ${n_boxes});min-width:calc(var(--RE_block) * ${n_boxes})">
							</div>
						</div>
						<label class="App_RE_C_l_title" onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)"
							data-value="c+ETHover">T</label>
					</div>
					<div class="App_RE_column_selector">
						<div id="App_RE_info" ${info_class}>
							<button class="App_RE_info_button" onclick="APP_show_info(this)"><img src="./Icons/arrow_open_rotated_dark.svg"/></button>
							<div class="App_RE_info_container">${info}</div>
						</div>`
	if(debug_RE){
		console.log('---RE Create headers S and C ' + stopTimer() + 'ms')
		startTimer()
	}
	//draw column selector IF blocked voice is visible
	let blocked_voice=data.voice_data.find(voice=>{return voice.blocked==true && voice.RE_visible})
	let column_selector_div = ""
	if(typeof(blocked_voice) != "undefined"){
		column_selector_div=`<div id="App_RE_column_selector_container" style="display : flex">`
		//draw segments headers
		let segment_list = blocked_voice.data.segment_data
		//svg
		let svg_width = RE_global_time_all_changes.length * 2 * nuzic_block + segment_list.length * nuzic_block * 2 +nuzic_block
		let div=`<svg id="App_RE_column_selector_svg" style="min-width: ${svg_width}px;"></svg><svg id="App_RE_column_selector_top_svg" style="min-width: ${svg_width}px;"></svg>`
		let N_NP = blocked_voice.neopulse.N
		let D_NP = blocked_voice.neopulse.D
		let Psg_=0
		let n_seg=segment_list.length
		segment_list.forEach((segment,indexS)=>{
			let Psg_segment_start = Psg_
			let Psg_segment_end = Psg_segment_start + segment.time.slice(-1)[0].P
			Psg_=Psg_segment_end
			//calculate times
			let time_segment_start= DATA_calculate_exact_time(Psg_segment_start,0,0,1,1,N_NP,D_NP)
			let time_segment_end= DATA_calculate_exact_time(Psg_segment_end,0,0,1,1,N_NP,D_NP)
			let index_col_start = RE_find_time_column_index(time_segment_start)
			let index_col_end = RE_find_time_column_index(time_segment_end)
			let n_column = (index_col_end-index_col_start)-1

			//white space
			div+=`<div class="App_RE_column_selector_spacer"></div>
				<div class="App_RE_column_selector_segment" style="width: calc(var(--RE_block)* ${n_column}); min-width: calc(var(--RE_block)* ${n_column});" onclick="RE_toggle_selection_segment_column(${indexS})">
					<div class="App_RE_column_selector_segment_menu">
					<div class="App_RE_column_selector_segment_menu_button" ><img class="App_RE_column_selector_icon" src="./Icons/menu.svg"></div>
				<div class="App_RE_dropdown_column_selector_content">
					<a onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)" data-value="NameColSegHover" onclick="RE_change_column_segment_name_button(this)"><img class="App_RE_icon" src="./Icons/op_menu_rename.svg"></a>
					<a onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)" data-value="ClearColSegHover" onclick="RE_clear_column_segment_button(this)"><img class="App_RE_icon" src="./Icons/op_menu_clear.svg"></a>
					<a onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)" data-value="PasteColSegHover" onclick="RE_repeat_column_segment_button(this,event)"><img class="App_RE_icon" src="./Icons/op_menu_repeat_right.svg"></a>
					<a onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)" data-value="CopyColSegHover" onclick="RE_copy_column_segment_button(this)"><img class="App_RE_icon" src="./Icons/op_menu_copy.svg"></a>
					<a onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)" data-value="PasteColSegHover" onclick="RE_paste_column_segment_button(this)"><img class="App_RE_icon" src="./Icons/op_menu_paste.svg"></a>
					<a onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)" data-value="OperColSegHover" onclick="RE_operations_column_segment_button(this,event)"><img class="App_RE_icon" src="./Icons/op_menu_operations.svg"></a>
				</div>
			</div>
			<div class="App_RE_column_selector_segment_number">${indexS}</div>`
			//segment name if
			if(n_column>5 && segment.segment_name!=""){
				div+=`<input class="App_RE_column_selector_segment_name" onchange="RE_change_column_segment_name(this)" onfocusout="RE_hide_column_segment_name(this)" maxlength="8" placeholder="" value="${segment.segment_name}"></input>`
			}else{
				div+=`<input class="App_RE_column_selector_segment_name hidden" onchange="RE_change_column_segment_name(this)" onfocusout="RE_hide_column_segment_name(this)" maxlength="8" placeholder="" value="${segment.segment_name}"></input>`
			}
			//delete segment column
			if(n_seg>1)div+=`<div class="App_RE_column_selector_segment_delete" onclick="RE_delete_column_segment_button(this)"><img class="App_RE_column_selector_icon" src="./Icons/delete.svg"></div>`

			//close segment
			div+='</div>'
		})
		//final space
		div+=`<div style="min-width: calc(var(--RE_block) * ${final_space} + var(--RE_scrollbar_th))"></div>
				</div>
			`
		column_selector_div+=div
	}else{
		column_selector_div=`<div id="App_RE_column_selector_container" style="display : none"></div>`
	}

	monolithic_string+=`
						${column_selector_div}
						<div id="App_RE_column_selector_end"></div>
					</div>
					<div class="App_RE_voice_matrix" ondragover="RE_voice_matrix_dragover(this,event)">
			`
	//voices matrix
	//delete voice button visibility
	let class_delete_voice_button=""
	if(voice_data.length==1){
		//disable
		class_delete_voice_button="disabled"
	}
	let indexV=0
	//labels (12)
	let labels_order=["Na","Nm","Ngm","iNa","iNm","iNgm","Nabc","Ta","Tm","Ts","iT","Tf"]
	//data.global_variables.RE
	let global_labels_needed=[]
	labels_order.forEach(label=>{
		if(data.global_variables.RE[label])global_labels_needed.push(label)
	})
	//create voices
	let index_up=null
	if(debug_RE){
		console.log('---RE Create column selectors RE ' + stopTimer() + ' ms')
		startTimer()
	}
	for (let index = 0 ; index<voice_data.length; index++){
		//change seq number
		let seq_number=voice_data.length-index-1
		//iA
		let show_iA=true
		if (voice_data[index].RE_visible){
			if(index==0 || index_up==null || !show_A)show_iA=false
		} else {
			show_iA=false
		}
		let class_iA=""
		let div_iA_string=""
		if(show_iA){
			div_iA_string=RE_calculate_voice_iA_div_string(voice_data[index],voice_data[index_up],type_A,iA_n_boxes,RE_grades_data)
		}else{
			class_iA="hidden"
		}
		if (voice_data[index].RE_visible)index_up=index
		monolithic_string+=`<div class="App_RE_voices_between">
						<label class="App_RE_label ${class_iA}" onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)" data-value="${A_label_text}Hover">${A_label_text}</label>
						<div class="App_RE_iA ${class_iA}" onscroll="RE_voice_scrolling(this,event)">
							${div_iA_string}
							<div class="App_RE_segment_spacer_end" style="min-width:calc(var(--RE_block) * ${n_end_space})">
							</div>
						</div>
					</div>`
		//voice
		//Adding extra labels
		let labels_needed=JSON.parse(JSON.stringify(global_labels_needed))
		if(data.global_variables.A.show && voice_data[index].A){
			labels_needed.unshift(data.global_variables.A.type)
		}
		monolithic_string+=RE_calculate_voice_string(voice_data[index],seq_number,labels_needed,class_delete_voice_button,n_end_space,RE_grades_data)
	}
	if(debug_RE){
		console.log('---RE Create voices ' + stopTimer() + 'ms')
		startTimer()
	}
	//XXX APPLY POSITION XXX  scrollLeft="${scrollbarH_left}"
	monolithic_string+=`</div>
					<div class="App_RE_main_scrollbar_H" onscroll="RE_checkScroll_H()">
						<div class="App_RE_element_step" style="min-width :calc(var(--RE_block) * ${n_boxes});width:calc(var(--RE_block) * ${n_boxes})"></div>
					</div>
				</div>`
	//apply monolithic string
	RE_container.innerHTML=monolithic_string  //HERE IT IS SLOW XXX

	if(debug_RE){
		console.log('---RE Create voices monolithic ' + stopTimer() + 'ms')
		startTimer()
	}

	RE_forceScroll_HV(current_position_PMCRE.RE.X_start,current_position_PMCRE.RE.Y_start)
	//current_position_PMCRE.RE.scrollbar=true //XXX inside RE_forceScroll_HV with a timeout
	//positions in PMCRE XXX
	//RE_verify_segment_visibility()//XXX no need?? better if there is
	if(debug_RE)console.log('---RE Segments &| scrollbar' + stopTimer() + 'ms') //XXX HERE IS SLOW
}

function RE_calculate_alignment_global_parameters(data){
	let time_event_list = []
	let note_event_list = []
	// object of any event in notation (useful in order to get the string)
	let object_event_list = []
	// frequency of said event (or duration)
	let frequency_event_list = []
	//metronome
	let time_fraction_list = []
	let fraction_list = []
	//change of an segment
	let time_segment_change = []
	let iA_n_boxes = 0
	//a copy of database
	let current_data = JSON.parse(JSON.stringify(data))
	current_data.voice_data.forEach((voice)=>{
		//find voice id
		if(voice.RE_visible){
			//add ending time
			voice.data.midi_data.time.push(DATA_calculate_exact_time(Li,0,0,1,1,1,1))
			time_event_list.push({"voice_id":voice.voice_id,"time":voice.data.midi_data.time})
			time_segment_change.push(DATA_list_voice_data_segment_change(voice))
			let note_list = []
			voice.data.segment_data.forEach(segment=>{
				segment.sound.forEach(item=>{note_list.push(item.note)})
			})
			note_event_list.push({"voice_id":voice.voice_id,"note":note_list})
		}
	})
	//calculate how many Global time intervals exist (how many iA are needed)
	let provv = []
	time_event_list.forEach((item)=> {
		provv = provv.concat(item.time)
	})
	//RE_global_time_voices_changes=time_event_list
	provv.sort(function(a, b){return a-b});
	let global_time_event_list = [...new Set(provv)]
	//calculate globally when a new segment start (change of segments)
	provv = []
	time_segment_change.forEach((item)=> {
		provv = provv.concat(item)
	})
	provv.sort(function(a, b){return a-b});
	let global_time_segment_change = [...new Set(provv)]
	// XXX  why this global variable??? to draw compasses??????
	//console.log(global_time_segment_change)
	global_time_segment_change.push(DATA_calculate_exact_time(Li,0,0,1,1,1,1))
	let compas_pulse = 0
	let compas_change = [0]
	//add end compass
	current_data.compas.forEach(compas=>{
		for (let i = 0; i < compas.compas_values[2]; i++) {
			let next=compas_pulse+compas.compas_values[1]
			compas_change.push(next)
			compas_pulse=next
		}
	})
	// current_data.compas.forEach(compas=>{
	// 	for (let i = 0; i < compas.compas_values[2]; i++) {
	// 		compas_change.push(compas.compas_values[1]*i+compas.compas_values[0])
	// 	}
	// })
	let global_time_compas_change = compas_change.map(item=>{
		return DATA_calculate_exact_time(item,0,0,1,1,1,1)
	})
	//scale_line
	let scale_pulse = 0
	let global_time_scale_change = current_data.scale.scale_sequence.map(item=>{
		// var value = scale_time
		///avoid sum of truncated values
		let value = DATA_calculate_exact_time(scale_pulse,0,0,1,1,1,1)
		scale_pulse += item.duration
		return value
	})
	//Ghost! eventually add end scale sequence
	if(scale_pulse!=0){
		let value = DATA_calculate_exact_time(scale_pulse,0,0,1,1,1,1)
		global_time_scale_change.push(value)
	}

	//add global_time_segment_change && global_time_scale_change + global_time_compas_change for voice alignment
	provv = []
	provv=  global_time_compas_change.concat(global_time_segment_change)
	provv.sort(function(a, b){return a-b});
	let global_time_compas_segment_changes = [...new Set(provv)]

	provv = []
	provv=  global_time_compas_segment_changes.concat(global_time_scale_change)
	provv.sort(function(a, b){return a-b});
	let global_time_compas_scale_segment_changes = [...new Set(provv)]

	//add all the time changing change
	provv = []
	provv=  global_time_compas_scale_segment_changes.concat(global_time_event_list)
	provv.sort(function(a, b){return a-b});
	let global_time_all_changes = [...new Set(provv)]
	//RE_global_time_all_changes = global_time_all_changes
	return [note_event_list,time_event_list,global_time_segment_change,global_time_all_changes]
}

function RE_calculate_voice_iA_div_string(voice_data_down,voice_data_up,type_A,iA_n_boxes,RE_grades_data){
	let div_string=`<div class="App_RE_iA_spacer">
				<svg data-name="Trazado 10" height="28" width="28" viewBox="0 0 28 28" preserveAspectRatio="none">
					<line x1="14" y1="0" x2="14" y2="28" style="stroke-width:2"></line>
				</svg>
			</div>`
	//add inp boxes
	//XXX direction XXX XXX XXX XXX XXX XXX XXX XXX XXX
	let [iA_size_list,iA_info_list]=RE_calculate_iA_list(type_A ,voice_data_up , voice_data_down,RE_grades_data)
	//let iA_cell_pink="" everyone is pink
	iA_info_list.forEach((info,index)=>{
		if(info.void==false){
			//add input and label
			if(info.iA_data.Na_up ==-1 || info.iA_data.Na_up ==-2 || info.iA_data.Na_down ==-1 || info.iA_data.Na_down ==-2){
				if(info.iA_data.Na_down<0){
					//not possible
					//add a single empty label
					div_string+=`<label class="App_RE_iA_cell App_RE_iA_cell_border" style="min-width:calc(var(--RE_block) * ${iA_size_list[index]})"></label>`
				}else{
					if((type_A=="Ag" && (info.iA_data.scale_down==null || info.iA_data.scale_up==null)) || info.disabled){
						//add a single empty label
						div_string+=`<label class="App_RE_iA_cell App_RE_iA_cell_border" style="min-width:calc(var(--RE_block) * ${iA_size_list[index]})"></label>`
					}else{
						//is possible to add a value
						//div_string+=`<input class="App_RE_iA_cell" asd="" value="">`
						div_string+=RE_calculate_iA_element_string(null,type_A,info.disabled,info.iA_data,RE_grades_data)
						div_string+=`<label class="App_RE_iA_cell App_RE_iA_cell_border" style="min-width:calc(var(--RE_block) * ${iA_size_list[index]-1})"></label>`
					}
				}
			}else{
				div_string+=RE_calculate_iA_element_string(info.iA_data.Na_up-info.iA_data.Na_down,type_A,info.disabled,info.iA_data,RE_grades_data)
				div_string+=`<label class="App_RE_iA_cell App_RE_iA_cell_border" style="min-width:calc(var(--RE_block) * ${iA_size_list[index]-1})"></label>`
			}
			return
		}
		if(info.void==true){
			//add a single empty label
			div_string+=`<label class="App_RE_iA_cell App_RE_iA_cell_border" style="min-width:calc(var(--RE_block) * ${iA_size_list[index]})"></label>`
		}
	})
	// add spacer
	div_string+=`<div class="App_RE_iA_spacer"><svg data-name="Trazado 10" height="28" width="28" viewBox="0 0 28 28" preserveAspectRatio="none">
				<line x1="14" y1="0" x2="14" y2="28" style="stroke-width:2"></line></svg></div>`
	return div_string
}

function RE_calculate_voice_string(voice_data,seq_number,labels_needed,class_delete_voice_button,n_end_space,RE_grades_data){
	let voice_string=""
	//voice is blocked
	let class_blocked_button=""
	let class_blocked_voice=""
	let display_blocked_icon_0=""
	let display_blocked_icon_1=""
	if(voice_data.blocked){
		//is blocked
		class_blocked_button="pressed"
		class_blocked_voice="blocked"
		display_blocked_icon_0 = 'flex'
		display_blocked_icon_1= 'none'
	}else{
		//unblocked
		display_blocked_icon_0 = 'none'
		display_blocked_icon_1 = 'flex'
	}
	//volume
	let volume =voice_data.volume
	//set mute button
	let class_mute=(voice_data.mute)?"pressed":""
	//solo
	let class_solo=(voice_data.solo)?"pressed":""
	//instrument
	let instrument_number=voice_data.instrument
	//if(instrument_number>=allinstruments)instrument_number=0
	let inst_name= instrument_name_list[instrument_number]
	//metronome
	let metronome_number=voice_data.metro
	let class_metro=""
	let metro_text=""
	if(metronome_number==0){
		metro_text="M"
		class_metro='App_RE_text_strikethrough'
	}else{
		metro_text="M"+metronome_number
	}
	//e_sound
	let e_sound=voice_data.e_sound
	let e_sound_text="e"
	if(e_sound==22){
		e_sound_text="e"
	}else{
		e_sound_text="e"+e_sound
	}
	//Polipulse TO BE IMPLEMENTED index 7 .polipulse
	//neopulse
	let D_NP = voice_data.neopulse.D
	let N_NP = voice_data.neopulse.N
	let np_string = N_NP+"/"+D_NP
	//voice and labels visibility
	//1+12 labels
	let class_show_button=""
	let class_voice_minimized=""
	let class_white_space_voice_end=""

	let display_header_button="block"
	let NP_disabled=""
	let display_header_vis_button="block"
	let display_header_R_minimized_button="none"
	let lines_needed=0
	let string_voice_labels=""
	if (voice_data.RE_visible){
		class_show_button="show"
		class_white_space_voice_end="hidden"
		string_voice_labels='<label class="App_RE_segment_label">sg</label>'
		let extra_lines_A=0
		labels_needed.forEach(item=>{
			switch (item) {
				case 'Aa':
				case 'Am':
				case 'Ag':
					extra_lines_A=1
					let A_string=(item=="Ag")?"iAº":("i"+item)
					string_voice_labels+=`<label class="hoverLabel boxHover App_RE_label App_RE_A_l">${A_string+"+"}</label>`
					string_voice_labels+=`<label class="hoverLabel boxHover App_RE_label App_RE_A_l">${A_string+"-"}</label>`
				break;
				case 'Na':
					string_voice_labels+='<label class="hoverLabel boxHover App_RE_label App_RE_Na_l" onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)" data-value="NaHover" >Na</label>'
				break;
				case 'Nm':
					string_voice_labels+='<label class="hoverLabel boxHover App_RE_label App_RE_Nm_l" onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)" data-value="NHover">Nm</label>'
				break;
				case 'Ngm':
					string_voice_labels+='<label class="hoverLabel App_RE_label App_RE_Ngm_l" onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)" data-value="NºHover">N°</label>'
				break;
				case 'iNa':
					string_voice_labels+='<label class="hoverLabel boxHover App_RE_label App_RE_iNa_l" onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)" data-value="iSaHover">iSa</label>'
				break;
				case 'iNm':
					string_voice_labels+='<label class="hoverLabel boxHover App_RE_label App_RE_iNm_l" onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)" data-value="iSHover">iSm</label>'
				break;
				case 'iNgm':
					string_voice_labels+='<label class="hoverLabel App_RE_label App_RE_iNgm_l" onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)" data-value="iSºHover">iS°</label>'
				break;
				case 'Nabc':
					string_voice_labels+='<label class="hoverLabel App_RE_label App_RE_Nabc_l" onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)" data-value="NabcHover">#/♭</label>'
				break;
				case 'Ta':
					string_voice_labels+='<label class="hoverLabel boxHover App_RE_label App_RE_Ta_l" onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)" data-value="PaHover">Pa</label>'
				break;
				case 'Tm':
					string_voice_labels+='<label class="hoverLabel boxHover App_RE_label App_RE_Tm_l" onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)" data-value="PmHover">Pm</label>'
				break;
				case 'Ts':
					string_voice_labels+='<label class="hoverLabel boxHover App_RE_label App_RE_Ts_l" onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)" data-value="PsHover">Psg</label>'
				break;
				case 'iT':
					string_voice_labels+='<label class="hoverLabel boxHover App_RE_label App_RE_iT_l" onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)" data-value="iTHover">iTa</label>'
				break;
				case 'Tf':
					string_voice_labels+='<label class="hoverLabel boxHover App_RE_label App_RE_Tf_l" onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)" data-value="frTHover">Fr</label>'
				break;
				default:
					console.error(`Line ${item} not found.`);
				break;
			}
		})
		lines_needed=labels_needed.length+2+extra_lines_A
	} else {
		//close head
		lines_needed=2
		class_voice_minimized="minimized"
		display_header_button="none"
		display_header_vis_button="none"
		NP_disabled="disabled"
		display_header_R_minimized_button="block"
	}
	let height_header= "calc(var(--RE_block) * "+(lines_needed)+")"
	let A_disabled=(voice_data.A)?"disabled":""
	let A_provv_extra_buttons="" //XXX provv , recolocate them
	if(voice_data.A){
		A_provv_extra_buttons=`<button class="App_RE_selected_voice_A_explode" style="display: ${display_header_button}" onclick="DATA_voice_explode_A(${voice_data.voice_id})">
										<img src="./Icons/bomb.svg">
									</button>
									<button class="App_RE_selected_voice_delete_A" style="display: ${display_header_button}" onclick="DATA_voice_delete_A(${voice_data.voice_id})">
										<img class="App_RE_icon" src="./Icons/delete.svg">
									</button>`
	}
	voice_string+=`
						<div class="App_RE_voice ${class_blocked_voice} ${class_voice_minimized}" value="${voice_data.voice_id}"
							ondragstart="RE_draggable_voice_dragstart(this,event)" ondragend="RE_draggable_voice_dragend(this)">
							<div class="App_RE_voice_header">
								<div class="App_RE_voice_header_body" style="height:${height_header};">
									<div class="App_RE_voice_name_container">
										<label class="App_RE_voice_number">${seq_number}</label>
										<input class="App_RE_voice_name" placeholder="Voice" maxlength="8" onchange="RE_change_voice_name(${voice_data.voice_id},this)" value="${voice_data.name}"></input>
									</div>
									<button class="App_RE_voice_instrument" style="display:${display_header_button};" onclick="RE_change_voice_instrument(${voice_data.voice_id},this)"
										value="${instrument_number}" onmouseenter="APP_hover_area_enter(this)"
										onmouseleave="APP_hover_area_exit(this)" data-value="InstrumentHover">
										${inst_name}
									</button>
									<div class="App_RE_selected_voice_A_container">
										<button class="App_RE_selected_voice_add_A" onclick="DATA_voice_add_A(${voice_data.voice_id})" ${A_disabled}>A</button>
										${A_provv_extra_buttons}
									</div>
									<div class="App_RE_voice_header_container_1">
										<button class="App_RE_voice_mute ${class_mute}" onClick="RE_mute_unmute_voice(${voice_data.voice_id},this)"
											value="${volume}" onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)"
											data-value="muteHover">M</button>
										<button class="App_RE_voice_solo ${class_solo} " onClick="RE_change_solo_voice(${voice_data.voice_id},this)"
											onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)"
											data-value="SoloHover">S</button>
									</div>
									<div class="App_RE_voice_header_container_2">
										<button class="App_RE_voice_block ${class_blocked_button}" onClick="RE_block_unblock_voice(${voice_data.voice_id},this)">
											<img class="App_RE_icon" onmouseenter="APP_hover_area_enter(this)"
												onmouseleave="APP_hover_area_exit(this)" data-value="SyncVoiceHover"
												src="./Icons/changes_prevent.svg" style="display: ${display_blocked_icon_0};" />
											<img class="App_RE_icon" onmouseenter="APP_hover_area_enter(this)"
												onmouseleave="APP_hover_area_exit(this)" data-value="AsyncVoiceHover"
												src="./Icons/changes_allow.svg" style="display: ${display_blocked_icon_1};" />
										</button>
										<input class="App_RE_neopulse_inp_box" ${NP_disabled} onkeypress="APP_inpTest_NP(event,this)"
											onfocusin="APP_set_previous_value(this)" onfocusout="RE_enter_new_NP(this)"
											maxlength="3" value="${np_string}" onmouseenter="APP_hover_area_enter(this)"
											onmouseleave="APP_hover_area_exit(this)" data-value="NeopulseHover"></input>
										<button class="App_RE_metro_sound ${class_metro}"
											onclick="RE_change_metro_sound(${voice_data.voice_id},${metronome_number},this)"
											onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)"
											data-value="MetronomeVoiceHover">${metro_text}</button>
										<button class="App_RE_pulse_sound" onclick="RE_show_pulse_sound_selector(${voice_data.voice_id},${e_sound})">
											${e_sound_text}</button>
									</div>
								</div>
								<div class="App_RE_voice_header_bar" onclick="RE_toggle_selection_voice(this)">
									<button class="App_RE_voice_menu_button show" style="display: ${display_header_button}">
										<img class="App_RE_voice_menu_button_img" src="./Icons/menu.svg">
									</button>
									<div class="App_RE_voice_dropdown_menu">
										<a onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)" onclick="RE_add_voice_button(this)"><img class="App_RE_icon" src="./Icons/op_menu_add.svg"></a>
										<!--a onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)" onclick="RE_clone_voice_button(this)">Clone</a-->
										<a onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)" onclick="RE_clear_voice_button(this)"><img class="App_RE_icon" src="./Icons/op_menu_clear.svg"></a>
										<a class="App_RE_delete_voice ${class_delete_voice_button}" onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)" onclick="RE_delete_voice_button(this)"><img class="App_RE_icon" src="./Icons/op_menu_delete.svg"></a>
										<a onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)" onclick="RE_repeat_voice_button(this,event)"><img class="App_RE_icon" src="./Icons/op_menu_repeat_up.svg"></a>
										<a onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)" onclick="RE_operations_voice_button(this,event)"><img class="App_RE_icon" src="./Icons/op_menu_operations.svg"></a>
										<a ></a>
									</div>
									<button class="App_RE_voice_visible ${class_show_button}" onclick="RE_show_hide_voice_button(this)" style="display: ${display_header_vis_button}" value="0">◂</button>
									<button class="App_RE_voice_header_bar_minimized_button" onclick="RE_minimized_voice_header_button(this)" style="display: ${display_header_R_minimized_button};" value="1">‣</button>
								</div>
							</div>
							<div class="App_RE_voice_selector_bg"></div>
							<div class="App_RE_voice_draggable" draggable="true" onclick="RE_toggle_selection_voice(this)">
								<p>=</p>
							</div>
							<div class="App_RE_voice_labels">
								${string_voice_labels}
							</div>
							<div class="App_RE_voice_data" onscroll="RE_voice_scrolling(this,event)" ondragover="RE_segment_container_dragover(this,event)">
								<div class="App_RE_segment_spacer">
									<svg data-name="Trazado 10" viewBox="0 0 28 28" preserveAspectRatio="none">
										<line x1="14" y1="0" x2="14" y2="28" style="stroke-width:2"></line>
									</svg>
									<div class="App_RE_segment_spacer_buttons">
										<button class="App_RE_add_segment" onmouseenter="APP_hover_area_enter(this)"
											onmouseleave="APP_hover_area_exit(this)" data-value="AddSegHover"
											onclick="RE_add_segment_button(this)">
											<img src="./Icons/new-segment.svg" />
										</button>
										<button class="App_RE_merge_segment" onmouseenter="APP_hover_area_enter(this)"
											onmouseleave="APP_hover_area_exit(this)" data-value="MergeSegHover"
											onclick="RE_merge_segment_button(this)" style="display:none">
											<img src="./Icons/merge-segment.svg" />
										</button>
									</div>
								</div>
			`
	//Voices values
	let segment_list = voice_data.data.segment_data
	let Psg_=0
	let delete_segment_button=[true]
	let merge_segment_button=[]//all except first
	segment_list.forEach((segment_data,segment_index)=>{
		merge_segment_button.push(true)
		delete_segment_button.push(true)
	})
	if(segment_list.length==1)delete_segment_button[0]=false
	merge_segment_button[segment_list.length-1]=false
	//let container_width=(document.querySelector(".App_Working_Space").clientWidth/nuzic_block)-7//196
	let container_width=document.querySelector(".App_Working_Space").clientWidth-(nuzic_block*7)//196
	let X_start=current_position_PMCRE.RE.X_start
	let X_end=X_start+container_width
	//add segment and segment spacer
	if (voice_data.RE_visible){//doesn' draw if not visible
		segment_list.forEach((segment_data,segment_index)=>{
			//n_column
			let Psg_segment_start = Psg_
			let Psg_segment_end = Psg_segment_start + segment_data.time.slice(-1)[0].P
			Psg_=Psg_segment_end
			//calculate times
			let time_segment_start= DATA_calculate_exact_time(Psg_segment_start,0,0,1,1,N_NP,D_NP)
			let time_segment_end= DATA_calculate_exact_time(Psg_segment_end,0,0,1,1,N_NP,D_NP)
			let index_col_start = RE_find_time_column_index(time_segment_start)
			let index_col_end = RE_find_time_column_index(time_segment_end)
			let n_column = (index_col_end-index_col_start)-1
			//segment name
			let display_name = (n_column>5)? true:false
			let seg_name_text=segment_data.segment_name
			let class_seg_name=""
			if(seg_name_text=="" || !display_name){
				class_seg_name="hidden"
			}
			let display_seg_delete=delete_segment_button[segment_index]
			let display_seg_merge="inline"
			if(merge_segment_button[segment_index]==false){
				display_seg_merge="none"
			}
			let draw_lines=false
			//calculate focus column ??? XXX
			//calculate segment visibility
			let left=(index_col_start+1)*nuzic_block
			let right=(left+n_column*nuzic_block)
			if((left>=X_start && left<X_end)||
				(right>X_start && right<=X_end)||
				(left<X_start && right>X_end)){
				draw_lines=true
			}
			voice_string+=RE_calculate_segment_string(Psg_segment_start,n_column,segment_index,voice_data.voice_id,class_seg_name,seg_name_text,display_seg_delete,draw_lines,segment_data,RE_grades_data,N_NP,D_NP,labels_needed,true)
			voice_string+=`<div class="App_RE_segment_spacer">
						<svg data-name="Trazado 10" viewBox="0 0 28 28" preserveAspectRatio="none">
							<line x1="14" y1="0" x2="14" y2="28" style="stroke-width:2"></line>
						</svg>
						<div class="App_RE_segment_spacer_buttons">
							<button class="App_RE_add_segment" onmouseenter="APP_hover_area_enter(this)"
								onmouseleave="APP_hover_area_exit(this)" data-value="AddSegHover"
								onclick="RE_add_segment_button(this)">
								<img src="./Icons/new-segment.svg" />
							</button>
							<button class="App_RE_merge_segment" onmouseenter="APP_hover_area_enter(this)"
								onmouseleave="APP_hover_area_exit(this)" data-value="MergeSegHover"
								onclick="RE_merge_segment_button(this)" style="display:${display_seg_merge}">
								<img src="./Icons/merge-segment.svg" />
							</button>
						</div>
					</div>`
		})
	}
	voice_string+=`
					<div class="App_RE_segment_spacer_end" style="min-width:calc(var(--RE_block) * ${n_end_space})">
					</div>
					</div>
				<div class="App_RE_white_space_voice_end ${class_white_space_voice_end}"></div>
		</div>
	`
	return voice_string
}

function RE_calculate_iA_list(type,data_up,data_down,RE_grades_data){
	//use RE_global_time_segment_change for allignment
	let time_list_voice_up = RE_global_time_voices_changes.find(time=>{return time.voice_id==data_up.voice_id}).time
	let time_list_voice_down = RE_global_time_voices_changes.find(time=>{return time.voice_id==data_down.voice_id}).time
	//make a list of all note up and down
	let Na_up_list =  []
	let Na_down_list = []
	let diesis_up_list=[]
	let diesis_down_list=[]
	let scale_data_up_list = []
	let scale_data_down_list = []
	let time_segment_change_up = []
	let time_segment_change_down = []
	//prepare a list of scale note up scale note down
	let N_NP_up = data_up.neopulse.N
	let N_NP_down = data_down.neopulse.N
	let D_NP_up = data_up.neopulse.D
	let D_NP_down = data_down.neopulse.D
	//scale_data_up_list
	let Psg_segment_start_up=0
	let index_end_segment_up = 0
	data_up.data.segment_data.forEach(segment=>{
		let last_index=segment.time.length-1
		segment.time.forEach((time,index)=>{
			if(index!=last_index){
				Na_up_list.push(segment.sound[index].note)
				diesis_up_list.push(segment.sound[index].diesis)
				if(type!="Ag"){
					scale_data_up_list.push(null)
					return
				}
				//find fraction
				let fraction = segment.fraction.find(fraction=>{return fraction.stop>time.P})
				//if (typeof(fraction) == "undefined")fraction = null //no need , end seg not processed
				let frac_p = fraction.N/fraction.D
				let Pa_equivalent = (Psg_segment_start_up+time.P) * N_NP_up/D_NP_up + time.F* frac_p * N_NP_up/D_NP_up
				let current_scale_index=DATA_get_Pa_eq_grade_data_index(Pa_equivalent,RE_grades_data)
				//element inside a scale module
				// let scale_end_p=0
				// let index=-1
				// current_scale = scale_sequence.find(scale=>{
				// 	index++
				// 	scale_end_p += parseInt(scale.duration)
				// 	return scale_end_p > Pa_equivalent
				// })
				// current_scale.scale_index=index
				scale_data_up_list.push(current_scale_index)
			}else{
				index_end_segment_up+= last_index
				time_segment_change_up.push(time_list_voice_up[index_end_segment_up])
			}
		})
		Psg_segment_start_up+=segment.time.slice(-1)[0].P
	})
	//scale_data_down_list
	let Psg_segment_start_down=0
	let index_end_segment_down = 0
	data_down.data.segment_data.forEach(segment=>{
		let last_index=segment.time.length-1
		segment.time.forEach((time,index)=>{
			if(index!=last_index){
				diesis_down_list.push(segment.sound[index].diesis)
				Na_down_list.push(segment.sound[index].note)
				if(type!="Ag"){
					scale_data_down_list.push(null)
					return
				}
				let fraction = segment.fraction.find(fraction=>{return fraction.stop>time.P})
				//if (typeof(fraction) == "undefined")fraction = null //no need , end seg not processed
				let frac_p = fraction.N/fraction.D
				let Pa_equivalent = (Psg_segment_start_down+time.P) * N_NP_down/D_NP_down + time.F* frac_p * N_NP_down/D_NP_down
				//element inside a scale module
				let current_scale_index=DATA_get_Pa_eq_grade_data_index(Pa_equivalent,RE_grades_data)
				scale_data_down_list.push(current_scale_index)
			}else{
				index_end_segment_down+= last_index
				time_segment_change_down.push(time_list_voice_down[index_end_segment_down])
			}
		})
		Psg_segment_start_down+=segment.time.slice(-1)[0].P
	})
	//var abs_position_all=0
	let abs_position_up=0
	let abs_position_down=0
	let pos_up=abs_position_up
	let pos_down=abs_position_down
	let prev_Na_up = null
	let prev_Na_down = null
	let prev_diesis_up = null
	let prev_diesis_down = null
	let prev_scale_up_index=null
	let prev_scale_down_index=null
	let time_segment_change= JSON.parse(JSON.stringify(RE_global_time_segment_change))
	//no need first or last element (0 and Li)
	let end_time = time_segment_change.slice(-1)[0]
	time_segment_change.shift()
	let abs_position_seg_change=0
	let time_all_changes = RE_global_time_all_changes.filter(time =>time<=end_time) //XXX
	let iA_object_list= []
	time_all_changes.forEach((time,index)=>{
		if(time==time_segment_change[abs_position_seg_change]){
			abs_position_seg_change++;
			//insert 2 values
			let seg_change_down = false
			let seg_change_up = false
			let Na_up = null
			let Na_down = null
			let diesis_up = null
			let diesis_down = null
			let scale_up_index=null
			let scale_down_index=null
			if(time==time_list_voice_up[abs_position_up]){
				Na_up =Na_up_list[abs_position_up]
				diesis_up = diesis_up_list[abs_position_up]
				scale_up_index = scale_data_up_list[abs_position_up]
				pos_up=abs_position_up
				abs_position_up++
				seg_change_up=time_segment_change_up.some(item=>{return item==time})
			}
			if(seg_change_up){
				if(Na_up==-3){
					//error
					prev_Na_up=null
					prev_diesis_up=null
					prev_scale_up_index=null
				}else{
					prev_Na_up=Na_up
					prev_diesis_up=diesis_up
					prev_scale_up_index=scale_up_index
				}
			}
			if(time==time_list_voice_down[abs_position_down]){
				Na_down =Na_down_list[abs_position_down]
				diesis_down = diesis_down_list[abs_position_down]
				scale_down_index = scale_data_down_list[abs_position_down]
				pos_down=abs_position_down
				abs_position_down++
				seg_change_down=time_segment_change_down.some(item=>{return item==time})
			}
			if(seg_change_down){
				if(Na_down==-3){
					//error
					prev_Na_down=null
					prev_diesis_down=null
					prev_scale_down_index=null
				}else{
					prev_Na_down=Na_down
					prev_diesis_down=diesis_down
					prev_scale_down_index=scale_down_index
				}
			}
			if(seg_change_down && seg_change_up){
				iA_object_list.push({void:"last"})
				//not possible Na=null
				//diesis_up:diesis_up,diesis_down:diesis_down, //XXX
				iA_object_list.push({void:false,disabled:false,iA_data:{Na_up:Na_up, Na_down:Na_down,diesis_up:diesis_up,diesis_down:diesis_down,abs_position_up:pos_up,abs_position_down:pos_down,scale_up_index:scale_up_index,scale_down_index:scale_down_index}})
				return
			}
			if(seg_change_down){
				iA_object_list.push({void:"last"})
				if(Na_up==null){
					Na_up=prev_Na_up
					if(Na_up==null){
						console.error("Error iA: prev Na not found")
						iA_object_list.push({void:"error"})
						return
					}
					diesis_up=prev_diesis_up
					scale_up_index=prev_scale_up_index
					iA_object_list.push({void:false,disabled:true,iA_data:{Na_up:Na_up, Na_down:Na_down,diesis_up:diesis_up,diesis_down:diesis_down,abs_position_up:pos_up,abs_position_down:pos_down,scale_up_index:scale_up_index,scale_down_index:scale_down_index}})
					return
				}else{
					if(Na_up==-3){
						Na_up=prev_Na_up
						diesis_up=prev_diesis_up
						scale_up_index=prev_scale_up_index
					}else{
						prev_Na_up=Na_up
						prev_diesis_up=diesis_up
						prev_scale_up_index=scale_up_index
					}
					if(Na_up==null){
						console.error("Error iA: prev Na not found")
						iA_object_list.push({void:"error"})
						return
					}
					iA_object_list.push({void:false,disabled:false,iA_data:{Na_up:Na_up, Na_down:Na_down,diesis_up:diesis_up,diesis_down:diesis_down,abs_position_up:pos_up,abs_position_down:pos_down,scale_up_index:scale_up_index,scale_down_index:scale_down_index}})
					return
				}
			}
			if(seg_change_up){
				iA_object_list.push({void:"last"})
				if(Na_down==null){
					Na_down=prev_Na_down
					if(Na_down==null){
						console.error("Error iA: prev Na not found")
						iA_object_list.push({void:"error"})
						return
					}
					diesis_down=prev_diesis_down
					scale_down_index=prev_scale_down_index
					iA_object_list.push({void:false,disabled:false,iA_data:{Na_up:Na_up, Na_down:Na_down,diesis_up:diesis_up,diesis_down:diesis_down,abs_position_up:pos_up,abs_position_down:pos_down,scale_up_index:scale_up_index,scale_down_index:scale_down_index}})
					return
				}else{
					if(Na_down==-3){
						Na_down=prev_Na_down
						diesis_down=prev_diesis_down
						scale_down_index=prev_scale_down_index
					}else{
						prev_Na_down=Na_down
						prev_diesis_down=diesis_down
						prev_scale_down_index=scale_down_index
					}
					if(Na_down==null){
						console.error("Error iA: prev Na not found")
						iA_object_list.push({void:"error"})
						return
					}
					iA_object_list.push({void:false,disabled:false,iA_data:{Na_up:Na_up, Na_down:Na_down,diesis_up:diesis_up,diesis_down:diesis_down,abs_position_up:pos_up,abs_position_down:pos_down,scale_up_index:scale_up_index,scale_down_index:scale_down_index}})
					return
				}
			}
			//case segment change elsewere
			if(Na_up==null && Na_down==null){
				//no value
				iA_object_list.push({void:true})
				iA_object_list.push({void:true})
				return
			}
			iA_object_list.push({void:true})
			if(Na_down==null){
				//deactivated
				Na_down=prev_Na_down
				if(Na_down==null){
					console.error("Error iA: prev Na not found")
					iA_object_list.push({void:"error"})
					return
				}
				diesis_down=prev_diesis_down
				scale_down_index=prev_scale_down_index
				iA_object_list.push({void:false,disabled:false,iA_data:{Na_up:Na_up, Na_down:Na_down,diesis_up:diesis_up,diesis_down:diesis_down,abs_position_up:pos_up,abs_position_down:pos_down,scale_up_index:scale_up_index,scale_down_index:scale_down_index}})
				return
			}
			let is_disabled=false
			if(Na_up==null){
				Na_up=prev_Na_up
				diesis_up=prev_diesis_up
				scale_up_index=prev_scale_up_index
				is_disabled=true
			}
			if(Na_up==null){
				console.error("iA calc : previous value Na UP null ")
				iA_object_list.push({void:"error"})
				return
			}
			iA_object_list.push({void:false,disabled:is_disabled,iA_data:{Na_up:Na_up, Na_down:Na_down,diesis_up:diesis_up,diesis_down:diesis_down,abs_position_up:pos_up,abs_position_down:pos_down,scale_up_index:scale_up_index,scale_down_index:scale_down_index}})
		}else{
			//insert 1 value
			let Na_up = null
			let Na_down = null
			let diesis_up = null
			let diesis_down = null
			let scale_up_index=null
			let scale_down_index=null
			if(time==time_list_voice_up[abs_position_up]){
				Na_up =Na_up_list[abs_position_up]
				diesis_up = diesis_up_list[abs_position_up]
				scale_up_index = scale_data_up_list[abs_position_up]
				if(Na_up==-3){
					Na_up=prev_Na_up
					diesis_up=prev_diesis_up
					scale_up_index=prev_scale_up_index
				}else{
					prev_Na_up=Na_up
					prev_diesis_up=diesis_up
					prev_scale_up_index=scale_up_index
				}
				pos_up=abs_position_up
				abs_position_up++
			}
			if(time==time_list_voice_down[abs_position_down]){
				Na_down =Na_down_list[abs_position_down]
				diesis_down = diesis_down_list[abs_position_down]
				scale_down_index = scale_data_down_list[abs_position_down]
				if(Na_down==-3){
					Na_down=prev_Na_down
					diesis_down=prev_diesis_down
					scale_down_index=prev_scale_down_index
				}else{
					prev_Na_down=Na_down
					prev_diesis_down=diesis_down
					prev_scale_down=scale_down_index
				}
				pos_down=abs_position_down
				abs_position_down++
			}
			if(Na_up==null && Na_down==null){
				//no value
				iA_object_list.push({void:true})
				return
			}
			if(Na_down==null){
				//deactivated
				Na_down=prev_Na_down
				if(Na_down==null){
					console.error("Error iA: prev Na not found")
					iA_object_list.push({void:"error"})
					return
				}
				diesis_down=prev_diesis_down
				scale_down_index=prev_scale_down_index
				iA_object_list.push({void:false,disabled:false,iA_data:{Na_up:Na_up, Na_down:Na_down,diesis_up:diesis_up,diesis_down:diesis_down,abs_position_up:pos_up,abs_position_down:pos_down,scale_up_index:scale_up_index,scale_down_index:scale_down_index}})
				return
			}
			let is_disabled=false
			if(Na_up==null){
				Na_up=prev_Na_up
				diesis_up=prev_diesis_up
				scale_up_index=prev_scale_up_index
				is_disabled=true
			}
			if(Na_up==null){
				console.error("iA calc : previous value Na UP null ")
				iA_object_list.push({void:"error"})
				return
			}
			iA_object_list.push({void:false,disabled:is_disabled,iA_data:{Na_up:Na_up, Na_down:Na_down,diesis_up:diesis_up,diesis_down:diesis_down,abs_position_up:pos_up,abs_position_down:pos_down,scale_up_index:scale_up_index,scale_down_index:scale_down_index}})
		}
	})
	//last one is undefined??
	iA_object_list.pop()
	//define scale DATA
	iA_object_list.forEach((item,index)=>{
		if(!item.disabled && !item.void){
			//add voice index and scale if necessary
			//item.iA_data.push({voice_id_up:data_up.voice_id})
			item.iA_data.voice_id_up=data_up.voice_id
			item.iA_data.voice_id_down=data_down.voice_id
		}
	})
	let iA_size_list=[0]
	//better object list
	let iA_info_list=[]
	iA_object_list.forEach(current_obj=>{
		if(current_obj.void==true){
			iA_size_list.push(iA_size_list.pop()+2)
		}
		if(current_obj.void=="error"){
			iA_size_list.push(iA_size_list.pop()+2)
		}
		if(current_obj.void=="last"){
			iA_size_list.push(iA_size_list.pop()+2)
			iA_size_list.push(0)
		}
		if(current_obj.void==false){
			if(iA_size_list.slice(-1)[0]==0){
				iA_size_list.pop()
			}
			iA_size_list.push(2)
			iA_info_list.push(current_obj)
		}
	})
	iA_size_list.pop()
	iA_size_list.push(iA_size_list.pop()-1)
	return [iA_size_list,iA_info_list]
}

function RE_calculate_iA_element_string(delta,type,disabled,iA_data,RE_grades_data){
	//item disabled if not possible indipendent change
	//give correct in function and callback
	let disabled_text=(disabled)?"disabled":""
	let copy_type=type
	if(delta=="no_value")type=delta
	let string=""
	let up_info,down_info
	switch (type) {
	case "Aa":
		if(disabled){
			string=`<label class="App_RE_iA_cell App_RE_iA_cell_pink" style="min-width:var(--RE_block)">${delta}</label>`
		}else{
			let rg_length = 3
			if(TET*N_reg>99)rg_length=4
			up_info = JSON.stringify({abs_position:iA_data.abs_position_up,voice_id:iA_data.voice_id_up,nzc_note:iA_data.Na_up,diesis:iA_data.diesis_up})
			down_info = JSON.stringify({abs_position:iA_data.abs_position_down,voice_id:iA_data.voice_id_down,nzc_note:iA_data.Na_down,diesis:iA_data.diesis_down})
			string=`<input class="App_RE_iA_cell" ondragstart="RE_disableEvent(event)" ondblclick="RE_dbclick_inp_box(this)"
				onkeypress="APP_inpTest_iAa(event,this)" onkeydown="RE_move_focus_iA(event,this)"
				onfocusin="APP_set_previous_value(this);APP_play_input_on_click(this,'iA');RE_replace_inp_box_superscript(this)" onfocusout="RE_enter_new_value_iA(this,'iAa')"
				up_info='${up_info}' down_info='${down_info}' placeholder=" " maxlength="${rg_length}" value="${(delta!=null)?delta:' '}" ${disabled_text}>`
		}
	break;
	case "Am":
		if(disabled){
			string=`<label class="App_RE_iA_cell App_RE_iA_cell_pink" style="min-width:var(--RE_block)">${delta}</label>`
		}else{
			up_info = JSON.stringify({abs_position:iA_data.abs_position_up,voice_id:iA_data.voice_id_up,nzc_note:iA_data.Na_up,diesis:iA_data.diesis_up})
			down_info = JSON.stringify({abs_position:iA_data.abs_position_down,voice_id:iA_data.voice_id_down,nzc_note:iA_data.Na_down,diesis:iA_data.diesis_down})
			let stringiA = ""
			if(delta!=null){
				let mod = TET
				let reg = Math.floor(delta/mod)
				let resto = delta%mod
				let neg = false
				// distance modular
				//verify if is negative
				if(delta<0){
					neg = true
					resto = Math.abs(resto)
					if(resto!=0){
						reg= Math.abs(reg)-1
					}else{
						reg= Math.abs(reg)
					}
				}
				if(neg){
					stringiA="-"+resto//+"r"+reg
				}else{
					stringiA=resto//+"r"+reg
				}
				if(reg!=0)stringiA=stringiA+"r"+reg
			}
			string=`<input class="App_RE_iA_cell" ondragstart="RE_disableEvent(event)" onfocusin="APP_set_previous_value(this);APP_play_input_on_click(this,'iA');RE_replace_inp_box_superscript(this)"
				ondblclick="RE_dbclick_inp_box(this)" oninput="RE_resize_inp_box(this)" onkeypress="APP_inpTest_iAm(event,this)" onfocusout="RE_enter_new_value_iA(this,'iAm')"
				onkeydown="RE_move_focus_iA(event,this)" value="${stringiA}"
				up_info='${up_info}' down_info='${down_info}' placeholder=" " maxlength="5" ${disabled_text}>`
		}
	break;
	case "Ag":
		let Ag_disabled=false
		let str_value=""
		// if(iA_data==null){
		// 	str_value = "Err"
		// 	Ag_disabled=true
		// 	break
		// }else{
		if(iA_data.scale_up_index==null){
			str_value = ""
			Ag_disabled=true
		}else{
			if(iA_data.scale_down_index==null){
				str_value = ""
				Ag_disabled=true
			}else{
				if(iA_data.scale_down_index==iA_data.scale_up_index){
					//calculate note interval and make a traduction in current scale
					if(delta==null){
						str_value = ""
					}else{
						//str_value = DATA_calculate_scale_interval_string(iA_data.Na_down,iA_data.diesis_down,iA_data.scale_down,iA_data.Na_up,iA_data.diesis_up,iA_data.scale_up,idea_scale_list)
						str_value = DATA_calculate_grade_interval_string(iA_data.Na_down,iA_data.diesis_down,RE_grades_data[iA_data.scale_down_index],iA_data.Na_up,iA_data.diesis_up,RE_grades_data[iA_data.scale_up_index])
					}
				}else{
					str_value = no_value_iA
					Ag_disabled=true
				}
			}
		}
		if(Ag_disabled || disabled){
			string=`<label class="App_RE_iA_cell App_RE_iA_cell_pink" style="min-width:var(--RE_block)">${str_value}</label>`
		}else{
			up_info = JSON.stringify({abs_position:iA_data.abs_position_up,voice_id:iA_data.voice_id_up,nzc_note:iA_data.Na_up,diesis:iA_data.diesis_up})
			down_info = JSON.stringify({abs_position:iA_data.abs_position_down,voice_id:iA_data.voice_id_down,nzc_note:iA_data.Na_down,diesis:iA_data.diesis_down})
			string=`<input class="App_RE_iA_cell" ondragstart="RE_disableEvent(event)" onfocusin="APP_set_previous_value(this);APP_play_input_on_click(this,'iA');RE_replace_inp_box_superscript(this)"
				ondblclick="RE_dbclick_inp_box(this)" oninput="RE_resize_inp_box(this)" onkeydown="RE_move_focus_iA(event,this)"
				onkeypress="APP_inpTest_iAgm(event,this)" onfocusout="RE_enter_new_value_iA(this,'iAgm')" value="${str_value}"
				up_info='${up_info}' down_info='${down_info}' placeholder=" " maxlength="4">`
		}
	break;
	}
	return string
}

function RE_calculate_segment_string(Psg_segment_start,n_column,segment_index,voice_id,class_seg_name,seg_name_text,display_seg_delete,draw_lines,segment_data,RE_grades_data,N_NP,D_NP,labels_needed,outer){
	let class_loading="loading"
	let segment_lines_string=""
	if(draw_lines){
		//try drawing it
		class_loading=""
		segment_lines_string=RE_calculate_line_strings(voice_id,segment_index,segment_data,RE_grades_data,Psg_segment_start,N_NP,D_NP,labels_needed,n_column)
	}
	let segment_string=``
	if(outer){
		segment_string=`<div class="App_RE_segment" starting="${Psg_segment_start}" n_column="${n_column}" delete="${display_seg_delete}" style="width: calc(var(--RE_block)* ${n_column}); min-width: calc(var(--RE_block)* ${n_column});"
						onmouseenter="SNAV_enter_on_segment_parameters(this)" ondragstart="RE_draggable_segment_dragstart(this,event)" ondragend="RE_draggable_segment_dragend(this,event)">`
	}
	let class_display_seg_delete=(display_seg_delete)?"App_RE_delete_segment_show":"App_RE_delete_segment_hide"
	segment_string+=`<div class="App_RE_segment_loader ${class_loading}">Loading
						</div>
						<div class="App_RE_segment_draggable" draggable="true" onclick="RE_toggle_selection_segment(this)">
							<div class="App_RE_dropdown_segment">
								<button class="App_RE_dropdown_segment_button">
									<img class="App_RE_icon" src="./Icons/menu.svg" />
								</button>
								<div class="App_RE_dropdown_segment_content">
									<a onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)"
										data-value="NameSegHover" onclick="RE_change_segment_name_button(this)"
										href="#"><img class="App_RE_icon" src="./Icons/op_menu_rename.svg"></a>
									<a onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)"
										data-value="ClearSegHover" onclick="RE_clear_segment_button(this)"
										href="#"><img class="App_RE_icon" src="./Icons/op_menu_clear.svg"></a>
									<a onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)"
										data-value="PasteSegHover" onclick="RE_repeat_segment_button(this,event)"
										href="#"><img class="App_RE_icon" src="./Icons/op_menu_repeat_right.svg"></a>
									<a onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)"
										data-value="CopySegHover" onclick="RE_copy_segment_button(this)"
										href="#"><img class="App_RE_icon" src="./Icons/op_menu_copy.svg"></a>
									<a onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)"
										data-value="PasteSegHover" onclick="RE_paste_segment_button(this)"
										href="#"><img class="App_RE_icon" src="./Icons/op_menu_paste.svg"></a>
									<a onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)"
										data-value="PasteSegHover" onclick="RE_operations_segment_button(this,event)"
										href="#"><img class="App_RE_icon" src="./Icons/op_menu_operations.svg"></a>
								</div>
							</div>
							<div class="App_RE_segment_number">
								${segment_index}
							</div>
							<input class="App_RE_segment_name ${class_seg_name}" onchange="RE_change_segment_name(this)" onfocusout="RE_hide_segment_name(this)" value="${seg_name_text}" maxlength="8" placeholder="">
							</input>
							<button class="App_RE_delete_segment ${class_display_seg_delete}" onmouseenter="APP_hover_area_enter(this)"
								onmouseleave="APP_hover_area_exit(this)" data-value="DeleteSegHover"
								onclick="RE_delete_segment_button(this)">
								<img class="App_RE_icon" src="./Icons/delete.svg" />
							</button>
						</div>
						<div class="App_RE_segment_draggable App_RE_segment_draggable_bottom" onmouseleave="RE_exit_break_segment(this)" onmousemove="RE_break_segment_calculate_position(this,event)" draggable="true">
							<button class="App_RE_break_segment" onclick="RE_break_segment_button(this)" value="0">
								<img src="./Icons/bolt_blue.svg" />
							</button>
						</div>
						${segment_lines_string}`
	if(outer){
		segment_string+=`</div>`
	}
	return segment_string
}

function RE_verify_segment_visibility(){
	//trigger when change in scrollbar, windows resizing, voice visibility (loading data???) PMCtoRE
	if(!current_position_PMCRE.RE.scrollbar)return
	//list voices
	let voices_container = document.querySelector(".App_RE_voice_matrix")
	let voice_obj_list = [...voices_container.querySelectorAll(".App_RE_voice")]
	voice_obj_list.forEach((voice_data_obj,voice_number)=>{
		let voice_id = RE_read_voice_id(voice_data_obj)[1]
		let segment_obj_list = [...voice_data_obj.querySelectorAll(".App_RE_segment")]
		segment_obj_list.forEach((segment_obj,segment_index)=>{
			if(RE_segment_is_visible(segment_obj)){
				//console.log("voice number "+voice_number+" segment "+segment_index+" is visible")
				//RE_redraw_segment_content(segment_obj,JSON.parse(JSON.stringify(segment_index)),JSON.parse(JSON.stringify(voice_id)))
				RE_redraw_segment_content(segment_obj,segment_index,voice_id,voice_number)
			}
		})
	})
}

function RE_redraw_segment_content(segment_obj,segment_index,voice_id,voice_number){
	let seg_loader = segment_obj.querySelector(".App_RE_segment_loader")
	if(seg_loader.classList.contains("loading")){
		let Psg_segment_start = Number(segment_obj.getAttribute('starting'))
		let n_column = Number(segment_obj.getAttribute('n_column'))
		let display_name = (n_column>5)? true:false
		//data
		let data = DATA_get_current_state_point(true)
		//list segments
		let labels_needed=[]
		let labels_order=["Na","Nm","Ngm","iNa","iNm","iNgm","Nabc","Ta","Tm","Ts","iT","Tf"]
		labels_order.forEach(label=>{
			if(data.global_variables.RE[label])labels_needed.push(label)
		})
		//var voice_data = data.voice_data.find(item=>{return item.voice_id==voice_id})
		let voice_data = data.voice_data[voice_number]
		if(data.global_variables.A.show && voice_data.A){
			labels_needed.unshift(data.global_variables.A.type)
		}
		let segment_data = voice_data.data.segment_data[segment_index]
		let N_NP=voice_data.neopulse.N
		let D_NP=voice_data.neopulse.D
		let seg_name_text=segment_data.segment_name
		let class_seg_name=""
		if(seg_name_text=="" || !display_name){
			class_seg_name="hidden"
		}
		let display_seg_merge=true
		//let display_seg_delete=Number(segment_obj.getAttribute('delete'))
		let display_seg_delete=segment_obj.getAttribute('delete')
		let draw_lines = true
		let RE_grades_data=DATA_calculate_grade_data(Li)
		let string = RE_calculate_segment_string(Psg_segment_start,n_column,segment_index,voice_id,class_seg_name,seg_name_text,display_seg_delete,draw_lines,segment_data,RE_grades_data,N_NP,D_NP,labels_needed,false)
		segment_obj.innerHTML=string
	}else{
		//console.log("already loaded")
		return
	}
}

function RE_segment_is_visible(segment) {
	const rect = segment.getBoundingClientRect()
	return (
		rect.width> 0 &&
		((rect.left >= 0 && rect.left<=(window.innerWidth || document.documentElement.clientWidth)) ||
		(rect.right >= 0 && rect.right <= (window.innerWidth || document.documentElement.clientWidth)) ||
		(rect.left <= 0 && rect.right >= (window.innerWidth || document.documentElement.clientWidth))//left left and right to the right of the border of the screen
		)
	)
}

//COPY IN WD
function RE_find_time_column_index(time,partial_RE_global_time_all_changes=null,partial_RE_global_time_segment_change=null){
	//Function give column index of a given time
	//if no time array is given it use the RE_global_time_all_changes AND RE_global_time_segment_change
	if (partial_RE_global_time_all_changes==null) {
		//look how much elements there are in global time and global time segment
		partial_RE_global_time_all_changes=RE_global_time_all_changes
		partial_RE_global_time_segment_change=RE_global_time_segment_change
	}
	let index_all = partial_RE_global_time_all_changes.indexOf(time)
	//var index_segment = partial_RE_global_time_segment_change.indexOf(time)
	let index_segment = -1
	partial_RE_global_time_segment_change.find((seconds,index)=>{
		if (seconds>time){
			return true
		}
		index_segment=index
		return false
	})
	/*
	let index_segment = 0
	let time_segment = -1
	partial_RE_global_time_segment_change.find((seconds,index)=>{
		if (seconds>=time){
			index_segment=index
			time_segment=seconds
			return true
		}
		return false
	})
	if (time_segment>time){
		index_segment--
	}*/
	if(index_segment<0)console.error("time not found SEGMENT")
	if(index_all<0){
		console.error("time not found")
	}
	return (index_all+index_segment)*2
}

function RE_disableEvent(event) {
	event.preventDefault()
	event.stopPropagation()
	return false
}

function RE_dbclick_inp_box(element){
	//iA or element
	APP_stop()
	let time = 0
	let pulse_abs = 0
	let pulse = 0
	let fraction = 0
	let segment_index = 0
	let voice_id = 0
	if(element.classList.contains("App_RE_iA_cell")){
		//return
		let info_list=RE_find_voice_elements_info_from_iA_element(element)
		//sub with the last one
		//var position = info.up.abs_position
		let info_up = info_list.up.info
		pulse = info_up.time.P
		pulse_abs = (info_up.Psg_segment_start + pulse)*info_up.N_NP/info_up.D_NP
		fraction = info_up.time.F
		time=DATA_calculate_exact_time(info_up.Psg_segment_start,pulse,fraction,info_up.fraction.N,info_up.fraction.D,info_up.N_NP,info_up.D_NP)
		segment_index = info_list.up.segment_index
		voice_id = info_list.up.voice_id
	}else{
		let position = RE_input_to_position(element)
		segment_index=position.segment_index
		voice_id=position.voice_id
		let info = DATA_get_object_index_info(voice_id,segment_index,position.sound_index)
		pulse = info.time.P
		pulse_abs = (info.Psg_segment_start + pulse)*info.N_NP/info.D_NP
		fraction = info.time.F
		if(info.fraction==null && fraction==0){
			//dbclick on Ls
			//info.fraction={N:1,D:1}
			//XXX
			return
		}
		time=DATA_calculate_exact_time(info.Psg_segment_start,pulse,fraction,info.fraction.N,info.fraction.D,info.N_NP,info.D_NP)
	}
	APP_player_to_time(time,pulse_abs,pulse,fraction,segment_index,voice_id)
	APP_play_all_notes_at_time(time)
}

//find the index of an element, default type ="input"
//for arrows and join etc = "button"
function RE_element_index(element, type){
	let parent = element.parentNode
	//if parent is null (element being deleted....)
	if(parent==null)return[-1,-1]
	let input_list = [...parent.querySelectorAll(type)]
	let elementIndex=input_list.indexOf(element)
	return [elementIndex,input_list]
}

function RE_read_segment_name(segment){
	return segment.querySelector(".App_RE_segment_name").value
}

function RE_change_segment_name(element){
	element.blur()
	APP_stop()
	let data = DATA_get_current_state_point(true)
	//Selected voice
	let voice_obj = element.closest(".App_RE_voice")
	let [,voice_id] = RE_read_voice_id(voice_obj)
	let selected_voice_data= data.voice_data.find(item=>{
		return item.voice_id==voice_id
	})
	//find the segment index
	let container = element.closest(".App_RE_segment_draggable")
	let segment_index= parseInt(container.querySelector(".App_RE_segment_number").innerHTML)

	selected_voice_data.data.segment_data[segment_index].segment_name = element.value

	if(element.value==""){
		element.classList.add("hidden")
	}else{
		element.classList.remove("hidden")
	}

	if(selected_voice_data.blocked){
		//change name all blocked voices
		data.voice_data.forEach(item=>{
			if(item.blocked)item.data.segment_data[segment_index].segment_name = element.value
		})
	}
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)//XXX NEED TO LOAD ????  FLAGS == TRUE/FALSE???
}

function RE_hide_segment_name(input) {
	if(input.classList.contains("washidden")){
		input.classList.add("hidden")
		input.classList.remove("washidden")
	}
}

function RE_parent_no_draggable(element){
	let segment = element.parentNode.parentNode
	segment.setAttribute("draggable", "false")
	let voice = segment.parentNode.parentNode
	voice.setAttribute("draggable", "false")
}

function RE_parent_draggable(element){
	let segment = element.closest(".App_RE_segment")
	segment.setAttribute("draggable", "true")
	//var voice = segment.parentNode.parentNode
	let voice = segment.closest(".App_RE_voice")
	voice.setAttribute("draggable", "true")
}

//segments management
function RE_add_segment_button(button){
	APP_stop()
	// define segment object
	let voice_data = button.closest(".App_RE_voice_data")
	let voice = button.closest(".App_RE_voice")
	let segment_list = [...voice_data.querySelectorAll(".App_RE_segment")]
	// define index of spacer
	let spacer = button.closest(".App_RE_segment_spacer")
	let [segment_index,] = RE_element_index(spacer, ".App_RE_segment_spacer")
	//console.log(parseInt(voice.getAttribute("value")))
	//console.log(RE_read_voice_id(voice))
	DATA_add_segment(RE_read_voice_id(voice)[1],segment_index)
}

function RE_delete_segment_button(button){
	let voice=button.closest(".App_RE_voice")
	let [segment_index, segment_list] = RE_element_index(button.closest(".App_RE_segment"),".App_RE_segment")
	DATA_delete_segment(RE_read_voice_id(voice)[1],segment_index)
}

function RE_merge_segment_button(button){
	APP_stop()
	// define segment object
	let voice_data = button.closest(".App_RE_voice_data")
	let voice = button.closest(".App_RE_voice")
	//var segment_list = [...voice_data.querySelectorAll(".App_RE_segment")]
	// define index of spacer
	let spacer = button.closest(".App_RE_segment_spacer")
	let [spacer_index,] = RE_element_index(spacer, ".App_RE_segment_spacer")
	let segment_index=spacer_index-1
	DATA_merge_segment(RE_read_voice_id(voice)[1],segment_index)
}

function RE_break_segment_button(button) {
	APP_stop()
	let segment = button.closest('.App_RE_segment')
	let voice_id = RE_read_voice_id(segment.closest(".App_RE_voice"))[1]
	let segment_index = [...segment.closest(".App_RE_voice_data").querySelectorAll(".App_RE_segment")].indexOf(segment)
	let input_P_list = segment.querySelectorAll(".App_RE_Ts>input")
	//all the calculation if breakable is done by the spawning of the button
	let P_target_index = Number(button.value)
	let pulse = parseInt(input_P_list[P_target_index].value)
	DATA_break_segment(voice_id,segment_index, pulse)
}

function RE_read_voice_id(voice_obj){
	let name = voice_obj.querySelector(".App_RE_voice_name").value
	let voice_id = Number(voice_obj.getAttribute("value"))
	return [name, voice_id]
}

function RE_find_voice_obj_by_id(find_voice_id){
	let data = document.querySelector(".App_RE")
	let voice_matrix = data.querySelector(".App_RE_voice_matrix")
	let voice_list = [...voice_matrix.querySelectorAll(".App_RE_voice")]
	if(voice_list.length==0)return null
	let voice = voice_list.find((voice)=>{
		//find voice id
		let [voice_name, voice_id] = RE_read_voice_id(voice)
		if(find_voice_id == voice_id) return voice
	})
	return voice
}

//return Nom and Den of a voice polipulse fraction
function RE_calculate_NP(voice){
	let NP_voice = voice.querySelector(".App_RE_neopulse_inp_box").value
	let split = NP_voice.split('/')
	let N = Math.floor(split[0])
	let D = Math.floor(split[1])
	return [N,D]
}

//scrollbars sync with different voices
function RE_voice_scrolling(voice_obj,event){
	if(voice_obj.scrollLeft != current_position_PMCRE.RE.X_start){
		RE_checkScroll_H(voice_obj.scrollLeft)
	}
}

async function RE_checkScroll_H(value=null,forceScrollbarCheck = false) {
	//.App_RE_voice_data
	if(!current_position_PMCRE.RE.scrollbar)return
	//console.error("ScrollbarCheck ")
	if(value==null){
		let data = document.querySelector('.App_RE')
		let scrollbar = data.querySelector('.App_RE_main_scrollbar_H')
		value=scrollbar.scrollLeft
	}else{
		forceScrollbarCheck=true
	}
	if(value != current_position_PMCRE.RE.X_start || forceScrollbarCheck) {
		RE_forceScroll_H(value)
		APP_hide_scale_circle_elements()
	}
}

function RE_forceScroll_H(value){
	setTimeout(() => {
		current_position_PMCRE.RE.scrollbar=false
		current_position_PMCRE.RE.X_start=value
		let data = document.querySelector('.App_RE')
		let voice_matrix = data.querySelector('.App_RE_voice_matrix')
		let voice_list = [...voice_matrix.querySelectorAll('.App_RE_voice_data')]
		voice_list.forEach((item)=>{item.scrollLeft = value})
		let iA_list = [...voice_matrix.querySelectorAll('.App_RE_iA:not(.hidden)')]
		iA_list.forEach((item)=>{item.scrollLeft = value})
		let C_line = data.querySelector('.App_RE_C')
		let S_line = data.querySelector('.App_RE_S')
		C_line.scrollLeft = value
		S_line.scrollLeft = value
		let column_selector = document.getElementById('App_RE_column_selector_container')
		column_selector.scrollLeft = value
	 	let scrollbar = data.querySelector('.App_RE_main_scrollbar_H')
		scrollbar.scrollLeft=value
		current_position_PMCRE.RE.scrollbar=true
		if(current_position_PMCRE.RE.scrollbar){
			RE_verify_segment_visibility()
		}
	}, 1);
}

function RE_forceScroll_HV(value,Y){
	setTimeout(() => {
		current_position_PMCRE.RE.scrollbar=false
		current_position_PMCRE.RE.X_start=value
		current_position_PMCRE.RE.Y_start=Y
		let data = document.querySelector('.App_RE')
		let voice_matrix = data.querySelector('.App_RE_voice_matrix')
		voice_matrix.scrollTop=Y
		let voice_list = [...voice_matrix.querySelectorAll('.App_RE_voice_data')]
		voice_list.forEach((item)=>{item.scrollLeft = value})
		let iA_list = [...voice_matrix.querySelectorAll('.App_RE_iA:not(.hidden)')]
		iA_list.forEach((item)=>{item.scrollLeft = value})
		let C_line = data.querySelector('.App_RE_C')
		let S_line = data.querySelector('.App_RE_S')
		C_line.scrollLeft = value
		S_line.scrollLeft = value
		let column_selector = document.getElementById('App_RE_column_selector_container')
		column_selector.scrollLeft = value
	 	let scrollbar = data.querySelector('.App_RE_main_scrollbar_H')
		scrollbar.scrollLeft=value
		current_position_PMCRE.RE.scrollbar=true
	}, 1);
}

function RE_input_to_position(element){
	let text=element.getAttribute("position").replaceAll("'",'"')
	return JSON.parse(text)
}

function RE_set_current_position(element){
	let pos = RE_input_to_position(element)
	let element_info = DATA_get_object_index_info(pos.voice_id,pos.segment_index,pos.sound_index)
	//RE position
	APP_reset_blink_play()
	let column_index=pos.column_index
	let segment_obj = element.closest(".App_RE_segment")
	if(element.parentElement.classList.contains("App_RE_iT"))column_index--;
	let sound_index=(element.value=="" && !element.classList.contains("App_RE_A_cell"))?null:pos.sound_index
	RE_highlight_column_at_inp_box_index(sound_index,column_index,segment_obj,"current")
	//find voicedata
	let current_voice_data=DATA_get_current_state_point(false).voice_data.find(voice=>{return voice.voice_id==pos.voice_id})
	//PMC_position if needed
	if(current_voice_data.selected){
		if(element_info.time.P==null){
			//clear PMC
			PMC_reset_progress_line()
		}else{
			let time = 0
			if(element_info.fraction==null){
				//console.log("end of segment")
				//time = (Psg_segment_start+pulse) *current_voice_data.neopulse.N/current_voice_data.neopulse.D * 60/PPM
				time= DATA_calculate_exact_time(element_info.Psg_segment_start,element_info.time.P,0,1,1,current_voice_data.neopulse.N,current_voice_data.neopulse.D)
			}else{
				//time = (Psg_segment_start+pulse+fraction *current_fraction.N/current_fraction.D) *current_voice_data.neopulse.N/current_voice_data.neopulse.D * 60/PPM
				time= DATA_calculate_exact_time(element_info.Psg_segment_start,element_info.time.P,element_info.time.F,element_info.fraction.N,element_info.fraction.N,current_voice_data.neopulse.N,current_voice_data.neopulse.D)
			}
			PMC_draw_progress_line_time(time)
		}
	}else{
		//clear PMC
		PMC_reset_progress_line()
	}
	current_position_PMCRE.voice_data= current_voice_data
	//current_position_PMCRE.PMC.X_start = null //PMC_draw_progress_line_time OR PMC_reset_progress_line  write it down
	current_position_PMCRE.PMC.Y_start = null
	current_position_PMCRE.segment_index = pos.segment_index
	current_position_PMCRE.pulse = element_info.time.P
	current_position_PMCRE.fraction = element_info.time.F
	current_position_PMCRE.note = null
	//not setting scroll position
}

function RE_highlight_column_at_inp_box_index(sound_index=null,column_index,segment_obj,color="current"){
	let line_list = [...segment_obj.querySelectorAll(".App_RE_segment_line:not(.App_RE_Tf):not(.App_RE_A_pos):not(.App_RE_A_neg)")]
	line_list.forEach(line=>{
		let inp_box_list = [...line.querySelectorAll(".App_RE_inp_box")]
		if(line.classList.contains("App_RE_iT")){
			if(color=="current"){
				APP_blink_play_green_light(inp_box_list[column_index+1])
			}else{
				//APP_blink_play_blue(inp_box_list[column_index+1])
				APP_blink_play_green(inp_box_list[column_index+1])
			}
		}else{
			if(color=="current"){
				APP_blink_play_green_light(inp_box_list[column_index])
			}else{
				//APP_blink_play_blue(inp_box_list[column_index])
				APP_blink_play_green(inp_box_list[column_index])
			}
		}
	})
	if(sound_index!=null){
		let A_line_list = [...segment_obj.querySelectorAll(".App_RE_A_pos,.App_RE_A_neg")]
		A_line_list.forEach(line=>{
			let inp_box_list = [...line.querySelectorAll(".App_RE_A_cell")]
			if(color=="current"){
				APP_blink_play_green_light(inp_box_list[sound_index])
			}else{
				APP_blink_play_green(inp_box_list[sound_index])
			}
		})
	}
}

function RE_highlight_columns_at_pulse(pulse_abs ,time=null, voice_id=null){
	// function used only by time bar , never used fraction
	//pulse => Ps
	if(!flag_DATA_updated.RE)return
	if(!tabREbutton.checked)return
	//console.log(pulse_abs+"   "+time)
	APP_reset_blink_play()
	//find correct pulse for every voice
	let voice_matrix_obj = document.querySelector('.App_RE_voice_matrix')
	let voice_obj_list = [...voice_matrix_obj.querySelectorAll('.App_RE_voice')]
	let index_scrollbar = null
	let move = false
	if(time==null){
		time = DATA_calculate_exact_time(pulse_abs,0,0,1,1,1,1)
	}
	let data = DATA_get_current_state_point(true)
	if(pulse_abs<Li){
		voice_obj_list.forEach(voice_obj=>{
			//use RE_global_time_voices_changes
			let current_voice_id = RE_read_voice_id(voice_obj)[1]
			//Selected voice
			let selected_voice_data= data.voice_data.find(item=>{
				return item.voice_id==current_voice_id
			})
			//if not visible end
			if(!selected_voice_data.RE_visible){
				return
			}
			let global_voice_index=null
			let found = selected_voice_data.data.midi_data.time.some((midi_time,index) =>{
				if(midi_time==time){
					global_voice_index=index
					return true
				}
				return false
			})

			if(found){
				let all_segment_data = selected_voice_data.data.segment_data
				let sound_index = null
				let column_index = null
				let segment_index = null
				let i_end=0
				all_segment_data.some((segment,index_s)=>{
					segment.time.pop()
					let i_start=i_end
					i_end+=segment.time.length
					if(i_end>=global_voice_index){
						segment_index=index_s
						return segment.time.some((t,index_t)=>{
							if(i_start+index_t==global_voice_index){
								sound_index=index_t
								return true
							}else{
								return false
							}
						})
					}else{
						return false
					}
				})
				let segment_obj_list= [...voice_obj.querySelectorAll(".App_RE_segment")]
				let segment_obj = segment_obj_list[segment_index]
				let segment_line_obj = segment_obj.querySelector(".App_RE_segment_line:not(.App_RE_A_pos):not(.App_RE_A_neg)")//the first is fine
				let input_list = [...segment_line_obj.querySelectorAll(".App_RE_inp_box")]
				let i=-1
				input_list.find((input,index)=>{
					if (input.value!=""){
						i++
						if(i==sound_index){
							column_index=index
							return true
						}else{
							return false
						}
					}
				})
				if(current_voice_id!=voice_id){
					RE_highlight_column_at_inp_box_index(sound_index,column_index,segment_obj,"other")
				}else{
					RE_highlight_column_at_inp_box_index(sound_index,column_index,segment_obj,"current")
				}
				if(index_scrollbar==null){
					let past_segments_columns = segment_obj_list.reduce((prop,item,index_s)=>{
						if(index_s<segment_index){
							return prop+1+item.clientWidth/nuzic_block
						}else{
							return prop
						}
					},0)
					index_scrollbar = column_index + past_segments_columns
					move=true
				}
			}
		})
		//move slider in best position if founs something or if bigger than Li (is a compas)
		if(move){
			//move slider to position
			RE_calculate_best_scrollbar_position(index_scrollbar)
		}
	}else{
		//calculate compas position
		let C_line = document.querySelector(".App_RE_C")
		let C_button_list =  [...C_line.querySelectorAll(".App_RE_C_button")]
		let found = C_button_list.find(button => {
			let position = parseInt(button.getAttribute("position"))
			if (position>=pulse_abs){
				return button
			}
		})
		//find index compas
		if(found===undefined)return
		let str = found.style.left
		let n_pixels_left=parseInt(str.substring(0, str.length - 2))
		//var tot_buttons = scale_button_list.length
		let index_compas = n_pixels_left/nuzic_block+1
		//move slider to position
		RE_calculate_best_scrollbar_position(index_compas)
	}
}

function RE_hightlight_column_on_play_on(segment_obj,column_index,sound_index=null){
	//APP_blink_play_input_list(element_list)
	let loader = segment_obj.querySelector(".loading")
	if(loader==null){
		let lines=segment_obj.querySelectorAll(".App_RE_segment_line:not(.App_RE_Tf):not(.App_RE_A_pos):not(.App_RE_A_neg)")
		let child_n=column_index+1
		lines.forEach(line=>{
			let element=null
			if(line.classList.contains("App_RE_iT")){
				element = line.querySelector(":nth-child("+(child_n+1)+")")
			}else{
				element = line.querySelector(":nth-child("+child_n+")")
			}
			if(element!=null){
				APP_blink_play_blue(element)
			}else{
				console.error("failed to blink column "+column_index)
			}
		})
		let A_line_list = [...segment_obj.querySelectorAll(".App_RE_A_pos,.App_RE_A_neg")]
		A_line_list.forEach(line=>{
			let element = [...line.querySelectorAll(".App_RE_A_cell")][sound_index]
			if(element!=null)APP_blink_play_blue(element)
		})
	}else{
		//wait??? TOO LATE????
		//console.error("wait for segment to load")
		//console.log(loader)
	}
}

function RE_hightlight_column_on_play_off(segment_obj,column_index,sound_index){
	let loader = segment_obj.querySelector(".loading")
	if(loader==null){
		let lines=segment_obj.querySelectorAll(".App_RE_segment_line:not(.App_RE_Tf):not(.App_RE_A_pos):not(.App_RE_A_neg)")
		let child_n=column_index+1
		lines.forEach(line=>{
			let element=null
			if(line.classList.contains("App_RE_iT")){
				element = line.querySelector(":nth-child("+(child_n+1)+")")
			}else{
				element = line.querySelector(":nth-child("+child_n+")")
			}
			if(element!=null){
				APP_stop_blink_play_blue(element)
			}else{
				console.error("failed to blink column "+column_index)
			}
		})
		let A_line_list = [...segment_obj.querySelectorAll(".App_RE_A_pos,.App_RE_A_neg")]
		A_line_list.forEach(line=>{
			let element = [...line.querySelectorAll(".App_RE_A_cell")][sound_index]
			if(element!=null)APP_stop_blink_play_blue(element)
		})
	}else{
		//wait??? TOO LATE?????
		//console.error("wait for segment to load")
	}
}

let old_index_position_scrollbar = 0
async function RE_calculate_best_scrollbar_position(index_position){
	if(!tabREbutton.checked){
		return
	}
	index_position++
	let data = document.querySelector('.App_RE')
	let scrollbar = data.querySelector('.App_RE_main_scrollbar_H')
	//see how big is the scrollbar
	let width = scrollbar.clientWidth -196 -13 //padding
	//see how many sqares is the visible space
	let Nsq = Math.round(width / 28)
	//how many squares in total
	let Nsq_max = Math.round(scrollbar.scrollWidth / 28) -1
	let range = scrollbar.scrollWidth-width
	//better steps
	if(Nsq>12){
		//defining the common part Nsq_common width
		//the better fit bw 6,8,10
		let buffer = [6,8,10]
		let sum = buffer.map( (val, i) => val + Nsq_max )
		let remainder = sum.map( (val, i) => val % Nsq )
		//take it bigger remainder
		let best = Math.max.apply(null,remainder)
		let best_index = remainder.indexOf(best)
		let Nsq_common = buffer[best_index]
		let mod = Nsq - Nsq_common
		let step = scrollbar.scrollWidth / Nsq_max
		if((index_position%mod)<Nsq_common-1 && index_position>Nsq_common){
			//inside common zone, deciding if necesary to jump page
			let mod_position = Math.round(index_position/mod)
			//determine if going right or left
			//going right
			if(index_position>old_index_position_scrollbar){
				//console.log("RIGHT")
				RE_checkScroll_H(mod_position*mod*step)
				old_index_position_scrollbar=index_position
				return
			}
			//going left
			if(index_position<old_index_position_scrollbar){
				//console.log("LEFT")
				RE_checkScroll_H((mod_position-1)*mod*step)
				old_index_position_scrollbar=index_position
				return
			}
		}else{
			//maybe simple control being in the correct place
			//case jump from fringe Nsq_common
			let init_position = Math.round(scrollbar.scrollLeft / step)
			let end_position = init_position + Nsq
			if(index_position < init_position || index_position > end_position){
				//console.log("need to move")
				let mod_position = Math.round(index_position/mod)
				RE_checkScroll_H((mod_position)*mod*step)
				old_index_position_scrollbar=index_position
				return
			}
		}
		old_index_position_scrollbar=index_position
	}else{
		//scroll 1 on 1
		let step = range / Nsq_max
		if(index_position==-1)index_position=Nsq_max
		RE_checkScroll_H(index_position*step)
		old_index_position_scrollbar=index_position
	}
}

function RE_string_to_superscript(string){
	//return the current text in subscript format
	let chars = '+−-=()0123456789AaÆᴂɐɑɒBbcɕDdðEeƎəɛɜɜfGgɡɣhHɦIiɪɨᵻɩjJʝɟKklLʟᶅɭMmɱNnɴɲɳŋOoɔᴖᴗɵȢPpɸrRɹɻʁsʂʃTtƫUuᴜᴝʉɥɯɰʊvVʋʌwWxyzʐʑʒꝯᴥβγδθφχнნʕⵡ'
	let sup = '⁺⁻⁻⁼⁽⁾⁰¹²³⁴⁵⁶⁷⁸⁹ᴬᵃᴭᵆᵄᵅᶛᴮᵇᶜᶝᴰᵈᶞᴱᵉᴲᵊᵋᶟᵌᶠᴳᵍᶢˠʰᴴʱᴵⁱᶦᶤᶧᶥʲᴶᶨᶡᴷᵏˡᴸᶫᶪᶩᴹᵐᶬᴺⁿᶰᶮᶯᵑᴼᵒᵓᵔᵕᶱᴽᴾᵖᶲʳᴿʴʵʶˢᶳᶴᵀᵗᶵᵁᵘᶸᵙᶶᶣᵚᶭᶷᵛⱽᶹᶺʷᵂˣʸᶻᶼᶽᶾꝰᵜᵝᵞᵟᶿᵠᵡᵸჼˤⵯ'
	return sup_string = [...string].map(char => {
		//console.log(char)
		let index = chars.indexOf(char)
		return (index != -1 ? sup[index] : char)
	}).join("")
}

function RE_superscript_to_string(string){
	let chars = '+−-=()0123456789AaÆᴂɐɑɒBbcɕDdðEeƎəɛɜɜfGgɡɣhHɦIiɪɨᵻɩjJʝɟKklLʟᶅɭMmɱNnɴɲɳŋOoɔᴖᴗɵȢPpɸrRɹɻʁsʂʃTtƫUuᴜᴝʉɥɯɰʊvVʋʌwWxyzʐʑʒꝯᴥβγδθφχнნʕⵡ'
	let sup = '⁺⁻⁻⁼⁽⁾⁰¹²³⁴⁵⁶⁷⁸⁹ᴬᵃᴭᵆᵄᵅᶛᴮᵇᶜᶝᴰᵈᶞᴱᵉᴲᵊᵋᶟᵌᶠᴳᵍᶢˠʰᴴʱᴵⁱᶦᶤᶧᶥʲᴶᶨᶡᴷᵏˡᴸᶫᶪᶩᴹᵐᶬᴺⁿᶰᶮᶯᵑᴼᵒᵓᵔᵕᶱᴽᴾᵖᶲʳᴿʴʵʶˢᶳᶴᵀᵗᶵᵁᵘᶸᵙᶶᶣᵚᶭᶷᵛⱽᶹᶺʷᵂˣʸᶻᶼᶽᶾꝰᵜᵝᵞᵟᶿᵠᵡᵸჼˤⵯ'
	let normalized_string = [...string].map(char => {
		//console.log(char)
		let index = sup.indexOf(char)
		return (index != -1 ? chars[index] : char)
	}).join("")
	return normalized_string
}

function RE_superscript_to_Tm_string(string){
	let chars = '+−-=()0123456789AaÆᴂɐɑɒBbcɕDdðEeƎəɛɜɜfGgɡɣhHɦIiɪɨᵻɩjJʝɟKklLʟᶅɭMmɱNnɴɲɳŋOoɔᴖᴗɵȢPpɸrRɹɻʁsʂʃTtƫUuᴜᴝʉɥɯɰʊvVʋʌwWxyzʐʑʒꝯᴥβγδθφχнნʕⵡ'
	let sup = '⁺⁻⁻⁼⁽⁾⁰¹²³⁴⁵⁶⁷⁸⁹ᴬᵃᴭᵆᵄᵅᶛᴮᵇᶜᶝᴰᵈᶞᴱᵉᴲᵊᵋᶟᵌᶠᴳᵍᶢˠʰᴴʱᴵⁱᶦᶤᶧᶥʲᴶᶨᶡᴷᵏˡᴸᶫᶪᶩᴹᵐᶬᴺⁿᶰᶮᶯᵑᴼᵒᵓᵔᵕᶱᴽᴾᵖᶲʳᴿʴʵʶˢᶳᶴᵀᵗᶵᵁᵘᶸᵙᶶᶣᵚᶭᶷᵛⱽᶹᶺʷᵂˣʸᶻᶼᶽᶾꝰᵜᵝᵞᵟᶿᵠᵡᵸჼˤⵯ'
	let normalized_string = [...string].map(char => {
		//console.log(char)
		let index = sup.indexOf(char)
		return (index != -1 ? "c"+chars[index] : char)
	}).join("")
	//clear extra "c" char
	let first = true
	let output_string = [...normalized_string].filter(char =>{
		if(char=="c"){
			if(first){
				first=false
				return true
			}else{
				return false
			}
		}else{
			return true
		}
	}).join("")
	return output_string
}

function RE_replace_inp_box_superscript_Tm(element){
	let string = element.value
	element.value=RE_superscript_to_Tm_string(string)
	RE_resize_inp_box(element)
}

function RE_replace_first_Tm_to_superscript(element){
	element.value=previous_value
}

function RE_replace_inp_box_superscript(element){
	let string = element.value
	element.value=RE_superscript_to_string(string)
}

function RE_resize_inp_box(element){
	//onchange only at intro...
	//oninput is better
	let width = element.value.length*8
	let margin = 0
	if(width>28){
		margin = -(element.value.length*8-28)/2
		element.style.width = element.value.length*8 + "px"
	}else {
		element.style.width = "28px"
	}
	element.style.marginLeft = margin + "px"
	element.style.marginRight = margin + "px"
}

function RE_open_C_lines_visualization(){
	let button = document.querySelector('.App_RE_C_show_visualization_options_button')
	let box = button.parentNode.querySelector('.App_RE_C_show_visualization_options_box')
	box.style.display='block'
}

function RE_close_C_lines_visualization(){
	let button = document.querySelector('.App_RE_C_show_visualization_options_button')
	if(button==null)return
	let box = button.parentNode.querySelector('.App_RE_C_show_visualization_options_box')
	box.style.display='none'
}

function RE_open_S_lines_visualization(){
	let button = document.querySelector('.App_RE_S_show_visualization_options_button')
	let box = button.parentNode.querySelector('.App_RE_S_show_visualization_options_box')
	box.style.display='block'
}

function RE_close_S_lines_visualization(){
	let button = document.querySelector('.App_RE_S_show_visualization_options_button')
	if(button==null)return
	let box = button.parentNode.querySelector('.App_RE_S_show_visualization_options_box')
	box.style.display='none'
}

function RE_calculate_line_strings(voice_id,segment_index,segment_data,RE_grades_data,Psg_segment_start,N_NP,D_NP,labels_needed,n_column){
	let line_string=""
	//calculate partial global time array
	let Psg_segment_end = segment_data.time.slice(-1)[0].P+Psg_segment_start
	let time_segment_start= DATA_calculate_exact_time(Psg_segment_start,0,0,1,1,N_NP,D_NP)
	let time_segment_end= DATA_calculate_exact_time(Psg_segment_end,0,0,1,1,N_NP,D_NP)
	let index_start = RE_global_time_all_changes.indexOf(time_segment_start)
	let index_end= RE_global_time_all_changes.indexOf(time_segment_end)
	//find alternative array
	let partial_RE_global_time_all_changes = RE_global_time_all_changes.slice(index_start,index_end+1)
	let index_start_segment_changes = RE_global_time_segment_change.indexOf(time_segment_start)
	let index_end_segment_changes= RE_global_time_segment_change.indexOf(time_segment_end)
	let partial_RE_global_time_segment_change = RE_global_time_segment_change.slice(index_start_segment_changes,index_end_segment_changes+1)
	//generate index array columns
	let max_fraction_index = segment_data.fraction.length-1
	let index_columns = segment_data.time.map((time,time_index)=>{
		//change length  arrow
		//calculate times
		let time_sec
		if(time.F!=0){
			let fraction_data = segment_data.fraction.find((fraction)=>{
				return fraction.stop>time.P
			})
			time_sec=DATA_calculate_exact_time(Psg_segment_start,time.P,time.F,fraction_data.N,fraction_data.D,N_NP,D_NP)
		}else{
			time_sec=DATA_calculate_exact_time(Psg_segment_start,time.P,time.F,1,1,N_NP,D_NP)
		}
		//look how much elements there are in global time
		return RE_find_time_column_index(time_sec,partial_RE_global_time_all_changes,partial_RE_global_time_segment_change)
	})
	let last_value = index_columns.pop()-2//correct last value
	index_columns.push(last_value)
	let change_dimension=true
	let sound_line=false
	let index=0
	let sound_index=0
	let first_note=true
	let prev_note=null
	let prev_reg=null
	let prev_scale_index=-1
	let prev_pulse=-1
	let prev_col=0
	let neopulse = N_NP!=1 || D_NP!=1
	let A_width_list=[]
	let line_start=0
	let line_end=0
	let tick_y1=10
	let tick_y2=18
	let no_scale=DATA_get_scale_sequence().length==0
	let idea_scale_list=DATA_get_idea_scale_list()
	labels_needed.forEach(type=>{
		let type_used=type
		if(type=="Ngm" && no_scale) type_used="Nm"
		if(type=="iNgm" && no_scale) type_used="iNm"
		if(type=="Ag" && no_scale) type_used="Am"
		switch (type_used) {
			case 'Aa':
			case 'Am':
			case 'Ag':
				sound_line=true
				let A_pos_input_string=""
				let A_neg_input_string=""
				let input_index=-1
				index_columns.forEach((col,index)=>{
					input_index++
					let current_note=segment_data.sound[index].note
					if(current_note==-4){
						A_pos_input_string+= `<label class="App_RE_inp_box App_RE_segment_last_cell"
									onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)"
									data-value="dotHover">${end_range}</label>`
						A_neg_input_string+= `<label class="App_RE_inp_box App_RE_segment_last_cell"
									onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)"
									data-value="dotHover">${end_range}</label>`
						return
					}
					let width=index_columns[index+1]-col
					let style_A=`style="width: calc(var(--RE_block) * ${width}"`
					if (current_note ==-1 || current_note ==-2){
						A_pos_input_string+= `<div class="App_RE_A_cell" ${style_A} disabled></div>`
						A_neg_input_string+= `<div class="App_RE_A_cell" ${style_A} disabled></div>`
						return
					}else if(current_note ==-3){
						style_A=`style="width: calc(var(--RE_block) * ${width});
							padding-right: calc(var(--RE_block) * ${(width-1)});"`
						//style_A=`style="width: calc(var(--RE_block) * ${width}); background-color:var(--Nuzic_pink_light)"`
						//A_pos_input_string+= `<input class="App_RE_A_cell" ${style_A} disabled="true" value="L"></input>`
						A_pos_input_string+= `<label class="App_RE_A_cell" ${style_A} >L</label>`
						A_neg_input_string+= `<label class="App_RE_A_cell" ${style_A} disabled="true" value="L">L</label>`
						return
					}
					let A_pos_value=""
					let A_neg_value=""
					let disabled=false
					//change style A
					if(type_used=="Ag"){
						let current_diesis=segment_data.sound[index].diesis
						let current_P = segment_data.time[index].P
						let current_F = segment_data.time[index].F
						//find correct scale
						//find Pa_equivalent and verify if inside a scale
						let current_fraction = segment_data.fraction.find(fraction=>{return fraction.stop>current_P})
						let frac_p = current_fraction.N/current_fraction.D
						let Pa_equivalent = (Psg_segment_start+current_P) * N_NP/D_NP + current_F* frac_p * N_NP/D_NP
						//let current_scale = DATA_get_Pa_eq_scale(Pa_equivalent)
						let current_grades_data= RE_grades_data[DATA_get_Pa_eq_grade_data_index(Pa_equivalent,RE_grades_data)]
						// if(current_scale!=null){
							A_pos_value=APP_A_list_to_text(segment_data.sound[index].A_pos,type_used,{current_grades_data,base_note:current_note,base_diesis:current_diesis,pos:true})
							A_neg_value=APP_A_list_to_text(segment_data.sound[index].A_neg,type_used,{current_grades_data,base_note:current_note,base_diesis:current_diesis,pos:false})
						// }else{
						// 	A_pos_value=APP_A_list_to_text(segment_data.sound[index].A_pos,type_used)
						// 	A_neg_value=APP_A_list_to_text(segment_data.sound[index].A_neg,type_used)
						// 	disabled=true
						// }
					}else{
						A_pos_value=APP_A_list_to_text(segment_data.sound[index].A_pos,type_used)
						A_neg_value=APP_A_list_to_text(segment_data.sound[index].A_neg,type_used)
					}
					let A_pos_blocs=Math.ceil(A_pos_value.length/4)
					if(A_pos_blocs==0)A_pos_blocs=1
					let A_pos_pad=((width-A_pos_blocs<=0))?"0 + 2px":(width-A_pos_blocs)
					let A_neg_blocs=Math.ceil(A_neg_value.length/4)
					if(A_neg_blocs==0)A_neg_blocs=1
					let A_neg_pad=((width-A_neg_blocs<=0))?"0 + 2px":(width-A_neg_blocs)
					let style_A_pos=`style="width: calc(var(--RE_block) * ${width});
						padding-right: calc(var(--RE_block) * ${A_pos_pad});"`
					let style_A_neg=`style="width: calc(var(--RE_block) * ${width});
						padding-right: calc(var(--RE_block) * ${A_neg_pad});"`
					if(disabled){
						A_pos_input_string+=`<label class="App_RE_A_cell" ${style_A_pos}>${A_pos_value}</label>`
						A_neg_input_string+=`<label class="App_RE_A_cell" ${style_A_neg}>${A_neg_value}</label>`
					}else{
						A_pos_input_string+= `<input class="App_RE_A_cell" ondragstart="RE_disableEvent(event)"
									onkeypress="APP_inpTest_A_line(event,this,'${type_used}')" onkeydown="RE_move_focus_A(event,this)"
									onfocusin="APP_set_previous_value(this);RE_parent_no_draggable(this);APP_play_input_on_click(this,'A');RE_set_current_position(this);APP_show_A_suggestions(this)"
									onfocusout="RE_parent_draggable(this);RE_enter_new_value_A(this,'${type_used}',true)"
									ondblclick="RE_dbclick_inp_box(this)" position="{'sound_index':${index},'segment_index':${segment_index},'voice_id':${voice_id},'column_index':${col},'input_index':${input_index}}"
									placeholder="" value="${A_pos_value}" ${style_A_pos}></input>`
						A_neg_input_string+= `<input class="App_RE_A_cell" ondragstart="RE_disableEvent(event)"
									onkeypress="APP_inpTest_A_line(event,this,'${type_used}')" onkeydown="RE_move_focus_A(event,this)"
									onfocusin="APP_set_previous_value(this);RE_parent_no_draggable(this);APP_play_input_on_click(this,'A');RE_set_current_position(this)"
									onfocusout="RE_parent_draggable(this);RE_enter_new_value_A(this,'${type_used}',false)"
									ondblclick="RE_dbclick_inp_box(this)" position="{'sound_index':${index},'segment_index':${segment_index},'voice_id':${voice_id},'column_index':${col},'input_index':${input_index}}"
									placeholder="" value="${A_neg_value}" ${style_A_neg}></input>`
					}
				})
				line_string+=`<div class="App_RE_segment_line App_RE_A_pos">${A_pos_input_string}</div>`
				line_string+=`<div class="App_RE_segment_line App_RE_A_neg">${A_neg_input_string}</div>`
			break;
			case 'Na':
				sound_line=true
				index=0
				sound_index=0
				line_string+='<div class="App_RE_segment_line App_RE_Na">'
				for (let i = 0; i < n_column-1; i++) {
					let input_value=""
					let data_value=""
					if(index_columns[index]==i){
						let current_note = segment_data.sound[index].note
						if(current_note<0){
							[input_value,data_value]=RE_write_a_no_note(current_note)
						}else {
							input_value= current_note
						}
					}
					let end_column=""
					if(index_columns[index]==i+1)end_column="App_RE_end_column"
					line_string+= `<input class="App_RE_inp_box App_RE_note_cell ${end_column}" ondragstart="RE_disableEvent(event)"
										onkeypress="APP_inpTest_Na_line(event,this)" onkeydown="RE_move_focus(event,this)"
										onfocusin="APP_set_previous_value(this);RE_parent_no_draggable(this);APP_play_input_on_click(this,'N');RE_set_current_position(this)"
										onfocusout="RE_parent_draggable(this);RE_enter_new_value_S(this,'Na')"
										ondblclick="RE_dbclick_inp_box(this)" onmouseenter="APP_hover_area_enter(this)"
										onmouseleave="APP_hover_area_exit(this)" maxlength="3"
										position="{'sound_index':${sound_index},'segment_index':${segment_index},'voice_id':${voice_id},'column_index':${i}}"
										placeholder=" " value=${input_value} ${data_value}></input>`
					if(index_columns[index]==i+1)sound_index++
					if(index_columns[index]==i)index++
				}
				line_string+= `<label class="App_RE_inp_box App_RE_segment_last_cell"
									onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)"
									data-value="dotHover">${end_range}</label>
								</div>`
			break;
			case 'Nm':
				sound_line=true
				index=0
				sound_index=0
				prev_reg=null
				line_string+='<div class="App_RE_segment_line App_RE_Nm">'
				for (let i = 0; i < n_column-1; i++) {
					let input_value=""
					let data_value=""
					if(index_columns[index]==i){
						let current_note = segment_data.sound[index].note
						if(current_note<0){
							[input_value,data_value]=RE_write_a_no_note(current_note)
						}else {
							let reg = Math.floor(current_note/TET)
							let resto = current_note%TET
							//warning first element
							//verify is has module, if not read previous value
							let showreg = true
							//here retrive button value
							if(prev_reg==reg)showreg=false
							if(showreg){
								input_value=resto+"r"+reg
							}else{
								input_value=resto
							}
							prev_reg= reg
						}
					}
					let end_column=""
					if(index_columns[index]==i+1)end_column="App_RE_end_column"
					line_string+= `<input class="App_RE_inp_box App_RE_note_cell ${end_column}" ondragstart="RE_disableEvent(event)"
									onkeypress="APP_inpTest_Nm_line(event,this)" onkeydown="RE_move_focus(event,this)"
									onfocusin="APP_set_previous_value(this);RE_parent_no_draggable(this);APP_play_input_on_click(this,'N');RE_set_current_position(this)"
									onfocusout="RE_parent_draggable(this);RE_enter_new_value_S(this,'Nm')"
									ondblclick="RE_dbclick_inp_box(this)" onmouseenter="APP_hover_area_enter(this)"
									position="{'sound_index':${sound_index},'segment_index':${segment_index},'voice_id':${voice_id},'column_index':${i}}"
									onmouseleave="APP_hover_area_exit(this)" maxlength="4" placeholder=" " value=${input_value} ${data_value}></input>`
					if(index_columns[index]==i+1)sound_index++
					if(index_columns[index]==i)index++
				}
				line_string+= `<label class="App_RE_inp_box App_RE_segment_last_cell"
									onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)"
									data-value="dotHover">${end_range}</label>
								</div>`
			break;
			case 'Ngm':
				sound_line=true
				index=0
				sound_index=0
				prev_reg=null
				prev_scale_index=-1
				line_string+='<div class="App_RE_segment_line App_RE_Ngm">'
				for (let i = 0; i < n_column-1; i++) {
					let input_value=""
					let data_value=""
					let input_class=""
					if(index_columns[index]==i){
						let current_note = segment_data.sound[index].note
						let current_P = segment_data.time[index].P
						let current_F = segment_data.time[index].F
						//find correct scale
						//find Pa_equivalent and verify if inside a scale
						let current_fraction = segment_data.fraction.find(fraction=>{return fraction.stop>current_P})
						let frac_p = current_fraction.N/current_fraction.D
						let Pa_equivalent = (Psg_segment_start+current_P) * N_NP/D_NP + current_F* frac_p * N_NP/D_NP
						let current_grades_index=DATA_get_Pa_eq_grade_data_index(Pa_equivalent,RE_grades_data)
						let current_grades_data= RE_grades_data[current_grades_index]
						//element inside a scale module
						if(current_note<0){
							[input_value,data_value]=RE_write_a_no_note(current_note)
						}else{
							//determine if diesis or bemolle
							let [grade,delta,reg]=DATA_calculate_absolute_note_to_grade(current_note,segment_data.sound[index].diesis,current_grades_data)
							//warning first element
							//verify is has module, if not read previous value
							let showreg = true
							if(prev_reg==reg)showreg=false
							prev_reg= reg
							if(prev_scale_index!= current_grades_index){
								// different scales
								input_class="App_RE_note_bordered_cell"
								showreg=true
							}
							prev_scale_index=current_grades_index
							let diesis = ""
							if(delta==1){
								diesis="+"
							}else if(delta>1){
								diesis="+"+(delta)
							}else if(delta==-1){
								diesis="-"
							}else if(delta<-1){
								diesis="-"+Math.abs(delta)
							}
							if(showreg){
								//stringN=grade+diesis+"r"+reg
								input_value=grade+RE_string_to_superscript(diesis)+"r"+reg
							}else{
								//stringN=grade+diesis
								input_value=grade+RE_string_to_superscript(diesis)
							}
						}
					}
					let end_column=""
					if(index_columns[index]==i+1)end_column="App_RE_end_column"
					line_string+= `<input class="App_RE_inp_box App_RE_note_cell ${input_class} ${end_column}" ondragstart="RE_disableEvent(event)"
									onkeypress="APP_inpTest_Ngm_line(event,this)" onkeydown="RE_move_focus(event,this)"
									onfocusin="APP_set_previous_value(this);RE_parent_no_draggable(this);APP_play_input_on_click(this,'N');RE_set_current_position(this);RE_replace_inp_box_superscript(this)"
									onfocusout="RE_parent_draggable(this);RE_enter_new_value_S(this,'Ngm')"
									ondblclick="RE_dbclick_inp_box(this)" onmouseenter="APP_hover_area_enter(this)"
									position="{'sound_index':${sound_index},'segment_index':${segment_index},'voice_id':${voice_id},'column_index':${i}}"
									onmouseleave="APP_hover_area_exit(this)" maxlength="7" placeholder=" " value=${input_value} ${data_value}></input>`
					if(index_columns[index]==i+1)sound_index++
					if(index_columns[index]==i)index++
				}
				line_string+= `<label class="App_RE_inp_box App_RE_segment_last_cell"
									onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)"
									data-value="dotHover">${end_range}</label>
								</div>`
			break;
			case 'iNa':
				sound_line=true
				index=0
				sound_index=0
				first_note=true
				prev_note=null
				line_string+='<div class="App_RE_segment_line App_RE_iNa">'
				for (let i = 0; i < n_column-1; i++) {
					let input_value=""
					let data_value=""
					let bordered=""
					if(index_columns[index]==i){
						let current_note = segment_data.sound[index].note
						if(current_note<0){
							[input_value,data_value]=RE_write_a_no_note(current_note)
						}else {
							if(first_note){
								input_value=current_note
								bordered="App_RE_note_bordered_cell"
								first_note=false
								prev_note=current_note
							}else{
								input_value=current_note-prev_note
								prev_note=current_note
							}
						}
					}
					let end_column=""
					if(index_columns[index]==i+1)end_column="App_RE_end_column"
					line_string+= `<input class="App_RE_inp_box App_RE_note_cell ${bordered} ${end_column}" ondragstart="RE_disableEvent(event)"
										onkeypress="APP_inpTest_iNa_line(event,this)"
										onkeydown="RE_move_focus(event,this)"
										onfocusin="APP_set_previous_value(this);RE_parent_no_draggable(this);APP_play_input_on_click(this,'N');RE_set_current_position(this)"
										onfocusout="RE_parent_draggable(this);RE_enter_new_value_iN(this,'iNa')"
										ondblclick="RE_dbclick_inp_box(this)" onmouseenter="APP_hover_area_enter(this)"
										position="{'sound_index':${sound_index},'segment_index':${segment_index},'voice_id':${voice_id},'column_index':${i}}"
										onmouseleave="APP_hover_area_exit(this)" maxlength="4" placeholder=" " value=${input_value} ${data_value}></input>`
					if(index_columns[index]==i+1)sound_index++
					if(index_columns[index]==i)index++
				}
				line_string+= `<label class="App_RE_inp_box App_RE_segment_last_cell"
									onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)"
									data-value="dotHover">${end_range}</label>`
				//draw iS_lines
				line_string+= `<svg class="App_RE_segment_line_svg App_RE_segment_line_svg_pink" viewBox="0 0 ${n_column*28} 28">`
				prev_col=-1
				line_start=0
				line_end=0
				index_columns.forEach((col,data_position)=>{
					if(prev_col<0 && segment_data.sound[data_position].note>=0){
						prev_col=col
						line_start=col
					}else if(col!=prev_col && col!=n_column-1 && segment_data.sound[data_position].note>=0){
						// let x1=(prev_col+1)*28+14
						// let x2=col*28
						let x2=(col+1)*28-1
						line_string+= `<line x1="${x2}" y1="${tick_y1}" x2="${x2}" y2="${tick_y2}"></line>`
						prev_col=col
						line_end=col
					}
				})
				line_string+= `<line x1="${((line_start+1)*28)}" y1="14" x2="${((line_end+1)*28)}" y2="14"></line>
							</svg></div>`
			break;
			case 'iNm':
				sound_line=true
				index=0
				first_note=true
				prev_note=null
				index=0
				line_string+='<div class="App_RE_segment_line App_RE_iNm">'
				sound_index=0
				for (let i = 0; i < n_column-1; i++) {
					let bordered=""
					let input_value=""
					let data_value=""
					if(index_columns[index]==i){
						let current_note=segment_data.sound[index].note
						if(current_note<0){
							[input_value,data_value]=RE_write_a_no_note(current_note)
						}else{
							if(first_note){
								bordered="App_RE_note_bordered_cell"
								let reg = Math.floor(current_note/TET)
								let resto = current_note%TET
								input_value= resto+"r"+reg
								first_note=false
								prev_note=current_note
							}else{
								let int_n = current_note-prev_note
								let reg = Math.floor(int_n/TET)
								let resto = int_n%TET
								let neg = false
								if(int_n<0){
									neg = true
									resto = Math.abs(resto)
									if(resto!=0){
										reg= Math.abs(reg)-1
									}else{
										reg= Math.abs(reg)
									}
								}
								if(neg){
									input_value="-"+resto//+"r"+reg
								}else{
									input_value=resto//+"r"+reg
								}
								if(reg!=0)input_value=input_value+"r"+reg
								prev_note=current_note
							}
						}
					}
					let end_column=""
					if(index_columns[index]==i+1)end_column="App_RE_end_column"
					line_string+= `<input class="App_RE_inp_box App_RE_note_cell ${bordered} ${end_column}" ondragstart="RE_disableEvent(event)"
									onkeypress="APP_inpTest_iNm_line(event,this)"
									onkeydown="RE_move_focus(event,this)"
									onfocusin="APP_set_previous_value(this);RE_parent_no_draggable(this);APP_play_input_on_click(this,'N');RE_set_current_position(this)"
									onfocusout="RE_parent_draggable(this);RE_enter_new_value_iN(this,'iNm')"
									ondblclick="RE_dbclick_inp_box(this)" onmouseenter="APP_hover_area_enter(this)"
									position="{'sound_index':${sound_index},'segment_index':${segment_index},'voice_id':${voice_id},'column_index':${i}}"
									onmouseleave="APP_hover_area_exit(this)" maxlength="5" placeholder=" " value=${input_value} ${data_value}></input>`
					if(index_columns[index]==i+1)sound_index++
					if(index_columns[index]==i)index++
				}
				line_string+= `<label class="App_RE_inp_box App_RE_segment_last_cell"
								onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)"
								data-value="dotHover">${end_range}</label>`
				//draw iS_lines
				line_string+= `<svg class="App_RE_segment_line_svg App_RE_segment_line_svg_pink" viewBox="0 0 ${n_column*28} 28">`
				prev_col=-1
				line_start=0
				line_end=0
				index_columns.forEach((col,data_position)=>{
					if(prev_col<0 && segment_data.sound[data_position].note>=0){
						prev_col=col
						line_start=col
					}else if(col!=prev_col && col!=n_column-1 && segment_data.sound[data_position].note>=0){
						let x2=(col+1)*28-1
						line_string+= `<line x1="${x2}" y1="${tick_y1}" x2="${x2}" y2="${tick_y2}"></line>`
						prev_col=col
						line_end=col
					}
				})
				line_string+= `<line x1="${((line_start+1)*28)}" y1="14" x2="${((line_end+1)*28)}" y2="14"></line>
							</svg></div>`
			break;
			case 'iNgm':
				sound_line=true
				index=0
				prev_note=null
				let prev_diesis=true
				prev_scale_index=-1
				line_string+='<div class="App_RE_segment_line App_RE_iNgm">'
				let bordered_col=[]
				sound_index=0
				for (let i = 0; i < n_column-1; i++) {
					let input_value=""
					let input_class=""
					let data_value=""
					if(index_columns[index]==i){
						let current_note=segment_data.sound[index].note
						let current_diesis=segment_data.sound[index].diesis
						let current_P=segment_data.time[index].P
						let current_F=segment_data.time[index].F
						//find correct scale
						//find Pa_equivalent and verify if inside a scale
						let current_fraction = segment_data.fraction.find(fraction=>{return fraction.stop>current_P})
						let frac_p = current_fraction.N/current_fraction.D
						let Pa_equivalent = (Psg_segment_start+current_P) * N_NP/D_NP + current_F* frac_p * N_NP/D_NP
						let current_grades_index=DATA_get_Pa_eq_grade_data_index(Pa_equivalent,RE_grades_data)
						let current_grades_data= RE_grades_data[current_grades_index]
						// if(current_scale!=null){
						//element inside a scale module
						if(current_note<0){
							[input_value,data_value]=RE_write_a_no_note(current_note)
						}else{
							let write_iN = true
							if(prev_scale_index!= current_grades_index){
								// different scales
								input_class="App_RE_note_bordered_cell"
								bordered_col.push(i)
								write_iN=false
							}
							prev_scale_index=current_grades_index
							if(write_iN){
								//calculate note interval and make a traduction in current scale
								//not really a string
								input_value= DATA_calculate_grade_interval_string(prev_note,prev_diesis,current_grades_data,current_note,current_diesis,current_grades_data)
							}else{
								//write note in current scale
								let [grade,delta,reg]=DATA_calculate_absolute_note_to_grade(current_note,current_diesis,current_grades_data)
								let diesis = ""
								if(delta==1){
									diesis="+"
								}else if(delta>1){
									diesis="+"+(delta)
								}else if(delta==-1){
									diesis="-"
								}else if(delta<-1){
									diesis="-"+Math.abs(delta)
								}
								input_value= grade+RE_string_to_superscript(diesis)+"r"+reg
							}
							prev_note=current_note
							prev_diesis=current_diesis
						}
					}
					let end_column=""
					if(index_columns[index]==i+1)end_column="App_RE_end_column"
					line_string+= `	<input class="App_RE_inp_box App_RE_note_cell ${input_class} ${end_column}" ondragstart="RE_disableEvent(event)"
									onkeypress="APP_inpTest_iNgm_line(event,this)"
									onkeydown="RE_move_focus(event,this)"
									onfocusin="APP_set_previous_value(this);RE_parent_no_draggable(this);APP_play_input_on_click(this,'N');RE_set_current_position(this);RE_replace_inp_box_superscript(this)"
									onfocusout="RE_parent_draggable(this);RE_enter_new_value_iN(this,'iNgm')"
									ondblclick="RE_dbclick_inp_box(this)" onmouseenter="APP_hover_area_enter(this)"
									position="{'sound_index':${sound_index},'segment_index':${segment_index},'voice_id':${voice_id},'column_index':${i}}"
									onmouseleave="APP_hover_area_exit(this)" maxlength="4" placeholder=" " value=${input_value} ${data_value}></input>`
					if(index_columns[index]==i+1)sound_index++
					if(index_columns[index]==i)index++
				}
				line_string+= `<label class="App_RE_inp_box App_RE_segment_last_cell"
								onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)"
								data-value="dotHover">${end_range}</label>`
				//draw iS_lines
				line_string+= `<svg class="App_RE_segment_line_svg App_RE_segment_line_svg_pink" viewBox="0 0 ${n_column*28} 28">`
				//prev_col=-1
				let prev_bordered_index=0
				let line_x1x2=[]
				if(bordered_col.length==0){
					index_columns.find((col,data_position)=>{
						if(segment_data.sound[data_position].note>=0){
							bordered_col.push(col)
							return true
						}
					})
				}
				index_columns.forEach((col,data_position)=>{
					//if(prev_col<0 && segment_data.sound[data_position].note>=0){
					if(bordered_col[prev_bordered_index]==col){
						//skip printing tick
						line_x1x2.push([col,col])
						prev_bordered_index++;
					//}else if(col!=prev_col && col!=n_column-1 && segment_data.sound[data_position].note>=0){
					}else if(col!=n_column-1 && segment_data.sound[data_position].note>=0){
						let x2=(col+1)*28-1
						line_x1x2[prev_bordered_index-1][1]=col
						line_string+= `<line x1="${x2}" y1="${tick_y1}" x2="${x2}" y2="${tick_y2}"></line>`
					}
				})
				line_x1x2.forEach(x1x2=>{
					let x1=(x1x2[0]+1)*28
					let x2=(x1x2[1]+1)*28
					line_string+= `<line x1="${x1}" y1="14" x2="${x2}" y2="14"></line>`
				})
				line_string+= `</svg></div>`
			break;
			case 'Nabc':
				sound_line=true
				index=0
				sound_index=0
				line_string+='<div class="App_RE_segment_line App_RE_Nabc">'
				for (let i = 0; i < n_column-1; i++) {
					let input_value=""
					let data_value=""
					if(TET==12){
						if(index_columns[index]==i){
							//note
							let current_note = segment_data.sound[index].note
							if(current_note<0){
								[input_value,data_value]=RE_write_a_no_note(current_note)
							}else{
								var resto = current_note%TET
								if(segment_data.sound[index].diesis){
									input_value= ABC_note_list_diesis[resto]
								}else{
									input_value= ABC_note_list_bemolle[resto]
								}
							}
						}
					}else{
						if(index_columns[index]==i){
							input_value= no_value
						}
					}
					let end_column=""
					if(index_columns[index]==i+1)end_column="App_RE_end_column"
					line_string+= `<input class="App_RE_inp_box App_RE_note_cell ${end_column}" ondragstart="RE_disableEvent(event)"
										onkeydown="RE_move_focus(event,this);APP_inpTest_denied(event,this)"
										onfocusin="RE_parent_no_draggable(this);APP_play_input_on_click(this,'N');RE_set_current_position(this)"
										onfocusout="RE_parent_draggable(this)"
										ondblclick="RE_dbclick_inp_box(this)" onmouseenter="APP_hover_area_enter(this)"
										onmouseleave="APP_hover_area_exit(this)"
										position="{'sound_index':${sound_index},'segment_index':${segment_index},'voice_id':${voice_id},'column_index':${i}}"
										placeholder=" " value=${input_value} ${data_value}></input>`
					if(index_columns[index]==i+1)sound_index++
					if(index_columns[index]==i)index++
				}
				line_string+= `<label class="App_RE_inp_box App_RE_segment_last_cell"
									onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)"
									data-value="dotHover">${end_range}</label>
								</div>`
			break;
			case 'Ta':
				sound_index=0
				if(change_dimension && sound_line){
					line_string+='<div class="App_RE_Matrix_Hline"></div>'
					change_dimension=false
				}
				let Ta_array_values=[]
				if(neopulse){
					index_columns.forEach((column_number,index)=>{
						Ta_array_values.push(no_value)
					})
				}else{
					prev_pulse=-1
					index_columns.forEach((column_number,index)=>{
						let string = ""
						if(segment_data.time[index].P!=prev_pulse){
							string+=(segment_data.time[index].P+Psg_segment_start)
							prev_pulse=segment_data.time[index].P
						}
						if(segment_data.time[index].F!=0){
							string+="."+segment_data.time[index].F
						}
						Ta_array_values.push(string)
					})
				}
				line_string+=`<div class="App_RE_segment_line App_RE_Ta">
								<input class="App_RE_inp_box App_RE_time_cell" ondragstart="RE_disableEvent(event)"
									onkeydown="RE_move_focus(event,this);APP_inpTest_denied(event,this)"
									onfocusin="RE_parent_no_draggable(this),RE_set_current_position(this)"
									onfocusout="RE_parent_draggable(this)"
									ondblclick="RE_dbclick_inp_box(this)" onmouseenter="APP_hover_area_enter(this)"
									onmouseleave="APP_hover_area_exit(this)"
									position="{'sound_index':0,'segment_index':${segment_index},'voice_id':${voice_id},'column_index':0}"
									maxlength="4" placeholder=" " value=${Ta_array_values[0]} ></input>`
				index=1
				for (let i = 1; i < n_column-1; i++) {
					let input_value=""
					if(index_columns[index]==i){
						input_value=Ta_array_values[index]
					}
					let end_column=""
					if(index_columns[index]==i+1)end_column="App_RE_end_column"
					//APP_inpTest_Ta_line(event,this)onfocusin="APP_set_previous_value(this)
					line_string+= `	<input class="App_RE_inp_box App_RE_time_cell ${end_column}" ondragstart="RE_disableEvent(event)"
									onkeydown="RE_move_focus(event,this);APP_inpTest_denied(event,this)"
									onfocusin="RE_parent_no_draggable(this),RE_set_current_position(this)"
									onfocusout="RE_parent_draggable(this);"
									ondblclick="RE_dbclick_inp_box(this)" onmouseenter="APP_hover_area_enter(this)"
									onmouseleave="APP_hover_area_exit(this)"
									position="{'sound_index':${sound_index},'segment_index':${segment_index},'voice_id':${voice_id},'column_index':${i}}"
									maxlength="4" placeholder=" " value=${input_value}></input>`
					if(index_columns[index]==i+1)sound_index++
					if(index_columns[index]==i)index++
				}
				line_string+= `<label class="App_RE_inp_box App_RE_segment_last_cell">${Ta_array_values.slice(-1)[0]}</label>
								</div>`
			break;
			case 'Tm':
				if(change_dimension && sound_line){
					line_string+='<div class="App_RE_Matrix_Hline"></div>'
					change_dimension=false
				}
				let Tm_array_values=[]
				let Tm_array_triade=[]
				if(neopulse){
					index_columns.forEach((column_number,index)=>{
						Tm_array_values.push(no_value)
						Tm_array_triade.push("")
					})
				}else{
					let previous_element=null
					let compas_data = DATA_get_compas_sequence()
					let last_compas= compas_data.slice(-1)[0].compas_values
					let compas_length_value = last_compas[0]+last_compas[1]*last_compas[2]
					index_columns.forEach((column_number,index)=>{
						if(Psg_segment_start==null)console.error("Write time element ERROR: not entered segment_starting_point value")
						let Pa_element = Psg_segment_start + segment_data.time[index].P
						if(Pa_element <compas_length_value && compas_length_value!=0){
							//element inside a module
							let string = ""
							let compas_type = compas_data.filter(compas=>{
								return compas.compas_values[0] <= Pa_element
							})
							let current_compas_type = compas_type.pop()
							let current_compas_number = compas_type.reduce((prop,item)=>{
								return item.compas_values[2]+prop
							},0)
							let dP = Pa_element - current_compas_type.compas_values[0]
							let mod = current_compas_type.compas_values[1]
							let dC = Math.floor(dP/mod)
							let resto = dP
							if(dC!=0) resto = dP%mod
							current_compas_number+= dC
							//warning first element
							//verify if it has module, if not read previous value
							let showcompas = true
							let showpulse = true
							//here retrive value
							if(previous_element!=null){
								if(previous_element[0]==resto)showpulse=false
								if(previous_element[1]==current_compas_number)showcompas=false
							}
							if(showpulse || showcompas){
								string = resto
							}
							if(segment_data.time[index].F!=0){
								string += "."+segment_data.time[index].F
							}
							if(showcompas){
								//stringT += "<sup>"+current_compas_number+"</sup>"
								string += RE_string_to_superscript(current_compas_number.toString())
							}
							Tm_array_values.push(string)
							Tm_array_triade.push([resto,segment_data.time[index].F,current_compas_number])
							previous_element=[resto,current_compas_number]
						}else{
							//outside or no compasses
							Tm_array_values.push(no_value)
							Tm_array_triade.push("")
						}
					})
				}
				index=1
				sound_index=0
				line_string+=`<div class="App_RE_segment_line App_RE_Tm">
								<input class="App_RE_inp_box App_RE_time_cell" ondragstart="RE_disableEvent(event)"
									onkeydown="RE_move_focus(event,this);APP_inpTest_denied(event,this)"
									onfocusin="APP_set_previous_value(this);RE_parent_no_draggable(this);RE_set_current_position(this);RE_replace_inp_box_superscript_Tm(this)"
									onfocusout="RE_parent_draggable(this);RE_replace_first_Tm_to_superscript(this);RE_resize_inp_box(this)"
									ondblclick="RE_dbclick_inp_box(this)" onmouseenter="APP_hover_area_enter(this)"
									onmouseleave="APP_hover_area_exit(this)" oninput="" maxlength="10"
									position="{'sound_index':0,'segment_index':${segment_index},'voice_id':${voice_id},'column_index':0}"
									placeholder=" " Tm_triade="${Tm_array_triade[0]}" value=${Tm_array_values[0]} ></input>`
				for (let i = 1; i < n_column-1; i++) {
					let input_value=""
					let input_triade=""
					if(index_columns[index]==i){
						input_value=Tm_array_values[index]
						input_triade=Tm_array_triade[index]
					}
					let end_column=""
					if(index_columns[index]==i+1)end_column="App_RE_end_column"
					line_string+= `	<input class="App_RE_inp_box App_RE_time_cell ${end_column}" ondragstart="RE_disableEvent(event)"
									onkeypress="APP_inpTest_Tm_line(event,this)" onkeydown="RE_move_focus(event,this)"
									onfocusin="APP_set_previous_value(this);RE_parent_no_draggable(this);RE_replace_inp_box_superscript_Tm(this),RE_set_current_position(this)"
									onfocusout="RE_parent_draggable(this);RE_enter_new_value_T(this,'Tm');RE_resize_inp_box(this)"
									ondblclick="RE_dbclick_inp_box(this)" onmouseenter="APP_hover_area_enter(this)"
									onmouseleave="APP_hover_area_exit(this)" oninput="RE_resize_inp_box(this)" maxlength="10"
									position="{'sound_index':${sound_index},'segment_index':${segment_index},'voice_id':${voice_id},'column_index':${i}}"
									placeholder=" " Tm_triade="${input_triade}" value=${input_value}></input>`
					if(index_columns[index]==i+1)sound_index++
					if(index_columns[index]==i)index++
				}
				line_string+= `<input class="App_RE_inp_box App_RE_time_cell App_RE_segment_last_cell App_RE_last_cell_editable_input" ondragstart="RE_disableEvent(event)"
									onkeypress="APP_inpTest_Tm_line(event,this)" onkeydown="RE_move_focus(event,this)"
									onfocusin="APP_set_previous_value(this);RE_parent_no_draggable(this);RE_replace_inp_box_superscript_Tm(this),RE_set_current_position(this)"
									onfocusout="RE_parent_draggable(this);RE_enter_new_value_T(this,'Tm');RE_resize_inp_box(this)"
									ondblclick="RE_dbclick_inp_box(this)" oninput="RE_resize_inp_box(this)" maxlength="10"
									position="{'sound_index':${index},'segment_index':${segment_index},'voice_id':${voice_id},'column_index':${n_column}}"
									placeholder=" " Tm_triade="${Tm_array_triade.slice(-1)[0]}" value=${Tm_array_values.slice(-1)[0]} ></input>
							</div>`
				break;
				case 'Ts':
					prev_pulse=-1
					index=1
					sound_index=0
					if(change_dimension && sound_line){
						line_string+='<div class="App_RE_Matrix_Hline"></div>'
						change_dimension=false
					}
					line_string+=`<div class="App_RE_segment_line App_RE_Ts">
									<input class="App_RE_inp_box App_RE_time_cell" ondragstart="RE_disableEvent(event)"
										onkeydown="RE_move_focus(event,this);APP_inpTest_denied(event,this)"
										onfocusin="APP_set_previous_value(this);RE_parent_no_draggable(this),RE_set_current_position(this)"
										onfocusout="RE_parent_draggable(this)"
										ondblclick="RE_dbclick_inp_box(this)" onmouseenter="APP_hover_area_enter(this)"
										position="{'sound_index':0,'segment_index':${segment_index},'voice_id':${voice_id},'column_index':0}"
										onmouseleave="APP_hover_area_exit(this)" maxlength="4" placeholder=" " value="0"></input>`
					for (let i = 1; i < n_column-1; i++) {
						let input_value=""
						if(index_columns[index]==i){
							let current_P=segment_data.time[index].P
							let current_F=segment_data.time[index].F
							if(current_P!=prev_pulse){
								input_value+=current_P
								prev_pulse=current_P
							}
							if(current_F!=0){
								input_value+="."+current_F
							}
						}
						let end_column=""
						if(index_columns[index]==i+1)end_column="App_RE_end_column"
						line_string+= `<input class="App_RE_inp_box App_RE_time_cell ${end_column}" ondragstart="RE_disableEvent(event)"
											onkeypress="APP_inpTest_Ts_line(event,this)" onkeydown="RE_move_focus(event,this)"
											onfocusin="APP_set_previous_value(this);RE_parent_no_draggable(this),RE_set_current_position(this)"
											onfocusout="RE_parent_draggable(this);RE_enter_new_value_T(this,'Ts')"
											ondblclick="RE_dbclick_inp_box(this)" onmouseenter="APP_hover_area_enter(this)"
											position="{'sound_index':${sound_index},'segment_index':${segment_index},'voice_id':${voice_id},'column_index':${i}}"
											onmouseleave="APP_hover_area_exit(this)" maxlength="4" placeholder=" " value=${input_value}></input>`
						if(index_columns[index]==i+1)sound_index++
						if(index_columns[index]==i)index++
					}
					line_string+= `<input class="App_RE_inp_box App_RE_time_cell App_RE_segment_last_cell App_RE_last_cell_editable_input" ondragstart="RE_disableEvent(event)"
											onkeypress="APP_inpTest_Ts_line(event,this)" onkeydown="RE_move_focus(event,this)"
											onfocusin="APP_set_previous_value(this);RE_parent_no_draggable(this),RE_set_current_position(this)"
											onfocusout="RE_parent_draggable(this);RE_enter_new_value_T(this,'Ts')"
											position="{'sound_index':${index},'segment_index':${segment_index},'voice_id':${voice_id},'column_index':${n_column}}"
											ondblclick="RE_dbclick_inp_box(this)" maxlength="4" placeholder=" " value="${segment_data.time.slice(-1)[0].P}"></input>
									</div>`
				break;
				case 'iT':
					index=0
					//attention use insertion index instead of sound index!
					sound_index=0
					if(change_dimension && sound_line){
						line_string+='<div class="App_RE_Matrix_Hline"></div>'
						change_dimension=false
					}
					line_string+='<div class="App_RE_segment_line App_RE_iT">'
					for (let i = 0; i < n_column-1; i++) {
						let input_value=""
						if(index_columns[index]+1==i){
							let A_element = segment_data.time[index]
							let B_element = segment_data.time[index+1]
							//find fraction
							let current_fraction = segment_data.fraction.find(fraction=>{return fraction.stop>A_element.P})
							input_value= DATA_calculate_interval_PA_PB(A_element,B_element,current_fraction)
						}
						let end_column=""
						if(input_value==""){
							if(index_columns[index]==(i+1))end_column="App_RE_end_column"
						}else{
							if(index_columns[index+1]==(i+1))end_column="App_RE_end_column"
						}
						line_string+= `<input class="App_RE_inp_box App_RE_time_cell ${end_column}" ondragstart="RE_disableEvent(event)"
										onkeypress="APP_inpTest_iT_line(event,this)"
										onkeydown="RE_move_focus(event,this)"
										onfocusin="APP_set_previous_value(this);RE_parent_no_draggable(this),RE_set_current_position(this)"
										onfocusout="RE_parent_draggable(this);RE_enter_new_value_dT(this,'iT')"
										ondblclick="RE_dbclick_inp_box(this)" onmouseenter="APP_hover_area_enter(this)"
										position="{'sound_index':${sound_index},'segment_index':${segment_index},'voice_id':${voice_id},'column_index':${i},'insertion_index':${index}}"
										onmouseleave="APP_hover_area_exit(this)" maxlength="3" placeholder=" " value=${input_value}></input>`
						if(end_column!="")sound_index++
						if(index_columns[index]+1==i)index++
					}
					line_string+= `<label class="App_RE_inp_box App_RE_time_cell App_RE_segment_last_cell"
										onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)"
										data-value="dotHover"></label>`
					//*/draw it_lines
					line_string+= `<svg class="App_RE_segment_line_svg App_RE_segment_line_svg_yellow" viewBox="0 0 ${n_column*28} 28">`
					prev_col=0
					line_string+= `<line x1="14" y1="14" x2="${n_column*28-14}" y2="14"></line>
									<line x1="14" y1="${tick_y1}" x2="14" y2="${tick_y2}"></line>`
					index_columns.forEach(col=>{
						if(col!=prev_col){
							let x1=(prev_col+2)*28
							let x2=(col+1)*28-14
							// line_string+= `<line x1="${x1}" y1="14" x2="${x2}" y2="14"></line>
							// 			<line x1="${x2}" y1="10" x2="${x2}" y2="18"></line>`
							line_string+= `<line x1="${x2}" y1="${tick_y1}" x2="${x2}" y2="${tick_y2}"></line>`
							prev_col=col
						}
					})
					line_string+= `</svg></div>`//*/
				break;
				case 'Tf':
					index=0
					if(change_dimension && sound_line){
						line_string+='<div class="App_RE_Matrix_Hline"></div>'
						change_dimension=false
					}
					line_string+='<div class="App_RE_segment_line App_RE_Tf">'
					let max_fraction_index = segment_data.fraction.length-1
					segment_data.fraction.forEach((fraction,fraction_index)=>{
						//change length  arrow
						//calculate times
						let time_fraction_start= DATA_calculate_exact_time(Psg_segment_start,fraction.start,0,1,1,N_NP,D_NP)
						let time_fraction_end= DATA_calculate_exact_time(Psg_segment_start,fraction.stop,0,1,1,N_NP,D_NP)
						//look how much elements there are in global time
						let index_start = RE_find_time_column_index(time_fraction_start,partial_RE_global_time_all_changes,partial_RE_global_time_segment_change)
						let index_end= RE_find_time_column_index(time_fraction_end,partial_RE_global_time_all_changes,partial_RE_global_time_segment_change)
						//set width arrow
						let arrow_length = (index_end-index_start)-1
						if(fraction_index==max_fraction_index){
							arrow_length--
						}
						let disabled=''
						if(fraction_index==max_fraction_index)disabled='disabled="true"'
						line_string+= `<input class="App_RE_inp_box" ondragstart="RE_disableEvent(event)"
											onkeypress="APP_inpTest_Fr(event,this)"
											onkeydown="RE_move_focus(event,this)" onfocusin="APP_set_previous_value(this)"
											onfocusout="RE_enter_new_fraction(this)" oninput="APP_stop()" maxlength="5" value="${fraction.N}/${fraction.D}"></input>
										<button class="App_RE_frac_arrow_button" onclick="RE_break_fraction_range(this)" onmouseleave="RE_exit_break_fraction_range(this)" onmousemove="RE_break_fraction_range_calculate_position(this,event)"
											index=${arrow_length} style="--index: ${arrow_length};">
											<img src="./Icons/bolt_yellow.svg" />
											<svg class="arrowline" data-name="Trazado 10" viewBox="0 0 28 28"
												preserveAspectRatio="none">
												<line x1="0" y1="14" x2="28" y2="14"></line>
											</svg>
											<svg class="arrowwings" data-name="Trazado 10" viewBox="0 0 28 28">
												<line x1="27" y1="14" x2="23" y2="10"></line>
												<line x1="27" y1="14" x2="23" y2="18"></line>
											</svg>
										</button>
										<button class="App_RE_frac_join_button" onclick="RE_join_fraction_ranges(this)" ${disabled}>«
										</button>`
					})
					line_string+='</div>'
				break;
				default:
					console.error(`Line ${item} not found.`);
				break;
			}
		})
	return line_string
}

//copy WD
function RE_write_a_no_note(int_n){
	let stringN="Err"
	let data_value=""
	if (int_n ==-1){
		//silence
		stringN="s"
		data_value= 'data-value="sHover"'
	} else if (int_n ==-2){
		//un-determined element
		stringN="e"
		data_value= 'data-value="eHover"'
	} else if (int_n ==-3){
		stringN=tie_intervals
		data_value= 'data-value="LHover"'
	} else if (int_n ==-4){
		//ending line
		stringN=end_range
		data_value= 'data-value="dotHover"'
	}
	return [stringN,data_value]
}




































