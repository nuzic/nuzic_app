//Loading the soundfont......
// const SF_NAME = "Nuzic_sf_02.2.sf2"
// const file_size=149451044 //found with  wc -c Nuzic_sf_02.2.sf2
// const SF_NAME = "Nuzic_sf_02.3.sf2"
// const file_size=149451352 //found with  wc -c Nuzic_sf_02.3.sf2
//new notes and replace metronome sounds
const SF_NAME = "Nuzic_sf_02.5.sf2"
const file_size=153978008 //found with  wc -c Nuzic_sf_02.5.sf2
//NUZIC sounds 128 / 0		nzc inst 128  nzc_note [23...84]
//Drumkit plus
//NUZIC sounds 128 / 1		nzc inst 129  nzc_note [23...75]
//TR-808
//NUZIC sounds 128 / 2		nzc inst 130
//pulse			midi 36		nzc_note 24
//accent		midi 37		nzc_note 25
//accent weak	midi 38		nzc_note 26
//tic			midi 39		nzc_note 27
//Metronome 1	midi 40		nzc_note 28
//Metronome 2	midi 41		nzc_note 29
//Metronome 3	midi 42		nzc_note 30
//Metronome 4	midi 43		nzc_note 31

const SF_URL = "https://cdn.nuzic.org/Notes/Soundfont/"
// IndexedDB stuff
const dbName = "Nuzic_sf_db"
const objectStoreName = "soundFontNuzic"
let soundFontBuffer

function initDatabase(callback) {
	const request = indexedDB.open(dbName, 1)
	request.onsuccess = () => {
		const db = request.result
		callback(db)
	}
	request.onupgradeneeded = (event) => {
		const db = event.target.result
		db.createObjectStore(objectStoreName, { keyPath: "id" })
	}
}
async function loadLastSoundFontFromDatabase(){
	return await new Promise(resolve => {
		// fetch from db
		initDatabase(db => {
			const transaction = db.transaction([objectStoreName], "readonly")
			const objectStore = transaction.objectStore(objectStoreName)
			const request = objectStore.get("buffer")
			request.onerror = e => {
				console.error("Database error")
				throw e
			}
			request.onsuccess = async () => {
				const result = request.result
				if(!result){
					resolve(undefined)
					return
				}
				if(result.SF_NAME!=SF_NAME){
					resolve(undefined)
					return
				}
				resolve(result.data)
			}
		})
	})
}

async function saveSoundFontToIndexedDB(arr){
	initDatabase(db => {
		const transaction = db.transaction([objectStoreName], "readwrite")
		const objectStore = transaction.objectStore(objectStoreName)
		try {
			const request = objectStore.put({ id: "buffer", data: arr ,SF_NAME})
			request.onsuccess = () => {
				console.log("SoundFont stored successfully")
				APP_loader_percentage_sf(100,10)
			}
			request.onerror = e => {
				console.error("Error saving soundfont", e)
			}
		}
		catch (e){
			console.error("Failed saving soundfont:")
		}
	})
}

// attempt to load soundfont from indexed db
async function demoInit(){
	soundFontBuffer = await loadLastSoundFontFromDatabase()
	let loadedFromDb = true;
	if (soundFontBuffer === undefined){
		//loading first time
		loadedFromDb = false
		console.log("Loading bundled soundfont...")
		soundFontBuffer = await fetchFont(`${SF_URL}${SF_NAME}`, percent =>{
			//console.log(`Loading bundled SoundFont.. ${percent}%`)
			APP_loader_percentage_sf(percent,0)
		})
	}else{
		APP_loader_percentage_sf(100,0)
	}
	if(!loadedFromDb) {
		console.log("Saving soundfont to the browser...")
		await saveSoundFontToIndexedDB(soundFontBuffer);
	}
}

async function fetchFont(url, callback){
	let response = await fetch(url)
	if(!response.ok){
		console.log("Error downloading soundfont!")
		throw response
	}
	let size = response.headers.get("content-length") //dont workwith cors
	if(size==null)size = file_size //manually
	let reader = await (await response.body).getReader()
	let done = false
	let dataArray = new Uint8Array(parseInt(size))
	let offset = 0
	do{
		let readData = await reader.read()
		if(readData.value) {
			dataArray.set(readData.value, offset)
			offset += readData.value.length
		}
		done = readData.done
		let percent = Math.round((offset / size) * 100)
		callback(percent)
	}while(!done)
	return dataArray.buffer
}

demoInit().then(() => {
	let audioContextSF
	try {
		const context = window.AudioContext || window.webkitAudioContext
		//audioContextSF = new context({ sampleRate: 44100 })
		audioContextSF = new context({ sampleRate: 48000 }) //this one
		//audioContextSF = new AudioContext({ sampleRate: 48000 }) //== at the first
	}
	catch (e) {
		console.log("Your browser doesn't support WebAudio.")
		throw e
	}
	if(audioContextSF.state !== "running"){
		document.addEventListener("mousedown", () => {
			if(audioContextSF.state !== "running") {
				audioContextSF.resume().then()
			}
		})
	}
	// prepare midi interface
	console.log("Initializing synthesizer...")
	//change audiocontext connections
	SF_manager = new Manager(audioContextSF,soundFontBuffer)
	return
	//load new SF
	//<input type="file" accept=".sf2,.sf3" id="sf_file_input"> //no need
	//let sfInput = document.getElementById("sf_file_input")
	/*
	sfInput.onchange = async e => {
		if (!e.target.files[0]) {
			return;
		}
		const file = e.target.files[0]
		document.getElementById("sf_upload").firstElementChild.innerText = file.name
		console.log("Parsing SoundFont...")
		// parse the soundfont
		let soundFontBuffer
		try {
			soundFontBuffer = await file.arrayBuffer()
			window.soundFontParser = soundFontBuffer
		}
		catch (e) {
			showNotification(
				manager.localeManager.getLocaleString("locale.warnings.warning"),
				manager.localeManager.getLocaleString("locale.warnings.outOfMemory")
			);
			throw e
		}
		window.manager.sfError = e => {
			console.log(`Error parsing soundfont: <pre style='font-family: monospace; font-weight: bold'>${e}</pre>`)
			console.log(e)
		}
		await window.manager.reloadSf(soundFontBuffer)
		if(window.manager.seq)
		{
			window.manager.seq.currentTime -= 0.1
		}
		await saveSoundFontToIndexedDB(soundFontBuffer)
	}
	*/
})

const RENDER_AUDIO_TIME_INTERVAL = 500;

export class Manager {
	//Creates a new midi user interface.
	constructor(context,soundFontBuffer) {
		this.context = context
		let SF_destination = context.createMediaStreamDestination();
		let SF_source= context.createMediaStreamSource(SF_destination.stream)
		SF_source.connect(context.destination)
		this.SF_destination= SF_destination
		this.SF_source= SF_source
		let solve
		this.ready = new Promise(resolve => solve = resolve)
		this.soundFontBuffer = soundFontBuffer
		this.initializeContext(context, SF_destination,soundFontBuffer).then(() => {
			solve()
		})
	}
	/*
	async renderAudio(callback=undefined){
		if(!this.seq){
			throw new Error("No sequencer active")
		}
		const parsedMid = this.seq.midiData;
		// prepare audio context
		const offline = new OfflineAudioContext({
			numberOfChannels: 2,
			sampleRate: this.context.sampleRate,
			length: this.context.sampleRate * (parsedMid.duration + 1)
		});
		const workletURL = new URL("./spessasynth_lib/synthetizer/worklet_system/worklet_processor.js", import.meta.url).href
		//await offline.audioWorklet.addModule(workletURL)
		await offline._audioWorklet.addModule(workletURL)
		const snapshot = await this.synth.getSynthesizerSnapshot()
		let synth
		try{
			synth = new Synthetizer(offline.destination, this.soundFontBuffer, false, {
				parsedMIDI: parsedMid,
				snapshot: snapshot
			}, {
				reverbEnabled: true,
				chorusEnabled: true,
				chorusConfig: undefined,
				reverbImpulseResponse: this.impulseResponse
			})
		}
		catch (e) {
			showNotification(this.localeManager.getLocaleString("locale.warnings.warning"), this.localeManager.getLocaleString("locale.warnings.outOfMemory"))
			throw e
		}
		if(callback){
			const RATI_SECONDS = RENDER_AUDIO_TIME_INTERVAL / 1000;
			let rendered = synth.currentTime
			const interval = setInterval(() => {
				let hasRendered = synth.currentTime - rendered
				rendered = synth.currentTime
				callback(synth.currentTime / parsedMid.duration, hasRendered / RATI_SECONDS)
			}, RENDER_AUDIO_TIME_INTERVAL)
			const buf = await offline.startRendering()
			clearInterval(interval)
			return buf
		}else{
			return offline.startRendering()
		}
	}*/
	sfError
	async initializeContext(context , SF_destination, soundFontBuffer) {
		if(!context.audioWorklet){
			alert("Audio worklet is not supported on your browser. Sorry!")
			throw "Not supported."
		}
		const impulseURL = new URL(SF_lib.impulse_response.flac, import.meta.url);
		const response = await fetch(impulseURL)
		const data = await response.arrayBuffer()
		this.impulseResponse = await context.decodeAudioData(data)
		const workletURL = new URL("https://static.nuzic.org/Lib/SF_lib/SF_worklet.min.js?nocache=8", import.meta.url).href
		//local
		// console.error("worklet local")
		//const workletURL = new URL("./SF_worklet.min.js?=nocache=2", import.meta.url).href
		if (context.audioWorklet) {
			context.audioWorklet.addModule(workletURL)
			.then(() => {
				console.log("Worklet added successfully")
				// Set up the synthesizer
				this.synth = new SF_lib.Synthetizer(
					//context.destination,
					SF_destination,
					this.soundFontBuffer,
					undefined,
					undefined,
					{
						chorusEnabled: true,
						chorusConfig: undefined,
						reverbImpulseResponse: this.impulseResponse,
						reverbEnabled: true,
					}
				)
				// Register event handlers
				this.synth.eventHandler.addEvent("soundfonterror", "manager-sf-error", (e) => {
					if (this.sfError) {
						this.sfError(e);
					}
				})
				// this.synth.eventHandler.addEvent("soundfontready", (e) => {
				// 	console.log("ready")
				// })
				// Wait for synthesizer to be ready
				this.synth.isReady.then(() => {
					APP_loader_percentage_sf(100,10)
					APP_ready_soundFont()
					APP_updateTime()
				})
			})
			.catch((err) => {
				console.error("Error adding worklet or initializing synthesizer:", err);
			})
		}
	}
	async reloadSf(sf){
		//this.soundFontMixer.soundFontChange(sf)
		await this.synth.reloadSoundFont(sf)
	}
}

