//LINE OPERATIONS

function CALC_line_sum(line_index, value, abs){
	var data=CALC_get_current_state_point()
	var content = Array.from(data[line_index].content)
	if(content.length==0)return false
	var used_value=value
	if(abs){
		used_value=Math.abs(value)
		value="|"+value+"|"
	}
	var new_content = []
	content.forEach(el=>{
		new_content.push(el+used_value)
	})
	var new_name=CALC_new_line_name()

	var new_line = CALC_generate_generic_line_data(new_content,new_name,'calc_sum',[CALC_line_name_to_text(data[line_index].name),value],[data[line_index].name])
	data.push(new_line)
	data[line_index].children.push(new_name)
	//select first line bf saving
	var left_line = document.querySelector('.App_BNav_calc_lines_container').children[line_index]
	CALC_select_line(left_line)
	CALC_insert_new_state_point(data)
	CALC_write_line_from_data(new_line,data.length-1)
	return true
}

function CALC_two_lines_sum(index1,index2,abs){
	var data=CALC_get_current_state_point()
	var content1 = Array.from(data[index1].content)
	var content2 = Array.from(data[index2].content)
	if(content1.length==0 && content2.length==0)return false
	if(content1.length<content2.length){
		for(i=0;content1.length<content2.length;i++){
			content1.push(0)
		}
	}
	if(content2.length<content1.length){
		for(i=0;content2.length<content1.length;i++){
			content2.push(0)
		}
	}
	var new_content = []
	if(abs){
		content1.forEach((element,index)=>{
			new_content.push(element+Math.abs(content2[index]))
		})
	}else{
		content1.forEach((element,index)=>{
			new_content.push(element+content2[index])
		})
	}

	var new_name=CALC_new_line_name()
	var new_line = CALC_generate_generic_line_data(new_content,new_name,'calc_sum',[CALC_line_name_to_text(data[index1].name),CALC_line_name_to_text(data[index2].name)],[data[index1].name,data[index2].name])
	data.push(new_line)
	data[index1].children.push(new_name)
	data[index2].children.push(new_name)
	//select first line bf saving
	//CALC_clear_selection()
	var left_line1 = document.querySelector('.App_BNav_calc_lines_container').children[index1]
	CALC_select_line(left_line1)
	CALC_insert_new_state_point(data)
	CALC_write_line_from_data(new_line,data.length-1)
	return true
}

function CALC_line_subtraction(line_index, value,abs){
	var data=CALC_get_current_state_point()
	var content = Array.from(data[line_index].content)
	if(content.length==0)return false
	var new_content = []
	var used_value=value
	if(abs){
		used_value=Math.abs(value)
		value="|"+value+"|"
	}
	content.forEach(el=>{
		new_content.push(el-used_value)
	})
	var new_name=CALC_new_line_name()
	var new_line = CALC_generate_generic_line_data(new_content,new_name,'calc_sub',[CALC_line_name_to_text(data[line_index].name),value],[data[line_index].name])
	data.push(new_line)
	data[line_index].children.push(new_name)
	//select first line bf saving
	var left_line = document.querySelector('.App_BNav_calc_lines_container').children[line_index]
	CALC_select_line(left_line)
	CALC_insert_new_state_point(data)
	CALC_write_line_from_data(new_line,data.length-1)
	return true
}

function CALC_two_lines_subtraction(index1,index2,abs){
	var data=CALC_get_current_state_point()
	var content1 = Array.from(data[index1].content)
	var content2 = Array.from(data[index2].content)
	if(content1.length==0&&content2.length==0)return false
	if(content1.length<content2.length){
		for(i=0;content1.length<content2.length;i++){
			content1.push(0)
		}
	}
	if(content2.length<content1.length){
		for(i=0;content2.length<content1.length;i++){
			content2.push(0)
		}
	}
	var new_content = []
	if(abs){
		content1.forEach((element,index)=>{
			//var result = element-Math.abs(content2[index])
			//if(result<0)result=0
			//new_content.push(result)
			new_content.push(element-Math.abs(content2[index]))
		})
	}else{
		content1.forEach((element,index)=>{
			//var result = element-content2[index]
			//if(result<0)result=0
			//new_content.push(result)
			new_content.push(element-content2[index])
		})
	}

	var new_name=CALC_new_line_name()
	var new_line = CALC_generate_generic_line_data(new_content,new_name,'calc_sub',[CALC_line_name_to_text(data[line_index].name),CALC_line_name_to_text(data[index2].name)],[data[index1].name,data[index2].name])
	data.push(new_line)
	data[index1].children.push(new_name)
	data[index2].children.push(new_name)
	//select first line bf saving
	var left_line1 = document.querySelector('.App_BNav_calc_lines_container').children[index1]
	CALC_select_line(left_line1)
	CALC_insert_new_state_point(data)
	CALC_write_line_from_data(new_line,data.length-1)
	return true
}

function CALC_line_rotation(line_index,rotation){
	var data=CALC_get_current_state_point()
	var new_content = Array.from(data[line_index].content)
	if(new_content.length<=1)return false
	for(i=0;i<rotation;i++){
		var el = new_content.shift()
		new_content.push(el)
	}
	var new_name=CALC_new_line_name()
	var new_line = CALC_generate_generic_line_data(new_content,new_name,'calc_rot',[CALC_line_name_to_text(data[line_index].name),rotation],[data[line_index].name])
	data.push(new_line)
	data[line_index].children.push(new_name)
	//select first line bf saving
	var left_line = document.querySelector('.App_BNav_calc_lines_container').children[line_index]
	CALC_select_line(left_line)
	CALC_insert_new_state_point(data)
	CALC_write_line_from_data(new_line,data.length-1)
	return true
}

function CALC_line_horizontal_mirror(line_index){
	var data=CALC_get_current_state_point()
	var content = Array.from(data[line_index].content)
	if(content.length<=1)return false
	var new_content = []
	for(i=0;i<content.length;i++){
		new_content.unshift(content[i])
	}
	var new_name=CALC_new_line_name()
	var new_line = CALC_generate_generic_line_data(new_content,new_name,'calc_h_mirror',[CALC_line_name_to_text(data[line_index].name)],[data[line_index].name])
	data.push(new_line)
	data[line_index].children.push(new_name)
	//select first line bf saving
	var left_line = document.querySelector('.App_BNav_calc_lines_container').children[line_index]
	CALC_select_line(left_line)
	CALC_insert_new_state_point(data)
	CALC_write_line_from_data(new_line,data.length-1)
	return true
}
/*
function CALC_line_vertical_mirror_0(line_index){
	var data=CALC_get_current_state_point()
	var content = Array.from(data[line_index].content)
	if(content.length==0)return false
	var new_content = []
	content.forEach(el=>{
		new_content.push(el*-1)
	})
	var new_name=CALC_new_line_name()
	var new_line = CALC_generate_generic_line_data(new_content,new_name,'calc_v_mirror',[CALC_line_name_to_text(data[line_index].name)],[data[line_index].name])
	data.push(new_line)
	data[line_index].children.push(new_name)
	//select first line bf saving
	var left_line = document.querySelector('.App_BNav_calc_lines_container').children[line_index]
	CALC_select_line(left_line)
	CALC_insert_new_state_point(data)
	CALC_write_line_from_data(new_line,data.length-1)
	return true
}*/

function CALC_line_vertical_mirror(line_index, value){
	var data=CALC_get_current_state_point()
	var content = Array.from(data[line_index].content)
	if(content.length==0)return false
	var new_content = []
	content.forEach(el=>{
		new_content.push(2*value-el)
	})
	var new_name=CALC_new_line_name()
	var new_line = CALC_generate_generic_line_data(new_content,new_name,'calc_v_mirror',[CALC_line_name_to_text(data[line_index].name),value],[data[line_index].name])
	data.push(new_line)
	data[line_index].children.push(new_name)
	//select first line bf saving
	var left_line = document.querySelector('.App_BNav_calc_lines_container').children[line_index]
	CALC_select_line(left_line)
	CALC_insert_new_state_point(data)
	CALC_write_line_from_data(new_line,data.length-1)
	return true
}

function CALC_lines_merge(line_index1,line_index2){
	var data=CALC_get_current_state_point()
	var new_content = Array.from(data[line_index1].content)
	var content2 = Array.from(data[line_index2].content)
	if(new_content.length==0 || content2.length==0)return false
	content2.forEach(el=>{
		new_content.push(el)
	})
	var new_name=CALC_new_line_name()
	var new_line = CALC_generate_generic_line_data(new_content,new_name,'calc_merge',[CALC_line_name_to_text(data[line_index1].name),CALC_line_name_to_text(data[line_index2].name)],[data[line_index1].name,data[line_index2].name])
	data.push(new_line)
	data[line_index1].children.push(new_name)
	data[line_index2].children.push(new_name)
	//select first line bf saving
	var left_line = document.querySelector('.App_BNav_calc_lines_container').children[line_index1]
	CALC_select_line(left_line)
	CALC_insert_new_state_point(data)
	CALC_write_line_from_data(new_line,data.length-1)
	return true
}

function CALC_line_split(line_index,index_cut,extract){
	//extract ==
	//0 LEFT
	//1 RIGHT
	//2 BOTH
	var data=CALC_get_current_state_point()
	var content = Array.from(data[line_index].content)
	if(content.length==0 || content.length<index_cut)return false
	var new_content_L = new_content1=content.slice(0,index_cut)
	var new_content_R = new_content=content.slice(index_cut,content.length+1)
	var new_name=CALC_new_line_name()
	var new_line_L=null
	var new_line_R=null
	if(extract==0 || extract==2){
		//Left
		new_line_L = CALC_generate_generic_line_data(new_content_L,new_name,'calc_split',[CALC_line_name_to_text(data[line_index].name),"·"+index_cut],[data[line_index].name])
		data.push(new_line_L)
		data[line_index].children.push(new_name)
		//in case of second line
		new_name++
	}
	if(extract==1 || extract==2){
		//RIGHT
		new_line_R = CALC_generate_generic_line_data(new_content_R,new_name,'calc_split',[CALC_line_name_to_text(data[line_index].name),index_cut+"·"],[data[line_index].name])
		data.push(new_line_R)
		data[line_index].children.push(new_name)
	}
	//select first line bf saving
	var left_line = document.querySelector('.App_BNav_calc_lines_container').children[line_index]
	CALC_select_line(left_line)
	CALC_insert_new_state_point(data)
	//CALC_write_line_from_data(new_line,data.length-1)
	if(extract==2){
		CALC_write_line_from_data(new_line_L,data.length-2)
		CALC_write_line_from_data(new_line_R,data.length-1)
	}else{
		if(new_line_R!=null)CALC_write_line_from_data(new_line_R,data.length-1)
		if(new_line_L!=null)CALC_write_line_from_data(new_line_L,data.length-1)
	}
	return true
}

function CALC_line_repeat(line_index,value){
	var data=CALC_get_current_state_point()
	var content = Array.from(data[line_index].content)
	if(content.length==0)return false
	var new_content = []
	for(let i=0;i<value;i++){
		new_content.push(...content)
	}

	var new_name=CALC_new_line_name()
	var new_line = CALC_generate_generic_line_data(new_content,new_name,'calc_repeat',[CALC_line_name_to_text(data[line_index].name),value],[data[line_index].name])
	data.push(new_line)
	data[line_index].children.push(new_name)
	//select first line bf saving
	var left_line = document.querySelector('.App_BNav_calc_lines_container').children[line_index]
	CALC_select_line(left_line)
	CALC_insert_new_state_point(data)
	CALC_write_line_from_data(new_line,data.length-1)
	return true
}

function CALC_line_random_from_line(line_index, n_items){
	var data=CALC_get_current_state_point()
	var content = Array.from(data[line_index].content)
	var new_content = []
	if(content.length==0)return false
	for(let i=0;i<n_items;i++){
		new_content.push(content[Math.floor(Math.random()*content.length)])
	}
	var new_name=CALC_new_line_name()
	var new_line = CALC_generate_generic_line_data(new_content,new_name,'calc_generate_random',[CALC_line_name_to_text(data[line_index].name),n_items],[data[line_index].name])
	data.push(new_line)
	data[line_index].children.push(new_name)
	//select first line bf saving
	var left_line = document.querySelector('.App_BNav_calc_lines_container').children[line_index]
	CALC_select_line(left_line)
	CALC_insert_new_state_point(data)
	CALC_write_line_from_data(new_line,data.length-1)
	return true
}

function CALC_line_random_from_range(min_value,max_value,n_items){
	if(min_value>max_value){
		var temp =max_value
		max_value=min_value
		min_value=temp
	}
	if(min_value==max_value || n_items==0)return false //already verified n_items!=0
	var new_content=[]
	for(let i=0;i<n_items;i++){
		new_content.push(Math.floor(Math.random()*(max_value+1-min_value))+min_value)
	}

	var data=CALC_get_current_state_point()
	var new_name=CALC_new_line_name()
	var new_line = CALC_generate_generic_line_data(new_content,new_name,'calc_generate_random',[min_value,max_value,n_items])
	data.push(new_line)
	//select first line bf saving
	CALC_insert_new_state_point(data)
	CALC_write_line_from_data(new_line,data.length-1)
	return true
}

function CALC_line_random_shuffle(line_index){
	var data=CALC_get_current_state_point()
	var new_content = Array.from(data[line_index].content)
	if(new_content.length==0)return false
	new_content.sort(() =>Math.random() - 0.5)
	var new_name=CALC_new_line_name()
	var new_line = CALC_generate_generic_line_data(new_content,new_name,'calc_generate_random',[CALC_line_name_to_text(data[line_index].name)],[data[line_index].name])
	data.push(new_line)
	data[line_index].children.push(new_name)
	//select first line bf saving
	var left_line = document.querySelector('.App_BNav_calc_lines_container').children[line_index]
	CALC_select_line(left_line)
	CALC_insert_new_state_point(data)
	CALC_write_line_from_data(new_line,data.length-1)
	return true
}

//TRANSLATION AND FORCING 
