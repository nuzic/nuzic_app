function RE_enter_new_value_iA(element,type){
	let string = element.value
	if(string==previous_value)return
	APP_stop()
	if (string==tie_intervals_m){
		//tie 2 intervals
		string=tie_intervals
		element.value=tie_intervals
	}
	//find info list ???
	let info_list=RE_find_voice_elements_info_from_iA_element(element)
	let position=[...element.parentNode.querySelectorAll("input")].indexOf(element)
	//modify existing Note
	if(string==""){
		DATA_delete_object_index(info_list.up.voice_id,null,info_list.up.abs_position)
		RE_focus_on_object_index_iA(info_list.down.voice_id,position,0)
		return
	}
	if (string=="s"){
		//silence
		let success = DATA_modify_object_index_sound(info_list.up.voice_id,null, info_list.up.abs_position, -1,null,null,null)
		if(!success){
			element.value=previous_value
			APP_blink_error(element)
			return
		}console.log("hi")
		RE_focus_on_object_index_iA(info_list.down.voice_id,position,1)
		return
	}
	//if(string==no_value || string == "e") {
	if(string == "e") {
		//enter e element in upper voice
		let success = DATA_modify_object_index_sound(info_list.up.voice_id,null, info_list.up.abs_position, -2,null,null,null)
		if(!success){
			element.value=previous_value
			APP_blink_error(element)
			return
		}
		RE_focus_on_object_index_iA(info_list.down.voice_id,position,1)
		return
	}
	if (string==tie_intervals){
		//enter L element in upper voice
		let success = DATA_modify_object_index_sound(info_list.up.voice_id,null, info_list.up.abs_position, -3,null,null,null)
		if(!success){
			element.value=previous_value
			APP_blink_error(element)
			return
		}
		RE_focus_on_object_index_iA(info_list.down.voice_id,position,1)
		return
	}
	//translate element.value to N element absolute (difference bw 2 notes)
	let delta_note = null
	let diesis=null
	//case it is a note
	switch(type) {
	case "iAa":
		// Abs
		delta_note = Math.floor(string)
		if(isNaN(delta_note)){
			delta_note = null
			break
		}
		break;
	case "iAm":
		//verify if is negative
		let iAm_pos= 1
		if (string[0]=="-"){
			iAm_pos=-1
		}
		//verify if has module
		let iAm_split = element.value.split('r')
		let iAm_resto = Math.floor(iAm_split[0])
		// code for different ranges and microtones XXX
		let iAm_reg=0
		if(iAm_split[1]>=0 && iAm_split[1]!=""){
			iAm_reg=Math.floor(iAm_split[1])
		}
		if(!isNaN(iAm_resto)){
			delta_note = iAm_resto + iAm_reg*TET*iAm_pos
		}else{
			delta_note = null
		}
		break;
	case "iAgm":
		// grade mod
		//find correct scale (it is the same up and down)
		let current_scale = info_list.up.info.scale
		if(current_scale==null){
			note_number = null
			break
		}
		//like iNgm is a simple number
		let iAgm_delta_note_grade = Math.floor(string)
		//changing this values in note_number
		//find previous note
		//let down_Na=info_list.down.nzc_note
		let down_sound=info_list.down.info.sound
		let idea_scale_list=DATA_get_idea_scale_list()
		let [current_grade,current_delta,current_reg]=DATA_calculate_absolute_note_to_scale(down_sound.note,down_sound.diesis,current_scale,idea_scale_list)
		current_grade+=iAgm_delta_note_grade
		//doesn't matter if reg doesnt change
		diesis=down_sound.diesis
		let result = DATA_calculate_scale_note_to_absolute(current_grade,0,current_reg,current_scale,idea_scale_list)
		if(result.note==null){
			delta_note = null
		}else{
			if(result.outside_range){
				//force delta note outside to gen warning
				if(result.note==0){
					delta_note = result.note-down_sound.note-1
				}else{
					delta_note = result.note-down_sound.note+1
				}
			}else{
				delta_note = result.note-down_sound.note
			}
		}
		break;
	default:
		console.error("Error reading case N writing roules")
		delta_note=null
	}
	if(delta_note==null){
		console.error("i don't understand what you wrote")
		element.value=previous_value
		APP_blink_error(element)
		return
	}
	let new_upper_note=info_list.down.nzc_note+delta_note
	//already contain an outside range controller (A and note)
	let success = DATA_modify_object_index_sound(info_list.up.voice_id,null, info_list.up.abs_position, new_upper_note,diesis,null,null)
	if(!success){
		element.value=previous_value
		APP_blink_error(element)
		return
	}
	let next=1
	RE_focus_on_object_index_iA(info_list.down.voice_id,position,next)
}

function RE_find_voice_obj_from_iA_line(iA_line,up){
	let iA_input_boxes = [...iA_line.querySelectorAll(".App_RE_inp_box")]
	//find index
	let visible_iA_line = [...document.querySelectorAll(".App_RE_iA:not(.hidden)")]
	let iA_index = visible_iA_line.indexOf(iA_line)
	let voice_list = [...document.querySelectorAll(".App_RE_voice")]
	let visible_voice_list = voice_list.filter((item)=>{return RE_voice_is_visible(item)})
	//find voice index and voice index+1
	if(up){
		return voice_up = visible_voice_list[iA_index]
	}else{
		return voice_down = visible_voice_list[iA_index+1]
	}
}

function RE_find_voice_elements_info_from_iA_element(element){
	//find up and down voice elements corresponding to an iA element
	let down_info=JSON.parse(element.getAttribute("down_info"))
	let up_info=JSON.parse(element.getAttribute("up_info"))
	//use
	let data_up = DATA_get_object_index_info(up_info.voice_id,null,up_info.abs_position,0,true,false,true)
	let data_down = DATA_get_object_index_info(down_info.voice_id,null,down_info.abs_position,0,true,false,true)
	//ATT!!! L is not acceptable (sometime??) add information
	return {	up:{voice_id: up_info.voice_id,abs_position:up_info.abs_position,nzc_note:up_info.nzc_note,info:data_up},
				down:{voice_id: down_info.voice_id,abs_position:down_info.abs_position,nzc_note:down_info.nzc_note,info:data_down}}
}

function Find_voice_segment_up_from_iA_element(element){//XXX
	//find up voice and segment corresponding to an iA element
	//finding voice
	console.error("OBSOLETE FUNCTION Find_voice_segment_up_from_iA_element")
	let iA_list = element.closest(".App_RE_iA")
	let iA_input_boxes = [...iA_list.querySelectorAll(".App_RE_inp_box")]
	//find index
	let visible_iA_list = [...document.querySelectorAll(".App_RE_iA:not(.hidden)")]
	let iA_index = visible_iA_list.indexOf(iA_list)
	let voice_list = [...document.querySelectorAll(".App_RE_voice")]
	let visible_voice_list = voice_list.filter((item)=>{return RE_voice_is_visible(item)})
	//find voice index and voice
	let voice_up = visible_voice_list[iA_index]
	let N_line = [...voice_up.querySelectorAll(".App_RE_Na")]
	let [elementIndex,element_list]=RE_element_index(element,".App_RE_inp_box")
	let segment_index = -1
	let segment_ending_element_index = -1
	N_line.find((line)=>{
		let inputs = [...line.querySelectorAll("input")]
		let segment_starting_element_index = segment_ending_element_index+1
		segment_ending_element_index = segment_ending_element_index + inputs.length +1
		segment_index++
		if(segment_starting_element_index<=elementIndex && elementIndex<=segment_ending_element_index)return true
		return false
	})
	let segment_list = [...voice_up.querySelectorAll(".App_RE_segment")]
	return [voice_up, segment_list[segment_index]]
}

function RE_play_iA(element){
	let down_info=JSON.parse(element.getAttribute("down_info"))
	let up_info=JSON.parse(element.getAttribute("up_info"))
	//find instrument
	let instrument_index_down= DATA_which_voice_instrument(down_info.voice_id)
	let instrument_index_up= DATA_which_voice_instrument(up_info.voice_id)
	//play note
	APP_play_this_note_number(down_info.nzc_note,instrument_index_down,down_info.voice_id)
	APP_play_this_note_number(up_info.nzc_note,instrument_index_up,up_info.voice_id)
}
