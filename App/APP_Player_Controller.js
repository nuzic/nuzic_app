//Player Controller

async function APP_updateTime() {
	requestAnimationFrame(APP_updateTime)
	//the time elapsed in seconds
	let currentCompositionTime = SF_manager.synth.nuzic_sequencer.currentCompositionTime
	if(!SF_manager.synth.nuzic_sequencer.isPlaying || SF_manager.synth.nuzic_sequencer.isPaused){
		output_timer_total.value = APP_convertToTime(currentCompositionTime)
	} else {
		if(currentCompositionTime>=0 && !_calc_playing && !_scale_playing && !_accents_playing){
			APP_draw_cursor_minimap_canvas_onPLay(currentCompositionTime)
			PMC_draw_progress_line_time(currentCompositionTime)
			output_timer_total.value = APP_convertToTime(currentCompositionTime)
			if(SF_manager.synth.nuzic_sequencer.isPlaying){
				//calculate PMC best position
				PMC_calculate_best_scrollbar_position(currentCompositionTime)
			}
		}
	}
}

function APP_convertToTime(value){
	let min = Math.floor(value/60)
	let sec = Math.floor(value)-min*60
	if(sec<10)sec="0"+sec
	if(min<10)min="0"+min
	return min+"' "+sec+'"'
}

function APP_player_to_time(time,pulse_abs=null,pulse=null,fraction=null,segment_index=null,voice_id=null,){
	//progress bar only in pulse
	APP_move_progress_bar(pulse_abs)
	//RE position
	if(tabREbutton.checked){
		RE_highlight_columns_at_pulse(pulse_abs,time,voice_id)
	}else{
		//PMC_position??? XXX check if current voice is selected???
		PMC_draw_progress_line_time(time)
	}
	//RE and PMC focus
	current_position_PMCRE.segment_index = segment_index
	current_position_PMCRE.pulse = pulse
	current_position_PMCRE.fraction = fraction
	SF_manager.synth.nuzic_sequencer.setCurrentTime(time)
	//minimap
	APP_draw_cursor_minimap_canvas_onPLay(time)
}

function APP_show_instrument_selector(options){
	//options = {"voice_id","instrument"}
	let background = document.getElementById("App_instrument_selector")
	background.style.display = 'flex'
	let button_container=background.querySelector(".App_instrument_selector_button_container")
	let button_list = [...button_container.querySelectorAll(".App_instrument_selector_button")]
	if(button_list.length==0){
		//create list of buttons
		let string=""
		instrument_name_list.forEach((instrument,index)=>{
			string+=`<button class="App_instrument_selector_button" onclick="APP_select_voice_instrument(this)" value="${index}">
						<img src="./Icons/play_mini.svg">
						${instrument}
					</button>`
		})
		button_container.innerHTML=string
		button_list = [...button_container.querySelectorAll(".App_instrument_selector_button")]
	}
	//select current button
	let current_voice_instrument_string=options.instrument+""
	button_list.forEach(button =>{
		if(button.value==current_voice_instrument_string){
			button.classList.add("selected")
		}else{
			button.classList.remove("selected")
		}
	})
	//PS attention to difference BW voice_number/index(SHOWN) and voice_id(DATABASE)
	data_info = JSON.stringify(options)
	background.setAttribute("data_info",data_info)
}

function APP_hide_instrument_selector(){
	let background = document.getElementById("App_instrument_selector")
	let button_container=background.querySelector(".App_instrument_selector_button_container")
	let selected_button = button_container.querySelector(".App_instrument_selector_button.selected")
	let options=JSON.parse(background.getAttribute("data_info",data_info))
	options.instrument=parseInt(selected_button.value)
	//DATA_set_pulse_sound(options)
	background.style.display = 'none'
	APP_change_voice_instrument(options)
}

function APP_select_voice_instrument(current_button){
	let button_container=current_button.closest(".App_instrument_selector_button_container")
	let button_list = [...button_container.querySelectorAll(".App_instrument_selector_button")]
	button_list.forEach(button =>{
		button.classList.remove("selected")
	})
	current_button.classList.add("selected")
	APP_play_selected_voice_instrument(parseInt(current_button.value))
	let instrument_name=instrument_name_list[parseInt(current_button.value)]
	if(tabREbutton.checked){
		let background = document.getElementById("App_instrument_selector")
		let options=JSON.parse(background.getAttribute("data_info",data_info))
		let voice = RE_find_voice_obj_by_id(options.voice_id)
		let instrument = voice.querySelector(".App_RE_voice_instrument")
		instrument.innerHTML=instrument_name
	}else{
		//PMC_position??? XXX check if current voice is selected???
		let list = document.querySelector(".App_PMC_voice_selector_voice_list")
		let voice = list.querySelector(".App_PMC_voice_selector_voice_item.checked")
		let instrument = voice.querySelector(".App_PMC_selected_voice_instrument")
		instrument.innerHTML=instrument_name
	}
}

function APP_change_voice_instrument(options){
	let data = DATA_get_current_state_point(true)
	//Selected voice
	let selected_voice_data= data.voice_data.find(item=>{
		return item.voice_id==options.voice_id
	})
	if(selected_voice_data.instrument == options.instrument)return
	selected_voice_data.instrument = options.instrument
	DATA_insert_new_state_point(data)
	flag_DATA_updated.RE=false
	flag_DATA_updated.PMC=false
	DATA_smart_redraw()
	BNAV_update_V(data)
}

function APP_prepare_SF_instruments(voice_list){
	if(SF_manager==null)return
	let voices_ready=SF_manager.synth.channelsAmount
	let reverb=V_global_variables.effects.reverb
	V_global_variables.channel_voice_id=[]
	voice_list.forEach((voice,index)=>{
		if(index>=voices_ready){
			//add a channel
			SF_manager.synth.addNewChannel()
			voices_ready++
		}
		//change channel instrument proprieties
		let V_channel=V_global_variables.volumes[voice.voice_id]
		V_global_variables.channel_voice_id[voice.voice_id]=index
		App_set_instrument(index,voice.instrument,V_channel,reverb)
	 })
	//extra voice for "e"
	e_channel=voice_list.length
	if(e_channel>=voices_ready){
		//add a channel
		SF_manager.synth.addNewChannel()
		voices_ready++
	}
	App_set_instrument(e_channel,128,undefined,reverb)
	//extra voice for pulses and metronome
	p_channel=e_channel+1
	if(p_channel>=voices_ready){
		//add a channel
		SF_manager.synth.addNewChannel()
	}
	App_set_instrument(p_channel,130,undefined,reverb)
}

function App_set_instrument(channel,instrument=0,V_channel={volume:90,pan:0.5,fx:1,lpf:0.5,vel:V_velocity_default},reverb=0){
	if (instrument>=128){
		let i_number=instrument-128
		SF_manager.synth.setDrums(channel,true)
		SF_manager.synth.programChange(channel,i_number)
	}else{
		SF_manager.synth.setDrums(channel,false)
		SF_manager.synth.programChange(channel,instrument)
	}
	let reverb_total_value=Math.round((reverb*V_channel.fx)*128)-1 //(or 91??)
	SF_manager.synth.controllerReverb(channel,reverb_total_value)
	//pan (10)
	let pan=Math.round(V_channel.pan*127)//0,64,127
	SF_manager.synth.controllerChange(channel,10,pan)
	//channel volume (7) 0-127
	SF_manager.synth.controllerChange(channel,7,V_channel.volume)
	//lpf (74 brightness)
	let lpf=Math.round(V_channel.lpf*127)
	SF_manager.synth.controllerChange(channel,74,lpf)
}

function APP_sequencer(){
	//in freq
	hz=PPM/60
	pulsationFrequency=hz+"hz"
	//change to database
	let current_data = DATA_get_current_state_point(false)
	let voice_solo = current_data.voice_data.filter(voice=>{
		return voice.solo==true
	})
	let voice_list_unmuted_time_data = []
	if(voice_solo!=null&&voice_solo.length!=0){
		//there is a solo
		voice_solo.forEach(element=>{
			if(element.volume!="0" && !element.mute){
				voice_list_unmuted_time_data.push(element)
			}
		})
	}else{
		//find other not muted voices
		voice_list_unmuted_time_data = current_data.voice_data.filter(voice=>{
			return voice.volume!=0 && !voice.mute
		})
	}
	//Soundfont set channels for every voice + one for "e"
	APP_prepare_SF_instruments(voice_list_unmuted_time_data)
	let compas_sequence = BNAV_read_compas_accents_sequence()
	let sequencer_instructions=[]
	//ADDING SEQUENCES OF SOUNDS LOOP BETTER IN TRANSPORT
	let [now,start,end,max]=APP_return_progress_bar_values()
	let now_seconds = DATA_calculate_exact_time(now,0,0,1,1,1,1)
	let start_seconds = DATA_calculate_exact_time(start,0,0,1,1,1,1)
	let end_seconds = DATA_calculate_exact_time(end,0,0,1,1,1,1)
	let end_progress_bar = max
	let outside_initial_playing_range=false
	//a little bit before the true end in order to reset animations
	//let composition_end=end_seconds-0.01
	if(end_seconds<=now_seconds){
		outside_initial_playing_range=true
		end_seconds=DATA_calculate_exact_time(end_progress_bar,0,0,1,1,1,1)
		if(end_seconds<now_seconds){
			//at least at the end
			end_seconds=now_seconds
		}
	}
	let loop=(outputLoop.value==="true")
	let composition_end_function="APP_end_reproduction"
	if(outputLoop.value==="true"){
		if(outside_initial_playing_range){
			loop=false
		}else{
			//composition_end_function="APP_end_reproduction_loop"
			//sequencer_instructions.push([composition_end,{action: "function", function_name:"APP_end_reproduction_loop",function_params:[]}])
		}
	}
	//CLASSIC METRONOME
	if(outputMainMetronome.value === "true"){
		//see if Li bigger than main metronome
		let duration = (60/PPM)
		compas_sequence.forEach(item=>{
			//let pulseN = Math.round(item[0]*(PPM/60))
			let end_time=item[0]+duration-((duration>0.01)?0.01:0)
			let midi_note= p_channel_midiNote.Accent
			// if( item[1]=="Accent"){
			// 	midi_note= p_channel_midiNote.Accent
			// }
			if( item[1]=="Accent_Weak"){
				//note 24 -> 65.405
				midi_note=p_channel_midiNote.Accent_weak
			}
			if( item[1]=="Tic"){
				//note 25 -> 61.73409942094616
				midi_note=p_channel_midiNote.Tic
			}
			sequencer_instructions.push([item[0],{action: "SF_metroOn", channel:p_channel,note:{midiNote:midi_note,cents:0}}])
			sequencer_instructions.push([end_time,{action: "SF_metroOff", channel:p_channel,note:{midiNote:midi_note,cents:0}}])
		})
	}
	//midi timing
	let fraction_beat=(60/PPM)/24
	// compas_sequence.forEach(item=>{
		//24 tick for every pulse
		//sequencer_instructions.push([item[0],{action: "SF_MIDI_clock"}])
		//sequencer_instructions.push([item[0],{action: "function",function_name:"SF_MIDI_clock",function_params:[]}])
	// })
	//advance progress bar //all the possible pulses, even if not used
	for (let pulseN = 0 ; pulseN < end_progress_bar; pulseN++){
		let time_sec = pulseN*(60/PPM)
		sequencer_instructions.push([time_sec,{action: "function", function_name:"APP_move_progress_bar",function_params:[pulseN]}])
		for (let tick = 0 ; tick < 24; tick++){
			let time_tick=time_sec+tick*fraction_beat
			sequencer_instructions.push([time_tick,{action: "SF_MIDI_clock"}])
		}
	}
	//METRONOME VOICE (FRACTIONING)
	voice_list_unmuted_time_data.forEach(voice_data=>{
		//check of i want to hear a metronome for this voice
		//from database
		let metro_sound = voice_data.metro
		if(metro_sound!=0){
			let midiNote= metro_sound+25+12
			//Metro sounds
			let metro_fractioning= DATA_list_voice_data_metro_fractioning(voice_data)
			metro_fractioning.forEach(change_fraction_data=>{
				let duration=(1/change_fraction_data.freq)
				for (let i =0; i<change_fraction_data.rep; i++){
					let time = change_fraction_data.time + i*duration
					let end_time=time+duration-((duration>0.01)?0.01:0)
					sequencer_instructions.push([time,{action: "SF_metroOn", channel:p_channel,note:{midiNote:midiNote,cents:0}}])
					sequencer_instructions.push([end_time,{action: "SF_metroOff", channel:p_channel,note:{midiNote:midiNote,cents:0}}])
				}
			})
		}
	})
	let note_id= 0
	//NOTES VOICE
	voice_list_unmuted_time_data.forEach((voice_data,channel_index)=>{
		let midi_data = (JSON.parse(JSON.stringify(voice_data.data.midi_data)))
		let index_position = 0
		//changing instruments
		let voice_instrument = voice_data.instrument
		//for midi reproduction XXX XXX XXX XXX
		let midi_channel=(voice_data.midiCh==null)?((voice_data.instrument<128)?0:9): voice_data.midiCh
		let midi_channel_e=9
		let channel_elements=(voice_data.e_sound==22)?p_channel:e_channel
		let nzc_note = midi_data.note_freq.map(freq=>{return freq.map(f=>{return DATA_Hz_to_note(f,TET)})})
		let freq_note = midi_data.note_freq
		let note_sequence = []//array nuzic_note and frequence
		for (let i = 0 ; i<nzc_note.length;i++){
			note_sequence.push({"nzc" : nzc_note[i], "freq" : freq_note[i]})
		}
		let time_list = midi_data.time
		//duration iT_freq
		let duration_list = midi_data.duration
		let Dduration = 0
		//creating segment_obj_list, column_index_list
		let segment_obj_list = []
		let index_position_list=[]//n column in segment XXX eventually will die XXX
		let sound_index_list=[]//eventually will be used for note and time too (now only for A)
		let segment_index_list=[]
		let index_abs_position_list=[]//n column from the start
		if(tabREbutton.checked && voice_data.RE_visible){
			//find voice
			voice_data.data.segment_data.forEach(segment=>{
				segment.sound.forEach((s,index)=>{
					sound_index_list.push(index)
				})
				sound_index_list.pop()
			})
			let voice_RE_obj = RE_find_voice_obj_by_id(voice_data.voice_id)
			if(voice_RE_obj == null){
				console.error("voice not found")
				return
			}
			segment_obj_list = [...voice_RE_obj.querySelectorAll(".App_RE_segment")]
			//XXX XXX XXX here i can force loading RE segments XXX XXX XXX
			let current_segment_change_index=1//when change first time (0->1)
			let segment_change_position=0
			let time_current_voice_segment_start=voice_data.data.segment_data.map(segment=>{
				let n_obj=segment.time.length-1
				segment_change_position+=n_obj
				return time_list[segment_change_position-n_obj]
			})
			let provv=0
			let ind_time_list=0
			let ind_abs_segment_start=0
			let ind_seg=0
			time_current_voice_segment_start.push("end")
			let time_list_size=time_list.length
			RE_global_time_all_changes.some(time=>{
				if(time==RE_global_time_segment_change[current_segment_change_index]){
					//plus a pair of columns
					provv+=2
					current_segment_change_index++
				}
				if(time==time_list[ind_time_list]){
					index_abs_position_list.push(provv)
					if(time==time_current_voice_segment_start[ind_seg+1]){
						ind_seg++
						ind_abs_segment_start=provv
					}
					segment_index_list.push(ind_seg)
					index_position_list.push(provv-ind_abs_segment_start)
					ind_time_list++
					if(ind_time_list>=time_list_size){
						return true
					}
				}
				provv+=2
				return false
			})
		}
		//create an array of pulse position
		let PMC_pulse_position_list=[]
		let first_note=note_sequence.find((note,index)=>{
			return note.nzc[0]>=0
		})
		let last_position=new Array([voice_data.e_sound])
		if(first_note!=null)last_position=first_note.nzc
		note_sequence.forEach((note,index)=>{
			if(note.nzc[0]>=0){
				last_position=note.nzc
			}
			PMC_pulse_position_list.push(last_position)
		})
		//reverse if not L not working
		nzc_note = nzc_note.reverse()
		freq_note = freq_note.reverse()
		note_sequence = note_sequence.reverse()
		time_list = time_list.reverse()
		duration_list = duration_list.reverse()
		index_position_list=index_position_list.reverse()//XXX future obsolete
		sound_index_list=sound_index_list.reverse()
		segment_index_list=segment_index_list.reverse()
		index_abs_position_list= index_abs_position_list.reverse()
		PMC_pulse_position_list= PMC_pulse_position_list.reverse()
		note_sequence.forEach((note,index)=>{
			//OPTIMIZED
			if(tabREbutton.checked && voice_data.RE_visible){
				index_column_blink=2
				//add movement to the bar
				sequencer_instructions.push([time_list[index],{action: "function", function_name:"RE_calculate_best_scrollbar_position",function_params:[index_abs_position_list[index]]}])
			}
			//dont sequence things outside
			if(time_list[index]<end_seconds){
				if(note.nzc<0){
					if(note.nzc==-3){
						//ligadura
						let duration = 1/duration_list[index]
						Dduration+=(duration)
						/// animation ligadura
						if(tabREbutton.checked && voice_data.RE_visible){
							let segment_obj = segment_obj_list[segment_index_list[index]]
							let index_column_blink = index_position_list[index]
							sequencer_instructions.push([time_list[index],{action: "function", function_name:"RE_hightlight_column_on_play_on",function_params:[segment_obj,index_column_blink,sound_index_list[index]]}])
							sequencer_instructions.push([time_list[index]+duration,{action: "function", function_name:"RE_hightlight_column_on_play_off",function_params:[segment_obj,index_column_blink,sound_index_list[index]]}])
						}
					}else{
						//silence or element
						//animation silence and element
						let duration = 1/duration_list[index]
						let start_note_time=time_list[index]
						//animation
						if(tabREbutton.checked && voice_data.RE_visible){
							let segment_obj = segment_obj_list[segment_index_list[index]]
							let index_column_blink = index_position_list[index]
							sequencer_instructions.push([start_note_time,{action: "function", function_name:"RE_hightlight_column_on_play_on",function_params:[segment_obj,index_column_blink,sound_index_list[index]]}])
							sequencer_instructions.push([start_note_time+duration,{action: "function", function_name:"RE_hightlight_column_on_play_off",function_params:[segment_obj,index_column_blink,sound_index_list[index]]}])
						}
						//modify duration for PMC only
						if(Dduration!=0){
							duration += Dduration
							//reset
							Dduration=0
						}
						//PMC animation
						if(tabPMCbutton.checked && note.nzc==-2){
							//animation of "e"
							note_id++
							let note_data = {note:note.nzc,y_position:PMC_pulse_position_list[index],pulsation:start_note_time*PPM/60,duration:duration*PPM/60,note_id:note_id, color:voice_data.color}
							sequencer_instructions.push([start_note_time,{action: "function", function_name:"PMC_add_note_to_playingNoteArr",function_params:[note_data]}])
							sequencer_instructions.push([start_note_time+duration,{action: "function", function_name:"PMC_remove_note_from_playingNotesArr",function_params:[note_data]}])
						}
						//element
						if(note.nzc==-2){
							let midiNote = voice_data.e_sound+12
							//midi_channel_e
							sequencer_instructions.push([start_note_time,{action: "SF_noteOn", midiCh:midi_channel_e, channel:channel_elements,voice_id: voice_data.voice_id, note:[{midiNote:midiNote,cents:0}]}])
							//avoid lag same note
							let end_time=start_note_time+duration-((duration>0.01)?0.01:0)
							sequencer_instructions.push([end_time,{action: "SF_noteOff", midiCh:midi_channel_e, channel:channel_elements, note:[{midiNote:midiNote,cents:0}]}])
						}
					}
				}else{
					//it is a note
					let duration = 1/duration_list[index]
					if(Dduration!=0){
						duration += Dduration
						//reset
						Dduration=0
					}
					let start_note_time = time_list[index]
					let end_note_time = start_note_time+duration
					//control loop start/end
					//control loop end
					//if commented (and sequence everything) this will prevent cutting a note in a loop if that end after the loop
					if(end_note_time>end_seconds && start_note_time<end_seconds){
						duration= end_seconds-start_note_time
					}
					//if instrument sync directly with transport
					//calculate midiNotes
					let midiNotes=[]
					note.freq.forEach(frequence=>{
						let midiNote_float = 69+12*Math.log2(frequence/440)
						let midiNote=Math.round(midiNote_float)
						let midiNote_freq=Math.pow(2, (midiNote-69)/12) * 440
						let cents=Math.round(1200*Math.log2(frequence/midiNote_freq))
						cents=(cents==0)?0:cents//-0 == 0
						midiNotes.push({midiNote:midiNote,cents:cents})
					})
					sequencer_instructions.push([start_note_time,{action: "SF_noteOn", midiCh:midi_channel, channel:channel_index,voice_id: voice_data.voice_id, note:midiNotes}])

					//avoid lag same note
					let end_time=start_note_time+duration-((duration>0.01)?0.01:0)
					sequencer_instructions.push([end_time,{action: "SF_noteOff", midiCh:midi_channel, channel:channel_index, note:midiNotes}])
					//RE animation
					if(tabREbutton.checked && voice_data.RE_visible){
						let segment_obj = segment_obj_list[segment_index_list[index]]
						let index_column_blink = index_position_list[index]
						sequencer_instructions.push([start_note_time,{action: "function", function_name:"RE_hightlight_column_on_play_on",function_params:[segment_obj,index_column_blink,sound_index_list[index]]}])
						sequencer_instructions.push([start_note_time+duration,{action: "function", function_name:"RE_hightlight_column_on_play_off",function_params:[segment_obj,index_column_blink,sound_index_list[index]]}])
					}
					//PMC animation
					note_id++
					if(tabPMCbutton.checked){
						let note_data = {note:note.nzc,y_position:note.nzc,pulsation:start_note_time*PPM/60,duration:duration*PPM/60,note_id:note_id, color:voice_data.color}
						sequencer_instructions.push([start_note_time,{action: "function", function_name:"PMC_add_note_to_playingNoteArr",function_params:[note_data]}])
						sequencer_instructions.push([start_note_time+duration,{action: "function", function_name:"PMC_remove_note_from_playingNotesArr",function_params:[note_data]}])
					}
				}
			}
		})
	})
	SF_manager.synth.nuzic_sequencer.new_sequence(sequencer_instructions,loop,start_seconds,end_seconds,composition_end_function)
}

function APP_end_reproduction(){
	//stop the reproduction
	APP_stop()//unnecessary???
	let [now,start,end,max]=APP_return_progress_bar_values()
	APP_move_progress_bar(start)
	APP_reset_blink_play()
}

// function APP_end_reproduction_loop(){
	//function that go at the end of a loop
// 	APP_reset_blink_play()
	//clear all the PMC notes
// 	PMC_reset_playing_notes()
// }

function APP_play(){
	APP_stop_secondary_players()
	let play_label= document.querySelector('.App_Nav_play_button')
	let pause_label= document.querySelector('.App_Nav_pause_button')
	if (tabPMCbutton.checked){
		PMC_redraw_selected_voice_svg(false,DATA_get_selected_voice_data())
	}
	let [now,start,end,max]=APP_return_progress_bar_values()
	let buffer = 0.2
	//using bar to define transport position (USED ONLY IF TRANSPORT IS STOPPED)
	let now_seconds = DATA_calculate_exact_time(now,0,0,1,1,1,1)
	let start_seconds = DATA_calculate_exact_time(start,0,0,1,1,1,1)
	let end_seconds = DATA_calculate_exact_time(end,0,0,1,1,1,1)
	play_label.style.display = 'none'
	pause_label.style.display = 'flex'
	//chrome and safari need action to start context
	if(SF_manager==null)return
	if(SF_manager.synth.nuzic_sequencer ==null)return
	if(!SF_manager.synth.nuzic_sequencer.isPlaying){
		//from the start
		APP_sequencer()
		//countin
		let j = TNAV_read_countIn()
		for (let i=0; i<j; i++){
			let countIn_time = i *1000 * 60 / PPM
			setTimeout(function () {
				SF_manager.synth.noteOn(p_channel,p_channel_midiNote.Accent_weak, 0 ,V_global_variables.mainMetronomeVolume)//XXX    or midi
			}, countIn_time)
		}
		let countIn_end = j * 1000 * 60 / PPM - buffer * 1000//buffering
		setTimeout(function () {
			//smaller buffer
			let now_seconds_buffer_SF=now_seconds-0.05
			if(now_seconds==0){
				now_seconds_buffer_SF=now_seconds-buffer
			}
			SF_manager.synth.nuzic_sequencer.start(now_seconds_buffer_SF)
		}, countIn_end)
	} else if(SF_manager.synth.nuzic_sequencer.isPaused){
		//paused: restart
		SF_manager.synth.nuzic_sequencer.start(now_seconds)
		//restart notes that were playing at the time
		//APP_restart_paused_notes()XXX
	} else {
		//NOTHING TO DO
	}
}

function APP_play_recorder(){
	APP_stop_secondary_players()
	let play_label= document.querySelector('.App_Nav_play_button')
	let pause_label= document.querySelector('.App_Nav_pause_button')

	let [now,start,end,max]=APP_return_progress_bar_values()
	let buffer = 0.3
	//using bar to define transport position (USED ONLY IF TRANSPORT IS STOPPED)
	let now_seconds = DATA_calculate_exact_time(now,0,0,1,1,1,1)
	let start_seconds = DATA_calculate_exact_time(start,0,0,1,1,1,1)
	let end_seconds = DATA_calculate_exact_time(end,0,0,1,1,1,1)
	play_label.style.display = 'none'
	pause_label.style.display = 'flex'
	if(SF_manager==null)return
	if(SF_manager.synth.nuzic_sequencer ==null)return
	if(!SF_manager.synth.nuzic_sequencer.isPlaying){
		//from the start
		APP_sequencer()
		let sequencer_instructions = SF_manager.synth.nuzic_sequencer.instructions.filter(element=>{
			if(element[1].action=="function"){
				if(element[1].function_name=="APP_move_progress_bar")return true
				return false
			}
			return true
		})
		let loop_start=SF_manager.synth.nuzic_sequencer.loopStart
		let loop_end=SF_manager.synth.nuzic_sequencer.loopEnd
		let composition_end_function=SF_manager.synth.nuzic_sequencer.stopFunction
		SF_manager.synth.nuzic_sequencer.new_sequence(sequencer_instructions,false,start_seconds,end_seconds,composition_end_function)
		//overwrite new nuzic_sequencer only scrollbar XXX XXX XXX XXX
		// let end_progress_bar=max
		// for (let pulseN = 0 ; pulseN < end_progress_bar; pulseN++){
		// 	let time_sec = pulseN*(60/PPM)
		// 	sequencer_instructions.push([time_sec,{action: "function", function_name:"APP_move_progress_bar",function_params:[pulseN]}])
		// }
		//countin
		let j = TNAV_read_countIn()
		for (let i=0; i<j; i++){
			let countIn_time = i *1000 * 60 / PPM
			setTimeout(function () {
				SF_manager.synth.noteOn_freq(p_channel,65.405 ,V_global_variables.mainMetronomeVolume)
			}, countIn_time)
		}
		let countIn_end = j * 1000 * 60 / PPM - buffer * 1000//buffering
		setTimeout(function () {
			//smaller buffer
			let now_seconds_buffer_SF=now_seconds-0.05
			if(now_seconds==0){
				now_seconds_buffer_SF=now_seconds
			}
			SF_manager.synth.nuzic_sequencer.start(now_seconds_buffer_SF)
		}, countIn_end)
	} else if(SF_manager.synth.nuzic_sequencer.isPaused){
		SF_manager.synth.nuzic_sequencer.start(now_seconds)
		//restart notes that were playing at the time
		//APP_restart_paused_notes()
	} else {
		//NOTHING TO DO
	}
}

function APP_stop(){
	APP_reset_blink_play()
	PMC_reset_progress_line()
	PMC_reset_action_pmc()
	PMC_reset_playing_notes()
	if (tabPMCbutton.checked){
		//CHECK bc slow if RE opened
		PMC_redraw_selected_voice_svg(true,DATA_get_selected_voice_data())
	}
	let play_label= document.querySelector('.App_Nav_play_button')
	let pause_label= document.querySelector('.App_Nav_pause_button')
	play_label.style.display = 'flex'
	pause_label.style.display = 'none'
	if(SF_manager==null)return
	if(SF_manager.synth.nuzic_sequencer ==null)return
	if(SF_manager.synth.nuzic_sequencer.isPlaying){
		//try to erease transport scheduled events
		// _calc_playing=false
		// _scale_playing=false
		// _accents_playing=false
		SF_manager.synth.nuzic_sequencer.stop()
		SF_manager.synth.stopAll()
	}
	APP_check_minimap_scroll()
	APP_stop_secondary_players()
}

function APP_stop_secondary_players(){
	if(_calc_playing || _accents_playing || _scale_playing){
		_calc_playing=false
		_scale_playing=false
		_accents_playing=false
		secondary_players_timeout_id.forEach(timeout=>{
			clearTimeout(timeout)
		})
		secondary_players_timeout_id=[]
		SF_manager.synth.stopAll()
	}
}

function APP_pause(){
	let play_label= document.querySelector('.App_Nav_play_button')
	let pause_label= document.querySelector('.App_Nav_pause_button')
	play_label.style.display = 'flex'
	pause_label.style.display = 'none'
	if(SF_manager==null)return
	if(SF_manager.synth.nuzic_sequencer ==null)return
	SF_manager.synth.nuzic_sequencer.pause()
}

function APP_play_selected_voice_instrument(instrument){
	let nuzic_note_1=4*TET+ Math.floor(Math.random()*TET)
	let nuzic_note_2=4*TET+ Math.floor(Math.random()*TET)
	let nuzic_note_3=4*TET+ Math.floor(Math.random()*TET)
	//let midi_note_3=30
	let f1=DATA_note_to_Hz(nuzic_note_1,TET)
	let f2=DATA_note_to_Hz(nuzic_note_2,TET)
	let f3=DATA_note_to_Hz(nuzic_note_3,TET)
	//ok for drum and TET != 12
	App_set_instrument(0,instrument)
	SF_manager.synth.noteOn_freq(0,f1,V_velocity_default)
	setTimeout(function () {
		SF_manager.synth.noteOff_freq(0,f1)
		SF_manager.synth.noteOn_freq(0,f2,V_velocity_default)
	}, 300)
	setTimeout(function () {
		SF_manager.synth.noteOff_freq(0,f2)
		SF_manager.synth.noteOn_freq(0,f3,V_velocity_default)
	}, 600)
	setTimeout(function () {
		SF_manager.synth.noteOff_freq(0,f3)
	}, 900)
}

let note_on_click_silenced=false
//play re click
function APP_play_this_note_RE_element(element,new_iN=0){
	//if new iN created, it will play next note new_iN=1
	if(note_on_click_silenced)return
	let pos = RE_input_to_position(element)
	let element_info = DATA_get_object_index_info(pos.voice_id,pos.segment_index,pos.sound_index+new_iN)
	let nzc_note=element_info.sound.note
	let voice = element.closest(".App_RE_voice")
	let instrument_index = RE_which_voice_instrument(voice)
	APP_play_this_note_number(nzc_note , instrument_index,pos.voice_id)
}

function APP_play_input_on_click(element, type){
	//if it is playng something
	if(type!="iA"){
		if(SNAV_read_note_on_click() == true && element.value!=""){
			APP_stop()
			//set timeout interference with play
			//setTimeout(function () {
				APP_play_this_note_RE_element(element)
			//}, 400)
		}
	}else{
		//is a iA
		if(SNAV_read_note_on_click() == true){
			RE_play_iA(element)
		}
	}
}

//re dbclick
function APP_play_all_notes_at_time(time){
	APP_stop()
	let data = DATA_get_current_state_point(true)
	let note_to_play= []
	let voices_ready=SF_manager.synth.channelsAmount
	APP_prepare_SF_instruments(data.voice_data)//all!!!
	data.voice_data.forEach((voice,channel_index)=>{
		//if not visible end
		if(!voice.RE_visible || voice.volume==0 || voice.mute){
			return
		}
		voice.data.midi_data.time.find((midi_time,index) =>{
			if(midi_time==time){
				note_to_play.push({"note_freq":voice.data.midi_data.note_freq[index], channel: channel_index,voice_id: voice.voice_id})
				return true
			}
			return false
		})
	})
	note_to_play.forEach(note=>{
		if(note.note_freq<=0)return
		note.note_freq.forEach(freq=>{
			SF_manager.synth.noteOn_freq(note.channel,freq,V_global_variables.volumes[note.voice_id].vel)
			setTimeout(function () {
				SF_manager.synth.noteOff_freq(note.channel,freq)
			}, 500)
		})
	})
	//avoid play of single click
	note_on_click_silenced=true
	setTimeout(function () {
		note_on_click_silenced=false
	}, 500)
}

//async function
function APP_play_this_note_number(nzc_note,instrument,voice_id){
	if(nzc_note<0)return
	let V_channel=(voice_id!=null)?V_global_variables.volumes[voice_id]:{volume:70,pan:0.5,fx:1,lpf:0.5,vel:V_velocity_default}
	App_set_instrument(0,instrument,V_channel,V_global_variables.effects.reverb)
	let freq_note = (instrument<128)?DATA_note_to_Hz(nzc_note,TET):DATA_note_to_Hz(nzc_note,12)
	SF_manager.synth.noteOn_freq(0,freq_note,V_channel.vel)
	setTimeout(function () {
		SF_manager.synth.noteOff_freq(0,freq_note)
	}, 500)
}

let _accents_playing=false
function APP_play_this_accents(){
	if(_accents_playing){
		APP_stop_secondary_players()
		return
	}
	if(SF_manager==null)return
	APP_stop()
	let table_compas = document.querySelector(".App_BNav_C_top_sequence")
	let selected = table_compas.querySelector(".checked")
	let compas_value = JSON.parse(selected.value)
	let accent_list = compas_value.compas_values[3]
	let Lc = compas_value.compas_values[1]
	if(Lc ==0)return
	let Ti = (60/PPM)*1000
	_accents_playing=true
	//millisec
	let start_time=100
	let waiting_time = Lc*Ti
	let end_time=start_time+waiting_time
	let timeout=setTimeout(() => {
		APP_stop_secondary_players()
	}, end_time)
	secondary_players_timeout_id.push(timeout)
	//sounds
	App_set_instrument(0,130,undefined,V_global_variables.effects.reverb)
	for (let i=0 ; i<Lc ; i++){
		let timeout
		if(i==0){
			//first accent
			timeout=setTimeout(() => {
				SF_manager.synth.noteOn_freq(0,p_channel_freq.Accent ,V_velocity_default)
			}, start_time)
		}else{
			if(accent_list.includes(i)){
				timeout=setTimeout(() => {
					SF_manager.synth.noteOn_freq(0,p_channel_freq.Accent_weak ,V_velocity_default)
				}, start_time+Ti*i)
			}else{
				timeout=setTimeout(() => {
					SF_manager.synth.noteOn_freq(0,p_channel_freq.Tic ,V_velocity_default)
				}, start_time+Ti*i)
			}
		}
		secondary_players_timeout_id.push(timeout)
	}
}

let _scale_playing=false
function APP_play_this_scale(scale_TET,iS,starting_note=0,rotation=0){
	if(_scale_playing){
		APP_stop_secondary_players()
		return
	}
	if(SF_manager==null)return
	APP_stop()
	//play note from reg 3
	//starting note only for editor!!!
	let instrument=0
	App_set_instrument(0,instrument)
	_scale_playing=true
	//millisec
	let start_time=100
	//1.5 a little bit more for last note sound better
	let module=iS.length
	let waiting_time = (module+1.5)*500
	let end_time=start_time+waiting_time
	let timeout=setTimeout(() => {
		APP_stop_secondary_players()
	}, end_time)
	secondary_players_timeout_id.push(timeout)
	let current_note = 3*scale_TET + starting_note
	let first_freq_note = DATA_note_to_Hz(current_note,scale_TET)
	//first note
	timeout=setTimeout(() => {
		SF_manager.synth.noteOn_freq(0,first_freq_note,V_velocity_default)
		setTimeout(function () {
			SF_manager.synth.noteOff_freq(0,first_freq_note)
		}, 500)
	}, start_time)
	secondary_players_timeout_id.push(timeout)
	//other notes
	//iS+rotation
	let temp_iS = Array.from(iS)
	for (let y=0 ; y<rotation; y++){
		let iS = temp_iS.shift()
		temp_iS.push(iS)
	}
	for (let i=0 ; i<module; i++){
		current_note += temp_iS[i]
		let freq_note=DATA_note_to_Hz(current_note,scale_TET)
		let timeout=setTimeout(() => {
			SF_manager.synth.noteOn_freq(0,freq_note,V_velocity_default)
			setTimeout(function () {
				SF_manager.synth.noteOff_freq(0,freq_note)
			}, 500)
		}, start_time+500*(i+1))
		secondary_players_timeout_id.push(timeout)
	}
}

function APP_play_this_scale_note(current_note,scale_TET){
	let freq_note = DATA_note_to_Hz(current_note,scale_TET)
	App_set_instrument(0,0)
	SF_manager.synth.noteOn_freq(0,freq_note,V_velocity_default)
	setTimeout(function () {
		SF_manager.synth.noteOff_freq(0,freq_note)
	}, 500)
}

function APP_play_selected_voice_pulse_sound(e_sound){
	let nzc_note=e_sound
	let instrument = (e_sound<23)?130:128
	let freq_note = DATA_note_to_Hz(nzc_note,12)
	App_set_instrument(0,instrument)
	SF_manager.synth.noteOn_freq(0,freq_note,V_velocity_default)
	setTimeout(function () {
		SF_manager.synth.noteOff_freq(0,freq_note)
	}, 1000)
}

let _calc_playing=false
function APP_play_this_calc_iT_line(iT_array){
	if(_calc_playing){
		APP_stop_secondary_players()
		return
	}
	if(SF_manager==null)return
	APP_stop()
	if(iT_array.length==0)return
	let L_iT = iT_array.reduce((prop,iT)=>{return iT+prop},0)
	if(L_iT==0)return
	let start=0
	let pulse_list=iT_array.map(iT=>{
		start+=iT
		return start
	})
	pulse_list.unshift(0)
	let dT = 0.3//0.1 for accents
	let Ti = (60/PPM)
	_calc_playing=true
	let start_time=100
	let waiting_time = L_iT*Ti*1000
	let end_time=start_time+waiting_time
	let timeout=setTimeout(() => {
		APP_stop_secondary_players()
	}, end_time)
	secondary_players_timeout_id.push(timeout)
	//sounds
	App_set_instrument(0,130,undefined,V_global_variables.effects.reverb)
	pulse_list.forEach(pulse=>{
		let timeout=setTimeout(() => {
			SF_manager.synth.noteOn_freq(0,p_channel_freq.Pulse ,V_global_variables.mainVolume)//mainMetronomeVolume
			setTimeout(function () {
				SF_manager.synth.noteOff_freq(0,p_channel_freq.Pulse)
			}, Ti*900)
		}, start_time+Ti*pulse*1000)
		secondary_players_timeout_id.push(timeout)
	})
}

function APP_play_this_calc_iN_line(iN_array){
	if(iN_array.length==0){
		console.error("Calculator Play error")
		return false
	}
	let N_array=[]
	N_array[0]=iN_array.shift()
	//other notes
	let current_note=N_array[0]
	for (let i=0 ; i<iN_array.length; i++){
		current_note += iN_array[i]
		N_array.push(current_note)
	}
	APP_play_this_calc_N_line(N_array)
}

function APP_play_this_calc_N_line(N_array){
	if(_calc_playing){
		APP_stop_secondary_players()
		return
	}
	if(N_array.length==0){
		console.error("Calculator Play error")
		return false
	}
	if(SF_manager==null)return
	APP_stop()
	let max_note_number = TET * N_reg-1
	let freq_note_array=[]
	N_array.forEach((note,index)=>{
		//verify intern range
		if(note<0)N_array[index]=0
		if(note>max_note_number)N_array[index]=max_note_number
		let freq_note = DATA_note_to_Hz(N_array[index],TET)
		freq_note_array.push(freq_note)
	})
	//test note inside TET*N_reg
	let L_iT = N_array.length
	let Ti = (60/PPM)
	_calc_playing=true
	let start_time=100
	let waiting_time = L_iT*Ti*1000
	let end_time=start_time+waiting_time
	let timeout=setTimeout(() => {
		APP_stop_secondary_players()
	}, end_time)
	secondary_players_timeout_id.push(timeout)
	let instrument = 0
	App_set_instrument(0,instrument)
	freq_note_array.forEach((freq_note,index)=>{
		let timeout=setTimeout(() => {
			SF_manager.synth.noteOn_freq(0,freq_note,V_velocity_default)
			setTimeout(function () {
				SF_manager.synth.noteOff_freq(0,freq_note)
			}, Ti*900)
		}, start_time+Ti*index*1000)
		secondary_players_timeout_id.push(timeout)
	})
}

//play stop with spacebar
//document.addEventListener('keyup', function(e){
document.addEventListener('keydown', function(e){
	//console.log(e.keyCode)
	//spacebar
	if(e.keyCode == "32"){
		//play pause
		let currentElement_id = document.activeElement.id
		if(currentElement_id=="App_notebook" || currentElement_id=="composition_name")return
		let current_class_list = document.activeElement.classList
		//current_class_list.contains("App_BNav_S_list_scale_name")
		if(current_class_list.contains("App_BNav_C_content_header_inp_name") || currentElement_id=="App_BNav_S_editor_title_scale_name" ||
			current_class_list.contains("App_RE_voice_name") || current_class_list.contains("App_RE_column_selector_segment_name") || current_class_list.contains("App_RE_segment_name") ||
			current_class_list.contains("App_PMC_segment_name") || current_class_list.contains("App_PMC_voice_selector_voice_item_name") ||
			current_class_list.contains("App_SNav_bug_divForm") || current_class_list.contains("App_PMC_selected_voice_sequence_object") ||
			current_class_list.contains("App_RE_A_cell"))return
		if(SF_manager==null)return
		if(SF_manager.synth.nuzic_sequencer ==null)return
		if(!SF_manager.synth.nuzic_sequencer.isPlaying || SF_manager.synth.nuzic_sequencer.isPaused){
			APP_play()
		}else{
			APP_pause()
		}
		e.preventDefault()
	}
})

/*
Sequencer with SF_lib
Initialization

const sequencer = new Sequencer(synthesizer);

    synthesizer - a Synthesizer instance to play to.

loadNewSongList

Loads a new song list.

sequencer.loadNewSongList(parsedMidis);

    parsedMidis - an array of MIDI instances representing the songs to play. If there's only one, the loop will be enabled.

play

Starts playing the sequence. If the sequence was paused, it won't change any controllers, but if it wasn't (ex. the time was changed) then it will go through all the controller changes from the start before playing. This function does NOT modify the current playback time!

sequencer.play(resetTime);

    resetTime - boolean, if set to true then the playback will start from 0. Defaults to false;

pause

Pauses the playback of the sequence.

sequencer.pause();

stop

Stops the playback of the sequence. Currently only used internally by the pause function.

sequencer.stop();

nextSong

Plays the next song in the list.

sequencer.nextSong();

previousSong

Plays the previous song in the list.

sequencer.previousSong();

paused

Read-only boolean, indicating that if the sequencer's playback is paused.

if(sequencer.paused)
{
   console.log("Sequencer paused!");
}
else
{
   console.log("Sequencer playing or stopped!");
}

loop

Boolean that controls if the sequencer loops.

sequencer.loop = false;

*/
