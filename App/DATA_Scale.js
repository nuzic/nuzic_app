//Function manipulating Note on scale

function DATA_get_Pa_eq_scale(time_p,scale_sequence=null){
	//time_p is the equivalent aprox ABSOLUTE PULSE
	if(scale_sequence==null)scale_sequence= DATA_get_scale_sequence()
	if(scale_sequence.length==0)return null
	//element inside a scale module
	let scale_end = 0
	let index=-1
	let current_scale = scale_sequence.find(scale=>{
		scale_end+=scale.duration
		index++
		return scale_end>time_p
	})
	if(current_scale==null){
		current_scale=scale_sequence.slice(-1)[0]
		index=-1
	}
	current_scale.scale_index=index
	return current_scale
}

function DATA_calculate_absolute_note_to_scale(note,positive=true,scale_data,idea_scale_list){
	let n_scale = note-scale_data.starting_note
	if(n_scale<0)n_scale=0-TET-n_scale
	let reg = Math.floor(n_scale/TET)
	let resto = Math.abs(n_scale)%TET
	let iS_list = []
	let scale = idea_scale_list.find(item=>{
		return item.acronym==scale_data.acronym
	})
	//changing the acronym
	if(scale!=null){
		//a COPY, not a pointer
		iS_list = JSON.parse(JSON.stringify(scale.iS))
	}else {
		return ["",0,""]
	}
	for (let i = 0 ; i<scale_data.rotation; i++){
		iS_list.push(iS_list.shift())
	}
	let current_grade = 0
	let current_note = 0
	let current_iS = iS_list[0]
	iS_list.find(iS=>{
		if (current_note>=resto){
			return true
		}else{
			current_grade++
			current_note+=iS
			current_iS = iS
		}
	})
	let delta = 0
	if((current_note-resto)!=0){
		delta = current_iS-(current_note-resto)
	}
	if(positive){
		//if is not a grade take ++ diesis
		if(delta!=0){
			return [current_grade-1,delta,reg]
		}else{
			return [current_grade,delta,reg]
		}
	}else{
		//if is not a grade take a bemolle
		if(delta!=0){
			let negative_delta = delta-iS_list[current_grade-1]
			//avoid over shoot grade ( es. grade 7- == 0-
			if(iS_list.length==current_grade) {
				current_grade=0
				reg++
			}
			return [current_grade,negative_delta,reg]
		}else{
			return [current_grade,delta,reg]
		}
	}
}

function DATA_calculate_scale_note_to_absolute(grade,delta,reg,scale_data,idea_scale_list){
	let starting_note = scale_data.starting_note
	let iS_list = []
	let scale = idea_scale_list.find(item=>{
		return item.acronym==scale_data.acronym
	})
	//changing the acronym
	if(scale!=null){
		//a COPY, not a pointer
		iS_list = JSON.parse(JSON.stringify(scale.iS))
	}else {
		console.error("scale not found")
		//dangerous to return null
		return {note:null, outside_range:false}
	}
	for (let i = 0 ; i<scale_data.rotation; i++){
		iS_list.push(iS_list.shift())
	}
	let grade_position = 0
	let y = 0
	if(grade<0){
		//case iNgm can generate neg grade
		//adjust reg
		let mod = scale.module
		let resto = (grade)%mod
		let new_grade = resto==0 ? 0 : mod+resto
		let d_reg = Math.floor(grade/mod)
		let new_reg=reg+d_reg
		reg=new_reg
		grade=new_grade
	}
	for (let i = 0 ; i<grade; i++){
		//grade CAN be > iS length
		grade_position += iS_list[y]
		y++
		if(y>=iS_list.length)y=0
	}
	//XXX TEST CONTROL IF DELTA IS POSSIBLE (POSITIVE OR NEGATIVE)
	if(delta>0){
		if(iS_list[y]<=delta)delta=iS_list[y]-1
	}
	if(delta<0){
		if(iS_list[y-1]+delta<=0)delta=1-iS_list[y-1]
	}
	let note_number = TET*reg + starting_note + grade_position + delta
	let max_note_number = TET * N_reg-1
	let outside_range=false
	if(note_number>max_note_number){
		note_number=max_note_number
		outside_range=true
	}
	if(note_number<0){
		note_number=0
		outside_range=true
	}
	return {note:note_number, outside_range}
}

function DATA_calculate_scale_interval_string(A_Na,A_diesis,A_Na_element_scale,B_Na,B_diesis,B_Na_element_scale,idea_scale_list){
	//calculate resto in grades
	let [grade_A,delta_A,reg_A]=DATA_calculate_absolute_note_to_scale(A_Na,A_diesis,A_Na_element_scale,idea_scale_list)
	let [grade_B,delta_B,reg_B]=DATA_calculate_absolute_note_to_scale(B_Na,B_diesis,B_Na_element_scale,idea_scale_list)
	//4 cases
	let scale_module = BNAV_get_scale_from_acronym(A_Na_element_scale.acronym).module
	let d_g=grade_B-grade_A
	let d_r=reg_B-reg_A
	let d_grad = d_g+d_r*scale_module
	return d_grad

/* grade AND diesis
	var d_grad = grade_B-grade_A+(reg_B-reg_A)*scale_module
	var d_delta = delta_B

	stringiN+= d_grad
	//diesis true / false ???? XXX
	if(d_delta==1)stringiN+=RE_string_to_superscript("+")
	if(d_delta>1)stringiN+=RE_string_to_superscript("+"+d_delta)
	if(d_delta==-1)stringiN+=RE_string_to_superscript("-")
	if(d_delta<-1)stringiN+=RE_string_to_superscript(d_delta.toString())
*/
/* OBSOLETE grade diesis reg
	var d_reg = reg_B-reg_A
	var d_grad = grade_B-grade_A
	var d_delta = delta_B-delta_A

	//deltas
	stringiN+= d_grad
	if(d_delta==1)stringiN+=RE_string_to_superscript("+")
	if(d_delta>1)stringiN+=RE_string_to_superscript("+"+d_delta)
	if(d_delta==-1)stringiN+=RE_string_to_superscript("-")
	if(d_delta<-1)stringiN+=RE_string_to_superscript(d_delta.toString())
	if(d_reg!=0)stringiN+="r"+(d_reg.toString())
//*/
}

function DATA_calculate_grade_interval_string(A_Na,A_diesis,A_Na_element_grades_data,B_Na,B_diesis,B_Na_element_grades_data){
	//calculate resto in grades
	let [grade_A,delta_A,reg_A]=DATA_calculate_absolute_note_to_grade(A_Na,A_diesis,A_Na_element_grades_data)
	let [grade_B,delta_B,reg_B]=DATA_calculate_absolute_note_to_grade(B_Na,B_diesis,B_Na_element_grades_data)
	//4 cases
	//let scale_module = BNAV_get_scale_from_acronym(A_Na_element_scale.acronym).module
	let scale_module = A_Na_element_grades_data.module
	let d_g=grade_B-grade_A
	let d_r=reg_B-reg_A
	let d_grad = d_g+d_r*scale_module
	return d_grad
}

function DATA_calculate_sound_translation_scale(sound,old_scale,new_scale,idea_scale_list,A_grade=false){
	let [grade,delta,reg]= DATA_calculate_absolute_note_to_scale(sound.note,sound.diesis,old_scale,idea_scale_list)
	let result = DATA_calculate_scale_note_to_absolute(grade,delta,reg,new_scale,idea_scale_list)
	//result.note result.outside_range
	let new_sound={note:result.note,diesis:sound.diesis,A_pos:JSON.parse(JSON.stringify(sound.A_pos)),A_neg:JSON.parse(JSON.stringify(sound.A_neg))}
	let outside_range=false
	if(result.outside_range)outside_range=true
	//A
	if(A_grade){
		//traduction A grade
		let A_pos_grade=[]
		let A_neg_grade=[]
		let provv_note=sound.note
		sound.A_pos.forEach(item=>{
			let current_note=provv_note+item
			let d_Ag = DATA_calculate_scale_interval_string(provv_note,sound.diesis,old_scale,current_note,sound.diesis,old_scale,idea_scale_list)
			A_pos_grade.push(d_Ag)
			provv_note=current_note
		})
		provv_note=sound.note
		sound.A_neg.forEach(item=>{
			let current_note=provv_note-item
			let d_Ag = DATA_calculate_scale_interval_string(current_note,sound.diesis,old_scale,provv_note,sound.diesis,old_scale,idea_scale_list)
			A_neg_grade.push(d_Ag)
			provv_note=current_note
		})
		let resultA=DATA_calculate_A_from_A_grade(result.note,grade,delta,reg,new_scale,idea_scale_list,A_pos_grade,A_neg_grade)
		//let resultA=DATA_calculate_A_from_A_grade(result.note,grade,0,reg,target_scale_list[index],idea_scale_list,data.A_pos_grade,data.A_neg_grade)
		new_sound.A_pos=resultA.A_pos
		new_sound.A_neg=resultA.A_neg
	}
	//verify A
	let o_r=DATA_verify_sound_A(sound)
	if(o_r)outside_range=true
	return {sound:new_sound,outside_range}
}

function DATA_calculate_A_from_sound_grade_change(sound,new_Na=null,new_diesis=null,old_scale,new_scale=null,idea_scale_list){
	//traduction A grade
	let A_pos_grade=[]
	let A_neg_grade=[]
	let provv_note=sound.note
	sound.A_pos.forEach(item=>{
		let current_note=provv_note+item
		let d_Ag = DATA_calculate_scale_interval_string(provv_note,sound.diesis,old_scale,current_note,sound.diesis,old_scale,idea_scale_list)
		A_pos_grade.push(d_Ag)
		provv_note=current_note
	})
	provv_note=sound.note
	sound.A_neg.forEach(item=>{
		let current_note=provv_note-item
		let d_Ag = DATA_calculate_scale_interval_string(current_note,sound.diesis,old_scale,provv_note,sound.diesis,old_scale,idea_scale_list)
		A_neg_grade.push(d_Ag)
		provv_note=current_note
	})
	if(new_Na==null)new_Na=sound.note
	if(new_scale==null)new_scale=old_scale
	if(new_diesis==null)new_diesis=sound.diesis
	let [grade,delta,reg]= DATA_calculate_absolute_note_to_scale(new_Na,new_diesis,new_scale,idea_scale_list)
	let resultA=DATA_calculate_A_from_A_grade(new_Na,grade,delta,reg,new_scale,idea_scale_list,A_pos_grade,A_neg_grade)
	return resultA
}

function DATA_calculate_A_from_A_grade(current_note,current_grade,current_delta,current_reg,current_scale,idea_scale_list,A_pos_grade,A_neg_grade){
	let A_pos=_A_grade_to_A_list(A_pos_grade,true,current_note,current_grade,current_delta,current_reg,current_scale)
	let A_neg=_A_grade_to_A_list(A_neg_grade,false,current_note,current_grade,current_delta,current_reg,current_scale)
	let returnA= {A_pos,A_neg}
	return returnA

	function _A_grade_to_A_list(A_grade,positive,current_note,current_grade,current_delta,current_reg,current_scale){
		let provv_grade=current_grade
		let provv_note=current_note
		let A_list=[]
		A_grade.forEach(item=>{
			//doesn't matter if reg doesnt change
			provv_grade=positive?(provv_grade+item):(provv_grade-item)
			let result = DATA_calculate_scale_note_to_absolute(provv_grade,0,current_reg,current_scale,idea_scale_list)
			let delta=Math.abs(provv_note-result.note)
			if(delta!=0)A_list.push(delta)
			provv_note=result.note
		})
		return A_list
	}
}

function DATA_verify_new_scale_name(string){
	let duplicate=global_scale_list.scale_list.some(item=>{
		let main_name=Object.keys(item.name).some(key => {
			return item.name[key]==string
		})
		let child_name=item.family.some(child => {
			return Object.keys(child.name).some(key => {
				return child.name[key]==string
			})
		})
		return (main_name || child_name)
	})
	if(duplicate)return string+"_mod"
	if(string=="")string=DATA_verify_new_scale_name("new")
	return string
}

function DATA_find_global_scale_from_iS_list(iS_list){
	let module=iS_list.length
	let scale_TET=iS_list.reduce((prop,item)=>{return item+prop},0)
	let iS_list_string=JSON.stringify(iS_list)
	let target=global_scale_list.scale_list.find(item=>{
		if(item.TET==scale_TET && item.module==module){
			//verify rotations too
			let iS_copy=JSON.parse(JSON.stringify(item.iS))
			for(let i=0;i<item.module;i++){
				if(JSON.stringify(iS_copy)==iS_list_string)return true
				iS_copy.push(iS_copy.shift())
			}
			return false
		}
	})
	// let target=global_scale_list.scale_list.find(item=>{
	// 	if(item.TET==scale_TET && item.module==module){
	// 		return JSON.stringify(item.iS)==iS_list_string
	// 	}
	// })
	if(target==null)return null
	return JSON.parse(JSON.stringify(target))
}

function DATA_find_user_scale_from_iS_list(iS_list){
	let module=iS_list.length
	let scale_TET=iS_list.reduce((prop,item)=>{return item+prop},0)
	let iS_list_string=JSON.stringify(iS_list)
	//exactly similar
	let target=user_scale_list.scale_list.find(item=>{
		if(item.TET==scale_TET && item.module==module){
			return JSON.stringify(item.iS)==iS_list_string
		}
	})
	if(target==null)return null
	return JSON.parse(JSON.stringify(target))
}


