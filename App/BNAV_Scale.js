function BNAV_refresh_PsLi(){
	let scales_length = document.getElementById('ES_scales_sum')
	//database
	let scale_sequence = DATA_get_scale_sequence()
	//var last_compas= compas_data.pop()
	let sum = scale_sequence.reduce((prop,item)=>{
		return parseInt(item.duration)+prop
	},0)
	scales_length.innerHTML = sum
	let scaleLiRep = document.getElementById('ES_idea_length')
	scaleLiRep.innerText = Li
	//check if ghost scale is needed
	BNAV_check_ghost(scale_sequence)
}

function BNAV_save_new_scale_data(scale_data=null,load=true){
	//read database
	let data = DATA_get_current_state_point(true)
	data.scale=(scale_data==null)?BNAV_read_current_scale_data():scale_data
	//verify scale ghost
	let last_compas= data.compas[data.compas.length-1]
	let end_last_compas_abs_pulse = last_compas.compas_values[0]+last_compas.compas_values[1]*last_compas.compas_values[2]
	let max_abs_pulse=(Li>end_last_compas_abs_pulse)?Li:end_last_compas_abs_pulse
	BNAV_check_ghost(data.scale.scale_sequence,max_abs_pulse)
	//save new database
	DATA_insert_new_state_point(data)
	//redraw/re align S lines
	if(load)DATA_load_state_point_data(false,true)
}

function BNAV_read_current_scale_data(){
	//current situation
	let scale_sequence = BNAV_read_current_scale_sequence()
	let idea_scale_list = BNAV_read_current_idea_scale_list()
	let scale = JSON.parse(JSON.stringify({idea_scale_list: idea_scale_list, scale_sequence: scale_sequence}))
	return scale
}

function BNAV_read_current_scale_sequence(){
	let parent = document.querySelector('.App_BNav_S_top_sequence')
	let scale_list_elements = [...parent.querySelectorAll(".App_BNav_S_obj:not(.ghost)")]
	let scale_sequence = scale_list_elements.map(item=>{
		return data = JSON.parse(item.getAttribute("value"))
	})
	return scale_sequence
}

function BNAV_read_current_idea_scale_list(){
	let container = document.getElementById("App_BNav_S_composition")
	let idea_scale_list_elements = [...container.querySelectorAll(".App_BNav_S_list_scale_family")]
	let idea_scale_list= idea_scale_list_elements.map(idea=>{return JSON.parse(idea.getAttribute("value"))})
	return idea_scale_list
}

/*SEQUENCE MANAGEMENT*/

function BNAV_write_scale_sequence(scale_sequence){
	let parent = document.querySelector('.App_BNav_S_top_sequence')
	let string=""
	scale_sequence.forEach(item=>{
		string+=BNAV_generate_scale_sequence_div(item)
	})
	parent.innerHTML=string
	BNAV_check_ghost(scale_sequence)
}

function BNAV_scroll_scale_sequence(event){
	let parent = document.querySelector('.App_BNav_S_top_sequence')
	parent.scrollLeft+=event.deltaY
}

function BNAV_check_ghost(scale_sequence,max_abs_pulse=null){
	//max_abs_pulse max (Li , sum compass)
	if(max_abs_pulse==null){
		let compas_sequence=DATA_get_compas_sequence()
		let last_compas= compas_sequence[compas_sequence.length-1]
		let end_last_compas_abs_pulse = last_compas.compas_values[0]+last_compas.compas_values[1]*last_compas.compas_values[2]
		max_abs_pulse=(Li>end_last_compas_abs_pulse)?Li:end_last_compas_abs_pulse
	}
	let parent = document.querySelector('.App_BNav_S_top_sequence')
	let total_scale_duration=scale_sequence.reduce((prop,item)=>{return item.duration+prop},0)
	let ghost=parent.querySelector(".App_BNav_S_obj.ghost")
	if(ghost!=null)ghost.remove(ghost)
	//add ghost
	if(total_scale_duration>=max_abs_pulse && scale_sequence.length!=0){
		//add last add button
		let ghost_string = `<div class="App_BNav_S_obj ghost add_last">
						<button class="App_BNav_S_scale_add" onclick="BNAV_add_last_scale_sequence()">
							<img src="./Icons/add_black.svg">
						</button>
					</div>`
		let div = document.createElement("div")
		div.innerHTML=ghost_string
		let ghost_div=div.firstChild
		parent.appendChild(ghost_div)
		div.remove()
		return
	}
	if(total_scale_duration<max_abs_pulse && scale_sequence.length!=0){
		let ghost_data=JSON.parse(JSON.stringify(scale_sequence.slice(-1)[0]))
		ghost_data.duration=max_abs_pulse-total_scale_duration
		_add_ghost(ghost_data,false)
		//problematic x input modification
		//parent.innerHTML+=ghost_string
		return
	}
	if(scale_sequence.length==0){
		//chromatic ghost
		let ghost_data={"acronym": "","duration": max_abs_pulse }
		//let ghost_data={"acronym": "TET","duration": Li }
		_add_ghost(ghost_data,true)
	}
	function _add_ghost(ghost_data,chrom){
		let ghost_string = BNAV_generate_scale_ghost_div(ghost_data,chrom)
		let div = document.createElement("div")
		div.innerHTML=ghost_string
		let ghost_div=div.firstChild
		parent.appendChild(ghost_div)
		div.remove()
	}
}

function BNAV_generate_scale_sequence_div(data,extra_class=""){
	//creation of the div
	let data_string = JSON.stringify(data)
	let div_string=`<div class="App_BNav_S_obj ${extra_class}" value='${data_string}' onclick="BNAV_select_scale_sequence_button(this,true)"
						ondragstart="BNAV_drag_start_scale_sequence_div(this,event)" ondragend="BNAV_drag_end_scale_sequence_div(this)"
						ondragover="BNAV_drag_over_scale_sequence_div(event)" draggable="true">
						<button class="App_BNav_S_scale_add" onclick="BNAV_add_scale_sequence_button(this)">
							<img src="./Icons/add_black.svg">
						</button>
						<div class="App_BNav_S_scale">
							<div class="App_BNav_S_value" onclick="BNAV_show_scale_circle(this);event.stopPropagation()">
								<p>${data.acronym}${data.rotation}.${data.starting_note}</p>
							</div>
							<div class="App_BNav_S_duration"><p>${data.duration}</p>
							</div>
						</div>
						<button class="App_BNav_S_scale_delete" onclick="BNAV_delete_scale_sequence_button(this)">
							<img src="./Icons/delete_black.svg">
						</button>
					</div>`
	return div_string
	//
}

function BNAV_generate_scale_ghost_div(data,chrom=false){
	let data_string = JSON.stringify(data)
	let inner_string=""
	if(chrom){
		inner_string=`<p>${data.acronym}</p>`
	}else{
		inner_string=`<p>${data.acronym}${data.rotation}.${data.starting_note}</p>`
	}
	let div_string=`<div class="App_BNav_S_obj ghost" value='${data_string}'>
						<button class="App_BNav_S_scale_add" onclick="BNAV_add_scale_sequence_button(this)">
							<img src="./Icons/add_black.svg">
						</button>
						<div class="App_BNav_S_scale">
							<div class="App_BNav_S_value">
								${inner_string}
							</div>
							<div class="App_BNav_S_duration"><p class="App_BNav_sub_S_duration">${data.duration}</p></div>
						</div>
					</div>`
	return div_string
}

function BNAV_add_first_scale_sequence(){
	let container=document.querySelector(".App_BNav_S_top_sequence")
	//open scale tab
	let ES_button = document.getElementById("App_BNav_tab_ES")
	ES_button.click()
	let sequence_item = container.querySelector(".App_BNav_S_obj")
	if(!sequence_item.classList.contains("ghost"))return
	//try add something
	let button =  sequence_item.querySelector(".App_BNav_S_scale_add")
	BNAV_add_scale_sequence_button(button)
}

function BNAV_add_last_scale_sequence(){
	let container=document.querySelector(".App_BNav_S_top_sequence")
	//open scale tab
	let ES_button = document.getElementById("App_BNav_tab_ES")
	ES_button.click()
	let sequence_item = container.querySelector(".App_BNav_S_obj.ghost")
	if(sequence_item==null)return
	//try add something
	let button =  sequence_item.querySelector(".App_BNav_S_scale_add")
	if(sequence_item.classList.contains("add_last")){
		//add duration=1
		let selected_scale=BNAV_insert_scale_use_current_selected_scale_data()
		if(selected_scale!=null){
			let scale_sequence=DATA_get_scale_sequence()
			scale_sequence.push(selected_scale)
			let scale_data={"scale_sequence":scale_sequence,"idea_scale_list":DATA_get_idea_scale_list()}
			BNAV_save_new_scale_data(scale_data,true)
			BNAV_find_and_select_scale_composition(selected_scale.acronym,selected_scale.rotation,selected_scale.starting_note)
		}
	}else{
		BNAV_add_scale_sequence_button(button)
	}
}

function BNAV_add_scale_sequence_button(button){
	let sequence_item = button.closest(".App_BNav_S_obj")
	let scale_data = JSON.parse(sequence_item.getAttribute("value"))
	let is_first=scale_data.rotation==null
	let selected_scale=BNAV_insert_scale_use_current_selected_scale_data()
	if(selected_scale!=null){
		let old_duration=scale_data.duration
		scale_data=selected_scale
		if(old_duration!=0)scale_data.duration=old_duration
	}
	if(is_first){
		let compas_sequence=DATA_get_compas_sequence()
		let last_compas= compas_sequence[compas_sequence.length-1]
		let end_last_compas_abs_pulse = last_compas.compas_values[0]+last_compas.compas_values[1]*last_compas.compas_values[2]
		let max_abs_pulse=(Li>end_last_compas_abs_pulse)?Li:end_last_compas_abs_pulse
		if(scale_data.acronym!=""){
			scale_data.duration=max_abs_pulse
		}else{
			//find first letter
			let idea_scale_list=DATA_get_idea_scale_list()
			if(idea_scale_list.length==0)return
			let first_scale=idea_scale_list[0]
			if(first_scale==null)return
			scale_data={ "acronym": first_scale.acronym, "starting_note": 0, "rotation": 0, "duration": max_abs_pulse}
		}
	}
	//duplicate and
	let new_scale_string=BNAV_generate_scale_sequence_div(scale_data)
	sequence_item.outerHTML=new_scale_string+sequence_item.outerHTML
	BNAV_save_new_scale_data()
	BNAV_refresh_S_composition(DATA_get_scale_data())
	BNAV_find_and_select_scale_composition(scale_data.acronym,scale_data.rotation,scale_data.starting_note)
	//BNAV_update_S_selected(false,selected_data)
	if(is_first)BNAV_update_S_selected(true)
}

function BNAV_delete_scale_sequence_button(button){
	let sequence_item = button.closest(".App_BNav_S_obj")
	let scale_duration = JSON.parse(sequence_item.getAttribute("value")).duration
	let prev_sequence_item=sequence_item.previousSibling
	sequence_item.innerHTML = ""
	sequence_item.remove(sequence_item)
	if(prev_sequence_item!=null){
		//change duration
		let scale_data = JSON.parse(prev_sequence_item.getAttribute("value"))
		scale_data.duration += scale_duration
		prev_sequence_item.setAttribute("value",JSON.stringify(scale_data))
		prev_sequence_item.querySelector(".App_BNav_S_duration p").innerHTML=scale_data.duration
	}
	BNAV_save_new_scale_data()
	BNAV_refresh_S_composition(DATA_get_scale_data())
}

function BNAV_show_scale_circle(S_value){
	APP_hide_scale_circle_elements()
	let sequence_item = S_value.closest(".App_BNav_S_obj")
	let scale_data = JSON.parse(sequence_item.getAttribute("value"))
	let [scale_index,]=RE_element_index(sequence_item,".App_BNav_S_obj")
	//add circle container //trigger mouse leave
	sequence_item.innerHTML+=BNAV_calculate_S_circle_container(scale_data,scale_index)
	//correct circle position
	let circle=sequence_item.querySelector(".App_BNav_S_circle_container")
	circle.style.bottom="calc(var(--RE_block) * 6)"
	let left_position=scale_index*(nuzic_block*3)+(nuzic_block*3)-sequence_item.closest(".App_BNav_S_top_sequence").scrollLeft
	//sequence_item.getBoundingClientRect().x//parent.getBoundingClientRect().left-App_RE.getBoundingClientRect().left
	circle.style.left=left_position+"px"
}

function BNAV_delete_scale_circle(){
	//no pink line
	let circle_list=[...document.querySelectorAll(".App_BNav_S_circle_container")]
	circle_list.forEach(item=>{
		item.remove()
	})
}

function BNAV_calculate_S_circle_container(scale_data,scale_index){
	return	`<div class="App_BNav_S_circle_container" onclick="event.stopPropagation()">
							<div class="App_BNav_S_circle">
								<div class="App_BNav_S_circle_top">
									<p>N</p>
									<label class="App_BNav_S_circle_checkbox">
										<input type="checkbox" onclick="BNAV_S_circle_checkbox(this,${scale_index})">
										<span></span>
									</label>
									<p>Nº</p>
								</div>
								<div class="App_BNav_S_circle_middle">
									<input class="App_BNav_S_scale_acronym" maxlength="1" onfocusin="APP_set_previous_value(this)" onkeypress="APP_inpTest_acronym(event)" value="${scale_data.acronym}">
									<input class="App_BNav_S_scale_rot" maxlength="2" oninput="BNAV_S_circle_show_apply_button(this)" onfocusin="APP_set_previous_value(this)" onkeypress="APP_inpTest_intPos(event,this)" onkeyup="BNAV_calculate_S_circle_rotation_starting_note(this,'${scale_data.acronym}',${TET})" value="${scale_data.rotation}">
									<input class="App_BNav_S_scale_starting_note" maxlength="2" oninput="BNAV_S_circle_show_apply_button(this)" onfocusin="APP_set_previous_value(this)" onkeypress="APP_inpTest_intPos(event,this)" value="${scale_data.starting_note}">
								</div>
								<div class="App_BNav_S_circle_bottom">
									<input class="App_BNav_S_scale_duration" maxlength="3" oninput="BNAV_S_circle_show_apply_button(this)" onfocusin="APP_set_previous_value(this)" onkeypress="APP_inpTest_intPos(event,this)" value="${scale_data.duration}">
								</div>
							</div>
							<button class="App_BNav_S_scale_apply" onclick="BNAV_S_circle_apply_button(this,${scale_index})"><img src="./Icons/selected.svg"></button>
						</div>`
}

function BNAV_calculate_S_circle_container_insert(result){
	return	`<div class="App_BNav_S_circle_container" onclick="event.stopPropagation()">
							<div class="App_BNav_S_circle">
								<div class="App_BNav_S_circle_top">
									<p>N</p>
									<label class="App_BNav_S_circle_checkbox">
										<input type="checkbox"">
										<span></span>
									</label>
									<p>Nº</p>
								</div>
								<div class="App_BNav_S_circle_middle">
									<input class="App_BNav_S_scale_acronym" maxlength="1" onfocusin="APP_set_previous_value(this)" onkeypress="APP_inpTest_acronym(event)" value="${result.insert_scale.acronym}">
									<input class="App_BNav_S_scale_rot" maxlength="2" oninput="BNAV_S_circle_show_apply_button(this)" onfocusin="APP_set_previous_value(this)" onkeypress="APP_inpTest_intPos(event,this)" onkeyup="BNAV_calculate_S_circle_rotation_starting_note(this,'${result.insert_scale.acronym}',${TET})" value="${result.insert_scale.rotation}">
									<input class="App_BNav_S_scale_starting_note" maxlength="2" oninput="BNAV_S_circle_show_apply_button(this)" onfocusin="APP_set_previous_value(this)" onkeypress="APP_inpTest_intPos(event,this)" value="${result.insert_scale.starting_note}">
								</div>
								<div class="App_BNav_S_circle_bottom">
									<input class="App_BNav_S_scale_duration" maxlength="3" oninput="BNAV_S_circle_show_apply_button(this)" onfocusin="APP_set_previous_value(this)" onkeypress="APP_inpTest_intPos(event,this)" value="${result.insert_scale.duration}" disabled>
								</div>
							</div>
							<button class="App_BNav_S_scale_apply show" onclick='BNAV_S_circle_apply_insert_button(this,${JSON.stringify(result)})'><img src="./Icons/selected.svg"></button>
						</div>`
}

function BNAV_S_circle_checkbox(checkbox,scale_index){
	let container=checkbox.closest(".App_BNav_S_circle_container")
	let duration_input=container.querySelector(".App_BNav_S_scale_duration")
	if(checkbox.checked){
		//block duration
		duration_input.disabled=true
		let current_scale_data=DATA_get_scale_sequence()[scale_index]
		duration_input.value=current_scale_data.duration
	}else{
		//unblock duration
		duration_input.disabled=false
	}
}

function BNAV_calculate_S_circle_rotation_starting_note(input_box,scale_acronym,current_TET){
	let idea_scale_list=DATA_get_idea_scale_list()
	let scale_info=idea_scale_list.find(item=>{
		return item.acronym===scale_acronym
	})
	let module=scale_info.module
	//find current starting note
	let starting_note_input=input_box.parentNode.querySelector(".App_BNav_S_scale_starting_note")
	let starting_note=parseInt(starting_note_input.value)
	let current_rotation=parseInt(input_box.value)
	let previous_rotation=parseInt(previous_value)
	if(isNaN(current_rotation))current_rotation=0
	if(isNaN(previous_rotation))previous_rotation=0
	current_rotation=current_rotation%module
	previous_rotation=previous_rotation%module
	let delta_note=scale_info.iS.reduce((prop,item,index)=>{
		let iS=(index<current_rotation)?item:0
		return iS+prop},0)-scale_info.iS.reduce((prop,item,index)=>{
		let iS=(index<previous_rotation)?item:0
		return iS+prop},0)
	previous_value= current_rotation
	starting_note=(starting_note+delta_note)%current_TET
	if(starting_note<0)starting_note=current_TET+starting_note
	starting_note_input.value=starting_note
}

function BNAV_S_circle_show_apply_button(input){
	let container=input.closest(".App_BNav_S_circle_container")
	let apply_button=container.querySelector(".App_BNav_S_scale_apply")
	apply_button.classList.add("show")
}

function BNAV_S_circle_apply_button(button,scale_index){
	let container=button.closest(".App_BNav_S_circle_container")
	let checkbox=container.querySelector(".App_BNav_S_circle_checkbox input")
	let duration=parseInt(container.querySelector(".App_BNav_S_scale_duration").value)
	let acronym=container.querySelector(".App_BNav_S_scale_acronym").value
	let starting_note=parseInt(container.querySelector(".App_BNav_S_scale_starting_note").value)
	let rotation=parseInt(container.querySelector(".App_BNav_S_scale_rot").value)
	if(acronym=="" || isNaN(duration) || isNaN(starting_note) || isNaN(rotation))return
	let scale_data={acronym,starting_note,rotation,duration}
	if(checkbox.checked){
		//translate grade
		DATA_translate_scale_sequence_at_index(scale_index,scale_data)
	}else{
		//mantain note
		DATA_modify_scale_sequence_at_index(scale_index,scale_data)
	}
	APP_hide_scale_circle_elements()
}

function BNAV_S_circle_apply_insert_button(button,insert_data){
	//modify indest data with correct info
	let container=button.closest(".App_BNav_S_circle_container")
	let checkbox=container.querySelector(".App_BNav_S_circle_checkbox input")
	let acronym=container.querySelector(".App_BNav_S_scale_acronym").value
	let starting_note=parseInt(container.querySelector(".App_BNav_S_scale_starting_note").value)
	let rotation=parseInt(container.querySelector(".App_BNav_S_scale_rot").value)
	if(acronym=="" || isNaN(starting_note) || isNaN(rotation))return
	insert_data.insert_scale.acronym=acronym
	insert_data.insert_scale.starting_note=starting_note
	insert_data.insert_scale.rotation=rotation
	insert_data.translate_grade=checkbox.checked
	DATA_insert_scale_at_pulse_extra(insert_data)
	//APP_hide_scale_circle_elements()
}

function BNAV_drag_start_scale_sequence_div(element,event){
	element.classList.add('dragging')
	let canvas = document.createElement('canvas')
	let context = canvas. getContext('2d')
	context. clearRect(0, 0, canvas. width, canvas. height)
	event.dataTransfer.setDragImage(canvas, 10, 10)
}

function BNAV_drag_over_scale_sequence_div(event){
	event.preventDefault()
	let scale_sequence_container = document.querySelector(".App_BNav_S_top_sequence")
	let after_element = _get_scale_drag_after_element(scale_sequence_container, event.clientX)
	let dragover_scale_sequence_div = scale_sequence_container.querySelector('.dragging')
	if (after_element == null){
		let ghost = scale_sequence_container.querySelector('.App_BNav_S_obj.ghost')
		if(ghost==null){
			scale_sequence_container.appendChild(dragover_scale_sequence_div)
		}else{
			scale_sequence_container.insertBefore(dragover_scale_sequence_div, ghost)
		}
	} else {
		scale_sequence_container.insertBefore(dragover_scale_sequence_div, after_element)
	}

	function _get_scale_drag_after_element (container, x) {
		let draggableElements = [...container.querySelectorAll('.App_BNav_S_obj:not(.dragging)')]
		return draggableElements.reduce((closest, child) => {
			let box = child.getBoundingClientRect()
			let offset = x -box.left - box.width / 2
			if(offset < 0 && offset > closest.offset){
				return { offset: offset, element: child }
			} else {
				return closest
			}
		},{offset: Number.NEGATIVE_INFINITY}).element
	}
}

function BNAV_drag_end_scale_sequence_div(element){
	element.classList.remove('dragging')
	element.style.border=""
	BNAV_save_new_scale_data()
}

/* SCALE MANAGMENT */

function BNAV_read_view_option(){
	let state=document.querySelector(".App_BNav_S_list_title_show_family").value
	if(state=="0")return "single"
	if(state=="1")return "family"
}

function BNAV_scale_list_change_view_button(button){
	button.value=(button.value=="0")?"1":"0"
	BNAV_refresh_scale_list()
}

function BNAV_show_hide_scale_family(button,current_view_option){
	let view_option="single"
	let family_div=button.closest(".App_BNav_S_list_scale_family")
	let scale_info=JSON.parse(family_div.getAttribute("value"))
	let scale_global=JSON.parse(family_div.classList.contains("global"))
	if(current_view_option=="single" && scale_info.family.length!=0)view_option="family"
	if(current_view_option=="single" && scale_info.family.length==0)view_option="all"
	if(current_view_option=="family" && scale_info.module>scale_info.family.length+1)view_option="all"
	//if(current_view_option=="family" && scale_info.module>scale_info.family.length+1 && !scale_global)view_option="all"
	let lang=current_language
	family_div.outerHTML=BNAV_calculate_scale_family_string(scale_info,scale_global,view_option,lang)
}

function BNAV_next_TET_scale_list(){
	let TET_inp_box = document.getElementById("App_BNav_S_list_title_TET_value")
	let current_TET = parseInt(TET_inp_box.innerHTML)
	let TET_list_all=BNAV_generate_scale_TET_list_available()
	let current_TET_index = TET_list_all.indexOf(current_TET)
	if(current_TET_index+1<TET_list_all.length){
		TET_inp_box.innerHTML = TET_list_all[current_TET_index+1]
		//display new list
		BNAV_refresh_scale_list()
	}
}

function BNAV_previous_TET_scale_list(){
	let TET_inp_box = document.getElementById("App_BNav_S_list_title_TET_value")
	let current_TET = parseInt(TET_inp_box.innerHTML)
	let TET_list_all=BNAV_generate_scale_TET_list_available()
	let current_TET_index = TET_list_all.indexOf(current_TET)
	if(current_TET_index-1>=0){
		TET_inp_box.innerHTML = TET_list_all[current_TET_index-1]
		//display new list
		BNAV_refresh_scale_list()
	}
}

function BNAV_generate_scale_TET_list_available(){
	let TET_list_all=[TET,...global_scale_list.TET_list,...user_scale_list.TET_list]
	TET_list_all=[...new Set(TET_list_all)]
	return TET_list_all.sort((a, b) => a - b)
}

function BNAV_reset_TET_scale_list(){
	let TET_inp_box = document.getElementById("App_BNav_S_list_title_TET_value")
	let current_TET=TET
	let current_TET_index = global_scale_list.TET_list.indexOf(current_TET)
	//if(current_TET_index!=-1){
	TET_inp_box.innerHTML = TET
	//display new list
	BNAV_refresh_scale_list()
}

function BNAV_close_scale_list(){
	let S_list = document.querySelector(".App_BNav_S_list")
	S_list.classList.toggle("closed")
	DATA_save_preferences_file()
}

function BNAV_read_show_scale_list(){
	let S_list = document.querySelector(".App_BNav_S_list")
	return !S_list.classList.contains("closed")
}

function BNAV_set_show_scale_list(show){
	let S_list = document.querySelector(".App_BNav_S_list")
	if(show==null || show){
		S_list.classList.remove("closed")
	}else{
		S_list.classList.add("closed")
	}
}

//FILTER FUNCTIONS

function BNAV_scale_list_filter(filter){
	filter.classList.toggle("selected")
	BNAV_refresh_scale_list()
	DATA_save_preferences_file()
}

function BNAV_read_show_global_scale(){
	let filter_button_list=[...document.querySelectorAll(".App_BNav_S_list_title_filter")]
	return filter_button_list[0].classList.contains("selected")
}

function BNAV_set_show_global_scale(show){
	let filter_button_list=[...document.querySelectorAll(".App_BNav_S_list_title_filter")]
	if(show==null || show){
		filter_button_list[0].classList.add("selected")
	}else{
		filter_button_list[0].classList.remove("selected")
	}
}

function BNAV_read_show_user_scale(){
	let filter_button_list=[...document.querySelectorAll(".App_BNav_S_list_title_filter")]
	return filter_button_list[1].classList.contains("selected")
}

function BNAV_set_show_user_scale(show){
	let filter_button_list=[...document.querySelectorAll(".App_BNav_S_list_title_filter")]
	if(show==null || show){
		filter_button_list[1].classList.add("selected")
	}else{
		filter_button_list[1].classList.remove("selected")
	}
}

function BNAV_refresh_scale_list(){
	// use visible_scale_list
	let sound_scale_div=document.querySelector(".App_BNav_S_list")
	let lang=current_language
	//not only global!!! depend on selection!!! XXX XXX XXX XXX
	let show_TET
	let TET_scale_list = document.getElementById("App_BNav_S_list_title_TET_value")
	if(TET_scale_list.innerHTML==0){//ok for empty string
		show_TET=TET
		TET_scale_list.innerHTML=show_TET
	}else{
		show_TET=parseInt(TET_scale_list.innerHTML)
	}
	let view_option=BNAV_read_view_option()
	//visible_scale_list=wanted_scale_list
	let container = sound_scale_div.querySelector(".App_BNav_S_list_content")
	let container_string=""
	let filter_button_list=[...document.querySelectorAll(".App_BNav_S_list_title_filter")]
	if(filter_button_list[0].classList.contains("selected")){
		//add community
		global_scale_list.scale_list.forEach(scale=>{
			if(scale.TET==show_TET){
				let scale_global=true
				container_string+= BNAV_calculate_scale_family_string(scale,scale_global,view_option,lang)
			}
		})
	}
	if(user_scale_list!=null)BNAV_generate_TET_list_from_scale_list(user_scale_list)
	if(filter_button_list[1].classList.contains("selected") && user_scale_list!=null){
		//add user
		user_scale_list.scale_list.forEach(scale=>{
			if(scale.TET==show_TET){
				let scale_global=false
				container_string+= BNAV_calculate_scale_family_string(scale,false,view_option,lang)
			}
		})
	}
	container.innerHTML=container_string
	//BNAV_order_scale_bookmark_list()
}

function BNAV_calculate_scale_family_string(scale,scale_global,view_option,lang){
	let item_string = JSON.stringify(scale)
	let scale_state_class=(scale_global)?" global":" user"
	let svg_string=((view_option=="family" && scale.family.length!=0) || (view_option=="all" && scale.module!=1))?'<svg ><line x1="14" y1="14" x2="14" y2="28" style="stroke-width:3"></line></svg>':""
	let mother_iS_string=BNAV_scale_list_iS_div_gen(scale,0)
	let idea_scale_list=DATA_get_idea_scale_list()
	let acronym="+"
	let acronym_option=0
	if(TET==scale.TET){
		let used=idea_scale_list.find(item=>{
			if(item.module==scale.module){
				acronym_option=(JSON.stringify(scale.iS)==JSON.stringify(item.iS))?1:0
				//verify names family
				if(acronym_option==1 && item.family.length==scale.family.length){
					let different=item.family.some((fam,index)=>{
						if(scale_global){
							return fam.name!=scale.family[index].name[current_language] || fam.rotation!=scale.family[index].rotation
						}else{
							return fam.name!=scale.family[index].name || fam.rotation!=scale.family[index].rotation
						}
					})
					acronym_option=(!different)?2:acronym_option
				}
				return acronym_option>0
			}
			return false
		})
		if(used!=null){
			acronym=used.acronym
		}
	}
	let family_string=`<div class="App_BNav_S_list_scale_family${scale_state_class}" value='${item_string}' acronym="${acronym}">`
	let name=(scale_global)?scale.name[lang]:scale.name
	//delete button //op_menu_delete.svg
	let delete_button_string_mother=(!scale_global)?`<button class="App_BNav_S_list_scale_delete" onclick="BNAV_delete_user_scale_button(this,event);"><img src="./Icons/delete.svg"></button>`:`<div class="App_BNav_S_list_scale_delete"></div>`
	let delete_button_string_child=`<div class="App_BNav_S_list_scale_delete"></div>`
	family_string+=BNAV_calculate_scale_family_member_string(true,svg_string,0,name,scale,acronym,view_option,acronym_option,delete_button_string_mother)
	if(view_option=="family"){
		let last_index=scale.family.length-1
		scale.family.forEach((item,index)=>{
			let svg_string=(index!=last_index)?'<svg ><line x1="14" y1="0" x2="14" y2="28" style="stroke-width:3"></line></svg>':'<svg ><line x1="14" y1="0" x2="14" y2="14" style="stroke-width:3"></line></svg>'
			let child_name=(scale_global)?item.name[lang]:item.name
			family_string+=BNAV_calculate_scale_family_member_string(false,svg_string,item.rotation,child_name,scale,acronym,null,acronym_option,delete_button_string_child)
		})
	}
	if(view_option=="all"){
		let last_index=scale.module-1
		for(let rot=1; rot<=last_index;rot++){
			let family_data=scale.family.find(item=>{return item.rotation==rot})
			let svg_string=(rot!=last_index)?'<svg ><line x1="14" y1="0" x2="14" y2="28" style="stroke-width:3"></line></svg>':'<svg ><line x1="14" y1="0" x2="14" y2="14" style="stroke-width:3"></line></svg>'
			if(family_data!=null){
				let child_name=(scale_global)?family_data.name[lang]:family_data.name
				family_string+=BNAV_calculate_scale_family_member_string(false,svg_string,rot,child_name,scale,acronym,null,acronym_option,delete_button_string_child)
			}else{
				family_string+=BNAV_calculate_scale_family_member_string(false,svg_string,rot,"",scale,acronym,null,acronym_option,delete_button_string_child)
			}
		}
	}
	family_string+="</div>"
	return family_string
}

function BNAV_calculate_scale_family_member_string(ismother,svg_string,rotation,name,scale,acronym,view_option,acronym_option,delete_button_string){
	let icon_string=(ismother)?'<img src="Icons/crown.svg"></img>':`${rotation}`
	let icon_function=(ismother)?`onclick="BNAV_show_hide_scale_family(this,'${view_option}');event.stopPropagation()"`:''
	let iS_string=BNAV_scale_list_iS_div_gen(scale,rotation)
	let add_function_string=(acronym_option<2)?' onclick="BNAV_scale_list_add_button(this);event.stopPropagation()"':"+"
	let add_class_string=""
	if(acronym_option==1)add_class_string=" used_similar"
	if(acronym_option==2)add_class_string=" used"
	//add button
	let add_button_string=(ismother && scale.TET==TET)?`<button class="App_BNav_S_list_scale_add${add_class_string}"${add_function_string}>${acronym}</button>`:""
	let string=`<div class="App_BNav_S_list_scale" rotation="${rotation}" onclick="BNAV_select_scale(this);event.stopPropagation()" ondblclick="BNAV_quick_add_scale_to_scale_sequence(this);event.stopPropagation()">
					<div class="App_BNav_S_list_scale_icon" ${icon_function}>
						${svg_string}
						<div>${icon_string}</div>
					</div>
					<div class="App_BNav_S_list_scale_name">${name}</div>
					<div class="App_BNav_S_list_scale_iS">
						${iS_string}
					</div>
					${delete_button_string}
					<div class="App_BNav_S_list_scale_button_container">
						<button class="App_BNav_S_list_scale_play" onclick="BNAV_play_scale_button(this);event.stopPropagation()">
							<img src="Icons/play_mini.svg"></img>
						</button>
						${add_button_string}
					</div>
				</div>`
	return string
}

function BNAV_select_scale(div,force=false){
	//div.classList.toggle("selected")//+complexity
	if(div.classList.contains("scale_selected") && !force){
		//deselect
		BNAV_clear_selected_scale()
	}else{
		//remove whatever is selected
		let selected_objects_list=[...document.querySelector(".App_BNav_S_bottom").querySelectorAll(".scale_selected")]
		selected_objects_list.forEach(item=>{item.classList.remove("scale_selected")})
		div.classList.add("scale_selected")
		BNAV_update_S_selected(true,null)
	}
}

function BNAV_clear_selected_scale(){
	//div.classList.toggle("selected")//+complexity
	//remove whatever is selected
	let selected_objects_list=[...document.querySelector(".App_BNav_S_bottom").querySelectorAll(".scale_selected")]
	selected_objects_list.forEach(item=>{item.classList.remove("scale_selected")})
	BNAV_update_S_selected(false,null)
}

function BNAV_delete_user_scale_button(button,event){
	if(!button.closest(".App_BNav_S_list_scale").classList.contains("scale_selected"))return
	let scale_div= button.closest(".App_BNav_S_list_scale_family")
	//no need
	//if(!scale_div.classlist.contains("user"))return
	let selected_data=JSON.parse(scale_div.getAttribute("value"))
	let scale_string=JSON.stringify(selected_data)
	let target=user_scale_list.scale_list.find(item=>{
		if(item.TET==selected_data.TET && item.module==selected_data.module){
			return JSON.stringify(item)==scale_string
		}
	})
	if(target!=null){
		let index=user_scale_list.scale_list.indexOf(target)
		user_scale_list.scale_list.splice(index,1)
		DATA_save_user_scale_list()
		BNAV_refresh_scale_list()
		BNAV_refresh_editor()
	}
	event.stopPropagation()
}

function BNAV_insert_scale_use_current_selected_scale_data(){
	// let selected_scale=document.querySelector(".App_BNav_S_bottom").querySelector(".scale_selected")
	// if(selected_scale==null)return null
	let S_selected_div= document.getElementById("App_BNav_S_selected")
	let selected_data=JSON.parse(S_selected_div.getAttribute("value"))
	if(selected_data.acronym==""){
		//VERIFY IF THE CURRENT SCALE IS legit ; ELSE OVERWRITE with GLOBAL
		// if(selected_data.global){
		// 	console.error("overwrite with global scale info")
		// }
		return null
	}
	return { "acronym":selected_data.acronym, "starting_note":selected_data.starting_note, "rotation":selected_data.rotation, "duration": 1 }
}

function BNAV_quick_add_scale_to_scale_sequence(div){
	//make sure div is selected
	if(div.classList.contains("App_BNav_composition_scale_add")){
		BNAV_select_composition_scale(div,true)
		//BNAV_scale_list_add_button
	}else{
		BNAV_select_scale(div,true)
	}
	//use selection
	//verify if this scale does not already exist
	let selected_scale=BNAV_insert_scale_use_current_selected_scale_data()
	//nonono only if not exist BNAV_use_selected_scale()//MUST DO ALL THE  trick??'
	let idea_scale_list=DATA_get_idea_scale_list()
	let scale_sequence = DATA_get_scale_sequence()
	if(selected_scale==null){
		//add to idea_scale_list
		BNAV_use_selected_scale()
		selected_scale=BNAV_insert_scale_use_current_selected_scale_data()
		idea_scale_list=DATA_get_idea_scale_list()
	}
	//identify what sequence is selected
	let scale_sequence_container = document.querySelector(".App_BNav_S_top_sequence")
	let button_selected = scale_sequence_container.querySelector(".checked")
	let new_scale_index=0
	if(button_selected){
		let target_index=[...scale_sequence_container.querySelectorAll(".App_BNav_S_obj")].indexOf(button_selected)
		selected_scale.duration=scale_sequence[target_index].duration
		new_scale_index=target_index+1
		scale_sequence.splice(new_scale_index,0,selected_scale)
	}else{
		//place last
		let compas_sequence=DATA_get_compas_sequence()
		let last_compas= compas_sequence[compas_sequence.length-1]
		let end_last_compas_abs_pulse = last_compas.compas_values[0]+last_compas.compas_values[1]*last_compas.compas_values[2]
		let max_abs_pulse=(Li>end_last_compas_abs_pulse)?Li:end_last_compas_abs_pulse
		let total_scale_duration=scale_sequence.reduce((prop,item)=>{return item.duration+prop},0)
		let delta_duration=max_abs_pulse-total_scale_duration
		let duration = (delta_duration<=0)? 1 : delta_duration
		selected_scale.duration=duration
		scale_sequence.push(selected_scale)
		new_scale_index=scale_sequence.length-1
	}
	let scale_data={"scale_sequence":scale_sequence,"idea_scale_list":idea_scale_list}
	BNAV_save_new_scale_data(scale_data,true)
	BNAV_find_and_select_scale_composition(selected_scale.acronym,selected_scale.rotation,selected_scale.starting_note)
	//selection, true move RE or PMC in position
	BNAV_select_scale_number_sequence_list(new_scale_index,true)
}

function BNAV_scale_list_add_button(button){
	let parent=button.closest(".App_BNav_S_list_scale")
	BNAV_select_scale(parent,true)
	BNAV_use_selected_scale()
	event.stopPropagation()
}

//selection from circle
function BNAV_select_composition_scale(div,force=false){
	let scale_parent= div.closest(".App_BNav_composition_scale")
	if(div.classList.contains("scale_selected") && !force){
		//deselect
		div.classList.remove("scale_selected")
		scale_parent.classList.remove("scale_selected")
		BNAV_update_S_selected(false,null)
	}else{
		//remove whatever is selected
		let selected_objects_list=[...document.querySelector(".App_BNav_S_bottom").querySelectorAll(".scale_selected")]
		selected_objects_list.forEach(item=>{item.classList.remove("scale_selected")})
		div.classList.add("scale_selected")
		scale_parent.classList.add("scale_selected")
		BNAV_update_S_selected(true,null)
	}
}

function BNAV_select_scale_sequence_button(button,move_RE_H_scrollbar=false){
	//find all other buttons and uncheck them
	let container = button.closest(".App_BNav_S_top_sequence")
	if(container==null){
		//eliminated button
		return
	}
	let checked_button_list = [...container.querySelectorAll(".checked")]
	let checked = checked_button_list.some(item=>{return item==button})
	checked_button_list.forEach(item=>{
		item.classList.remove('checked')
	})
	//var circle_data = {acronym:acronym,starting_note:0,rotation:0,duration:""}
	if(!checked){
		button.classList.add('checked')
		//move horizontal scrollbar to correct point
		if(move_RE_H_scrollbar){
			let button_list = [...container.querySelectorAll(".App_BNav_S_obj")]
			let index= button_list.indexOf(button)
			BNAV_move_scrollbar_to_selected_scale_number(index)
		}
	}
}

function BNAV_move_scrollbar_to_selected_scale_number(index){
	if(tabREbutton.checked){
		RE_show_scale_number_S_line(index)
	}else{
		PMC_show_scale_number_S_line(index)
	}
}

//selection from RE and PMC
function BNAV_select_scale_number_sequence_list(index,move_PMC_RE=false){
	let scale_list_div = document.querySelector('.App_BNav_S_top_sequence')
	let scale_list = [...scale_list_div.querySelectorAll(".App_BNav_S_obj")]
	let selected = scale_list_div.querySelector(".checked")
	if(selected!=null)selected.classList.remove('checked')
	//no virtual click
	//scale_list[index].click()
	//call the function directly + false (not move the H bar)
	BNAV_select_scale_sequence_button(scale_list[index],move_PMC_RE)
	//scroll check
	let selection_position=index*(3*nuzic_block)
	let scroll_position=scale_list_div.scrollLeft
	let client_width=scale_list_div.clientWidth
	if(selection_position<scroll_position || selection_position>(scroll_position+client_width))scale_list_div.scrollLeft=selection_position
}

function BNAV_play_scale_button(button){
	//play this scale
	let scale = button.closest(".App_BNav_S_list_scale")
	let family = scale.closest(".App_BNav_S_list_scale_family")
	let data =JSON.parse(family.getAttribute("value"))
	let rotation= parseInt(scale.getAttribute("rotation"))
	let iS=data.iS
	let scale_TET=data.TET
	let starting_note=iS.reduce((prop,item,index)=>{return (index<rotation)?item+prop:prop},0)
	APP_play_this_scale(scale_TET,iS,starting_note,rotation)
	//select scale
	BNAV_select_scale(scale,true)
}

function BNAV_scale_list_iS_div_gen(scale,rotation,background_white=false){
	//function add miniature iS representation
	let iS_list = JSON.parse(JSON.stringify(scale.iS))
	for(i=0; i<rotation;i++){
		iS_list.push(iS_list.shift())
	}
	let string=""
	if(!background_white){
		iS_list.forEach(iS=>{
			let color=BNAV_calculate_iS_color(iS,scale.TET)
			string+=`<div style="width: calc(var(--RE_block_half) * ${iS}); background-color: var(${color});"></div>`
		})
	}else{
		iS_list.forEach(iS=>{
			let color=BNAV_calculate_iS_color(iS,scale.TET)
			let added_border=""
			if(color=="--Nuzic_white")added_border="border: 1px solid var(--Nuzic_pink_light);"
			string+=`<div style="width: calc(var(--RE_block_half) * ${iS}); background-color: var(${color});${added_border}"></div>`
		})
	}
	return string
}

function BNAV_calculate_iS_color(iS,current_TET=0){
	//change scale with TET
	if(current_TET==0){
		//use TET
		current_TET=TET
	}
	//creating array
	let color_sequence = ["--Nuzic_white",
							"--Nuzic_blue",
							"--Nuzic_yellow",
							"--Nuzic_red",
							"--Nuzic_green",
							"--Nuzic_pink",
	]
	//modify array base TET
	if(current_TET==12){
		//reverse second half range
		let position=iS%6
		if(iS>6)position=(current_TET-iS)%6
		return color_sequence[position]
	}else{
		let position=iS%6
		return color_sequence[position]
	}
}


//EDITOR

function BNAV_refresh_editor(){
	let editor_div = document.getElementById("App_BNav_S_editor")
	let editor_button = document.getElementById("App_BNav_S_selected_scale_edit")
	if(editor_button.classList.contains("pressed")){
		//actualize editor
		editor_div.style.display=""
		let S_selected_div= document.getElementById("App_BNav_S_selected")
		let selected_data=JSON.parse(S_selected_div.getAttribute("value"))
		let name_line = document.getElementById("App_BNav_S_editor_title_scale_name")
		let title_line = document.getElementById("App_BNav_S_editor_title_line")
		let note_line = document.getElementById("App_BNav_S_editor_note_line_container")
		let grade_line = document.getElementById("App_BNav_S_editor_grade_line_container")
		let [s1,s2]=BNAV_calculate_S_editor_lines(selected_data)
		note_line.innerHTML=s1
		grade_line.innerHTML=s2
		name_line.value=selected_data.name
		BNAV_refresh_editor_extra_info()
	}else{
		//control editor is hidden
		editor_div.style.display="none"
	}
}

function BNAV_refresh_editor_extra_info(edit_name=null){
	let S_selected_div= document.getElementById("App_BNav_S_selected")
	let selected_data=JSON.parse(S_selected_div.getAttribute("value"))
	//verify button statuses
	//hide if global
	//normal (green_light) if possible to save
	//green already saved
	//yellow modified
	let save_button_class="normal"
	let target_info=null
	let target_info_global=false
	if(selected_data.global){
		let target=DATA_find_global_scale_from_iS_list(selected_data.iS)
		save_button_class="hide"
		target_info=target
		target_info_global=true
	}else{
		let target_global=DATA_find_global_scale_from_iS_list(selected_data.iS)
		if(target_global!=null){
			save_button_class="hide"
			target_info=target_global
			target_info_global=true
		}else{
			let target_user=DATA_find_user_scale_from_iS_list(selected_data.iS)
			if(target_user!=null){
				let name = (edit_name==null)?selected_data.name:edit_name
				if(selected_data.rotation==0){
					save_button_class=(name==target_user.name)?"green":"yellow"
				}else{
					let child=target_user.family.find(item=>{return item.rotation==selected_data.rotation})
					if(child==null){
						save_button_class=(name=="")?"green":"yellow"
					}else{
						save_button_class=(name==child.name)?"green":"yellow"
					}
				}
				target_info=target_user
			}
		}
	}
	let save_button_div=document.getElementById("App_BNav_S_editor_title_save_scale")
	save_button_div.classList.remove(...save_button_div.classList);
	save_button_div.classList.add(save_button_class)
	// if(save_button_class=="normal"){
		//change icon ???
	// }
	//verify name legit
	let name_div=document.getElementById("App_BNav_S_editor_title_scale_name")
	let new_name=DATA_verify_new_scale_name(edit_name)
	if(edit_name==new_name || edit_name==null){
		name_div.classList.remove("duplicate")
	}else{
		name_div.classList.add("duplicate")
	}
	let use_button_class=(selected_data.scale_TET==TET)?"normal":"hide"
	if(selected_data.acronym!=""){
		let idea_scale_list=DATA_get_idea_scale_list()
		let target=idea_scale_list.find((item,index)=>{
			return item.acronym==selected_data.acronym
		})
		if(target!=null){
			let name = (edit_name==null)?selected_data.name:edit_name
			if(selected_data.rotation==0){
				use_button_class=(name==target.name)?"green":"yellow"
			}else{
				let child=target.family.find(item=>{return item.rotation==selected_data.rotation})
				if(child==null){
					use_button_class=(name=="")?"green":"yellow"
				}else{
					use_button_class=(name==child.name)?"green":"yellow"
				}
			}
		}
	}
	let use_button_div=document.getElementById("App_BNav_S_editor_title_use_scale")
	use_button_div.classList.remove(...use_button_div.classList);
	use_button_div.classList.add(use_button_class)
	//print extra info
	let extra_info_div= document.getElementById("App_BNav_S_editor_extra_info")
	if(target_info==null){
		extra_info_div.innerHTML=""
		return
	}
	//calculate equivalent rotation
	let eq_rotation=selected_data.rotation
	if(target_info_global){
		//verify similar iS too
		let delta_rotation=0
		let iS_list_string=JSON.stringify(selected_data.iS)
		let test_iS_list=JSON.parse(JSON.stringify(target_info.iS))
		let module=selected_data.iS.length
		for(let i=0;i<module;i++){
			let test_iS_list_string_rotated=JSON.stringify(test_iS_list)
			test_iS_list.push(test_iS_list.shift())
			if(iS_list_string==test_iS_list_string_rotated){
				delta_rotation=i
				i=1000
			}
		}
		eq_rotation=(eq_rotation+delta_rotation)%module
	}
	let family_name=(target_info_global)?target_info.name[current_language]:target_info.name
	//let family_name_init=(target_info_global)?"N - ":"M - "
	let family_name_init=(target_info_global)?"<label class='App_BNav_S_editor_extra_info_nuzic'>N</label> ":"<label class='App_BNav_S_editor_extra_info_user'>M</label> - "
	let extra_info_string=`<p${(eq_rotation==0)?' class="bold"':''}>${family_name_init+family_name}</p>`
	for(let i=1;i<target_info.module;i++){
		let child=target_info.family.find(item=>{return item.rotation==i})
		if(child==null){
			//extra_info_string+=`<p${(eq_rotation==i)?' class="bold"':''}>${i} - r_${i}_${family_name}</p>`
			extra_info_string+=`<p${(eq_rotation==i)?' class="bold"':''}>${i} - </p>`
		}else{
			extra_info_string+=`<p${(eq_rotation==i)?' class="bold"':''}>${i} - ${(target_info_global)?child.name[current_language]:child.name}</p>`
		}
	}
	extra_info_div.innerHTML=extra_info_string
}

function BNAV_calculate_S_editor_lines(data){
	let string1=""
	let string2=""
	//a COPY, not a pointer
	let iS_list = JSON.parse(JSON.stringify(data.iS))
	for (let i = 0 ; i<data.rotation; i++){
		iS_list.push(iS_list.shift())
	}
	let grades_index_list=[0]
	iS_list.forEach(iS=>{grades_index_list.push(grades_index_list.slice(-1)[0]+iS)})
	for(let i=0; i<=data.scale_TET; i++){
		let note=3*data.scale_TET+i+data.starting_note
		let note_value=note%data.scale_TET
		let pressed=(grades_index_list.includes(i))?"pressed":""
		let grade_value=(grades_index_list.includes(i))?grades_index_list.indexOf(i):""
		if(i==data.scale_TET)grade_value=0
		if(grade_value===0)pressed="boundary"
		string1+=`<button class="${pressed}" onclick="BNAV_play_S_editor_note(this);BNAV_editor_scale_select_note(this)" note="${note}" scale_TET="${data.scale_TET}">${note_value}</button> `
		string2+=`<div class="${pressed}">${grade_value}</div> `
	}
	return [string1,string2]
}

function BNAV_play_S_editor_note(button){
	//avoid play if already pressed-> going deselected
	if(button.classList.contains("pressed"))return
	let note = parseInt(button.getAttribute("note"))
	let scale_TET = parseInt(button.getAttribute("scale_TET"))
	if(Number.isInteger(note) && SNAV_read_note_on_click() == true){
		APP_play_this_scale_note(note,scale_TET)
	}
}

function BNAV_editor_scale_select_note(button){
	if(button.classList.contains("boundary"))return
	button.classList.toggle('pressed')
	let S_selected_div= document.getElementById("App_BNav_S_selected")
	let selected_data=JSON.parse(S_selected_div.getAttribute("value"))
	//change name if name used in global
	selected_data.name=DATA_verify_new_scale_name(document.getElementById("App_BNav_S_editor_title_scale_name").value)
	selected_data.global=false
	selected_data.user=false
	//DESELECT SELECTION
	let selected_objects_list=[...document.querySelector(".App_BNav_S_bottom").querySelectorAll(".scale_selected")]
	selected_objects_list.forEach(item=>{item.classList.remove("scale_selected")})
	//Change letter
	selected_data.acronym=""
	//selected_data.name="new"
	//CALCULATE iS
	let note_button_list=[...document.getElementById("App_BNav_S_editor_note_line_container").querySelectorAll("button")]
	let note_list=[]
	note_button_list.forEach(button=>{
		if(button.classList.contains("boundary") || button.classList.contains("pressed"))note_list.push(parseInt(button.getAttribute("note")))
	})
	let provv=0
	let iS_list=note_list.map(item=>{
		let iS= item-provv
		provv=item
		return iS
	})
	iS_list.shift()
	//counter rotation (plus verification module??)
	//selected_data.rotation=selected_data.rotation%iS_List.length
	for(let i=0;i<selected_data.rotation;i++){
		iS_list.unshift(iS_list.pop())
	}
	selected_data.iS=iS_list
	//show
	BNAV_update_S_selected(false,selected_data)
}

function BNAV_reset_selected_scale_basic_values(){
	let S_selected_div= document.getElementById("App_BNav_S_selected")
	let selected_data=JSON.parse(S_selected_div.getAttribute("value"))
	let family_name=""
	if(selected_data.rotation==0){
		family_name=selected_data.name
	}else if(selected_data.global){
		family_name=DATA_find_global_scale_from_iS_list(selected_data.iS).name[current_language]
	}else if(selected_data.user){
		family_name=DATA_find_user_scale_from_iS_list(selected_data.iS).name
	}else{
		family_name=selected_data.name
		if(selected_data.acronym!=""){
			let idea_scale_list=DATA_get_idea_scale_list()
			let target=idea_scale_list.find((item,index)=>{
				return item.acronym==selected_data.acronym
			})
			if(target!=null)family_name=target.name
		}
	}
	selected_data.rotation=0
	selected_data.starting_note=0
	selected_data.name=family_name
	BNAV_update_S_selected(false,selected_data)
}

//SELECTED LINE INFO

function BNAV_update_S_selected(use_UI_selection,selected_data=null){
	let S_selected_div= document.getElementById("App_BNav_S_selected")
	let acronym_div = document.getElementById("App_BNav_S_selected_acronym")
	let rotation_input = document.getElementById("App_BNav_S_selected_rotation")
	let starting_note_input = document.getElementById("App_BNav_S_selected_starting_note")
	let iS_div = document.getElementById("App_BNav_S_selected_scale_iS")
	//data
	let acronym=""
	let rotation=0
	let starting_note=0
	let iS=[TET]
	let scale_TET=TET
	let name=""
	let global=false
	let user=false
	if(use_UI_selection){
		let scale = document.querySelector(".scale_selected")
		if(scale!=null){
			//reading selected
			let family=scale.closest(".App_BNav_S_list_scale_family")
			let data =JSON.parse(family.getAttribute("value"))
			rotation= parseInt(scale.getAttribute("rotation"))
			iS=data.iS
			scale_TET=(data.TET!=null)?data.TET:TET
			let sub_selection_div=scale.querySelector(".scale_selected")
			if(sub_selection_div!=null){
				starting_note=parseInt(sub_selection_div.getAttribute("starting_note"))
			}else{
				starting_note=iS.reduce((prop,item,index)=>{return (index<rotation)?item+prop:prop},0)
			}
			acronym=(data.acronym!=null)?data.acronym:""
			if(acronym==""){
				//no acronym means from list global OR user
				global=!(typeof data.name === 'string' || data.name instanceof String)
				user=!global
				let provv = family.getAttribute("acronym")
				if(global && provv!="+")acronym=provv
				if(user && provv!="+")acronym=provv
				//verify used global scale
				name=BNAV_get_scale_name(data,global,rotation)
			}else{
				//composition scale list
				global=data.global
				user=false//false, never connected to specific user list
				name = BNAV_get_scale_name(data,false,rotation)
				/*if(rotation==0){
					name=data.name
				}else{
					//search in family names
					let found=data.family.find(item=>{return item.rotation==rotation})
					if(found!=null){
						name=found.name
					}else{
						name="r_"+rotation+"_"+data.name
					}
				}*/
			}
		}
	}else{
		//verify idea scale list exist and compatible
		if(selected_data!=null){
			let target=null
			if(selected_data.acronym!=""){
				let idea_scale_list=DATA_get_idea_scale_list()
				target=idea_scale_list.find((item,index)=>{
					return item.acronym==selected_data.acronym
				})
			}else{
				target=selected_data
			}
			if(target!=null){
				if(JSON.stringify(selected_data.iS) == JSON.stringify(target.iS)){
					scale_TET=selected_data.scale_TET
					iS=selected_data.iS
					rotation=selected_data.rotation
					starting_note=selected_data.starting_note
					name=selected_data.name
					acronym=selected_data.acronym
					user=selected_data.user
					global=selected_data.global
				}
			}
		}
	}
	acronym_div.innerHTML=acronym
	rotation_input.value=rotation
	starting_note_input.value=starting_note
	let iS_string=BNAV_selected_scale_iS_div_gen(iS,scale_TET,rotation,starting_note)
	iS_div.innerHTML=iS_string
	selected_data={iS,scale_TET,rotation,starting_note,name,acronym,global,user}
	S_selected_div.setAttribute("value",JSON.stringify(selected_data))
	BNAV_refresh_editor()
}

function BNAV_get_scale_name(scale_data,global,rot){
	if(rot==0)return (global)?scale_data.name[current_language]:scale_data.name
	let family_data=scale_data.family.find(item=>{return item.rotation==rot})
	if(family_data!=null){
		return (global)?family_data.name[current_language]:family_data.name
	}else{
		return (global)?("r_"+rot+"_"+scale_data.name[current_language]):("r_"+rot+"_"+scale_data.name)
	}
}

function BNAV_find_and_select_scale_composition(acronym,rotation,starting_note){
	//only UI, no focus
	let idea_scale_list_elements = [...document.getElementById("App_BNav_S_composition").querySelectorAll(".App_BNav_S_list_scale_family")]
	//let idea_scale_list= idea_scale_list_elements.map(idea=>{return JSON.parse(idea.getAttribute("value"))})
	if(acronym!=""){
		let idea_scale_list=DATA_get_idea_scale_list()
		let target=idea_scale_list.find((item,index)=>{
			return item.acronym==acronym
		})
		let target_index=idea_scale_list.indexOf(target)
		if(target_index!=-1){
			let selected_objects_list=[...document.querySelector(".App_BNav_S_bottom").querySelectorAll(".scale_selected")]
			selected_objects_list.forEach(item=>{item.classList.remove("scale_selected")})
			let target_family=idea_scale_list_elements[target_index]
			let child_div_list=[...target_family.querySelectorAll(".App_BNav_composition_scale")]
			let target_child=child_div_list.find(child=>{
				return parseInt(child.getAttribute("rotation"))==rotation
			})
			if(target_child!=null){
				target_child.classList.add("scale_selected")
				let add_div_list=[...target_child.querySelectorAll(".App_BNav_composition_scale_add")]
				let target_add=add_div_list.find(child=>{
					return parseInt(child.getAttribute("starting_note"))==starting_note
				})
				if(target_add!=null){
					target_add.classList.add("scale_selected")
				}
				let container = document.getElementById("App_BNav_S_composition")
				let scrollTop=target_child.offsetTop-container.offsetTop
				let h=container.clientHeight
				let sT=container.scrollTop
				if(sT>scrollTop){
					container.scrollTop=scrollTop
				}else if(scrollTop+nuzic_block>sT+h){
					container.scrollTop=scrollTop-h+nuzic_block
				}
			}
		}
	}
}

function BNAV_selected_scale_iS_div_gen(iS,scale_TET,rotation,starting_note){
	//function add miniature iS representation
	let iS_list = JSON.parse(JSON.stringify(iS))
	for(i=0; i<rotation;i++){
		iS_list.push(iS_list.shift())
	}
	let string=""
	iS_list.forEach(iS=>{
		let color=BNAV_calculate_iS_color(iS,scale_TET)
		string+=`<div style="width: calc(var(--RE_block) * ${iS}); background-color: var(${color});">${iS}</div>`
	})
	return string
}

function BNAV_play_selected_scale_button(){
	let S_selected_div= document.getElementById("App_BNav_S_selected")
	let selected_data=JSON.parse(S_selected_div.getAttribute("value"))
	APP_play_this_scale(selected_data.scale_TET,selected_data.iS,selected_data.starting_note,selected_data.rotation)
}

function BNAV_use_selected_scale(){
	//save editor scale to idea scale list
	//search idea list if already exist said name
	let S_selected_div= document.getElementById("App_BNav_S_selected")
	let selected_data=JSON.parse(S_selected_div.getAttribute("value"))
	let idea_scale_list=DATA_get_idea_scale_list()
	let new_acronym=BNAV_generate_new_scale_acronym(idea_scale_list)
	if(selected_data.scale_TET!=TET)return
	let success=false
	let refresh_S_list=false
	let new_scale={"module":null,"acronym":new_acronym,"iS":null,"name":null,"family":[],"global":false}
	if(selected_data.global){
		let target=DATA_find_global_scale_from_iS_list(selected_data.iS)
		if(target==null)return
		let family=[]
		target.family.forEach(fam=>{
			family.push({"name":fam.name[current_language],"rotation":fam.rotation})
		})
		new_scale.module=target.module
		new_scale.acronym=new_acronym
		new_scale.iS=target.iS
		new_scale.name=target.name[current_language]
		new_scale.family=family
		new_scale.global=true
		if(selected_data.acronym==""){
			//new scale
			idea_scale_list.push(new_scale)
			success=true
			refresh_S_list=true
		}else{
			//overwrite
			new_scale.acronym=selected_data.acronym
			let scale_to_be_modified=idea_scale_list.find((item,index)=>{
				if( item.acronym==selected_data.acronym){
					success=true
					refresh_S_list=true
					idea_scale_list[index]=new_scale
					return true
				}
			})
		}
	}else if(selected_data.user){
		let target=DATA_find_user_scale_from_iS_list(selected_data.iS)
		if(target==null)return
		let family=[]
		target.family.forEach(fam=>{
			family.push({"name":fam.name,"rotation":fam.rotation})
		})
		new_scale.module=target.module
		new_scale.acronym=new_acronym
		new_scale.iS=target.iS
		new_scale.name=target.name
		new_scale.family=family
		new_scale.global=false
		if(selected_data.acronym==""){
			//new scale
			idea_scale_list.push(new_scale)
			success=true
			refresh_S_list=true
		}else{
			//overwrite
			new_scale.acronym=selected_data.acronym
			let scale_to_be_modified=idea_scale_list.find((item,index)=>{
				if( item.acronym==selected_data.acronym){
					success=true
					refresh_S_list=true
					idea_scale_list[index]=new_scale
					return true
				}
			})
		}
	}else{
		//new mod from editor
		if(selected_data.acronym==""){
			//VERIFY IF THE CURRENT SCALE IS legit ; ELSE OVERWRITE with GLOBAL
			let target=DATA_find_global_scale_from_iS_list(selected_data.iS)
			let scale_name_div=document.querySelector("#App_BNav_S_editor_title_scale_name")
			if(target!=null){
				//select and use!!!
				//find acromyn (eventually) and name
				let current_name=target.name[current_language]
				new_acronym=""
				idea_scale_list.find(item=>{
					if(item.module==target.module){
						if(JSON.stringify(target.iS)==JSON.stringify(item.iS)){
							new_acronym=item.acronym
							return true
						}
					}
					return false
				})
				selected_data.acronym=new_acronym
				//selected_data.iS
				selected_data.name=target.name[current_language]
				selected_data.rotation=0
				//selected_data.scale_TET
				selected_data.starting_note=0
				selected_data.global=true
				selected_data.user=false
				let acronym_div=document.getElementById("App_BNav_S_selected_acronym")
				let rotation_input = document.getElementById("App_BNav_S_selected_rotation")
				let starting_note_input = document.getElementById("App_BNav_S_selected_starting_note")
				acronym_div.innerHTML=new_acronym
				rotation_input.value=0
				starting_note_input.value=0
				S_selected_div.setAttribute("value",JSON.stringify(selected_data))
				BNAV_refresh_editor()
				BNAV_use_selected_scale()
				return
			}
			//new scale
			let new_scale_name=scale_name_div.value
			//verify the name is allowed (no global name)
			new_scale_name=DATA_verify_new_scale_name(new_scale_name)
			scale_name_div.value=new_scale_name
			//change the name
			selected_data.name=new_scale_name
			new_scale.module=selected_data.iS.length
			new_scale.acronym=new_acronym
			new_scale.iS=selected_data.iS
			new_scale.name=selected_data.name
			new_scale.family=[]
			new_scale.global=false
			if(selected_data.rotation==0){
				new_scale.name=new_scale_name
			}else{
				//create new child
				new_scale.name="r_0_"+new_scale_name
				let child={"name":new_scale_name,"rotation":selected_data.rotation}
				new_scale.family.push(child)
			}
			idea_scale_list.push(new_scale)
			success=true
		}else{
			//already exist
			//permitted mod name
			new_scale=idea_scale_list.find(item=>{return item.acronym==selected_data.acronym})
			//verify scale sequence is the same no need
			let scale_name_div=document.querySelector("#App_BNav_S_editor_title_scale_name")
			let new_scale_name=scale_name_div.value
			//verify the name is allowed
			new_scale_name=DATA_verify_new_scale_name(new_scale_name)
			scale_name_div.value=new_scale_name
			//change the name
			selected_data.name=new_scale_name
			let changed= BNAV_scale_family_change_name(new_scale,new_scale_name,selected_data.rotation)
			if(!changed)return
			success=true
			refresh_S_list=(DATA_find_global_scale_from_iS_list(new_scale.iS)!=null || DATA_find_user_scale_from_iS_list(new_scale.iS)!=null)
		}
	}
	if(success){
		S_selected_div.setAttribute("value",JSON.stringify(selected_data))
		let scale_data={"scale_sequence":DATA_get_scale_sequence(),"idea_scale_list":idea_scale_list}
		BNAV_refresh_S_composition(scale_data)
		BNAV_save_new_scale_data(scale_data,false)
		if(refresh_S_list)BNAV_refresh_scale_list()
		BNAV_find_and_select_scale_composition(new_scale.acronym,selected_data.rotation,selected_data.starting_note)
		selected_data.acronym=new_scale.acronym
		BNAV_update_S_selected(false,selected_data)
	}
}

function BNAV_save_selected_scale(){
	//save selected idea scale to user scale list
	let S_selected_div= document.getElementById("App_BNav_S_selected")
	let selected_data=JSON.parse(S_selected_div.getAttribute("value"))
	//user_scale_list = {scale_list , TET_list}
	//scale_list {TET,module,name,iS,family}
	if(selected_data.global)return
	//check iS
	//VERIFY IF THE CURRENT SCALE IS legit
	let target=DATA_find_global_scale_from_iS_list(selected_data.iS)
	if(target!=null){
		return
	}
	//check family
	let family=[]
	let family_name=""
	if(selected_data.acronym!=""){
		//check if name changed?? ->no , other function
		let idea_scale_list=DATA_get_idea_scale_list()
		let idea_scale=idea_scale_list.find(item=>{
				if( item.acronym==selected_data.acronym){
					return true
				}
			})
		family_name=idea_scale.name
		family=idea_scale.family
	}
	//check name
	let scale_name_div=document.querySelector("#App_BNav_S_editor_title_scale_name")
	let new_scale_name=scale_name_div.value
	//verify the name is allowed (no global name)
	new_scale_name=DATA_verify_new_scale_name(new_scale_name)
	scale_name_div.value=new_scale_name
	selected_data.name=new_scale_name
	S_selected_div.setAttribute("value",JSON.stringify(selected_data))
	let new_scale={"TET": selected_data.scale_TET,"module":selected_data.iS.length,"iS":selected_data.iS,"name":family_name,"family":family}
	//change the name
	BNAV_scale_family_change_name(new_scale,new_scale_name,selected_data.rotation)
	//verify if already exist
	let exist=user_scale_list.scale_list.find((item,index)=>{
			if(new_scale.module==item.module && new_scale.TET==item.TET){
				return JSON.stringify(new_scale.iS)==JSON.stringify(item.iS)
			}
			return false
		})
	if(exist!=null){
		//if exist- save new names
		if(BNAV_open_alert_overwrite_scale("user",exist.name)){
			//verify tet list
			BNAV_scale_family_change_name(exist,new_scale_name,selected_data.rotation)
		}
	}else{
		//else save new
		user_scale_list.scale_list.push(new_scale)
	}
	//save new user file
	DATA_save_user_scale_list()
	BNAV_refresh_scale_list()
	BNAV_refresh_editor()
}

function BNAV_scale_family_change_name(scale,new_name,rotation){
	if(rotation==0){
		if(scale.name==new_name)return false
		scale.name=new_name
	}else{
		//find family
		let child=scale.family.find(child=>{return child.rotation==rotation})
		if(child==null){
			//create new child
			child={"name":new_name,"rotation":rotation}
			let insertion_index=0
			scale.family.some((child,index)=>{
				if(child.rotation>rotation)return true
				insertion_index=index+1
			})
			scale.family.splice(insertion_index,0,child)
		}else{
			//mod child
			if(child.name==new_name)return false
			child.name=new_name
		}
		if(scale.name=="")scale.name="r_0_"+new_name
	}
	return true
}

function BNAV_generate_new_scale_acronym(idea_scale_list){
	let first_letter="A"
	let already_used = _test_letter(first_letter)
	if(already_used){
		//try alphabet
		let alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz".split("");
		return alphabet.find(letter=>{return !_test_letter(letter)})
	}else{
		return first_letter
	}
	function _test_letter(char){
		return idea_scale_list.some(item=>{
			return item.acronym===char
		})
	}
}

function BNAV_calculate_S_selection_rotation_starting_note(mod_rotation){
	let S_selected_div= document.getElementById("App_BNav_S_selected")
	let selected_data=JSON.parse(S_selected_div.getAttribute("value"))
	let rotation_input = document.getElementById("App_BNav_S_selected_rotation")
	let starting_note_input = document.getElementById("App_BNav_S_selected_starting_note")
	let starting_note=parseInt(starting_note_input.value)%selected_data.scale_TET
	if(isNaN(starting_note))starting_note=0
	let module=selected_data.iS.length
	let rotation=parseInt(rotation_input.value)%module
	if(isNaN(rotation))rotation=0
	let previous_rotation=selected_data.rotation%module
	if(previous_rotation!=rotation){
		let delta_note=selected_data.iS.reduce((prop,item,index)=>{
			let iS=(index<rotation)?item:0
			return iS+prop},0)-selected_data.iS.reduce((prop,item,index)=>{
			let iS=(index<previous_rotation)?item:0
			return iS+prop},0)
		starting_note=(starting_note+delta_note)%selected_data.scale_TET
	}
	if(starting_note<0)starting_note=selected_data.scale_TET+starting_note
	selected_data.rotation=rotation
	selected_data.starting_note=starting_note
	//S_selected_div.setAttribute("value",JSON.stringify(selected_data))
	//BNAV_refresh_editor()
	//change name if used
	if(selected_data.global){
		let target=DATA_find_global_scale_from_iS_list(selected_data.iS)
		selected_data.name=BNAV_get_scale_name(target,true,rotation)
	}else{
		if(selected_data.acronym!=""){
			//find rot name in composition name
			let idea_scale_list=DATA_get_idea_scale_list()
			let target=idea_scale_list.find((item,index)=>{
				return item.acronym==selected_data.acronym
			})
			selected_data.name=BNAV_get_scale_name(target,false,rotation)
		}else{
			//find name in user or editor
			if(selected_data.user){
				let target=DATA_find_user_scale_from_iS_list(selected_data.iS)
				selected_data.name=BNAV_get_scale_name(target,false,rotation)
			}else{
				//nothing to do, editor doesnt have family
			}
		}
		//let family_data=scale.family.find(item=>{return item.rotation==rot})
	}
	BNAV_find_and_select_scale_composition(selected_data.acronym,rotation,starting_note)
	BNAV_update_S_selected(false,selected_data)
}

//edit
function BNAV_edit_selected_scale_button(button){
	button.classList.toggle("pressed")
	BNAV_refresh_editor()
	DATA_save_preferences_file()
}

function BNAV_read_show_editor_selected_scale(){
	let button=document.getElementById("App_BNav_S_selected_scale_edit")
	return button.classList.contains("pressed")
}

function BNAV_set_show_editor_selected_scale(show){
	let button=document.getElementById("App_BNav_S_selected_scale_edit")
	if(show==null || show){
		button.classList.add("pressed")
	}else{
		button.classList.remove("pressed")
	}
}







/*CIRCLE WRITE VALUES*/  //XXX XXX XXX OBSOLETE
function BNAV_scale_circle_change_input_values(data){
console.error("OBSOLETE")
	// document.getElementById('App_BNav_S_scale_duration').value = data.duration
	//


}





//USED SCALE COMP INFO

function BNAV_refresh_S_composition(scale_data){
	let scale_composition = document.getElementById("App_BNav_S_composition")
	//Actualize global variable
	let idea_scale_list = scale_data.idea_scale_list
	let scale_sequence = scale_data.scale_sequence
	let view_option="used"
	if(idea_scale_list.length>0){
		let string=""
		idea_scale_list.forEach(item=>{
			string+=BNAV_calculate_composition_scale_family_string(item,scale_sequence,view_option)
		})
		scale_composition.innerHTML=string
	}else{
		scale_composition.innerHTML = `<div class="App_BNav_S_composition_placeholder">Add scales from COMMUNITY, scales saved on the USER profile or create and use a new one on the scale EDITOR
			</div>`
	}
}

function BNAV_calculate_composition_scale_family_string(scale,scale_sequence,view_option){
	//view_option:
	//used -> only used and first
	//family -> only used family and first
	//all
	let item_string = JSON.stringify(scale)
	let family_string=`<div class="App_BNav_S_list_scale_family" value='${item_string}'>`
	//determine if a family member is used
	let used_scales=[]
	scale_sequence.forEach(item=>{
		if(item.acronym==scale.acronym){
			let exist=used_scales.some(sc=>{return sc.starting_note==item.starting_note && sc.rotation==item.rotation})
			if(!exist)used_scales.push({"starting_note":item.starting_note,"rotation":item.rotation})
		}
	})
	//doesnt work
	//used_scales=[...new Set(used_scales)]
	let rot=0
	let multiple_lines=used_scales.find(item=>{return item.rotation!=rot})
	let svg_string=(multiple_lines || (view_option=="family" && scale.family.length!=0) || (view_option=="all" && scale.module!=1))?'<svg ><line x1="50%" y1="14" x2="50%" y2="28" style="stroke-width:3"></line></svg>':""
	let mother_iS_string=BNAV_scale_list_iS_div_gen(scale,0)
	let acronym=scale.acronym
	let used_rot_0_starting_note_list=[]
	used_scales.forEach(us=>{
		let delta_note=scale.iS.reduce((prop,item,index)=>{
			let iS=(index<us.rotation)?item:0
			return iS+prop},0)
		let rot_0_starting_note=(us.starting_note-delta_note)%TET//XXX
		if(rot_0_starting_note<0)rot_0_starting_note=TET+rot_0_starting_note
		used_rot_0_starting_note_list.push(rot_0_starting_note)
	})
	used_rot_0_starting_note_list= [...new Set(used_rot_0_starting_note_list)]
	//used_rot_0_starting_note_list.sort()
	used_rot_0_starting_note_list = used_rot_0_starting_note_list.sort(function(a, b){return a-b})
	family_string+=BNAV_calculate_composition_scale_family_member_string(true,svg_string,0,scale.name,scale,acronym,view_option,used_scales,used_rot_0_starting_note_list)
	let last_index=scale.module-1
	if(view_option=="used" && multiple_lines){
		let last_visible_rotation=0
		used_scales.forEach((item,index)=>{if(item.rotation>last_visible_rotation)last_visible_rotation=item.rotation})
		for(let rot=1; rot<=last_index;rot++){
			let family_data=scale.family.find(item=>{return item.rotation==rot})
			let svg_string=(rot!=last_visible_rotation)?'<svg ><line x1="14" y1="0" x2="14" y2="28" style="stroke-width:3"></line></svg>':'<svg ><line x1="14" y1="0" x2="14" y2="14" style="stroke-width:3"></line></svg>'
			let current_scale_name=""
			if(family_data!=null){
				current_scale_name=family_data.name
			}
			let found=used_scales.some(item=>{return item.rotation==rot})
			if(found){
				family_string+=BNAV_calculate_composition_scale_family_member_string(false,svg_string,rot,current_scale_name,scale,"",null,used_scales,used_rot_0_starting_note_list)
			}
		}
	}
	if(view_option=="family"){
		let last_visible_rotation=0
		used_scales.forEach((item,index)=>{if(item.rotation>last_visible_rotation)last_visible_rotation=item.rotation})
		scale.family.forEach(item=>{if(item.rotation>last_visible_rotation)last_visible_rotation=item.rotation})
		for(let rot=1; rot<=last_index;rot++){
			let family_data=scale.family.find(item=>{return item.rotation==rot})
			let svg_string=(rot!=last_visible_rotation)?'<svg ><line x1="14" y1="0" x2="14" y2="28" style="stroke-width:3"></line></svg>':'<svg ><line x1="14" y1="0" x2="14" y2="14" style="stroke-width:3"></line></svg>'
			let current_scale_name=""
			let found=false
			if(family_data!=null){
				found=true
				current_scale_name=family_data.name
			}else {
				found=used_scales.some(item=>{return item.rotation==rot})
			}
			if(found)family_string+=BNAV_calculate_composition_scale_family_member_string(false,svg_string,rot,current_scale_name,scale,"",null,used_scales,used_rot_0_starting_note_list)
		}
	}
	if(view_option=="all"){
		for(let rot=1; rot<=last_index;rot++){
			let family_data=scale.family.find(item=>{return item.rotation==rot})
			let svg_string=(rot!=last_index)?'<svg ><line x1="14" y1="0" x2="14" y2="28" style="stroke-width:3"></line></svg>':'<svg ><line x1="14" y1="0" x2="14" y2="14" style="stroke-width:3"></line></svg>'
			let current_scale_name=""
			if(family_data!=null){
				current_scale_name=family_data.name
			}
			family_string+=BNAV_calculate_composition_scale_family_member_string(false,svg_string,rot,current_scale_name,scale,"",null,used_scales,used_rot_0_starting_note_list)
		}
	}
	family_string+="</div>"
	return family_string
}

function BNAV_calculate_composition_scale_family_member_string(ismother,svg_string,rotation,name,scale,acronym,view_option,used_scales,used_rot_0_starting_note_list){
	//let icon_string=(ismother)?'<img src="Icons/crown.svg"></img>':`<div >${rotation}</div>`
	let icon_function=(ismother)?`onclick="BNAV_show_hide_composition_scale_family(this,'${view_option}');event.stopPropagation()"`:''
	let used=(used_scales.length!=0)
	let delete_button= (ismother && !used)?`<button class="App_BNav_composition_scale_delete" onclick="BNAV_delete_composition_scale_button(this,event)"><img src="./Icons/delete.svg"></button>`:'<div class="App_BNav_composition_scale_delete"></div>'
	let child_class=(ismother)?"":" child"
	acronym=(ismother)?acronym:rotation
	let iS_string=BNAV_scale_list_iS_div_gen(scale,rotation,true)
	let scale_add_string = ""
	//scales matrix
	let delta_note=scale.iS.reduce((prop,item,index)=>{
			let iS=(index<rotation)?item:0
			return iS+prop},0)
	used_rot_0_starting_note_list.forEach(sn=>{
		let starting_note=(sn+delta_note)%TET
		let class_grey=(used_scales.some(item=>{return item.rotation==rotation && item.starting_note==starting_note}))?"":" scale_suggested"
		scale_add_string+=`<button class="App_BNav_composition_scale_add${class_grey}" starting_note="${starting_note}" onclick="BNAV_select_composition_scale(this);event.stopPropagation()" ondblclick="BNAV_quick_add_scale_to_scale_sequence(this);event.stopPropagation()">${rotation}.${starting_note}</button>`
	})
	//only used scales
	// used_scales.forEach(us=>{
	// 	if(us.rotation==rotation){
	// 		scale_add_string+=`<button class="App_BNav_composition_scale_add" starting_note="${us.starting_note}" onclick="BNAV_select_composition_scale(this);event.stopPropagation()" ondblclick="BNAV_quick_add_scale_to_scale_sequence(this);event.stopPropagation()">${us.rotation}.${us.starting_note}</button>`
	// 	}
	// })
	let string=`<div class="App_BNav_composition_scale" rotation="${rotation}" onclick="BNAV_select_scale(this)" ondblclick="BNAV_quick_add_scale_to_scale_sequence(this);event.stopPropagation()">
					${delete_button}
					<div class="App_BNav_composition_scale_acronym${child_class}" ${icon_function}>
						${svg_string}
						<p>${acronym}</p>
					</div>
					<div class="App_BNav_composition_scale_iS">
						${iS_string}
					</div>
					<div class="App_BNav_composition_scale_name">${name}</div>
					<div class="App_BNav_composition_scale_button_container">
						${scale_add_string}
					</div>
				</div>`
	return string
}

function BNAV_show_hide_composition_scale_family(button,current_view_option){
	let view_option="used"
	let family_div=button.closest(".App_BNav_S_list_scale_family")
	let scale_info=JSON.parse(family_div.getAttribute("value"))
	if(current_view_option=="used" && scale_info.family.length!=0)view_option="family"
	if(current_view_option=="used" && scale_info.family.length==0)view_option="all"
	if(current_view_option=="family" && scale_info.module>scale_info.family.length+1)view_option="all"
	let scale_sequence = DATA_get_scale_sequence()
	let lang=current_language
	family_div.outerHTML=BNAV_calculate_composition_scale_family_string(scale_info,scale_sequence,view_option)
}

function BNAV_delete_composition_scale_button(button,event){
	if(!button.closest(".App_BNav_composition_scale").classList.contains("scale_selected"))return
	let scale_div= button.closest(".App_BNav_S_list_scale_family")
	//no need
	//if(!scale_div.classlist.contains("user"))return
	let selected_data=JSON.parse(scale_div.getAttribute("value"))
	let scale_sequence = DATA_get_scale_sequence()
	let scale_is_used = scale_sequence.some(item=>{
		return selected_data.acronym==item.acronym
	})
	if(scale_is_used==true){
		//deleting not possible
		return
	}else{
		let idea_scale_list=DATA_get_idea_scale_list()
		let index_target = null
		idea_scale_list.find((item,index)=>{
			if(selected_data.acronym===item.acronym){
				index_target=index
				return true
			}
		})
		if(index_target!=null){
			idea_scale_list.splice(index_target,1)
			//let scale_data={"scale_sequence":DATA_get_scale_sequence(),"idea_scale_list":idea_scale_list}
			let scale_data={"scale_sequence":scale_sequence,"idea_scale_list":idea_scale_list}
			//no need to reload all
			//BNAV_save_new_scale_data(scale_data,true)
			BNAV_save_new_scale_data(scale_data,false)
			BNAV_refresh_S_composition(scale_data)
			BNAV_update_S_selected(true)
		}
	}
	event.stopPropagation()
}

function BNAV_generate_TET_list_from_scale_list(data){
	let TET_list = [...new Set(data.scale_list.map((item)=>{
			return item.TET
		})
	)]
	//sort and record
	data.TET_list = TET_list.sort(function(a, b){return a-b})
}

function BNAV_get_scale_from_acronym(acronym){
	let idea_scale_list=DATA_get_idea_scale_list()
	let scale = idea_scale_list.find(item=>{
		return item.acronym==acronym
	})
	//a COPY, not a pointer
	return scale
}

//Accessory functions
//XXX USED
function BNAV_open_alert_overwrite_scale(type="generic",name=""){
	let message = ""
	switch (type) {
		case "user":
			message = 'You are about to overwrite Scale "'+name+'" from your user library. \n\nAre you sure you want to proceed? \n\n'+RE_string_to_superscript("deactivate this window from the options")
		break;
		case "idea":
			message = 'You are about to overwrite Scale "'+name+'" from your idea library. \n\nAre you sure you want to proceed? \n\n'+RE_string_to_superscript("deactivate this window from the options")
		break;
		default:
			message = "You are about to overwrite a Scale from your library. \n\nAre you sure you want to proceed? \n\n"+RE_string_to_superscript("deactivate this window from the options")
	}
	if(SNAV_read_alert_overwrite_scale() == true){
		//string= "You are about to overwrite a Scale from your library. \n\nAre you sure you want to proceed? \n\n"+RE_string_to_superscript("deactivate this window from the options")
		//var retVal = confirm("You are about to overwrite a Scale from your library. \n\nAre you sure you want to proceed? \n\n"+"You can deactivate this window from the options")
		let retVal = confirm(message)
		return retVal
	}else{
		return true
	}
}

//OLD FUNCTIONS
/*
function BNAV_order_scale_bookmark_list(){
	//var scale_tab = document.getElementById("ES_bottom_content")
	var user_scale_subtab = document.getElementById("")
	var global_scale_subtab = document.getElementById("")
	Order(user_scale_subtab)
	Order(global_scale_subtab)
	function Order(element){
		var container = element.querySelector(".")
		var scale_list = [...container.querySelectorAll(".App_BNav_S_scale_radio")]
		var index=0

		scale_list.forEach(item=>{
			if(item.querySelector("button").classList.contains("starred") == 1){
				//is bookmark
				if(index==0){
					//move at the very start
					if(scale_list.indexOf(item)!=0){
						//put it first
						container.insertBefore(item,container.firstChild)
					}
				}else{
					//after last_bookmarked_element
					container.insertBefore(item,container.children[index])
				}
				index++
			}
		})
	}
}
*/
