function DATA_calculate_insert_iT_array_segment_data(data,voice_id,segment_index,iT_array,expand_structure){
	let current_voice_data = data.voice_data.find(item=>{return item.voice_id==voice_id})
	if(current_voice_data.data.segment_data.length<=segment_index){
		console.error("Operation error, data not found")
		return [false,0]
	}
	let current_segment_data=current_voice_data.data.segment_data[segment_index]
	let old_Ls=current_segment_data.time.slice(-1)[0].P
	let success = false
	//calculate new segment with iT array timing, modify fractioning ranges
	let new_iT_list=[]
	let new_sound_list=[]
	let new_fraction_list=JSON.parse(JSON.stringify(current_segment_data.fraction.slice(0)))
	let iT_index=0
	//for every fraction list in the segment, try to insert as many iT of the list as is possible, mantaining the note list
	//and adding "e" in case is necessary to add an artificial iT
	//if iT==0 eliminate the note (if exist)
	//if last segment add every lasting iTs
	//create first pulse
	let current_fraction_start = 0
	let last_fraction_index=new_fraction_list.length-1
	let Ls_reached=false
	let available_sound=current_segment_data.sound.length-1
	let last_fraction_expanded_index=null
	new_fraction_list.forEach((fraction,fraction_index)=>{
		//count how many events are in the fractioning range
		let start_index=null
		let stop_index=null
		current_segment_data.time.find((time,index)=>{
			if(time.P==fraction.start && time.F==0){
				start_index=index
			}
			if(time.P==fraction.stop && time.F==0){
				stop_index=index
				return true
			}
		})
		let fraction_items_number=stop_index-start_index
		//calculate number of iTs to use
		let next_iT_index
		if(expand_structure){
			if(fraction_index==last_fraction_index){
				//all the iT remaining
				next_iT_index=iT_array.length
			}else{
				//all the iT of the range
				next_iT_index=iT_index+fraction_items_number
			}
		}else{
			if(Ls_reached){
				//case !expand_structure delete this fractioning range
				next_iT_index=iT_index
			}else{
				//max number of iTs <= to old_Ls
				let pulses_to_end_segment = old_Ls-current_fraction_start
				//can be ==0
				let sum_iTs_available = Math.floor(pulses_to_end_segment/fraction.N)*fraction.D
				let provv_next_iT_index=iT_index+fraction_items_number
				if(fraction_index==last_fraction_index){
					//verify how many iT can be fitted
					provv_next_iT_index=iT_array.length
				}
				let provv_iT_array_fraction = iT_array.slice(iT_index,provv_next_iT_index)
				let available_space_d_iT_index=0
				let sum_iTs=0
				provv_iT_array_fraction.forEach(iT=>{
					sum_iTs+=iT
					if(sum_iTs<=sum_iTs_available){
						available_space_d_iT_index++
					}else{
						Ls_reached=true
					}
				})
				next_iT_index=iT_index+available_space_d_iT_index
			}
		}
		if(next_iT_index>iT_array.length)next_iT_index=iT_array.length
		//d_index==0??? no need to check
		//add only necessary iTs
		let iT_array_fraction = iT_array.slice(iT_index,next_iT_index)
		iT_index=next_iT_index
		let fraction_sum_iT=0
		//if iT_array_fraction==[] fraction_sum_iT stay == 0 and fraction will be eliminated
		iT_array_fraction.forEach((iT,index)=>{
			if(iT!=0){
				//copy sounds
				new_iT_list.push(iT)
				if(available_sound>start_index+index){
					new_sound_list.push(current_segment_data.sound[start_index+index])
				}else{
					//last fraction
					new_sound_list.push({note:-2,diesis:true,A_pos:[],A_neg:[]})
				}
				fraction_sum_iT+=iT
			}
			//if iT == 0 eliminate note
		})
		if(fraction_sum_iT==0){
			//if no iT eliminate fraction
			new_fraction_list[fraction_index]=null
		}else{
			//complete and redefine fraction range
			let resto = fraction_sum_iT%fraction.D
			if(resto!=0){
				//add last iT + "e"
				//if resto == next iT use this value
				let end_iT=fraction.D-resto
				fraction_sum_iT+=end_iT
				//console.log(iT_array[next_iT_index])
				//add next iT list if ok
				while (end_iT>=iT_array[next_iT_index] && end_iT!=0) {
					//console.log("Use next iTs from iT list "+iT_array[next_iT_index])
					new_iT_list.push(iT_array[next_iT_index])
					new_sound_list.push({note:-2,diesis:true,A_pos:[],A_neg:[]})
					end_iT-=iT_array[next_iT_index]
					next_iT_index++
					iT_index=next_iT_index
				}
				//add the remaining
				if(end_iT!=0){
					new_iT_list.push(end_iT)
					new_sound_list.push({note:-2,diesis:true,A_pos:[],A_neg:[]})
					last_fraction_expanded_index=fraction_index
				}
				//last_fraction_expanded_index=fraction_index
			}
			new_fraction_list[fraction_index].start=current_fraction_start
			current_fraction_start+= fraction_sum_iT*fraction.N/fraction.D
			new_fraction_list[fraction_index].stop=current_fraction_start
		}
	})
	//control that fraction list has at least 1 fraction and eliminate null ones
	let provv = []
	new_fraction_list.forEach((fraction,fraction_index)=>{
		if(fraction!=null){
			provv.push(fraction)
			if(fraction_index==last_fraction_expanded_index){
				//convert to provv index
				last_fraction_expanded_index=provv.length-1
			}
		}
	})
	new_fraction_list=provv
	// if no fraction => control no pulses and no diesis
	if(new_fraction_list.length==0){
		new_fraction_list.push({N:1,D:1,start:0,stop:old_Ls})
		new_iT_list=[old_Ls]
		new_sound_list.push({note:-2,diesis:true,A_pos:[],A_neg:[]})
		new_sound_list.push({note:-4,diesis:true,A_pos:[],A_neg:[]})
	}else{
		//control last fraction at least to Ls, if not add another fraction range
		let last_fraction=new_fraction_list[new_fraction_list.length-1]
		let new_Ls=last_fraction.stop
		let remaining_pulses=old_Ls-new_Ls
		if(remaining_pulses>0){
			//force segment segment to Ls
			if(remaining_pulses%last_fraction.N==0){
				//last fraction range compatible for expansion
				let new_iT=remaining_pulses*last_fraction.D/last_fraction.N
				if(last_fraction_expanded_index==new_fraction_list.length-1){
					//expand last element
					let last_iT=new_iT_list.pop()
					last_iT+=new_iT
					new_iT_list.push(last_iT)
				}else{
					//create new element
					new_iT_list.push(new_iT)
					new_sound_list.push({note:-2,diesis:true,A_pos:[],A_neg:[]})
				}
				last_fraction.stop=old_Ls
			}else{
				//add new fraction range
				new_fraction_list.push({N:1,D:1,start:new_Ls,stop:old_Ls})
				new_iT_list.push(remaining_pulses)
				new_sound_list.push({note:-2,diesis:true,A_pos:[],A_neg:[]})
			}
			//verify if last element is compatible for insertion of next_iT from iT list
			let end_iT=new_iT_list.slice(-1)[0]
			if(new_iT_list.slice(-1)[0]>=iT_array[iT_index]){
				//eliminate last inserted pulse
				new_iT_list.pop()
				new_sound_list.pop()
				//substitute with iT from list
				let next_iT_index=iT_index
				while (end_iT>=iT_array[next_iT_index] && end_iT!=0) {
					//console.log("Use next iTs from iT list "+iT_array[next_iT_index])
					new_iT_list.push(iT_array[next_iT_index])
					new_sound_list.push({note:-2,diesis:true,A_pos:[],A_neg:[]})
					end_iT-=iT_array[next_iT_index]
					next_iT_index++
					iT_index=next_iT_index
				}
				//add the remaining
				if(end_iT!=0){
					new_iT_list.push(end_iT)
					new_sound_list.push({note:-2,diesis:true,A_pos:[],A_neg:[]})
				}
			}
		}
		//control if new EXPANDED Ls compatible with min Ls composition
		if(remaining_pulses<0){
			let min_Ls_abs = DATA_calculate_minimum_Li()
			let min_Ls = min_Ls_abs*current_voice_data.neopulse.D/current_voice_data.neopulse.N
			//verify if divisible by min Ls
			let resto = new_Ls%min_Ls
			if(resto!=0){
				if(resto%last_fraction.N==0){
					//last fraction range compatible for expansion
					let new_iT=resto*last_fraction.D/last_fraction.N
					if(last_fraction_expanded_index==new_fraction_list.length-1){
						//expand last element
						let last_iT=new_iT_list.pop()
						last_iT+=new_iT
						new_iT_list.push(last_iT)
					}else{
						//create new element
						new_iT_list.push(new_iT)
						new_sound_list.push({note:-2,diesis:true,A_pos:[],A_neg:[]})
					}
					last_fraction.stop+=resto
				}else{
					//add a final fraction range
					new_fraction_list.push({N:1,D:1,start:new_Ls,stop:new_Ls+resto})
					new_iT_list.push(resto)
					new_sound_list.push({note:-2,diesis:true,A_pos:[],A_neg:[]})
				}
			}
		}
		//add ending
		new_sound_list.push({note:-4,diesis:true,A_pos:[],A_neg:[]})
	}
	//control success
	//control number time == note==diesis
	//control fraction is correct
	let new_time_list = DATA_calculate_time_from_iT(new_iT_list,new_fraction_list)
	if(new_time_list.length==0){
		//success=false
		return [false,iT_index]
	}
	if(new_time_list.length!=new_sound_list.length){
		return [false,iT_index]
	}
	//control "L"
	if(new_sound_list[0].note==-3)new_sound_list[0].note=-2
	//success
	current_segment_data.time=new_time_list
	current_segment_data.sound=new_sound_list
	current_segment_data.fraction=new_fraction_list
	let iT_used=iT_index
	return [true,iT_used]
}

function DATA_insert_iT_array(voice_id,segment_index,iT_array,expand_structure=false){
	//verify data
	let insert_success=true
	if(iT_array.length==0)insert_success=false
	iT_array.forEach((iT)=>{
		//verify intern range
		if(iT<0)insert_success=false
	})
	if (!insert_success) {
		console.error("Calculator Export error")
		return false
	}
	let data = DATA_get_current_state_point(true)
	//verify if voice index !=null
	if(voice_id!=null){
		let current_voice_data = data.voice_data.find(item=>{return item.voice_id==voice_id})
		if(current_voice_data==null){
			console.error("Operation error, data not found")
			return false
		}
		if(segment_index!=null){
			//apply to single segment
			let success=DATA_calculate_insert_iT_array_segment_data(data,voice_id,segment_index,iT_array,expand_structure)
			insert_success=success[0]
		}else{
			//apply to entire voice
			let iT_array_index=0
			let iT_array_complete=false
			let iT_array_length=iT_array.length
			let last_segment_index=current_voice_data.data.segment_data.length-1
			current_voice_data.data.segment_data.forEach((segment,s_index)=>{
				if(iT_array_complete || !insert_success)return
				if(last_segment_index==s_index)return
				//pass everything from index to end
				let segment_iT_array=iT_array.slice(iT_array_index)
				let success = DATA_calculate_insert_iT_array_segment_data(data,voice_id,s_index,segment_iT_array,false)
				insert_success=success[0]
				//recalculate completion
				iT_array_index+=success[1]
				if(iT_array_length<=iT_array_index)iT_array_complete=true
			})
			if(!iT_array_complete && insert_success){
				//try to add remaining elements to the last segment
				//verify last iT
				let last_segment_iT_array=iT_array.slice(iT_array_index)
				let success = DATA_calculate_insert_iT_array_segment_data(data,voice_id,last_segment_index,last_segment_iT_array,expand_structure)
				insert_success=success[0]
			}
		}
		if(expand_structure){
			//verify length Li
			let new_Li_voice=current_voice_data.data.segment_data.reduce((prop,segment)=>{
				return prop+segment.time.slice(-1)[0].P
			},0)
			let new_Li_absolute=new_Li_voice*current_voice_data.neopulse.N/current_voice_data.neopulse.D
			//console.log(new_Li_absolute)
			if(new_Li_absolute>data.global_variables.Li){
				//unblock current voice
				current_voice_data.blocked=false
				let extend_blocked=DATA_verify_blocked_voices_extend_last_segment(data)
				data.voice_data.forEach(voice=>{
					if(voice.voice_id!=voice_id){
						DATA_calculate_force_voice_Li(voice,new_Li_absolute,extend_blocked)
					}
				})
				data.global_variables.Li=new_Li_absolute
				//DATA_calculate_force_voice_Li(voice,new_Li_absolute,extend_last_segment_blocked="false")
			}
			if(new_Li_absolute<data.global_variables.Li){
				//error
				insert_success=false
			}
		}
		current_voice_data.data.midi_data=DATA_segment_data_to_midi_data(current_voice_data.data.segment_data,current_voice_data.neopulse)
	}else{
		if(segment_index!=null){
			//apply to column XXX NOT ALLOWED FOR NOW (see insert Na for code structure)
			insert_success=false
		}else{
			insert_success=false
		}
	}
	if (!insert_success) {
		console.error("Calculator Export error")
		return false
	}
	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

function DATA_insert_iNa_array(voice_id,segment_index,iN_array,overwrite_note=true,overwrite_e=false,overwrite_s=false,overwrite_L=false,expand_structure=false){
	if(iN_array.length==0){
		console.error("Calculator Export error")
		return false
	}
	var N_array=[]
	N_array[0]=iN_array.shift()
	//other notes
	let current_note=N_array[0]
	for (var i=0 ; i<iN_array.length; i++){
		current_note += iN_array[i]
		N_array.push(current_note)
	}
	//ATT! this operation doesn't preserve iN IF not overwrite note line
	DATA_insert_Na_array(voice_id,segment_index,N_array,overwrite_note,overwrite_e,overwrite_s,overwrite_L,expand_structure)
}

function DATA_calculate_insert_sound_array_segment_data(data,voice_id,segment_index,sound_array,overwrite_note,overwrite_e,overwrite_s,overwrite_L,expand_structure){
	let current_voice_data = data.voice_data.find(item=>{return item.voice_id==voice_id})
	if(current_voice_data.data.segment_data.length<=segment_index){
		console.error("Operation error, data not found")
		return false
	}
	let current_segment_data=current_voice_data.data.segment_data[segment_index]
	let sound_array_index=0
	let sound_array_complete=false
	let sound_array_length=sound_array.length
	current_segment_data.sound.forEach((sound,index)=>{
		if(sound_array_complete)return
		let substitute=false
		if(sound.note>0 && overwrite_note){
			substitute=true
		}else if(sound.note==-1 && overwrite_s){
			substitute=true
		}else if(sound.note==-2 && overwrite_e){
			substitute=true
		}else if(sound.note==-3 && overwrite_L){
			substitute=true
		}
		if(substitute){
			current_segment_data.sound[index]=sound_array[sound_array_index]
			sound_array_index++
		}
		if(sound_array_length<=sound_array_index)sound_array_complete=true
	})
	if(!sound_array_complete){
		//try to add remaining elements to the segment
		//verify last iT
		let last_fraction=current_segment_data.fraction.slice(-1)[0]
		let last_element_pulse=current_segment_data.time.slice(-2)[0]
		let last_pulse=current_segment_data.time.slice(-1)[0]
		let last_iT=DATA_calculate_interval_PA_PB(last_element_pulse,last_pulse,last_fraction)
		//if is needed and permitted expand segment
		let sound_remaining= sound_array.length-sound_array_index
		let iT_needed = sound_remaining+1
		let expanded=false
		if(iT_needed>last_iT && expand_structure){
			//preliminary expansion segment and last iT
			//calculate new Ls as minimum Ls needed to have AT LEAST iT_needed
			let iT_to_allocate=iT_needed-last_iT
			let min_D_Ls_to_allocate = Math.ceil(iT_to_allocate/last_fraction.D)*last_fraction.N
			let new_Ls=last_pulse.P+min_D_Ls_to_allocate
			last_fraction.stop=new_Ls
			current_segment_data.fraction.pop()
			current_segment_data.fraction.push(last_fraction)
			last_pulse.P=new_Ls
			current_segment_data.time.pop()
			current_segment_data.time.push(last_pulse)
			expanded=true
			//or
			//var extend_last_element=true
			//DATA_calculate_force_segment_data_Ls(current_segment_data,new_Ls,extend_last_element)
			//recalculate last iT
			last_iT+=min_D_Ls_to_allocate*last_fraction.D/last_fraction.N
		}
		if(last_iT>1){
			current_segment_data.time.pop()
			current_segment_data.sound.pop()
			for (let i = 0; i < last_iT-1; i++) {
				if(!sound_array_complete){
					let new_time = DATA_calculate_next_position_PA_iT(last_element_pulse,1,last_fraction)
					current_segment_data.time.push(new_time)
					current_segment_data.sound.push(sound_array[sound_array_index])
					last_element_pulse=new_time
					sound_array_index++
					if(sound_array_length<=sound_array_index)sound_array_complete=true
				}
			}
			//reinsert end segment
			current_segment_data.time.push(last_pulse)
			current_segment_data.sound.push({note:-4,diesis:true,A_pos:[],A_neg:[]})
		}
		if(expanded){
			//unblock voice if blocked and there are others voices, force Li to every voice
			if(data.voice_data.length>1)current_voice_data.blocked=false
			//verify Li compatibility  new Ls and expand other voices
			let delta_Ls_min_absolute=DATA_calculate_minimum_Li()
			let N_N=current_voice_data.neopulse.N
			let N_D=current_voice_data.neopulse.D
			let delta_Ls_min=delta_Ls_min_absolute*N_D/N_N
			let resto=last_pulse.P%delta_Ls_min
			//var delta_Ls=new_Ls-old_Ls
			if(resto!=0){
				//new Ls not compatible
				let new_Ls=last_pulse.P+(delta_Ls_min-resto)
				let extend_last_element=false
				DATA_calculate_force_segment_data_Ls(current_segment_data,new_Ls,extend_last_element)
			}
			//calculating new Li_absolute
			let new_Li_voice= current_voice_data.data.segment_data.reduce((prop,segment)=>{
				return prop+segment.time.slice(-1)[0].P
			},0)
			let new_Li_absolute=new_Li_voice*N_N/N_D
			if(new_Li_absolute>max_Li){
				console.error("ERROR: max Li reached")
				return false
			}
			//add a segment to the end of every other voice
			//verify if last segment blocked voices can be extended
			let extend_blocked=DATA_verify_blocked_voices_extend_last_segment(data)
			data.voice_data.forEach(voice=>{
				if(voice.voice_id!=voice_id){
					DATA_calculate_force_voice_Li(voice,new_Li_absolute,extend_blocked)//make its own midi calc
				}
			})
			data.global_variables.Li=new_Li_absolute
		}
	}
	return true
}

function DATA_insert_Na_array(voice_id,segment_index,N_array,overwrite_note=true,overwrite_e=false,overwrite_s=false,overwrite_L=false,expand_structure=false){
	//verify data
	let insert_success=true
	//if(!overwrite_note && !overwrite_e && !overwrite_s && !overwrite_L && !expand_structure)insert_success=false
	if(N_array.length==0)insert_success=false
	if (!insert_success) {
		console.error("Calculator Export error")
		return false
	}

	let max_note_number = TET * N_reg-1
	let outside_range=false
	N_array.forEach((note,index)=>{
		//verify intern range
		if(note<0){
			N_array[index]=0
			outside_range=true
		}
		if(note>max_note_number){
			N_array[index]=max_note_number
			outside_range=true
		}
	})
	if(outside_range){
		APP_info_msg("out_N_range")
	}
	let data = DATA_get_current_state_point(true)
	//verify if voice index !=null
	let data_changed=false
	//create sound _array
	let sound_array=[]
	N_array.forEach(note=>{
		sound_array.push({note:note,diesis:true,A_pos:[],A_neg:[]})
	})
	if(voice_id!=null){
		let current_voice_data = data.voice_data.find(item=>{return item.voice_id==voice_id})
		if(current_voice_data==null){
			console.error("Operation error, data not found")
			return false
		}
		if(segment_index!=null){
			//apply to single segment
			let sound_array_copy=JSON.stringify(current_voice_data.data.segment_data[segment_index].sound)
			insert_success=DATA_calculate_insert_sound_array_segment_data(data,voice_id,segment_index,sound_array,overwrite_note,overwrite_e,overwrite_s,overwrite_L,expand_structure)
			data_changed=(sound_array_copy==JSON.stringify(current_voice_data.data.segment_data[segment_index].sound))?false:true
		}else{
			//apply to entire voice
			let sound_array_index=0
			let souund_array_complete=false
			let sound_array_length=N_array.length
			let last_segment_index=current_voice_data.data.segment_data.length-1
			current_voice_data.data.segment_data.forEach((segment,s_index)=>{
				if(sound_array_complete)return
				if(last_segment_index==s_index)return
				segment.sound.forEach((sound,index)=>{
					if(sound_array_complete)return
					let substitute=false
					if(sound.note>0 && overwrite_note){
						substitute=true
					}else if(sound.note==-1 && overwrite_s){
						substitute=true
					}else if(sound.note==-2 && overwrite_e){
						substitute=true
					}else if(sound.note==-3 && overwrite_L){
						substitute=true
					}
					if(substitute){
						segment.sound[index]=sound_array[sound_array_index]
						sound_array_index++
						data_changed=true
					}
					if(sound_array_length<=sound_array_index)sound_array_complete=true
				})
			})
			if(!sound_array_complete){
				//try to add remaining elements to the last segment
				let sound_array_copy=JSON.stringify(current_voice_data.data.segment_data[last_segment_index].sound)
				//verify last iT
				let last_segment_sound_array=sound_array.slice(sound_array_index)
				insert_success=DATA_calculate_insert_sound_array_segment_data(data,voice_id,last_segment_index,last_segment_sound_array,overwrite_note,overwrite_e,overwrite_s,overwrite_L,expand_structure)
				data_changed=(sound_array_copy==JSON.stringify(current_voice_data.data.segment_data[last_segment_index].sound))?data_changed:true
			}
		}
		current_voice_data.data.midi_data=DATA_segment_data_to_midi_data(current_voice_data.data.segment_data,current_voice_data.neopulse)
	}else{
		if(segment_index!=null){
			//apply to column XXX NOT ALLOWED FOR NOW
			insert_success=false
			data_changed=false
		}else{
			insert_success=false
			data_changed=false
		}
	}
	if (!insert_success) {
		console.error("Calculator Export error")
		return false
	}
	if (!data_changed) {
		console.error("Calculator Export: nothing to do")
		return false
	}
	//save new database
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(false,true)
}

//Note grade
function DATA_insert_iNg_array(voice_id,segment_index,iNg_array,overwrite_note=true,overwrite_e=false,overwrite_s=false,overwrite_L=false,expand_structure=false,reg=3){
	if(iNg_array.length==0){
		console.error("Calculator Export error")
		return false
	}
	let Ng_array=[]
	Ng_array[0]=iNg_array.shift()
	//other notes
	let current_note=Ng_array[0]
	for (let i=0 ; i<iNg_array.length; i++){
		current_note += iNg_array[i]
		Ng_array.push(current_note)
	}
	//ATT! this operation doesn't preserve iN IF not overwrite note line
	//insert
	DATA_insert_Ng_array(voice_id,segment_index,Ng_array,overwrite_note,overwrite_e,overwrite_s,overwrite_L,expand_structure,reg)
}

function DATA_insert_Ng_array(voice_id,segment_index,Ng_array,overwrite_note=true,overwrite_e=false,overwrite_s=false,overwrite_L=false,expand_structure=false,reg=3){
	//verify data
	let insert_success=true
	//if(!overwrite_note && !overwrite_e && !overwrite_s && !overwrite_L && !expand_structure)insert_success=false
	if(Ng_array.length==0)insert_success=false
	if (!insert_success) {
		console.error("Calculator Export error")
		return false
	}
	let data = DATA_get_current_state_point(true)
	let insert_length = Ng_array.length
	let scale_list = DATA_calculate_insert_scale_list(data,voice_id,segment_index,insert_length,overwrite_note,overwrite_s,overwrite_e,overwrite_L)
	//based on scale and data calculate Na
	let Na_array=[]
	let outside_range=false
	let scale_list_length=scale_list.length
	Ng_array.forEach((Ng,index)=>{
		if(index>=scale_list_length)return
		if(scale_list[index]==null){
			Na_array.push(Ng+reg*TET)
			return
		}
		let result=DATA_calculate_scale_note_to_absolute(Ng,0,reg,scale_list[index],data.scale.idea_scale_list)
		if(result.outside_range)outside_range=true
		let new_Na = result.note
		//Na_list.push({note:new_Na,diesis:current_i_note_grade.diesis})
		Na_array.push(new_Na)
	})
	if(outside_range){
		APP_info_msg("out_N_range")
	}
	if(scale_list_length<Ng_array.length){
		//APP_info_msg("calc_not_complete")//warn?? double in case insert Na wont work
		//console.error("Calculator Export incomplete insert")
	}
	//insert
	DATA_insert_Na_array(voice_id,segment_index,Na_array,overwrite_note,overwrite_e,overwrite_s,overwrite_L,expand_structure)
}

function DATA_calculate_insert_scale_list(data,voice_id,segment_index,insert_length,overwrite_note,overwrite_s,overwrite_e,overwrite_L){
	//given the options calculate scale list of inserted elements
	let scale_list=[]
	let current_voice_data = data.voice_data.find(item=>{return item.voice_id==voice_id})
	if(current_voice_data.data.segment_data.length<=segment_index){
		console.error("Operation error, data not found")
		return scale_list
	}
	let segment=null
	let Psg_segment_start=0
	if(segment_index==null){
		segment= DATA_concat_segments(current_voice_data)
	}else{
		if(current_voice_data.data.segment_data.length<=segment_index){
			console.error("Operation error, data not found")
			return []
		}
		segment=current_voice_data.data.segment_data[segment_index]
		for (let i = 0; i < segment_index; i++) {
			Psg_segment_start+=current_voice_data.data.segment_data[i].time.slice(-1)[0].P
		}
	}
	let scale_sequence= data.scale.scale_sequence
	let [N_NP,D_NP]=[current_voice_data.neopulse.N,current_voice_data.neopulse.D]
	//based on options calculate every available position scale
	let last_P=0
	let last_F=0
	let frac_p
	segment.sound.find((sound,index)=>{
		//traduction
		let time = segment.time[index]
		let fraction = segment.fraction.find(fraction=>{return fraction.stop>time.P})
		if (typeof(fraction) == "undefined"){
			//end segment
			return true
		}
		//calculating position
		frac_p = fraction.N/fraction.D
		let Pa_equivalent = (Psg_segment_start+time.P) * N_NP/D_NP + time.F* frac_p * N_NP/D_NP
		let current_scale = DATA_get_Pa_eq_scale(Pa_equivalent , scale_sequence)
		last_P=time.P
		last_F=time.F
		let scale_end_p=0
		//handy store the calculated scale for later calculations
		//only for option elements
		let substitute=false
		if(sound.note>0 && overwrite_note){
			substitute=true
		}else if(sound.note==-1 && overwrite_s){
			substitute=true
		}else if(sound.note==-2 && overwrite_e){
			substitute=true
		}else if(sound.note==-3 && overwrite_L){
			substitute=true
		}
		if(substitute){
			scale_list.push(current_scale)
		}
	})
	//fill with all_scale_length_value elements
	if(insert_length>scale_list.length){
		//last fraction == frac_p
		//last pulse== Pa_equivalent
		//how many position to end scale range???
		let needed_scale=insert_length-scale_list.length
		for (let i = 0; i < needed_scale; i++) {
			let Pa_equivalent = (Psg_segment_start+last_P) * N_NP/D_NP + (last_F+i+1)* frac_p * N_NP/D_NP
			let current_scale = DATA_get_Pa_eq_scale(Pa_equivalent , scale_sequence)
			scale_list.push(current_scale)
		}
	}
	return scale_list
}
