let last_focus_line_name

//LINES

function CALC_add_line(){
	var data_lines=CALC_get_current_state_point()
	var line_data=CALC_data_add_new_line(data_lines)
	CALC_insert_new_state_point(data_lines)
	var focus=true
	CALC_write_line_from_data(line_data,null,focus)
	CALC_refresh_line_counter()
}

//only one boolean true at the time
//all false = only write data as is (full selection based on database)
//focus: full selection and focus
//rewrite_content: NOT USED  modify content (future application?)
//force_grafical_selection: change only Left and select grafically
function CALC_write_line_from_data(line_data,index=null,focus=false,rewrite_content=false,force_grafical_selection=false){
	//rewrite content in casein the future operations will be allowed to modify content
	if((focus&&rewrite_content) || (focus&&force_grafical_selection)||(rewrite_content&&force_grafical_selection)){
		console.error("Write CALC line: more than 1 true boolean not permitted")
		return
	}
	var L_parent = document.querySelector('.App_BNav_calc_lines_container')
	var L_lines_array=[...L_parent.querySelectorAll(".App_BNav_calc_line_box")]
	var R_parent = document.querySelector('.App_BNav_calc_lines_content_container')
	var R_lines_array=[...R_parent.querySelectorAll(".App_BNav_calc_line_content")]
	//write DIVS at hte end if
	if(index==null){index=L_lines_array.length}

	if(L_lines_array.length<=index){
		//new line at the end
		var L_line_string=CALC_calculate_L_line_string(line_data)
		var R_line_string=CALC_calculate_R_line_string(line_data,focus)
		L_parent.insertAdjacentHTML('beforeend',L_line_string)
		R_parent.insertAdjacentHTML('beforeend',R_line_string)
		//scroll to the new line
		CALC_scroll_end()
	}else{
		//modify line
		var L_line_string=CALC_calculate_L_line_string(line_data)
		L_lines_array[index].outerHTML=L_line_string
		if(rewrite_content){
			var R_line_string=CALC_calculate_R_line_string(line_data,focus)
			R_lines_array[index].outerHTML=R_line_string
		}else{
			//control first element color
			var first_input=R_lines_array[index].querySelector(".App_BNav_calc_line_input")
			if(first_input!=null){
				if(line_data.type=='iNa'||line_data.type=='iNgm'){
					first_input.classList.add("pink")
				}else{
					first_input.classList.remove("pink")
				}
			}
		}
		//scroll ??
	}
	if(focus){
		//line creation by user
		var new_R_lines_array=[...R_parent.querySelectorAll(".App_BNav_calc_line_content")]
		CALC_select_line(new_R_lines_array[index])
		//focus on first element
		var first_input=new_R_lines_array[index].querySelector(".App_BNav_calc_line_input").focus()
	}else if(force_grafical_selection){
		//only grafical selection, only for already selected items
		var new_R_lines_array=[...R_parent.querySelectorAll(".App_BNav_calc_line_content")]
		CALC_select_line_grafical(new_R_lines_array[index])
	}else{
		//undo redo && rewrite_content
		if(line_data.selected){
			var new_R_lines_array=[...R_parent.querySelectorAll(".App_BNav_calc_line_content")]
			//!!! recursion
			if(!new_R_lines_array[index].classList.contains("selected") )CALC_select_line(new_R_lines_array[index])
		}
	}
}

function CALC_calculate_L_line_string(line_data){
	var color
	if(line_data.type=='n'){
		color = `var(--Nuzic_blue)`
	}else if(line_data.type=='Na'||line_data.type=='iNa'||line_data.type=='Ngm'||line_data.type=='iNgm'){
		color = `var(--Nuzic_pink)`
	}else if(line_data.type=='T'||line_data.type=='iT'){
		color = `var(--Nuzic_yellow)`
	}
	var active=(line_data.origin.active || line_data.origin.operation=='calc_add')?'active':''
	var origin_string=""
	if(line_data.origin!=''){
		line_data.origin.values.forEach(el=>{
			var extended=(line_data.origin.operation=='calc_import')?'extended':''
			origin_string+= `<div class="App_BNav_calc_line_origin_extra ${extended} ${active}">${el}</div>`
		})
	}

	var L_line_string = `<div class="App_BNav_calc_line_box" onclick="CALC_select_line(this)" name="${line_data.name}">
			<div class="App_BNav_calc_line_box_L">
				<button class="App_BNav_calc_line_delete_button" onclick="CALC_delete_line_button(event,this);"><img src=./Icons/delete.svg></button>
				<div class="App_BNav_calc_line_origin ${active}"><img src=./Icons/${line_data.origin.operation}.svg></div>${origin_string}
			</div>
			<div class="App_BNav_calc_line_box_R">
				<div class="App_BNav_calc_line_type" style='background:${color}'>${CALC_line_data_type_to_text(line_data.type)}</div>
				<div class="App_BNav_calc_line_number">${CALC_line_name_to_text(line_data.name)}</div>
			</div>
	 </div>
	`
	return L_line_string
}

function CALC_line_data_type_to_text(type){
	switch (type) {
		case "iNa":
			return "iSa"
			break;
		case "Ngm":
			return "Nº"
			break;
		case "iNgm":
			return "iSº"
			break;
		case "T":
			return "P"
			break;
		default:
			//n Na iT
			return type
			break;
	}
}

function CALC_calculate_R_line_string(line_data,focus){
	var R_line_string = `<div class="App_BNav_calc_line_content" onclick='CALC_select_line(this)' name="${line_data.name}">`
	//first object
	var first_cell_color=(line_data.type=='iNa'||line_data.type=='iNgm')?"pink":""

	line_data.content.forEach((el,index)=>{
		R_line_string+= `<input class="App_BNav_calc_line_input ${(index==0)?first_cell_color:''}" value="${el}" maxlength="4" onfocusin="CALC_line_input_focus_in(this)"
			onkeypress='CALC_inpTest_int(event,this)' onkeyup="CALC_enter_line_input(this,event)"
			onfocusout="CALC_line_input_focus_out(this)">`
	})
	if(focus && line_data.content.length==0){
		//add empty element to focus on
		R_line_string+= `<input class="App_BNav_calc_line_input ${first_cell_color}" value="" maxlength="4" onfocusin="CALC_line_input_focus_in(this)"
			onkeypress='CALC_inpTest_int(event,this)' onkeyup="CALC_enter_line_input(this,event)"
			onfocusout="CALC_line_input_focus_out(this)">`
	}
	R_line_string+=`<button class="App_BNav_calc_line_add_input" onclick="CALC_add_line_input_button(this)" >+</button></div>`
	return R_line_string
}

function CALC_line_name_to_text(name){
	var text=""
	if(name<10){
		text=`[0${name}]`
	}else{
		text=`[${name}]`
	}
	return text
}

function CALC_line_name_to_index(data,name){
	var index = null
	data.find((line,i)=>{
		if(line.name==name){
			index=i
			return true
		}
	})
	return index
}

function CALC_refresh_line_counter(){
	var value=CALC_get_current_state_point().length
	var lineCounter = document.querySelector('.App_BNav_calc_box_menu_L_end').querySelector('p')
	lineCounter.innerHTML=`(${value})`
}

function CALC_scroll_end(){
	var scroll = document.querySelector('.App_BNav_calc_box_content_R')
	scroll.scrollTop=scroll.scrollHeight
}

function CALC_delete_all_lines_button(){
	var data_lines=[]
	CALC_insert_new_state_point(data_lines)
	CALC_load_state_point_data()
}

function CALC_delete_all_lines(){
	var parent = document.querySelector('.App_BNav_calc_lines_container')
	var line_content_parent = document.querySelector('.App_BNav_calc_lines_content_container')
	parent.innerHTML=''
	line_content_parent.innerHTML=''
}

function CALC_delete_line_button(event,button){
	event.stopPropagation()
	var parent = button.closest('.App_BNav_calc_lines_container')
	var line = button.closest('.App_BNav_calc_line_box')
	var line_content_parent = document.querySelector('.App_BNav_calc_lines_content_container')
	var index = [...parent.children].indexOf(line)
	if(line.classList.contains("selected")){
		CALC_clear_selection()
	}
	//select to be deleted line in order to save state x undo
	CALC_select_line(line)

	//first change data (change children left)
	var data_lines = CALC_data_remove_line(index)
	CALC_insert_new_state_point(data_lines)
	line.innerHTML=''
	line.remove()
	line_content_parent.children[index].innerHTML=''
	line_content_parent.children[index].remove()
	//again clear selection (clear number selected)
	CALC_clear_selection()
	CALC_refresh_line_counter()
	CALC_hide_all_menus()
}

function CALC_delete_all_lines_button_hover_in(bttn){
	var lines_container = document.querySelector('.App_BNav_calc_lines_container')
	bttn.style.background='var(--Nuzic_red)'
	lines_container.style.background='var(--Nuzic_red_light)'
}

function CALC_delete_all_lines_button_hover_out(bttn){
	var lines_container = document.querySelector('.App_BNav_calc_lines_container')
	bttn.style.background='var(--Nuzic_red_light)'
	lines_container.style.background='transparent'
}

function CALC_select_line_grafical(line){
	//no clear selection
	var line_index = [...line.parentNode.children].indexOf(line)
	var right_line = document.querySelector('.App_BNav_calc_lines_content_container').children[line_index]
	var left_line = document.querySelector('.App_BNav_calc_lines_container').children[line_index]
	right_line.classList.add('selected')
	left_line.classList.add('selected')
}

function CALC_select_line(line){
	CALC_clear_selection()
	CALC_select_line_grafical(line)
	// CALC_clear_selection()
	var line_index = [...line.parentNode.children].indexOf(line)
	// var right_line = document.querySelector('.App_BNav_calc_lines_content_container').children[line_index]
	// var left_line = document.querySelector('.App_BNav_calc_lines_container').children[line_index]
	// right_line.classList.add('selected')
	// left_line.classList.add('selected')

	var data_lines=CALC_get_current_state_point()
	//write line name in upper box
	var lineBox = document.querySelector('.App_BNav_calc_box_menu_L_end').querySelector('div')
	lineBox.innerHTML=CALC_line_name_to_text(data_lines[line_index].name)

	if(last_focus_line_name!=null){
		//if selection from a input box that need line name
		//!!! translate input not compatible with every line type (n)
		//came from operation_input_focusout
		last_focus_line_name.value=CALC_line_name_to_text(data_lines[line_index].name)
		//jump input box
		var inp_indx = [...last_focus_line_name.parentNode.children].indexOf(last_focus_line_name)
		if(last_focus_line_name.parentNode.children[inp_indx+2]!=null){
			last_focus_line_name.parentNode.children[inp_indx+2].focus()
		}
	}

	var error=CALC_refresh_line_type_selection(data_lines[line_index],line_index,false)
	if(error)console.error("CALC line selection changed database")
}

function CALC_clear_selection(){
	var line_selected_R =document.querySelector('.App_BNav_calc_lines_content_container').querySelector('.selected')
	var line_selected_L = document.querySelector('.App_BNav_calc_lines_container').querySelector('.selected')
	if(line_selected_R!=null){
		line_selected_R.classList.remove('selected')
	}
	if(line_selected_L!=null){
		line_selected_L.classList.remove('selected')
	}
	//clear number
	document.querySelector('.App_BNav_calc_box_menu_L_end').querySelector('div').innerHTML=""
	CALC_clear_line_type_selector()
}

function CALC_clear_line_type_selector(){
	var parent = [...document.querySelector('.App_BNav_calc_line_type_selector').children]
	parent.forEach(el=>{
		el.style.display='none'
		if(el.classList.contains('selected'))el.classList.remove('selected')
	})
}

function CALC_show_line_type_selector_button(string){
	document.getElementById(string).style.display='block'
}

function CALC_assign_line_type_button(bttn){
	var data_lines = CALC_get_current_state_point()
	var line_selected_R = document.querySelector('.App_BNav_calc_line_content.selected')
	var line_index = [...line_selected_R.parentNode.children].indexOf(line_selected_R)
	data_lines[line_index].type = bttn.value
	//CALC_disconnect_line_origin_data(data_lines[line_index])

	//change buttons ,rewrite line partially, activate play buttons
	var error= CALC_refresh_line_type_selection(data_lines[line_index],line_index,true)
	if(error)console.error("CALC changing line type modified data")
	CALC_insert_new_state_point(data_lines)
}

function CALC_activate_play_button_data_line(data_line){
	var button = document.querySelector('.App_BNav_calc_play')
	if(data_line.type=='Na'||data_line.type=='iNa'||data_line.type=='iT'){
		button.disabled=false
	}else{
		button.disabled=true
	}
}

function CALC_disconnect_line_origin_data(line_data){
	if(line_data.origin.operation!='calc_add')line_data.origin.active=false
}

function CALC_refresh_line_type_selection(line_data,line_index,redraw_L_line_selected=false){
	CALC_clear_line_type_selector()
	var type = line_data.type

	//activate compatible buttons
	CALC_show_line_type_selector_button('CALC_force_button')
	if((type=='Na'||type=='iNa'||type=='iT') && line_data.content.length!=0)CALC_show_line_type_selector_button('CALC_translate_button')
	if(type=='T' && line_data.content.length>1)CALC_show_line_type_selector_button('CALC_translate_button')
	CALC_show_line_type_selector_button('CALC_n')

	var max_Na=TET*N_reg-1
	var iSCounter=0
	var calc_iNa= true
	var calc_iNgm= true
	line_data.content.forEach(el=>{
		iSCounter+=el
		if(iSCounter<0||iSCounter>max_Na){
			calc_iNa=false
		}
		if(iSCounter<-TET||iSCounter>max_Na){
			calc_iNgm=false
		}
	})
	if(calc_iNa==true){
		CALC_show_line_type_selector_button('CALC_iNa')
	}
	if(calc_iNgm==true){
		CALC_show_line_type_selector_button('CALC_iNgm')
	}
	var calc_Na= false
	if(Math.min.apply(Math,line_data.content)>=0&& Math.max.apply(Math,line_data.content)<=max_Na){
		calc_Na=true
		CALC_show_line_type_selector_button('CALC_Na')
	}
	var calc_Ngm= false
	if(Math.min.apply(Math,line_data.content)>=-max_Na && Math.max.apply(Math,line_data.content)<=max_Na){
		calc_Ngm= true
		CALC_show_line_type_selector_button('CALC_Ngm')
	}

	var calc_T= true
	var calc_iT= false
	if(line_data.content[0]!=0 && line_data.content.length!=0){
		calc_T=false
	}else{
		for(let i=1;i<line_data.content.length;i++){
			if(line_data.content[i]<=line_data.content[i-1]){
				calc_T=false
			}
		}
		if(calc_T==true){
			CALC_show_line_type_selector_button('CALC_T')
		}
	}
	if(line_data.content.some(v => v <= 0) == false){
		calc_iT=true
		CALC_show_line_type_selector_button('CALC_iT')
	}

	//select correct
	var calc_type_string = 'CALC_'+type
	var wanted_selection=document.getElementById(calc_type_string)
	var changed=false

	if(wanted_selection.style.display=="none"){
		//content changed
		line_data.type="n"
		document.getElementById("CALC_n").classList.add('selected')
		//CALC_disconnect_line_origin_data(line_data)
		document.getElementById("CALC_translate_button").style.display='none'
		changed=true
	}else{
		wanted_selection.classList.add('selected')
	}

	//redraw left line if content changed or type changed (exclude selection recursion)
	if(redraw_L_line_selected || changed){
		var focus=false
		var rewrite_content=false
		var force_grafical_selection=true
		CALC_write_line_from_data(line_data,line_index,focus,rewrite_content,force_grafical_selection)
	}
	//control if playable
	CALC_activate_play_button_data_line(line_data)
	return changed
}

function CALC_add_line_input_button(bttn){
	var line_content = bttn.parentElement
	var newInput = document.createElement('input')
	newInput.classList.add('App_BNav_calc_line_input')
	newInput.setAttribute('onkeyup',"CALC_enter_line_input(this,event)")
	newInput.setAttribute('onfocusout',"CALC_line_input_focus_out(this)")
	newInput.setAttribute('onfocusin',"CALC_line_input_focus_in(this)")
	newInput.setAttribute('onkeypress','CALC_inpTest_int(event,this)')
	newInput.setAttribute('maxlength',"4")
	newInput.value=''
	line_content.insertBefore(newInput,bttn)
	newInput.focus()
}

function CALC_div_scroll_y_R(thisScroll){
	var scrollL = document.querySelector('.App_BNav_calc_box_content_L')
	scrollL.scrollTop=thisScroll.scrollTop
}

function CALC_div_scroll_y_L(thisScroll){
	var scrollL = document.querySelector('.App_BNav_calc_box_content_R')
	scrollL.scrollTop=thisScroll.scrollTop
}

//INPUT MANIPULATION

let prev_calc_inp_value =''
function CALC_line_input_focus_in(input){
	prev_calc_inp_value=input.value
	//for operation extract subset, fill inp_box
	var cut_at_inp =  document.getElementById('calc_cut_at_position')
	var parent = [...input.parentNode.children]
	var index = parent.indexOf(input)
	cut_at_inp.value=index+1
}

function CALC_line_input_focus_out(input){
	if(input.value!=prev_calc_inp_value){
		//new data point
		var parent = input.parentNode
		var line_index  = [...parent.parentNode.children].indexOf(parent)
		var tempArr = []
		var inputArr = [...parent.children]
		inputArr.forEach((el,index) => {
			if(index<parent.children.length-1){
				if(el.value!=''){
					tempArr.push(parseInt(el.value))
				}
			}
		})
		var data_lines = CALC_get_current_state_point()
		data_lines[line_index].content=tempArr
		CALC_disconnect_line_origin_data(data_lines[line_index])
		//correct data type if not ok
		//force rewriting line L
		var rewrite_line_L=true
		CALC_refresh_line_type_selection(data_lines[line_index],line_index,rewrite_line_L)
		CALC_insert_new_state_point(data_lines)
	}

	if(input.value==''){
		input.remove()
	}
}

function CALC_enter_line_input(input,event){
	if (event.code === 'Enter') {
		if(input.value!=""){
			//new empty input
			var newInput = document.createElement('input')
			newInput.classList.add('App_BNav_calc_line_input')
			newInput.setAttribute('onkeyup',"CALC_enter_line_input(this,event)")
			newInput.setAttribute('onfocusout',"CALC_line_input_focus_out(this)")
			newInput.setAttribute('onfocusin',"CALC_line_input_focus_in(this)")
			newInput.setAttribute('onkeypress','CALC_inpTest_int(event,this)')
			newInput.setAttribute('maxlength',"4")
			newInput.value=''
			input.parentNode.insertBefore(newInput,input.nextSibling)
			newInput.focus()
		}else{
			input.nextSibling.focus()
		}
	}
}

//INPUT BOX CHARS

//calc line inp box
function CALC_inpTest_int(evt,element){
	var ch = String.fromCharCode(evt.which)
	if(!((/[0-9]/.test(ch)) || (/[-]/.test(ch)))){
		evt.preventDefault();
	}

	var isSelected = APP_input_box_is_selected(element)
	var isEmpty = !!(element.value=="")
	var prev_char = APP_input_box_prev_char(element)

	let stringOk = false
	//if - must be alone or first
	if(/[-]/.test(ch) && !element.value.includes("-") && (isSelected || prev_char=="")){
		stringOk = true
	}

	//number not before a minus sign
	if( /[0-9]/.test(ch)){
		let string = APP_input_box_prev_str(element)+ch+APP_input_box_next_str(element)
		if(!isNaN(string)){
			stringOk = true
		}
	}

	if(!stringOk){
		evt.preventDefault()
	}
}

function CALC_inpTest_intPos(evt,element){
	var ch = String.fromCharCode(evt.which)
	if(!(/[0-9]/.test(ch))){
		evt.preventDefault();
	}
}

function CALC_inpTest_line(evt,element){
	var ch = String.fromCharCode(evt.which)

	if(!(/[0-9]/.test(ch) || ch==`[` || ch==`]`)){
		evt.preventDefault();
	}

	var isSelected = APP_input_box_is_selected(element)
	var isEmpty = !!(element.value=="")
	var prev_char = APP_input_box_prev_char(element)
	var prev_str = APP_input_box_prev_str(element)
	var next_str = APP_input_box_next_str(element)

	let stringOk = false

	//if [ must be alone or first
	if(ch==`[` && ((!element.value.includes("[") && prev_char=="") || isSelected )){
		stringOk = true
	}

	//if ] must be alone and last
	if(ch==`]` && !isSelected && prev_char!="" && prev_char!="[" && prev_char!="]" && next_str==""){
		stringOk = true
	}

	//if number is
	// second or third value
	if( /[0-9]/.test(ch) && !isSelected && !isEmpty && prev_str!="" && prev_char!="]"){
		stringOk = true
	}

	// if (evt.keyCode == 13) {
		//exit focus
	// 	element.blur()
	// }

	if(!stringOk){
		evt.preventDefault()
	}
}

function CALC_inpTest_lineIntPos(evt,element){
	var ch = String.fromCharCode(evt.which)
	if(!(/[0-9]/.test(ch) || ch==`[` || ch==`]`)){
		evt.preventDefault();
	}

	var isSelected = APP_input_box_is_selected(element)
	var isEmpty = !!(element.value=="")
	var prev_char = APP_input_box_prev_char(element)
	var prev_str = APP_input_box_prev_str(element)
	var next_str = APP_input_box_next_str(element)

	let stringOk = false

	//if [ must be alone or first
	if(ch==`[` && ((!element.value.includes("[") && prev_char=="") || isSelected )){
		stringOk = true
	}

	//if ] must be alone and last
	if(ch==`]` && !isSelected && prev_char!="" && prev_char!="[" && prev_char!="]" && next_str==""){
		stringOk = true
	}

	//if number is
	// second or third value or only numbers
	if( /[0-9]/.test(ch) && !isSelected && !isEmpty && prev_str!="" && prev_char!="]"){
		stringOk = true
	}
	//or number alone
	if( /[0-9]/.test(ch) && (isSelected || (!prev_str.includes("[") && !prev_str.includes("]")))){
		stringOk = true
	}

	// if (evt.keyCode == 13) {
		//exit focus
	// 	element.blur()
	// }

	if(!stringOk){
		evt.preventDefault()
	}
}

//OPERATIONS MENU

function CALC_display_menu_operation(button){
	CALC_hide_all_menus()
	var container = document.querySelector(".App_BNav_calc_menu_operations_container")
	var boxes = [...container.getElementsByClassName("App_BNav_calc_menu_operation")]
	boxes.forEach(el=>{
		el.style.display="none"
	})
	container.style.display="block"
	var current_box = document.getElementById(button.value)
	current_box.style.display="flex"

	if(button.value=="App_BNav_calc_ran"){
		//control current selection
		var select = current_box.querySelector('.App_BNav_calc_menu_select')
		CALC_operation_random_select_option(select)
	}

	var selected_line = document.querySelector(".App_BNav_calc_line_content.selected")

	//clear boxes //XXX if only [...] need something better ?? sum can contain number AND line
	var inp_box_list=[...current_box.querySelectorAll("input")]
	inp_box_list.forEach(input=>{input.value=""})
	inp_box_list[0].focus()

	if(selected_line!=null){
		var selected_line_name=parseInt(selected_line.getAttribute("name"))
		var name_string=CALC_line_name_to_text(selected_line_name)
		inp_box_list[0].value=name_string
		if(inp_box_list[1]!=null)inp_box_list[1].focus()
		if(inp_box_list[2]!=null){
			inp_box_list[2].value=name_string
			if(inp_box_list[2].style.display!="none")inp_box_list[2].focus()
		}
	}
}

function CALC_hide_menu_operation(){
	var container = document.querySelector(".App_BNav_calc_menu_operations_container")
	container.style.display="none"
}

function CALC_operation_random_select_option(select){
	var operation_container= select.closest(".App_BNav_calc_menu_operation")
	var line_content_array= [...operation_container.querySelectorAll('.App_BNav_calc_menu_input_container')]
	line_content_array.forEach(line=>{line.style.display="none"})
	if(select.value=='minmax'){
		line_content_array[2].style.display='flex'
		line_content_array[3].style.display='flex'
	}else if(select.value=='set'){
		line_content_array[0].style.display='flex'
	}else if(select.value=='shuffle'){
		line_content_array[1].style.display='flex'
	}
}

function CALC_menu_operation_ok_button(button){
	var operation_container= button.closest(".App_BNav_calc_menu_operation")
	var success=false
	var input_list = [...operation_container.querySelectorAll('.App_BNav_calc_menu_input')]
	var input_error_list=[]
	var select = operation_container.querySelector('.App_BNav_calc_menu_select_container select')
	switch (operation_container.id) {
		case "App_BNav_calc_sum":
			var [sum_line_index1,] = CALC_read_input(input_list[0])
			var [sum_line_index2,value] = CALC_read_input(input_list[1])
			if(sum_line_index1!=null){
				if(sum_line_index2!=null){
					//2 line sum
					success = CALC_two_lines_sum(sum_line_index1,sum_line_index2,select.value=='sum_abs')
					input_error_list.push(input_list[0])
					input_error_list.push(input_list[1])
				}else if(value!=null){
					//1 line sum
					success = CALC_line_sum(sum_line_index1,value,select.value=='sum_abs')
					input_error_list.push(input_list[0])
				}else{
					input_error_list.push(input_list[1])
				}
			}else{
				input_error_list.push(input_list[0])
			}
		break;
		case "App_BNav_calc_sub":
			var [sub_line_index1,] = CALC_read_input(input_list[0])
			var [sub_line_index2,value] = CALC_read_input(input_list[1])
			if(sub_line_index1!=null){
				if(sub_line_index2!=null){
					//2 line sum
					success = CALC_two_lines_subtraction(sub_line_index1,sub_line_index2,select.value=='sub_abs')
					input_error_list.push(input_list[0])
					input_error_list.push(input_list[1])
				}else if(value!=null){
					//1 line sum
					success = CALC_line_subtraction(sub_line_index1,value,select.value=='sub_abs')
					input_error_list.push(input_list[0])
				}else{
					input_error_list.push(input_list[1])
				}
			}else{
				input_error_list.push(input_list[0])
			}
		break;
		case "App_BNav_calc_rot":
			var [rot_line_index,] = CALC_read_input(input_list[0])
			var [,rot] = CALC_read_input(input_list[1])
			if(rot!=null){
				input_error_list.push(input_list[0])
				if(rot_line_index!=null)success=CALC_line_rotation(rot_line_index, rot)
			}else{
				input_error_list.push(input_list[1])
			}
		break;
		case "App_BNav_calc_rep":
			var [rep_line_index,] = CALC_read_input(input_list[0])
			var [,rep] = CALC_read_input(input_list[1])
			if(rep_line_index!=null){
				if(rep!=null && rep>0) {
					success = CALC_line_repeat(rep_line_index,rep)
					//if error is content = []
					input_error_list.push(input_list[0])
				}else{
					input_error_list.push(input_list[1])
				}
			}else{
				input_error_list.push(input_list[0])
			}
		break;
		case "App_BNav_calc_h_m":
			var [h_m_line_index,] = CALC_read_input(input_list[0])
			if(h_m_line_index!=null){
				success = CALC_line_horizontal_mirror(h_m_line_index)
			}
			input_error_list.push(input_list[0])
		break;
		case "App_BNav_calc_v_m":
			var [v_m_line_index,] = CALC_read_input(input_list[0])
			var [,axis] = CALC_read_input(input_list[1])
			if(v_m_line_index!=null){
				if(axis!=null) {
					success = CALC_line_vertical_mirror(v_m_line_index,axis)
					//if error is content = []
					input_error_list.push(input_list[0])
				}else{
					input_error_list.push(input_list[1])
				}
			}else{
				input_error_list.push(input_list[0])
			}
		break;
		case "App_BNav_calc_conc":
			var [conc_line_index1,] = CALC_read_input(input_list[0])
			var [conc_line_index2,] = CALC_read_input(input_list[1])
			if(conc_line_index1!=null){
				if(conc_line_index2!=null){
					//2 line sum
					success = CALC_lines_merge(conc_line_index1,conc_line_index2)
					input_error_list.push(input_list[0])
					input_error_list.push(input_list[1])
				}else{
					input_error_list.push(input_list[1])
				}
			}else{
				input_error_list.push(input_list[0])
			}
		break;
		case "App_BNav_calc_spl":
			var [spl_line_index,] = CALC_read_input(input_list[0])
			var [,spl_index_cut] = CALC_read_input(input_list[1])
			if(spl_line_index!=null){
				if(spl_index_cut!=null && spl_index_cut>=0) {
					var extract=(select.value=="first")?0:(select.value=="second")?1:2
					success =  CALC_line_split(spl_line_index,spl_index_cut,extract)
					//if error is content = []
					input_error_list.push(input_list[0])
				}else{
					input_error_list.push(input_list[1])
				}
			}else{
				input_error_list.push(input_list[0])
			}
		break;
		case "App_BNav_calc_ran":
			if(select.value=="minmax"){
				var [,min_value] = CALC_read_input(input_list[3])
				var [,max_value] = CALC_read_input(input_list[4])
				var [,n_items] = CALC_read_input(input_list[5])
				if(min_value!=null){
					if(max_value!=null){
						if(n_items!=null && n_items>0){
							success = CALC_line_random_from_range(min_value,max_value,n_items)
							//error if min == max
							input_error_list.push(input_list[3])
							input_error_list.push(input_list[4])
						}else{
							input_error_list.push(input_list[5])
						}
					}else{
						input_error_list.push(input_list[4])
					}
				}else{
					input_error_list.push(input_list[3])
				}
			}else if(select.value=="set"){
				var [ran_line_index,] = CALC_read_input(input_list[0])
				var [,ran_items] = CALC_read_input(input_list[1])
				if(ran_line_index!=null){
					if(ran_items!=null && ran_items>0) {
						success = CALC_line_random_from_line(ran_line_index, ran_items)
						//if error is content = []
						input_error_list.push(input_list[0])
					}else{
						input_error_list.push(input_list[1])
					}
				}else{
					input_error_list.push(input_list[0])
				}
			}else{
				//shuffle
				var [ran_line_index,] = CALC_read_input(input_list[2])
				if(ran_line_index!=null)success = CALC_line_random_shuffle(ran_line_index)
				input_error_list.push(input_list[2])
			}
		break;
	}
	if(success){
		CALC_hide_menu_operation()
		CALC_refresh_line_counter()
	}else{
		APP_info_msg("op_not_succes")
		input_error_list.forEach(inp=>{
			if(inp.style)APP_blink_error(inp)
		})
	}
}

function CALC_read_input(inp_box){
	//return [line_index, integer]
	var string = inp_box.value
	if(string.charAt(0)!='['||string.charAt(string.length-1)!=']'){
		//try number
		if(isNaN(string) || string==""){
			return [null,null]
		}else{
			return [null,parseInt(string)]
		}
	}else{
		//try line
		var new_string=string.substring(1,string.length-1)
		if(new_string.length==0 || isNaN(new_string)){
			return [null,null]
		}else{
			var name=parseInt(new_string)
			var current_state_point = CALC_get_current_state_point()
			var line_index=CALC_line_name_to_index(current_state_point,name)
			return [line_index,null]
		}
	}
}

//MENUS

document.addEventListener('click',function(event){
	CALC_menu_import_refresh_selection_target()
	CALC_menu_export_refresh_content()
	//always close
	//onclick="event.stopPropagation()" on box and buttons
	CALC_hide_menu_force()
	CALC_hide_menu_translate()
})

function CALC_display_menu_container(){
	var menu = document.querySelector("#App_BNav_calc_menu_container")
	menu.style.display="flex"
}

function CALC_hide_menu_container(){
	var menu = document.querySelector("#App_BNav_calc_menu_container")
	//dont close if
	if(document.querySelector("#App_BNav_calc_menu_import").style.display!="none" ||
		document.querySelector("#App_BNav_calc_menu_export").style.display!="none")return
	menu.style.display="none"
}

function CALC_hide_all_menus(){
	CALC_hide_menu_operation()
	CALC_hide_menu_import()
	CALC_hide_menu_export()
	CALC_hide_menu_force()
	CALC_hide_menu_translate()
}

function CALC_display_menu_import(){
	CALC_hide_all_menus()
	CALC_display_menu_container()
	var menu = document.querySelector("#App_BNav_calc_menu_import")
	menu.style.display="flex"
	CALC_menu_import_refresh_content()
	event.stopPropagation()
}

function CALC_hide_menu_import(){
	var menu = document.querySelector("#App_BNav_calc_menu_import")
	menu.style.display="none"
	menu.querySelector('.App_BNav_calc_menu_apply_button').disabled=true
	CALC_hide_menu_container()
}

function CALC_display_menu_export(){
	CALC_hide_all_menus()
	CALC_display_menu_container()
	var menu = document.querySelector("#App_BNav_calc_menu_export")
	menu.style.display="flex"
	CALC_menu_export_refresh_content()
	event.stopPropagation()
}

function CALC_hide_menu_export(){
	var menu = document.querySelector("#App_BNav_calc_menu_export")
	menu.style.display="none"
	menu.querySelector('.App_BNav_calc_menu_apply_button').disabled=true
	CALC_hide_menu_container()
}

function CALC_display_menu_force(event){
	CALC_hide_all_menus()
	CALC_display_menu_container()
	var menu_force = document.querySelector("#App_BNav_calc_menu_force")
	menu_force.style.display="flex"
	event.stopPropagation()
	//write line name and type
	var selected_line = document.querySelector(".App_BNav_calc_line_content.selected")
	var line_name=document.querySelector("#App_BNav_calc_menu_force_line_name")

	//no case no selected line
	var selected_line_index =[...selected_line.parentNode.children].indexOf(selected_line)
	var calc_data=CALC_get_current_state_point()
	var selected_line_name=calc_data[selected_line_index].name
	line_name.innerHTML=CALC_line_name_to_text(selected_line_name)
	//var selected_line_type=calc_data[selected_line_index].type
	//colors and text and contents
	CALC_check_force_content()
}

function CALC_hide_menu_force(){
	var menu_force = document.querySelector("#App_BNav_calc_menu_force")
	menu_force.style.display="none"
	CALC_hide_menu_container()
}

function CALC_display_menu_translate(event){
	CALC_hide_all_menus()
	CALC_display_menu_container()
	var menu_translate = document.querySelector('#App_BNav_calc_menu_translate')
	menu_translate.style.display='flex'
	event.stopPropagation()

	//write line name and type
	var selected_line = document.querySelector(".App_BNav_calc_line_content.selected")
	var line_name=document.querySelector("#App_BNav_calc_menu_translate_line_name")
	var line_type=document.querySelector("#App_BNav_calc_menu_translate_line_type")
	var line_type_target=document.querySelector("#App_BNav_calc_menu_translate_line_to_type")

	//no case no selected line
	var selected_line_index =[...selected_line.parentNode.children].indexOf(selected_line)
	var calc_data=CALC_get_current_state_point()
	var selected_line_name=calc_data[selected_line_index].name
	//var selected_line_name=parseInt(selected_line.getAttribute("name"))
	line_name.innerHTML=CALC_line_name_to_text(selected_line_name)
	var selected_line_type=calc_data[selected_line_index].type
	line_type.innerHTML=CALC_line_data_type_to_text(selected_line_type)
	switch (selected_line_type) {
		case "Na":
			line_type.style.background='var(--Nuzic_pink)'
			line_type.style.outline='2px solid var(--Nuzic_pink)'
			line_type_target.innerHTML='iSa'
			line_type_target.style.background='var(--Nuzic_pink)'
			line_type_target.style.outline='2px solid var(--Nuzic_pink)'
			break;
		case "iNa":
			line_type.style.background='var(--Nuzic_pink)'
			line_type.style.outline='2px solid var(--Nuzic_pink)'
			line_type_target.innerHTML='Na'
			line_type_target.style.background='var(--Nuzic_pink)'
			line_type_target.style.outline='2px solid var(--Nuzic_pink)'
			break;
		case "T":
			line_type.style.background='var(--Nuzic_yellow)'
			line_type.style.outline='2px solid var(--Nuzic_yellow)'
			line_type_target.innerHTML='iT'
			line_type_target.style.background='var(--Nuzic_yellow)'
			line_type_target.style.outline='2px solid var(--Nuzic_yellow)'
			break;
		case "iT":
			line_type.style.background='var(--Nuzic_yellow)'
			line_type.style.outline='2px solid var(--Nuzic_yellow)'
			line_type_target.innerHTML='P'
			line_type_target.style.background='var(--Nuzic_yellow)'
			line_type_target.style.outline='2px solid var(--Nuzic_yellow)'
			break;
	}
}

function CALC_hide_menu_translate(){
	var menu_translate = document.querySelector("#App_BNav_calc_menu_translate")
	menu_translate.style.display='none'
	CALC_hide_menu_container()
}

//LINE IMPORT

function CALC_menu_import_refresh_content(){
	//refresh voice/segment selection
	CALC_menu_import_refresh_selection_target()
	//type / refresh reg selector
	CALC_menu_import_refresh_type_selector()
}

function CALC_menu_import_refresh_selection_target(){
	var menu = document.querySelector("#App_BNav_calc_menu_import")
	var target_text = document.querySelector("#App_BNav_calc_menu_import_container div")
	if(APP_selection_options.target!=null){
		menu.querySelector('.App_BNav_calc_menu_apply_button').disabled=false
		if(APP_selection_options.target=='voice'){
			target_text.innerHTML=`[v${APP_selection_options.voice_id}]`
		}else if(APP_selection_options.target=='segment'){
			target_text.innerHTML=`[v${APP_selection_options.voice_id}s${APP_selection_options.segment_index}]`
		}
	}else{
		menu.querySelector(".App_BNav_calc_menu_apply_button").disabled=true
		target_text.innerHTML=`[v-s-]`
	}
}

function CALC_menu_import_refresh_type_selector(){
	var menu = document.querySelector("#App_BNav_calc_menu_import")
	var type_container=menu.querySelector(".App_BNav_calc_menu_type_container")
	var select=menu.querySelector(".App_BNav_calc_menu_select_container select")
	var line_type= type_container.querySelector("input:checked").value
	if(line_type=="Ngm" ||line_type=="iNgm"){
		select.disabled=false
	}else{
		select.disabled=true
	}
}

function CALC_import_line(){
	if(APP_selection_options.target!=null){
		let menu = document.querySelector("#App_BNav_calc_menu_import")
		let type_container=menu.querySelector(".App_BNav_calc_menu_type_container")
		let select=menu.querySelector(".App_BNav_calc_menu_select_container select")
		let line_type= type_container.querySelector("input:checked").value
		let data = DATA_get_current_state_point()
		let index_v = data.voice_data.findIndex(voice=>{
			return voice.voice_id==APP_selection_options.voice_id
		})
		let new_content =[]
		switch (line_type) {
			case "Na":
				if(APP_selection_options.target=='voice'){
					data.voice_data[index_v].data.segment_data.forEach(seg=>{
						seg.sound.forEach(sound=>{
							if(sound.note>=0){
								new_content.push(sound.note)
							}
						})
					})
				}else if(APP_selection_options.target=='segment'){
					data.voice_data[index_v].data.segment_data[APP_selection_options.segment_index].sound.forEach(sound=>{
						if(sound.note>=0){
							new_content.push(sound.note)
						}
					})
				}
				break;
			case "iNa":
				let note_list = []
				if(APP_selection_options.target=='voice'){
					data.voice_data[index_v].data.segment_data.forEach(seg=>{
						seg.sound.forEach(sound=>{
							if(sound.note>=0){
								note_list.push(sound.note)
							}
						})
					})
				}else if(APP_selection_options.target=='segment'){
					data.voice_data[index_v].data.segment_data[APP_selection_options.segment_index].sound.forEach(sound=>{
						if(sound.note>=0){
							note_list.push(sound.note)
						}
					})
				}
				new_content = DATA_calculate_iNa_from_Na_list(note_list)
				break;
			case "Ngm":
				var reg=null
				if(select.value!="null")reg=parseInt(select.value)
				if(APP_selection_options.target=='voice'){
					new_content = DATA_calculate_Ng_import_from_data_index(APP_selection_options.voice_id,null,reg)
				}else if(APP_selection_options.target=='segment'){
					new_content = DATA_calculate_Ng_import_from_data_index(APP_selection_options.voice_id,APP_selection_options.segment_index,reg)
				}
				break;
			case "iNgm":
				var reg=null
				if(select.value!="null")reg=parseInt(select.value)
				if(APP_selection_options.target=='voice'){
					new_content = DATA_calculate_iNg_import_from_data_index(APP_selection_options.voice_id,null,reg)
				}else if(APP_selection_options.target=='segment'){
					new_content = DATA_calculate_iNg_import_from_data_index(APP_selection_options.voice_id,APP_selection_options.segment_index,reg)
				}
				break;
			case "T":
				if(APP_selection_options.target=='voice'){
					var segment_start=0
					data.voice_data[index_v].data.segment_data.forEach(seg=>{
						seg.time.forEach(pulse=>{
							new_content.push(pulse.P+segment_start)
						})
						segment_start+=seg.time[seg.time.length-1].P
					})
					lineType='n'
				}else if(APP_selection_options.target=='segment'){
					data.voice_data[index_v].data.segment_data[APP_selection_options.segment_index].time.forEach(pulse=>{
						new_content.push(pulse.P)
					})
				}
				break;
			case "iT":
				if(APP_selection_options.target=='voice'){
					data.voice_data[index_v].data.segment_data.forEach(seg=>{
						var temp_list = DATA_calculate_iT_list_from_segment_data(seg)
						temp_list.forEach(el=>{
							new_content.push(el)
						})
					})
				}else if(APP_selection_options.target=='segment'){
					new_content = DATA_calculate_iT_list_from_segment_data(data.voice_data[index_v].data.segment_data[APP_selection_options.segment_index])
				}
				break;
		}

		//console.log(new_content)
		if(line_type=="T"){
			//verify for double pulses
			var mom=-1
			var error = new_content.some((item,i)=>{
				if(item==mom)return true
				if(i!=0 && item==0)return true
				mom=item
			})
			if(error)line_type='n'
		}

		if(new_content.length!=0){
			var calc_data=CALC_get_current_state_point()
			var new_name=CALC_new_line_name()
			var from=""
			if(APP_selection_options.target=='voice'){
				from=`[v${APP_selection_options.voice_id}]`
			}else if(APP_selection_options.target=='segment'){
				from=`[v${APP_selection_options.voice_id}s${APP_selection_options.segment_index}]`
			}
			var new_line = CALC_generate_generic_line_data(new_content,new_name,'calc_import',[from],[])
			new_line.type=line_type
			calc_data.push(new_line)
			//no need select
			//var left_line = document.querySelector('.App_BNav_calc_lines_container').children[index]
			//CALC_select_line(left_line)
			CALC_insert_new_state_point(calc_data)
			CALC_write_line_from_data(new_line,calc_data.length-1)
			CALC_hide_menu_import()
			CALC_refresh_line_counter()
		}else{
			console.error("Calc Import: no data found ")
			APP_info_msg("op_not_succes")
		}
	}
}

//LINE EXPORT

function CALC_menu_export_refresh_content(){
	var menu = document.querySelector("#App_BNav_calc_menu_export")
	var from_text = document.querySelector("#App_BNav_calc_export_from")
	var from_type = document.querySelector("#App_BNav_calc_export_from_type")
	var target_text = document.querySelector("#App_BNav_calc_export_to")
	var apply_button = menu.querySelector(".App_BNav_calc_menu_apply_button")
	var from_success=false
	var target_success=false

	var selected_line = document.querySelector(".App_BNav_calc_line_content.selected")

	if(selected_line!=null){
		var selected_line_index =[...selected_line.parentNode.children].indexOf(selected_line)
		var data_lines=CALC_get_current_state_point()
		var current_line_data = data_lines[selected_line_index]
		var selected_line_name=CALC_line_name_to_text(current_line_data.name)
		var selected_line_type=current_line_data.type
		from_text.innerHTML=selected_line_name
		switch (selected_line_type) {
			case "Na":
				from_type.innerHTML=CALC_line_data_type_to_text(selected_line_type)
				from_type.style.backgroundColor='var(--Nuzic_pink)'
				from_success=true
				break;
			case "iNa":
				from_type.innerHTML=CALC_line_data_type_to_text(selected_line_type)
				from_type.style.backgroundColor='var(--Nuzic_pink)'
				from_success=true
				break;
			case "Ngm":
				from_type.innerHTML=CALC_line_data_type_to_text(selected_line_type)
				from_type.style.backgroundColor='var(--Nuzic_pink)'
				from_success=true
				break;
			case "iNgm":
				from_type.innerHTML=CALC_line_data_type_to_text(selected_line_type)
				from_type.style.backgroundColor='var(--Nuzic_pink)'
				from_success=true
				break;
			case "T":
				from_type.innerHTML=CALC_line_data_type_to_text(selected_line_type)
				from_type.style.backgroundColor='var(--Nuzic_yellow)'
				from_success=false
				break;
			case "iT":
				from_type.innerHTML=CALC_line_data_type_to_text(selected_line_type)
				from_type.style.backgroundColor='var(--Nuzic_yellow)'
				from_success=true
				break;
			case "n":
				from_type.innerHTML=CALC_line_data_type_to_text(selected_line_type)
				from_type.style.backgroundColor='var(--Nuzic_blue)'
				from_success=false
				break;
			default:
				from_type.innerHTML="[···]"
				selected_line_type.style.backgroundColor=''
				break;
		}

		if(current_line_data.content.length==0)from_success=false

		var button_list = [...document.querySelector("#App_BNav_calc_menu_export_replace_container").children]
		if(selected_line_type=='Na'||selected_line_type=='iNa'|| selected_line_type=='Ngm' || selected_line_type=='iNgm'){
			button_list.forEach(button =>{
				button.disabled=false
			})
		}else{
			button_list.forEach(button =>{
				button.disabled=true
			})
		}
		var select=document.querySelector("#App_BNav_calc_menu_select_export")
		if(selected_line_type=='Ngm' || selected_line_type=='iNgm'){
			select.disabled=false
		}else{
			select.disabled=true
		}
	}else{
		from_text.innerHTML="[···]"
		from_type.innerHTML=""
		from_type.style.backgroundColor=''
	}

	if(APP_selection_options.target!=null){
		if(APP_selection_options.target=='voice'){
			target_text.innerHTML=`[v${APP_selection_options.voice_id}]`
		}else if(APP_selection_options.target=='segment'){
			target_text.innerHTML=`[v${APP_selection_options.voice_id}s${APP_selection_options.segment_index}]`
		}
		target_success=true
	}else{
		target_text.innerHTML=`[v-s-]`
	}

	if(from_success && target_success){
		apply_button.disabled=false
	}else{
		apply_button.disabled=true
	}
}

function CALC_export_line(){
	var selected_line = document.querySelector(".App_BNav_calc_line_content.selected")
	if(selected_line!=null && APP_selection_options.target!=null){
		var selected_line_index =[...selected_line.parentNode.children].indexOf(selected_line)
		var data_lines=CALC_get_current_state_point()
		var current_line_data = data_lines[selected_line_index]
		var selected_line_type=current_line_data.type
		var content=current_line_data.content
		if(content.length==0){
			APP_info_msg("op_not_succes")
			return
		}
		var voice_id = APP_selection_options.voice_id
		var segment_index = APP_selection_options.segment_index
		var n_button= document.querySelector('#calc_export_n_button').classList.contains("checked")
		var e_button= document.querySelector('#calc_export_e_button').classList.contains("checked")
		var s_button= document.querySelector('#calc_export_s_button').classList.contains("checked")
		var L_button= document.querySelector('#calc_export_L_button').classList.contains("checked")
		var expand_structure =(document.querySelector('#App_BNav_calc_menu_export_keepLs').checked)? false : true
		var select = document.querySelector("#App_BNav_calc_menu_select_export")
		var reg = parseInt(select.value)
		switch (selected_line_type) {
			case "Na":
				DATA_insert_Na_array(voice_id,segment_index,content,n_button,e_button,s_button,L_button,expand_structure)
				break;
			case "iNa":
				DATA_insert_iNa_array(voice_id,segment_index,content,n_button,e_button,s_button,L_button,expand_structure)
				break;
			case "Ngm":
				DATA_insert_Ng_array(voice_id,segment_index,content,n_button,e_button,s_button,L_button,expand_structure,reg)
				break;
			case "iNgm":
				DATA_insert_iNg_array(voice_id,segment_index,content,n_button,e_button,s_button,L_button,expand_structure,reg)
				break;
			case "iT":
				DATA_insert_iT_array(voice_id,segment_index,content,expand_structure)
				break;
		}
		CALC_hide_menu_export()
	}
}

function CALC_export_check_note_replacements(button){
	if(button.classList.contains('checked')){
		button.classList.remove('checked')
	}else{
		button.classList.add('checked')
	}
}

//LINE TRANSLATE

function CALC_line_translate(){
	var selected_line = document.querySelector(".App_BNav_calc_line_content.selected")
	var selected_line_index =[...selected_line.parentNode.children].indexOf(selected_line)
	var data_lines=CALC_get_current_state_point()
	var current_line_data = data_lines[selected_line_index]
	var selected_line_name=current_line_data.name
	var selected_line_type=current_line_data.type
	var content = current_line_data.content
	if(content.length==0){
		APP_info_msg("op_not_succes")
		return
	}
	var new_content=[]
	var new_type
	switch (selected_line_type) {
		case "Na":
			new_content = [content[0]]
			for(let i=1;i<content.length;i++){
				new_content.push(content[i]-content[i-1])
			}
			new_type="iNa"
			break;
		case "iNa":
			new_content = [content[0]]
			for(let i=1;i<content.length;i++){
				new_content.push(new_content[new_content.length-1]+content[i])
			}
			new_type="Na"
			break;
		case "T":
			for(let i=0;i<content.length-1;i++){
				new_content.push(content[i+1]-content[i])
			}
			new_type="iT"
			break;
		case "iT":
			new_content = [0]
			content.forEach(iT=>{
				new_content.push(new_content[new_content.length-1]+iT)
			})
			new_type="T"
			break;
	}

	if(new_content.length!=0){
		var new_name=CALC_new_line_name()
		var new_line = CALC_generate_generic_line_data(new_content,new_name,'calc_translate',[CALC_line_name_to_text(current_line_data.name)],[current_line_data.name])
		new_line.type=new_type
		data_lines.push(new_line)
		data_lines[selected_line_index].children.push(new_name)
		//select first line bf saving no need, already selected
		//var left_line = document.querySelector('.App_BNav_calc_lines_container').children[line_index]
		//CALC_select_line(left_line)
		CALC_insert_new_state_point(data_lines)
		CALC_write_line_from_data(new_line,data_lines.length-1)
		CALC_hide_menu_translate()
		CALC_refresh_line_counter()
	}
}

//LINE FORCE

function CALC_check_force_content(){
	var menu = document.querySelector("#App_BNav_calc_menu_force")
	var type_container = menu.querySelector(".App_BNav_calc_menu_type_container")
	var line_type_target= type_container.querySelector("input:checked").value
	var line_type_target_div=document.querySelector("#App_BNav_calc_menu_force_line_type")
	var container_Na = menu.querySelector("#App_BNav_calc_menu_force_container_option_Na")
	var container_iNa = menu.querySelector("#App_BNav_calc_menu_force_container_option_iNa")
	var container_T = menu.querySelector("#App_BNav_calc_menu_force_container_option_T")
	var container_iT = menu.querySelector("#App_BNav_calc_menu_force_container_option_iT")
	container_Na.style.display="none"
	container_iNa.style.display="none"
	container_T.style.display="none"
	container_iT.style.display="none"
	line_type_target_div.innerHTML=CALC_line_data_type_to_text(line_type_target)
	switch (line_type_target) {
		case "Na":
			line_type_target_div.style.background='var(--Nuzic_pink)'
			container_Na.style.display=""
			break;
		case "iNa":
			line_type_target_div.style.background='var(--Nuzic_pink)'
			container_iNa.style.display=""
			CALC_check_force_content_iNa_selector()
			break;
		case "T":
			line_type_target_div.style.background='var(--Nuzic_yellow)'
			container_T.style.display=""
			break;
		case "iT":
			line_type_target_div.style.background='var(--Nuzic_yellow)'
			container_iT.style.display=""
			break;
	}
}

function CALC_check_force_content_iNa_selector(){
	var container_iNa = document.querySelector("#App_BNav_calc_menu_force_container_option_iNa")
	var select=container_iNa.querySelector("select")
	var starting_note = select.value
	var manual_note = container_iNa.querySelector("#App_BNav_calc_menu_force_starting_note")
	if(starting_note=="manual"){
		manual_note.disabled=false
	}else{
		manual_note.disabled=true
	}
}

function CALC_line_force(){
	var menu = document.querySelector("#App_BNav_calc_menu_force")
	var type_container = menu.querySelector(".App_BNav_calc_menu_type_container")
	var line_type_target= type_container.querySelector("input:checked").value
	var container_Na = menu.querySelector("#App_BNav_calc_menu_force_container_option_Na")
	var container_iNa = menu.querySelector("#App_BNav_calc_menu_force_container_option_iNa")
	var container_T = menu.querySelector("#App_BNav_calc_menu_force_container_option_T")
	var container_iT = menu.querySelector("#App_BNav_calc_menu_force_container_option_iT")

	//selected line
	var selected_line = document.querySelector(".App_BNav_calc_line_content.selected")
	var selected_line_index =[...selected_line.parentNode.children].indexOf(selected_line)
	var data_lines=CALC_get_current_state_point()
	var current_line_data = data_lines[selected_line_index]
	var selected_line_name=current_line_data.name
	var selected_line_type=current_line_data.type
	var selected_line_content = current_line_data.content

	var new_content=[]
	if(selected_line_content.length==0){
		APP_info_msg("op_not_succes")
		return
	}

	var max_value=TET*N_reg-1
	switch (line_type_target) {
		case "Na":
			var select_neg=container_Na.querySelector("select")
			var neg_note = select_neg.value
			var select_rg=container_Na.querySelector("select:last-child")
			var rg_note = select_rg.value
			selected_line_content.forEach(val=>{
				if(val<0){
					if(neg_note=="abs"){
						new_content.push(Math.abs(val))
					}else if(neg_note=="to0"){
						new_content.push(0)
					}//else delete
				}else if(val>max_value){
					if(rg_note=="max"){
						new_content.push(max_value)
					}//else delete
				}else{
					new_content.push(val)
				}
			})
			break;
		case "iNa":
			var select=container_iNa.querySelector("select")
			var starting_note_option = select.value
			var manual_note_str = container_iNa.querySelector("#App_BNav_calc_menu_force_starting_note").value
			var starting_note=0//selected_line_content[0] is the real starting note
			if(starting_note_option=="manual"){
				starting_note=(manual_note_str!="")? parseInt(manual_note_str):0
				new_content.push(starting_note)
			}
			var prev=starting_note
			selected_line_content.forEach(val=>{
				if(prev+val>max_value){
					new_content.push(max_value-prev)
					prev=max_value
				}else if(prev+val<0){
					new_content.push(0-prev)
					prev=0
				}else{
					new_content.push(val)
					prev+=val
				}
			})
			break;
		case "T":
			var select=container_T.querySelector("select")
			var neg_option = select.value
			selected_line_content.forEach(val=>{
				if(val<0){
					if(neg_option=="abs"){
						new_content.push(Math.abs(val))
					}//else if(neg_option=="to0"){
						//new_content.push(0)//equivalent to deletion
					//}//else delete
				}else{
					new_content.push(val)
				}
			})
			//control first element is 0 and elements ordered
			new_content.push(0)
			new_content.sort(function(a, b){return a-b})
			new_content = [...new Set(new_content)]
			break;
		case "iT":
			var select_neg=container_iT.querySelector("select")
			var neg_val = select_neg.value
			var select_zero=container_iT.querySelector("select:last-child")
			var zero_val = select_zero.value

			selected_line_content.forEach(val=>{
				if(val<0){
					if(neg_val=="abs"){
						new_content.push(Math.abs(val))
					}//delete
				}else if(val==0){
					if(zero_val=="to1"){
						new_content.push(1)
					}//delete
				}else{
					new_content.push(val)
				}
			})
			break;
	}

	if(new_content.length!=0){
		var new_name=CALC_new_line_name()
		var new_line = CALC_generate_generic_line_data(new_content,new_name,'calc_force',[CALC_line_name_to_text(current_line_data.name)],[current_line_data.name])
		new_line.type=line_type_target
		data_lines.push(new_line)
		data_lines[selected_line_index].children.push(new_name)
		//select first line bf saving no need, already selected
		//var left_line = document.querySelector('.App_BNav_calc_lines_container').children[line_index]
		//CALC_select_line(left_line)
		CALC_insert_new_state_point(data_lines)
		CALC_write_line_from_data(new_line,data_lines.length-1)
		CALC_hide_menu_force()
		CALC_refresh_line_counter()
	}else{
		APP_info_msg("op_not_succes")
	}
}

//PLAY SOUNDS

function CALC_line_play(){
	var selected_line = document.querySelector(".App_BNav_calc_line_content.selected")
	var selected_line_index =[...selected_line.parentNode.children].indexOf(selected_line)
	var data_lines=CALC_get_current_state_point()
	var current_line_data = data_lines[selected_line_index]
	var content = Array.from(current_line_data.content)
	if(current_line_data.type=='Na'){
		APP_play_this_calc_N_line(content)
	}else if(current_line_data.type=='iNa'){
		APP_play_this_calc_iN_line(content)
	}else if(current_line_data.type=='iT'){
		APP_play_this_calc_iT_line(content)
	}
}

//BETTER SOLUTION ???

function CALC_operation_input_focusout(inp){
	last_focus_line_name= inp
	setTimeout(function(){
		last_focus_line_name=null
	},200)
}


