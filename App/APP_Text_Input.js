//input box text control

function APP_inpTest_letter(evt,element){
	let ch = String.fromCharCode(evt.which);
	if(!(/[a-zA-Z]/.test(ch))){
		evt.preventDefault()
	}
	if (evt.keyCode == 13) {
		//exit focus
		element.blur()
	}
}

function APP_inpTest_acronym(event){
	let ch = String.fromCharCode(event.which);
	let idea_scale_list=DATA_get_idea_scale_list()
	let already_used = _test_letter(ch)
	if(!already_used){
		event.preventDefault()
		//verify if uppercase char is used
		let new_ch= ch.toUpperCase()
		let already_used_UP = _test_letter(new_ch)
		if(!already_used_UP){
			//blink an
			APP_blink_error(event.target)
		}else{
			//do something
			event.target.value=new_ch
			//oninput=""
			BNAV_S_circle_show_apply_button(event.target)
		}
	}
	function _test_letter(char){
		return idea_scale_list.some(item=>{
			return item.acronym===char
		})
	}
}

function APP_inpTest_text(event,element){
	let ch = String.fromCharCode(event.which);
	if(!(/^[a-zA-Z0-9!@#$%\^&*)(+=._-]*$/.test(ch) || /[\s]/.test(ch))){
		event.preventDefault()
	}
	if (evt.keyCode == 13) {
		//exit focus
		element.blur()
	}
}

function APP_inpTest_intNo0(evt){  //XXX not used
	let ch = String.fromCharCode(evt.which);
	if(!(/[1-9]/.test(ch))){
		evt.preventDefault()
	}
}

function APP_inpTest_intPos(evt,element){
	let ch = String.fromCharCode(event.which);
	if(!(/[0-9]/.test(ch))){
		evt.preventDefault()
	}
	if (evt.keyCode == 13) {
		//exit focus
		element.blur()
	}
}

function APP_inpTest_Fr(evt,element){
	let ch = String.fromCharCode(event.which);
	if(!((/[0-9]/.test(ch)) || (/[/]/.test(ch)))){
		evt.preventDefault()
	}
}

function APP_inpTest_NP(evt,element){
	let ch = String.fromCharCode(evt.which);
	if(!((/[0-9]/.test(ch)) || (/[/]/.test(ch)))){
		evt.preventDefault()
	}
	if (evt.keyCode == 13) {
		//exit focus
		element.blur()
	}
}

//Sound line absolute
function APP_inpTest_Na_line(evt,element){
	let ch = String.fromCharCode(evt.which)
	if(!((/[0-9]/.test(ch)) || (/[s]/.test(ch)) || (/[l]/.test(ch)) || (/[e]/.test(ch)))){
		evt.preventDefault()
		return
	}
	let isSelected = APP_input_box_is_selected(element)
	let isEmpty = !!(element.value=="")
	//if s must be alone
	let stringOk = false
	if((/[s]/.test(ch)) && (isSelected || isEmpty)){
		stringOk = true
	}
	//if e must be alone
	if((/[e]/.test(ch)) && (isSelected || isEmpty)){
		stringOk = true
	}
	//if l must be alone
	if((/[l]/.test(ch)) && (isSelected || isEmpty)){
		stringOk = true
	}
	//if number
	if( /[0-9]/.test(ch) && ((isSelected || isEmpty) || !isNaN(element.value))){
		//control if < TET*N_reg-1
		let string = parseInt(APP_input_box_prev_str(element)+ch+APP_input_box_next_str(element))//!! defined fractioning
		if(isSelected || string<TET*N_reg){
			stringOk = true
		}
	}
	if(!stringOk){
		evt.preventDefault()
	}
}

function APP_inpTest_Nm_line(evt,element){
	//element.value=previous_value
	let ch = String.fromCharCode(evt.which)
	if(!((/[0-9]/.test(ch)) || (/[s]/.test(ch)) || (/[r]/.test(ch)) || (/[l]/.test(ch)) || (/[e]/.test(ch)))){
		evt.preventDefault();
		return
	}
	let isSelected = APP_input_box_is_selected(element)
	let isEmpty = !!(element.value=="")
	//if s must be alone
	let stringOk = false
	if((/[s]/.test(ch) && (isSelected || isEmpty))){
		stringOk = true
	}
	//if e must be alone
	if((/[e]/.test(ch) && (isSelected || isEmpty))){
		stringOk = true
	}
	//if l must be alone
	if((/[l]/.test(ch) && (isSelected || isEmpty))){
		stringOk = true
	}
	//if r must be after a number of 1 or 2 char
	let prev_str = APP_input_box_prev_str(element)
	let prev_char = APP_input_box_prev_char(element)
	if((/[r]/.test(ch) 	&& !(isSelected || isEmpty)
						&& !element.value.includes("r") && parseInt(prev_str)<TET
						&& prev_char!="")
		){
		stringOk = true
	}
	//if number is
	// first or second value of another number OR
	if( /[0-9]/.test(ch) && (	(isSelected || isEmpty) ||
								!isNaN(prev_str)
							)
		){
		//control if < TET
		let string = parseInt(prev_str+ch)
		if(string<TET && string>-TET){
		//if((string<TET && string>-TET) || isSelected){
			stringOk = true
		}
	}
	//number after a r
	if( /[0-9]/.test(ch) && (prev_char=="r" && element.value.slice(-1)=="r") && ch<N_reg){
		stringOk = true
	}
	if(!stringOk){
		evt.preventDefault()
	}
}

function APP_inpTest_Ngm_line(evt,element){
	//max 7 char   11+11r0
	let ch = String.fromCharCode(evt.which)
	if(element.classList.contains("App_RE_inp_box")){
		let position = RE_input_to_position(element)
		let iT=0
		if(previous_value==""){
			iT=1
		}
		let current_scale = DATA_get_object_index_info(position.voice_id,position.segment_index,position.sound_index,iT,false,false,true).scale
		if(current_scale==null){
			evt.preventDefault()
			return
		}
		//find previous note
		let from_index=(previous_value=="")?position.sound_index+1:position.sound_index
		let previous_element_position = DATA_get_object_prev_note_index(position.voice_id,position.segment_index,from_index)
		let previous_scale = null
		if(previous_element_position!= null){
			let prev_element_info = DATA_get_object_index_info(position.voice_id,position.segment_index,previous_element_position,0,false,false,true)
			//reg= c
			previous_scale = prev_element_info.scale
		}
		let is_special= false
		if(previous_scale!=null){
			if(previous_scale.scale_index!=current_scale.scale_index){
				// different scales
				is_special=true
			}
		}else{
			//first element in segment
			is_special=true
		}
		if(is_special){
			//first possible note???
			if(!element.classList.contains("App_RE_note_bordered_cell")){
				element.classList.add("App_RE_note_bordered_cell")
				element.addEventListener("focusout",()=>{
					element.classList.remove("App_RE_note_bordered_cell")
				})
			}
		}
	}else{
		//PMC current selected voice, no need to define exact scale
		// if(previous_value==no_value){
		// 	evt.preventDefault()
		// 	return
		// }
	}
	if(!((/[0-9]/.test(ch)) || (/[r]/.test(ch)) || (/[+]/.test(ch)) || (/[-]/.test(ch)) || (/[s]/.test(ch)) || (/[l]/.test(ch)) || (/[e]/.test(ch)))){
		evt.preventDefault();
	}
	let isSelected = APP_input_box_is_selected(element)
	let isEmpty = !!(element.value=="")
	//if s must be alone
	let stringOk = false
	if((/[s]/.test(ch) && (isSelected || isEmpty))){
		stringOk = true
	}
	//if e must be alone
	if((/[e]/.test(ch) && (isSelected || isEmpty))){
		stringOk = true
	}
	//if l must be alone
	if((/[l]/.test(ch) && (isSelected || isEmpty))){
		stringOk = true
	}
	let caret_position = evt.target.selectionStart
	let before = element.value.slice(0,caret_position)
	caret_position = evt.target.selectionEnd
	let after = element.value.slice(caret_position,element.value.length)
	//if + or - must be after something that start with a number
	if((/[+]/.test(ch) || /[-]/.test(ch))	&& !(isSelected || isEmpty)){
		//must not exist a + or - or r before position
		if(!isNaN(element.value[0]) && !before.includes("r")){
			if(!before.includes("-") && !after.includes("-") && !before.includes("+") && !after.includes("+")){
				stringOk = true
			}
		}
	}
	//if - can be after a r
	if(/[-]/.test(ch)	&& !(isSelected || isEmpty)){
		//must not exist a + or - or r before position
		if(before[before.length-1]=="r"){
			stringOk = true
		}
	}
	//if r must be after something that start with a number
	if(/[r]/.test(ch) 	&& !(isSelected || isEmpty)){
		//MUST exist at least a number no + or + after and no other r
		if(!isNaN(element.value[0]) && !after.includes("-") && !after.includes("+") && !before.includes("r") && !after.includes("r")){
			stringOk = true
		}
	}
	//if number is
	// first or second value of another number OR
	// number at start (2 char)
	if(/[0-9]/.test(ch)){
		if(isSelected || element.value==""){
		}
		if(after==""){
			if(before.length<=1){
				stringOk = true
			}else{
				if(before.includes("+")){
					let split = before.split('+')
					if(split[1].length<2){
						stringOk = true
					}
				}
				if(before.includes("-")){
					let split = before.split('-')
					if(split[1].length<2){
						stringOk = true
					}
				}
			}
		}else{
			//max 2 int before a + or -
			if(after.includes("+")){
				let split = after.split('+')
				if(split[0].length+before.length<2){
					stringOk = true
				}
			}
			if(after.includes("-")){
				let split = after.split('-')
				if(split[0].length+before.length<2){
					stringOk = true
				}
			}
			//max 2 int before a r
			if(after.includes("r") && !after.includes("+") && !after.includes("-")){
				let split = after.split('r')
				let b = split[0]
				let a=""
				if(before.includes("+")){
					split = before.split('+')
					a=split[1]
				}else if (before.includes("+")) {
				 	split = before.split('-')
					a=split[1]
				}else{
				 	a=before
				}
				if(b.length+a.length<2){
					stringOk = true
				}
			}
		}
	}
	//number after a r
	if( /[0-9]/.test(ch) && before.includes("r") && before[before.length-1]=="r" && after.length==0 && ch<N_reg){
		stringOk = true
	}
	if(!stringOk){
		evt.preventDefault()
	}
}

function APP_inpTest_Nabc_line(evt,element){
	evt.preventDefault()
}

function APP_inpTest_iNa_line(evt,element){
	let ch = String.fromCharCode(evt.which)
	let position = RE_input_to_position(element)
	//find previous note
	let from_index=(previous_value=="")?position.sound_index+1:position.sound_index
	let previous_element_position = DATA_get_object_prev_note_index(position.voice_id,position.segment_index,from_index)
	if(previous_element_position== null){
		//first possible note???
		if(!element.classList.contains("App_RE_note_bordered_cell")){
			element.classList.add("App_RE_note_bordered_cell")
			element.addEventListener("focusout",()=>{
				element.classList.remove("App_RE_note_bordered_cell")
			})
		}
	}
	if(!((/[0-9]/.test(ch)) || (/[s]/.test(ch)) || (/[l]/.test(ch)) || (/[-]/.test(ch)) || (/[e]/.test(ch)))){
		evt.preventDefault()
		return
	}
	let isSelected = APP_input_box_is_selected(element)
	let isEmpty = !!(element.value=="")
	let prev_char = APP_input_box_prev_char(element)
	let stringOk = false
	//if - must be alone or first
	if(/[-]/.test(ch) && !element.value.includes("-") && (isSelected || prev_char=="")){
		stringOk = true
	}
	//if s must be alone
	if(/[s]/.test(ch) && (isSelected || isEmpty)){
		//console.log("rad")
		stringOk = true
	}
	//if e must be alone
	if(/[e]/.test(ch) && (isSelected || isEmpty)){
		stringOk = true
	}
	//if l must be alone
	if(/[l]/.test(ch) && (isSelected || isEmpty)){
		stringOk = true
	}
	if( /[0-9]/.test(ch)){
		let string = parseInt(APP_input_box_prev_str(element)+ch+APP_input_box_next_str(element))
		if(string<TET*N_reg && string>-TET*N_reg){
			stringOk = true
		}
	}
	if(!stringOk){
		evt.preventDefault()
	}
}

//fractioning ????
function APP_inpTest_iNm_line(evt,element){
	let ch = String.fromCharCode(evt.which)
	let position = RE_input_to_position(element)
	//find previous note
	let from_index=(previous_value=="")?position.sound_index+1:position.sound_index
	let previous_element_position = DATA_get_object_prev_note_index(position.voice_id,position.segment_index,from_index)
	if(previous_element_position== null){
		//first possible note???
		if(!element.classList.contains("App_RE_note_bordered_cell")){
			element.classList.add("App_RE_note_bordered_cell")
			element.addEventListener("focusout",()=>{
				element.classList.remove("App_RE_note_bordered_cell")
			})
		}
	}
	if(!((/[0-9]/.test(ch)) || (/[-]/.test(ch)) || (/[s]/.test(ch)) || (/[r]/.test(ch)) || (/[l]/.test(ch)) || (/[e]/.test(ch)))){
		evt.preventDefault()
		return
	}
	let isSelected = APP_input_box_is_selected(element)
	let isEmpty = !!(element.value=="")
	let prev_char = APP_input_box_prev_char(element)
	let stringOk = false
	//if - must be alone or first
	if(/[-]/.test(ch) && !element.value.includes("-") && (isSelected || prev_char=="")){
		stringOk = true
	}
	//if s must be alone
	if(/[s]/.test(ch) && (isSelected || isEmpty)){
		//console.log("rad")
		stringOk = true
	}
	//if e must be alone
	if(/[e]/.test(ch) && (isSelected || isEmpty)){
		stringOk = true
	}
	//if l must be alone
	if(/[l]/.test(ch) && (isSelected || isEmpty)){
		stringOk = true
	}
	//if r must be after a number of 1 or 2 char
	if(/[r]/.test(ch) && !(isSelected || isEmpty)
					&& !element.value.includes("r")
					&& prev_char!=""
		){
		stringOk = true
	}
	//if number is
	let prev_str = APP_input_box_prev_str(element)
	// first or second value of another number
	if( /[0-9]/.test(ch) && (	(isSelected || isEmpty) ||
								!isNaN(prev_str) ||
								(prev_char=="-")
							)
		){
		//control if < TET
		let string = parseInt(prev_str+ch)
		if(string<TET && string>-TET){
			stringOk = true
		}
	}
	//number after a r
	if( /[0-9]/.test(ch) && (prev_char=="r" && element.value.slice(-1)=="r") && ch<N_reg){
		stringOk = true
	}
	if(!stringOk){
		evt.preventDefault()
	}
}

function APP_inpTest_iNgm_line(evt,element){
	let ch = String.fromCharCode(evt.which);
	let isSelected = APP_input_box_is_selected(element)
	let isEmpty = !!(element.value=="")
	let prev_char = APP_input_box_prev_char(element)
	//verify if is a new scale or not
	//using database in order to find prev element note/scale/etc...
	let position = RE_input_to_position(element)
	let iT=(previous_value=="")?1:0
	let current_scale = DATA_get_object_index_info(position.voice_id,position.segment_index,position.sound_index,iT,false,false,true).scale
	if(current_scale==null){
		evt.preventDefault()
		return
	}
	//find previous note
	let from_index=(previous_value=="")?position.sound_index+1:position.sound_index
	let previous_element_position=DATA_get_object_prev_note_index(position.voice_id,position.segment_index,from_index)
	let previous_scale = null
	//var previous_Na=0
	if(previous_element_position!= null){
		let prev_element_info = DATA_get_object_index_info(position.voice_id,position.segment_index,previous_element_position,0,false,false,true)
		previous_scale = prev_element_info.scale
		//bordered if new scale no need , made by APP_inpTest_Ngm_line
		// if(previous_scale!=null){
		// 	if(previous_scale.scale_index!=current_scale.scale_index){
				// different scales
		// 		if(!element.classList.contains("App_RE_note_bordered_cell")){
		// 			element.classList.add("App_RE_note_bordered_cell")
		// 			element.addEventListener("focusout",()=>{
		// 				element.classList.remove("App_RE_note_bordered_cell")
		// 			})
		// 		}
		// 	}
		// }
	}else{
		//bordered if first
		// if(!element.classList.contains("App_RE_note_bordered_cell")){
		// 	element.classList.add("App_RE_note_bordered_cell")
		// 	element.addEventListener("focusout",()=>{
		// 		element.classList.remove("App_RE_note_bordered_cell")
		// 	})
		// }
	}
	let write_iN = true
	if(previous_scale!=null){
		if(previous_scale.scale_index!=current_scale.scale_index){
			// different scales
			write_iN=false
		}
	}else{
		//first element in segment
		write_iN=false
	}
	if(write_iN){
		//calculate note interval and make a traduction in current scale //PREV OR CURRENT SCALE??? XXX XXX
		if(!((/[0-9]/.test(ch)) || (/[-]/.test(ch)) || (/[s]/.test(ch)) || (/[l]/.test(ch)) || (/[e]/.test(ch)))){
			evt.preventDefault()
			return
		}
		let stringOk = false
		//if - must be alone or first and only one
		if(/[-]/.test(ch) && !element.value.includes("-") && (isSelected || prev_char=="")){
			stringOk = true
		}
		//if s must be alone
		if((/[s]/.test(ch) && (isSelected || isEmpty))){
			//console.log("rad")
			stringOk = true
		}
		//if e must be alone
		if((/[e]/.test(ch) && (isSelected || isEmpty))){
			stringOk = true
		}
		//if l must be alone
		if((/[l]/.test(ch) && (isSelected || isEmpty))){
			stringOk = true
		}
		// first or second value of another number
		if( /[0-9]/.test(ch) && (	isSelected || isEmpty ||
									(element.value.includes("-") && prev_char!="") || !element.value.includes("-")
								)
			){
			//if number is max X values depending of TET
			let string = parseInt(APP_input_box_prev_str(element)+ch+APP_input_box_next_str(element))
			if(string<TET*N_reg && string>-TET*N_reg){
				stringOk = true
			}
			//stringOk = true
		}
		if(!stringOk){
			evt.preventDefault()
			return
		}
	}else{
		//write note in current scale
		//apply Ngm roules
		APP_inpTest_Ngm_line(evt,element)
	}
}

//iA lines
function APP_inpTest_iAa(evt,element){
	let ch = String.fromCharCode(evt.which)
	if(!((/[0-9]/.test(ch)) || (/[s]/.test(ch)) || (/[l]/.test(ch)) || (/[-]/.test(ch)) || (/[e]/.test(ch)))){
		evt.preventDefault()
		return
	}
	let isSelected = APP_input_box_is_selected(element)
	let isEmpty = !!(element.value=="")
	//if - must be alone
	let stringOk = false
	if(/[-]/.test(ch) && (isSelected || isEmpty)){
		stringOk = true
	}
	//if s must be alone
	if(/[s]/.test(ch) && (isSelected || isEmpty)){
		stringOk = true
	}
	//if e must be alone
	if(/[e]/.test(ch) && (isSelected || isEmpty)){
		stringOk = true
	}
	//if l must be alone
	if(/[l]/.test(ch) && (isSelected || isEmpty)){
		stringOk = true
	}
	//if number, only numbers or minus sign
	//length depends on TET >= 15 == 4 char (-100)
	let max_length = 3
	if(TET>=15)max_length++
	let prev_value= 10*(max_length-2)
	if( /[0-9]/.test(ch) && (
		(isSelected || element.value=="") ||
		(!isNaN(element.value) && element.value<prev_value && element.value>-prev_value) ||
		(element.value=="-")
		)
	){
		stringOk = true
	}
	if(!stringOk){
		evt.preventDefault()
	}
}

function APP_inpTest_iAm(evt,element){
	let ch = String.fromCharCode(evt.which);
	if(!((/[0-9]/.test(ch)) || (/[-]/.test(ch)) || (/[s]/.test(ch)) || (/[r]/.test(ch)) || (/[l]/.test(ch)) || (/[e]/.test(ch)))){
		evt.preventDefault()
		return
	}
	let isSelected = APP_input_box_is_selected(element)
	let isEmpty = !!(element.value=="")
	//if - must be alone
	let stringOk = false
	if(/[-]/.test(ch) && (isSelected || isEmpty)){
		stringOk = true
	}
	//if s must be alone
	if((/[s]/.test(ch) && (isSelected || isEmpty))){
		stringOk = true
	}
	//if e must be alone
	if((/[e]/.test(ch) && (isSelected || isEmpty))){
		stringOk = true
	}
	//if l must be alone
	if((/[l]/.test(ch) && (isSelected || isEmpty))){
		stringOk = true
	}
	//if r must be after a number of 1 or 2 char
	let prev_str = APP_input_box_prev_str(element)
	let prev_char = APP_input_box_prev_char(element)
	if((/[r]/.test(ch) 	&& !(isSelected || isEmpty)
						&& !element.value.includes("r") && parseInt(prev_str)<TET
						&& prev_char!="")
		){
		stringOk = true
	}
	// if((/[r]/.test(ch) && !(
	// 							isSelected || isEmpty)
	// 							&& !isNaN(element.value)
	// 							&& element.value<100
	// 							&& element.value>-100
	// 						)
	// 	){
	// 	stringOk = true
	// }
	//if number is
	// first or second value of another number OR
	if( /[0-9]/.test(ch) && (	(isSelected || isEmpty) ||
								!isNaN(prev_str)
							)
		){
		//control if < TET
		let string = parseInt(prev_str+ch)
		if(string<TET && string>-TET){
		//if((string<TET && string>-TET) || isSelected){
			stringOk = true
		}
	}
	// if( /[0-9]/.test(ch) && (	(isSelected || isEmpty) ||
	// 							!isNaN(element.value) ||
								//(element.value.slice(-1)=="r") ||
	// 							(element.value=="-")
	// 						)
	// 	){
		//control if < TET
	// 	let string = parseInt(element.value+ch)// prev + ch + next
	// 	if((string<TET && string>-TET) || isSelected){
	// 		stringOk = true
	// 	}
	// }
	//number after a r
	if( /[0-9]/.test(ch) && (prev_char=="r" && element.value.slice(-1)=="r") && ch<N_reg){
		stringOk = true
	}
	// if( /[0-9]/.test(ch) && (element.value.slice(-1)=="r") && ch<N_reg){
	// 	stringOk = true
	// }
	if(!stringOk){
		evt.preventDefault()
	}
}

function APP_inpTest_iAgm(evt,element){
	let ch = String.fromCharCode(evt.which);
	let isSelected = APP_input_box_is_selected(element)
	let isEmpty = !!(element.value=="")
	let prev_char = APP_input_box_prev_char(element)
	//NOT THIS APP_inpTest_iNgm_line(evt,element)
	if(!((/[0-9]/.test(ch)) || (/[-]/.test(ch)) || (/[s]/.test(ch)) || (/[l]/.test(ch)) || (/[e]/.test(ch)))){
		evt.preventDefault()
	}
	let stringOk = false
	//if - must be alone or first and only one
	//if(/[-]/.test(ch) && (isSelected || isEmpty || prev_char=="")){
	if(/[-]/.test(ch) && !element.value.includes("-") && (isSelected || prev_char=="")){
		stringOk = true
	}
	//if s must be alone
	if((/[s]/.test(ch) && (isSelected || isEmpty))){
		//console.log("rad")
		stringOk = true
	}
	//if e must be alone
	if((/[e]/.test(ch) && (isSelected || isEmpty))){
		stringOk = true
	}
	//if l must be alone
	if((/[l]/.test(ch) && (isSelected || isEmpty))){
		stringOk = true
	}
	//if number
	if( /[0-9]/.test(ch) && ((isSelected || isEmpty) || !isNaN(element.value))){
		//control if < TET*N_reg-1
		let string = parseInt(APP_input_box_prev_str(element)+ch+APP_input_box_next_str(element))//!! defined fractioning
		if(isSelected || string<TET*N_reg){
			stringOk = true
		}
	}
	if(!stringOk){
		evt.preventDefault()
	}
}

function APP_inpTest_denied(evt,element){
	//not editable
	evt.preventDefault()
}

//Time line
function APP_inpTest_Ta_line(evt,element){
	APP_inpTest_denied(evt,element)
	//XXX not yet editable
}

//segment line
function APP_inpTest_Ts_line(evt,element){
	let ch = String.fromCharCode(evt.which)
	let isRange
	if(previous_value==""){
		isRange = false
	}else{
		isRange = RE_element_is_fraction_range(element)
	}
	if(isRange){
		if(!(/[0-9]/.test(ch))){
			evt.preventDefault()
		}
	}else {
		if(!((/[0-9]/.test(ch)) || (/[.]/.test(ch)))){
			evt.preventDefault()
		}
	}
	let isSelected = APP_input_box_is_selected(element)
	//must be only ONE "."
	if((/[.]/.test(ch) && (!isSelected && element.value.includes(".")))){
		evt.preventDefault()
	}
}

//modular line (compas)
function APP_inpTest_Tm_line(evt,element){
	let ch = String.fromCharCode(evt.which)
	let isRange
	if(previous_value==""){
		isRange = false
	}else{
		isRange = RE_element_is_fraction_range(element)
	}
	let isSelected = APP_input_box_is_selected(element)
	let isEmpty = !!(element.value=="")
	if(isRange){
		if(!((/[0-9]/.test(ch)) || (/[c]/.test(ch)) )){
			evt.preventDefault();
		}
	}else {
		if(!((/[0-9]/.test(ch)) || (/[.]/.test(ch)) || (/[c]/.test(ch)) )){
			evt.preventDefault();
		}
	}
	let stringOk = false
	//must be only ONE "."
	if(/[.]/.test(ch) && (isSelected || (!element.value.includes(".") && !element.value.includes("c")))){
		stringOk = true
	}
	//must be only ONE "c"
	if(/[c]/.test(ch) && (isSelected || !element.value.includes("c"))){
		stringOk = true
	}
	// first value of another number
	if( /[0-9]/.test(ch) && (	(isSelected || isEmpty) ||
								(
									!isNaN(element.value) &&
									!element.value.includes(".")
								)
							)
		){
		//control if < 16 (max compas number)
		let string = parseInt(element.value+ch)
		if((string<16) || isSelected){
			stringOk = true
		}
	}
	//number after a "."
	if( /[0-9]/.test(ch) && (element.value.slice(-1)==".") && !isSelected){
		stringOk = true
	}
	//number after a c
	if( /[0-9]/.test(ch) && element.value.includes("c")){
		stringOk = true
	}
	if(!stringOk){
		evt.preventDefault()
	}
}

//intervals
function APP_inpTest_iT_line(evt,element){
	let ch = String.fromCharCode(evt.which);
	if(!((/[0-9]/.test(ch)))){
		evt.preventDefault();
	}
}


function APP_inpTest_Cvalue(evt,element){
	let ch = String.fromCharCode(evt.which);
	if(!(/[0-9]/.test(ch))){
		evt.preventDefault()
	}
	if (evt.keyCode == 13) {
		//exit focus
		element.blur()
	}
}

function APP_inpTest_iTKb(evt, element){
	let char = String.fromCharCode(evt.which);
	let regex = /^([0-9]|[1-9][0-9])$/;
	if(!regex.test(char)){
		evt.preventDefault()
	}
}

function APP_input_box_is_selected(input){
	if (typeof input.selectionStart == "number") {
		return input.selectionStart == 0 && input.selectionEnd == input.value.length;
	}
}

function APP_input_box_prev_char(input){
	if (typeof input.selectionStart == "number") {
		//return input.selectionStart
		if(input.selectionStart==0)return ""
		return input.value[input.selectionStart-1]
	}
}

function APP_input_box_prev_str(input){
	if (typeof input.selectionStart == "number") {
		//return input.selectionStart
		if(input.selectionStart==0)return ""
		return input.value.slice(0,input.selectionStart)
	}
}

function APP_input_box_next_char(input){
	if (typeof input.selectionStart == "number") {
		//return input.selectionStart
		if(input.selectionStart==input.value.length)return ""
		return input.value[input.selectionStart]
	}
}

function APP_input_box_next_str(input){
	if (typeof input.selectionEnd == "number") {
		//return input.selectionStart
		if(input.selectionEnd==input.value.length)return ""
		return input.value.slice(input.selectionEnd,input.value.length)
	}
}

//Operations

function APP_inpTest_op_int(evt,element){
	let ch = String.fromCharCode(evt.which)
	if(!((/[0-9]/.test(ch)) || (/[-]/.test(ch)))){
		evt.preventDefault();
	}
	let isSelected = APP_input_box_is_selected(element)
	let isEmpty = !!(element.value=="")
	let prev_char = APP_input_box_prev_char(element)
	let stringOk = false
	//if - must be alone or first
	if(/[-]/.test(ch) && !element.value.includes("-") && (isSelected || prev_char=="")){
		stringOk = true
	}
	//number not before a minus sign
	if( /[0-9]/.test(ch)){
		let string = APP_input_box_prev_str(element)+ch+APP_input_box_next_str(element)
		if(!isNaN(string)){
			stringOk = true
		}
	}
	if (evt.keyCode == 13) {
		//exit focus
		element.blur()
	}
	if(!stringOk){
		evt.preventDefault()
	}
}

function APP_inpTest_op_intMod(evt,element){
	//verify inp box is an integer or a modular
	let ch = String.fromCharCode(evt.which);
	if(!(/[0-9]/.test(ch) || /[r]/.test(ch) || /[-]/.test(ch))){
		evt.preventDefault()
		return
	}
	let isSelected = APP_input_box_is_selected(element)
	let isEmpty = !!(element.value=="")
	let prev_char = APP_input_box_prev_char(element)
	let prev_str = APP_input_box_prev_str(element)
	let stringOk = false
	//if - must be alone or first
	if(/[-]/.test(ch) && !element.value.includes("-") && (isSelected || prev_char=="")){
		stringOk = true
	}
	//if r must be after a number of lesser than TET (positive or negative)
	if((/[r]/.test(ch) 	&& !(isSelected || isEmpty)
						&& !element.value.includes("r") && parseInt(prev_str)<TET && parseInt(prev_str)>-TET
						&& prev_char!="")
		){
		stringOk = true
	}
	//if number is
	// first or second value minor of max note number OR
	let max_note = TET*N_reg -1
	// numb
	if( /[0-9]/.test(ch) && (	(isSelected || isEmpty) ||
								!element.value.includes("r") ||
								element.value=="-"
							)
		){
		//control if < TET
		let string_value = parseInt(prev_str+ch+APP_input_box_next_str(element))
		if((string_value<max_note && string_value>-max_note) || isSelected){
			stringOk = true
		}
	}
	//number after a r
	if( /[0-9]/.test(ch) && (element.value.slice(-1)=="r") && ch<N_reg){
		stringOk = true
	}
	if (evt.keyCode == 13) {
		//exit focus
		element.blur()
	}
	if(!stringOk){
		evt.preventDefault()
	}
}

function APP_inpTest_op_intModPos(evt,element){
	//verify inp box is positive integer or positive modular
	let ch = String.fromCharCode(evt.which);
	if(!(/[0-9]/.test(ch) || /[r]/.test(ch))){
		evt.preventDefault()
		return
	}
	let isSelected = APP_input_box_is_selected(element)
	let isEmpty = !!(element.value=="")
	let prev_char = APP_input_box_prev_char(element)
	let prev_str = APP_input_box_prev_str(element)
	let stringOk = false
	//if r must be after a number of 1 or 2 char
	if((/[r]/.test(ch) 	&& !(isSelected || isEmpty)
						&& !element.value.includes("r") && parseInt(prev_str)<TET
						&& prev_char!="")
		){
		stringOk = true
	}
	//if number is
	let max_note = TET*N_reg -1
	// first or second value of another number OR
	if( /[0-9]/.test(ch) && (	(isSelected || isEmpty) ||
								!isNaN(prev_str)
							)
		){
		//control if < TET
		let string = parseInt(prev_str+ch)
		if(string<TET && string>-TET){
		//if((string<TET && string>-TET) || isSelected){
			stringOk = true
		}
	}
	// first or second value minor of max note number OR
	if( /[0-9]/.test(ch) && (	(isSelected || isEmpty) ||
								//Obsolete : later control of TET magnitude
								//(!isNaN(element.value) && element.value.length<2) ||
								!isNaN(element.value)
							)
		){
		//control if < TET
		let string_value = parseInt(element.value+ch)
		if((string_value<max_note) || isSelected){
			stringOk = true
		}
	}
	//number after a r
	if( /[0-9]/.test(ch) && (prev_char=="r" && element.value.slice(-1)=="r") && ch<N_reg){
		stringOk = true
	}
	if (evt.keyCode == 13) {
		//exit focus
		element.blur()
	}
	if(!stringOk){
		evt.preventDefault()
	}
}
