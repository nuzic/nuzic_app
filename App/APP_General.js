//block right click on pmc // mac user bug
document.body.addEventListener('contextmenu', (evt) => {
	if (evt.target.closest(".App_PMC_svg_container")!=null) {
		//console.log("no right click on PMC")
		evt.preventDefault();
	}
	if (evt.ctrlKey && evt.buttons == 1) {
		//console.log("ctrl-click detected");
		evt.preventDefault();
	}
})

window.onclick = function(event) {
	let parent = event.target.parentNode
	if(parent==null){
		APP_close_all_dropdown_segment_contents()
		APP_hide_scale_circle_elements()
	}else if (!event.target.matches('.App_RE_dropdown_segment_button') && !parent.matches('.App_RE_dropdown_segment_button')
			&& !event.target.matches('.App_RE_voice_menu_button_img')
			&& !event.target.matches('.App_RE_column_selector_segment_menu_button') && !parent.matches('.App_RE_column_selector_segment_menu_button')
			&& !parent.matches('.App_RE_S_show_visualization_options_button') && event.target.closest('.App_RE_C_show_visualization_options_box')==null
			&& event.target.closest('.App_RE_S_show_visualization_options_box')==null
			&& !parent.matches('.App_PMC_time_line_pulse_top') && !parent.matches('.App_RE_C_show_visualization_options_button')
			&& !event.target.matches('.App_PMC_voice_selector_voice_item_menuIcon') && !parent.matches('.App_PMC_voice_selector_voice_item_menuIcon')
			&& !event.target.matches('.App_PMC_dropdown_segment_toggle_selection') && !parent.matches('.App_PMC_dropdown_segment_toggle_selection')
			&& !event.target.matches('.App_RE_info_button') && !parent.matches('.App_RE_info_button')
			&& !event.target.matches('.App_PMC_info_button') && !parent.matches('.App_PMC_info_button')
			&& !event.target.matches('#App_RE_info') && !parent.matches('#App_RE_info')
			&& !event.target.matches('#App_PMC_info') && !parent.matches('#App_PMC_info')
			&& !event.target.matches('#App_BNav_V_midi_output') && !parent.matches('#App_BNav_V_midi_output')
			&& !event.target.matches('#App_BNav_keyboard_midi_input') && !parent.matches('#App_BNav_keyboard_midi_input')
			&& !event.target.matches('.App_BNav_voice_channel') && !parent.matches('.App_BNav_voice_channel')
			&& !event.target.matches('.App_Nav_button') && !parent.matches('.App_Nav_button')){
		APP_close_all_dropdown_segment_contents()
	}
	if(event.target.matches('.App_RE_dropdown_segment_button')){
		let dropdown = event.target.closest(".App_RE_dropdown_segment").querySelector(".App_RE_dropdown_segment_content")
		APP_close_all_dropdown_segment_contents(dropdown)
		RE_toggle_dropdown_segment(parent)
	} else if (event.target.matches('.App_RE_voice_menu_button_img')){
		let dropdown = event.target.closest(".App_RE_voice_header_bar").querySelector(".App_RE_voice_dropdown_menu")
		APP_close_all_dropdown_segment_contents(dropdown)
		RE_toggle_dropdown_voice(parent)
	} else if (event.target.matches('.App_RE_column_selector_segment_menu_button')){
		let dropdown = event.target.closest(".App_RE_column_selector_segment_menu").querySelector(".App_RE_dropdown_column_selector_content")
		APP_close_all_dropdown_segment_contents(dropdown)
		RE_toggle_dropdown_column_selector_segment(event.target)
	} else if (event.target.matches('.App_PMC_voice_selector_voice_item_menuIcon')){
		let dropdown = event.target.closest(".App_PMC_voice_selector_voice_item").querySelector(".App_PMC_dropdown_voiceMenu")
		APP_close_all_dropdown_segment_contents(dropdown)
		PMC_toggle_dropdown_voice(parent)
	} else if (event.target.matches('.App_PMC_voice_selector_visibility_dropdown_button_img')){
		APP_close_all_dropdown_segment_contents()
		PMC_toggle_line_selector_menu(event.target.parentNode)
	}else if (event.target.matches('.App_PMC_voice_selector_visibility_dropdown_button')){
		APP_close_all_dropdown_segment_contents()
		PMC_toggle_line_selector_menu(event.target)
	} else if (event.target.matches('.APP_PMC_voice_selector_color_picker')){
		APP_close_all_dropdown_segment_contents()
		PMC_color_picker_toggle(parent)
	} else if (event.target.matches('.App_Nav_button')){
		APP_close_all_dropdown_segment_contents()
		TNAV_show_main_menu()
	}
	if(parent!=null){
		if(parent.matches('.App_RE_dropdown_segment_button')){
			let dropdown = event.target.closest(".App_RE_dropdown_segment").querySelector(".App_RE_dropdown_segment_content")
			APP_close_all_dropdown_segment_contents(dropdown)
			RE_toggle_dropdown_segment(event.target)
		}else if(parent.matches('.App_PMC_voice_selector_voice_item_menuIcon')){
			//let dropdown = event.target.closest(".App_PMC_voice_selector_voice_item").querySelector(".App_PMC_dropdown_voiceMenu")
			let dropdown = parent.querySelector(".App_PMC_dropdown_voiceMenu")
			APP_close_all_dropdown_segment_contents(dropdown)
			PMC_toggle_dropdown_voice(event.target)
		}else if (event.target.matches('.App_PMC_voice_selector_visibility_dropdown_button_img')){
			APP_close_all_dropdown_segment_contents()
			PMC_toggle_line_selector_menu(event.target.parentNode)
		} else if (event.target.matches('.App_PMC_voice_selector_visibility_dropdown_button')){
			APP_close_all_dropdown_segment_contents()
			PMC_toggle_line_selector_menu(event.target)
		} else if(parent.matches('.APP_PMC_voice_selector_color_picker')){
			APP_close_all_dropdown_segment_contents()
			PMC_color_picker_toggle(event.target)
		}else if(parent.matches('.App_RE_S_show_visualization_options_button')){
			let RE_sound_visualization = document.querySelector('.App_RE_S_show_visualization_options_box')
			if(RE_sound_visualization.style.display=='none'){
				APP_close_all_dropdown_segment_contents()
				RE_open_S_lines_visualization()
			}else{
				APP_close_all_dropdown_segment_contents()
			}
		}else if(parent.matches('.App_RE_C_show_visualization_options_button')){
			let RE_time_visualization = document.querySelector('.App_RE_C_show_visualization_options_box')
			if(RE_time_visualization.style.display=='none'){
				APP_close_all_dropdown_segment_contents()
				RE_open_C_lines_visualization()
			}else{
				APP_close_all_dropdown_segment_contents()
			}
		} else if (parent.matches('.App_RE_column_selector_segment_menu_button')){
			let dropdown = event.target.closest(".App_RE_column_selector_segment_menu").querySelector(".App_RE_dropdown_column_selector_content")
			APP_close_all_dropdown_segment_contents(dropdown)
			RE_toggle_dropdown_column_selector_segment(parent)
		} else if (parent.matches('.App_Nav_button')){
			//no need , is not a toggle
			//APP_close_all_dropdown_segment_contents()
			//TNAV_show_main_menu()
		}
		let circle_div=event.target.closest(".App_BNav_S_circle_container")
		if(circle_div==null){
			APP_hide_scale_circle_elements()
		}
	}
}

function APP_close_all_dropdown_segment_contents(exception=null){
	let dropdowns = [...document.querySelectorAll(".App_RE_dropdown_segment_content")]
	let PMCdropDowns =[...document.querySelectorAll(".App_PMC_dropdown_voiceMenu")]
	let REdropDowns =[...document.querySelectorAll('.App_RE_voice_dropdown_menu')]
	let REdropDownsColumn =[...document.querySelectorAll('.App_RE_dropdown_column_selector_content')]
	let PMCsegmentdropDown =document.querySelector("#App_PMC_dropdown_segment_content")
	let colorPickerDropDowns =[...document.querySelectorAll(".App_PMC_dropdown_colorPicker")]
	let PMCvoiceSelectDropDowns = [...document.querySelectorAll('.App_PMC_voice_selector_button_visibility_dropdown')]
	//var RE_sound_visualization = document.querySelector('.App_RE_S_show_visualization_options_box')
	//var RE_time_visualization = document.querySelector('.App_RE_C_show_visualization_options_box')
	dropdowns.forEach(item=>{
		if(item==exception)return
		item.classList.remove('show');
	})
	PMCdropDowns.forEach(item=>{
		if(item==exception)return
		item.classList.remove('show');
	})
	REdropDowns.forEach(item=>{
		if(item==exception)return
		item.classList.remove('show');
	})
	
	REdropDownsColumn.forEach(item=>{
		if(item==exception)return
		item.classList.remove('show');
	})

	PMCvoiceSelectDropDowns.forEach(item=>{
		if(item==exception)return
		item.classList.remove('show');
	})
	colorPickerDropDowns.forEach(item=>{
		if(item==exception)return
		item.classList.remove('show');
	})
	RE_close_S_lines_visualization()
	RE_close_C_lines_visualization()
	PMCsegmentdropDown.style.display = "none"
	//keyboardDropdown.style.display='none'

	TNAV_hide_main_menu()
	APP_hide_info()

	BNAV_V_midi_close_menu()
}

function APP_populate_global_inputs_PMC(data){
	//outputRE_PPM.value= data.global_variables.PPM
	outputPMC_PPM.value= data.global_variables.PPM
	//outputRE_Li.value = data.global_variables.Li
	outputPMC_Li.value= data.global_variables.Li
	//outputRE_TET.value=data.global_variables.TET
	outputPMC_TET.value=data.global_variables.TET
	//outputRE_Rg.innerText= data.global_variables.TET*N_reg
	outputPMC_Rg.innerText = data.global_variables.TET*N_reg
}

function APP_new_composition(){
	if(typeof WebApp_show_welcome_menu!='function' || DEBUG_MODE){
		DATA_new()
	}else{
		WebApp_show_welcome_menu()
	}
}

//anonymous event handlers
let anonymous_events_list = [];

//used in dragover segment PMC
function APP_store_anonymous_event(fn, useCaptureMode, event) {
	let e = APP_find_stored_anonymous_event(event, useCaptureMode);
	if (!e) {
		anonymous_events_list.push({fn, useCaptureMode, event});
	}
}

function APP_find_stored_anonymous_event(event, useCaptureMode) {
	return anonymous_events_list.find(el => el.event === event && el.useCaptureMode === el.useCaptureMode);
}

function APP_delete_stored_anonymous_event(element,type){
	let e = APP_find_stored_anonymous_event(type, false);
	if (e) {
		element.removeEventListener(e.event, e.fn, e.useCaptureMode);
		anonymous_events_list.splice(anonymous_events_list.findIndex(el => el === e), 1);
	}else{
		console.error("function not found")
	}
}

//remove_scroll_on_spacebar
//function for chrome
window.onkeydown = function(e) {
	e = e || window.event  //normalize the evebnt for IE
	let target = e.srcElement || e.target  //Get the element that event was triggered on
	let tagName = target.tagName  //get the tag name of the element [could also just compare elements]
	return !(tagName==="BODY" && e.keyCode == 32)  //see if it was body and space
}

function APP_enter_new_value_Li(input){
	APP_stop()
	let new_Li = Math.round(input.value)
	if(new_Li==0){
		input.value=previous_value
		return
	}
	if(input.value==previous_value)return
	let min_Li = DATA_calculate_minimum_Li()
	let rest = new_Li%min_Li
	if (rest==0){
		//Li = new_Li
		DATA_force_Li(new_Li)
	}else {
		//reject value
		input.value=previous_value
		return
	}
}

function APP_enter_new_value_PPM(input){
	APP_stop()
	DATA_change_PPM(parseInt(input.value))
}

function APP_enter_new_value_TET(input){
	//verify value input is an ok option in order to create a equally tone spaced set of notes
	APP_stop()
	let new_TET = Math.round(input.value)
	if(new_TET==0){
		input.value=TET
		return
	}
	if(new_TET==TET){
		return
	}
	let max_TET = 53
	if (new_TET<=max_TET){
		TET = new_TET
	}else {
		//reject value
		TET = max_TET
	}
	alert("Warning: note values are not automatically updated. Be carefull!")
	let data = DATA_new_composition_data()
	data.global_variables.TET = TET
	DATA_insert_new_state_point(data)
	DATA_load_state_point_data(true)
	TNAV_reset_composition_name()
	current_composition_id=""
	if(typeof Reset_current_composition_id === "function"){
		Reset_current_composition_id()
	}
}

function APP_set_composition_name(name){
	if(name==null){
		name=generic_file_name
	}
	current_file_name = name
	let composition_name = document.getElementById('composition_name')
	composition_name.value = name
}

/*
function array_division(vector1,vector2){  //XXX
	if(vector1.length!=vector2.length) return
	var index=0
	var result=[]
	while (index<vector1.length){
		result[index]=vector1[index]/vector2[index]
		index++
	}
	return result
}

function array_moltiplication(vector1,vector2){  //XXX
	if(vector1.length!=vector2.length) return
	var index=0
	var result=[]
	while (index<vector1.length){
		result[index]=vector1[index]*vector2[index]
		index++
	}
	return result
}

function array_sum(vector1,vector2){  //XXX
	if(vector1.length!=vector2.length) return
	var index=0
	var result=[]
	while (index<vector1.length){
		result[index]=vector1[index]+vector2[index]
		index++
	}
	return result
}

function array_difference(vector1,vector2){  //XXX
	if(vector1.length!=vector2.length) return
	var index=0
	var result=[]
	while (index<vector1.length){
		result[index]=vector1[index]-vector2[index]
		index++
	}
	return result
}*/

function isOdd(num) { return num % 2;}

function APP_set_previous_value(element){
	previous_value= element.value
	//last_focused_input = element;
	//select all text
	element.select()
}

function APP_show_hide_minimap_button(button){
	if(button.value==1){
		APP_set_show_hide_minimap(false)
	} else {
		APP_set_show_hide_minimap(true)
	}
	SNAV_select_zoom(document.querySelector('.App_SNav_select_zoom'))
	DATA_save_preferences_file()
}

function APP_set_show_hide_minimap(show){
	let show_button = document.querySelector("#App_show_minimap")
	let show_button2 = document.querySelector("#App_show_minimap2")
	let show_buttonNav = document.getElementById('Nav_switch_minimap')
	if(show){
		if(show_button.value!=1){
			show_button.value=1
			show_button2.value=1
			show_buttonNav.value=1
			show_buttonNav.children[0].src='./Icons/mini_pmc_active.svg'
			show_button.classList.remove('rotated')
			show_button2.classList.remove('rotated')
			let minimap = document.querySelector('.App_MMap_scroll')
			minimap.style.display = 'flex'
			let upperBox = document.querySelector('.App_MMap')
			upperBox.style.height= '98px'
			let sideCanvas = document.getElementsByClassName('App_MMap_sound_line_canvas')
			sideCanvas[0].style.display='flex'
			sideCanvas[1].style.display='flex'
			APP_refresh_minimap()
		}
	}else{
		if(show_button.value!=0){
			show_button.value=0
			show_button2.value=0
			show_buttonNav.value=0
			show_buttonNav.children[0].src='./Icons/mini_pmc.svg'
			show_button.classList.add('rotated')
			show_button2.classList.add('rotated')
			let minimap = document.querySelector('.App_MMap_scroll')
			minimap.style.display = 'none'
			let upperBox = document.querySelector('.App_MMap')
			upperBox.style.height= '14px'
			let sideCanvas = document.getElementsByClassName('App_MMap_sound_line_canvas')
			sideCanvas[0].style.display='none'
			sideCanvas[1].style.display='none'
		}
	}
}

//ERROR/TEXTS/LANGUAGE MANAGEMENT

function APP_error_popup(text){
	let string = text+ "\n\nPlease consider reporting the BUG on Discord"
	window.alert(string)
}

function APP_warning_popup(text){
	let string = text+ "\n\n"
	window.alert(string)
}

window.onerror = function (message,url,line,whatisthis,error){
	if(!DEBUG_MODE)console.error("Message : "+message+"\nURL : "+url+"\nLine Number : "+line+"\nStack :"+error.stack)
}

function APP_add_language_array(array){
	array.forEach(value=>{
		//find if exist and eventually create a new obj
		let found = lang_localization_list.find(item=>{
			return item.lang==value
		})
		if (found==null){
			//add empty obj
			lang_localization_list.push({lang:value,hover:[],msg:[],text:[],ready:false,current:false})
		}else{
			//console.log(value +" found")
		}
	})
}

function APP_verify_language_availability(force=false){
	//check is lang_localization_list has all the columns
	if(!force)if(!spread1 || !spread2 || !spread3)return
	let language_selection = []
	let found = false
	lang_localization_list.forEach(language=>{
		if(language.msg.length!=0 && language.text.length!=0 && language.hover.length!=0){
			language.ready=true
			if(language.lang==current_language){
				found=true
				language.current=true
				language_selection.push({id:language.lang,active:true})
			}else{
				language_selection.push({id:language.lang,active:false})
			}
		}
	})
	//find current active language
	if(!found){
		//select base ES
		current_language="ES"
		//give some problem opening things like operation and hover labels
	}
	//actualize oprions
	let change_language_button = document.querySelector("#change_language_option button")
	let language_selection_string=JSON.stringify(language_selection)
	change_language_button.value=language_selection_string
	let old_string=change_language_button.innerHTML
	change_language_button.innerHTML=current_language
	//save current selection in cache
	DATA_save_preferences_file()
	if(old_string!=current_language){
		//need to reload language
		APP_change_texts_language()
	}
}

function APP_change_language(element){
	let language_selection= JSON.parse(element.value)
	let next_index=0
	language_selection.find((lang,index)=>{
		if(lang.active){
			next_index=index+1
			if(next_index>=language_selection.length)next_index=0
			return true
		}
	})
	language_selection.forEach((lang,index)=>{
		language_selection[index].active=false
	})
	language_selection[next_index].active=true
	element.innerHTML=language_selection[next_index].id
	element.value=JSON.stringify(language_selection)
	//change language on all the APP
	//global var
	current_language=language_selection[next_index].id
	lang_localization_list.forEach(language=>{
		if(language.lang==current_language){
			language.current=true
		}else{
			language.current=false
		}
	})
	APP_change_texts_language()
	//save current selection in cache
	DATA_save_preferences_file()
}

function APP_info_msg(err_id){
	let app_msg_list = lang_localization_list.find(lang=>{return lang.current}).msg
	let app_text_list = lang_localization_list.find(lang=>{return lang.current}).text
	let msg_string = _return_msg_id(err_id)
	let outputRE_info=document.getElementById("App_RE_info")
	let RE_info_container= outputRE_info.querySelector(".App_RE_info_container")
	let PMC_info_container= outputPMC_info.querySelector(".App_PMC_info_container")
	let infoRE=document.createElement("div")
	let infoPMC=document.createElement("div")
	infoRE.classList.add("App_RE_info_item")
	infoPMC.classList.add("App_PMC_info_item")
	//timestamp
	let time_string= new Date().toISOString()
	time_string=time_string.substring(0, time_string.length - 5);
	let info_string = err_id+"-"+time_string
	//css string based on type of message (error or warning)
	let [string_style,string_type] = _return_type_id(err_id)
	info_string=`<p style="${string_style}">${string_type} ${msg_string}</p>
					<p>${info_string}</p>`
	infoRE.innerHTML=info_string
	infoPMC.innerHTML=info_string
	RE_info_container.prepend(infoRE)
	PMC_info_container.prepend(infoPMC)
	//delete fifth child
	let length=PMC_info_container.childElementCount
	if(length>5){
		for (let i = 0; i < length-5; i++) {
			RE_info_container.removeChild(RE_info_container.lastChild)
			PMC_info_container.removeChild(PMC_info_container.lastChild)
			//element.remove();
		}
	}
	//add preview
	outputRE_info.classList.add("App_RE_info_preview")
	outputPMC_info.classList.add("App_PMC_info_preview")
	//APP_blink_error(outputRE_info) //there is no class!!
	function _return_msg_id(id){
		let found = app_msg_list.find(item=>{
			return item.id==id
		})
		if(found==null){
			//console.log(id)
			return id
		}
		return found.text
	}
	function _return_type_id(id){
		let found = app_msg_list.find(item=>{
			return item.id==id
		})
		if(found==null){
			//console.log(id)
			return ""
		}
		let string_type=""
		let string_style=""
		if(found.type=="error"){
			string_type=APP_language_return_text_id("error")
			string_style="color: var(--Nuzic_dark);"
		}
		if(found.type=="warn"){
			string_type=APP_language_return_text_id("warn")
			string_style="color: var(--Nuzic_dark);"
		}
		return [string_style,string_type]
	}
}

function APP_show_info(button){
	let outputRE_info = document.getElementById("App_RE_info")
	outputRE_info.classList.remove("App_RE_info_preview")
	outputPMC_info.classList.remove("App_PMC_info_preview")
	outputRE_info.classList.toggle("App_RE_info_show")
	outputPMC_info.classList.toggle("App_PMC_info_show")
}

function APP_hide_info(){
	let outputRE_info = document.getElementById("App_RE_info")
	if(outputRE_info==null)return
	outputRE_info.classList.remove("App_RE_info_show")
	outputPMC_info.classList.remove("App_PMC_info_show")
}

function APP_show_pulse_sound_selector(options){
	let background = document.getElementById("App_pulse_sound_selector")
	background.style.display = 'flex'
	let button_container=background.querySelector(".App_pulse_sound_selector_button_container")
	let button_list = [...button_container.querySelectorAll(".App_pulse_sound_selector_button")]
	if(button_list.length==0){
		//create list of buttons
		let string=""
		for (let i = 0; i < percussion_list.length; i++) {
			let button_value=i+22
			let button_text = (i==0)? "e&nbsp &nbsp &nbsp - " : "e"+button_value+" - "
			button_text+=percussion_list[i]
			string+=`<button class="App_pulse_sound_selector_button" onclick="APP_select_voice_pulse_sound(this)" value="${button_value}">
						<img src="./Icons/play_mini.svg">
						${button_text}
					</button>
					`
		}
		button_container.innerHTML=string
		button_list = [...button_container.querySelectorAll(".App_pulse_sound_selector_button")]
	}
	//select current button
	let current_voice_sound_string=options.e_sound+""
	button_list.forEach(button =>{
		if(button.value==current_voice_sound_string){
			button.classList.add("selected")
		}else{
			button.classList.remove("selected")
		}
	})
	//PS attention to difference BW voice_number/index(SHOWN) and voice_id(DATABASE)
	data_info = JSON.stringify(options)
	background.setAttribute("data_info",data_info)
}

function APP_hide_pulse_sound_selector(){
	//hide div
	APP_change_voice_pulse_sound()
}

function APP_change_voice_pulse_sound(){
	let background = document.getElementById("App_pulse_sound_selector")
	let button_container=background.querySelector(".App_pulse_sound_selector_button_container")
	let selected_button = button_container.querySelector(".App_pulse_sound_selector_button.selected")
	let options=JSON.parse(background.getAttribute("data_info",data_info))
	options.e_sound=parseInt(selected_button.value)
	DATA_set_pulse_sound(options)
	//APP_hide_pulse_sound_selector()
	background.style.display = 'none'
}

function APP_select_voice_pulse_sound(current_button){
	let button_container=current_button.closest(".App_pulse_sound_selector_button_container")
	let button_list = [...button_container.querySelectorAll(".App_pulse_sound_selector_button")]
	button_list.forEach(button =>{
		button.classList.remove("selected")
	})
	current_button.classList.add("selected")
	APP_play_selected_voice_pulse_sound(parseInt(current_button.value))
}

function APP_read_PMC_extra_info(){
	let button = document.querySelector("#App_PMC_show_extra")
	return button.checked
}

function APP_set_PMC_extra_info(value){
	let button = document.querySelector("#App_PMC_show_extra")
	if(value){
		button.checked=true
	}else{
		button.checked=false
	}
}

function APP_change_PMC_extra_info(value){
	//Save state preferences
	DATA_save_preferences_file()
	PMC_redraw_all_voices_svg()
	PMC_redraw_selected_voice_svg(true,DATA_get_selected_voice_data())
}

function APP_hide_scale_circle_elements(){
	BNAV_delete_scale_circle()
	PMC_hide_scale_circle_elements()
	RE_hide_scale_circle_elements()
}

function APP_midi_output_toggle_menu(button){
	DATA_update_midi_devices()
	let position = button.getBoundingClientRect()
	let menu_div = document.querySelector("#App_BNav_dropdown_midi_list")
	menu_div.innerHTML=""
	if(menu_div.style.display == "flex"){
		BNAV_V_midi_close_menu()
	}else{
		let midi_input_ID = document.getElementById("App_BNav_keyboard_midi_input").getAttribute("midi_ID")
		let midi_output_ID = document.getElementById("App_BNav_V_output").getAttribute("midi_ID")
		menu_div.style.display = "flex"
		inner_string='<a onclick="BNAV_select_midi_device_out(-1)">-----------</a>'
		midi_output_devices.forEach((output,index)=>{
			let class_string=(output.id==midi_input_ID)?'class="disabled"':(output.id==midi_output_ID)?'class="selected"':""
			inner_string+=`<a ${class_string} onclick="BNAV_select_midi_device_out('${output.id}')"><label style="display:none">INPUT</label>${output.name}</a>`
		})
		menu_div.innerHTML=inner_string
		//menu_div.setAttribute('value',segment_index)
	}
	//position label hover
	let posx = position.x+position.width
	let posy = position.y+position.height-menu_div.clientHeight//-3
	let navTNAV_maximizeBttn = document.getElementById('App_Nav_maximize')
	let header = document.getElementById("mainHeaderNav")
	if(header!=null && navTNAV_maximizeBttn.value == "0"){
		posy-=56
	}
	menu_div.style.left = `${posx}px`
	menu_div.style.top = `${posy}px`
}

function APP_midi_input_toggle_menu(button){
	DATA_update_midi_devices()
	let position = button.getBoundingClientRect()
	let menu_div = document.querySelector("#App_BNav_dropdown_midi_list")
	menu_div.innerHTML=""
	if(menu_div.style.display == "flex"){
		BNAV_V_midi_close_menu()
	}else{
		let midi_input_ID = document.getElementById("App_BNav_keyboard_midi_input").getAttribute("midi_ID")
		let midi_output_ID = document.getElementById("App_BNav_V_output").getAttribute("midi_ID")
		menu_div.style.display = "flex"
		inner_string='<a onclick="BNAV_select_midi_device_in(-1)">-----------</a>'
		midi_input_devices.forEach((input,index)=>{
			let class_string=(input.id==midi_output_ID)?'class="disabled"':(input.id==midi_input_ID)?'class="selected"':""
			inner_string+=`<a ${class_string} onclick="BNAV_select_midi_device_in('${input.id}')"><label style="display:none">OUTPUT</label>${input.name}</a>`
		})
		menu_div.innerHTML=inner_string
	}
	//position label hover
	let posx = position.x+position.width
	let posy = position.y+position.height-menu_div.clientHeight//-3
	let navTNAV_maximizeBttn = document.getElementById('App_Nav_maximize')
	let header = document.getElementById("mainHeaderNav")
	if(header!=null && navTNAV_maximizeBttn.value == "0"){
		posy-=56
	}
	menu_div.style.left = `${posx}px`
	menu_div.style.top = `${posy}px`
}

function APP_voice_midi_channel_toggle_menu(button,options){
	let position = button.getBoundingClientRect()
	let menu_div = document.querySelector("#App_BNav_dropdown_midi_list")
	menu_div.innerHTML=""
	if(menu_div.style.display == "flex"){
		BNAV_V_midi_close_menu()
	}else{
		menu_div.style.display = "flex"
		let string="<div>"
		for(let ch=0;ch<16;ch++){
			let disabled=(ch==options.channel)?"class='selected'":""
			disabled=(ch==15)?"class='disabled'":disabled
			let ch_user=ch+1
			string+=`<b ${disabled} onclick="DATA_voice_change_midi_channel(${options.voice_id},${ch})">${ch_user}</b>`
		}
		string+="</div>"
		menu_div.innerHTML=string
	}
	//position label hover
	let posx = position.x//+position.width
	//let posy = position.y+position.height-menu_div.clientHeight//-3
	let posy = position.y-menu_div.clientHeight//-3
	let navTNAV_maximizeBttn = document.getElementById('App_Nav_maximize')
	let header = document.getElementById("mainHeaderNav")
	if(header!=null && navTNAV_maximizeBttn.value == "0"){
		posy-=56
	}
	menu_div.style.left = `${posx}px`
	menu_div.style.top = `${posy}px`
}
