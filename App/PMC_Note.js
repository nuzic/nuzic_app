function PMC_redraw_sound_line(){
	//need to be called every time TET change
	//redraw sound line
	let line_container = document.querySelector(".App_PMC_sound_line")
	let note_container = line_container.querySelector(".App_PMC_sound_line_note_container")
	let note_list = [...line_container.querySelectorAll(".App_PMC_sound_line_note")]
	let note_number = TET*N_reg
	if(note_number!=note_list.length){
		//redraw
		note_container.innerHTML=""
		for (let i=0; i < note_number; i++) {
			//add a div
			let div = document.createElement('div')
			div.classList.add("App_PMC_sound_line_note")
			note_container.appendChild(div)
		}
		//add final spacer
		let spacer_div = document.createElement('div')
		spacer_div.classList.add("App_PMC_element_spacer_V")
		note_container.appendChild(spacer_div)
		note_container.style.height="calc(var(--PMC_y_block) * "+(note_number-0.5)+" + var(--RE_block_half))"
	}
	//write values
	PMC_write_sound_line_values()
	//write_canvas
	PMC_write_sound_line_canvas_note()
	PMC_redraw_Nabc()
}

function PMC_redraw_Nabc(){
	let parent = document.querySelector('.App_PMC_voice_selector_letter_guide')
	parent.innerHTML=''
	if(PMC_zoom_y==1.5 || PMC_zoom_y==1.2 || PMC_zoom_y==1 || PMC_zoom_y==0.75 || PMC_zoom_y==0.5){
		let letterHeight = nuzic_block*PMC_zoom_y
		if(PMC_read_vsb_Nabc() && TET==12){
			//PINTO
			for(let i=0;i<84;i++){
				let letterContainer = document.createElement('div')
				letterContainer.classList.add('App_PMC_voice_selector_letter_div')
				letterContainer.style.height=`${letterHeight}px`
				letterContainer.style.minHeight=`${letterHeight}px`
				parent.appendChild(letterContainer)
				let letter = document.createElement('p')
				letter.classList.add('App_PMC_voice_selector_letter_text')
				letter.innerHTML= ABC_note_list_diesis[11-(i%12)]
				letterContainer.appendChild(letter)
			}
		}else{
			return
		}
	}else{
		return
	}
}

let _PMC_sound_line_scale=null
function PMC_write_sound_line_values(X_pos=null){
	//read what tipe of values to write
	let vsb_N=PMC_read_vsb_N()
	let note_type = vsb_N.type
	//list objects
	let container = document.querySelector(".App_PMC_sound_line")
	let note_list = [...container.querySelectorAll(".App_PMC_sound_line_note")]
	let note_number = TET*N_reg
	if(note_number!=note_list.length){
		PMC_redraw_sound_line()
		return
	}
	note_list = note_list.reverse()
	//write values
	switch (note_type) {
	case "Na":
		let i = 0
		note_list.forEach(item=>{
			if(PMC_zoom_y<0.5){
				if(i%TET==0){
					item.innerHTML = i
				}else{
					item.innerHTML = ""
				}
			} else {
				item.innerHTML = i
			}
			i++
		})
	break;
	case "Nm":
		_write_sound_line_modular()
	break;
	case "Ngm":
		if(X_pos==null){
			_PMC_sound_line_scale=null
			_write_sound_line_modular()
		}else{
			//find scale
			//let scale_sequence=DATA_get_scale_sequence()
			//let idea_scale_list=DATA_get_idea_scale_list()
			let PMC_grades_data=DATA_calculate_grade_data(Li)
			//if(scale_sequence.length==0)return //modular no need to rewrite
			//simpler with pixels
			let pulse = ((X_pos-nuzic_block)/(PMC_zoom_x*nuzic_block))
			let current_grades_data= PMC_grades_data[DATA_get_Pa_eq_grade_data_index(pulse,PMC_grades_data)]
			//let scale_end = 0
			// let current_scale=scale_sequence.find(scale=>{
			// 	scale_end+=scale.duration
			// 	return scale_end>pulse
			// })
			//if(current_scale == undefined)current_scale=scale_sequence.slice(-1)[0]


			// with current scale
			//draw
			if(JSON.stringify(current_grades_data)!=JSON.stringify(_PMC_sound_line_scale)){
				_PMC_sound_line_scale=current_grades_data
				if(current_grades_data==null){
					//no scale
					PMC_write_sound_line_values()
					// canvas
					PMC_write_sound_line_canvas_note()
					return
				}
				//write scale
				note_list.forEach((item,index)=>{
					let [grade,delta,reg] = DATA_calculate_absolute_note_to_grade(index,true,current_grades_data)
					if(PMC_zoom_y<0.5){
						if(grade==0){
							item.innerHTML = `${grade}r${reg}`
						}else{
							item.innerHTML = ""
						}
					} else {
						if(grade==0 && delta==0){
							item.innerHTML = `${grade}r${reg}`
						} else{
							if(delta==0) {item.innerHTML = `${grade}`}else{item.innerHTML =''}
						}
					}
					index++
				})
				//canvas
				PMC_write_sound_line_canvas_note(current_grades_data.starting_note)
			}
		}
	break;
	}
	function _write_sound_line_modular(){
		let index = 0
		note_list.forEach(item=>{
			let reg = Math.floor(index/TET)
			let note = index%TET
			if(PMC_zoom_y<0.5){
				if(note==0){
					item.innerHTML = `${note}r${reg}`
				}else{
					item.innerHTML = ""
				}
			} else {
				if(note==0){
					item.innerHTML = `${note}r${reg}`
				} else{
					item.innerHTML = `${note}`
				}
			}
			index++
		})
	}
}

function PMC_write_sound_line_canvas_note(note_offset=0){
	//note offset for scale with N!=0
	let canvas = document.querySelector("#App_PMC_sound_line_canvas_note")
	let delta = nuzic_block * PMC_zoom_y
	let offset = delta/2
	let note_number = TET*N_reg
	canvas.style.height="calc(var(--PMC_y_block) * "+(note_number-0.5)+" + var(--RE_block_half))"
	PMC_mouse_action_sound_svg.style.height="calc(var(--PMC_y_block) * "+(note_number-0.5)+" + var(--RE_block_half))"
	canvas.width= nuzic_block *1.5
	canvas.height = nuzic_block/2 + delta * (note_number-0.5)
	let ctx = canvas.getContext('2d')
	//clear canvas
	ctx.clearRect(0, 0, canvas.width, canvas.height)
	//note tics
	for (let i=0; i<note_number;i++){
		//left
		ctx.beginPath()
		let y = Math.round(offset+i*delta)-0.5
		ctx.moveTo(0,y)
		ctx.lineTo(7,y)
		ctx.strokeStyle = nuzic_dark
		ctx.lineWidth = 1
		ctx.stroke()
		//right
		ctx.beginPath()
		ctx.moveTo(nuzic_block *1.5-7,y)
		ctx.lineTo(nuzic_block *1.5,y)
		ctx.strokeStyle = nuzic_dark
		ctx.lineWidth = 1
		ctx.stroke()
	}
	//reg
	for (let i=0; i<N_reg;i++){
		//left
		ctx.beginPath()
		let y = offset+delta*(TET-1)+(i)*delta*TET-note_offset*delta-0.5
		ctx.moveTo(0,y)
		ctx.lineTo(7,y)
		ctx.strokeStyle = nuzic_dark
		ctx.lineWidth = 3
		ctx.stroke()
		//right
		ctx.beginPath()
		ctx.moveTo(nuzic_block *1.5-7,y)
		ctx.lineTo(nuzic_block *1.5,y)
		ctx.strokeStyle = nuzic_dark
		ctx.lineWidth = 3
		ctx.stroke()
	}
}
