function RE_select_line_type_button(type){
	let active_lines = Object.keys(RE_global_variables).filter(id=>{
		if(RE_global_variables[id] && id!="Tf" && id!="scroll_x" && id!="scroll_y"){
			return true
		}
	}).length
	if(RE_global_variables[type]){
		//remove BUT leave at least 1
		if(type=="Tf"){
			RE_global_variables[type]=false
		}else{
			let line_needed = 2
			//2 lines min if shown A,else 1
			//if(DATA_get_current_state_point(false).global_variables.A.show)line_needed++;
			//bug if activate A and show only 1...
			if (active_lines > line_needed) {
				RE_global_variables[type]=false
			}else{
				return
			}
		}
	}else{
		//add
		RE_global_variables[type]=true
		//warnings
		// if(type=="Ngm" || type=="iNgm"){
			//verify if there is at least a scale
			// if(DATA_get_scale_sequence().length==0){
			// 	APP_info_msg("no_scale")
			// }
		// }
	}
	flag_DATA_updated.RE=false
	DATA_smart_redraw()
}
