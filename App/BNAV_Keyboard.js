function BNAV_draw_keyboard(){
	let [note,reg]=BNAV_read_keyboard_starting_note()
	let abs=BNAV_read_keyboard_abs()
	let chromatic = BNAV_read_keyboard_type()
	let iT_lock = BNAV_read_keyboard_iT_lock()
	let N_lock = BNAV_read_keyboard_N_lock()
	let use_letters = document.getElementById("compKeyBttn").value=="true"

	if(iT_lock && N_lock){
		iT_lock=false
		N_lock=false
		let iT_lock_button = document.querySelector('.App_BNav_keyboard_hex_key_iT_lock ')
		let N_lock_button = document.querySelector('.App_BNav_keyboard_hex_key_N_lock')
		iT_lock_button.classList.remove("locked")
		N_lock_button.classList.remove("locked")
		iT_lock_button.childNodes[0].src='./Icons/lock_open.svg'
		N_lock_button.childNodes[0].src='./Icons/lock_open.svg'
	}

	if(TET!=12 && !chromatic){
		chromatic=true
		let types_buttons=document.querySelectorAll(".App_BNav_keyboard_optionsSelectKeyboard_selectButtons")
		types_buttons[0].classList.remove("checked")
		types_buttons[1].classList.add("checked")
		let selected_type = document.querySelector("#App_BNav_keyboard_selector_label")
		selected_type.innerHTML=types_buttons[1].innerHTML
		selected_type.setAttribute("value",types_buttons[1].value)
	}

	//change iT buttons
	let iTHexes = [...document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')]
	if(iT_lock){
		iTHexes.forEach((hex,index)=>{
			hex.classList.add('locked')
		})
	}else{
		iTHexes.forEach((hex,index)=>{
			hex.classList.remove('locked')
		})
	}

	//App_BNav_keyboard_N_keys
	let container = document.querySelector(".App_BNav_keyboard_container_N_keys")
	//preparing texts
	let hex_black_order=[]
	let black_str_list=[]
	let white_str_list=[]
	let black_value_list=[]
	let white_value_list=[]

	if(chromatic){
		let half_TET=Math.ceil(TET/2)
		hex_black_order=Array.from({length: half_TET}, i => i = true);
		if(half_TET!=(TET/2))hex_black_order[hex_black_order.length-1]=false
	}else{
		//TET 12
		hex_black_order=[true,true,false,true,true,true,false]
	}

	let current_note=0
	for (let r = 0; r < N_reg; r++) {
		if(abs){
			hex_black_order.forEach(hex=>{
				white_str_list.push(current_note)
				white_value_list.push(current_note)
				current_note++
				if(hex){
					black_str_list.push(current_note)
					black_value_list.push(current_note)
					current_note++
				}else{
					black_str_list.push(null)
					black_value_list.push(null)
				}
			})
		}else{
			hex_black_order.forEach((hex,i)=>{
				if(i==0){
					white_str_list.push(0+"<sup>"+r+"</sup>")
					current_note=0
				}else{
					white_str_list.push(current_note)
				}
				white_value_list.push(current_note+r*TET)
				current_note++

				if(hex){
					black_str_list.push(current_note)
					black_value_list.push(current_note+r*TET)
					current_note++
				}else{
					black_str_list.push(null)
					black_value_list.push(null)
				}
			})
		}
	}

	//available letters
	let white_letters=['a','s','d','f','g','h','j','k','l']
	let black_letters=['q','w','e','r','t','y','u','i','o','p']
	let white_letters_list=[]
	let black_letters_list=[]
	_BNAV_keyboard_key_letters=[]

	let note_letter_position_start=note+reg*TET
	let start_letter_white=white_value_list.some(value=>{return value==note_letter_position_start})
	if(start_letter_white)black_letters.shift()
	if(use_letters){
		let letter_running=false
		white_value_list.forEach((value,index)=>{
			let string_w = ""
			if(value==null){
				if(letter_running)white_letters.shift()
			}else{
				if(value>=note_letter_position_start && value<note_letter_position_start+TET){
					letter_running=true
					let char=white_letters.shift()
					if(char!=null){
						string_w="<p>"+char+"</p>"
						_BNAV_keyboard_key_letters.push({char,value})
					}
				}else{
					letter_running=false
				}
			}
			white_letters_list.push(string_w)
			let string_b = ""
			if(black_value_list[index]==null){
				if(letter_running)black_letters.shift()
			}else{
				if(black_value_list[index]>=note_letter_position_start && black_value_list[index]<note_letter_position_start+TET){
					letter_running=true
					let char=black_letters.shift()
					if(char!=null){
						string_b="<p>"+char+"</p>"
						_BNAV_keyboard_key_letters.push({char,"value":black_value_list[index]})
					}
				}else{
					letter_running=false
				}
			}
			black_letters_list.push(string_b)
		})
	}else{
		black_value_list.forEach(value=>{
			white_letters_list.push("")
			black_letters_list.push("")
		})
	}

	//App_BNav_keyboard_N_keys_selection position
	let hex_width=48
	let selection_position
	if(start_letter_white){
		selection_position=white_value_list.indexOf(note_letter_position_start)*hex_width
	}else{
		selection_position=(black_value_list.indexOf(note_letter_position_start)+0.5)*hex_width
	}

	let selection_width=hex_black_order.length*hex_width
	if(!start_letter_white)selection_width+=hex_width/2
	//last key selection is black?
	let last_selected_is_black=false
	let last_selected_note=(note-1<0)?(TET-1):(note-1)
	let last_selected_prop=0
	hex_black_order.find(item=>{
		if(last_selected_prop==last_selected_note){
			last_selected_is_black=false
			return true
		}
		if(item==true){
			last_selected_prop++
			if(last_selected_prop==last_selected_note){
				last_selected_is_black=true
				return true
			}
		}
		last_selected_prop++
		return false
	})
	if(last_selected_is_black)selection_width+=hex_width/2
	let selection_color = "" //var(--Nuzic_light)
	if(use_letters)selection_color = "var(--Nuzic_pink)"

	let container_string=`<div class="App_BNav_keyboard_N_keys_selection" style="left: ${selection_position}px;width: ${selection_width}px;background-color: ${selection_color};"></div>
		<div class="App_BNav_keyboard_N_keys">`

	let function_string=""
	let key_locked_class =" locked"
	if(!N_lock){
		key_locked_class=""
		function_string=`onclick="BNAV_try_insert_note_duration(parseInt(this.value))"
		onmousedown="BNAV_play_note(this.value,true)" onmouseup="BNAV_play_note(this.value,false)"
		onmouseenter="BNAV_enter_kbTile(this.value,event)" onmouseleave="BNAV_exit_kbTile(this.value,event)"`
	}
	let hex_key_length=hex_black_order.length
	for (let r = 0; r < N_reg; r++) {
		container_string+=`<div class="App_BNav_keyboard_N_keys_reg"><div class="App_BNav_keyboard_N_keys_reg_black">`
		hex_black_order.forEach((hex,i)=>{
			if(hex){
				//let index = r*half_TET+r+i
				let index = r*hex_key_length+i
				container_string+=`<button class="App_BNav_keyboard_hex_key_black${key_locked_class}" ${function_string} value="${black_value_list[index]}">${black_str_list[index]+black_letters_list[index]}</button>`
			}else{
				container_string+=`<div class="App_BNav_keyboard_invisible_key"></div>`
			}
		})

		container_string+=`</div><div class="App_BNav_keyboard_N_keys_reg_white">`
		hex_black_order.forEach((hex,i)=>{
			//let index = r*half_TET+r+i
			let index = r*hex_key_length+i
			container_string+=`<button class="App_BNav_keyboard_hex_key_white${key_locked_class}" ${function_string} value="${white_value_list[index]}">${white_str_list[index]+white_letters_list[index]}</button>`
		})
		container_string+=`</div></div>`
	}
	container_string+=`</div>`
	container.innerHTML=container_string
	BNAV_keyboard_check_scroll_position()
}

//controls

function BNAV_keyboard_minimizeControls(button){
	let rightDiv = document.querySelector('.App_BNav_keyboard_head_R')
	let parent = rightDiv.parentNode

	if(button.value == 0){
		//midDiv.style.display='none'
		rightDiv.style.display='none'
		parent.style.width =`var(--RE_block)`
		button.classList.remove('App_BNav_tab_button_rotated')
		button.value =1
	} else{
		//midDiv.style.display='flex'
		rightDiv.style.display='flex'
		parent.style.width=`calc(var(--RE_block) * 12)`
		button.classList.add('App_BNav_tab_button_rotated')
		button.value =0
	}
}

function APP_switch_keyboard_as_MIDI_controller(){
	let button = document.getElementById("compKeyBttn")
	let use_keyboard_as_MIDI_controller=!(button.value=="true")
	let kbDiv = document.querySelector('.App_BNav_keyboard_N_keys_selection')
	//S E L . , > <
	let SEL_buttons = [...document.querySelectorAll(".App_BNav_keyboard_optionsHex,.App_Bnav_keyboard_options_IncDecOctave,.App_Bnav_keyboard_options_IncDecNote")]

	if(use_keyboard_as_MIDI_controller){
		_BNAV_keyboard_key_pressed = {}
		button.innerHTML=`<img data-value="KbToVKbHover" onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)" src="./Icons/kb_key_on.svg"/>`
		document.addEventListener("keydown", BNAV_on_computer_keyboard_MIDI_Success)
		document.addEventListener("keyup", BNAV_on_computer_keyboard_MIDI_Success)
		kbDiv.style.background = nuzic_pink
		SEL_buttons.forEach(button=>{button.querySelector("p").style="display:block"})
		APP_info_msg("keyboard_active")
	}else{
		_BNAV_keyboard_key_pressed = {}
		button.innerHTML=`<img data-value="KbToVKbHover" onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)" src="./Icons/kb_key.svg"/>`
		document.removeEventListener("keydown", BNAV_on_computer_keyboard_MIDI_Success)
		document.removeEventListener("keyup", BNAV_on_computer_keyboard_MIDI_Success)
		kbDiv.style.background = nuzic_light
		SEL_buttons.forEach(button=>{button.querySelector("p").style="display:none"})
	}
	button.value=use_keyboard_as_MIDI_controller
	BNAV_draw_keyboard()
}

function BNAV_read_keyboard_starting_note(){
	let split = document.getElementById("App_BNav_keyboard_starting_note").value.split('r')
	let note = Math.floor(split[0])
	let reg=Math.floor(split[1])
	return [note,reg]
}

function BNAV_set_keyboard_starting_note(note,reg){
	document.getElementById("App_BNav_keyboard_starting_note").value = `${note}r${reg}`
	BNAV_draw_keyboard()
	BNAV_keyboard_check_scroll_position()
}

function BNAV_keyboard_check_scroll_position(){
	//let [note,reg]=BNAV_read_keyboard_starting_note()
	let keyboard = document.querySelector(".App_BNav_keyboard_container_N_keys")
	let selector= document.querySelector(".App_BNav_keyboard_N_keys_selection")
	// let center_note=(reg+note/TET)/6
	// let center_note_pos_perc= (keyboard.scrollWidth*center_note-keyboard.clientWidth/2)/(keyboard.scrollWidth-keyboard.clientWidth)
	let center_note_pos=selector.offsetLeft+selector.clientWidth/2-keyboard.clientWidth/2
	let center_note_pos_perc= (center_note_pos)/(keyboard.scrollWidth-keyboard.clientWidth)
	//keyboard.scrollLeft=keyboard.scrollLeftMax*(reg+note/TET)/6-keyboard.clientWidth/2
	//keyboard.scrollLeft=keyboard.scrollLeftMax*center_note_pos_perc //ONLY FIREFOX
	keyboard.scrollLeft=(keyboard.scrollWidth - keyboard.clientWidth)*center_note_pos_perc //ONLY FIREFOX
}

function BNAV_keyboard_increase_reg(){
	let [note,reg]=BNAV_read_keyboard_starting_note()
	if(reg == N_reg-1 || reg == N_reg-2 && note!=0){
		return
	}else{
		reg++
		BNAV_set_keyboard_starting_note(note,reg)
	}
}

function BNAV_keyboard_decrease_reg(){
	let [note,reg]=BNAV_read_keyboard_starting_note()
	if(reg == 0){
		return
	}else{
		reg--
		BNAV_set_keyboard_starting_note(note,reg)
	}
}

function BNAV_keyboard_increase_note(bttn){
	let [note,reg]=BNAV_read_keyboard_starting_note()
	if(reg==N_reg-1){
		return
	}else if(note==TET-1){
		note=0
		reg++
	}else{
		note++
	}
	BNAV_set_keyboard_starting_note(note,reg)
}

function BNAV_keyboard_decrease_note(bttn){
	let [note,reg]=BNAV_read_keyboard_starting_note()
	if(reg==0 && note==0){
		return
	}else if(note==0){
		note=TET-1
		reg--
	}else{
		note--
	}
	BNAV_set_keyboard_starting_note(note,reg)
}

function BNAV_read_keyboard_abs(){
	return document.getElementById('App_BNav_keyboard_abs_mod').classList.contains("abs")
}

function BNAV_keyboard_select_abs_mod(button){
	let string=""
	if(button.classList.contains("mod")){
		button.classList.remove("mod")
		button.classList.add("abs")
		string="Na"
	}else{
		button.classList.remove("abs")
		button.classList.add("mod")
		string="Nm"
	}
	button.innerHTML=string
	BNAV_draw_keyboard()
}

function BNAV_read_keyboard_iT_lock(){
	return document.querySelector('.App_BNav_keyboard_hex_key_iT_lock ').classList.contains("locked")
}

function BNAV_read_keyboard_N_lock(){
	return document.querySelector('.App_BNav_keyboard_hex_key_N_lock').classList.contains("locked")
}

function BNAV_keyboard_lockiT(){
	let iT_lock = document.querySelector('.App_BNav_keyboard_hex_key_iT_lock ')
	let N_lock = document.querySelector('.App_BNav_keyboard_hex_key_N_lock')

	if(N_lock.classList.contains("locked")){
		N_lock.classList.remove("locked")
		N_lock.childNodes[0].src='./Icons/lock_open.svg'
	}

	if(iT_lock.classList.contains("locked")){
		iT_lock.classList.remove("locked")
		iT_lock.childNodes[0].src='./Icons/lock_open.svg'
	}else{
		iT_lock.classList.add("locked")
		iT_lock.childNodes[0].src='./Icons/lock_closed.svg'
	}

	BNAV_draw_keyboard()
}

function BNAV_show_keyboard_type_dropdown(show=true){
	let keyboardDropdown=document.querySelector('.App_BNav_keyboard_optionsSelectKeyboard_container')
	if(show){
		keyboardDropdown.style.display='flex'
	}else{
		keyboardDropdown.style.display='none'
	}
}

function BNAV_read_keyboard_type(){
	//true if chromatic, false if classical piano
	return document.querySelector('#App_BNav_keyboard_selector_label').getAttribute("value")=="chromatic"
}

function BNAV_select_keyboard_type(button){
	let types_buttons=document.querySelectorAll(".App_BNav_keyboard_optionsSelectKeyboard_selectButtons")
	if(button.value=="piano"){
		//select regular piano
		if(TET!=12)return
		types_buttons[1].classList.remove("checked")
	}else{
		types_buttons[0].classList.remove("checked")
	}
	button.classList.add("checked")
	let selected_type = document.querySelector("#App_BNav_keyboard_selector_label")
	selected_type.innerHTML=button.innerHTML
	selected_type.setAttribute("value",button.value)
	BNAV_show_keyboard_type_dropdown(false)
	event.stopPropagation()
	BNAV_draw_keyboard()
}

function BNAV_keyboard_lockN(){
	let iT_lock = document.querySelector('.App_BNav_keyboard_hex_key_iT_lock ')
	let N_lock = document.querySelector('.App_BNav_keyboard_hex_key_N_lock')
	if(iT_lock.classList.contains("locked")){
		iT_lock.classList.remove("locked")
		iT_lock.childNodes[0].src='./Icons/lock_open.svg'
	}
	if(N_lock.classList.contains("locked")){
		N_lock.classList.remove("locked")
		N_lock.childNodes[0].src='./Icons/lock_open.svg'
	}else{
		N_lock.classList.add("locked")
		N_lock.childNodes[0].src='./Icons/lock_closed.svg'
	}
	BNAV_draw_keyboard()
}

// iT functions

function BNAV_keyboard_selectiT(button){
	if (BNAV_read_keyboard_iT_lock())return
	let parent = button.parentNode
	parent.querySelector('.App_BNav_keyboard_hex_key_iT.checked').classList.remove('checked')
	button.classList.add('checked')
	if(BNAV_read_keyboard_N_lock()){
		BNAV_try_insert_note_duration(null)
	}
}

//note functions

function BNAV_play_note(nzc_note , down){
	let quantization = false
	let instrument = 0
	let windowTabPMC = document.getElementById('App_BNav_tab_PMC')
	if(windowTabPMC.checked){
		let  selectedData = DATA_get_selected_voice_data()
		instrument=selectedData.instrument
		//XXX
	}else{
		let focused_elem = document.querySelector('.animate_play_green_light')
		if(focused_elem!=null ){
			let voice = focused_elem.closest(".App_RE_voice")
			instrument = RE_which_voice_instrument(voice)
			//XXX
		}
	}
	let freq_note = 0
	if(instrument<128){
		freq_note = DATA_note_to_Hz(nzc_note,TET)
	}else{
		freq_note = DATA_note_to_Hz(nzc_note,12)
	}
	if(down){
		//play note
		App_set_instrument(0,instrument)
		SF_manager.synth.noteOn_freq(0,freq_note,V_velocity_default)
	}else{
		SF_manager.synth.noteOff_freq(0,freq_note)
	}
}

function BNAV_enter_kbTile(nzc_note,event){
	if(event.buttons==1){
		BNAV_play_note( nzc_note , true )
	}
}

function BNAV_exit_kbTile(nzc_note,event){
	if(event.buttons==1){
		BNAV_play_note( nzc_note , false)
	}
}

function BNAV_try_insert_note_duration(note=null){
	let duration_iT=parseInt(document.querySelector('.App_BNav_keyboard_hex_key_iT.checked').value)
	if(duration_iT==10){
		//read real value inside
		duration_iT=parseInt(document.querySelector('.App_BNav_keyboard_hex_key_iT.checked input').value)
	}
	if(BNAV_read_keyboard_iT_lock()){
		duration_iT=0
	}
	//N_lock note necessary, give note=-2
	// if(BNAV_read_keyboard_N_lock()){
	// 	BNAV_try_insert_note_duration(null)
	// }
	let N_lock = BNAV_read_keyboard_N_lock()
	//find voice_id pulse start and fraction start FIRE
	if(current_position_PMCRE.segment_index==null)return
	if (tabPMCbutton.checked){
		//PMC
		//verify current_position_PMCRE is the one showed  XXX XXX XXX
		if(current_position_PMCRE.voice_data.selected){
			if(N_lock==false){
				DATA_overwrite_new_object_PA_iT(current_position_PMCRE.voice_data.voice_id,current_position_PMCRE.segment_index,current_position_PMCRE.pulse,current_position_PMCRE.fraction,duration_iT,note,true,[],[])
			}else{
				if(duration_iT==0)return //not a delete!!!
				let timeArr = current_position_PMCRE.voice_data.data.segment_data[current_position_PMCRE.segment_index].time
				let timeIndex_next =  timeArr.findIndex(function(thePulse){
					return (thePulse.P>current_position_PMCRE.pulse || (thePulse.P==current_position_PMCRE.pulse && thePulse.F>current_position_PMCRE.fraction))
				})
				let timeIndex=timeIndex_next-1
				let segment_index=current_position_PMCRE.segment_index
				//ATTENTION TO END SEGMENT XXX
				if(timeIndex<0){
					//end segment??
					if(current_position_PMCRE.voice_data.data.segment_data.length<=segment_index+1){
						console.error("end voice")
						return
					}
					segment_index++
					timeIndex=0
					timeIndex_next=1
				}
				let current_time = current_position_PMCRE.voice_data.data.segment_data[segment_index].time[timeIndex]
				let current_time_next = current_position_PMCRE.voice_data.data.segment_data[segment_index].time[timeIndex_next]
				let current_fraction_range = current_position_PMCRE.voice_data.data.segment_data[segment_index].fraction.find((range,index)=>{
					if(range.stop>current_time.P){
						current_fraction_range_index=index
						return true
					}
					return false
				})
				let old_iT_value = DATA_calculate_interval_PA_PB(current_time,current_time_next,current_fraction_range)
				let pulse_end_target=current_time.P
				let fraction_end_target=current_time.F
				let N_NP=current_position_PMCRE.voice_data.neopulse.N
				let D_NP=current_position_PMCRE.voice_data.neopulse.D
				let Psg_segment_start = 0
				//overwrited by DATA_function
				for (let i = 0; i < segment_index; i++) {
					Psg_segment_start+=current_position_PMCRE.voice_data.data.segment_data[i].time.slice(-1)[0].P
				}
				//NEOPULSE correction
				//segment_start_P_abs=Psg_segment_start * N_NP / D_NP
				//console.log(current_fraction_range)
				// if(current_fraction_range==null){
					//end segment
				// 	current_fraction_range= {N:1,D:1}
				// }
				//if no change go to next element
				if(old_iT_value==duration_iT){
					//move
					pulse_end_target=current_time_next.P
					fraction_end_target=current_time_next.F
				}else{
					//change
					let [success,error_type,error_data] = DATA_modify_object_index_iT(current_position_PMCRE.voice_data.voice_id,segment_index,timeIndex,duration_iT)
					//various case of insuccess
					if(!success){
						if(error_type==1){
							//no space for interval
						}
						if(error_type==2){
							//fraction range incompatible with new iT
						}
						if(error_type==3){
							//voices neopulse doesn't permit change of the range
						}
						//stay on position
					}else{
						let new_time = DATA_calculate_next_position_PA_iT(current_time,duration_iT,current_fraction_range)
						pulse_end_target=new_time.P
						fraction_end_target=new_time.F
					}
				}
				//let pulse_abs_target=(Psg_segment_start)*N_NP/D_NP + (pulse_end_target)*N_NP/D_NP //MAYBE BETTER ERR TRUNC
				let pulse_abs_target=(Psg_segment_start + pulse_end_target)*N_NP/D_NP
				let time_target=DATA_calculate_exact_time(Psg_segment_start,pulse_end_target,fraction_end_target,current_fraction_range.N,current_fraction_range.D,N_NP,D_NP)
				//console.log("move at seg  "+segment_index+" P "+pulse_end_target+" F "+fraction_end_target+" Pabs "+pulse_abs_target+" time "+time_target)
				APP_player_to_time(time_target, pulse_abs_target,pulse_end_target,fraction_end_target,segment_index,current_position_PMCRE.voice_data.voice_id)
			}
		}
	}else{
		// RE
		if(current_position_PMCRE.pulse==null){
			var elements_list=[...document.querySelectorAll(".animate_play_green_light")]
			elements_list.forEach(item=>{APP_blink_error(item)})
			return
		}
		let focus_column = false
		let focusGreen = document.querySelector(".animate_play_green_light")
		if(focusGreen==null)return
		if(focusGreen.value==""){
			focus_column=true
		}
		if(N_lock==false){
			current_position_PMCRE.note=note
			let current_pulse=current_position_PMCRE.pulse
			let current_fraction=current_position_PMCRE.fraction
			//let
			let old_position=RE_input_to_position(focusGreen)
			DATA_overwrite_new_object_PA_iT(current_position_PMCRE.voice_data.voice_id,current_position_PMCRE.segment_index,current_position_PMCRE.pulse,current_position_PMCRE.fraction,duration_iT,note,true,[],[])
			//if last
			focusGreen2 = document.querySelector(".animate_play_green_light")
			if(focusGreen2==null){
				//end of voice mantain focus on that voice

				console.log(old_position)
				console.log(current_position_PMCRE)
				//let time_target=DATA_calculate_exact_time(Psg_segment_start,pulse_end_target,fraction_end_target,current_fraction_range.N,current_fraction_range.D,N_NP,D_NP)
			console.log("last")//
				let time_target=current_position_PMCRE.voice_data.data.midi_data.time.slice(-1)[0]
				//RE_highlight_columns_at_pulse(current_pulse,time_target,old_position.voice_id)//highlight but kb not work
				let aprox_pulse_abs=time_target/60*PPM
				APP_player_to_time(time_target, aprox_pulse_abs,current_pulse,current_fraction,old_position.segment_index,old_position.voice_id)



				//	index_scrollbar = column_index + past_segments_columns

			//move slider to position
			//RE_calculate_best_scrollbar_position(index_scrollbar)














			}
			//blink mod object bc focus was not directly on it (verify if needed XXX)
			if(focus_column){
				let elements_list=[...document.querySelectorAll(".animate_play_green_light")]
				elements_list.forEach(item=>{
					let input_list = [...item.parentNode.querySelectorAll("input")]
					let element_index = input_list.indexOf(item)
					let prev_filled_element_index = -1
					for (let i=element_index-1; i>=0;i--){
						let value = input_list[i].value
						if(value!=""){
							//element not void
							prev_filled_element_index=i
							i=-1
						}
					}
					let previous_item=input_list[prev_filled_element_index]
					APP_blink_good(previous_item)
				})
			}
		}else{
			if(duration_iT==0)return //not a delete!!!
			let timeArr = current_position_PMCRE.voice_data.data.segment_data[current_position_PMCRE.segment_index].time
			let timeIndex =  timeArr.findIndex(function(thePulse){
				return thePulse.P==current_position_PMCRE.pulse
			})
			let prevNote = current_position_PMCRE.voice_data.data.segment_data[current_position_PMCRE.segment_index].sound[timeIndex].note
			current_position_PMCRE.note=prevNote
			let RE_voiceData = [...document.getElementsByClassName('App_RE_voice')]
			let selectedVoice = RE_voiceData.filter(voice => voice.querySelector('.App_RE_voice_name').value == current_position_PMCRE.voice_data.name)[0]
			let iTinput = selectedVoice.querySelectorAll('.App_RE_iT')[current_position_PMCRE.segment_index].querySelector('.animate_play_green_light')
			if(iTinput==null)return
			//blink mod object bc focus was not directly on it
			if(focus_column){
				let elements_list=[...document.querySelectorAll(".animate_play_green_light")]
				elements_list.forEach(item=>{
					let input_list = [...item.parentNode.querySelectorAll("input")]
					let element_index = input_list.indexOf(item)
					let prev_filled_element_index = -1
					for (let i=element_index-1; i>=0;i--){
						let value = input_list[i].value
						if(value!=""){
							//element not void
							prev_filled_element_index=i
							i=-1
						}
					}
					let previous_item=input_list[prev_filled_element_index]
					APP_blink_good(previous_item)
					iTinput=previous_item
				})
			}
			if(duration_iT==iTinput.value){
				//move focus
				_focus_Tab_RE(iTinput)
			}else{
				previous_value=iTinput.value
				iTinput.value=duration_iT
				//not correct focus
				//return
				tab_pressed=true
				RE_enter_new_value_dT(iTinput,'iT')
			}
			function _focus_Tab_RE(element){
				let parent = element.parentNode
				let elements_list = [...parent.querySelectorAll(".App_RE_inp_box")]
				let element_index=elements_list.indexOf(element)
				let next_element= elements_list.find((item,index)=>{
					return item.value!="" && item.value!=end_range && index>element_index
				})
				if(next_element!=null){
					RE_focus_next_index(next_element,0)
				}else{
					let last_segment_element = elements_list.pop()
					if(last_segment_element==element){
						//jump next segment
						RE_focus_next_index(last_segment_element,1)
					}else{
						if(element.classList.contains("RE_note_cell")){
							//if sound line jump next segment
							RE_focus_next_index(last_segment_element,1)
						}else{
							//jump end line
							//for iT is next line
							RE_focus_next_index(last_segment_element,2)
						}
					}
				}
			}
		}
	}
}

//KEYBOARD AS MIDI KEYBOARD

// Computer keys avoid multiple press
let _BNAV_keyboard_key_pressed = {}
let _BNAV_keyboard_key_letters = []

function BNAV_on_computer_keyboard_MIDI_Success(event) {
	//when a keyboard is pressed act like a virtual MIDI keyboard
	//stop duplication
	if (_BNAV_keyboard_key_pressed[event.which] && event.type=="keydown")return
	if(event.type=="keydown"){
		// add key to store
		_BNAV_keyboard_key_pressed = { ..._BNAV_keyboard_key_pressed, [event.which]: true }
	}else{
		const { [event.which]: id, ...rest } = _BNAV_keyboard_key_pressed
		// remove key from store
		_BNAV_keyboard_key_pressed = rest
	}

	let nzc_note=null
	//verify fot iT and starting note
	let check_note=false
	switch (event.key) {
		case ".":
			if(event.type=="keydown")BNAV_keyboard_increase_reg()
		break;
		case ",":
			if(event.type=="keydown")BNAV_keyboard_decrease_reg()
		break;
		case ">":
			if(event.type=="keydown")BNAV_keyboard_increase_note()
		break;
		case "<":
			if(event.type=="keydown")BNAV_keyboard_decrease_note()
		break;
		case "z":
			nzc_note = -1
		break;
		case "x":
			nzc_note = -2
		break;
		case "c":
			nzc_note = -3
		break;
		default:
			check_note=true
		break;
	}
	if (/[0-9]/.test(event.key)){
		let iT_key_index=parseInt(event.key)-1
		if(iT_key_index==-1)iT_key_index=9
		//change iT
		if(event.type=='keydown')BNAV_keyboard_selectiT(document.getElementsByClassName('App_BNav_keyboard_hex_key_iT')[iT_key_index])
		check_note=false
	}
	if(check_note){
		//define at witch note correspond the letter
		//available letters
		let key_letter = _BNAV_keyboard_key_letters.find(item=>{return item.char==event.key})
		if(key_letter!=null){
			nzc_note = key_letter.value
		}
	}
	RE_disableEvent(event)
	if(nzc_note!=null){
		//add a note to the current voice segment
		let focused_elem = document.activeElement
		if(event.type=="keydown"){
			if(nzc_note>=0){
				BNAV_play_note(nzc_note, true, 2)
			}
			BNAV_virtual_keyboard_key_down(nzc_note)
		}else{
			if(nzc_note>=0){
				BNAV_play_note(nzc_note, false, 2)
			}
			BNAV_virtual_keyboard_key_up(nzc_note)
			BNAV_try_insert_note_duration(nzc_note)
		}
	}
}

function BNAV_virtual_keyboard_key_down(nzc_note){
	let Key_button = document.getElementById("App_BNav_tab_keyboard")
	if(Key_button.checked){
		//notes
		let key_list=[...document.querySelector(".App_BNav_keyboard_N_keys").querySelectorAll("button")]
		//S E L
		key_list.push(...document.querySelectorAll(".App_BNav_keyboard_optionsHex"))
		let pressed=key_list.find(key=>{return key.value==nzc_note})
		pressed.classList.add("active")
	}
}

function BNAV_virtual_keyboard_key_up(nzc_note){
	let Key_button = document.getElementById("App_BNav_tab_keyboard")
	if(Key_button.checked){
		let key_list=[...document.querySelector(".App_BNav_keyboard_N_keys").querySelectorAll("button")]
		//S E L
		key_list.push(...document.querySelectorAll(".App_BNav_keyboard_optionsHex"))
		let pressed=key_list.find(key=>{return key.value==nzc_note})
		pressed.classList.remove("active")
	}
}













