function APP_set_progressBar_eventListeners(){
	let progress_label = document.querySelector('#progress_label');
	let progress_label_inloop = document.querySelector('#inloop_label');
	let progress_label_endloop = document.querySelector('#endloop_label');
	let canvas = document.querySelector('#progress_bar_canvas')
	let upperBox = document.querySelector('.App_MMap')
	/*  Event Listeners */
	ProgressBar.addEventListener('input', (event) => {
		APP_stop()
		let P_abs = Number(ProgressBar.value)
		RE_highlight_columns_at_pulse(P_abs)
		if(tabREbutton.checked){
			RE_highlight_columns_at_pulse(P_abs)
		}else{
			//PMC_draw_progress_line_time(time)//later
		}
		APP_draw_cursor_minimap_canvas(P_abs)
		let uBox = parseInt(window.getComputedStyle(upperBox, null).getPropertyValue('width'));
		let w =  parseInt(window.getComputedStyle(canvas, null).getPropertyValue('width'));
		let progress_label_Left = (uBox-w)/2-10.5
		let pxls = w/ProgressEndLoop.max;
		let correction = 0
		progress_label.style.display = 'block'
		progress_label.innerHTML = ProgressBar.value
		progress_label.style.left = progress_label_Left + correction + ((ProgressBar.value * pxls)) + 'px';
		let time=DATA_calculate_exact_time(P_abs,0,0,1,1,1,1)
		if(!tabREbutton.checked){
			PMC_draw_progress_line_time(time)
		}
	})
	ProgressBar,addEventListener('mouseup',() => {
		progress_label.style.display = 'none'
	})
	ProgressBar.addEventListener('change', (event) => {
		if(parseInt(ProgressBar.value) < parseInt(ProgressInLoop.value)){
			ProgressInLoop.value = ProgressBar.value
			APP_draw_progress_canvas()
		}
		if(parseInt(ProgressBar.value) > parseInt(ProgressEndLoop.value)){
			ProgressEndLoop.value = ProgressBar.value
			ProgressBar.value = ProgressInLoop.value
			APP_draw_progress_canvas()
		}
	})
	ProgressInLoop.addEventListener('input', (event) => {
		APP_stop()
		let uBox = parseInt(window.getComputedStyle(upperBox, null).getPropertyValue('width'));
		let w =  parseInt(window.getComputedStyle(canvas, null).getPropertyValue('width'));
		let progress_label_Left = (uBox-w)/2-10.5
		let pxls = w /ProgressEndLoop.max;
		progress_label_inloop.style.display = 'block'
		progress_label_inloop.innerHTML = ProgressInLoop.value
		progress_label_inloop.style.left = progress_label_Left + ((ProgressInLoop.value * pxls)) + 'px';
	})
	ProgressInLoop,addEventListener('mouseup',() => {
		progress_label_inloop.style.display = 'none'
	})

	ProgressInLoop.addEventListener('change', (event) => {
		if(parseInt(ProgressInLoop.value) >= parseInt(ProgressEndLoop.value)){
			if(ProgressInLoop.value == Li){
				//ProgressInLoop.value = parseFloat(ProgressEndLoop.value)-1
				ProgressInLoop.value= Li-1
				ProgressEndLoop.value = Li
				// ProgressBar.value = Li-1
			} else {
				ProgressEndLoop.value = parseFloat(ProgressInLoop.value)+1
			}
		}
		if(parseInt(ProgressInLoop.value) > parseInt(ProgressBar.value )){
			ProgressBar.value =  ProgressInLoop.value
			APP_check_minimap_scroll()
			APP_draw_cursor_minimap_canvas(ProgressBar.value)
		}
		APP_draw_progress_canvas()
	})
	ProgressEndLoop.addEventListener('input', (event) => {
		APP_stop()
		let uBox = parseInt(window.getComputedStyle(upperBox, null).getPropertyValue('width'));
		let w =  parseInt(window.getComputedStyle(canvas, null).getPropertyValue('width'));
		let progress_label_Left = (uBox-w)/2-10.5
		var pxls = w /ProgressEndLoop.max;
		progress_label_endloop.style.display = 'block'
		progress_label_endloop.innerHTML = ProgressEndLoop.value
		progress_label_endloop.style.left = progress_label_Left + ((ProgressEndLoop.value * pxls)) + 'px';
	})
	ProgressEndLoop,addEventListener('mouseup',() => {
		progress_label_endloop.style.display = 'none'
	})
	ProgressEndLoop.addEventListener('change', (event) => {
		if(parseInt(ProgressEndLoop.value) <= parseInt(ProgressInLoop.value)){
			if(ProgressEndLoop.value == 0){
				ProgressInLoop.value = 0
				ProgressEndLoop.value = 1
			} else {
				ProgressInLoop.value = parseFloat(ProgressEndLoop.value)-1
			}
		}
		if(parseInt(ProgressEndLoop.value) < parseInt(ProgressBar.value )){
			ProgressBar.value = ProgressInLoop.value
			APP_check_minimap_scroll()
			APP_draw_cursor_minimap_canvas(ProgressBar.value)
		}
		APP_draw_progress_canvas()
	})
}

APP_set_progressBar_eventListeners()

function APP_return_progress_bar_values(){
	return [parseInt(ProgressBar.value),parseInt(ProgressInLoop.value),parseInt(ProgressEndLoop.value),parseInt(ProgressEndLoop.max)]
}

function APP_move_progress_bar(position1, position2=null, position3=null){
	//current position play
	if(position1!=null){
		ProgressBar.value = position1
		APP_check_minimap_scroll()
	}
	//move starting point
	if(position2!=null){
		//at position
		ProgressInLoop.value=position2
	}
	//move ending point
	if(position3!=null){
		if(position3=="max"){
			//at the end
			ProgressEndLoop.value = ProgressEndLoop.max
		}else{
			//at position
			ProgressEndLoop.value = position3
		}
	}
}

function APP_redraw_Progress_bar_limits(reset=false,endloop_at_Li=false){
	let max_progress=Li
	let last_compas = DATA_get_compas_sequence().pop()
	let max_compas=last_compas.compas_values[0]+last_compas.compas_values[1]*last_compas.compas_values[2]
	if(max_compas>Li)max_progress=max_compas
	let current_values=APP_return_progress_bar_values()
	ProgressBar.max = max_progress
	ProgressInLoop.max=max_progress
	ProgressEndLoop.max =max_progress
	//reset positions
	if(reset==true){
		APP_move_progress_bar(0,0,max_progress)
	}else{
		//try to not reset values
		let a=0
		let b=0
		let c=max_progress
		if(current_values[1]<max_progress) b=current_values[1]
		if(current_values[0]<max_progress){
			a=current_values[0]
		} else {
			a=b
		}
		if(current_values[2]<max_progress) c=current_values[2]
		if(endloop_at_Li){
			//endLoop was at the end and i want it there
		 	c=max_progress
		 }
		// APP_move_progress_bar(0,0,max_progress)
		APP_move_progress_bar(a,b,c)
	}
	APP_draw_progress_canvas()
}

function APP_draw_progress_canvas(){
	let box = document.querySelector('.App_MMap_progress_bar_canvas_container')
	let canvas = box.querySelector('canvas')
	canvas.setAttribute('width', window.getComputedStyle(canvas, null).getPropertyValue("width"))
	canvas.setAttribute('height', window.getComputedStyle(canvas, null).getPropertyValue("height"))
	let width = canvas.width
	let height = canvas.height
	let context = canvas.getContext('2d')
	let progress_number = 0
	let n = 1
	//get what is bigger (Li or compass)
	//see if metronome bigger of Li
	let timeSignature = ProgressEndLoop.max
	if (timeSignature > 5000){
		n= 1000
	}else if (timeSignature > 2000){
		n= 500
	}else if (timeSignature > 600){
		n= 100
	}else if (timeSignature > 200){
		n=50
	}else if (timeSignature > 160){
		n=20
	} else if (timeSignature > 81){
		n=10
	} else if (timeSignature > 20){
		n=5
	} else  if (timeSignature > 10){
		n = 2
	}
	let DtimeSignature = timeSignature/n
	let cuts = Math.round(width/DtimeSignature)
	context.clearRect(0, 0, canvas.width, canvas.height)
	context.beginPath()
	context.fillStyle = "#FFBB33"
	context.fillRect((ProgressInLoop.value/timeSignature)*width, 0, ((ProgressEndLoop.value-ProgressInLoop.value)/timeSignature)*width, height) //no round here (meaningless)
	// first line
	context.stroke()
	let initLoop= document.getElementById('init_loop')
	let endLoop= document.getElementById('end_loop')
	for (let i = cuts; i < width; i += cuts){
		//lines
		context.beginPath()
		context.lineWidth = 3
		context.moveTo(i, 0)
		context.lineTo(i, height)
		if(i/box.offsetWidth*endLoop.max> initLoop.value && i/box.offsetWidth*endLoop.max < endLoop.value){
			context.strokeStyle="white"
		} else {
			context.strokeStyle="#EBC150"
		}
		context.stroke()
		//text
		context.beginPath()
		context.font = "11px Krub"
		context.lineWidth = 1
		context.strokeStyle="black"
		progress_number ++
		context.strokeText(`${progress_number*n}`, i+10, height-4)
	}
}

window.addEventListener('resize', () => {
	APP_draw_progress_canvas()
})

function APP_sraw_sideCanvas_minimap(){
	let canvas = [...document.getElementsByClassName('App_MMap_sound_line_canvas')]
	canvas[0].setAttribute('width', window.getComputedStyle(canvas[0], null).getPropertyValue("width"))
	canvas[0].setAttribute('height', window.getComputedStyle(canvas[0], null).getPropertyValue("height"))
	canvas[1].setAttribute('width', window.getComputedStyle(canvas[1], null).getPropertyValue("width"))
	canvas[1].setAttribute('height', window.getComputedStyle(canvas[1], null).getPropertyValue("height"))
	let width = canvas[0].width
	let height = canvas[0].height
	canvas.forEach(function (element){
		let context = element.getContext('2d')
		for(i=1;i<=80;i++){
			let context = element.getContext('2d')
			context.beginPath()
			context.lineWidth = 1;
			context.strokeStyle= 'white';
			context.moveTo(3, 0+i*5);
			context.lineTo(11, 0+i*5);
			context.stroke()
		}
	})
}

APP_sraw_sideCanvas_minimap()
