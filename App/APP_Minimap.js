function APP_draw_minimap_layer(time_array,freq_array,minimap_id,voice_color){
	if(minimap_id == null){return}
	let canvas = document.querySelector(`#${minimap_id}`)
	if(canvas==null){
		return
	}
	//get what is bigger (Li or compass)
	//see if metronome bigger of Li
	let timeSignature = parseInt(ProgressEndLoop.max/PPM*60*18)
	let minimapContainer = document.querySelector('.App_MMap_minimap')
	minimapContainer.style.width=timeSignature
	canvas.height= canvas.offsetHeight
	canvas.width = timeSignature
	let ctx = canvas.getContext('2d')
	//Less TET specific
	let f_last_note = DATA_note_to_Hz((TET*N_reg-1),TET)
	let f_first_note = DATA_note_to_Hz(0,TET)
	let value_0 = Math.log(f_first_note)/Math.log(f_last_note)
	let percentual_freq_array = []
	freq_array.forEach(f_list=>{
		let value_list=[]
		f_list.forEach(f=>{
			value_list.push(_frequency_to_percentage(f))
		})
		percentual_freq_array.push(value_list)
	})
	let color=eval(voice_color)
	for(let i=0; i<percentual_freq_array.length;i++){
		percentual_freq_array[i].forEach(pf=>{
			if(isNaN(pf))return
			ctx.beginPath()
			ctx.moveTo(Math.ceil(time_array[i]*18),Math.round((1-pf)*canvas.height))
			//ctx.moveTo(Math.ceil(time_array[i]*18),Math.round((1-pf)*canvas.offsetHeight))
			ctx.lineTo(Math.ceil(time_array[i+1]*18),Math.round((1-pf)*canvas.height))
			//ctx.lineTo(Math.ceil(time_array[i+1]*18),Math.round((1-pf)*canvas.offsetHeight))
			//ctx.strokeStyle = nuzic_dark
			ctx.strokeStyle = color
			ctx.stroke()
		})
	}
	function _frequency_to_percentage(frequency){
		let value = Math.log(frequency)/Math.log(f_last_note)
		let linear = (value- value_0)/(1-value_0)
		return linear
	}
}

function APP_draw_cursor_minimap_canvas(time){
	let canvas = document.getElementById('App_MMap_cursor_canvas')
	let timeSignature = parseInt(ProgressEndLoop.max/PPM*60*18)
	canvas.height= canvas.offsetHeight
	canvas.width = timeSignature
	let position = time/PPM*60*18
	let ctx = canvas.getContext('2d')
	ctx.beginPath()
	ctx.moveTo(Math.round(position),canvas.offsetHeight)
	ctx.lineTo(Math.round(position),0)
	ctx.strokeStyle = nuzic_dark
	ctx.stroke()
}

function APP_draw_cursor_minimap_canvas_onPLay(time){
	let pulsation = time*PPM/60
	let canvas = document.getElementById('App_MMap_cursor_canvas')
	let timeSignature = parseInt(ProgressEndLoop.max/PPM*60*18)
	canvas.height= canvas.offsetHeight
	canvas.width = timeSignature
	let position = pulsation/PPM*60*18
	let ctx = canvas.getContext('2d')
	ctx.beginPath()
	ctx.moveTo(Math.round(position),canvas.offsetHeight)
	ctx.lineTo(Math.round(position),0)
	ctx.strokeStyle = nuzic_dark
	ctx.stroke()
}

function APP_clear_minimaps(){
	let canvasArr = [...document.querySelectorAll('.App_MMap_minimap_subCanvas')]
	canvasArr.forEach(el=>{
		let ctx= el.getContext('2d')
		ctx.clearRect(0, 0, canvas.width, canvas.height)
	})
}

function APP_refresh_minimap(){
	if(document.querySelector("#App_show_minimap").value != 1)return
	let voice_data=DATA_get_current_state_point(false).voice_data
	if(voice_data==null){
		console.error("DATA_get_current_state_point failed")
		return
	}
	let idea_length = Li*60/PPM
	voice_data.forEach((voice)=>{
		let time_array = voice.data.midi_data.time
		//add ending time //
		time_array.push(idea_length)
		let freq_array = voice.data.midi_data.note_freq
		let minimap_id = "MMvoice"+voice.voice_id
		APP_draw_cursor_minimap_canvas(ProgressBar.value)
		APP_draw_minimap_layer(time_array, freq_array, minimap_id,voice.color)
	})
}

function APP_create_minimap_canvas(voice_id){
	let container = document.querySelector('.App_MMap_minimap')
	let newMap = document.createElement('canvas')
	newMap.classList.add('App_MMap_minimap_subCanvas')
	let id = "MMvoice"+voice_id
	newMap.setAttribute('id',`${id}`)
	container.appendChild(newMap)
}

function APP_create_cursor_minimap_canvas(){
	let container = document.querySelector('.App_MMap_minimap')
	let newMap = document.createElement('canvas')
	newMap.setAttribute('id',`App_MMap_cursor_canvas`)
	container.appendChild(newMap)
}

function APP_clear_minimap(){
	let container = document.querySelector('.App_MMap_minimap')
	container.innerHTML = ''
}

function APP_load_minimap(){
	APP_clear_minimap()
	let voice_data=DATA_get_current_state_point(false).voice_data
	APP_create_cursor_minimap_canvas()
	voice_data.forEach(voice =>{
		APP_create_minimap_canvas(voice.voice_id)
	})
}

function APP_check_minimap_scroll(){
	let container = document.querySelector('.App_MMap_scroll')
	let progressBar = document.querySelector('#progress_bar')
	let minimap = document.querySelector('.App_MMap_minimap_subCanvas')
	let playValue = progressBar.value/ProgressEndLoop.max
	container.scrollLeft = minimap.width*playValue
}

