//general functions for RE
let debug_PMC=false
if(DEBUG_MODE && DEBUG_MODE_EXTRA)debug_PMC=true

function PMC_select_zoom_sound(selector){
	let r = document.querySelector(':root')
	let old_zoom_y=PMC_zoom_y
	if(selector.value!="0"){
		PMC_zoom_y = Number(selector.value)
	}else{
		//calculate fit value
		let container=document.querySelector(".App_PMC_sound_line")
		let height = container.clientHeight
		let available_height = height-nuzic_block//spacer
		PMC_zoom_y = available_height/((TET*N_reg-0.5)*nuzic_block)
	}
	//setting a variable value
	//try to mantain focus on current sector
	let scrollbar_V = document.querySelector('.App_PMC_main_scrollbar_V')
	let percent = DATA_calculate_vertical_srollbar_percentage(scrollbar_V)
	r.style.setProperty('--PMC_y_block', 'calc(var(--RE_block) * '+PMC_zoom_y+')')
	PMC_redraw()
	PMC_redraw_sound_line()
	PMC_write_sound_line_values()
	PMC_redraw_Nabc()
	//mantain correct focus
	PMC_checkScroll_V(percent,true)
}

function PMC_mouse_wheel_zoom_sound_line(sound_line,event){
	//SWITCH ZOOM IF POSSIBLE
	let selector=document.querySelector("#App_PMC_select_zoom_sound")
	let target_selected=selector.selectedIndex
	if(event.deltaY>0){
		//select,next,voice
		target_selected++
	}else{
		//select,prev,voice
		target_selected--
	}
	if(target_selected>=0 && target_selected<selector.length){
		//make new selection
		selector.selectedIndex=target_selected
		PMC_select_zoom_sound(selector)
	}
}

let PMC_height=0
//CONTROL IF RESIZED PMC AND Y ZOOM ALL
const resize_PMC = new ResizeObserver(function(entries) {
	// since we are observing only a single element, so we access the first element in entries array
	let rect = entries[0].contentRect
	// current width & height
	//let width = rect.width
	let current_height = rect.height
	if(current_height!=PMC_height){
		PMC_height = current_height
		//verify if zoom == all an if so resize it
		if(PMC_read_zoom_sound()==0){
			//console.log("need to redraw")
			PMC_set_zoom_sound(0)
			PMC_redraw()
			PMC_redraw_sound_line()
			PMC_write_sound_line_values()
		}
	}
})
// start observing for resize
resize_PMC.observe(document.querySelector(".App_PMC"))

function PMC_select_zoom_time(selector){
	let old_zoom_x=PMC_zoom_x
	PMC_zoom_x = Number(selector.value)
	let r = document.querySelector(':root')
	//setting a variable value
	r.style.setProperty('--PMC_x_block', 'calc(var(--RE_block) * '+PMC_zoom_x+')')
		//try to mantain focus on current sector
	let old_position=PMC_svg_container.scrollLeft+PMC_svg_container.clientWidth/2
	let new_position=old_position*PMC_zoom_x/old_zoom_x
	let new_scrollbar_position=new_position-PMC_svg_container.clientWidth/2
	PMC_redraw()
	//mantain correct focus
	PMC_checkScroll_H(new_scrollbar_position)
}

function PMC_mouse_wheel_zoom_time_line(time_line,event){
	//SWITCH ZOOM IF POSSIBLE
	let selector=document.querySelector("#App_PMC_select_zoom_time")
	let target_selected=selector.selectedIndex
	if(event.deltaY>0){
		//select,next,voice
		target_selected++
	}else{
		//select,prev,voice
		target_selected--
	}
	if(target_selected>=0 && target_selected<selector.length){
		//make new selection
		selector.selectedIndex=target_selected
		PMC_select_zoom_time(selector)
	}
}

function PMC_read_zoom_sound(){
	let selector= document.querySelector("#App_PMC_select_zoom_sound")
	return selector.value
}

function PMC_set_zoom_sound(value){
	let selector= document.querySelector("#App_PMC_select_zoom_sound")
	if(value==0){
		selector.value=0
		let container=document.querySelector(".App_PMC_sound_line")
		let height = container.clientHeight
		let available_height = height-nuzic_block//spacer
		PMC_zoom_y = available_height/((TET*N_reg-0.5)*nuzic_block)
	}else{
		selector.value=value
		PMC_zoom_y = value
	}
	//this doesn't trigger  PMC_select_zoom_sound
	let r = document.querySelector(':root')
	//setting a variable value
	r.style.setProperty('--PMC_y_block', 'calc(var(--RE_block) * '+PMC_zoom_y+')')
}

function PMC_set_zoom_time(value){
	let selector= document.querySelector("#App_PMC_select_zoom_time")
	selector.value=value
	PMC_zoom_x = value
	//this doesnt trigger  PMC_select_zoom_time
	let r = document.querySelector(':root')
	//setting a variable value
	r.style.setProperty('--PMC_x_block', 'calc(var(--RE_block) * '+PMC_zoom_x+')')
}

function PMC_redraw(){
	//PMC_redraw_sound_line()  //not here, only if zoom change
	let  t1, t2,
	startTimer = function() {
		t1 = new Date().getTime();
		return t1;
	},
	stopTimer = function() {
		t2 = new Date().getTime();
		return t2 - t1;
	}

	if(debug_PMC)startTimer()
	let voice_data = DATA_get_selected_voice_data()
	PMC_load_selected_voice_header(voice_data)
	//if voice_datachanged....
	PMC_redraw_time_line(voice_data)
	if(debug_PMC){
		console.log('---PMC time line and headers ' + stopTimer() + 'ms')
		startTimer()
	}
	let PMC_grades_data=DATA_calculate_grade_data()
	if(debug_PMC){
		console.log('---PMC grade calculations ' + stopTimer() + 'ms')
		startTimer()
	}
	//redraw svg
	PMC_redraw_all_voices_svg(null,PMC_grades_data)
	if(debug_PMC){
		console.log('---PMC all non selected voices ' + stopTimer() + 'ms')//XXX
		startTimer()
	}
	//redraw selected voice
	PMC_redraw_selected_voice(voice_data,Li,PMC_grades_data) //after painting all (visible) voices canvas bc it draw over that
	if(debug_PMC){
		console.log('---PMC selected voice ' + stopTimer() + 'ms')//XXX
		startTimer()
	}
	PMC_redraw_selected_voice_sequence(voice_data,Li,PMC_grades_data)
	if(debug_PMC){
		console.log('---PMC voice sequence ' + stopTimer() + 'ms')//XXX
		startTimer()
	}
	//PMC_reset_action_pmc()after
	//redraw copy RE on PMC
	let n_box = PMC_x_length(voice_data)
	PMC_set_H_scrollbar_step_N(n_box) // here?
	PMC_write_S_line_values()
	PMC_write_C_line_values()
	PMC_checkScroll_H()
	PMC_checkScroll_V()
	PMC_reset_playing_notes()
	PMC_reset_progress_line()
	PMC_reset_action_pmc()
	if(debug_PMC){
		console.log('---PMC rest ' + stopTimer() + 'ms')
		startTimer()
	}
}

function DATA_calculate_grade_data(current_Li){//do when change scale sequence not here!!!! XXX XXX XXX DATA_load XXX XXX XXX no , on change data pmc...
	let grade_data=[]
	let PMC_global_variables = DATA_read_PMC_global_variables()
	if(current_Li==null) current_Li=Li
	if((PMC_global_variables.vsb_N.show && PMC_global_variables.vsb_N.type=="Ngm") || (PMC_global_variables.vsb_iN.show && PMC_global_variables.vsb_iN.type=="iNgm")){//XXX iA
		let scale_data=DATA_get_scale_data()
		let Psg_start=0
		scale_data.scale_sequence.forEach(current_scale=>{
			let start=Psg_start
			let stop=start+current_scale.duration
			//calculate notes
			let max_nzc_note = N_reg*TET
			let notes=[]
			let iS_list = []
			let current_scale_data = scale_data.idea_scale_list.find(item=>{
				return item.acronym==current_scale.acronym
			})
			//a COPY, not a pointer
			iS_list = JSON.parse(JSON.stringify(current_scale_data.iS))
			for (let i = 0 ; i<current_scale.rotation; i++){
				iS_list.push(iS_list.shift())
			}
			for(let i =0; i<max_nzc_note; i++){
				let n_scale = i-current_scale.starting_note
				if(n_scale<0)n_scale=0-TET-n_scale
				let reg = Math.floor(n_scale/TET)
				let resto = Math.abs(n_scale)%TET
				let current_grade = 0
				let current_note = 0
				let current_iS = iS_list[0]
				iS_list.find(iS=>{
					if (current_note>=resto){
						return true
					}else{
						current_grade++
						current_note+=iS
						current_iS = iS
					}
				})
				let delta = 0
				if((current_note-resto)!=0){
					delta = current_iS-(current_note-resto)
				}
				//positive){
				//if is not a grade take ++ diesis
				let grade_pos=(delta!=0)?current_grade-1:current_grade
				// let delta_pos=delta
				// let reg_pos=reg
				let grade_neg=current_grade
				let delta_neg=delta
				let reg_neg=reg
				//if is not a grade take a bemolle
				if(delta!=0){
					delta_neg = delta-iS_list[current_grade-1]
					//avoid over shoot grade ( es grade 7- == 0-
					if(iS_list.length==current_grade) {
						grade_neg=0
						reg_neg++
					}
				}
				notes.push({Dpos:{grade:grade_pos,delta,reg},Dneg:{grade:grade_neg,delta:delta_neg,reg:reg_neg}})
			}
			let data_scale_sequence={start,stop,duration:current_scale.duration,
									module:iS_list.length,
									acronym:current_scale.acronym,
									starting_note:current_scale.starting_note,
									rotation:current_scale.rotation,
									notes}
			Psg_start=stop
			grade_data.push(data_scale_sequence)
		})
	}
	if(grade_data.length!=0){
		grade_data[grade_data.length-1].stop=max_Li
	}else{
		//Nm
		let max_nzc_note = N_reg*TET
		let notes=[]
		for(let r =0; r<N_reg; r++){
			for(let m = 0; m<TET; m++){
				notes.push({Dpos:{grade:m,delta:0,reg:r},Dneg:{grade:m,delta:0,reg:r}})
			}
		}
		let data_scale_sequence={start:0,stop:max_Li,duration:current_Li,module:TET,
									acronym:null,
									starting_note:0,
									rotation:0,
									notes}
		grade_data.push(data_scale_sequence)
	}
	return grade_data
}

function DATA_calculate_absolute_note_to_grade(nzc_note,diesis,current_grades_data){
	let grade_data=(diesis)?current_grades_data.notes[nzc_note].Dpos:current_grades_data.notes[nzc_note].Dneg
	return [grade_data.grade,grade_data.delta,grade_data.reg]
}

function DATA_get_Pa_eq_grade_data_index(Pa,grade_data){
	return grade_data.findIndex(data=>{return data.start<=Pa && data.stop>Pa})
}

function PMC_redraw_all_voices_svg(data=null,PMC_grades_data=null){
	//draw all visible voices
	let voice_data = DATA_get_PMC_visible_voice_data(data) //every voice minus selected one
	let note_number = TET*N_reg
	let height_string = 'calc('+TET+' * '+N_reg+' * var(--PMC_y_block) )'
	//generic width of IDEA
	let current_Li=Li
	if(data!=null){
		//current_Li=reduce
		current_Li=data.global_variables.Li
	}
	if(PMC_grades_data==null)PMC_grades_data=DATA_calculate_grade_data(current_Li)
	let width_string = "calc(var(--PMC_x_block) * "+current_Li+" + var(--RE_block))"
	let offset = nuzic_block
	let c_height = nuzic_block * PMC_zoom_y * TET * N_reg + nuzic_block/2
	let delta_y = nuzic_block * PMC_zoom_y
	let extra_info=APP_read_PMC_extra_info()
	let extra_info_N=false
	let extra_info_iT=false
	let PMC_global_variables = DATA_read_PMC_global_variables()
	if(extra_info){
		if(PMC_global_variables.vsb_iT.show){
			extra_info_iT=true
		}
		if(PMC_global_variables.vsb_N.show){
			extra_info_N=true
		}
	}
	// using SVG
	PMC_svg_all_voices.style.height=height_string
	PMC_svg_all_voices.style.width=width_string
	PMC_svg_all_voices.innerHTML= ""
	let svg_string=""
	voice_data.forEach(item=>{
		//voice widths
		let D_NP = item.neopulse.D
		let N_NP = item.neopulse.N
		let neopulse_moltiplicator =  N_NP/D_NP
		let delta = nuzic_block * PMC_zoom_x * neopulse_moltiplicator
		let voice_color_light = eval(item.color+"_light")
		let array_voice_data = PMC_segment_data_to_obj_data(item.data.segment_data,item.neopulse)
		let [string_N_list,string_note_A_pos_list,string_note_A_neg_list] = (extra_info_N)?PMC_calculate_object_N_text(array_voice_data,PMC_global_variables,item.neopulse,A_global_variables.show_note,PMC_grades_data):[[null],[null],[null]]
		//positioning and resizing main block
		let first_note = array_voice_data.find(object=>{
				return object.nzc_note>-1
		})
		//draw objects
		let starting_pos = item.e_sound
		if(first_note!=null){
			starting_pos = first_note.nzc_note
		}
		let last_nzc_note=starting_pos
		let last_nzc_note_A_pos=[]
		let last_nzc_note_A_neg=[]
		let last_pos=starting_pos
		array_voice_data.forEach((object,index)=>{
			let nzc_note = object.nzc_note
			let note_string=""
			if (nzc_note ==-1){
				//silence == nothing
				last_nzc_note=nzc_note
				last_nzc_note_A_pos=[]
				last_nzc_note_A_neg=[]
			}else if(nzc_note ==-2){
				//element box positioned in line last note
				let note = last_pos
				let x1 = offset+object.pulse_float*delta
				let x2 = x1 + object.duration *delta
				let y = delta_y*(note_number-note)-delta_y+1
				if(x2>2)x2-=1//little space
				svg_string+=`
					<rect x="${x1}" y="${y}" width="${x2-x1}" height="${delta_y-2}" fill="var(--Nuzic_white)" stroke="${voice_color_light}" stroke-width="2"></rect>
				`
				last_nzc_note=nzc_note
				last_nzc_note_A_pos=[]
				last_nzc_note_A_neg=[]
				svg_string+=_extra_iT(x1,x2,object,note,delta)
				if(extra_info_N)svg_string+=_extra_N(x1,y+delta_y/2,object,"e",delta,voice_color_light)
			}else if(nzc_note ==-4){
				//nothing
				last_nzc_note=nzc_note
				last_nzc_note_A_pos=[]
				last_nzc_note_A_neg=[]
			} else if(nzc_note ==-3){
				//ligadura, need to print something
				if(last_nzc_note!=-1 && last_nzc_note!=-4){
					let note = last_pos
					let x1 = offset+object.pulse_float*delta
					let x2 = x1 + object.duration *delta
					let y = delta_y*(note_number-note)-delta_y/2
					if(x2>2)x2-=1//little space
					x1-=1 //connect with previous
					if(last_nzc_note==-2){
						//modify last
						svg_string+=`
							<line x1="${x1-1}" y1="${y}" x2="${x2}" y2="${y}" stroke="${voice_color_light}" stroke-width="${delta_y}"></line>
							<line x1="${x1-1}" y1="${y}" x2="${x2}" y2="${y}" stroke="var(--Nuzic_white)" stroke-width="${delta_y-4}"></line>
							<line x1="${x2}" y1="${y-delta_y/2}" x2="${x2}" y2="${y+delta_y/2}" stroke="${voice_color_light}" stroke-width="2"></line>`
						if(extra_info_N)svg_string+=_extra_N(x1,y,object,"L",delta,voice_color_light)
					}else{
						svg_string+=`<line x1="${x1}" y1="${y}" x2="${x2}" y2="${y}" stroke="${voice_color_light}" stroke-width="${delta_y}"></line>`
						if(extra_info_N)svg_string+=_extra_N(x1,y,object,"L",delta,nuzic_white)

						if(A_global_variables.show_note){
							last_nzc_note_A_pos.forEach(item=>{
								y=delta_y*(note_number-item)-delta_y/2
								svg_string+=`<line x1="${x1}" y1="${y}" x2="${x2}" y2="${y}" stroke="${voice_color_light}" stroke-width="${delta_y}"></line>`
							})
							last_nzc_note_A_neg.forEach(item=>{
								y=delta_y*(note_number-item)-delta_y/2
								svg_string+=`<line x1="${x1}" y1="${y}" x2="${x2}" y2="${y}" stroke="${voice_color_light}" stroke-width="${delta_y}"></line>`
							})
						}
						if(A_global_variables.show){
							//A_global_variables.type not selected voices no value
						}
					}
					svg_string+=_extra_iT(x1,x2,object,note,delta)
				}
				//svg_string+=_extra_iT(x1,x2,object,note,delta)
			} else {
				let note = nzc_note
				let x1 = offset+object.pulse_float*delta
				let x2 = x1 + object.duration *delta
				let y = delta_y*(note_number-note)-delta_y/2
				if(x2>2)x2-=1//little space
				svg_string+=`<line x1="${x1}" y1="${y}" x2="${x2}" y2="${y}" stroke="${voice_color_light}" stroke-width="${delta_y}"></line>`
				if(extra_info_N)svg_string+=_extra_N(x1,y,object,string_N_list[index],delta)
				svg_string+=_extra_iT(x1,x2,object,note,delta)
				last_nzc_note=nzc_note
				last_pos=nzc_note
				if(A_global_variables.show_note){
					object.nzc_note_A_pos.forEach((item,j)=>{
						y=delta_y*(note_number-item)-delta_y/2
						svg_string+=`<line x1="${x1}" y1="${y}" x2="${x2}" y2="${y}" stroke="${voice_color_light}" stroke-width="${delta_y}"></line>`
						if(extra_info_N)svg_string+=_extra_N(x1,y,object,string_note_A_pos_list[index][j],delta)

					})
					object.nzc_note_A_neg.forEach((item,j)=>{
						y=delta_y*(note_number-item)-delta_y/2
						svg_string+=`<line x1="${x1}" y1="${y}" x2="${x2}" y2="${y}" stroke="${voice_color_light}" stroke-width="${delta_y}"></line>`
						if(extra_info_N)svg_string+=_extra_N(x1,y,object,string_note_A_neg_list[index][j],delta)
					})
					last_nzc_note_A_pos=object.nzc_note_A_pos
					last_nzc_note_A_neg=object.nzc_note_A_neg
				}
			}
		})
	})
	PMC_svg_all_voices.innerHTML=svg_string

	function _extra_N(x1,y,object,note,delta){
		let x_middle = x1 + object.duration *delta/2
		let note_string
			note_string=note
		return `<text x="${x_middle}" y="${y+1}"  >${note_string}</text>`
	}

	function _extra_iT(x1,x2,object,note,delta){
		if(extra_info_iT){
			let x_middle = x1 + object.duration *delta/2
			let x_middle_R = x_middle + nuzic_block/4
			let x_middle_L = x_middle - nuzic_block/4
			let y_low = delta_y*(note_number-note)
			let y_low_iT_middle = y_low + nuzic_block/4
			let y_low_iT = y_low_iT_middle+1
			return `<line x1="${x1}" y1="${y_low}" x2="${x2}" y2="${y_low}" stroke="${nuzic_yellow_light}" stroke-width="2px"></line>
			<line x1="${x_middle_L}" y1="${y_low_iT_middle}" x2="${x_middle_R}" y2="${y_low_iT_middle}" stroke="${nuzic_yellow_light}" stroke-width="${nuzic_block_half}"></line>
			<text x="${x_middle}" y="${y_low_iT}">${object.iT}</text>`
		}
		return ""
	}
}

function PMC_redraw_selected_voice(voice_data=null,current_Li=null,PMC_grades_data=null){
	PMC_reset_selection()
	//Selected voice
	if(voice_data==null) voice_data = DATA_get_selected_voice_data()
	if(current_Li==null) current_Li=Li
	if(PMC_grades_data==null)PMC_grades_data=DATA_calculate_grade_data(current_Li)
	let D_NP = voice_data.neopulse.D
	let N_NP = voice_data.neopulse.N
	let neopulse_moltiplicator =  N_NP/D_NP
	let total_length = PMC_x_length(voice_data,current_Li)
	let voice_color = eval(voice_data.color)
	let voice_color_light = eval(voice_data.color+"_light")
	let voice_instrument = voice_data.instrument
	let note_lines = TET * N_reg -0.5
	let max_nzc_note = N_reg*TET
	let height_string = 'calc('+note_lines+' * var(--PMC_y_block) + var(--RE_block))'
	//give extra  PMC_x_bloks
	let width_string = "calc(var(--PMC_x_block) * "+(total_length-0.5)+" + var(--RE_block))"
	let delta_x = nuzic_block * PMC_zoom_x * neopulse_moltiplicator
	let offset = nuzic_block
	let c_height = nuzic_block * PMC_zoom_y * note_lines + nuzic_block
	let c_width = nuzic_block + nuzic_block * PMC_zoom_x * total_length - 0.5 * nuzic_block * PMC_zoom_x
	//segment svg
	PMC_svg_segment.style.height=height_string
	PMC_svg_segment.style.width=width_string
	let segment_position = PMC_segment_data_to_segment_position(voice_data.data.segment_data)
	PMC_svg_segment.innerHTML=""
	let svg_string=""
	segment_position.forEach(item=>{
		let x = offset+item*delta_x//-0.5
		svg_string+=`<line x1="${x}" y1="0" x2="${x}" y2="${c_height}" stroke="${nuzic_dark}" stroke-width="2"></line>`
	})
	let fraction_position= PMC_segment_data_to_fraction_position(voice_data.data.segment_data)
	fraction_position.forEach(item=>{
		let x = offset+item*delta_x//-0.5
		svg_string+=`<line x1="${x}" y1="0" x2="${x}" y2="${c_height}" stroke="${nuzic_yellow}" stroke-dasharray="2,5" stroke-width="2"></line>`
	})
	PMC_svg_segment.innerHTML=svg_string
	//draw black points with tiling
	let delta_y = nuzic_block * PMC_zoom_y
	let PMC_global_variables = DATA_read_PMC_global_variables()
	let dots_N = PMC_x_dots(voice_data,current_Li)
	//tiling canvas with CSS
	PMC_dots_container.innerHTML=""
	PMC_dots_container.style.height=height_string
	PMC_dots_container.style.width="calc(var(--PMC_x_block) * "+(total_length-5)+")"
	let x = 0.5*delta_x
	let x1=x-0.5
	let x2=x+0.5
	let scale_end_Pabs=0
	let last_div_end_P=0
	//if no scale only one tiling, else multiple tiling
	// let scale_sequence=DATA_get_scale_sequence()
	// let idea_scale_list=DATA_get_idea_scale_list()
	if(PMC_global_variables.vsb_N.type=="Ngm"){
		//for (let i = 0; i < scale_sequence.length; i++) {
		for (let i = 0; i < PMC_grades_data.length; i++) {
			//create tiling div
			PMC_dots_container.innerHTML+='<div><svg></svg></div>'
			let PMC_div_dots=PMC_dots_container.querySelector("div:last-child")
			let PMC_svg_dots=PMC_div_dots.querySelector("svg")
			//PMC_svg_dots.style.height=height_string
			PMC_svg_dots.style.width=delta_x+"px"
			PMC_svg_dots.height= c_height
			PMC_svg_dots.width = delta_x
			let svg_dots_string=""
			//let current_scale=scale_sequence[i]
			for (let j = 0; j < max_nzc_note; j++) {
				let y = delta_y/2+(max_nzc_note-j-1)*delta_y-0.5
				let delta_grade=PMC_grades_data[i].notes[j].Dpos.delta
				if(delta_grade==0){
					svg_dots_string+=`<line x1="${x1}" y1="${y}" x2="${x2}" y2="${y}" stroke="black" stroke-width="1"></line>`
				}
			}
			PMC_svg_dots.innerHTML=svg_dots_string
			PMC_svg_dots.style.display="block"
			PMC_div_dots.style.backgroundSize=delta_x+"px "+c_height+"px"
			//scale_end_Pabs+=current_scale.duration
			scale_end_Pabs+=PMC_grades_data[i].duration
			if(i ==PMC_grades_data.length-1 && scale_end_Pabs<current_Li)scale_end_Pabs=current_Li
			let pulse_width=Math.ceil(scale_end_Pabs/neopulse_moltiplicator-last_div_end_P)
			let width= pulse_width*delta_x
			PMC_div_dots.style.width=width+"px"
			PMC_div_dots.style.backgroundImage='url('+SVGtoDataURL(PMC_svg_dots)+')'
			PMC_svg_dots.style.display="none"
			last_div_end_P+=pulse_width
		}
	}
	//one basic tiling with the remaining width if no scale
	if(scale_end_Pabs<current_Li){
		PMC_dots_container.innerHTML+='<div><svg></svg></div>'
		let PMC_div_dots_basic=PMC_dots_container.querySelector("div:last-child")
		let PMC_svg_dots_basic=PMC_div_dots_basic.querySelector("svg")
		PMC_svg_dots_basic.style.height=height_string
		PMC_svg_dots_basic.style.width=delta_x+"px"
		PMC_svg_dots_basic.height= c_height
		PMC_svg_dots_basic.width = delta_x
		let svg_dots_string=""
		//att! draw inverse order!
		for (let j = 0; j < max_nzc_note; j++) {
			var y = delta_y/2+j*delta_y-0.5
			svg_dots_string+=`<line x1="${x1}" y1="${y}" x2="${x2}" y2="${y}" stroke="black" stroke-width="1"></line>`
		}
		PMC_svg_dots_basic.innerHTML=svg_dots_string
		PMC_svg_dots_basic.style.display="block"
		//manca namespace  xmlns="http://www.w3.org/2000/svg" ??? non funziona
		//var dataUrl = 'data:image/svg+xml,'+encodeURIComponent(PMC_svg_dots.outerHTML);
		//console.log(dataUrl) //doesn t work
		let pulse_width=Math.floor(current_Li/neopulse_moltiplicator-last_div_end_P)
		let width= pulse_width*delta_x
		PMC_div_dots_basic.style.width=width+"px"
		PMC_div_dots_basic.style.backgroundImage='url('+SVGtoDataURL(PMC_svg_dots_basic)+')'
		PMC_div_dots_basic.style.backgroundSize=delta_x+"px "+c_height+"px"
		//PMC_div_dots.style.backgroundPositionX= (delta_x*0.5)+"px"
		PMC_svg_dots_basic.style.display="none"
	}
	//allign first div
	PMC_dots_container.querySelector("div:first-child").style.marginLeft=(0-delta_x* 0.5)+"px"
	function SVGtoDataURL(_svg){
		let type = "image/svg+xml";
		let svg_xml
		// quick-n-serialize an SVG dom, needed for IE9 where there's no XMLSerializer nor SVG.xml
		if (window.XMLSerializer) {
			svg_xml = (new XMLSerializer()).serializeToString(_svg);
		} else {
			svg_xml = _XMLSerializerForIE(_svg);
		}
		let svg_dataurl = "data:image/svg+xml;base64,";
		if (window.btoa) {
			svg_dataurl += btoa(svg_xml);
		} else {
			//using custom base64 encoder
			svg_dataurl += Base64.encode(svg_xml);
		}
		return svg_dataurl;
		function _XMLSerializerForIE(s) {
			let out = "";
			out += "<" + s.nodeName;
			for (let n = 0; n < s.attributes.length; n++) {
				out += " " + s.attributes[n].name + "=" + "'" + s.attributes[n].value + "'";
			}
			if (s.hasChildNodes()) {
				out += ">\n";
				for (let n = 0; n < s.childNodes.length; n++) {
					out += _XMLSerializerForIE(s.childNodes[n]);
				}
				out += "</" + s.nodeName + ">" + "\n";
			} else out += " />\n";
			return out;
		}
	}
	//Draw selected voice objects
	let div_selected_voice = PMC_svg_container.querySelector("#App_PMC_selected_voice")
	let selected_voice_height_string = 'calc('+TET+' * '+N_reg+' * var(--PMC_y_block) )'
	div_selected_voice.style.height=selected_voice_height_string
	div_selected_voice.style.width=width_string
	//div_selected_voice.innerHTML=""
	let array_voice_data = PMC_segment_data_to_obj_data(voice_data.data.segment_data,voice_data.neopulse)
	//generate list subobjects

	//write pulse number
	let P_obj_string_list=[]
	let Pend_obj_string_list=[]
	if(PMC_global_variables.vsb_T.show){
		//Add green points show number
		//pulse
		let pulse_type = PMC_global_variables.vsb_T.type
		let time_text_list=[]
		let last_pulse=0
		let last_pulse_PMC_visible=false
		switch (pulse_type) {
			case "Ta":
				//array_voice_data.forEach(data=>{time_text_list.push(data.Ta_text)})
				last_pulse=0
				last_pulse_PMC_visible=false
				array_voice_data.forEach(data=>{
					let text=""
					//calc pulse absolutre if possible
					if(N_NP==1 && D_NP==1){
						if(data.Ta_data.P==0 && data.Ta_data.F==0){
							text="0"
						}else{
							if(data.Ta_data.F==0){
								last_pulse=data.Ta_data.P
								last_pulse_PMC_visible=(data.nzc_note!=-1 && data.nzc_note!=-3)
								text=data.Ta_data.P+""
							}else{
								if(data.Ta_data.P==last_pulse && last_pulse_PMC_visible){
									text="."+data.Ta_data.F
								}else{
									last_pulse=data.Ta_data.P
									last_pulse_PMC_visible=(data.nzc_note!=-1 && data.nzc_note!=-3)
									text=data.Ta_data.P+"."+data.Ta_data.F
								}
							}
						}
					}else{
						text=no_value
					}
					time_text_list.push(text)
				})
			break;
			case "Tm":
				let neopulse = N_NP!=1 || D_NP!=1
				if(neopulse){
					array_voice_data.forEach(data=>{time_text_list.push(no_value)})
				}else{
					let compas_data = DATA_get_compas_sequence()
					let last_compas= compas_data.slice(-1)[0].compas_values
					let compas_length_value = last_compas[0]+last_compas[1]*last_compas[2]
					let prev_comp = null
					let new_compas_shown = false
					let prev_pulse = null
					//it int_p inside a compas
					//take into account that pulse is part of a segment un x position
					//No problem using Pa, no compas if neopulse!!! Psg===Pa
					array_voice_data.forEach((data,index)=>{
						if(data.Ta_data.P <compas_length_value && compas_length_value!=0){
							//element inside a module
							let compas_type = compas_data.filter(compas=>{
								return compas.compas_values[0] <= data.Ta_data.P
							})
							let current_compas_type = compas_type.pop()
							let current_compas_number = compas_type.reduce((prop,item)=>{
								return item.compas_values[2]+prop
							},0)
							let dP = data.Ta_data.P - current_compas_type.compas_values[0]
							let mod = current_compas_type.compas_values[1]
							let dC = Math.floor(dP/mod)
							let resto = dP
							if(dC!=0) resto = dP%mod
							current_compas_number+= dC
							//  <p>This text contains <sup>superscript</sup> text.</p>
							//warning first element
							//verify if it has module, if not read previous value
							let showcompas = false
							let showpulse = true
							//here retrive value
							if(prev_comp!=null){
								if(prev_pulse==resto)showpulse=false
								if(prev_comp==current_compas_number){
									if(!new_compas_shown){
										showcompas=true
										new_compas_shown=true
									}
								}else{
									new_compas_shown=false
									if(data.nzc_note>=0){
										showcompas=true
										new_compas_shown=true
									}
								}
							}else{
								if(data.nzc_note>=0){
									showcompas=true
									new_compas_shown=true
								}
							}
							let string=""
							if(showpulse || showcompas){
								string = resto
							}
							if(data.Ta_data.F!=0){
								string += "."+data.Ta_data.F
							}
							if(showcompas){
								//stringT += "<sup>"+current_compas_number+"</sup>"
								string += RE_string_to_superscript(current_compas_number.toString())
							}
							prev_comp = current_compas_number
							//if s , e , L doesnt register prev compas number???
							prev_pulse = resto
							time_text_list.push(string)
						}else{
							//outside or no compasses
							time_text_list.push(no_value)
						}
					})
				}
			break;
			case "Ts":
				//array_voice_data.forEach(data=>{time_text_list.push(data.Ts_text)})
				last_pulse=0
				last_pulse_PMC_visible=false
				array_voice_data.forEach(data=>{
					let text=""
					if(data.Ts_data.P==0 && data.Ts_data.F==0){
						text="0"
					}else{
						if(data.Ts_data.F==0){
							last_pulse=data.Ts_data.P
							last_pulse_PMC_visible=(data.nzc_note!=-1 && data.nzc_note!=-3)
							text=data.Ts_data.P+""
						}else{
							if(data.Ts_data.P==last_pulse && last_pulse_PMC_visible){
								text="."+data.Ts_data.F
							}else{
								last_pulse=data.Ts_data.P
								last_pulse_PMC_visible=(data.nzc_note!=-1 && data.nzc_note!=-3)
								text=data.Ts_data.P+"."+data.Ts_data.F
							}
						}
					}
					time_text_list.push(text)
				})
			break;
		}
		array_voice_data.forEach((object,index)=>{
			let nzc_note = object.nzc_note
			//if (nzc_note ==-1 || nzc_note ==-2 || nzc_note==-3){
			let div_class=""
			let value=""
			if (nzc_note ==-1 || nzc_note==-3){
				//previous position
				div_class="hidden"
			}else{
				//div_1.innerHTML= array_voice_data[index].Time_text
				//Hiding roule
				value= time_text_list[index]
				if(time_text_list[index].length>3)div_class="long_text"
			}
			P_obj_string_list.push(`<div class="App_PMC_selected_voice_object_dot_start ${div_class}">${value}</div>`)
			Pend_obj_string_list.push('<div class="App_PMC_selected_voice_object_dot_end"></div>')
		})
	}else{
		//add little green points
		array_voice_data.forEach(object=>{
			P_obj_string_list.push('<div class="App_PMC_selected_voice_object_dot_start_unselected"></div>')
			Pend_obj_string_list.push('<div class="App_PMC_selected_voice_object_dot_end_unselected"></div>')
		})
	}

	//note number
	let last_note = -1
	let prev_reg = 99
	let N_obj_string_list=[]
	let string_N_list=[]
	let string_note_A_pos_list=[]
	let string_note_A_neg_list=[]
	if(PMC_global_variables.vsb_N.show){
		[string_N_list,string_note_A_pos_list,string_note_A_neg_list] = PMC_calculate_object_N_text(array_voice_data,PMC_global_variables,voice_data.neopulse,true,PMC_grades_data)
		array_voice_data.forEach((object,index)=>{
			let nzc_note = object.nzc_note
			let color=""
			let value=""
			let function_string=""
			if (nzc_note ==-1){
				//no color
				last_note=nzc_note
			}else if(nzc_note ==-2){
				color="color:"+voice_color
				value="e"
				last_note=nzc_note
			}else if(nzc_note ==-3){
				//follow last note
				color="color:"+voice_color
				value="L"
			} else {
				//note
				let note_type = PMC_global_variables.vsb_N.type
				function_string = `onmousedown="PMC_play_this_note_number(${nzc_note},${voice_instrument},${voice_data.voice_id})"`
				value=string_N_list[index]
				last_note=nzc_note
			}
			N_obj_string_list.push(`<div class="App_PMC_selected_voice_object_N" style="${color}" ${function_string}>${value}</div>`)
		})
	}else{
		array_voice_data.forEach(object=>{
			let nzc_note = object.nzc_note
			if (nzc_note ==-1){
				//no color
				last_note=nzc_note
			}else if(nzc_note ==-2){
				last_note=nzc_note
			}else if(object.nzc_note ==-3){
			} else {
				//note
				let note = nzc_note
				last_note=nzc_note
			}
			N_obj_string_list.push('<div class="App_PMC_selected_voice_object_N"></div>')
		})
	}

	//add iT object and write iT number
	let iT_obj_string_list=[]
	if(PMC_global_variables.vsb_iT.show){
		array_voice_data.forEach((object,index)=>{
			let nzc_note = object.nzc_note
			let div_class=""
			let borderBottom=""
			if (nzc_note ==-1){
				div_class='hidden'//already contain border TOP
			} else {
				//draw border in objects
				borderBottom = "border-top: 2px solid var(--Nuzic_yellow)"
			}
			iT_obj_string_list.push(`<div class="App_PMC_selected_voice_object_iT ${div_class}" style="${borderBottom}">${object.iT}</div>`)
		})
	}else{
		array_voice_data.forEach(object=>{
			//create for alignment of iS no need anymore
			//iT_obj_string_list.push('<div class="App_PMC_selected_voice_object_iT"></div>')
			iT_obj_string_list.push('')
		})
	}

	//write iN number
	let iN_obj_string_list=[]
	let iN_obj_size=0
	if(PMC_global_variables.vsb_iN.show){
		//type traduction
		let prev_note = null
		let voice_started = false
		let value_array = []
		let iN_type = PMC_global_variables.vsb_iN.type
		//if(scale_sequence.length==0 && iN_type=="iNgm")iN_type="iNm"
		switch(iN_type){
			case 'iNa':
				array_voice_data.forEach(item=>{
					let note = item.nzc_note
					if (note ==-1 || note ==-2){
						value_array.push({"value":0,"string":""})
					}else if(note ==-3){
						value_array.push({"value":0,"string":""})
					} else {
						//note
						if(voice_started){
							let value = note-prev_note
							value_array.push({"value":value,"string":value})
						}else {
							voice_started = true
							value_array.push({"value":0,"string":""})
						}
						prev_note=note
					}
				})
			break;
			case 'iNm':
				array_voice_data.forEach(item=>{
					let note = item.nzc_note
					if (note ==-1 || note ==-2){
						value_array.push({"value":0,"string":""})
					}else if(note ==-3){
						value_array.push({"value":0,"string":""})
					} else {
						//note
						if(voice_started){
							let value = note-prev_note
							let reg = Math.floor(value/TET)
							let resto = value%TET
							let neg = false
							if(value<0){
								neg = true
								//console.log(int_n+"  resto "+resto)
								resto = Math.abs(resto)
								if(resto!=0){
									reg= Math.abs(reg)-1
								}else{
									reg= Math.abs(reg)
								}
							}
							let string_iN=""
							if(neg){
								string_iN="-"+resto//+"r"+reg
							}else{
								string_iN=resto//+"r"+reg
							}
							if(reg!=0)string_iN=string_iN+"r"+reg
							value_array.push({"value":value,"string":string_iN})
						}else {
							voice_started = true
							value_array.push({"value":0,"string":""})
						}
						prev_note=note
					}
				})
			break;
			case 'iNgm':
				let prev_diesis=true
				//let prev_scale=null
				let prev_scale_index=null
				//let idea_scale_list=DATA_get_idea_scale_list()
				array_voice_data.forEach((item,index)=>{
					let note = item.nzc_note
					let current_diesis = item.diesis
					let current_P_seg_global = item.Ta_data.P
					let current_F = item.Ta_data.F
					//find correct scale
					//find Pa_equivalent and verify if inside a scale
					// var current_fraction = segment_data.fraction.find(fraction=>{return fraction.stop>current_P})
					// var frac_p = current_fraction.N/current_fraction.D
					let frac_p = item.frac_moltiplicator
					let Pa_equivalent = current_P_seg_global * N_NP/D_NP + current_F* frac_p * N_NP/D_NP
					let current_scale_index=DATA_get_Pa_eq_grade_data_index(Pa_equivalent,PMC_grades_data)
					//element inside a scale module
					if(note<0){
						//no note
						value_array.push({"value":0,"string":""})
					}else{
						if(voice_started){
							let write_iN = true
							let value = note-prev_note
							if(prev_scale_index!=null){
								//strict
								//if(JSON.stringify(prev_scale) != JSON.stringify(current_scale)){
								if(prev_scale_index!= current_scale_index){
									// different scales
									write_iN=false
								}
							}else{
								write_iN=false
							}
							if(write_iN){
								//calculate note interval and make a traduction in current scale //PREV OR CURRENT SCALE???
								//not really a string
								let string_iN= DATA_calculate_grade_interval_string(prev_note,prev_diesis,PMC_grades_data[current_scale_index],note,current_diesis,PMC_grades_data[current_scale_index])
								value_array.push({"value":value,"string":string_iN})
							}else{
								value_array.push({"value":value,"string":no_value})
							}
						}else {
							voice_started = true
							value_array.push({"value":0,"string":""})
						}
						prev_scale_index=current_scale_index
						prev_note=note
						prev_diesis=current_diesis
					}
				})
			break;
		}

		array_voice_data.forEach((item,index)=>{
			let note = item.nzc_note
			let div_class=""
			let inner_value=""
			// let lineheight=""
			let height=""
			let top=""
			if (note ==-1){
				div_class="dotted"
			}else if(note ==-2 ){
			}else if(note ==-3){
			}else{
				//note
				let value = value_array[index].value
				if(value!=0){
					inner_value = value_array[index].string
					iN_obj_size=value
					// lineheight="line-height : calc(var(--PMC_y_block) * "+Math.abs(value)+");"
					height="height : calc(var(--PMC_y_block) * "+Math.abs(value)+");"
					if(value<0){
						//repositioning
						top ="top: calc(var(--PMC_y_block) * "+(0.5 + value)+");"
					}
				}
			}
			//iN_obj_string_list.push(`<div class="App_PMC_selected_voice_object_iN ${div_class}" style="${lineheight}${height}${top}">${inner_value}</div>`)
			iN_obj_string_list.push(`<div class="App_PMC_selected_voice_object_iN_line" style="${height}${top}"><div class="App_PMC_selected_voice_object_iN ${div_class}">${inner_value}</div></div>`)
		})
	}else{
		array_voice_data.forEach(object=>{
			iN_obj_string_list.push('')
		})
	}
	let A_note_obj_string_list=[]
	let A_obj_string_list=[]
	if(!PMC_global_variables.vsb_N.show){
		//no text values
		// string_note_A_pos_list=Array(array_voice_data.length).fill(" ")
		// string_note_A_neg_list=Array(array_voice_data.length).fill(" ")
		string_note_A_pos_list=[]
		string_note_A_neg_list=[]
		array_voice_data.forEach((object,j)=>{
			string_note_A_pos_list.push(Array(object.A_pos.length).fill(""))
			string_note_A_neg_list.push(Array(object.A_neg.length).fill(""))
		})
	}
	if(A_global_variables.show_note){
		let prev_obj=[]
		array_voice_data.forEach((object,j)=>{
			let A_note_string=""
			if(object.nzc_note==-3){
				//L
				let position=0

				prev_obj.A_neg.forEach((item,index)=>{
					let inner_value = (PMC_global_variables.vsb_N.show)?"L":"";
					position+=item
					let top ="top: calc(var(--PMC_y_block) * "+position+");"
					A_note_string+=`<div class="App_PMC_selected_voice_object_A_note" style="${top}" value="${object.nzc_note_A_neg[index]}">${inner_value}</div>`
				})
				position=0
				prev_obj.A_pos.forEach((item,index)=>{
					let inner_value = (PMC_global_variables.vsb_N.show)?"L":""
					position-=item
					let top ="top: calc(var(--PMC_y_block) * "+position+");"
					A_note_string+=`<div class="App_PMC_selected_voice_object_A_note" style="${top}" value="${object.nzc_note_A_pos[index]}">${inner_value}</div>`
				})
				A_note_obj_string_list.push(A_note_string)
				A_obj_string_list.push("")
				return
			}
			if(object.nzc_note==-1 || object.nzc_note==-2){
				prev_obj=object
			}
			if(object.nzc_note<0){
				A_note_obj_string_list.push("")
				A_obj_string_list.push("")
				return
			}
			prev_obj=object
			let position=0
			object.A_neg.forEach((item,index)=>{
				//inner value is note
				let inner_value=string_note_A_neg_list[j][index]
				position+=item
				let top ="top: calc(var(--PMC_y_block) * "+position+");"
				//let top=""
				A_note_string+=`<div class="App_PMC_selected_voice_object_A_note" style="${top}" value="${object.nzc_note_A_neg[index]}">${inner_value}</div>`
			})
			position=0
			object.A_pos.forEach((item,index)=>{
				position-=item
				// inner value is note
				let inner_value=string_note_A_pos_list[j][index]
				let top ="top: calc(var(--PMC_y_block) * "+position+");"
				//let top=""
				A_note_string+=`<div class="App_PMC_selected_voice_object_A_note" style="${top}" value="${object.nzc_note_A_pos[index]}">${inner_value}</div>`
			})
			A_note_obj_string_list.push(A_note_string)
			if(A_global_variables.show){
				//show A values too
				let A_pos_value_list
				let A_neg_value_list
				if(A_global_variables.type=="Ag"){
					//need to evaluate scale
					//find scale
					let pulse = object.pulse_float*voice_data.neopulse.N/voice_data.neopulse.D
					let current_grades_data= PMC_grades_data[DATA_get_Pa_eq_grade_data_index(pulse,PMC_grades_data)]
					// let current_scale= DATA_get_Pa_eq_scale(pulse)
					// if(current_scale!=null){
					A_pos_value_list=APP_A_list_to_text(object.A_pos,A_global_variables.type,{current_grades_data,base_note:object.nzc_note,base_diesis:object.diesis,pos:true}).split(A_spacer)
					A_neg_value_list=APP_A_list_to_text(object.A_neg,A_global_variables.type,{current_grades_data,base_note:object.nzc_note,base_diesis:object.diesis,pos:false}).split(A_spacer)
					// }else{
					// 	A_pos_value_list=APP_A_list_to_text(object.A_pos,"Am").split(A_spacer)
					// 	A_neg_value_list=APP_A_list_to_text(object.A_neg,"Am").split(A_spacer)
					// }
				}else{
					A_pos_value_list=APP_A_list_to_text(object.A_pos,A_global_variables.type).split(A_spacer)
					A_neg_value_list=APP_A_list_to_text(object.A_neg,A_global_variables.type).split(A_spacer)
				}
				let A_string=""
				let position=0.5
				object.A_pos.forEach((item,index)=>{
					position-=item
					if(item==1)return
					//let inner_value = item
					let inner_value = A_pos_value_list[index]
					let top ="top: calc(var(--PMC_y_block) * "+(position+0.5)+");"
					let left= "left: calc(50% - var(--RE_block_half));"
					A_string+=`<div class="App_PMC_selected_voice_object_A" value="[true,${index}]" style="${top}${left}line-height : calc(var(--PMC_y_block) * ${item-1});height : calc(var(--PMC_y_block) * ${item-1});">${inner_value}</div>`
				})
				position=0.5
				object.A_neg.forEach((item,index)=>{
					position+=item
					if(item==1)return
					//let inner_value = item
					let inner_value =  A_neg_value_list[index]
					let top ="top: calc(var(--PMC_y_block) * "+(position-item+0.5)+");"
					let left= "left: calc(50% - var(--RE_block_half));"
					A_string+=`<div class="App_PMC_selected_voice_object_A" value="[false,${index}]" style="${top}${left}line-height : calc(var(--PMC_y_block) * ${item-1});height : calc(var(--PMC_y_block) * ${item-1});">${inner_value}</div>`
				})
				A_obj_string_list.push(A_string)
			}else{
				A_obj_string_list.push("")
			}
		})
	}else{
		array_voice_data.forEach(object=>{
			A_note_obj_string_list.push("")
			A_obj_string_list.push("")
		})
	}

	let div_selected_voice_string=""
	//positioning and resizing main block
	let first_note = array_voice_data.find(object=>{
			return object.nzc_note>-1
	})
	let starting_pos = voice_data.e_sound
	if(first_note!=null){
		starting_pos = first_note.nzc_note
	}
	let prev_pos=starting_pos
	let margin_x=nuzic_block
	array_voice_data.forEach((object,index)=>{
		let nzc_note = object.nzc_note
		let note = 0
		if (nzc_note ==-1 || nzc_note ==-2 || nzc_note==-3){
			//previous position
			note = prev_pos
		}else{
			note = Number(nzc_note)
		}
		let y = delta_y*(max_nzc_note-note)-delta_y
		let width= object.duration *delta_x+"px"
		let marginTop= y+"px"
		prev_pos=note
		let marginLeft= margin_x+"px"
		margin_x+=object.duration *delta_x
		div_selected_voice_string+=`<div class="App_PMC_selected_voice_object" style="width:${width};top:${marginTop};left:${marginLeft}">
			${N_obj_string_list[index]}${P_obj_string_list[index]}${Pend_obj_string_list[index]}${iT_obj_string_list[index]}${iN_obj_string_list[index]}${A_obj_string_list[index]}${A_note_obj_string_list[index]}
		</div>`
	})
	div_selected_voice.innerHTML=div_selected_voice_string
	//repaint selected voice in visible voices canvas
	PMC_redraw_selected_voice_svg(true,voice_data)
}

function PMC_calculate_object_N_text(object_list,PMC_global_variables,neopulse,calc_A=false,PMC_grades_data){
	if(!PMC_global_variables.vsb_N.show){
		//return arr = Array(object_list.length).fill('')
		return [Array(object_list.length).fill(''),Array(object_list.length).fill(''),Array(object_list.length).fill('')]
	}
	let note_type = PMC_global_variables.vsb_N.type
	let last_note = -1
	//write note number
	let prev_reg = 999
	let string_list=[]
	let string_note_A_pos_list=[]
	let string_note_A_neg_list=[]
	// let data= DATA_get_current_state_point(false)
	// let scale_sequence = data.scale.scale_sequence
	// let idea_scale_list = data.scale.idea_scale_list
	//if(scale_sequence.length==0 && note_type=="Ngm")note_type="Nm"
	object_list.forEach((object,index)=>{
		let nzc_note = object.nzc_note
		if (nzc_note ==-1){
			//nothing to do
			string_list.push("")
			string_note_A_pos_list.push("")
			string_note_A_neg_list.push("")
		}else if(object.nzc_note ==-2){
			string_list.push("e")
			string_note_A_pos_list.push("")
			string_note_A_neg_list.push("")
		}else if(object.nzc_note ==-3){
			string_list.push("L")
			string_note_A_pos_list.push("")
			string_note_A_neg_list.push("")
		} else {
			//note
			let mod = TET
			let A_string=[]
			switch(note_type){
				case 'Na':
					string_list.push(nzc_note)
					//last_note=nzc_note
					string_note_A_pos_list.push(object.nzc_note_A_pos)
					string_note_A_neg_list.push(object.nzc_note_A_neg)
				break;
				case 'Nm':
					let reg = Math.floor(nzc_note/mod)
					let resto = nzc_note%mod
					if(reg != prev_reg){
						string_list.push(resto+"r"+reg)
					} else {
						string_list.push(resto)
					}
					prev_reg = reg
					last_note=nzc_note
					let prev_i_reg=reg
					object.nzc_note_A_pos.forEach(item=>{
						let i_reg=Math.floor(item/mod)
						let i_resto = item%mod
						if(prev_i_reg != i_reg){
							A_string.push(i_resto+"r"+i_reg)
							prev_i_reg=i_reg
						} else {
							A_string.push(i_resto)
						}
					})
					string_note_A_pos_list.push(A_string)
					A_string=[]
					prev_i_reg=reg
					object.nzc_note_A_neg.forEach(item=>{
						let i_reg=Math.floor(item/mod)
						let i_resto = item%mod
						if(prev_i_reg != i_reg){
							A_string.push(i_resto+"r"+i_reg)
							prev_i_reg=i_reg
						} else {
							A_string.push(i_resto)
						}
					})
					string_note_A_neg_list.push(A_string)
				break;
				case'Ngm':
					//find scale
					let pulse = object.pulse_float*neopulse.N/neopulse.D
					//let current_scale= DATA_get_Pa_eq_scale(pulse)
					let current_grades_data= PMC_grades_data[DATA_get_Pa_eq_grade_data_index(pulse,PMC_grades_data)]
					//scale never == null (type changed to Nm bf switch)
					//element inside a scale module
					//determine if diesis or bemolle
					//var positive = object.diesis
					let [grade,delta,regG] = DATA_calculate_absolute_note_to_grade(object.nzc_note,object.diesis,current_grades_data)
					//warning first element
					//verify is has module, if not read previous value
					let showreg = true
					//here retrive prev element with a reg
					if(prev_reg==regG)showreg=false
					prev_reg=regG
					let diesis = _delta_to_diesis_string(delta)
					if(delta==1){
						diesis="+"
					}else if(delta>1){
						diesis="+"+(delta)
					}else if(delta==-1){
						diesis="-"
					}else if(delta<-1){
						diesis="-"+Math.abs(delta)
					}
					if(showreg){
						stringN=grade+diesis+"r"+regG
					}else{
						stringN=grade+diesis
					}
					string_list.push(stringN)
					let prev_i_regG=regG
					object.nzc_note_A_pos.forEach(item=>{
						let [i_grade,i_delta,i_reg]=DATA_calculate_absolute_note_to_grade(item,object.diesis,current_grades_data)
						let i_diesis= _delta_to_diesis_string(i_delta)
						if(prev_i_regG != i_reg){
							A_string.push(i_grade+i_diesis+"r"+i_reg)
							prev_i_regG=i_reg
						} else {
							A_string.push(i_grade+i_diesis)
						}
					})
					string_note_A_pos_list.push(A_string)
					A_string=[]
					prev_i_regG=regG
					object.nzc_note_A_neg.forEach(item=>{
						let [i_grade,i_delta,i_reg]=DATA_calculate_absolute_note_to_grade(item,object.diesis,current_grades_data)
						let i_diesis= _delta_to_diesis_string(i_delta)
						if(prev_i_regG != i_reg){
							A_string.push(i_grade+i_diesis+"r"+i_reg)
							prev_i_regG=i_reg
						} else {
							A_string.push(i_grade+i_diesis)
						}
					})
					string_note_A_neg_list.push(A_string)
					last_note=nzc_note
				break;
			}
		}
	})
	function _delta_to_diesis_string(d){
		let diesis = ""
		if(d==1){
			diesis="+"
		}else if(d>1){
			diesis="+"+(d)
		}else if(d==-1){
			diesis="-"
		}else if(d<-1){
			diesis="-"+Math.abs(d)
		}
		return diesis
	}
	return [string_list,string_note_A_pos_list,string_note_A_neg_list]
}

function PMC_redraw_selected_voice_svg(full_color,voice_data){
	//Selected voice
	let D_NP = voice_data.neopulse.D
	let N_NP = voice_data.neopulse.N
	let neopulse_moltiplicator =  N_NP/D_NP
	let voice_color = eval(voice_data.color)
	let voice_color_light = eval(voice_data.color+"_light")
	let voice_color_white = eval("nuzic_white")
	if(!full_color) voice_color = voice_color_light
	let offset = nuzic_block
	//voice widths
	let delta = nuzic_block * PMC_zoom_x * neopulse_moltiplicator
	let delta_y = nuzic_block * PMC_zoom_y
	let note_number = TET*N_reg
	let array_voice_positions = PMC_segment_data_to_obj_data(voice_data.data.segment_data,voice_data.neopulse)
	//draw objects
	//positioning and resizing main block
	let first_note = array_voice_positions.find(object=>{
			return object.nzc_note>-1
	})
	//draw objects
	let starting_pos = voice_data.e_sound
	if(first_note!=null){
		starting_pos = first_note.nzc_note
	}
	let last_nzc_note=starting_pos
	let last_nzc_note_A_pos=[]
	let last_nzc_note_A_neg=[]
	let last_pos=starting_pos
	let svg_string=""
	array_voice_positions.forEach(object=>{
		let nzc_note = object.nzc_note
		if (nzc_note ==-1){
			//silence == nothing
			last_nzc_note=nzc_note
			last_nzc_note_A_pos=[]
			last_nzc_note_A_neg=[]
		}else if (nzc_note ==-2){
			//element box positioned in line last note
			let note = last_pos
			let x1 = offset+object.pulse_float*delta
			let x2 = x1 + object.duration *delta
			let y = delta_y*(note_number-note)-delta_y+1
			if(x2>2)x2-=1//little space
			svg_string+=`<rect x="${x1}" y="${y}" width="${x2-x1}" height="${delta_y-2}" fill="var(--Nuzic_white)" stroke="${voice_color}" stroke-width="2"></rect>`
			last_nzc_note=nzc_note
			last_nzc_note_A_pos=[]
			last_nzc_note_A_neg=[]
		}else if(nzc_note ==-4){
			//nothing
			last_nzc_note=nzc_note
			last_nzc_note_A_pos=[]
			last_nzc_note_A_neg=[]
		} else if(nzc_note ==-3){
			//ligadura, need to print something
			if(last_nzc_note!=-1 && last_nzc_note!=-4){
				let note = last_pos
				let x1 = offset+object.pulse_float*delta
				let x2 = x1 + object.duration *delta
				let y = delta_y*(note_number-note)-delta_y/2
				//if(x2>2)x2-=1//NO NEED little space FOR SELECTED VOICE
				//x1-=1 //connect with previous
				if(last_nzc_note==-2){
					//modify last
					let last_line = svg_string.trim().match(/.*$/)[0]
					//modify value x2 of the rect in last line
					let last_line_array = svg_string.split(' width="').pop().split('" height=')
					let width_text = last_line_array[0]
					let old_width=parseInt(width_text)
					let s_add=old_width+x2-x1
					last_line=last_line.replace(' width="'+width_text+'"',' width="'+s_add+'"')
					svg_string+=last_line
				}else{
					//it is a note
					svg_string+=`<line x1="${x1}" y1="${y}" x2="${x2}" y2="${y}" stroke="${voice_color}" stroke-width="${delta_y}"></line>
						<line x1="${x1}" y1="${y}" x2="${x2}" y2="${y}" stroke="${voice_color_light}" stroke-width="${delta_y-4}"></line>
						<line x1="${x2}" y1="${y-delta_y/2}" x2="${x2}" y2="${y+delta_y/2}" stroke="${voice_color}" stroke-width="2"></line>`
					if(A_global_variables.show_note){
						//A_global_variables.type for the value
						last_nzc_note_A_pos.forEach(item=>{
							y=delta_y*(note_number-item)-delta_y/2
							svg_string+=`<line x1="${x1}" y1="${y}" x2="${x2}" y2="${y}" stroke="${voice_color_light}" stroke-width="${delta_y}"></line>`
						})
						last_nzc_note_A_neg.forEach(item=>{
							y=delta_y*(note_number-item)-delta_y/2
							svg_string+=`<line x1="${x1}" y1="${y}" x2="${x2}" y2="${y}" stroke="${voice_color_light}" stroke-width="${delta_y}"></line>`
						})
					}
				}
			}else if(last_nzc_note==-1){
				//L of a silence (not visible in not selected voice BUT visible in current voice)
				let note = last_pos
				let x1 = offset+object.pulse_float*delta
				let x2 = x1 + object.duration *delta
				let y = delta_y*(note_number-note)-delta_y/2-1
				svg_string+=`<line x1="${x1}" y1="${y}" x2="${x2}" y2="${y}" stroke="var(--Nuzic_pink)" stroke-dasharray="4,4" stroke-width="2"></line>`
			}
		} else {
			let note = nzc_note
			let x1 = offset+object.pulse_float*delta
			let x2 = x1 + object.duration *delta
			let y = delta_y*(note_number-note)-delta_y/2
			//if(x2>2)x2-=1//little space
			svg_string+=`<line x1="${x1}" y1="${y}" x2="${x2}" y2="${y}" stroke="${voice_color}" stroke-width="${delta_y}"></line>`
			if(A_global_variables.show_note){
				object.nzc_note_A_pos.forEach(item=>{
					//y=delta_y*(note_number-item)-delta_y/2
					//svg_string+=`<line x1="${x1}" y1="${y}" x2="${x2}" y2="${y}" stroke="${voice_color}" stroke-width="${delta_y}"></line>`
					y=delta_y*(note_number-item)-delta_y+1
					svg_string+=`<rect x="${x1}" y="${y}" width="${x2-x1}" height="${delta_y-2}" fill="${voice_color_light}" stroke="${voice_color}" stroke-width="2"></rect>`
					// y=delta_y*(note_number-item)-delta_y+6
					// svg_string+=`<rect x="${x1}" y="${y}" width="${x2-x1}" height="${delta_y-10}" fill="${voice_color}" stroke="${voice_color}" stroke-width="2"></rect>`
				})
				object.nzc_note_A_neg.forEach(item=>{
					y=delta_y*(note_number-item)-delta_y+1
					svg_string+=`<rect x="${x1}" y="${y}" width="${x2-x1}" height="${delta_y-2}" fill="${voice_color_light}" stroke="${voice_color}" stroke-width="2"></rect>`
				})
				last_nzc_note_A_pos=object.nzc_note_A_pos
				last_nzc_note_A_neg=object.nzc_note_A_neg
			}
			last_nzc_note=nzc_note
			last_pos=nzc_note
		}
	})
	PMC_svg_selected_voice.style.height=PMC_svg_all_voices.style.height
	PMC_svg_selected_voice.style.width=PMC_svg_all_voices.style.width //not for all iT redraw first redraw all svg voices!!!
	PMC_svg_selected_voice.innerHTML=svg_string
}

function PMC_redraw_selected_voice_sequence(voice_data=null,current_Li=null,PMC_grades_data=null){
	let note_container = document.querySelector("#App_PMC_selected_voice_note_sequence_container")
	let A_pos_container = document.querySelector("#App_PMC_selected_voice_A_pos_sequence_container")
	let A_neg_container = document.querySelector("#App_PMC_selected_voice_A_neg_sequence_container")
	if(voice_data==null) voice_data = DATA_get_selected_voice_data()
	if(current_Li==null) current_Li=Li
	if(PMC_grades_data==null)PMC_grades_data=DATA_calculate_grade_data(current_Li)
	let D_NP = voice_data.neopulse.D
	let N_NP = voice_data.neopulse.N
	let neopulse_moltiplicator =  N_NP/D_NP
	let total_length = PMC_x_length(voice_data) /// needed in order to add ending spacers
	let voice_color = eval(voice_data.color)
	let voice_color_light = eval(voice_data.color+"_light")
	let voice_instrument = voice_data.instrument
	let voice_id=voice_data.voice_id
	let css_border_note="border-right:1px solid var(--Nuzic_light);border-left:1px solid var(--Nuzic_light);border-top:2px solid var(--Nuzic_white);border-bottom:2px solid var(--Nuzic_white);"
	let css_border_note_es="border-right:1px solid var(--Nuzic_light);border-left:1px solid var(--Nuzic_light);border-top:2px solid var(--Nuzic_light);border-bottom:2px solid var(--Nuzic_light);"
	let css_border_note_A_b="border-right:1px solid var(--Nuzic_light);border-left:1px solid var(--Nuzic_light);border-top:2px solid var(--Nuzic_white);border-bottom:2px solid var(--Nuzic_pink_light);"
	let css_border_note_A_t="border-right:1px solid var(--Nuzic_light);border-left:1px solid var(--Nuzic_light);border-top:2px solid var(--Nuzic_pink_light);border-bottom:2px solid var(--Nuzic_white);"
	let css_border_note_A="border-right:1px solid var(--Nuzic_light);border-left:1px solid var(--Nuzic_light);border-top:2px solid var(--Nuzic_pink_light);border-bottom:2px solid var(--Nuzic_pink_light);"
	let css_border_A_pos="border-right:1px solid var(--Nuzic_pink_light);border-left:1px solid var(--Nuzic_pink_light);border-top:4px solid var(--Nuzic_white);border-bottom:2px solid var(--Nuzic_pink_light);"
	let css_border_A_neg="border-right:1px solid var(--Nuzic_pink_light);border-left:1px solid var(--Nuzic_pink_light);border-top:2px solid var(--Nuzic_pink_light);border-bottom:4px solid var(--Nuzic_white)"
	let css_border_A_pos_W="border-right:1px solid var(--Nuzic_white);border-left:1px solid var(--Nuzic_white);border-top:4px solid var(--Nuzic_white);border-bottom:2px solid var(--Nuzic_white);"
	let css_border_A_neg_W="border-right:1px solid var(--Nuzic_white);border-left:1px solid var(--Nuzic_white);border-top:2px solid var(--Nuzic_white);border-bottom:4px solid var(--Nuzic_white)"
	let css_border_A_pos_L="border-right:1px solid var(--Nuzic_pink_light);border-left:1px solid var(--Nuzic_pink_light);border-top:4px solid var(--Nuzic_white);border-bottom:2px solid var(--Nuzic_white);"
	let css_border_A_neg_L="border-right:1px solid var(--Nuzic_pink_light);border-left:1px solid var(--Nuzic_pink_light);border-top:2px solid var(--Nuzic_white);border-bottom:4px solid var(--Nuzic_white)"
	let delta_x = nuzic_block * PMC_zoom_x * neopulse_moltiplicator
	let offset = nuzic_block
	//selected voices objects
	let array_voice_positions = PMC_segment_data_to_obj_data(voice_data.data.segment_data,voice_data.neopulse)
	//draw objects
	let note_container_string=""
	let A_pos_container_string=""
	let A_neg_container_string=""
	//first spacer
	let spacer_div= '<div class="App_PMC_selected_voice_sequence_spacer" style="width: var(--RE_block); min-width: var(--RE_block);"></div>'
	note_container_string+=spacer_div
	A_pos_container_string+=spacer_div
	A_neg_container_string+=spacer_div
	//elements
	let prev_reg = 999
	let previousRange = -1
	let positive = true
	//global variables
	A_global_list=[]
	let A_type=A_global_variables.type
	let vsb_N = PMC_read_vsb_N()
	let type = vsb_N.type
	// if(scale_sequence.length==0&&A_type=="Ag")A_type="Am"
	// if(scale_sequence.length==0 && type=="Ngm")type="Nm"
	let prev_A_pos=false
	let prev_A_neg=false
	array_voice_positions.forEach(object=>{
		let value
		let A_pos_value=""
		let A_neg_value=""
		let A_pos=false
		let A_neg=false
		let current_grades_data=null
		//need to evaluate scale
		if(A_type=="Ag" || type=="Ngm"){
			//find scale
			let pulse = object.pulse_float*voice_data.neopulse.N/voice_data.neopulse.D
			//current_scale= DATA_get_Pa_eq_scale(pulse)
			current_grades_data= PMC_grades_data[DATA_get_Pa_eq_grade_data_index(pulse,PMC_grades_data)]
		}
		if(object.nzc_note>=0){
			// if(A_type=="Ag" && current_scale!=null){
			if(A_type=="Ag"){
				A_pos_value=APP_A_list_to_text(object.A_pos,A_type,{current_grades_data,base_note:object.nzc_note,base_diesis:object.diesis,pos:true})
				A_neg_value=APP_A_list_to_text(object.A_neg,A_type,{current_grades_data,base_note:object.nzc_note,base_diesis:object.diesis,pos:false})
			}else{
				//let used_type=(A_type=="Ag" && current_scale==null)?"Am":A_type
				A_pos_value=APP_A_list_to_text(object.A_pos,A_type)
				A_neg_value=APP_A_list_to_text(object.A_neg,A_type)
			}
			APP_add_A_text_to_A_global_list(A_pos_value)
			APP_add_A_text_to_A_global_list(A_neg_value)
		}
		let onchange_string=""
		let onkeypress_string=""
		//inp.value = object.nzc_note
		let backgroundColor_string = "var(--Nuzic_white)"
		//let color_string=voice_color
		let A_disabled=""
		let border=(A_disabled=="")?css_border_note_A:css_border_note
		if (object.nzc_note ==-1){
			value = "s"
			A_disabled=" disabled=true "
			border=css_border_note_es
		}else if (object.nzc_note ==-2){
			value = "e"
			A_disabled=" disabled=true "
			border=css_border_note_es
		}else if(object.nzc_note ==-3){
			value = "L"
			A_disabled=" disabled=true "
			//borders
			backgroundColor_string = voice_color_light
			border=css_border_note
		} else {
			backgroundColor_string = voice_color
			//color_string = "var(--Nuzic_white)"
			border=css_border_note
			A_pos=!(object.A_pos.length==0)
			A_neg=!(object.A_neg.length==0)
			if(A_pos && A_neg)border=css_border_note_A
			if(A_pos && !A_neg)border=css_border_note_A_t
			if(!A_pos && A_neg)border=css_border_note_A_b
		}
		//note input calc
		switch(type){
		case 'Na':
			if(object.nzc_note >=0)value = object.nzc_note
			onkeypress_string="APP_inpTest_Na_line(event,this)"
			onchange_string=`PMC_change_selected_voice_sequence_object_note(this,'${type}')`
		break;
		case 'Nm':
			if(object.nzc_note >=0){
				let mod = TET
				let reg = Math.floor(object.nzc_note/mod)
				let resto = object.nzc_note%mod
				if(reg != prev_reg){
					value=`${resto}r${reg}`
				} else {
					value=resto
				}
				prev_reg = reg
			}
			onkeypress_string="APP_inpTest_Nm_line(event,this)"
			onchange_string=`PMC_change_selected_voice_sequence_object_note(this,'${type}')`
		break;
		case 'Ngm':
			//element inside a scale module
			if(object.nzc_note >=0){
				//determine if diesis or bemolle
				let [grade,delta,reg] = DATA_calculate_absolute_note_to_grade(object.nzc_note,object.diesis,current_grades_data)
				//warning first element
				//verify is has module, if not read previous value
				let showreg = true
				//here retrive prev element with a reg
				if(prev_reg==reg)showreg=false
				prev_reg=reg
				let diesis = ""
				if(delta==1){
					diesis="+"
				}else if(delta>1){
					diesis="+"+(delta)
				}else if(delta==-1){
					diesis="-"
				}else if(delta<-1){
					diesis="-"+Math.abs(delta)
				}
				if(showreg){
					stringN=grade+diesis+"r"+reg
				}else{
					stringN=grade+diesis
				}
				value=stringN
			}
			onkeypress_string="APP_inpTest_Ngm_line(event,this)"
			if(current_grades_data.acronym==null){
				onchange_string=`PMC_change_selected_voice_sequence_object_note(this,'Nm')`
			}else{
				let current_scale_string="null"
				current_scale_string=current_grades_data.acronym+"_"+current_grades_data.starting_note+"_"+current_grades_data.rotation+"_"+current_grades_data.duration
				onchange_string=`PMC_change_selected_voice_sequence_object_note(this,'${type}','${current_scale_string}')`
			}
		break;
		}
		note_container_string+=`<input class="App_PMC_selected_voice_sequence_object"
		style="width: calc(var(--PMC_x_block) * ${object.duration} * ${neopulse_moltiplicator}); min-width: calc(var(--PMC_x_block) * ${object.duration} * ${neopulse_moltiplicator});
		background-color: ${backgroundColor_string};${border};"
		onfocusin="APP_set_previous_value(this);PMC_play_this_note_number(${object.nzc_note},${voice_instrument},${voice_id})"
		onchange="${onchange_string}"
		onkeydown="PMC_move_focus_voice_sequence(event,this,'App_PMC_selected_voice_sequence_object')"
		onkeypress="${onkeypress_string}"
		value="${value}"/>`
		let A_function_string=""
		if(A_disabled==""){
			if(current_grades_data==null && A_type=="Ag"){
				//no delete
				//A_function_string=`onkeypress="event.preventDefault();this.value=previous_value" onfocusin="APP_set_previous_value(this)" onfocusout="this.value=previous_value" onkeydown="PMC_move_focus_voice_sequence_A(event,this)"`
				A_function_string=`onkeypress="event.preventDefault();this.value=previous_value" onfocusin="APP_set_previous_value(this)" onfocusout="PMC_change_selected_voice_sequence_object_A(this,'${A_type}')" onkeydown="PMC_move_focus_voice_sequence_A(event,this)"`
			}else{
				A_function_string=`onkeypress="APP_inpTest_A_line(event,this,'${A_type}')" onkeydown="PMC_move_focus_voice_sequence_A(event,this)"
				onfocusin="APP_set_previous_value(this);APP_show_A_suggestions(this)" onfocusout="PMC_change_selected_voice_sequence_object_A(this,'${A_type}')"`
			}
		}
		let border_pos=(A_disabled=="" && A_pos)?css_border_A_pos:css_border_A_pos_W
		let border_neg=(A_disabled=="" && A_neg)?css_border_A_neg:css_border_A_neg_W
		//let bg_pos=(A_disabled=="" && A_pos)?"var(--Nuzic_pink)":"transparent"
		let bg_pos=(A_disabled=="")?((A_pos)?"var(--Nuzic_pink)":"var(--Nuzic_pink_light)"):"transparent"
		let bg_neg=(A_disabled=="")?((A_neg)?"var(--Nuzic_pink)":"var(--Nuzic_pink_light)"):"transparent"
		//let bg_neg=(A_disabled=="" && A_neg)?"var(--Nuzic_pink)":"transparent"
		if(A_disabled!="" && object.nzc_note==-3){
			if(prev_A_pos){
				A_pos_value="L"
				border_pos=css_border_A_pos_L
				bg_pos="var(--Nuzic_pink_light)"
			}
			if(prev_A_neg){
				A_neg_value="L"
				border_neg=css_border_A_neg_L
				bg_neg="var(--Nuzic_pink_light)"
			}
		}
		prev_A_pos=A_pos
		prev_A_neg=A_neg
		A_pos_container_string+=`<input class="App_PMC_selected_voice_sequence_object"
			style="width: calc(var(--PMC_x_block) * ${object.duration} * ${neopulse_moltiplicator}); min-width: calc(var(--PMC_x_block) * ${object.duration} * ${neopulse_moltiplicator});
			background-color: ${bg_pos};${border_pos}"
			pos="true" value="${A_pos_value}" ${A_disabled} ${A_function_string}/>`
		A_neg_container_string+=`<input class="App_PMC_selected_voice_sequence_object"
			style="width: calc(var(--PMC_x_block) * ${object.duration} * ${neopulse_moltiplicator}); min-width: calc(var(--PMC_x_block) * ${object.duration} * ${neopulse_moltiplicator});
			background-color: ${bg_neg};${border_neg}"
			pos="false" value="${A_neg_value}" ${A_disabled} ${A_function_string}/>`
	})
	//last spacer
	let spacer_moltiplicator= total_length-Li-0.5
	spacer_div= `<div class="App_PMC_selected_voice_sequence_spacer" style="width: calc(var(--PMC_x_block) * ${spacer_moltiplicator}); min-width: calc(var(--PMC_x_block) * ${spacer_moltiplicator});"></div>`
	note_container_string+=spacer_div
	A_pos_container_string+=spacer_div
	A_neg_container_string+=spacer_div

	//svg segments
	let svg_string=`<svg class="App_PMC_selected_voice_sequence_svg" style="height: var(--RE_block);
	width: calc(var(--PMC_x_block) * ${total_length-0.5} + var(--RE_block));">`
	let segment_position = PMC_segment_data_to_segment_position(voice_data.data.segment_data)
	let c_height = nuzic_block * 1
	segment_position.forEach(item=>{
		let x = offset+item*delta_x-0.5
		svg_string+=`<line x1="${x}" y1="0" x2="${x}" y2="${c_height}" stroke="${nuzic_dark}" stroke-width="3"></line>`
	})
	svg_string+=`</svg>`

	note_container_string+=svg_string
	A_pos_container_string+=svg_string
	A_neg_container_string+=svg_string
	note_container.innerHTML = note_container_string
	A_pos_container.innerHTML = A_pos_container_string
	A_neg_container.innerHTML = A_neg_container_string
}

function PMC_redraw_compas_grid(voice_data){
	//Selected voice
	let total_length = PMC_x_length(voice_data)
	let note_lines = TET*N_reg-0.5
	let height_string = 'calc('+note_lines+' * var(--PMC_y_block) + var(--RE_block))'
	let width_string = "calc(var(--PMC_x_block) * "+(total_length-0.5)+" + var(--RE_block))"
	PMC_svg_compas.style.height=height_string
	PMC_svg_compas.style.width=width_string
	let delta = nuzic_block * PMC_zoom_x
	//var offset = (block + delta)/2 //2 half block
	let offset = nuzic_block
	let compas_data = DATA_get_current_state_point(false).compas
	let c_height = nuzic_block * PMC_zoom_y * note_lines + nuzic_block
	PMC_svg_compas.innerHTML=""
	let time_compas_change = []
	let position = 0
	compas_data.forEach(item=>{
		let lg = item.compas_values[1]
		let rep = item.compas_values[2]
		for (let i=0; i<rep;i++){
			position+=lg
			time_compas_change.push(position)
		}
	})
	//compas grid
	let svg_string = ""
	if(time_compas_change.length!=0){
		time_compas_change.unshift(0)
		for (let i=0; i<time_compas_change.length;i++){
			let x = offset+time_compas_change[i]*delta
			svg_string+=`
					<line x1="${x}" y1="0" x2="${x}" y2="${c_height}" stroke="${nuzic_light}" stroke-width="3"></line>
				`
		}
	}
	PMC_svg_compas.innerHTML=svg_string
}

function PMC_clear_compas_grid(voice_data){
	//Selected voice
	let total_length = PMC_x_length(voice_data)
	let note_lines = TET*N_reg-0.5
	let height_string = 'calc('+note_lines+' * var(--PMC_y_block) + var(--RE_block))'
	let width_string = "calc(var(--PMC_x_block) * "+(total_length-0.5)+" + var(--RE_block))"
	//svg
	PMC_svg_compas.style.height=height_string
	PMC_svg_compas.style.width=width_string
	PMC_svg_compas.innerHTML=""
}

function PMC_segment_data_to_obj_data(segment_data_in,neopulse){
	//given a segment_data_array return position of every object
	let segment_data = JSON.parse(JSON.stringify(segment_data_in))//FK COPY!!!
	let time = []
	let nzc_note_list=[]
	let A_pos_list=[]
	let A_neg_list=[]
	let diesis_list=[]
	let nzc_time_list = []
	let nzc_frac_list = []
	segment_data.forEach(segment=>{
		segment.sound.pop()
		segment.sound.forEach(item=>{
			nzc_note_list.push(item.note)
			diesis_list.push(item.diesis)
			A_pos_list.push(item.A_pos)
			A_neg_list.push(item.A_neg)
		})
		//create a list of times
		nzc_time_list = nzc_time_list.concat(segment.time)
		//create a list of fractioning ranges
		nzc_frac_list = nzc_frac_list.concat(segment.fraction)
	})

	//convert array time in pulse/fraction data
	let last_pulse = 0
	let segment_start = 0
	let time_PF_list = [[0,0]]
	nzc_time_list.forEach(value=> {
		if(value==="") {
			console.error("error database, pulse string conversion")
			time_PF_list.push( [null,null])
		}
		let pulse = value.P
		let fraction = value.F
		if(pulse == 0 && fraction==0) {
			//start of a segment
			segment_start=last_pulse
			pulse=last_pulse
		}else{
			pulse +=segment_start
			time_PF_list.push([pulse,fraction])
		}
		last_pulse = pulse
	})
	//convert array fraction ranges
	last_pulse = 0
	let fraction_range_list = []
	nzc_frac_list.forEach(fraction=>{
		let delta = fraction.stop-fraction.start
		let start = last_pulse
		let end = start+delta
		last_pulse = end
		fraction_range_list.push([[fraction.N,fraction.D],start,end,delta])
	})
	//calculate time in pulse
	let time_list = []
	let moltiplicator_list = []
	time_PF_list.forEach(item=>{
		let time_P = item[0]
		let time_F = 0
		if (item[1]!=0){
			let fraction_data = fraction_range_list.find((fraction)=>{
				return fraction[2]>item[0]
			})
			time_F = ((item[1] * fraction_data[0][0])/(fraction_data[0][1]))
			moltiplicator_list.push(fraction_data[0][0]/fraction_data[0][1])
		}else{
			moltiplicator_list.push(1)//F==0 so doesn t matter
		}
		time_list.push(time_P+time_F)
	})
	//recalculate the duration or iTs
	let index=1
	let max_index = time_list.length
	let duration_list = time_list.map(item=>{
		if(index<=max_index){
			let duration= time_list[index] - item
			index++
			return duration
		}
	})
	duration_list.pop()//last item is NaN
	//recalculate iT
	index=1
	let iT_list = time_PF_list.map(item=>{
		if(index<max_index){
			let time_P1 = item[0]
			let time_F1 = item[1]
			let time_P2 = time_PF_list[index][0]
			let time_F2 = time_PF_list[index][1]
			let fraction_data = fraction_range_list.find((fraction)=>{
				return fraction[2]>time_P1
			})
			let iT= (time_P2-time_P1) * (fraction_data[0][1]/fraction_data[0][0]) - time_F1 + time_F2
			index++
			return iT
		}
	})
	time_list.pop()//no end in midi_data only start and duration
	let seg_start_pulse=0
	let Ta_list = [] //pulse + pulse start segm
	let Ts_list = [] //pulse
	//calc pulse segments (pulse + segment start)
	Ta_list.push({P:0,F:0})
	Ts_list.push({P:0,F:0})
	nzc_time_list.forEach(nzc_time=>{
		if(nzc_time.P==0 && nzc_time.F==0){
			//Ta already there
			seg_start_pulse=Ta_list.slice(-1)[0].P
		}else{
			let current_Tsg_P=(seg_start_pulse+nzc_time.P)
			Ta_list.push({P:current_Tsg_P,F:nzc_time.F})
			Ts_list.push({P:nzc_time.P,F:nzc_time.F})
		}
	})

	//organize in a single beautiful array
	var array = []
	time_list.forEach((item,index)=>{
		let provv=nzc_note_list[index]
		let nzc_note_A_pos_list=A_pos_list[index].map(item=>{
			provv+=item
			return provv
		})
		provv=nzc_note_list[index]
		let nzc_note_A_neg_list=A_neg_list[index].map(item=>{
			provv-=item
			return provv
		})
		array.push({"pulse_float":item ,"duration":duration_list[index],"iT":iT_list[index],
			"nzc_note":nzc_note_list[index],"diesis":diesis_list[index],
			"nzc_note_A_pos":nzc_note_A_pos_list,"nzc_note_A_neg":nzc_note_A_neg_list,
			"A_pos":A_pos_list[index],"A_neg":A_neg_list[index],
			"Ts_data":Ts_list[index],"Ta_data":Ta_list[index],"frac_moltiplicator":moltiplicator_list[index]})
	})
	return array
}

function PMC_segment_data_to_segment_position(segment_data){
	//given a segment_data_array return position of every segment
	let position = 0
	let segment_position = segment_data.map(segment=>{
		position += segment.time[segment.time.length-1].P
		return position
	})
	segment_position.unshift(0)
	return segment_position
}

function PMC_segment_data_to_fraction_position(segment_data){
	//given a segment_data_array return position of every segment
	let position = 0
	let fraction_position = []
	segment_data.forEach(segment=>{
		segment.fraction.forEach((fraction,i)=>{
			if(i!=0)fraction_position.push(position+fraction.start)
		})
		position += segment.time[segment.time.length-1].P
	})
	//segment_position.unshift(0)
	return fraction_position
}

function PMC_set_H_scrollbar_step_N(n_boxes){
	let step = PMC_main_scrollbar_H.querySelector(".App_PMC_element_step_H")
	//change div width according to required boxes
	step.style.minWidth = "calc(var(--PMC_x_block) * "+(n_boxes-0.5)+" + var(--RE_block))"
}

function PMC_set_V_scrollbar_step_N(){
	//call every time TET change
	//changing V scrollbar elements
	let scrollbar_container = document.querySelector(".App_PMC_main_scrollbar_V")
	let step = scrollbar_container.querySelector(".App_PMC_element_step_V")
	//change div height according to required note
	let note_number = TET*N_reg-0.5
	step.style.minHeight = "calc(var(--PMC_y_block) * "+note_number+")"
}

//scrollbars sync with different voices
let scrollbarPMCposition_x = 0
let scrollbarPMCposition_y = 0
function PMC_checkScroll_H(value=null,forceScrollbarCheck = false) {
	let container = document.querySelector(".App_PMC")
	let time_line = container.querySelector(".App_PMC_time_line")
	let voice_note_sequence = container.querySelector("#App_PMC_selected_voice_note_sequence_container")
	let voice_A_pos_sequence = container.querySelector("#App_PMC_selected_voice_A_pos_sequence_container")
	let voice_A_neg_sequence = container.querySelector("#App_PMC_selected_voice_A_neg_sequence_container")
	// App_PMC_scale/compas modules
	let compas_div = container.querySelector('.App_PMC_S')
	let scales_div = container.querySelector('.App_PMC_C')
	// App_PMC_RE_preview XXX
	if(!PMC_main_scrollbar_H || !time_line || !PMC_svg_container) return;
	if(value!=null){
		//force scrollbar position
		PMC_main_scrollbar_H.scrollLeft=value
		forceScrollbarCheck=true
	}
	if(PMC_main_scrollbar_H.scrollLeft != scrollbarPMCposition_x) {
		//no in forceScrollbar
		PMC_close_dropdown_menu_segment()
	}
	if(PMC_main_scrollbar_H.scrollLeft != scrollbarPMCposition_x || forceScrollbarCheck) {
		scrollbarPMCposition_x = PMC_main_scrollbar_H.scrollLeft
		PMC_svg_container.scrollLeft = scrollbarPMCposition_x
		time_line.scrollLeft=  scrollbarPMCposition_x
		voice_note_sequence.scrollLeft=  scrollbarPMCposition_x
		voice_A_pos_sequence.scrollLeft=  scrollbarPMCposition_x
		voice_A_neg_sequence.scrollLeft=  scrollbarPMCposition_x
		compas_div.scrollLeft = scrollbarPMCposition_x
		scales_div.scrollLeft = scrollbarPMCposition_x

		APP_hide_scale_circle_elements()
	}
}

function PMC_checkScroll_V(percent=null,forceScrollbarCheck = false) {
	let container = document.querySelector(".App_PMC")
	let sound_line = container.querySelector(".App_PMC_sound_line")
	let letter_line = document.querySelector('.App_PMC_voice_selector_letter_guide')
	let scrollbar = container.querySelector('.App_PMC_main_scrollbar_V')
	scrollbar.removeEventListener('scroll', PMC_checkScroll_V)
	if(!scrollbar || !sound_line || !PMC_svg_container) return;
	if(percent!=null){
		//force scrollbar position
		if(percent>100)percent=100
		let value = Math.round(percent * (scrollbar.scrollHeight - scrollbar.clientHeight ) / 100)
			//scrollbar will move
		scrollbar.scrollTop=value
		scrollbarPMCposition_y=value
		//forceScrollbarCheck=true
		sound_line.scrollTop=  scrollbar.scrollTop
		letter_line.scrollTop = scrollbar.scrollTop
		return
	}
	if(scrollbar.scrollTop != scrollbarPMCposition_y || forceScrollbarCheck) {
		scrollbarPMCposition_y = scrollbar.scrollTop
		PMC_svg_container.scrollTop = scrollbar.scrollTop
		sound_line.scrollTop=  scrollbar.scrollTop
		letter_line.scrollTop = scrollbar.scrollTop
	}
}

function PMC_x_length(voice_data,current_Li=null){
	if(current_Li==null)current_Li=Li
	let max_length=PMC_x_dots(voice_data,current_Li)
	//neopulse!!
	max_length=max_length*voice_data.neopulse.N/voice_data.neopulse.D
	//Add scale if necessary
	let scaleData= DATA_get_current_state_point(false)
	let scale_values = scaleData.scale.scale_sequence
	let scaleCounter = 0
	scale_values.forEach((element, index) => {
		scaleCounter += element.duration
	})
	//extra bit
	if(max_length>scaleCounter){
		max_length += PMC_extra_x_space
		return max_length
	} else {
		scaleCounter += PMC_extra_x_space
		return scaleCounter
	}
}

//ELEMENTS

function PMC_change_selected_voice_sequence_object_note(element,type,current_scale_string){
	APP_stop()
	let line_container = document.querySelector("#App_PMC_selected_voice_note_sequence_container")
	let object_list = [...line_container.querySelectorAll(".App_PMC_selected_voice_sequence_object")]
	let position = object_list.indexOf(element)
	let voice_data = DATA_get_selected_voice_data()
	if(element.value==""){
		//DELETE
		let success = DATA_delete_object_index(voice_data.voice_id,null, position)
		if(!success){
			element.value=previous_value
			return
		}
		return
	}
	let note_Na,success
	switch(type){
	case 'Na':
		//re-check
		note_Na = parseInt(element.value)
		if(element.value=="L" || element.value=="l")note_Na=-3
		if(element.value=="e")note_Na=-2
		if(element.value=="s")note_Na=-1
		//calc in DATABASE
		success = DATA_modify_object_index_sound(voice_data.voice_id,null, position, note_Na,null,null,null)
		if(!success){
			element.value=previous_value
		}else{
			if(SNAV_read_play_new_note())APP_play_this_note_number(note_Na,voice_data.instrument,voice_data.voice_id)
		}
	break;
	case 'Nm':
		if(element.value=="L" || element.value=="l"){
			note_Na=-3
		}else if(element.value=="e"){
			note_Na=-2
		} else if(element.value=="s"){
			note_Na=-1
		}else {
			let split = element.value.split('r')
			let resto = Math.floor(split[0])
			let reg=3
			if(split[1]>=0 && split[1]!=""){
				reg=Math.floor(split[1])
			}else{
				//find previous element value
				if(position!= 0){
					let found = false
					let prev_position = position-1
					let note_array= []
					voice_data.data.segment_data.forEach(segment=>{
						let note_list = segment.sound.map(item=>{return item.note})
						note_list.pop()
						note_array= note_array.concat(note_list)
					})
					while(found == false){
						//console.log(note_array[prev_position])
						if(note_array[prev_position] >= 0){
							found = true
							reg= Math.floor(note_array[prev_position]/TET)
						} else {
							prev_position--
							if(prev_position<0)found=true
						}
					}
				}
			}
			note_Na = resto + reg*TET
		}
		//calc in DATABASE
		success = DATA_modify_object_index_sound(voice_data.voice_id,null, position, note_Na,null,null,null)
		if(!success){
			element.value=previous_value
		}else{
			if(SNAV_read_play_new_note())APP_play_this_note_number(note_Na,voice_data.instrument,voice_data.voice_id)
		}
	break;
	case 'Ngm':
		note_Na = null
		let diesis = null
		if(element.value=="L" || element.value=="l"){
			note_Na=-3
		}else if(element.value=="e"){
			note_Na=-2
		} else if(element.value=="s"){
			note_Na=-1
		}else {
			//current_scale
			if (current_scale_string=="null"){
				//no scale in this pulse
				console.error("no scale in this position")
				//using a unidentified element or a note if exist
				element.value=previous_value
				return
			}
			let s_values=current_scale_string.split("_")
			let current_scale = {acronym:s_values[0],starting_note:parseInt(s_values[1]),rotation:parseInt(s_values[2]),duration:parseInt(s_values[3])}
			//ATT! reg can be -1
			let reg = 3
			let grade = 0
			let delta = 0
			let string = element.value
			if(string.includes("r")){
				let split = string.split("r")
				if(split[1]!=""){
					reg=Math.floor(split[1])
				}else{
					//no scale in this pulse
					console.error("no reg spec")
					note_Na = null
					element.value=previous_value
					break;
				}
				let str_grade = split[0]
				if(split[0].includes("+")){
					let split2= split[0].split("+")
					str_grade=split2[0]
					delta=parseInt(split2[1])
					if(isNaN(delta))delta=1
					diesis=true
				}
				if(split[0].includes("-")){
					let split2= split[0].split("-")
					str_grade=split2[0]
					delta=parseInt(split2[1])*(-1)
					if(isNaN(delta))delta=-1
					diesis=false
				}
				grade = parseInt(str_grade)
			}else{
				//find reg in previous OR reg==3
				if(position!= 0){
					let found = false
					let prev_position = position-1
					let note_array= []
					voice_data.data.segment_data.forEach(segment=>{
						let note_list = segment.sound.map(item=>{return item.note})
						note_list.pop()
						note_array= note_array.concat(note_list)
					})
					while(found == false){
						//console.log(note_array[prev_position])
						if(note_array[prev_position] >= 0){
							found = true
							reg= Math.floor(note_array[prev_position]/TET)
						} else {
							prev_position--
							if(prev_position<0)found=true
						}
					}
				}
				let str_grade = string
				if(string.includes("+")){
					let split2= string.split("+")
					str_grade=split2[0]
					delta=parseInt(split2[1])
					if(isNaN(delta))delta=1
					diesis=true
				}
				if(string.includes("-")){
					let split2= string.split("-")
					str_grade=split2[0]
					delta=parseInt(split2[1])*(-1)
					if(isNaN(delta))delta=-1
					diesis=false
				}
				grade = parseInt(str_grade)
			}
			let idea_scale_list=DATA_get_idea_scale_list()
			let scaleLength = idea_scale_list.find(item=>{
				return item.acronym==current_scale.acronym
			})
			if(grade>=scaleLength.iS.length){
				//console.log(scale)
				element.value = previous_value
				return
			} else {
				note_Na = DATA_calculate_scale_note_to_absolute(grade,delta,reg,current_scale,idea_scale_list).note//and outside_range XXX???
				if(note_Na==null){
					element.value = previous_value
					console.error("error calculating Na from Ngm")
					return
				}
			}
			if(isNaN(note_Na)){
				note_Na = null
			}
		}
		if(note_Na==null){
			console.error("i dont understand what woy wrote")
			element.value=previous_value
			return
		}

		//calc in DATABASE
		success = DATA_modify_object_index_sound(voice_data.voice_id,null, position, note_Na,diesis,null,null)
		if(!success){
			element.value=previous_value
		}else{
			if(SNAV_read_play_new_note())APP_play_this_note_number(note_Na,voice_data.instrument,voice_data.voice_id)
		}
	break;
	}
	//in case focus out with PMC click
	// PMC_reset_action_pmc()
}

function PMC_change_selected_voice_sequence_object_A(element,type){
	element.placeholder=""
	if(element.value==previous_value)return
	APP_stop()
	let line_container = element.parentNode
	if(line_container==null)return //case double fire last element refocus
	let object_list = [...line_container.querySelectorAll(".App_PMC_selected_voice_sequence_object")]
	let position = object_list.indexOf(element)
	let voice_data = DATA_get_selected_voice_data()
	//modify database
	let A_list=APP_text_to_A_list(element.value,type)
	if(A_list==null){
		//error
		element.value=previous_value
		return
	}
	let positive=element.getAttribute("pos")=="true"
	if(A_list.length==0){
		if(previous_value!=""){
			//delete
			let success = DATA_delete_object_index_A(voice_data.voice_id,null,position,positive,!positive)
			if(!success){
				element.value=previous_value
			}
		}else{
			element.value=previous_value
		}
		return
	}
	if(type=="Ag"){
		A_list=DATA_calculate_object_index_Ag_list_to_A_list(voice_data.voice_id,null, position,positive,A_list)
	}
	let A_pos=positive?A_list:null
	let A_neg=positive?null:A_list
	let success = DATA_modify_object_index_sound(voice_data.voice_id,null, position, null,null,A_pos,A_neg)
	//in case focusout with click on PMC
	//PMC_reset_action_pmc()
	if(!success){
		element.value=previous_value
		return
	}
	//mod global variable NO NEED
	//let cleaned_string=APP_A_list_to_text(A_list)
	//APP_add_suggestion_to_A_global_list(cleaned_string)//no need
}

function PMC_move_focus_voice_sequence_A(event,element){
	//like RE_move_focus
	switch(event.keyCode){
	case 38://UP
		if(element.placeholder!="")APP_scan_A_suggestions(element,true)
		break
	case 40://DOWN
		if(element.placeholder!="")APP_scan_A_suggestions(element,false)
		break
	default:
		let changed=false
		if(element.value!=previous_value)changed=true
		//if(A_list_a==A_list_b)changed=true
		if(event.keyCode==37 || event.keyCode==39 || event.keyCode==9 || event.keyCode==13)PMC_move_focus_voice_sequence(event,element,'App_PMC_selected_voice_sequence_object')
		// Key left.
		// Key right.
		// tab
		// intro
		//manually trigger event
		if((event.keyCode==37 || event.keyCode==39 || event.keyCode==9 || event.keyCode==13) && changed)element.dispatchEvent(new Event("focusout"))
		break
	}
}

function PMC_move_focus_voice_sequence(evt,element,class_type){
	switch(evt.keyCode){
	case 37:
		// Key left.
		PMC_focus_prev_index_voice_sequence(element,class_type)
		evt.preventDefault();
		break
	case 39:
		// Key right.
		PMC_focus_next_index_voice_sequence(element,class_type)
		evt.preventDefault();
		break
	case 9:
		//tab
		evt.preventDefault()
		if(evt.shiftKey) {
			//shift was down when tab was pressed
			PMC_focus_prev_index_voice_sequence(element,class_type)
		}else{
			//tab
			PMC_focus_next_index_voice_sequence(element,class_type)
		}
		break
	case 13:
		//intro
		PMC_focus_next_index_voice_sequence(element,class_type)
		break
	}
	//preventDefault to avoid scrollbar to move automatically
	//NO need to a function that eventually move the scrollbar if the imput is out of sight
}

function PMC_focus_next_index_voice_sequence(element,class_type){
	let parent = element.parentNode
	let obj_list = [...parent.querySelectorAll("."+class_type)]
	let index=obj_list.indexOf(element)
	if(index<obj_list.length-1){
		if(obj_list[index+1].disabled){
			PMC_focus_next_index_voice_sequence(obj_list[index+1],class_type)
		}else{
			PMC_focus_on_index_voice_sequence(index+1,obj_list,class_type,parent)
		}
	}else{
		if(element.disabled){
			//find original focus
			let current_focus=document.activeElement
			index=obj_list.indexOf(current_focus)
			if(index>-1){
				current_focus.dispatchEvent(new Event("change"))
				PMC_focus_on_index_voice_sequence(index,obj_list,class_type,parent)
			}
		}else{
			//manually trigger event
			element.dispatchEvent(new Event("change"))
			PMC_focus_on_index_voice_sequence(index,obj_list,class_type,parent)
		}
	}
}

function PMC_focus_prev_index_voice_sequence(element,class_type){
	let parent = element.parentNode
	let obj_list = [...parent.querySelectorAll("."+class_type)]
	let index=obj_list.indexOf(element)
	if(index!=0){
		if(obj_list[index-1].disabled){
			PMC_focus_prev_index_voice_sequence(obj_list[index-1],class_type)
		}else{
			PMC_focus_on_index_voice_sequence(index-1,obj_list,class_type,parent)
		}
	}else{
		if(element.disabled){
			//find original focus
			let current_focus=document.activeElement
			index=obj_list.indexOf(current_focus)
			if(index>-1){
				current_focus.dispatchEvent(new Event("change"))
				PMC_focus_on_index_voice_sequence(index,obj_list,class_type,parent)
			}
		}else{
			//manually trigger event
			element.dispatchEvent(new Event("change"))
			PMC_focus_on_index_voice_sequence(index,obj_list,class_type,parent)
		}
	}
}

function PMC_focus_on_index_voice_sequence(index,obj_list,class_type,parent){
	//first focus trigger refresh
	obj_list[index].focus()
	obj_list[index].select()

	//second focus stay
	requestAnimationFrame(function(){
		let obj_list = [...parent.querySelectorAll("."+class_type)]
		if(index>obj_list.length-1)index=obj_list.length-1
		obj_list[index].focus()
		obj_list[index].select()
	})

	//control scrollbar
	PMC_checkScroll_H(parent.scrollLeft)
}

function PMC_focus_on_element(element){
	element.focus()
	element.select()
}

//function PMC_reset_action_pmc(canvas=null,event=null){
function PMC_reset_action_pmc(reset_PMCRE=true){
	if(_PMC_drag_changed_voice){
		//reset previous state selected voice (mouse out canvas)
		PMC_redraw_selected_voice()
	}
	if(_PMC_drag_changed_all_voices){
		PMC_redraw_all_voices_svg()
	}
	PMC_reset_drag_variables()
	if(reset_PMCRE){
		//console.error("reset current_position_PMCRE")
		current_position_PMCRE.voice_data=DATA_get_selected_voice_data()
		current_position_PMCRE.PMC.X_start=null
		current_position_PMCRE.PMC.Y_start=null
		current_position_PMCRE.RE.scrollbar=true
		current_position_PMCRE.segment_index=null
		current_position_PMCRE.pulse=null
		current_position_PMCRE.fraction=null
		current_position_PMCRE.note=null
	}

	let [now,start,end,max]=APP_return_progress_bar_values()
	let width_pulse = max + 4.5
	//SVG
	let note_lines = TET * N_reg -0.5
	let height_string = 'calc('+note_lines+' * var(--PMC_y_block) + var(--RE_block))'
	let width_string = "calc(var(--PMC_x_block) * "+width_pulse+" + var(--RE_block))"

	PMC_svg_selection_background.style.height=height_string
	PMC_svg_selection_background.style.width=width_string
	PMC_svg_selection_elements.style.height=height_string
	PMC_svg_selection_elements.style.width=width_string
	PMC_mouse_action_svg_1.style.height=height_string
	PMC_mouse_action_svg_1.style.width=width_string
	PMC_mouse_action_svg_2.style.height=height_string
	PMC_mouse_action_svg_2.style.width=width_string

	//clear lines in axes
	PMC_mouse_action_x.style.stroke="transparent"
	PMC_mouse_action_y.style.stroke="transparent"
	PMC_mouse_action_y_text.innerHTML=""
	//clear lines
	// PMC_mouse_action_x_PMC.style.stroke="transparent" //OPTIONAL not implemented
	// PMC_mouse_action_y_PMC.style.stroke="transparent" //OPTIONAL not implemented
	//clear svg
	PMC_mouse_action_drag.style.fill="transparent"
	PMC_mouse_action_drag_element.style.fill="transparent"
	PMC_mouse_action_drag_element.style.stroke="transparent"
}

function PMC_calculate_mouse_position(X_pos,Y_pos,voice_data){
	//function calculate mouse position (segment_index , pulse, fraction, note, fraction_index, X(pixels), Y(pixels)
	//calc note
	let note = Math.round((TET*N_reg-1)- (Y_pos-0.5*PMC_zoom_y*nuzic_block)/(PMC_zoom_y*nuzic_block))
	//calc pulse,fraction and segment
	//var position = ((X_pos)/(PMC_zoom_x*nuzic_block))-1
	let position = ((X_pos-nuzic_block)/(PMC_zoom_x*nuzic_block))
	if(position<0){
		return {segment_index:0 , pulse: 0, fraction: 0 , note: note, X:nuzic_block, Y:(TET*N_reg- 0.5 - note) * PMC_zoom_y*nuzic_block, time: 0}
	}
	let time
	if(position>=Li){
		let segment_index= voice_data.data.segment_data.length-1
		let pulse = voice_data.data.segment_data[segment_index].time.slice(-1)[0].P
		X_pos = nuzic_block + Li * (PMC_zoom_x*nuzic_block)
		Y_pos = (TET*N_reg- 0.5 - note) * PMC_zoom_y*nuzic_block
		time = Li*60/PPM
		return {segment_index:segment_index, pulse: pulse, fraction: 0 , note: note, X:X_pos, Y:Y_pos, time: time}
	}

	//var pulse_raw_voice = Math.floor(Math.floor(position)*voice_data.neopulse.D/voice_data.neopulse.N)
	let pulse_raw_voice = (position*voice_data.neopulse.D/voice_data.neopulse.N)
	//calculate pulse in segment
	let segment_index=0
	let segment_start_position=0
	let segment_end_position=0
	voice_data.data.segment_data.find(segment=>{
		let Ls = segment.time.slice(-1)[0].P
		segment_start_position=segment_end_position
		segment_end_position+=Ls
		if(segment_end_position>pulse_raw_voice)return true
		segment_index++
	})
	let segment_start_position_abs=segment_start_position*voice_data.neopulse.N/voice_data.neopulse.D
	let pulse_raw_segment = pulse_raw_voice-segment_start_position
	//calculate pulse in current fractioning range
	//console.log(segment_index)
	let current_fraction = voice_data.data.segment_data[segment_index].fraction.find(fraction=>{
		return fraction.stop>pulse_raw_segment
	})
	let current_fraction_start_abs=current_fraction.start*voice_data.neopulse.N/voice_data.neopulse.D
	let delta_pixel_start_fraction =X_pos - nuzic_block - ((segment_start_position_abs+current_fraction_start_abs)*(PMC_zoom_x*nuzic_block))
	let delta_pixel_pulse= (PMC_zoom_x*nuzic_block)*voice_data.neopulse.N/voice_data.neopulse.D
	let delta_pixel_iT= delta_pixel_pulse*current_fraction.N/current_fraction.D
	//NEW ROULE: add whatever is smaller: 75% or 14px XXX XXX XXX UI XXX XXX XXX
	let roule75 = 14
	if((delta_pixel_iT*0.25)<roule75)roule75=Math.round(delta_pixel_iT*0.25)
	let iT_from_start_fractioning_range = Math.floor((delta_pixel_start_fraction+roule75)/delta_pixel_iT)
	let pulse = current_fraction.start+Math.floor(iT_from_start_fractioning_range/current_fraction.D)*current_fraction.N
	let fraction = iT_from_start_fractioning_range-(pulse-current_fraction.start)*(current_fraction.D/current_fraction.N)
	if(pulse<0 || fraction<0){
		console.error("PMC enter_new_element_pulse_note failed calculation mouse position")
		return
	}
	//not exactly here
	X_pos = nuzic_block + (segment_start_position_abs + current_fraction_start_abs) * (PMC_zoom_x*nuzic_block) + iT_from_start_fractioning_range * delta_pixel_iT
	Y_pos = (TET*N_reg- 0.5 - note) * PMC_zoom_y*nuzic_block
	time = (X_pos-nuzic_block)/(PMC_zoom_x*nuzic_block)*60/PPM
	return {segment_index:segment_index , pulse: pulse, fraction: fraction , note: note, X:X_pos, Y:Y_pos, time: time}
}

let _PMC_drag_A=false
let _PMC_drag_A_note=false
let _PMC_drag_A_mod=-1
let _PMC_drag_A_value=null
let _PMC_drag_iN=false
let _PMC_drag_iN_value=null
let _PMC_drag_iT=false
let _PMC_drag_iT_value=null
let _PMC_drag_iT_value_success=null
let _PMC_drag_el=false
let _PMC_drag_el_value=null
let _PMC_drag_obj_position=null
let _PMC_drag_changed_voice=false
let _PMC_drag_changed_all_voices=false

function PMC_reset_drag_variables(){
	_PMC_drag_A=false
	_PMC_drag_A_note=false
	_PMC_drag_A_mod=-1
	_PMC_drag_A_value=null
	_PMC_drag_iN=false
	_PMC_drag_iN_value=null
	_PMC_drag_iT=false
	_PMC_drag_iT_value=null
	_PMC_drag_iT_value_success=null
	_PMC_drag_el=false
	_PMC_drag_el_value=null
	_PMC_drag_changed_voice=false
	_PMC_drag_changed_all_voices=false
}

function PMC_mouse_down(event){
	APP_stop()
	let Y_pos=event.offsetY
	let X_pos=event.offsetX
	if(event.target.id!="App_PMC_selected_voice"){
		//hover on div
		Y_pos += event.target.offsetTop
		X_pos += event.target.offsetLeft
		if(!event.target.classList.contains("App_PMC_selected_voice_object")){
			let obj= event.target.closest(".App_PMC_selected_voice_object")
			Y_pos += obj.offsetTop
			X_pos += obj.offsetLeft
			if(event.target.classList.contains("App_PMC_selected_voice_object_iN")){
			 	Y_pos += event.target.parentNode.offsetTop
			}
		}
	}
	let mouse_position_corrected = PMC_calculate_mouse_position(X_pos,Y_pos,current_position_PMCRE.voice_data)
	current_position_PMCRE.PMC={X_start: mouse_position_corrected.X,Y_start : mouse_position_corrected.Y}
	current_position_PMCRE.segment_index = mouse_position_corrected.segment_index
	current_position_PMCRE.pulse = mouse_position_corrected.pulse
	current_position_PMCRE.fraction = mouse_position_corrected.fraction
	current_position_PMCRE.note = mouse_position_corrected.note
	//no need .... APP_player_to_time do that
	//PMC_draw_progress_line_position(mouse_position_corrected.X)
	let segment_start_PM=0
	let index=0
	current_position_PMCRE.voice_data.data.segment_data.find(item=>{
		if(index==current_position_PMCRE.segment_index){
			return true
		}
		segment_start_PM+=item.time.slice(-1)[0].P
		index++
		return false
	})
	let neopulse_moltiplicator = current_position_PMCRE.voice_data.neopulse.N/current_position_PMCRE.voice_data.neopulse.D
	let pulse_abs = segment_start_PM + mouse_position_corrected.pulse*neopulse_moltiplicator
	//iN or iT modifier
	PMC_reset_drag_variables()
	if(event.target.classList.contains("App_PMC_selected_voice_object_N") && event.ctrlKey){
		if(A_global_variables.show_note){
			_PMC_drag_A_note=true
			_PMC_drag_A_mod=-1
			_PMC_drag_A_value=0
			_PMC_drag_obj_position=PMC_calculate_mouse_position(event.target.parentNode.offsetLeft,event.target.parentNode.offsetTop+2,current_position_PMCRE.voice_data)
			return
		}
	}
	if(event.target.classList.contains("App_PMC_selected_voice_object_A_note") && event.ctrlKey){
		if(A_global_variables.show_note){
			_PMC_drag_A_note=true
			_PMC_drag_A_mod=parseInt(event.target.getAttribute("value"))
			_PMC_drag_A_value=0
			_PMC_drag_obj_position=PMC_calculate_mouse_position(event.target.parentNode.offsetLeft,event.target.parentNode.offsetTop+2,current_position_PMCRE.voice_data)
			return
		}
	}
	if(event.target.classList.contains("App_PMC_selected_voice_object_A")){
		if(A_global_variables.show_note){
			_PMC_drag_A=true
			_PMC_drag_A_mod=JSON.parse(event.target.getAttribute("value"))
			_PMC_drag_A_value=0
			_PMC_drag_obj_position=PMC_calculate_mouse_position(event.target.parentNode.offsetLeft,event.target.parentNode.offsetTop+2,current_position_PMCRE.voice_data)
			return
		}
	}
	if(event.target.classList.contains("App_PMC_selected_voice_object_iN")){
		_PMC_drag_iN=true
		_PMC_drag_iN_value=0
		let true_parentNode=event.target.parentNode.parentNode
		_PMC_drag_obj_position=PMC_calculate_mouse_position(true_parentNode.offsetLeft,true_parentNode.offsetTop+2,current_position_PMCRE.voice_data)
		//_PMC_drag_obj_position=PMC_calculate_mouse_position(event.target.parentNode.offsetLeft,event.target.parentNode.offsetTop+2,current_position_PMCRE.voice_data)
		return
	}
	if(event.target.classList.contains("App_PMC_selected_voice_object_iT")){
		if(!event.target.innerHTML=="" || event.target.classList.contains("hidden")){
			_PMC_drag_iT=true
			_PMC_drag_iT_value=0
			_PMC_drag_obj_position=PMC_calculate_mouse_position(event.target.parentNode.offsetLeft,event.target.parentNode.offsetTop+2,current_position_PMCRE.voice_data)
			return
		}
	}
	if(event.target.classList.contains("App_PMC_selected_voice_object_dot_start") || event.target.classList.contains("App_PMC_selected_voice_object_dot_start_unselected")){
		_PMC_drag_el=true
		_PMC_drag_el_value={d_N:0,d_T:{P:current_position_PMCRE.pulse,F:current_position_PMCRE.fraction}}
		_PMC_drag_obj_position=PMC_calculate_mouse_position(event.target.parentNode.offsetLeft,event.target.parentNode.offsetTop+2,current_position_PMCRE.voice_data)
		return
	}
	APP_player_to_time(mouse_position_corrected.time, pulse_abs , mouse_position_corrected.pulse,current_position_PMCRE.fraction,current_position_PMCRE.segment_index,current_position_PMCRE.voice_data.voice_id)
}

function PMC_mouse_move(event){
	//calc position
	let Y_pos=event.offsetY
	let X_pos=event.offsetX
	if(event.target.id!="App_PMC_selected_voice"){
		//hover on div
		Y_pos += event.target.offsetTop
		X_pos += event.target.offsetLeft
		if(!event.target.classList.contains("App_PMC_selected_voice_object")){
			let obj= event.target.closest(".App_PMC_selected_voice_object")
			Y_pos += obj.offsetTop
			X_pos += obj.offsetLeft
			if(event.target.classList.contains("App_PMC_selected_voice_object_iN")){
			 	Y_pos += event.target.parentNode.offsetTop
			}
		}
	}
	if(current_position_PMCRE==null || current_position_PMCRE.voice_data==null){
		PMC_reset_action_pmc()
	}
	//if(current_position_PMCRE.note==null)return//probably click from a focusout
	let mouse_position_corrected = PMC_calculate_mouse_position(X_pos,Y_pos,current_position_PMCRE.voice_data)
	//parameters
	SNAV_enter_on_segment_parameters(null,current_position_PMCRE.voice_data.name,current_position_PMCRE.voice_data.voice_id,mouse_position_corrected.segment_index)
	if(_PMC_drag_iN || _PMC_drag_iT || _PMC_drag_el || _PMC_drag_A_note || _PMC_drag_A){
		if(mouse_position_corrected!=undefined){
			if(_PMC_drag_A){
				let dA_value=mouse_position_corrected.note - current_position_PMCRE.note
				if(dA_value==_PMC_drag_A_value)return
				_PMC_drag_A_value=dA_value
				let selected_voice_data= JSON.parse(JSON.stringify(current_position_PMCRE.voice_data))
				result = DATA_calculate_modify_object_position_A_delta(selected_voice_data,_PMC_drag_obj_position.segment_index,_PMC_drag_obj_position.pulse,_PMC_drag_obj_position.fraction,dA_value,_PMC_drag_A_mod)
				if(!result.success){
					return
				}
				PMC_redraw_selected_voice(selected_voice_data)
				_PMC_drag_changed_voice=true
				return
			}
			if(_PMC_drag_A_note){
				//d_A generic interval (note or grade)
				let A_value=mouse_position_corrected.note - current_position_PMCRE.note
				if(A_value==_PMC_drag_A_value)return
				_PMC_drag_A_value=A_value
				let selected_voice_data= JSON.parse(JSON.stringify(current_position_PMCRE.voice_data))
				//modify existing Note sequence
				let result={success:false}
				result = DATA_calculate_modify_object_position_A_note_delta(selected_voice_data,_PMC_drag_obj_position.segment_index,_PMC_drag_obj_position.pulse,_PMC_drag_obj_position.fraction,A_value,_PMC_drag_A_mod)
				if(!result.success){
					return
				}
				//calculate need to be redrawn (multiple refresh on very little mouse move is bad)
				PMC_redraw_selected_voice(selected_voice_data)
				_PMC_drag_changed_voice=true
				return
			}
			if(_PMC_drag_iN){
				//d_iN generic interval (note or grade)
				let d_iN=mouse_position_corrected.note - current_position_PMCRE.note
				if(d_iN==_PMC_drag_iN_value)return
				_PMC_drag_iN_value=d_iN
				let selected_voice_data= JSON.parse(JSON.stringify(current_position_PMCRE.voice_data))
				//modify existing Note sequence
				let vsb_iN=PMC_read_vsb_iN()
				let result={success:false}
				if(vsb_iN.type=="iNgm"){
					result = DATA_calculate_modify_object_position_iNgrade_delta(selected_voice_data,_PMC_drag_obj_position.segment_index,_PMC_drag_obj_position.pulse,_PMC_drag_obj_position.fraction,d_iN)
				}else{
					//modify existing Note sequence
					result = DATA_calculate_modify_object_position_iN_delta(selected_voice_data,_PMC_drag_obj_position.segment_index,_PMC_drag_obj_position.pulse,_PMC_drag_obj_position.fraction,d_iN)
				}
				if(!result.success){
					return
				}
				//calculate need to be redrawn (multiple refresh on very little mouse move is bad)
				PMC_redraw_selected_voice(selected_voice_data)
				_PMC_drag_changed_voice=true
				return
			}
			if(_PMC_drag_iT){
				//var new_iT=mouse_position_corrected.note - current_position_PMCRE.note
				let segment=current_position_PMCRE.voice_data.data.segment_data[current_position_PMCRE.segment_index]
				let current_fraction= segment.fraction.find(frac_data=>{return frac_data.stop>current_position_PMCRE.pulse})
				//let d_px_iT=(current_position_PMCRE.voice_data.neopulse.N/current_position_PMCRE.voice_data.neopulse.D)* (PMC_zoom_x*nuzic_block)
				if(current_fraction==null)return
				let d_px_iT=(current_position_PMCRE.voice_data.neopulse.N/current_position_PMCRE.voice_data.neopulse.D)* (PMC_zoom_x*nuzic_block)*(current_fraction.N/current_fraction.D)
				//var d_iT= Math.round((mouse_position_corrected.X-current_position_PMCRE.PMC.X_start)/d_px_iT)
				let d_iT= Math.round((X_pos-current_position_PMCRE.PMC.X_start)/d_px_iT)
				//var new_iT=DATA_calculate_iT_from_time(current_position_PMCRE.pulse,current_position_PMCRE.fraction,mouse_position_corrected.pulse,mouse_position_corrected.fraction,segment)
				//if(new_iT==_PMC_drag_iT_value || new_iT==null)return
				if(d_iT==_PMC_drag_iT_value || d_iT==null)return
				_PMC_drag_iT_value=d_iT
				let selected_voice_data= JSON.parse(JSON.stringify(current_position_PMCRE.voice_data))
				//modify existing iT sequence
				let result = DATA_calculate_modify_object_position_iT_delta(selected_voice_data,_PMC_drag_obj_position.segment_index,_PMC_drag_obj_position.pulse,_PMC_drag_obj_position.fraction,d_iT)
				//result={success (true/false), voice_data (!=null if voice mod), data (!=null if other voices modified), error_iT_index (!= null if error, no redraw but blink)}
				if(!result.success){
					if(result.error_D_iT_index!=null){
						//blink error
						//find object
						let object_list = [...document.getElementById("App_PMC_selected_voice").querySelectorAll(".App_PMC_selected_voice_object")]
						object_list.find(element=>{
							if(element.offsetLeft==_PMC_drag_obj_position.X){
								//console.log(element)
								let error_element=object_list[object_list.indexOf(element)+result.error_D_iT_index]
								APP_blink_error(error_element)
								//error_element.style.backgroundColor="blue"
								return true
							}
						})
					}
					return
				}
				_PMC_drag_iT_value_success=d_iT
				let current_Li=null
				if(result.data!=null){
					//svg all data need redraw
					PMC_redraw_all_voices_svg(result.data)
					_PMC_drag_changed_all_voices=true
					selected_voice_data=result.data.voice_data.find(voice=>{return voice.voice_id==selected_voice_data.voice_id})
					current_Li=result.data.global_variables.Li
				} else if(_PMC_drag_changed_all_voices){
					//still need redraw to return normal value
					PMC_redraw_all_voices_svg()
					_PMC_drag_changed_all_voices=false
				}
				//redraw selected voice
				PMC_redraw_selected_voice(selected_voice_data,current_Li)
				_PMC_drag_changed_voice=true
				return
			}
			if(_PMC_drag_el){
				//drag note AND pulse start
				//NOTE
				//d_N generic interval (SINGLE note)
				let d_N=mouse_position_corrected.note - current_position_PMCRE.note
				let selected_voice_data=null
				let result_N={success:false}
				let success_T=false
				if(d_N!=_PMC_drag_el_value.d_N ){
					_PMC_drag_el_value.d_N=d_N
					selected_voice_data= JSON.parse(JSON.stringify(current_position_PMCRE.voice_data))
					//modify existing Note sequence
					result_N = DATA_calculate_modify_object_position_N_delta(selected_voice_data,_PMC_drag_obj_position.segment_index,_PMC_drag_obj_position.pulse,_PMC_drag_obj_position.fraction,d_N)
				}
				//PULSE
				if(mouse_position_corrected.segment_index==_PMC_drag_obj_position.segment_index &&
					(mouse_position_corrected.pulse!=_PMC_drag_el_value.d_T.P ||
					mouse_position_corrected.fraction!=_PMC_drag_el_value.d_T.F) ){
					if(selected_voice_data==null){
						selected_voice_data= JSON.parse(JSON.stringify(current_position_PMCRE.voice_data))
						//what if _PMC_drag_el_value.d_N is unsuccessful? nothing....
						DATA_calculate_modify_object_position_N_delta(selected_voice_data,_PMC_drag_obj_position.segment_index,_PMC_drag_obj_position.pulse,_PMC_drag_obj_position.fraction,_PMC_drag_el_value.d_N)
					}
					let result_T = DATA_calculate_modify_object_position_T_delta_free(selected_voice_data,_PMC_drag_obj_position.segment_index,_PMC_drag_obj_position.pulse,_PMC_drag_obj_position.fraction,mouse_position_corrected.pulse,mouse_position_corrected.fraction)
					success_T=result_T.success
					if(success_T){
						_PMC_drag_el_value.d_T.P=mouse_position_corrected.pulse
						_PMC_drag_el_value.d_T.F=mouse_position_corrected.fraction
					}
				}
				if(result_N.success && !success_T){
					//what if _PMC_drag_el_value.d_T is unsuccessful? nothing....
					DATA_calculate_modify_object_position_T_delta_free(selected_voice_data,_PMC_drag_obj_position.segment_index,_PMC_drag_obj_position.pulse,_PMC_drag_obj_position.fraction,_PMC_drag_el_value.d_T.P,_PMC_drag_el_value.d_T.F)
				}
				//if((result_N.success && result_N.outside_range!=true)|| success_T){
				if(result_N.success || success_T){
					//calculate need to be redrawn (multiple refresh on very little mouse move is bad)
					PMC_redraw_selected_voice(selected_voice_data)
					_PMC_drag_changed_voice=true
					return
				}
				return
			}
		}
		return
	}
	//axis //adding object
	if(mouse_position_corrected!=undefined){
		PMC_mouse_action_x.style.stroke=eval("nuzic_yellow")
		PMC_mouse_action_x.setAttribute('x1',mouse_position_corrected.X-0.5)
		PMC_mouse_action_x.setAttribute('y1','0')
		PMC_mouse_action_x.setAttribute('x2',mouse_position_corrected.X-0.5)
		PMC_mouse_action_x.setAttribute('y2','100%')
		PMC_mouse_action_y.style.stroke=eval("nuzic_pink")
		PMC_mouse_action_y.setAttribute('x1','0')
		PMC_mouse_action_y.setAttribute('y1',mouse_position_corrected.Y-0.5)
		PMC_mouse_action_y.setAttribute('x2','100%')
		PMC_mouse_action_y.setAttribute('y2',mouse_position_corrected.Y-0.5)
		if(PMC_read_zoom_sound()=="0"){
			//write note
			PMC_mouse_action_y_text.setAttribute('y',mouse_position_corrected.Y+nuzic_block_half*0.5)
			let string_note = ""
			let vsb_N=PMC_read_vsb_N()
			let note_type = vsb_N.type
			//write values
			let reg,note
			switch (note_type) {
			case "Na":
				string_note = mouse_position_corrected.note
			break;
			case "Nm":
				reg = Math.floor(mouse_position_corrected.note/TET)
				note = mouse_position_corrected.note%TET
				if(note==0){
					//string_note = `${note}r${reg}`
				} else{
					string_note = `${note}`
				}
			break;
			case "Ngm":
				reg = Math.floor(mouse_position_corrected.note/TET)
				note = mouse_position_corrected.note%TET
				if(note==0){
					//string_note = `${note}r${reg}`
				}else{
					string_note = `${note}`
				}
			break;
			}
			PMC_mouse_action_y_text.innerHTML=string_note
		}else{
			PMC_mouse_action_y_text.innerHTML=""
			//write correct note on axis
			if(PMC_read_vsb_N().type=="Ngm")PMC_write_sound_line_values(X_pos)//X_pos ??mouse_position_corrected
		}
	}else{
		PMC_mouse_action_x.style.stroke="transparent"
		PMC_mouse_action_y.style.stroke="transparent"
		// PMC_mouse_action_y_text.fill="transparent"
		// PMC_mouse_action_y_text.stroke="transparent"
		PMC_mouse_action_y_text.innerHTML=""
	}
	if(current_position_PMCRE.note==null)return//probably click from a focusout
	//if(!input_active)return//click from focusout
	//drag a note
	if(mouse_position_corrected!=undefined && event.buttons==1){
		if(mouse_position_corrected.segment_index==current_position_PMCRE.segment_index){
			if(mouse_position_corrected.X>current_position_PMCRE.PMC.X_start){
				let width = mouse_position_corrected.X-current_position_PMCRE.PMC.X_start
				PMC_mouse_action_drag.style.fill=eval("nuzic_light")
				PMC_mouse_action_drag.setAttribute('x',current_position_PMCRE.PMC.X_start-0.5)
				PMC_mouse_action_drag.setAttribute('width',width)
				let fill_color=current_position_PMCRE.voice_data.color
				let fill_color_light=fill_color+"_light"
				let border_color=current_position_PMCRE.voice_data.color
				//is_silence
				fill_color= (currKey=="s" || currKey=="z")?   "nuzic_white" :fill_color
				border_color= (currKey=="s" || currKey=="z")?   "nuzic_white" :fill_color
				//is_element
				fill_color= (currKey=="e" || currKey=="x")?   "nuzic_white" :fill_color
				//is_tie_intervals
				fill_color= (currKey=="l" || currKey=="c")?   fill_color_light :fill_color
				PMC_mouse_action_drag_element.style.fill=eval(fill_color)
				PMC_mouse_action_drag_element.style.stroke=`${eval(border_color)}`
				PMC_mouse_action_drag_element.setAttribute('x',current_position_PMCRE.PMC.X_start-0.5)
				PMC_mouse_action_drag_element.setAttribute('y',current_position_PMCRE.PMC.Y_start-0.5*PMC_zoom_y*nuzic_block)
				PMC_mouse_action_drag_element.setAttribute('width',width)
				PMC_mouse_action_drag_element.setAttribute('height',PMC_zoom_y*nuzic_block)
			}else{
				//clear
				PMC_mouse_action_drag_element.style.fill="transparent"
				PMC_mouse_action_drag_element.style.stroke="transparent"
				PMC_mouse_action_drag.style.fill="transparent"
			}
		}else{
			//end of segment
			if(current_position_PMCRE.segment_index==null)return
			if(mouse_position_corrected.X>current_position_PMCRE.PMC.X_start){
				//console.log(mouse_position_corrected)
				let segment_data = current_position_PMCRE.voice_data.data.segment_data
				let index = -1
				let pulse_abs = 0
				segment_data.find(item=>{
					index++
					pulse_abs+=item.time.slice(-1)[0].P
					return index==current_position_PMCRE.segment_index
				})
				let X_end = pulse_abs * (current_position_PMCRE.voice_data.neopulse.N/current_position_PMCRE.voice_data.neopulse.D)* (PMC_zoom_x*nuzic_block) + nuzic_block
				let width = X_end-current_position_PMCRE.PMC.X_start
				PMC_mouse_action_drag.style.fill=eval("nuzic_light")
				PMC_mouse_action_drag.setAttribute('x',current_position_PMCRE.PMC.X_start-0.5)
				PMC_mouse_action_drag.setAttribute('width',width)
				let fill_color=current_position_PMCRE.voice_data.color
				let fill_color_light=fill_color+"_light"
				let border_color=current_position_PMCRE.voice_data.color
				//is_silence
				fill_color= (currKey=="s" || currKey=="z")?   "nuzic_white" :fill_color
				border_color= (currKey=="s" || currKey=="z")?   "nuzic_white" :fill_color
				//is_element
				fill_color= (currKey=="e" || currKey=="x")?   "nuzic_white" :fill_color
				//is_tie_intervals
				fill_color= (currKey=="l" || currKey=="c")?   fill_color_light :fill_color
				PMC_mouse_action_drag_element.style.fill=eval(fill_color)
				PMC_mouse_action_drag_element.style.stroke=`${eval(border_color)}`
				PMC_mouse_action_drag_element.setAttribute('x',current_position_PMCRE.PMC.X_start-0.5)
				PMC_mouse_action_drag_element.setAttribute('y',current_position_PMCRE.PMC.Y_start-0.5*PMC_zoom_y*nuzic_block)
				PMC_mouse_action_drag_element.setAttribute('width',width)
				PMC_mouse_action_drag_element.setAttribute('height',PMC_zoom_y*nuzic_block)
			}else{
				//clear
				PMC_mouse_action_drag_element.style.fill="transparent"
				PMC_mouse_action_drag_element.style.stroke="transparent"
				PMC_mouse_action_drag.style.fill="transparent"
			}
		}
	}
}

let currKey = null;
window.onkeydown = function(event) {
	currKey = event.key
	PMC_key_shortcut_mouse_wheel(event)
}

window.onkeyup = function(event) {
	currKey = null
}

function PMC_mouse_up(event){
	//if mouse has moved create new element
	// console.log(input_active)
	// if(!input_active){
	// 	input_active=true
	// 	return
	// }
	if(current_position_PMCRE.note==null)return//probably click from a focusout
	let Y_pos=event.offsetY
	let X_pos=event.offsetX
	if(event.target.id!="App_PMC_selected_voice"){
		//hover on div
		Y_pos += event.target.offsetTop
		X_pos += event.target.offsetLeft
		if(!event.target.classList.contains("App_PMC_selected_voice_object")){
			let obj= event.target.closest(".App_PMC_selected_voice_object")
			Y_pos += obj.offsetTop
			X_pos += obj.offsetLeft
		}
	}
	//console.log(current_position_PMCRE)
	let mouse_position_corrected = PMC_calculate_mouse_position(X_pos,Y_pos,current_position_PMCRE.voice_data)
	//console.log(mouse_position_corrected)
	if(_PMC_drag_iN || _PMC_drag_iT || _PMC_drag_el || _PMC_drag_A_note || _PMC_drag_A){
		if(mouse_position_corrected!=undefined){
			if(_PMC_drag_A){
				let dA_value=mouse_position_corrected.note - current_position_PMCRE.note
				let selected_voice_data= JSON.parse(JSON.stringify(current_position_PMCRE.voice_data))
				let data_change=(dA_value!=0)
				result = DATA_calculate_modify_object_position_A_delta(selected_voice_data,_PMC_drag_obj_position.segment_index,_PMC_drag_obj_position.pulse,_PMC_drag_obj_position.fraction,dA_value,_PMC_drag_A_mod)
				PMC_reset_drag_variables()
				if(!result.success || !data_change){
					//rewrite
					PMC_redraw_selected_voice()
					//possible no note to change
					return
				}
				if(result.outside_range){
					APP_info_msg("out_N_range")
				}
				//SAVE new voice
				DATA_change_voice_data_at_voice_id(selected_voice_data)
				return
			}
			if(_PMC_drag_A_note){
				//d_A generic interval (note or grade)
				let A_value=mouse_position_corrected.note - current_position_PMCRE.note
				let selected_voice_data= JSON.parse(JSON.stringify(current_position_PMCRE.voice_data))
				//modify existing Note sequence
				// let vsb_iN=PMC_read_vsb_iN()
				let result={success:false}
				let data_change=false
				//modify existing Note sequence
				result = DATA_calculate_modify_object_position_A_note_delta(selected_voice_data,_PMC_drag_obj_position.segment_index,_PMC_drag_obj_position.pulse,_PMC_drag_obj_position.fraction,A_value,_PMC_drag_A_mod)
				data_change=result.data_change
				PMC_reset_drag_variables()
				if(!result.success || !data_change){
					//rewrite
					PMC_redraw_selected_voice()
					//possible no note to change
					return
				}
				if(result.outside_range){
					APP_info_msg("out_N_range")
				}
				//SAVE new voice
				DATA_change_voice_data_at_voice_id(selected_voice_data)
				return
			}
			if(_PMC_drag_iN){
				let d_iN=mouse_position_corrected.note - current_position_PMCRE.note
				let vsb_iN=PMC_read_vsb_iN()
				if(d_iN==0 && !vsb_iN.type=="iNgm"){//always try to save if iNgm, bc can change some diesis
					if(_PMC_drag_changed_voice){
						//reset previous state selected voice (is more secure)
						PMC_redraw_selected_voice()
						_PMC_drag_changed_voice=false
					}
					PMC_reset_drag_variables()
					return
				}
				let selected_voice_data= JSON.parse(JSON.stringify(current_position_PMCRE.voice_data))

				let result={success:true}
				let data_change=false//true if iNgm and d_iN=0 modify a single diesis
				if(vsb_iN.type=="iNgm"){
					result = DATA_calculate_modify_object_position_iNgrade_delta(selected_voice_data,_PMC_drag_obj_position.segment_index,_PMC_drag_obj_position.pulse,_PMC_drag_obj_position.fraction,d_iN)
					data_change=result.data_change
				}else{
					result = DATA_calculate_modify_object_position_iN_delta(selected_voice_data,_PMC_drag_obj_position.segment_index,_PMC_drag_obj_position.pulse,_PMC_drag_obj_position.fraction,d_iN)
					data_change=true
				}
				//simpler, not those functions ...
				//modify existing Note sequence
				PMC_reset_drag_variables()
				if(!result.success || !data_change){
					//rewrite
					PMC_redraw_selected_voice()
					PMC_reset_drag_variables()
					//possible no note to change
					//console.error("mouse up iN error calculations")
					return
				}
				if(result.outside_range){
					APP_info_msg("out_N_range")
				}
				//calculate need to be redrawn (multiple refresh on very little mouse move is bad)
				//SAVE new voice
				DATA_change_voice_data_at_voice_id(selected_voice_data)
			}else if(_PMC_drag_iT){
				let d_px_iT=(current_position_PMCRE.voice_data.neopulse.N/current_position_PMCRE.voice_data.neopulse.D)* (PMC_zoom_x*nuzic_block)
				//var new_iT=mouse_position_corrected.note - current_position_PMCRE.note
				let segment=current_position_PMCRE.voice_data.data.segment_data[current_position_PMCRE.segment_index]
				//new value
				//var d_iT= Math.round((mouse_position_corrected.X-current_position_PMCRE.PMC.X_start)/d_px_iT)
				//last viable value
				let d_iT= Math.round((mouse_position_corrected.X-current_position_PMCRE.PMC.X_start)/d_px_iT)
				if(_PMC_drag_iT_value_success!=null)d_iT=_PMC_drag_iT_value_success
				if(d_iT==0){
					if(_PMC_drag_changed_voice){
						//reset previous state selected voice (is more secure)
						PMC_redraw_selected_voice()
					}
					if(_PMC_drag_changed_all_voices){
						PMC_redraw_all_voices_svg()
					}
					PMC_reset_drag_variables()
					return
				}
				let selected_voice_data= JSON.parse(JSON.stringify(current_position_PMCRE.voice_data))
				//modify existing iT sequence
				//force=true if it<=0 iT=1, if iT>max_value iT=max_value
				let result = DATA_calculate_modify_object_position_iT_delta(selected_voice_data,_PMC_drag_obj_position.segment_index,_PMC_drag_obj_position.pulse,_PMC_drag_obj_position.fraction,d_iT,true)
				if(!result.success){
					if(_PMC_drag_changed_voice){
						PMC_redraw_selected_voice()
					}
					if(_PMC_drag_changed_all_voices){
						PMC_redraw_all_voices_svg()
					}
					PMC_reset_drag_variables()
					return
				}
				if(result.data!=null){
					//all data canged
					DATA_insert_new_state_point(result.data)
					DATA_load_state_point_data(false,false)
				} else {
					//changed only one voice
					DATA_change_voice_data_at_voice_id(selected_voice_data)
				}
			} else if(_PMC_drag_el){
				//drag note AND pulse start
				//NOTE
				//d_N generic interval (SINGLE note)
				let d_N=mouse_position_corrected.note - current_position_PMCRE.note
				let selected_voice_data=JSON.parse(JSON.stringify(current_position_PMCRE.voice_data))
				let result_N={success:false}
				let success_T=false
				if(d_N!=0){
					//modify existing Note sequence
					result_N = DATA_calculate_modify_object_position_N_delta(selected_voice_data,_PMC_drag_obj_position.segment_index,_PMC_drag_obj_position.pulse,_PMC_drag_obj_position.fraction,d_N)
				}
				//PULSE
				if((mouse_position_corrected.pulse!=_PMC_drag_obj_position.pulse ||
					mouse_position_corrected.fraction!=_PMC_drag_obj_position.fraction ||
					mouse_position_corrected.segment_index!=_PMC_drag_obj_position.segment_index )){
					let result_T = DATA_calculate_modify_object_position_T_delta_free(selected_voice_data,_PMC_drag_obj_position.segment_index,_PMC_drag_obj_position.pulse,_PMC_drag_obj_position.fraction,_PMC_drag_el_value.d_T.P,_PMC_drag_el_value.d_T.F)
					success_T=result_T.success
				}
				//dont care of outside range, it is corrected
				//if((result_N.success && result_N.outside_range!=true)|| success_T){
				if(result_N.success || success_T){
					//calculate need to be redrawn (multiple refresh on very little mouse move is bad)
					//changed only one voice
					DATA_change_voice_data_at_voice_id(selected_voice_data)
				}else{
					if(_PMC_drag_changed_voice){
						//reset previous state selected voice (is more secure)
						PMC_redraw_selected_voice()
					}
					PMC_reset_drag_variables()
					return
				}
			}
		}
		PMC_reset_drag_variables()
		return
	}
	if(mouse_position_corrected==null){
		return
	}

	let is_silence = currKey=="s" || currKey=="z"
	let is_element = currKey=="e" || currKey=="x"
	let is_tie_intervals = currKey=="l" || currKey=="c"
	let copy_current_position_PMCRE = JSON.parse(JSON.stringify(current_position_PMCRE))
	//change database
	if(mouse_position_corrected.X>current_position_PMCRE.PMC.X_start){
		let P_start = current_position_PMCRE.pulse
		let P_end = null
		let F_start = current_position_PMCRE.fraction
		let F_end = null
		let segment_data = current_position_PMCRE.voice_data.data.segment_data[current_position_PMCRE.segment_index]
		if(mouse_position_corrected.segment_index>current_position_PMCRE.segment_index){
			P_end = segment_data.time[segment_data.time.length-1].P
			F_end = 0
		}
		if(mouse_position_corrected.segment_index==current_position_PMCRE.segment_index){
			P_end = mouse_position_corrected.pulse
			F_end = mouse_position_corrected.fraction
		}
		if(P_start<P_end || F_start<F_end){
			//proceed
			//console.log(P_start+"."+F_start+"->"+P_end+"."+F_end)
			APP_stop()
			current_position_PMCRE = JSON.parse(JSON.stringify(copy_current_position_PMCRE))
			//console.log(P_start+" . "+F_start+"  "+P_end+" . "+F_end)
			//define note
			let note_to_insert=current_position_PMCRE.note
			let proceed=true
			if(is_silence){
				note_to_insert=-1
			}
			if(is_element){
				note_to_insert=-2
			}
			if(is_tie_intervals){
				//control if is possible to write note
				note_to_insert=-3
				if(P_start==0 && F_start==0)proceed=false
			}
			if(proceed){
				//write note
				DATA_calculate_overwrite_new_object_PA_PB(current_position_PMCRE.voice_data.voice_id,current_position_PMCRE.segment_index,P_start,F_start,P_end,F_end,note_to_insert,true,[],[])
				DATA_load_state_point_data(false,true)
			}
			//play note
			if(SNAV_read_play_new_note() && note_to_insert>-1)APP_play_this_note_number(note_to_insert,current_position_PMCRE.voice_data.instrument,current_position_PMCRE.voice_data.voice_id)
		}
	}
	//console.log(current_position_PMCRE) cleared
	// RE / PMC / progress position

	current_position_PMCRE = JSON.parse(JSON.stringify(copy_current_position_PMCRE))
	// RE / PMC / progress position
	let pulse_abs = (current_position_PMCRE.PMC.X_start-nuzic_block)/(PMC_zoom_x*nuzic_block)
	let time = (current_position_PMCRE.PMC.X_start-nuzic_block)/(PMC_zoom_x*nuzic_block)*60/PPM
	APP_player_to_time(time, pulse_abs , current_position_PMCRE.pulse,current_position_PMCRE.fraction,current_position_PMCRE.segment_index,current_position_PMCRE.voice_data.voice_id)

	//coherence...
	// var pulse_abs = (mouse_position_corrected.X-nuzic_block)/(PMC_zoom_x*nuzic_block)
	// APP_player_to_time(mouse_position_corrected.time, pulse_abs , mouse_position_corrected.pulse,mouse_position_corrected.fraction,current_position_PMCRE.segment_index,current_position_PMCRE.voice_data.voice_id)
	PMC_mouse_action_x.style.stroke=eval("nuzic_yellow")
	PMC_mouse_action_y.style.stroke=eval("nuzic_pink")
	//clear svg
	PMC_mouse_action_drag.style.fill="transparent"
	PMC_mouse_action_drag_element.style.fill="transparent"
	PMC_mouse_action_drag_element.style.stroke="transparent"
}

function PMC_mouse_wheel(event){
	//SWITCH VOICE IF POSSIBLE
	 if (event.ctrlKey) {
		//console.log("ctr key was pressed during the click");
		//event.preventDefault()
	}
	if (event.shiftKey) {
		//console.log("SHIFT key was pressed during the click "+ event.deltaY);
		event.preventDefault()
		let data = DATA_get_current_state_point(true)
		let current_index=null
		data.voice_data.forEach((voice,index)=>{
			if(voice.selected)current_index=index
			voice.selected=false
		})
		let next_index=null
		if(event.deltaY>0){
			//select,next,voice
			next_index=current_index+1
		}else{
			//select,prev,voice
			next_index=current_index-1
		}
		if(!isNaN(next_index) && next_index>=0 && next_index<data.voice_data.length){
			data.voice_data[next_index].selected=true
			DATA_insert_new_state_point(data)
			DATA_load_state_point_data(false,true)
		}
	}
}

function PMC_key_shortcut_mouse_wheel(event){
	//SWITCH VOICE IF POSSIBLE
	if (event.shiftKey && tabPMCbutton.checked && (event.key=="ArrowDown" || event.key=="ArrowUp")) {
		//console.log("SHIFT key was pressed during the click "+ event.deltaY);
		event.preventDefault()
		let data = DATA_get_current_state_point(true)
		let current_index=null
		data.voice_data.forEach((voice,index)=>{
			if(voice.selected)current_index=index
			voice.selected=false
		})
		let next_index=null
		if(event.key=="ArrowDown"){
			//select,next,voice
			next_index=current_index+1
		}
		if(event.key=="ArrowUp"){
			//select,prev,voice
			next_index=current_index-1
		}
		if(!isNaN(next_index) && next_index>=0 && next_index<data.voice_data.length){
			data.voice_data[next_index].selected=true
			DATA_insert_new_state_point(data)
			DATA_load_state_point_data(false,true)
		}
	}
}

//double click
function PMC_enter_new_element_pulse_note(canvas,event){
	//calc position
	APP_stop()
	let Y_pos=event.offsetY
	let X_pos=event.offsetX
	if(event.target!=canvas){
		console.log("click outside canvas")
		//console.log(event.target)
		//hover on div
		Y_pos += event.target.offsetTop
		X_pos += event.target.offsetLeft
		//return
	}
	let voice_data_mod = DATA_get_selected_voice_data()
	//calc voice index
	//calc note
	let note = Math.round((TET*N_reg-1)- (Y_pos-0.5*PMC_zoom_y*nuzic_block)/(PMC_zoom_y*nuzic_block))
	//console.log(note)

	//calc pulse,fraction and segment
	//var position = ((X_pos)/(PMC_zoom_x*nuzic_block))-1
	let position = ((X_pos-nuzic_block)/(PMC_zoom_x*nuzic_block))
	if(position<0)return
	if(position>Li)return

	let mouse_position_corrected = PMC_calculate_mouse_position(X_pos,Y_pos,current_position_PMCRE.voice_data)
	//add element to data
	DATA_enter_new_object(voice_data_mod.voice_id,mouse_position_corrected.segment_index,mouse_position_corrected.pulse,mouse_position_corrected.fraction,mouse_position_corrected.note,true,[],[])
	//play note
	if(SNAV_read_play_new_note())APP_play_this_note_number(mouse_position_corrected.note,voice_data_mod.instrument,voice_data_mod.voice_id)

	// RE / PMC / progress position
	let pulse_abs = (mouse_position_corrected.X-nuzic_block)/(PMC_zoom_x*nuzic_block)
	APP_player_to_time(mouse_position_corrected.time, pulse_abs , mouse_position_corrected.pulse , mouse_position_corrected.fraction, mouse_position_corrected.segment_index,voice_data_mod.voice_id)
}

function PMC_draw_progress_line_time(time){
	//if (tabPMCbutton.checked){
	//PMC
	let pulsation = time*PPM/60
	let position = pulsation*nuzic_block*PMC_zoom_x+nuzic_block
	PMC_draw_progress_line_position(position)
	current_position_PMCRE.PMC.X_start=position
}

function PMC_draw_progress_line_position(position){
	//SVG
	//PMC_progress_line.style.stroke=nuzic_dark
	//PMC_progress_line.style.stroke="violet"
	//console.error(position)
	PMC_progress_line.style.stroke=nuzic_blue
	PMC_progress_line.setAttribute('x1',position-0.5)
	PMC_progress_line.setAttribute('y1','0')
	PMC_progress_line.setAttribute('x2',position-0.5)
	PMC_progress_line.setAttribute('y2','100%')
}

function PMC_reset_progress_line(){
	let [now,start,end,max]=APP_return_progress_bar_values()
	let width_pulse = max + 4.5
	//SVG
	let note_lines = TET*N_reg-0.5
	let height_string = 'calc('+note_lines+' * var(--PMC_y_block) + var(--RE_block))'
	let width_string = "calc(var(--PMC_x_block) * "+width_pulse+" + var(--RE_block))"
	PMC_progress_svg.style.height=height_string
	PMC_progress_svg.style.width=width_string
	//clear line
	PMC_progress_line.style.stroke="transparent"
	if(current_position_PMCRE!=null)current_position_PMCRE.PMC.X_start = null
	//PMC_reset_action_pmc()
}

function PMC_reset_playing_notes(){
	//SVG
	let [now,start,end,max]=APP_return_progress_bar_values()
	let width_pulse = max + 4.5
	//SVG
	let note_lines= TET*N_reg-0.5
	let height_string = 'calc('+note_lines+' * var(--PMC_y_block) + var(--RE_block))'
	let width_string = "calc(var(--PMC_x_block) * "+width_pulse+" + var(--RE_block))"
	PMC_playing_notes_svg.style.height=height_string
	PMC_playing_notes_svg.style.width=width_string
	PMC_clear_playing_notes()
}

function PMC_clear_playing_notes(){
	//clear
	PMC_playing_notes_svg.innerHTML="" //this break everything
	// var line_array= [...PMC_playing_notes_svg.querySelectorAll("line")]
	// if(line_array.length==0)return
	// line_array.forEach(line=>{
	// 	PMC_playing_notes_svg.removeChild(line)
	// })
	// var rect_array= [...PMC_playing_notes_svg.querySelectorAll("rect")]
	// if(rect_array.length==0)return
	// rect_array.forEach(rect=>{
	// 	PMC_playing_notes_svg.removeChild(rect)
	// })
}

async function PMC_add_note_to_playingNoteArr(note_data){
	let x1 = note_data.pulsation*nuzic_block*PMC_zoom_x+nuzic_block
	let x2 = x1+Math.round(note_data.duration*nuzic_block*PMC_zoom_x)
	let note_number = TET*N_reg
	let delta_y = nuzic_block * PMC_zoom_y
	//note_data.note is not the position

	let svg_string=`<g id="${'event'+note_data.note_id}">`
	note_data.note.forEach((note,index)=>{
		let y = delta_y*(note_number-note_data.y_position[index]-0.5)
		if(note==-2){
			svg_string +=`
				<rect x="${x1}" y="${y-(delta_y/2)+1}" width="${x2-x1}" height="${delta_y-2}" fill="var(--Nuzic_white)" stroke="${eval(note_data.color)}" stroke-width="2"></rect>
			`
		}else{
			svg_string+=`
				<line x1="${x1}" y1="${y}" x2="${x2}" y2="${y}" stroke="${eval(note_data.color)}" stroke-width="${delta_y}"></line>
			`
		}
	})
	svg_string +='</g>'
	PMC_playing_notes_svg.innerHTML+=svg_string
}

async function PMC_remove_note_from_playingNotesArr(note){
	let line= PMC_playing_notes_svg.querySelector("#event"+note.note_id)
	if(line!= null)PMC_playing_notes_svg.removeChild(line)
}

function PMC_calculate_best_scrollbar_position(time){
	if (tabPMCbutton.checked){
		let overflow_w = PMC_svg_container.clientWidth
		let position = nuzic_block + time/60 * PPM * (nuzic_block*PMC_zoom_x)
		if(time<0.5)position=0
		let overflow_start_position = PMC_svg_container.scrollLeft
		let overflow_end_position = overflow_start_position+overflow_w
		if(position<overflow_start_position || position>(overflow_end_position-nuzic_block*PMC_zoom_x)){
			//move scrollbar to position
			PMC_main_scrollbar_H.scrollLeft=position
		}
	}
}

//play App_PMC_selected_voice_object_N and App_PMC_selected_voice_sequence_object note
function PMC_play_this_note_number(nzc_note, instrument_index,voice_id){
	if(SNAV_read_note_on_click() == true){
		APP_play_this_note_number(nzc_note , instrument_index,voice_id)
	}
}

