function APP_show_operation_dialog_box(type){
	let background = document.getElementById("App_OP_dialog_box_background")
	let option_box_list = [...background.querySelectorAll(".App_OP_option_box")]
	let option_box_string=""
	//PS attention to difference BW voice_number/index(SHOWN) and voice_id(DATABASE)
	let options = JSON.parse(JSON.stringify(APP_selection_options))
	options.type=type
	switch (options.working_space) {
		case "RE":

		break;
		case "PMC":

		break;
	}

	switch (options.target) {
		case "voice":
			option_box_string=`[v${options.voice_number}]`
		break;
		case "segment":
			option_box_string=`[v${options.voice_number}s${options.segment_index}]`
		break;
		case "column":
			option_box_string=`[c${options.segment_index}]`
		break;
	}
	option_box_list.forEach(box=>{
		box.innerHTML=option_box_string
	})

	let div_repetitions = background.querySelector(".App_OP_dialog_repetitions")
	let div_operations = background.querySelector(".App_OP_dialog_operations")
	let div_paste = background.querySelector(".App_OP_dialog_paste")
	background.style.display = 'flex'
	let data_info = JSON.stringify(options)
	background.setAttribute("data_info",data_info)
	switch (options.type) {
		case "operations":
			div_repetitions.style="display:none"
			div_operations.style="display:flex"
			div_paste.style="display:none"
			APP_change_operation()
			APP_verify_apply_operation()
		break;
		case "repetitions":
			div_repetitions.style="display:flex"
			div_operations.style="display:none"
			div_paste.style="display:none"
			//APP_change_operation()
			APP_verify_apply_operation()
		break;
		case "paste":
			div_repetitions.style="display:none"
			div_operations.style="display:none"
			div_paste.style="display:flex"
			//select first
			div_paste.querySelector("#App_paste_NaT").checked=true
			//let apply_buttons = [...background.querySelectorAll(".App_OP_apply")]
			APP_verify_apply_operation()
		break;
	}
}

function APP_hide_operation_dialog_box(){
	//hide div
	let background = document.getElementById('App_OP_dialog_box_background')
	background.style.display = 'none'

	//clear info operation in div
	//reset RE or PMC selection
	if (tabPMCbutton.checked){
		PMC_reset_selection()
	}else{
		RE_reset_selection()
	}
}

function APP_reset_selection_global_variable(){
	//clear info operation in global variable
	APP_selection_options={	"target": null,
							"working_space": null,
							"voice_number": null,
							"voice_id": null,
							"segment_index": null
	}
}

function APP_change_operation(){
	let dialog_box= document.querySelector(".App_OP_dialog_box")
	let div_operations= dialog_box.querySelector(".App_OP_dialog_operations")
	let container_type = dialog_box.querySelector(".App_OP_container_type")
	let container_operation = dialog_box.querySelector(".App_OP_container_operation")
	//var type_button_list = [...container_type.querySelectorAll("input:checked")]
	let type_button = container_type.querySelector("input:checked")

	//activate/deactivate buttons
	//make sure all buttons are activated
	let op_button_list= [...container_operation.querySelectorAll("input")]
	op_button_list.forEach(input=>{input.disabled=false})
	//make sure all options are hidden
	let op_option_container_list= [...div_operations.querySelectorAll(".App_OP_options")]
	op_option_container_list.forEach(container=>{container.style="display:none"})

	if(type_button.value=="iT"){
		//deactivate last operation
		let v_mirror_button = container_operation.querySelector("#App_OP_v_mirror")
		if(v_mirror_button.checked==true){
			let sum_button=container_operation.querySelector("#App_OP_sum")
			sum_button.checked=true
		}
		v_mirror_button.disabled=true
	}else if(type_button.value=="T"){
		//deactivate everything
		let op_button_list= [...container_operation.querySelectorAll("input")]
		op_button_list.forEach(input=>{input.disabled=true})
		//deactivate ok button
		let OP_apply = div_operations.querySelector(".App_OP_apply")
		OP_apply.disabled=true
		let OP_title = div_operations.querySelector(".App_OP_title")
		OP_title.innerHTML="NO OPERATION"
		return
	}

	//change current operation title
	let OP_title = div_operations.querySelector(".App_OP_title")
	let operation_button = container_operation.querySelector("input:checked")
	if(operation_button==null){
		//select the first
		operation_button=container_operation.querySelector("input")
		operation_button.checked=true
	}
	//maybe not needed
	let background = document.getElementById("App_OP_dialog_box_background")
	let options = JSON.parse(background.getAttribute("data_info"))
	let app_text_list = lang_localization_list.find(lang=>{return lang.current}).text

	switch (operation_button.value) {
		case "sum":
			OP_title.innerHTML=APP_language_return_text_id("oper_sum")
			OP_title.value="sum"
			var op_option_container= div_operations.querySelector(".App_OP_options_sum")
			op_option_container.style="display:block"
			var op_lines_list = [...op_option_container.querySelectorAll(".App_OP_option_line")]
			if(type_button.value=="iT"){
				_change_current_operation_options(op_lines_list,[false,true,true,true])
				//add correct text controller
				var inp_box = op_lines_list[1].querySelector(".App_OP_inp_box")
				inp_box.setAttribute('onkeypress',"APP_inpTest_op_int(event,this)")
				inp_box.value=""
			}
			if(type_button.value=="iN" || type_button.value=="iNgm"){
				_change_current_operation_options(op_lines_list,[true,true,false,false])
				//add correct text controller
				var inp_box = op_lines_list[1].querySelector(".App_OP_inp_box")
				inp_box.setAttribute('onkeypress',"APP_inpTest_op_intMod(event,this)")
			}
			if(type_button.value=="N" || type_button.value=="Ngm"){
				_change_current_operation_options(op_lines_list,[false,true,false,false])
				//add correct text controller
				var inp_box = op_lines_list[1].querySelector(".App_OP_inp_box")
				inp_box.setAttribute('onkeypress',"APP_inpTest_op_intMod(event,this)")
			}
		break;
		case "rot":
			OP_title.innerHTML=APP_language_return_text_id("oper_rot")
			OP_title.value="rot"
			var op_option_container= div_operations.querySelector(".App_OP_options_rot")
			op_option_container.style="display:block"
			var op_lines_list = [...op_option_container.querySelectorAll(".App_OP_option_line")]
			if(type_button.value=="iT"){
				_change_current_operation_options(op_lines_list,[true,true,false,false,true])
			}
			if(type_button.value=="iN" || type_button.value=="iNgm"){
				_change_current_operation_options(op_lines_list,[true,false,true,options.target=="voice",false])
			}
			if(type_button.value=="N" || type_button.value=="Ngm"){
				_change_current_operation_options(op_lines_list,[true,false,true,options.target=="voice",false])
			}
		break;
		case "h_mirror":
			OP_title.innerHTML=APP_language_return_text_id("oper_h_mirror")
			OP_title.value="h_mirror"
			var op_option_container= div_operations.querySelector(".App_OP_options_h_mirror")
			op_option_container.style="display:block"
			var op_lines_list = [...op_option_container.querySelectorAll(".App_OP_option_line")]
			if(type_button.value=="iT"){
				_change_current_operation_options(op_lines_list,[true,true,false,options.target=="voice",true])
			}
			if(type_button.value=="iN" || type_button.value=="iNgm"){
				_change_current_operation_options(op_lines_list,[true,false,true,options.target=="voice",false])
			}
			if(type_button.value=="N" || type_button.value=="Ngm"){
				_change_current_operation_options(op_lines_list,[true,false,true,options.target=="voice",false])
			}
		break;
		case "v_mirror":
			OP_title.innerHTML=APP_language_return_text_id("oper_v_mirror")
			OP_title.value="v_mirror"
			var op_option_container= div_operations.querySelector(".App_OP_options_v_mirror")
			op_option_container.style="display:block"
			var op_lines_list = [...op_option_container.querySelectorAll(".App_OP_option_line")]
			if(type_button.value=="iN" || type_button.value=="iNgm"){_change_current_operation_options(op_lines_list,[false,false,true])}
			if(type_button.value=="N" || type_button.value=="Ngm"){_change_current_operation_options(op_lines_list,[true,true,true])}
		break;
	}

	//verify if ok button is good to go
	APP_verify_apply_operation()

	//change current operation options
	function _change_current_operation_options(op_line_list,active_array,menu){
		//array of true/false
		op_line_list.forEach((line,index)=>{
			if(active_array[index]==true){
				line.style="display:flex"
			}else{
				line.style="display:none"
			}
		})
	}

	// function APP_language_return_text_id(id){
	// 	let found = app_text_list.find(item=>{
	// 		return item.id==id
	// 	})
	// 	if(found==null){
			//console.log(id)
	// 		return id
	// 	}
	// 	return found.text
	// }
}

//OPERATIONS

function APP_apply_operation_paste(){
	let selector=document.querySelector(".App_OP_dialog_paste .App_OP_container_paste input:checked")
	if(selector!=null){
		let id=selector.value
		//column or segment
		var background = document.getElementById("App_OP_dialog_box_background")
		var options = JSON.parse(background.getAttribute("data_info"))
		//console.log(options)
		if(options.target=="segment"){
			//segment
			let segment_index=options.segment_index
			let voice_id=options.voice_id
			switch (id) {
				case "all":
					DATA_paste_segment_all(voice_id,segment_index)
					break;
				case "all_g":
					DATA_paste_segment_all_grade(voice_id,segment_index)
					break;
				case "Na":
					DATA_paste_segment_note(voice_id,segment_index)
					break;
				case "Ng":
					DATA_paste_segment_grade(voice_id,segment_index)
					break;
				case "time":
					DATA_paste_segment_time(voice_id,segment_index)
					break;
				case "fraction":
					DATA_paste_segment_fraction(voice_id,segment_index)
					break;
			}
		}else{
			let segment_index=options.segment_index
			switch (id) {
				case "all":
					DATA_paste_column_segment_all(segment_index)
					break;
				case "all_g":
					DATA_paste_segment_all_grade(null,segment_index)
					break;
				case "Na":
					DATA_paste_segment_note(null,segment_index)
					break;
				case "Ng":
					DATA_paste_segment_grade(null,segment_index)
					break;
				case "time":
					DATA_paste_segment_time(null,segment_index)
					break;
				case "fraction":
					DATA_paste_segment_fraction(null,segment_index)
					break;
			}
		}
	}
	//close operation box
	APP_hide_operation_dialog_box()
}

function APP_verify_apply_operation(){
	//determine what operation is active
	let background = document.getElementById("App_OP_dialog_box_background")
	let options = JSON.parse(background.getAttribute("data_info"))//XXX XXX XXX XXX XXX
	let container_type = background.querySelector(".App_OP_container_type")
	let container_operation = background.querySelector(".App_OP_container_operation")
	let type_button = container_type.querySelector("input:checked")
	let operation_button = container_operation.querySelector("input:checked")

	let apply_buttons = [...background.querySelectorAll(".App_OP_apply")]

	let success=false
	_disable()
	switch (options.type) {
		case "operations":
			//find inp_box
			let div_operations = background.querySelector(".App_OP_dialog_operations")
			switch (operation_button.value) {
				case "sum":
					var op_option_container= div_operations.querySelector(".App_OP_options_sum")
					var op_lines_list = [...op_option_container.querySelectorAll(".App_OP_option_line")]
					if(type_button.value=="iT"){
						//warning always present
						//if(op_lines_list[2].querySelector("#App_OP_block_Ls").checked){
						//	op_lines_list[3].style="display:flex"
						//}else{
						//	op_lines_list[3].style="display:none"
						//}
						var delta_value = op_lines_list[1].querySelector(".App_OP_inp_box").value
						if(parseInt(delta_value)!=0 && delta_value!="" && delta_value!="-")success=true
					}else{
						success=_check_note(op_lines_list[1].querySelector(".App_OP_inp_box"),false)
					}
					// if(type_button.value=="iN" || type_button.value=="iNgm"){}
					// if(type_button.value=="N" || type_button.value=="Ngm"){}
					//same for everyone
				break;
				case "rot":
					var op_option_container= div_operations.querySelector(".App_OP_options_rot")
					var op_lines_list = [...op_option_container.querySelectorAll(".App_OP_option_line")]
					if(type_button.value=="iT"){
						//warning visibility if multiple range selected
						let multiple_Fr_ranges=false
						//rotation voice global
						let data = DATA_get_current_state_point()
						if(options.target=="voice"){
							//search if voice contains multiple segments
							//find voice
							let voice_data = data.voice_data.find(item=>{
								return item.voice_id==options.voice_id
							})
							multiple_Fr_ranges=(voice_data.data.segment_data.length==1)?false:true
							if(!multiple_Fr_ranges){
								//verify if single segment is single fracc range
								multiple_Fr_ranges=(voice_data.data.segment_data[0].fraction.length==1)?false:true
							}
						}
						if(options.target=="column"){
							//search if column contains multiple segments
							let blocked_voices=0
							let multiple_Fr_ranges = data.voice_data.some(item=>{
								if(item.blocked)blocked_voices++
								return blocked_voices>1
							})
							if(!multiple_Fr_ranges){
								//verify if single segment is single fracc range
								let voice_data = data.voice_data.find(item=>{
									return item.blocked
								})
								multiple_Fr_ranges=(voice_data.data.segment_data[options.segment_index].fraction.length==1)?false:true
							}

						}
						if(options.target=="segment"){
							//search if segment contains multiple segments
							let voice_data = data.voice_data.find(item=>{
								return item.voice_id==options.voice_id
							})
							let segment_data=voice_data.data.segment_data.find((item,index)=>{
								return index==options.segment_index
							})
							multiple_Fr_ranges=(segment_data.fraction.length==1)?false:true
						}

						if(multiple_Fr_ranges){
							op_lines_list[4].style="display:flex"
						}else{
							op_lines_list[4].style="display:none"
						}
					}
					//same for everyone
					var delta_value = op_lines_list[0].querySelector(".App_OP_inp_box").value
					if(parseInt(delta_value)!=0 && delta_value!="" && delta_value!="-")success=true
				break;
				case "h_mirror":
					if(type_button.value=="iT"){
						var op_option_container= div_operations.querySelector(".App_OP_options_h_mirror")
						var op_lines_list = [...op_option_container.querySelectorAll(".App_OP_option_line")]
						//voice global
						if(options.target=="voice" && !op_lines_list[3].querySelector("#App_OP_voice_h_mirror").checked){
							//warning disconect
							op_lines_list[4].style="display:flex"
						}else{
							op_lines_list[4].style="display:none"
						}
					}
					//same for everyone
					success=true
				break;
				case "v_mirror":
					var op_option_container= div_operations.querySelector(".App_OP_options_v_mirror")
					var op_lines_list = [...op_option_container.querySelectorAll(".App_OP_option_line")]
					if(type_button.value=="N" || type_button.value=="Ngm"){
						//second line depend on first value
						let dropdown_value=op_lines_list[0].querySelector(".App_v_mirror_axis").value

						if(dropdown_value=="user_note"){
							op_lines_list[1].style="display:flex"
							op_lines_list[2].style="display:none"

							//check value
							success=_check_note(op_lines_list[1].querySelector(".App_OP_inp_box"),true)
						}else{
							op_lines_list[1].style="display:none"
							op_lines_list[2].style="display:flex"
							success=true
						}
					}
					if(type_button.value=="iN" || type_button.value=="iNgm")success=true
				break;
			}
		break;
		case "repetitions":
			//find inp_box
			let div_repetitions = background.querySelector(".App_OP_dialog_repetitions")
			let N_rep_inp_box = div_repetitions.querySelector(".App_OP_inp_box")
			let N_rep = parseInt(N_rep_inp_box.value)
			if(N_rep>0){
				success=true
			}

			let type_repetition = div_repetitions.querySelector("select").value
			let warning_div = div_repetitions.querySelector(".App_OP_option_line:last-child")
			if(type_repetition=="insert"){
				warning_div.style="display:none"
			}else{
				warning_div.style="display:flex"
			}
		break;
		case "paste":
			success=true
		break;
	}

	if(success)_enable()

	function _disable(){
		apply_buttons.forEach(button=>{button.disabled=true})
	}

	function _enable(){
		apply_buttons.forEach(button=>{button.disabled=false})
	}

	function _check_note(input,only_positive){
		//only_positive==false -> works for negative too
		let string_value=input.value
		if(string_value!=""){
			let Na_value=null
			let positive = 1
			if(string_value[0]=="-"){
				if(only_positive){
					return false
				}else{
					positive=-1
					string_value=string_value.slice(1)
					if(string_value=="")return false
				}
			}
			if(string_value.includes("r")){
				// Mod
				let split = string_value.split('r')
				let resto = Math.floor(split[0])
				//if no module r 3
				let reg=-10
				if(split[1]>=0 && split[1]!=""){
					reg=Math.floor(split[1])
					Na_value = resto + reg*TET
				}else{
					return false
				}
			}else{
				Na_value=parseInt(string_value)
			}
			Na_value=Na_value*positive
			if(!(isNaN(Na_value) || Na_value==null)){
				//pleonastico
				let max_note = TET*N_reg -1
				if(Na_value>0 && Na_value<max_note && only_positive){
					return true
				}
				if(Na_value>-max_note && Na_value<max_note && !only_positive){
					return true
				}
			}
		}
		return false
	}
}

function APP_apply_operation(){
	//determine what operation is active
	let background = document.getElementById("App_OP_dialog_box_background")
	let options = JSON.parse(background.getAttribute("data_info"))//XXX XXX XXX XXX XXX
	let container_type = background.querySelector(".App_OP_container_type")
	let container_operation = background.querySelector(".App_OP_container_operation")
	let type_button = container_type.querySelector("input:checked").value
	let operation_button = container_operation.querySelector("input:checked")
	let use_mod=DATA_get_scale_sequence().length==0
	if(use_mod && type_button=="Ngm")type_button="N"
	if(use_mod && type_button=="iNgm")type_button="iN"
	if(use_mod && type_button=="Ngm")type_button="N"
	if(use_mod && type_button=="Ngm")type_button="N"
	//do the operation
	switch (options.type) {
		case "operations":
			//find inp_box
			let div_operations = background.querySelector(".App_OP_dialog_operations")
			let voice_id=-1
			let segment_index =-1
			if(options.target=="segment"){
				voice_id=options.voice_id
				segment_index=options.segment_index
			}
			if(options.target=="column"){
				segment_index=options.segment_index
			}
			if(options.target=="voice"){
				voice_id=options.voice_id
			}

			switch (operation_button.value) {
				case "sum":
					var op_option_container= div_operations.querySelector(".App_OP_options_sum")
					var op_lines_list = [...op_option_container.querySelectorAll(".App_OP_option_line")]
					if(type_button=="iT"){
						let value = parseInt(op_lines_list[1].querySelector(".App_OP_inp_box").value)
						let block_Ls = op_lines_list[2].querySelector("#App_OP_block_Ls").checked
						if(options.target=="segment")DATA_sum_iT_segment(voice_id,segment_index,value,block_Ls)
						if(options.target=="column")DATA_sum_iT_segment_column(segment_index,value,block_Ls)
						if(options.target=="voice")DATA_sum_iT_voice(voice_id,value,block_Ls)
					}
					if(type_button=="N" || type_button=="Ngm"){
						let value = null
						let type_N = null
						if(type_button=="N"){
							value=_value_note(op_lines_list[1].querySelector(".App_OP_inp_box"),false)
							type_N="Na"
						}
						if(type_button=="Ngm"){
							//grade
							value=_value_note_grade(op_lines_list[1].querySelector(".App_OP_inp_box"),false)
							type_N="Ngm"
						}
						if(value==null){
							console.error("Sum note not valid")
							return
						}
						if(options.target=="segment")DATA_sum_N_segment(voice_id,segment_index,value,type_N)
						if(options.target=="column")DATA_sum_N_segment_column(segment_index,value,type_N)
						if(options.target=="voice")DATA_sum_N_voice(voice_id,value,type_N)
					}
					if(type_button=="iN" || type_button=="iNgm"){
						let value = null
						let type_N = null
						if(type_button=="iN"){
							value=_value_note(op_lines_list[1].querySelector(".App_OP_inp_box"),false)
							type_N="Na"
						}
						if(type_button=="iNgm"){
							//grade
							value=_value_note_grade(op_lines_list[1].querySelector(".App_OP_inp_box"),false)
							type_N="Ngm"
						}
						if(value==null){
							console.error("Sum note not valid")
							return
						}
						let type_sum = op_lines_list[0].querySelector("select").value
						if(options.target=="segment")DATA_sum_iN_segment(voice_id,segment_index,value,type_sum,type_N)
						if(options.target=="column")DATA_sum_iN_segment_column(segment_index,value,type_sum,type_N)
						if(options.target=="voice")DATA_sum_iN_voice(voice_id,value,type_sum,type_N)
					}
				break;
				case "rot":
					var op_option_container= div_operations.querySelector(".App_OP_options_rot")
					var op_lines_list = [...op_option_container.querySelectorAll(".App_OP_option_line")]
					let value=parseInt(op_lines_list[0].querySelector(".App_OP_inp_box").value)
					if(isNaN(value) || value==null){
						console.error("Rot value not valid")
						return
					}
					var by_segment=op_lines_list[3].querySelector("#App_OP_voice_rot").checked
					let rot_se=op_lines_list[2].querySelector("#App_OP_rot_se").checked
					if(type_button=="iT"){
						let rot_N=op_lines_list[1].querySelector("#App_OP_rot_N").checked
						if(rot_N){
							if(options.target=="segment")DATA_rotate_element_segment(voice_id,segment_index,value)
							if(options.target=="column")DATA_rotate_element_segment_column(segment_index,value)
							if(options.target=="voice")DATA_rotate_element_voice(voice_id,value)
						}else{
							if(options.target=="segment")DATA_rotate_iT_segment(voice_id,segment_index,value)
							if(options.target=="column")DATA_rotate_iT_segment_column(segment_index,value)
							if(options.target=="voice")DATA_rotate_iT_voice(voice_id,value)
						}
					}
					if(type_button=="N" || type_button=="Ngm"){
						let type_N = "Na"
						if(type_button=="Ngm")type_N = "Ngm"
						if(options.target=="segment")DATA_rotate_N_segment(voice_id,segment_index,value,rot_se,type_N)
						if(options.target=="column")DATA_rotate_N_segment_column(segment_index,value,rot_se,type_N)
						if(options.target=="voice")DATA_rotate_N_voice(voice_id,value,rot_se,by_segment,type_N)
					}
					if(type_button=="iN" || type_button=="iNgm"){
						let type_N = "Na"
						if(type_button=="iNgm")type_N = "Ngm"
						if(options.target=="segment")DATA_rotate_iN_segment(voice_id,segment_index,value,rot_se,type_N)
						if(options.target=="column")DATA_rotate_iN_segment_column(segment_index,value,rot_se,type_N)
						if(options.target=="voice")DATA_rotate_iN_voice(voice_id,value,rot_se,by_segment,type_N)
					}
				break;
				case "v_mirror":
					var op_option_container= div_operations.querySelector(".App_OP_options_v_mirror")
					var op_lines_list = [...op_option_container.querySelectorAll(".App_OP_option_line")]
					//intervals has no options
					let max_note = TET*N_reg -1
					if(type_button=="N" || type_button=="Ngm"){
						var values={axis_type:op_lines_list[0].querySelector(".App_v_mirror_axis").value}
						if(values.axis_type=="user_note"){
							let Na_value = null
							if(type_button=="N")Na_value=_value_note(op_lines_list[1].querySelector(".App_OP_inp_box"),true)
							if(type_button=="Ngm")Na_value=_value_note_grade(op_lines_list[1].querySelector(".App_OP_inp_box"),true)
							if(Na_value==null){
								console.error("Axis note not valid")
								return
							}
							//extra control not really needed
							if(type_button=="N" && (Na_value<=0 || Na_value>=max_note)){
								console.error("Axis note not valid")
								return
							}
							values.value=Na_value
						}
						let type_N = "Na"
						if(type_button=="Ngm")type_N = "Ngm"
						if(options.target=="segment")DATA_v_mirror_N_segment(voice_id,segment_index,values,type_N)
						if(options.target=="column")DATA_v_mirror_N_segment_column(segment_index,values,type_N)
						if(options.target=="voice")DATA_v_mirror_N_voice(voice_id,values,type_N)
					}
					if(type_button=="iN" || type_button=="iNgm"){
						let type_N = "Na"
						if(type_button=="iNgm")type_N = "Ngm"
						if(options.target=="segment")DATA_v_mirror_iN_segment(voice_id,segment_index,type_N)
						if(options.target=="column")DATA_v_mirror_iN_segment_column(segment_index,type_N)
						if(options.target=="voice")DATA_v_mirror_iN_voice(voice_id,type_N)
					}
				break;
				case "h_mirror":
					var op_option_container= div_operations.querySelector(".App_OP_options_h_mirror")
					var op_lines_list = [...op_option_container.querySelectorAll(".App_OP_option_line")]
					var by_segment=op_lines_list[3].querySelector("#App_OP_voice_h_mirror").checked
					let mirror_se=op_lines_list[2].querySelector("#App_OP_h_mirror_se").checked
					if(type_button=="iT"){
						if(op_lines_list[1].querySelector("#App_OP_h_mirror_N").checked){
							if(options.target=="segment")DATA_h_mirror_element_segment(voice_id,segment_index)
							if(options.target=="column")DATA_h_mirror_element_segment_column(segment_index)
							if(options.target=="voice")DATA_h_mirror_element_voice(voice_id,by_segment)
						}else{
							if(options.target=="segment")DATA_h_mirror_iT_segment(voice_id,segment_index)
							if(options.target=="column")DATA_h_mirror_iT_segment_column(segment_index)
							if(options.target=="voice")DATA_h_mirror_iT_voice(voice_id,by_segment)
						}
					}
					if(type_button=="N" || type_button=="Ngm"){
						let type_N = "Na"
						if(type_button=="Ngm")type_N = "Ngm"
						if(options.target=="segment")DATA_h_mirror_N_segment(voice_id,segment_index,mirror_se,type_N)
						if(options.target=="column")DATA_h_mirror_N_segment_column(segment_index,mirror_se,type_N)
						if(options.target=="voice")DATA_h_mirror_N_voice(voice_id,mirror_se,by_segment,type_N)
					}
					if(type_button=="iN" || type_button=="iNgm"){
						let type_N = "Na"
						if(type_button=="iNgm")type_N = "Ngm"
						if(options.target=="segment")DATA_h_mirror_iN_segment(voice_id,segment_index,mirror_se,type_N)
						if(options.target=="column")DATA_h_mirror_iN_segment_column(segment_index,mirror_se,type_N)
						if(options.target=="voice")DATA_h_mirror_iN_voice(voice_id,mirror_se,by_segment,type_N)
					}
				break;
			}
		break;
		case "repetitions":
			//find inp_box
			let div_repetitions = background.querySelector(".App_OP_dialog_repetitions")
			let N_rep_inp_box = div_repetitions.querySelector(".App_OP_inp_box")
			let N_rep = parseInt(N_rep_inp_box.value)
			let type_repetition = div_repetitions.querySelector("select").value
			if(N_rep>0){
				let insert = false
				if(type_repetition=="insert")insert=true
				if(options.target=="voice") DATA_repeat_voice(options.voice_id,N_rep,insert)
				if(options.target=="segment") DATA_repeat_segment(options.voice_id,options.segment_index,N_rep,insert)
				if(options.target=="column") DATA_repeat_segment_column(options.segment_index,N_rep,insert)
			}
		break;
	}

	//close operation box
	APP_hide_operation_dialog_box()

	function _value_note(input,only_positive){
		//only_positive==false -> works for negative too
		let string_value=input.value
		if(string_value!=""){
			let Na_value=null
			let positive = 1
			if(string_value[0]=="-"){
				if(only_positive){
					return null
				}else{
					positive=-1
					string_value=string_value.slice(1)
					if(string_value=="")return null
				}
			}
			if(string_value.includes("r")){
				// Mod
				let split = string_value.split('r')
				let resto = Math.floor(split[0])
				//if no module r 3
				let reg=-10
				if(split[1]>=0 && split[1]!=""){
					reg=Math.floor(split[1])
					Na_value = resto + reg*TET
				}else{
					return null
				}
			}else{
				Na_value=parseInt(string_value)
			}
			Na_value=Na_value*positive
			if(!(isNaN(Na_value) || Na_value==null)){
				//pleonastico
				let max_note = TET*N_reg -1
				if(Na_value>0 && Na_value<max_note && only_positive){
					return Na_value
				}
				if(Na_value>-max_note && Na_value<max_note && !only_positive){
					return Na_value
				}
			}
		}
		return null
	}

	function _value_note_grade(input,only_positive){
		//only_positive==false -> works for negative too
		let string_value=input.value
		if(string_value!=""){
			let grade=null
			let diesis=0 //not in use
			let reg=0
			let positive = 1
			if(string_value[0]=="-"){
				if(only_positive){
					return null
				}else{
					positive=-1
					string_value=string_value.slice(1)
					if(string_value=="")return null
				}
			}
			if(string_value.includes("r")){
				// Mod
				let split = string_value.split('r')
				let resto = Math.floor(split[0])
				if(split[1]>=0 && split[1]!=""){
					reg=Math.floor(split[1])
					grade = resto
				}else{
					//if no module r 3?
					return null
				}
			}else{
				grade=parseInt(string_value)
			}

			if(!(isNaN(grade) || grade==null)){
				//no control outside min max note (pos or neg)
				return {positive:positive,grade:grade,diesis:diesis,reg:reg}
			}
		}
		return null
	}
}

