//Global Variables
//*
const DEBUG_MODE=false
/*/
const DEBUG_MODE=true
let DEBUG_MODE_EXTRA=false
//*/
window.global_nuzic={}
let is_App=!(document.querySelector(".App_Nav_timer_loader")==null)
let bug_data=[]
//SoundFont manager
var SF_manager

var PPM = 120
var Li = 60
const max_Li=9999
var version= "v2.2.6b"
var TET = 12
const N_reg = 8
var mute_A=false
const generic_file_name = "Sin titulo"
var current_file_name = generic_file_name
var current_language = "ES"
var lang_localization_list=[]
var app_text_list=[]
const extension = ".nzc"
const extension_old = ".pmc"
const outputPMC_Li = document.getElementById("PMC_Li")
const outputPMC_PPM= document.getElementById('PMC_PPM')
const outputPMC_Rg =document.getElementById('PMC_Rg')
const outputPMC_TET =document.getElementById('PMC_TET')
const outputPMC_info = document.getElementById("App_PMC_info")
const output_timer_total = document.getElementById("timer_total")
const output_timer_partial_min = document.getElementById('timer_partial_min')
const output_timer_partial_sec = document.getElementById('timer_partial_sec')
const outputLoop = document.getElementById("Nav_loop")
const outputMainMetronome = document.getElementById("Nav_main_metronome")
const outputMainMetronomeVolume = document.getElementById("App_BNav_V_master_metronome_volume")
const outputMasterVolume = document.getElementById("App_BNav_V_master_volume")
const outputMasterPan = document.getElementById("App_BNav_V_master_pan")
const tabREbutton = document.getElementById("App_BNav_tab_RE")
const tabPMCbutton = document.getElementById("App_BNav_tab_PMC")
const ProgressBar = document.getElementById('progress_bar')
const ProgressInLoop = document.getElementById('init_loop')
const ProgressEndLoop = document.getElementById('end_loop')
//PMC canvas and svg
const PMC_progress_svg = document.getElementById('App_PMC_progress_svg')
const PMC_progress_line = document.getElementById('App_PMC_progress_line')
const PMC_svg_selection_background = document.getElementById('App_PMC_svg_selection_background')
const PMC_svg_selection_elements = document.getElementById('App_PMC_svg_selection_elements')
const PMC_playing_notes_svg = document.getElementById('App_PMC_playing_notes_svg')
const PMC_mouse_action_svg_1 = document.getElementById('App_PMC_mouse_action_svg_1')
const PMC_mouse_action_svg_2 = document.getElementById('App_PMC_mouse_action_svg_2')
const PMC_mouse_action_time_svg = document.getElementById('App_PMC_mouse_action_time_svg')
const PMC_mouse_action_sound_svg = document.getElementById('App_PMC_mouse_action_sound_svg')
const PMC_selection_time_svg = document.getElementById('App_PMC_selection_time_svg')
const PMC_mouse_action_x = document.getElementById('App_PMC_mouse_action_x')
const PMC_mouse_action_y = document.getElementById('App_PMC_mouse_action_y')
const PMC_mouse_action_y_text = document.getElementById('App_PMC_mouse_action_y_text')

const PMC_mouse_action_drag = document.getElementById('App_PMC_mouse_action_drag')
const PMC_mouse_action_drag_element = document.getElementById('App_PMC_mouse_action_drag_element')

const PMC_main_scrollbar_H = document.querySelector(".App_PMC_main_scrollbar_H")
const PMC_svg_container = document.querySelector(".App_PMC_svg_container")
const PMC_svg_compas = document.getElementById('App_PMC_svg_compas')
const PMC_canvas_dots = document.getElementById('App_PMC_canvas_dots')
const PMC_dots_container = document.getElementById('App_PMC_dots_container')
const PMC_svg_segment = document.getElementById('App_PMC_svg_segment')
//all PMC visible voices BUT the selected one
const PMC_svg_all_voices = document.getElementById('App_PMC_svg_all_voices')
const PMC_svg_selected_voice = document.getElementById('App_PMC_svg_selected_voice')

var flag_DATA_updated={PMC:false,RE:false}
var APP_selection_options={	"target": null,
							"working_space": null,
							"voice_number": null,
							"voice_id": null,
							"segment_index": null
}
let RE_global_variables=null
let A_global_variables=null
let V_global_variables={"effects":{},"volumes":[],"channel_voice_id":[],"mainVolume":90,"mainPan":0.5, "mainMetronomeVolume":90}
const V_velocity_default=80
const V_velocity_default_min=25
const V_velocity_default_max=125
let e_channel=0
let p_channel=0
const p_channel_freq={"Pulse":58.2692306599689,"Accent":61.73409942094616, "Accent_weak":65.405,"Tic":69.29418368656971,"M1":73.41463026967455,"M2":77.78009135675298,"M3":82.40513626837418,"M4":87.3052006619911}
const p_channel_midiNote={"Pulse":34,"Accent":35, "Accent_weak":36,"Tic":37,"M1":38,"M2":39,"M3":40,"M4":41}
var midi_input_devices=[]
var midi_output_devices=[]
//let channel_voice_id=[]

//inp box
var previous_value=0
//let input_active=true

//keyboard
const ABC_note_list_diesis = ["C","C♯","D","D♯","E","F","F♯","G","G♯","A","A♯","B"]
const ABC_note_list_bemolle = ["C","D♭","D","E♭","E","F","G♭","G","A♭","A","B♭","B"]

//var current_position_PMCRE = null
let current_position_PMCRE={voice_data:null,PMC:{X_start:null,Y_start:null},RE:{scrollbar:true,column:null,X_start:null,Y_start:null},segment_index : null,pulse :null,fraction :null,note : null}
let BNAV_knob_position={start_Y:null,start_value:null,end_value:null,knob_div:null}

//init data_array
var state_point_array = new Array(100)
var current_state_point_index = 0
var max_state_point_index = 0
var min_state_point_index = 0

//CALCULATOR
var state_point_array_calc= new Array(100)
var current_state_point_index_calc = 0
var max_state_point_index_calc = 0
var min_state_point_index_calc = 0

//copy paste
//ID == segment , voice, lineiD
var copied_data =  {"data":null,"id":""}


//ALIGNMENT XXX
var RE_global_time_all_changes // == global_time_all_changes
var RE_global_time_voices_changes
var RE_global_note_voices_changes
var RE_global_time_segment_change // == global time segment_changes

//Graph var
var maincanvas = document.getElementById('maincanvas')

//Mouse Check

//Char
const end_range = "•"
const no_value = "⸰"
const A_spacer=" "//medium space En
const no_value_iA = "△"
//smaller •
const tie_intervals = "L"
const tie_intervals_m = "l"
var A_global_list=[]
//user
var user = null

//Sound and Player variables
var soundsindexP = 0
var soundsindexF = 0
var soundsindexM = 0
var soundFontLoaded = false

if(document.getElementById("play_button")!=null)document.getElementById("play_button").disabled = true

//PMC
var PMC_zoom_x = 1
var PMC_zoom_y = 1
var PMC_extra_x_space = 5
var PMC_segment_name_prevent_default = false
//PMC dragging steps counter
var PMC_drag_x_steps =0
var PMC_drag_y_steps =0

//CSS VARIABLES
var r = document.querySelector(':root')
rs = getComputedStyle(r)
var block_string = rs.getPropertyValue('--RE_block')
const nuzic_block = parseInt(block_string,10)
const nuzic_block_half = parseInt(rs.getPropertyValue('--RE_block_half'),10)

const nuzic_dark = rs.getPropertyValue('--Nuzic_dark')
const nuzic_light = rs.getPropertyValue('--Nuzic_light')
const nuzic_white = rs.getPropertyValue('--Nuzic_white')
const nuzic_grey = rs.getPropertyValue('--Nuzic_grey')
const nuzic_blue = rs.getPropertyValue('--Nuzic_blue')
const nuzic_blue_light = rs.getPropertyValue('--Nuzic_blue_light')
const nuzic_red = rs.getPropertyValue('--Nuzic_red')
const nuzic_red_light = rs.getPropertyValue('--Nuzic_red_light')
const nuzic_green = rs.getPropertyValue('--Nuzic_green')
const nuzic_green_light = rs.getPropertyValue('--Nuzic_green_light')
const nuzic_yellow = rs.getPropertyValue('--Nuzic_yellow')
const nuzic_yellow_light = rs.getPropertyValue('--Nuzic_yellow_light')
const nuzic_pink = rs.getPropertyValue('--Nuzic_pink')
const nuzic_pink_light = rs.getPropertyValue('--Nuzic_pink_light')

//ticks por quarter note???
//better to calculate it manually
APP_ready_pulse_sounds()
APP_ready_metronome()
APP_ready_fractioning_metronome()

//INSTRUMENTS

let secondary_players_timeout_id=[]

APP_disable_voice_instrument_buttons()
function APP_disable_voice_instrument_buttons(){
	var change_instrument_buttons = [...document.querySelectorAll(".App_RE_voice_instrument")]
	change_instrument_buttons.forEach(item=>{
		item.disabled=true
	})
}

const instrument_name_list=["0 - Stereo Grand","1 - Bright Grand","2 - Electric Grand","3 - Honky-Tonk","4 - Tine Electric Piano","5 - FM Electric Piano",
"6 - Harpsichord","7 - Clavinet","8 - Celeste","9 - Glockenspiel","10 - Music Box","11 - Vibraphone","12 - Marimba","13 - Xylophone",
"14 - Tubular Bells","15 - Dulcimer","16 - Tonewheel Organ","17 - Percussive Organ","18 - Rock Organ","19 - Pipe Organ","20 - Reed Organ",
"21 - Accordian","22 - Harmonica","23 - Bandoneon","24 - Nylon Guitar","25 - Steel Guitar","26 - Jazz Guitar","27 - Clean Guitar",
"28 - Muted Guitar","29 - Overdrive Guitar","30 - Distortion Guitar","31 - Guitar Harmonics","32 - Acoustic Bass","33 - Finger Bass",
"34 - Pick Bass","35 - Fretless Bass","36 - Slap Bass 1","37 - Slap Bass 2","38 - Synth Bass 1","39 - Synth Bass 2",
"40 - Violin","41 - Viola","42 - Cello","43 - Double Bass","44 - Stereo Strings Trem","45 - Pizzicato Strings","46 - Orchestral Harp",
"47 - Timpani","48 - Stereo Strings Fast","49 - Stereo Strings Slow","50 - Synth Strings 1","51 - Synth Strings 2","52 - Concert Choir",
"53 - Voice Oohs","54 - Synth Voice","55 - Orchestra Hit","56 - Trumpet","57 - Trombone","58 - Tuba","59 - Muted Trumpet","60 - French Horns",
"61 - Brass Section","62 - Synth Brass 1","63 - Synth Brass 2","64 - Soprano Sax","65 - Alto Sax","66 - Tenor Sax","67 - Baritone Sax","68 - Oboe",
"69 - English Horn","70 - Bassoon","71 - Clarinet","72 - Piccolo","73 - Flute","74 - Recorder","75 - Pan Flute","76 - Bottle Blow","77 - Shakuhachi",
"78 - Irish Tin Whistle","79 - Ocarina","80 - Square Lead","81 - Saw Lead","82 - Synth Calliope","83 - Chiffer Lead","84 - Charang","85 - Solo Vox",
"86 - 5th Saw Wave","87 - Bass & Lead","88 - Fantasia","89 - Warm Pad","90 - Polysynth","91 - Space Voice","92 - Bowed Glass","93 - Metal Pad","94 - Halo Pad",
"95 - Sweep Pad","96 - Ice Rain","97 - Soundtrack","98 - Crystal","99 - Atmosphere","100 - Brightness","101 - Goblin","102 - Echo Drops","103 - Star Theme",
"104 - Sitar","105 - Banjo","106 - Shamisen","107 - Koto","108 - Kalimba","109 - Bagpipes","110 - Fiddle","111 - Shenai","112 - Tinker Bell","113 - Agogo",
"114 - Steel Drums","115 - Wood Block","116 - Taiko Drum","117 - Melodic Tom","118 - Synth Drum","119 - Reverse Cymbal","120 - Fret Noise","121 - Breath Noise",
"122 - Seashore","123 - Birds","124 - Telephone 1","125 - Helicopter","126 - Applause","127 - Gun Shot","128.0 - Standard Drum","128.1 - TR-808"]
/*
const percussion_list=["Nuzic Pulse","Acoustic Bass Drum","Bass Drum 1","Side Stick","Acoustic Snare","Hand Clap","Electric Snare",
		"Low Floor Tom","Closed Hi Hat","High Floor Tom","Pedal Hi-Hat","Low Tom","Open Hi-Hat","Low-Mid Tom","Hi Mid Tom","Crash Cymbal 1",
		"High Tom","Ride Cymbal 1","Chinese Cymbal","Ride Bell","Tambourine","Splash Cymbal","Cowbell","Crash Cymbal 2","Vibraslap","Ride Cymbal 2",
		"Hi Bongo","Low Bongo","Mute Hi Conga","Open Hi Conga","Low Conga","High Timbale","Low Timbale","High Agogo","Low Agogo","Cabasa","Maracas",
		"Short Whistle","Long Whistle","Short Guiro","Long Guiro","Claves","Hi Wood Block","Low Wood Block","Mute Cuica","Open Cuica","Mute Triangle","Open Triangle",
		//new 70 to 84
		"Shaker","Jingle Bell","Cortina","Castañuela","Zurdo Low","Zurdo High","Finger Snap","Clap","Clap Muted","Cajon Low",
		"Cajon Subdivision","Cajon High","Sticks","Clinck","Gong"]*/

const percussion_list=["Nuzic Pulse","Low Kick","Kick","Ring Stick","Snare 1","Snare 2","Snare 3","Tom 1 (Low)","Close Hi-Hat","Tom 2","Pedal Hi-Hat","Tom 3","Open Hi-Hat","Tom 4","Tom 5","Crash 1","Tom 6 (high)","Ride",
		"Crash 2","Ride Bell","Tambourine","Crash 3","Cowbell","Crash 4","Carraca","Ride Low","Bongo High","Bongo Low","Conga Muted","Conga Open","Conga Low","Timbal High","Timbal Low",
		"Agogo High","Agogo Low","Cabasa","Maracas","Whistle Short","Whistle Long","Guiro Short","Guiro Long","Clave","WoodBlock High","WoodBlock Low","Quica Hi Tone","Quica Down Stroke",
		"Triangle Close","Triangle Open","Shaker","Jingle Bell","Cortina","Castañuela","Zurdo Low","Zurdo High","Finger Snap","Clap","Clap Muted","Cajon Low","Cajon Subdivision",
		"Cajon High","Sticks","Clinck","Gong"]

function APP_ready_pulse_sounds(){
	soundsindexP++;
	//players
	console.info(`all sounds Pulse have been loaded`)
	APP_loader_add_value(10)
	if(soundsindexP==1 && soundsindexM==1 && soundsindexF==1 && soundFontLoaded){
		//activate play button
		var playbutton = document.getElementById("playbutton")
		var playbuttonlabel = document.getElementById("playbuttonlabel")
		playbutton.disabled = false
		playbuttonlabel.innerHTML = "Play"
	}
}

function APP_ready_fractioning_metronome(){
	soundsindexF++;
	//players
	console.info(`all sounds Frac Metro have been loaded`)
	APP_loader_add_value(10)
	if(soundsindexP==1 && soundsindexM==1 && soundsindexF==1 && soundFontLoaded){
		//activate play button
		var playbutton = document.getElementById("playbutton")
		var playbuttonlabel = document.getElementById("playbuttonlabel")
		playbutton.disabled = false
		//playbuttonlabel.innerHTML = '<img class="RE_icon" src="./Icons/play.svg">'
	}
}

function APP_ready_metronome(){
	//main metronome sound
	soundsindexM++;
	//players
	console.info(`all sounds Main Metro have been loaded`)
	APP_loader_add_value(10)
	if(soundsindexP==1 && soundsindexM==1 && soundsindexF==1 && soundFontLoaded){
		//activate play button
		var playbutton = document.getElementById("playbutton")
		var playbuttonlabel = document.getElementById("playbuttonlabel")
		playbutton.disabled = false
		//playbuttonlabel.innerHTML = '<img class="RE_icon" src="./Icons/play.svg">'
	}
}

//function APP_ready_instrument(){
function APP_ready_soundFont(){
	//console.log("ready instrument "+instrumentindex)
	soundFontLoaded=true
	//players
	console.log(`all note have been loaded`)
	//abilitate all RE "change instruments buttons"
	if(!is_App)return
	let RE_voice_matrix=document.querySelector(".App_RE_voice_matrix")
	let change_instrument_buttons = (RE_voice_matrix!=null)?[...document.querySelector(".App_RE_voice_matrix").querySelectorAll(".App_RE_voice_instrument")]:[]
	change_instrument_buttons.forEach(item=>{
		item.disabled=false
	})
	//abilitate PMC "change instrument button"
	let PMC_instrument=document.querySelector(".App_PMC").querySelector(".App_PMC_selected_voice_instrument")
	if(PMC_instrument!=null)PMC_instrument.disabled=false
	let change_instrument_buttons_mixer = [...document.querySelector("#App_BNav_V").querySelectorAll(".App_BNav_V_voice_instrument")]
	change_instrument_buttons_mixer.forEach(item=>{
		item.disabled=false
	})
	if(APP_verify_sound_ready()){
		//activate play button
		let playbutton = document.getElementById("play_button")
		if(playbutton!=null){
			var playbuttonlabel = document.querySelector(".App_Nav_play_button")
			playbutton.disabled = false
			playbuttonlabel.innerHTML = '<img class="RE_icon" src="./Icons/main_play.svg">'
		}
	}
}

function APP_verify_sound_ready(){
	//if(soundsindexP==1 && soundsindexM==1 && soundsindexF==1 && firstinstrumentloaded && instrumentindex==allinstruments){
	if(soundsindexP==1 && soundsindexM==1 && soundsindexF==1 && soundFontLoaded){
		if(is_App)APP_loader_hide()
		return true
	}else{
		return false
	}
}

function APP_loader_hide(){
	let label=document.querySelector(".App_Nav_timer_loader_label")
	let loader=document.querySelector(".App_Nav_timer_loader")
	setTimeout(function(){
		label.style.display = "none"
		loader.style.display = "none"
	}, 500)
}

function APP_loader_zero(){
	let loader=document.querySelector(".App_Nav_timer_loader")
	let label=document.querySelector(".App_Nav_timer_loader_label")
	loader.value=0
	label.style.display = "flex"
	loader.style.display = "block"
}

function APP_loader_add_value(value=1){
	let loader=document.querySelector(".App_Nav_timer_loader")
	if(loader==null)return
	let current_value=parseInt(loader.value)
	loader.value=current_value+value
}

let current_loader_sf_percentage=0
let current_loader_sf_extra=0
function APP_loader_percentage_sf(percent,extra){
	let value=percent-current_loader_sf_percentage+extra-current_loader_sf_extra
	current_loader_sf_percentage=percent
	current_loader_sf_extra=extra
	if(value!=0)APP_loader_add_value(value)
}

//User and global variables SCALE

// tet,name,iS,module,tags
var global_scale_list = null
var user_scale_list = null
// idea_scale_list =  name,iS,acronym,module,family,global


