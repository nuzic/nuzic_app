function RE_clear_C_line_values(){
	let C_line=document.querySelector('.App_RE_C')
	C_line.innerHTML=''
}

//SCALE LINE

function RE_show_scale_circle(S_button){
	APP_hide_scale_circle_elements()
	let scale_data = JSON.parse(S_button.getAttribute("value"))
	let parent = S_button.closest(".App_RE_S_obj")
	let [scale_index,]=RE_element_index(parent,".App_RE_S_obj")
	let pink_line=document.querySelector(".App_RE_pink_scale_range_background")
	//add circle container //trigger mouse leave
	pink_line.innerHTML+=BNAV_calculate_S_circle_container(scale_data,scale_index)
	//make sure start is visible
	let App_RE=document.querySelector(".App_RE")
	let left_position=parent.getBoundingClientRect().left-App_RE.getBoundingClientRect().left
	if(left_position<(nuzic_block*7)){
		//move scrollbar
		RE_show_scale_number_S_line(scale_index)
		//setTimeout(function(){RE_show_pink_scale_line_background(parent,true)},10)
		setTimeout(function(){RE_show_scale_circle(S_button)},10)
		return
	}
	//add pink line / break scale range
	RE_show_pink_scale_line_background(parent,true)
	BNAV_find_and_select_scale_composition(scale_data.acronym,scale_data.rotation,scale_data.starting_note)
	BNAV_select_scale_number_sequence_list(scale_index,false)
}

function RE_show_scale_input(S_button=null){
	//use scale != null == button pressed to focus on correct scale
	//APP_stop()
	let selected = -1
	if(S_button==null){
		//select first scale
		selected=0
	}else{
		let parent = S_button.closest(".App_RE_S_obj")
		let [elementIndex,list]=RE_element_index(parent,".App_RE_S_obj")
		selected=elementIndex
	}
	//verify if found something
	let scale_sequence= DATA_get_scale_sequence()
	if(selected<0 && selected>=scale_sequence.length)selected=0
	let ES_button = document.getElementById("App_BNav_tab_ES")
	ES_button.click()
	BNAV_select_scale_number_sequence_list(selected)
}

function RE_hide_scale_circle_elements(){
	let pink_line=document.querySelector(".App_RE_pink_scale_range_background")
	if(pink_line==null)return
	pink_line.classList.add("hidden")
	RE_delete_S_circle_container()
}

function RE_delete_S_circle_container(){
	let pink_line=document.querySelector(".App_RE_pink_scale_range_background")
	let circle_container= [...pink_line.querySelectorAll(".App_BNav_S_circle_container")]
	if(circle_container!=null){
		circle_container.forEach(item=>{item.remove()})
	}
}

function RE_show_pink_scale_line_background(obj,onTop=false){
	let App_RE=document.querySelector(".App_RE")
	let pink_line=App_RE.querySelector(".App_RE_pink_scale_range_background")
	let left_position=obj.getBoundingClientRect().left-App_RE.getBoundingClientRect().left
	pink_line.style=""
	if(left_position<(nuzic_block*7))return false
	let global_zoom = Number(document.querySelector('.App_SNav_select_zoom').value)
	pink_line.style.left=(left_position/global_zoom)+"px"
	//pink_line.style.zIndex=10
	if(onTop)pink_line.style.zIndex=100
	pink_line.classList.remove("hidden")
}

function RE_show_scale_number_S_line(index){
	let scale_line = document.querySelector('.App_RE_S')
	let scale_obj_list = [...scale_line.querySelectorAll(".App_RE_S_obj")]
	//find correct div and move horizontal bar there!
	let scale_obj_target = scale_obj_list[index]
	let str = scale_obj_target.style.left
	let n_pixels_left=parseInt(str.substring(0, str.length - 2))
	//var tot_buttons = scale_button_list.length
	let value = n_pixels_left-nuzic_block
	RE_checkScroll_H(value)
}

function RE_break_scale_range(object){
	let pulse=parseInt(object.getAttribute("value"))
	let result=DATA_insert_scale_at_pulse(pulse)
	if(!result.success){
		//open scale circle for extra input
		//avoid pink line disappearing
		event.stopPropagation()
		object.classList.add("insert_input")
		let pink_line=document.querySelector(".App_RE_pink_scale_range_background")
		pink_line.style.zIndex=100
		//add circle container //trigger mouse leave
		pink_line.innerHTML+=BNAV_calculate_S_circle_container_insert(result)
		setTimeout(function(){object.classList.remove("insert_input");object.classList.add("hidden")},10)
	}
}

function RE_exit_break_scale_range(break_scale_range,object){
	if(break_scale_range!=null){
		if(break_scale_range.classList.contains("insert_input")){
			return
		}
	}
	if(break_scale_range==null)break_scale_range=object.querySelector(".App_RE_break_scale_range")
	if(!break_scale_range.classList.contains("hidden")){
		break_scale_range.classList.add("hidden")
		RE_hide_scale_circle_elements()
	}
}

function RE_break_scale_range_calculate_position(object,event){
	if(RE_scale_circle_active())return
	let box_position=Math.floor(event.layerX/(nuzic_block))
	let break_obj=object.querySelector(".App_RE_break_scale_range")
	//current_position_PMCRE.RE.scrollbar=false
	if(!isOdd(box_position)){
		if(box_position==0)return
		//let scale_value=JSON.parse(object.querySelector("button").getAttribute("value"))
		let scale_position=Math.floor(object.offsetLeft/(nuzic_block))-1
		let position=(box_position+scale_position)/2
		let scale_column=box_position/2
		let start_scale_column=scale_position/2
		let found=false
		let show=false
		let pulse=0
		let pulse_scale=0
		RE_global_time_all_changes.find((time,index)=>{
			let times_scale_segment_change = RE_global_time_segment_change.filter((stime) => {
				return stime <=time
			}).length-1
			if(index+times_scale_segment_change==start_scale_column){
				//found time scale start
				pulse_scale=Math.round(time*PPM/60)
			}
			if(index+times_scale_segment_change==position){
				pulse=Math.round(time*PPM/60)
				if((time*PPM/60)==pulse){
					show=true
					return true
				}
			}
			if(index+times_scale_segment_change>position)return true
		})
		if(show){
			break_obj.classList.remove("hidden")
			break_obj.style.left=(scale_column-1)*nuzic_block*2+"px"
			break_obj.setAttribute("value",pulse)
			let parent_duration=JSON.parse(object.querySelector(".App_RE_S_value").getAttribute("value")).duration
			let duration=parent_duration-(pulse-pulse_scale)
			//console.log(pulse_scale)
			break_obj.querySelector(".App_RE_break_scale_info").innerHTML=duration
			RE_delete_S_circle_container()
			RE_show_pink_scale_line_background(break_obj)
		}
	}else{
		//img.classList.add("hidden")
		let break_scale_range=object.querySelector(".App_RE_break_scale_range")
		RE_exit_break_scale_range(break_scale_range)
	}
}

function RE_scale_circle_active(){
	let pink_line=document.querySelector(".App_RE_pink_scale_range_background")
	let circle_container= pink_line.querySelector(".App_BNav_S_circle_container")
	return circle_container!=null
}
