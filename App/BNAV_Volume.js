function BNAV_update_V(data=null){
	let V_button = document.getElementById("App_BNav_tab_V")
	if(!V_button.checked)return
	if (data==null)data=DATA_get_current_state_point()
	//master effects
	let reverb_knob=document.getElementById("App_BNav_V_master_reverb").querySelector(".App_BNav_V_button")
	let reverb_value_degree= (data.global_variables.effects.reverb*2-1)*135//120
	reverb_knob.style=`transform: rotate(${reverb_value_degree}deg)`
	reverb_knob.setAttribute("value",data.global_variables.effects.reverb)
	//voice vol controllers
	let volume_module_container=document.getElementById("App_BNav_V_module_container")
	let string_module=""
	//APP_prepare_SF_instruments(data.voice_data)
	data.voice_data.forEach((voice,index)=>{
		let inst_name= instrument_name_list[voice.instrument]
		let metro_class=(voice.metro==0)?'App_PMC_text_strikethrough':""
		let metro_text=(voice.metro==0)?"M":("M"+voice.metro)
		let e_sound_text=(voice.e_sound==22)?"e":"e"+voice.e_sound
		let class_mute=(voice.mute)?" pressed":""
		let class_solo=(voice.solo)?" pressed":""
		let instrument_disabled=(soundFontLoaded)?"":'disabled="true"'
		let voice_number=data.voice_data.length-index-1
		let channel=(voice.midiCh==null)?((voice.instrument<128)?0:9): voice.midiCh
		let pan_value_degree= (voice.pan*2-1)*135
		let fx_value_degree= (voice.fx*2-1)*135
		let lpf_value_degree=(voice.lpf*2-1)*135
		let v_value_norm=(1/(V_velocity_default_max-V_velocity_default_min))*(voice.vel-V_velocity_default_min)
		let v_value_degree=(v_value_norm*2-1)*135
		//let v_value_degree=(135/(V_velocity_default_max-(V_velocity_default_max-V_velocity_default_min)/2))*(voice.vel-(V_velocity_default_max-V_velocity_default_min)/2)
		string_module+=`<div class="App_BNav_V_voice">
			<div class="App_BNav_V_voice_head">
				<div class="App_BNav_V_voice_head_top">
					<label class="App_BNav_V_voice_number">${voice_number}</label>
					<div class="App_BNav_V_voice_name">${voice.name}</div>
				</div>
				<button class="App_BNav_V_voice_instrument" onclick="BNAV_change_voice_instrument(${voice.voice_id},${voice.instrument})" onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)" data-value="InstrumentHover" ${instrument_disabled}>${inst_name}</button>
			</div>
			<div class="App_BNav_V_module_content">
				<div class="App_BNav_V_volume_container">
					<input data-value="VolumeHover" onmouseenter="APP_hover_area_enter(this)" value="${voice.volume}" onmouseleave="APP_hover_area_exit(this)" onchange="BNAV_slider_change_voice_volume(this,${voice.voice_id})" oninput="BNAV_slider_voice_volume(this,${voice.voice_id})" type="range" min="0" max="127" step="1" ondblclick="BNAV_slider_double_click(this,90)" class="slider"/>
					<div class="App_BNav_V_volume_default"></div>
					<div class="App_BNav_V_button" value="${voice.pan}" style="transform: rotate(${pan_value_degree}deg)" onmousedown="BNAV_knob_mouse_down(event,'voice_pan',${voice.voice_id})" ondblclick="BNAV_knob_double_click(0.5)">
						<div></div>
					</div>
				</div>
				<div class="App_BNav_V_button_container">
					<div>
						<div class="App_BNav_V_button" value="${voice.fx}" style="transform: rotate(${fx_value_degree}deg)" onmousedown="BNAV_knob_mouse_down(event,'fx',${voice.voice_id})">
							<div></div>
						</div>
						<label>FX</label>
					</div>
					<div>
						<div class="App_BNav_V_button" value="${voice.lpf}" style="transform: rotate(${lpf_value_degree}deg)" onmousedown="BNAV_knob_mouse_down(event,'lpf',${voice.voice_id})" ondblclick="BNAV_knob_double_click(0.5)">
							<div></div>
						</div>
						<label>LPF</label>
					</div>
					<div class="App_BNav_V_humanize">
						<svg>
							<line x1="2" y1="9" x2="2" y2="28" stroke="var(--Nuzic_dark)" stroke-width="2"></line>
							<line x1="8" y1="7" x2="8" y2="28" stroke="var(--Nuzic_dark)" stroke-width="2"></line>
							<line x1="14" y1="9" x2="14" y2="28" stroke="var(--Nuzic_dark)" stroke-width="2"></line>
							<line x1="20" y1="7" x2="20" y2="28" stroke="var(--Nuzic_dark)" stroke-width="2"></line>
							<line x1="26" y1="9" x2="26" y2="28" stroke="var(--Nuzic_dark)" stroke-width="2"></line>
							<line x1="32" y1="5" x2="32" y2="28" stroke="var(--Nuzic_dark)" stroke-width="2"></line>
							<line x1="38" y1="9" x2="38" y2="28" stroke="var(--Nuzic_dark)" stroke-width="2"></line>
							<line x1="44" y1="7" x2="44" y2="28" stroke="var(--Nuzic_dark)" stroke-width="2"></line>
							<line x1="50" y1="9" x2="50" y2="28" stroke="var(--Nuzic_dark)" stroke-width="2"></line>
						</svg>
					</div>
					<div>
						<div class="App_BNav_V_button" value="${v_value_norm}" style="transform: rotate(${v_value_degree}deg)" onmousedown="BNAV_knob_mouse_down(event,'vel',${voice.voice_id})" ondblclick="BNAV_knob_double_click(0.6)">
							<div></div>
						</div>
						<label>VEL</label>
					</div>
					<div>
						<button class="App_BNav_voice_mute ${class_mute}" onClick="BNAV_change_voice_mute(${voice.voice_id},${voice.mute})" onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)" data-value="muteHover">M</button>
						<button class="App_BNav_voice_solo ${class_solo}" onClick="BNAV_change_voice_solo(${voice.voice_id},${voice.solo})" onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)" data-value="SoloHover">S</button>
					</div>
				</div>
			</div>
			<div class="App_BNav_V_voice_bottom">
				<button class="App_BNav_voice_channel" onclick="BNAV_show_midi_channel_selector(this,${voice.voice_id},${channel})">ch${channel+1}</button>
				<button class="App_BNav_voice_pulse_sound" onclick="BNAV_show_pulse_sound_selector(${voice.voice_id},${voice.e_sound})">${e_sound_text}</button>
				<button class="App_BNav_voice_metro_sound ${metro_class}" onclick="BNAV_change_metro_sound(${voice.voice_id},${voice.metro})" onmouseenter="APP_hover_area_enter(this)" onmouseleave="APP_hover_area_exit(this)" data-value="MetronomeVoiceHover">${metro_text}</button>
			</div>
		</div>`
	})
	volume_module_container.innerHTML=string_module
}

//master volume
function BNAV_set_master_volume(value){
	//if(value>1)value=1	//compatibility
	outputMasterVolume.value = value
	BNAV_change_master_volume(value)
}

function BNAV_slider_master_volume(){
	let value = BNAV_read_master_volume()
	BNAV_set_master_volume(value)
	DATA_save_preferences_file()
}

function BNAV_read_master_volume(){
	return Number(outputMasterVolume.value)
}

function BNAV_change_master_volume(value){
	//value in SF
	V_global_variables.mainVolume=value
	if(SF_manager!=undefined){
		if(SF_manager.synth!=undefined){
			//dB
			//SF_manager.synth.setMainVolume((value+20)/4) //0 to 1 or five
			SF_manager.synth.setMainVolume(value/127) //0 to 1 or five
		}else{
			//wait
			setTimeout(function(){
				BNAV_change_master_volume(value)
			}, 500)
		}
	}else{
		//wait
		setTimeout(function(){
			BNAV_change_master_volume(value)
		}, 500)
	}
}

//master pan volume
function BNAV_set_master_pan(value){
	//value = 0-1
	let pan_value=value*2-1
	let pan_value_degree= (pan_value)*135//120
	outputMasterPan.style=`transform: rotate(${pan_value_degree}deg)`
	outputMasterPan.setAttribute("value",value)
	V_global_variables.mainPan=value
	//value in SF
	if(SF_manager!=undefined){
		if(SF_manager.synth!=undefined){
			SF_manager.synth.setMasterPan(pan_value)
		}else{
			//wait
			setTimeout(function(){
				BNAV_set_master_pan(value)
			}, 500)
		}
	}else{
		//wait
		setTimeout(function(){
			BNAV_set_master_pan(value)
		}, 500)
	}
}

function BNAV_read_master_pan(){
	let value_knob=Number(outputMasterPan.getAttribute("value"))
	//let value_pan=value_knob*2-1
	return value_knob
}

//main metronome volume
function BNAV_set_main_metronome_volume(value){
	//div knob 0 -->127
	let value_norm=(value)/127
	let value_degree=(value_norm*2-1)*135
	outputMainMetronomeVolume.style=`transform: rotate(${value_degree}deg)`
	outputMainMetronomeVolume.setAttribute("value",value)
	V_global_variables.mainMetronomeVolume=value
	//if(SF_manager!=null)SF_manager.synth.nuzic_sequencer.update_V_global_variables(V_global_variables)
	//value in SF
	if(SF_manager!=undefined){
		if(SF_manager.synth!=undefined){
			SF_manager.synth.nuzic_sequencer.update_V_global_variables(V_global_variables)
		}else{
			//wait
			setTimeout(function(){
				BNAV_set_main_metronome_volume(value)
			}, 500)
		}
	}else{
		//wait
		setTimeout(function(){
			BNAV_set_main_metronome_volume(value)
		}, 500)
	}
}

function BNAV_knob_main_metro_mouse_down(event){
	BNAV_knob_position.start_X=event.clientX
	BNAV_knob_position.start_value=parseFloat(event.target.getAttribute("value"))/127
	BNAV_knob_position.end_value=BNAV_knob_position.start_value
	BNAV_knob_position.knob_div=event.target
	BNAV_knob_position.type=null
	BNAV_knob_position.voice_id=null
	this._handlers = {
		mouseMove: this.BNAV_knob_main_metro_mouse_move.bind(this),
		mouseUp: this.BNAV_knob_main_metro_mouse_up.bind(this)}
	//add a mouse move listener to window
	window.addEventListener('mousemove', this._handlers.mouseMove)
	window.addEventListener('mouseup', this._handlers.mouseUp)
	event.preventDefault()
}

function BNAV_knob_main_metro_mouse_move(event){
	//value is from 0 to 127
	//value degree from -120 to 120
	let travel=100
	let delta=(event.clientX-BNAV_knob_position.start_X)/travel+BNAV_knob_position.start_value
	let new_value=0
	if(delta>1){
		new_value=1
	}else if(delta<0){
		new_value=0
	}else{
		new_value=delta
	}
	BNAV_knob_position.end_value=new_value
	let real_value=Math.round(new_value*127)
	BNAV_set_main_metronome_volume(real_value)
	event.preventDefault()
}

function BNAV_knob_main_metro_mouse_up(event){
	//value is from -1 to 1
	//value degree from -120 to 120
	window.removeEventListener('mousemove', this._handlers.mouseMove)
	window.removeEventListener('mouseup', this._handlers.mouseUp)
	//other stuff??? XXX ((change value))
	let real_value=Math.round(BNAV_knob_position.end_value*127)
	BNAV_set_main_metronome_volume(real_value)
	DATA_save_preferences_file()
}

function BNAV_knob_main_metro_double_click(real_value){
	BNAV_set_main_metronome_volume(real_value)
	DATA_save_preferences_file()
}

function BNAV_read_main_metronome_volume(){
	//return Number(outputMainMetronomeVolume.value) //div knob
	return Number(outputMainMetronomeVolume.getAttribute("value"))
}


//MIDI IN MIDI OUT functions

function BNAV_V_switch_output(){
	let [speaker,midi_output_ID]=BNAV_read_output_preferences()
	//BNAV_set_output_preferences(!speaker,midi_output_ID)
	//check changed before firing the function
	BNAV_set_output_preferences(speaker,midi_output_ID)
	DATA_save_preferences_file()
}

function BNAV_read_output_preferences(){
	let selector = document.getElementById("App_BNav_V_output")
	//let speaker = selector.classList.contains("speaker")
	let speaker = !selector.checked
	let midi_output_ID=selector.getAttribute("midi_ID")
	return [speaker,midi_output_ID]
}

function BNAV_read_input_preferences(){
	let selector = document.getElementById("App_BNav_keyboard_midi_input")
	let midi_input_ID=selector.getAttribute("midi_ID")
	return midi_input_ID
}

function BNAV_set_output_preferences(speaker=true,midi_output_ID=null){
	let selector = document.getElementById("App_BNav_V_output")
	let midi_selector = document.getElementById("App_BNav_V_midi_output")
	let midi_label = midi_selector.querySelector("label")
	let midi_icon = [...document.getElementById("App_BNav_V_output_container").querySelectorAll("img")][1]
	let found=midi_output_devices.find(output=>{
		return output.id==midi_output_ID
	})
	//use midi var so async doesnt change
	let midi=!speaker
	if(found){
		midi_label.innerHTML=found.name
	}else{
		midi_label.innerHTML="-----"
		midi=false
	}
	if(midi){
	//selector.classList.remove("speaker")
		selector.checked=true
		midi_selector.classList.add("selected")
		midi_icon.classList.add("selected")
	}else{
		//selector.classList.add("speaker")
		selector.checked=false
		midi_selector.classList.remove("selected")
		midi_icon.classList.remove("selected")
	}
	selector.setAttribute("midi_ID",midi_output_ID)
	if(SF_manager!=undefined){
		if(SF_manager.synth!=undefined){
			if(speaker || !found){
				SF_manager.synth.nuzic_sequencer._stop_all_sound_events()
				SF_manager.synth.nuzic_sequencer.midiOut=null
			}else{
				SF_manager.synth.nuzic_sequencer._stop_all_sound_events()
				SF_manager.synth.nuzic_sequencer.midiOut=found
			}
		}else{
			//wait
			setTimeout(function(){
				BNAV_set_output_preferences(speaker,midi_output_ID)
			}, 500)
		}
	}else{
		//wait
		setTimeout(function(){
			BNAV_set_output_preferences(speaker,midi_output_ID)
		}, 500)
	}
}

function BNAV_set_input_preferences(midi_input_ID=null){
	let midi_selector = document.getElementById("App_BNav_keyboard_midi_input")
	let midi_label = midi_selector.querySelector("label")
	//this is essential for data not to be replaced
	midi_selector.setAttribute("midi_ID",midi_input_ID)
	//wait system start
	if(SF_manager!=undefined){
		if(SF_manager.synth!=undefined){
			let found=midi_input_devices.find(input=>{
				return input.id==midi_input_ID
			})
			if(found){
				midi_label.innerHTML=found.name
			}else{
				midi_label.innerHTML="-----"
			}
			DATA_select_midi_input(midi_input_ID)
		}else{
			//wait
			setTimeout(function(){
				BNAV_set_input_preferences(midi_input_ID)
			}, 500)
		}
	}else{
		//wait
		setTimeout(function(){
			BNAV_set_input_preferences(midi_input_ID)
		}, 500)
	}
}

function BNAV_V_midi_close_menu(){
	let menu_div = document.querySelector("#App_BNav_dropdown_midi_list")
	menu_div.style.display = "none"
}

function BNAV_select_midi_device_in(id){
	let midi_input_ID = document.getElementById("App_BNav_keyboard_midi_input").getAttribute("midi_ID")
	let midi_output_ID = document.getElementById("App_BNav_V_output").getAttribute("midi_ID")
	if(id==midi_input_ID || id==midi_output_ID)return
	if(id==-1)id=null
	BNAV_set_input_preferences(id)
	BNAV_V_midi_close_menu()
	DATA_save_preferences_file()
}

function BNAV_select_midi_device_out(id){
	let midi_input_ID = document.getElementById("App_BNav_keyboard_midi_input").getAttribute("midi_ID")
	let midi_output_ID = document.getElementById("App_BNav_V_output").getAttribute("midi_ID")
	if(id==midi_input_ID || id==midi_output_ID)return
	if(id==-1)id=null
	let [speaker,]=BNAV_read_output_preferences()
	BNAV_set_output_preferences(speaker,id)
	BNAV_V_midi_close_menu()
	DATA_save_preferences_file()
}

//dB

function BNAV_volume_value_to_dB(value){
	//transform volume value 0-127 to decibel value -20 a 0
	var dB = -20 + (value/127) * 20
	return dB
}

//voice controllers

function BNAV_change_voice_instrument(voice_id,instrument){
	APP_stop()
	//better using array
	let options = {"voice_id":voice_id,"instrument":instrument}
	APP_show_instrument_selector(options)
}

function BNAV_slider_voice_volume(slider,voice_id){
	V_global_variables.volumes[voice_id].volume=Number(slider.value)
}

function BNAV_slider_change_voice_volume(slider,voice_id){
	let volume =Number(slider.value)
	V_global_variables.volumes[voice_id].volume=volume
	//DATA save
	let data = DATA_get_current_state_point(true)
	//Selected voice
	let selected_voice_data= data.voice_data.find(item=>{
		return item.voice_id==voice_id
	})
	selected_voice_data.volume = volume
	let old_RE=flag_DATA_updated.RE
	let old_PMC=flag_DATA_updated.PMC
	DATA_insert_new_state_point(data)
	flag_DATA_updated.RE=old_RE
	flag_DATA_updated.PMC=old_PMC
	//change channel volume
	let channel=BNAV_voice_id_to_channel(voice_id)
	if(channel!=null)SF_manager.synth.controllerChange(channel,7,volume)
}

function BNAV_change_voice_mute(voice_id){
	APP_stop()
	//Selected voice
	let data = DATA_get_current_state_point(true)
	let selected_voice_data= data.voice_data.find(item=>{
		return item.voice_id==voice_id
	})
	selected_voice_data.mute=!selected_voice_data.mute
	DATA_insert_new_state_point(data)
	if (tabPMCbutton.checked){
		BNAV_update_V(data)
		PMC_populate_voice_list()
		flag_DATA_updated.PMC=true
	}else{
		DATA_load_state_point_data(false,true)
	}
}

function BNAV_change_voice_solo(voice_id){
	APP_stop()
	//Selected voice
	let data = DATA_get_current_state_point(true)
	let selected_voice_data= data.voice_data.find(item=>{
		return item.voice_id==voice_id
	})
	selected_voice_data.solo=!selected_voice_data.solo
	DATA_insert_new_state_point(data)
	if (tabPMCbutton.checked){
		BNAV_update_V(data)
		PMC_populate_voice_list()
		flag_DATA_updated.PMC=true
	}else{
		DATA_load_state_point_data(false,true)
	}
}

function BNAV_show_midi_channel_selector(button,voice_id,channel){
	APP_stop()
	let options = {"voice_id":voice_id,"channel":channel}
	APP_voice_midi_channel_toggle_menu(button,options)
}

function BNAV_show_pulse_sound_selector(voice_id,e_sound){
	APP_stop()
	let options = {"voice_id":voice_id,"e_sound":e_sound}
	APP_show_pulse_sound_selector(options)
}

function BNAV_change_metro_sound(voice_id,current_value){
	APP_stop()
	let available_metro_sounds = DATA_list_available_metro_sounds()
	//var max_value = 5
	let max_value = available_metro_sounds-length-1
	current_value = available_metro_sounds.find(item=>{return item>current_value})
	//null if current = 5 (last one)
	if(current_value>max_value || current_value==null)current_value=0
	let data = DATA_get_current_state_point(true)
	//Selected voice
	let selected_voice_data= data.voice_data.find(item=>{
		return item.voice_id==voice_id
	})
	selected_voice_data.metro=current_value
	DATA_insert_new_state_point(data)
	if (tabPMCbutton.checked){
		BNAV_update_V(data)
		PMC_populate_voice_list()
		flag_DATA_updated.PMC=true
	}else{
		DATA_load_state_point_data(false,true)
	}
}


//KNOBS

function BNAV_knob_mouse_down(event,type,voice_id=null){
	BNAV_knob_position.start_X=event.clientX
	BNAV_knob_position.start_value=parseFloat(event.target.getAttribute("value"))
	BNAV_knob_position.end_value=BNAV_knob_position.start_value
	BNAV_knob_position.knob_div=event.target
	BNAV_knob_position.type=type
	BNAV_knob_position.voice_id=voice_id
	// define event listeners
	// have to store bound versions of handlers so they can be removed later
	this._handlers = {
		mouseMove: this.BNAV_knob_mouse_move.bind(this),
		mouseUp: this.BNAV_knob_mouse_up.bind(this)}
	//add a mouse move listener to window
	window.addEventListener('mousemove', this._handlers.mouseMove)
	//document.body.addEventListener('mouseup', this._handlers.mouseUp)
	window.addEventListener('mouseup', this._handlers.mouseUp)
	//window.addEventListener('mouseleave', this._handlers.mouseUp)
	event.preventDefault()
}

function BNAV_knob_mouse_move(event){
	//value is from 0 to 1
	//value degree from -120 to 120
	let travel=100
	let delta=(event.clientX-BNAV_knob_position.start_X)/travel+BNAV_knob_position.start_value
	let new_value=0
	if(delta>1){
		new_value=1
	}else if(delta<0){
		new_value=0
	}else{
		new_value=delta
	}
	BNAV_knob_position.end_value=new_value
	//rotate
	let value_degree=(BNAV_knob_position.end_value*2-1)*135
	BNAV_knob_position.knob_div.style=`transform: rotate(${value_degree}deg)`
	BNAV_knob_modify_value()
	event.preventDefault()
}

function BNAV_knob_mouse_up(event){
	//value is from -1 to 1
	//value degree from -120 to 120
	window.removeEventListener('mousemove', this._handlers.mouseMove)
	window.removeEventListener('mouseup', this._handlers.mouseUp)
	//other stuff??? XXX ((change value))
	BNAV_knob_position.knob_div.setAttribute("value",BNAV_knob_position.end_value)
	BNAV_knob_modify_value()
	BNAV_knob_save_value()
}

function BNAV_knob_modify_value(){
	//let change knobs on the fly
	let value=BNAV_knob_position.end_value
	let type=BNAV_knob_position.type
	let voice_id=BNAV_knob_position.voice_id
	let channel=BNAV_voice_id_to_channel(voice_id)
	switch (type) {
		case "main_pan":
			BNAV_set_master_pan(value)
			DATA_save_preferences_file()
		break
		case "reverb":
			V_global_variables.effects.reverb=value
			//for each channel
			V_global_variables.volumes.forEach((item,index)=>{
				let reverb_total_value=Math.round((value*item.fx)*128)-1 //(or 91??)
				channel=BNAV_voice_id_to_channel(index)
				if(channel!=null)SF_manager.synth.controllerReverb(channel,reverb_total_value)
			})
		break
		case "voice_pan":
			//V_global_variables.volumes[voice_id].pan=value
			let pan=Math.round(value*127)
			if(channel==null)return
			SF_manager.synth.controllerChange(channel,10,pan)
		break
		case "fx":
			V_global_variables.volumes[voice_id].fx=value
			let reverb_total_value=Math.round((V_global_variables.effects.reverb*value)*128)-1 //(or 91??)
			if(channel==null)return
			SF_manager.synth.controllerReverb(channel,reverb_total_value)
		break
		case "lpf":
			//V_global_variables.volumes[voice_id].lpf=value
			let lpf=Math.round(value*127)
			if(channel==null)return
			SF_manager.synth.controllerChange(channel,74,lpf)
		break
		case "vel":
			let v_value=Math.round((V_velocity_default_min)*(1-value)+V_velocity_default_max*value)
			V_global_variables.volumes[voice_id].vel=v_value
			if(SF_manager!=null)SF_manager.synth.nuzic_sequencer.update_V_global_variables(V_global_variables)
		break
	}
}

function BNAV_knob_save_value(){
	let value=BNAV_knob_position.end_value
	let type=BNAV_knob_position.type
	let voice_id=BNAV_knob_position.voice_id
	//APP_stop()
	let data = DATA_get_current_state_point(true)
	//Selected voice
	let selected_voice_data= data.voice_data.find(item=>{
		return item.voice_id==voice_id
	})
	switch (type) {
		case "reverb":
			V_global_variables.effects.reverb=value
			data.global_variables.effects.reverb=value
		break
		case "voice_pan":
			V_global_variables.volumes[voice_id].pan=value
			selected_voice_data.pan=value
		break
		case "fx":
			V_global_variables.volumes[voice_id].fx=value
			selected_voice_data.fx=value
		break
		case "lpf":
			V_global_variables.volumes[voice_id].lpf=value
			selected_voice_data.lpf=value
		break
		case "vel":
			let v_value=Math.round((V_velocity_default_min)*(1-value)+V_velocity_default_max*value)
			V_global_variables.volumes[voice_id].vel=v_value
			selected_voice_data.vel=v_value
		break
	}
	let old_RE=flag_DATA_updated.RE
	let old_PMC=flag_DATA_updated.PMC
	DATA_insert_new_state_point(data)//XXX ???? like vol channel is in DATA save???
	flag_DATA_updated.RE=old_RE
	flag_DATA_updated.PMC=old_PMC
}

function BNAV_knob_double_click(value){
	//button.value=value
	BNAV_knob_position.knob_div.setAttribute("value",value)
	BNAV_knob_position.end_value=value
	//rotate
	let value_degree=(BNAV_knob_position.end_value*2-1)*135
	BNAV_knob_position.knob_div.style=`transform: rotate(${value_degree}deg)`
	BNAV_knob_modify_value()
	BNAV_knob_save_value()
}

function BNAV_slider_double_click(slider,value){
	//button.value=value
	slider.value=value
	let event = new Event('input')
	slider.dispatchEvent(event)
	event = new Event('change')
	// Dispatch it.
	slider.dispatchEvent(event)
}

function BNAV_voice_id_to_channel(voice_id){
	return V_global_variables.channel_voice_id[voice_id]
}


function sendPolyphonicAftertouch(noteNumber, pressure, channel = 0) {
	const statusByte = 0xA0 + channel;  // Polyphonic Aftertouch for the channel
	let output=midi_output_devices.find(output=>{
		return output.id=="OmDrhPUg97/k9pY/ic3RVKw1zdFhAvjaF+EsPQrnax0="
	})
	output.send([statusByte, noteNumber, pressure]);
}



//TEST AFTERTOUCH


/*  Sending Polyphonic Aftertouch via JavaScript

function sendPolyphonicAftertouch(output, noteNumber, pressure, channel = 0) {
    const statusByte = 0xA0 + channel;  // Polyphonic Aftertouch for the channel
    output.send([statusByte, noteNumber, pressure]);
}

// Example: send polyphonic aftertouch to MIDI note 60 (C4)
navigator.requestMIDIAccess()
  .then(onMIDISuccess, onMIDIFailure);

function onMIDISuccess(midiAccess) {
    const outputs = Array.from(midiAccess.outputs.values());
    if (outputs.length > 0) {
        const output = outputs[0];

        // Send a note on (optional) and polyphonic aftertouch
        sendNoteOn(output, 60);  // Send Note On message for C4
        sendPolyphonicAftertouch(output, 60, 80);  // Apply aftertouch to C4 with pressure value 80

        setTimeout(() => sendNoteOff(output, 60), 1000);  // Stop the note after 1 second
    }
}

function onMIDIFailure() {
    console.log('Could not access MIDI devices.');
}

// Basic note on/off functions for context
function sendNoteOn(output, noteNumber, velocity = 0x7f, channel = 0) {
    const statusByte = 0x90 + channel;  // Note On for the channel
    output.send([statusByte, noteNumber, velocity]);
}

function sendNoteOff(output, noteNumber, channel = 0) {
    const statusByte = 0x80 + channel;  // Note Off for the channel
    output.send([statusByte, noteNumber, 0x40]);  // velocity of 0x40
}


Explanation of Code:

    The sendPolyphonicAftertouch function sends a Polyphonic Aftertouch message for a specific note.
    In this case, the note C4 (noteNumber = 60) is triggered with a Note On message, and shortly after, a Polyphonic Aftertouch message is sent, applying pressure value 80 to that note.
    You can adjust the pressure dynamically while the note is being held.

Polyphonic Aftertouch vs Channel Aftertouch:
Feature	Polyphonic Aftertouch	Channel Aftertouch
Pressure Applied To	Individual notes	Entire MIDI channel (all notes)
Message Status Byte	0xA0 + channel	0xD0 + channel
Data Sent	Note number and pressure value	Pressure value only (no note number)
Supported Controllers	Fewer, more advanced controllers	Most modern MIDI keyboards and devices
Expressiveness	Allows more nuanced control of each note	Affects all notes equally in the c

/*++

Example: Sending Polyphonic Aftertouch via JavaScript

with spessasynth

tune(1, 0, 36, 0);
tune(2, 0, 36, 50);
tune(3, 0, 36, 80);

synth.noteOn(0, 1, 127);
synth.noteOn(0, 2, 127);
synth.noteOn(0, 3, 127);



If your MIDI controller supports Polyphonic Aftertouch, you can send these messages from JavaScript using the Web MIDI API. Here's an example of how to send a Polyphonic Aftertouch message for a specific note:

javascript

function sendPolyphonicAftertouch(output, noteNumber, pressure, channel = 0) {
    const statusByte = 0xA0 + channel;  // Polyphonic Aftertouch for the channel
    output.send([statusByte, noteNumber, pressure]);
}

// Example: send polyphonic aftertouch to MIDI note 60 (C4)
navigator.requestMIDIAccess()
  .then(onMIDISuccess, onMIDIFailure);

function onMIDISuccess(midiAccess) {
    const outputs = Array.from(midiAccess.outputs.values());
    if (outputs.length > 0) {
        const output = outputs[0];

        // Send a note on (optional) and polyphonic aftertouch
        sendNoteOn(output, 60);  // Send Note On message for C4
        sendPolyphonicAftertouch(output, 60, 80);  // Apply aftertouch to C4 with pressure value 80

        setTimeout(() => sendNoteOff(output, 60), 1000);  // Stop the note after 1 second
    }
}

function onMIDIFailure() {
    console.log('Could not access MIDI devices.');
}

// Basic note on/off functions for context
function sendNoteOn(output, noteNumber, velocity = 0x7f, channel = 0) {
    const statusByte = 0x90 + channel;  // Note On for the channel
    output.send([statusByte, noteNumber, velocity]);
}

function sendNoteOff(output, noteNumber, channel = 0) {
    const statusByte = 0x80 + channel;  // Note Off for the channel
    output.send([statusByte, noteNumber, 0x40]);  // velocity of 0x40
}

Explanation of Code:

    The sendPolyphonicAftertouch function sends a Polyphonic Aftertouch message for a specific note.
    In this case, the note C4 (noteNumber = 60) is triggered with a Note On message, and shortly after, a Polyphonic Aftertouch message is sent, applying pressure value 80 to that note.
    You can adjust the pressure dynamically while the note is being held.

Polyphonic Aftertouch vs Channel Aftertouch:
Feature	Polyphonic Aftertouch	Channel Aftertouch
Pressure Applied To	Individual notes	Entire MIDI channel (all notes)
Message Status Byte	0xA0 + channel	0xD0 + channel
Data Sent	Note number and pressure value	Pressure value only (no note number)
Supported Controllers	Fewer, more advanced controllers	Most modern MIDI keyboards and devices
Expressiveness	Allows more nuanced control of each note	Affects all notes equally in the channel
Use Cases for Polyphonic Aftertouch:

    MPE (MIDI Polyphonic Expression): Polyphonic Aftertouch is one of the key features in MPE (MIDI Polyphonic Expression), which is used by expressive instruments like the ROLI Seaboard or LinnStrument. MPE enables per-note pitch bends, slides, and aftertouch, making it ideal for highly expressive and fluid performance styles.

    Advanced Sound Design: If you're using a synthesizer that supports Polyphonic Aftertouch, it can be used to control sound parameters at the individual note level, such as opening the filter or adding vibrato to just one note in a chord.

Challenges and Limitations:

    Hardware Limitations: Many standard MIDI keyboards do not support Polyphonic Aftertouch because it requires more advanced sensing technology for detecting pressure on each key independently. Only a few premium devices offer this feature.

    Software Support: Even if your MIDI controller supports Polyphonic Aftertouch, not all synthesizers or virtual instruments can interpret it. Check your synthesizer's manual or MIDI implementation chart to ensure it can respond to Polyphonic Aftertouch messages.

*/











