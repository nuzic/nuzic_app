function APP_add_suggestion_to_A_global_list(suggestion){
	//global variables
	if(suggestion!=""){
		A_global_list.push(suggestion)
		A_global_list=[...new Set(A_global_list)];
	}
}

function APP_scan_A_suggestions(element,direction){
	if(A_global_list.length!=0){
		let index=A_global_list.indexOf(element.value)
		let new_index=direction?index+1:index-1
		if(index==-1)new_index=direction?0:null
		if(new_index==A_global_list.length || new_index<0)new_index=null
		if(new_index!=null){
			element.value=A_global_list[new_index]
		}else{
			element.value=""
		}
	}
}

function APP_show_A_suggestions(input){
	//if(input.value==""){
		//input.placeholder=A_global_list[0]==null?"":A_global_list[0]+"↕"
		//input.placeholder=A_global_list[0]==null?"":"⇅"
		input.placeholder=A_global_list[0]==null?"":"↑ ↓"
	//}
}

function APP_text_to_A_list(string,type){
	//return null if error
	string = string.replace(" ", A_spacer)
	let string_array=string.split(A_spacer)
	let A_list=[]
	let max_note_number = TET * N_reg-1
	let error=false
	switch (type) {
	case "Aa":
		string_array.forEach(item=>{
			let note_number = Math.floor(item)
			if(isNaN(note_number)){
				note_number = null
				error=true
			}
			A_list.push(note_number)
		})
		break;
	case "Am":
		string_array.forEach(item=>{
			let note_number = null
			let substr=item.split("r")
			if(substr.length>2)error=true
			let resto = Math.floor(substr[0])
			let reg=0
			if(substr[1]>=0 && substr[1]!=""){
				reg=Math.floor(substr[1])
			}
			if(resto>=TET){
				note_number = null
				error=true
			}else{
				note_number = resto + reg*TET
				if(isNaN(note_number)){
					note_number = null
					error=true
				}
			}
			A_list.push(note_number)
		})
		break;
	case "Ag":
		string_array.forEach(item=>{
			let note_number = Math.floor(item)
			if(isNaN(note_number)){
				note_number = null
				error=true
			}
			A_list.push(note_number)
		})
		break;
	}
	if(error)return null
	//clear of 0 es
	result=A_list.filter(item=>{return item>0})
	if(result==null)result=[]
	return result
}

function APP_A_list_to_text(A_list,type,scale_info=null){
	//scale info==   [current_grades_data,base_note,pos]
	let string=""
	if(A_list.length!=0){
		switch (type) {
		case "Aa":
			//give simple Aa
			//A_list.forEach(item=>{string+=","+item})
			//A_list.forEach(item=>{string+=" "+item})//large space Em
			A_list.forEach(item=>{string+=A_spacer+item})//medium space En
			break;
		case "Am":
			let mod = TET
			A_list.forEach(item=>{
				let reg = Math.floor(item/mod)
				let resto = item%mod
				let value=""
				if(reg != 0){
					value=`${resto}r${reg}`
				} else {
					value=resto
				}
				string+=A_spacer+value//medium space En
			})
			break;
		case "Ag":
			if(scale_info==null){
				//string=A_spacer+no_value
				string=APP_A_list_to_text(A_list,"Am")
			}else{
				let prev_note=scale_info.base_note
				A_list.forEach(item=>{
					let value=""
					let current_note=scale_info.pos?prev_note+item:prev_note-item
					value= Math.abs(DATA_calculate_grade_interval_string(prev_note,scale_info.base_diesis,scale_info.current_grades_data,current_note,scale_info.base_diesis,scale_info.current_grades_data))
					string+=A_spacer+value//medium space En
					prev_note=current_note
				})
			}
			break;
		}
		string=string.substr(1)
	}
	return string
}

function APP_add_A_text_to_A_global_list(A_text){
	//global variables
	if(A_text.length!=0){
		A_global_list.push(A_text)
		A_global_list=[...new Set(A_global_list)];
	}
}

function APP_inpTest_A_line(evt,element,type){
	let ch = String.fromCharCode(evt.which)
	if(!((/[0-9]/.test(ch)) || (/[r]/.test(ch)) || (/[ ]/.test(ch)))){
		evt.preventDefault()
	}
	//if space must be after a number
	let prev_str = APP_input_box_prev_str(element)
	let next_str = APP_input_box_next_str(element)
	let prev_char = APP_input_box_prev_char(element)
	let next_char = APP_input_box_next_char(element)
	let isSelected = APP_input_box_is_selected(element)
	let isEmpty = !!(element.value=="")
	let stringOk = false
	if((/[ ]/.test(ch) 	&& !(isSelected || isEmpty)
						//&& (prev_char!=" " || prev_char.charCodeAt(0)!=8194))//medium space En
						&& prev_char!=" " && prev_char!=A_spacer)//medium space En
		){
		stringOk = true
	}
	if(!/[ ]/.test(ch)){
		element.value = element.value.replace(" ", A_spacer)//medium space En
	}
	switch (type) {
		case "Aa":
			if( /[0-9]/.test(ch)){
				//control if < TET*N_reg-1
				let new_string = prev_str+ch+next_str
				let new_string_array=new_string.split(A_spacer)
				let new_values=new_string_array.map(el=>parseInt(el))
				let maxvalue=Math.max(...new_values)
				if(isSelected || maxvalue<TET*N_reg){
					stringOk = true
				}
			}
			break;
		case "Am":
			if(/[r]/.test(ch)	&& (isSelected || isEmpty)){
				stringOk = true
			}
			//if r is after first number
			//after space or space+number XXX
			//not before another r or number r??XXX
			if(/[r]/.test(ch)	&& prev_char!="r" && next_char!="r"){
				stringOk = true
			}
			//if number
			//real control in onfocusout
			if( /[0-9]/.test(ch)){
				stringOk = true
			}
			// let new_string = prev_str+ch+next_str
			// let new_string_array=new_string.split(A_spacer)
			break;
		case "Ag":
			///probably different??? XXX XXX XXX XXX
			if( /[0-9]/.test(ch)){
				//control if < TET*N_reg-1
				let new_string = prev_str+ch+next_str
				let new_string_array=new_string.split(A_spacer)
				let new_values=new_string_array.map(el=>parseInt(el))
				let maxvalue=Math.max(...new_values)
				if(isSelected || maxvalue<TET*N_reg){
					stringOk = true
				}
			}
			break;
	}
	if(!stringOk){
		evt.preventDefault()
	}
	if(element.classList.contains("App_RE_A_cell")){
		APP_inpTest_A_line_size(element)
	}
}

function APP_inpTest_A_line_size(element){
	let A_blocs=Math.ceil(element.value.length/4)
	let width=element.clientWidth/28//`style="width: calc(var(--RE_block) * ${width});
	if(A_blocs==0)A_blocs=1
	let A_pad=((width-A_blocs<=0))?"0 + 2px":(width-A_blocs)
	element.style.paddingRight=`calc(var(--RE_block) * ${A_pad})`
}
